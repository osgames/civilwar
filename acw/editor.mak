#####################################################################
#
# Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
# This Software is subject to the GNU General Public License.  
# For License information see the file COPYING in the root directory of the project.
# For more information see the file README.
#
#####################################################
# $Id$
####################################################
#
# ACW Editors makefile
#
# Use with Watcom wmake
#
####################################################
#
# $Log$
# Revision 1.1  2001/03/15 14:08:48  greenius
# Converted filenames to lower case
#
# Revision 1.1  2001/03/11 00:58:48  greenius
# Added to sourceforge
#
#
####################################################

!include include.mak

##################
# Set up special compilation stuff

.extensions:
.extensions: .exe .ldb .lnk .lib .obj .cpp .c .h .asm .i

O = camped.obs
.c   : $(C)
.cpp : $(C)
.h   : $(H)
.obj : $(O)
.asm : $(S)
.i   : $(I)
.exe : $(EXEPATH)
.lib : $(LIBDIR)
.ldb : $(LNK)
.lnk : $(LNK)

.cpp.obj: .AUTODEPEND
	set WPP386=$(CPPFLAGS) -DCAMPEDIT
	$(CPP) $[* -fo$(O)\$^.


#######################
# Program specifics

.BEFORE:
	@del *.err *.tmp c\*.err s\*.err >& nul:

all: camped.exe .SYMBOLIC

#######################
# Campaign EDITOR

CAMPEDOBS = &
	camped.obj &
	ed_state.obj ed_facil.obj ed_water.obj ed_troop.obj ed_terr.obj &
	campwld.obj &
	resource.obj mobilise.obj moblist.obj &
	unitcamp.obj camptab.obj tables.obj &
	cal.obj calmain.obj calicon.obj calshow.obj &
	calgen.obj calreg.obj calinfo.obj &
	mapwind.obj &
	mapob.obj city.obj &
	generals.obj orders.obj ob.obj &
	railway.obj water.obj naval.obj &
	scroll.obj menuicon.obj gameicon.obj filesel.obj &
	options.obj &
	bitmap.obj barchart.obj &
	measure.obj gametime.obj &
	pool.obj language.obj &
	dfile.obj filesup.obj obfile.obj wldfile.obj &
	zoom_map.obj	&
	side.obj	&
	files.obj
	
#	datafile.obj dataread.obj datawrit.obj logarmy.obj 

camped.exe: $(CAMPEDOBS) $(DAGLIB)
	@%make camped.$(LNKEXT)
	$(LN) $(LNFLAGS) NAME $@ @$(LNK)\camped.$(LNKEXT)
!ifdef %DBPATH
        $(CP) $@
!endif

camped.$(LNKEXT): editor.mak
	@echo making $(LNK)\$^.
	@%create $(LNK)\$^.
	@%append $(LNK)\$^. PATH $(O)
	@%append $(LNK)\$^. LIB $(LIBDIR)\$(DAGLIB)
	@%append $(LNK)\$^. LIB $(DSMILIB)
	@for %i in ($(CAMPEDOBS)) do @%append $(LNK)\$^. FILE %i

