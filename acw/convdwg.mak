#####################################################################
#
# Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
# This Software is subject to the GNU General Public License.  
# For License information see the file COPYING in the root directory of the project.
# For more information see the file README.
#
#####################################################
#
# Makefile for Testing Binary Files
#
#####################################################
#
# (C) 1994, Steven Green, Dagger Interactive Technologies Ltd
#
#####################################################

!include include.mak

all: convdwg.exe .SYMBOLIC

CONV_OBS = convdwg.obj &
	dfile.obj filesup.obj obfile.obj wldfile.obj &
	city.obj generals.obj &
	gametime.obj water.obj orders.obj resource.obj mapob.obj &
	campwld.obj ob.obj railway.obj mobilise.obj naval.obj &
	datafile.obj dataread.obj moblist.obj &
	camptab.obj options.obj tables.obj files.obj lang_dum.obj


CONV_LNK = convdwg.$(LNKEXT)

convdwg.exe : $(CONV_OBS)
	@%make $(CONV_LNK)
	$(LN) $(LNFLAGS) NAME $@ @$(LNK)\$(CONV_LNK)

$(CONV_LNK) : convdwg.mak
	@%create $(LNK)\$^.
	@%append $(LNK)\$^. PATH $(O)
	@%append $(LNK)\$^. LIB $(LIBDIR)\$(DAGLIB)
	@%append $(LNK)\$^. LIB $(DSMILIB)
	@for %i in ($(CONV_OBS)) do @%append $(LNK)\$^. FILE %i

################################
# Test Program

TESTDFIL_OBS = testdfil.obj &
	dfile.obj filesup.obj obfile.obj wldfile.obj &
	city.obj generals.obj &
	gametime.obj water.obj orders.obj resource.obj mapob.obj &
	campwld.obj ob.obj railway.obj mobilise.obj naval.obj &
	camptab.obj options.obj tables.obj


TESTDFIL_LNK = testdfil.$(LNKEXT)

testdfil.exe : $(TESTDFIL_OBS)
	@%make $(TESTDFIL_LNK)
	$(LN) $(LNFLAGS) NAME $@ @$(LNK)\$(TESTDFIL_LNK)

$(TESTDFIL_LNK) : convdwg.mak
	@%create $(LNK)\$^.
	@%append $(LNK)\$^. PATH $(O)
	@%append $(LNK)\$^. LIB $(LIBDIR)\$(DAGLIB)
	@%append $(LNK)\$^. LIB $(DSMILIB)
	@for %i in ($(TESTDFIL_OBS)) do @%append $(LNK)\$^. FILE %i




