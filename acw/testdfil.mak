#####################################################################
#
# Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
# This Software is subject to the GNU General Public License.  
# For License information see the file COPYING in the root directory of the project.
# For more information see the file README.
#
#####################################################################
#
# Makefile for Testing Binary Files
#
#####################################################
#
# (C) 1994, Steven Green, Dagger Interactive Technologies Ltd
#
#####################################################

!include include.mak

all: testdfil.exe .SYMBOLIC

DFIL_OBS = testdfil.obj dfile.obj
DFIL_LNK = testdfil.$(LNKEXT)

testdfil.exe : $(DFIL_OBS)
	@%make $(DFIL_LNK)
	$(LN) $(LNFLAGS) NAME $@ @$(LNK)\$(DFIL_LNK)

$(DFIL_LNK) : testdfil.mak
	@%create $(LNK)\$^.
	@%append $(LNK)\$^. PATH $(O)
	@%append $(LNK)\$^. LIB $(LIBDIR)\$(DAGLIB)
	@%append $(LNK)\$^. LIB $(DSMILIB)
	@for %i in ($(DFIL_OBS)) do @%append $(LNK)\$^. FILE %i



