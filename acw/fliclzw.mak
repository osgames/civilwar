#####################################################################
#
# Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
# This Software is subject to the GNU General Public License.  
# For License information see the file COPYING in the root directory of the project.
# For more information see the file README.
#
#####################################################################

!include include.mak

FLICLZOBS = fliclzw.obj lzencode.obj lzmisc.obj

fliclzw.exe : $(FLICLZOBS)
	@%make fliclzw.lnk
	$(LN) $(LNFLAGS) NAME $@ @fliclzw.lnk
	
fliclzw.lnk : fliclzw.mak
	@%create $^.
	@%append $^. PATH $(O)
	@for %i in ($(FLICLZOBS)) do @%append $^. FILE %i
