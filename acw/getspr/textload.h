/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */

#ifndef strlen
#include<string.h>
#endif

#ifndef EOF
#include<stdio.h>
#endif

#ifndef fstream
#include<fstream.h>
#endif


/*
	 'textload' loads a ascii file called 'filename'.

*/

class TextLoad		
{

 protected:

	char *textfile;
	fstream txt;
 
	long tsize;
 
	char *texdata;
 	char error;

 public:

	TextLoad( const char *filename );
	void getdata();

	long getSize()
		{ return	tsize;	}

	char *gettexdata()
		{ return texdata; }

	void setpointer( int pointer )
		{ txt.seekg ( pointer ); tsize = tsize - pointer; }
		
//  	const char err()
//				{ return error; }

};


TextLoad::TextLoad( const char *filename )
{
	textfile = new char [ strlen ( filename ) + 1 ];
	cout << ".";
	for ( char loop = 0; loop <= strlen( filename ); loop++ )
	  {
	   textfile[loop] = filename[loop];
	  }
		
	txt.open( textfile, ios::in | ios::binary );
	error = 0;
	
	if ( !txt )
		{
		 error = 1;
		 cout << "Can't find that text file!!\n";
		}
	else
	  {
		
			txt.seekg( 0, ios::end);
			tsize = txt.tellg ();
			txt.seekg( 0, ios::beg );    // restore read ptr position;
	
			texdata = new char [ tsize + 1 ];

		}
	  
}



void TextLoad::getdata()
{
	txt.read( texdata, tsize );
 //	keyw.read( kwdata, ksize );

	texdata[ tsize ] = '\0';
 //	kwdata[ ksize ] = '\0';
}


