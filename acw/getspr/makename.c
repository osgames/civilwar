/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * $Id$
 *
 * Filename handling function
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:20:08  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:10:25  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1993/12/16  16:42:54  Steven_Green
 * Initial revision
 *
 * 
 *    Rev 1.1   25 Jun 1993 15:44:14   sgreen
 * No change.
 * 
 *    Rev 1.0   28 Apr 1993 16:33:48   sgreen
 * Initial revision.
 * Revision 1.1  1992/10/17  00:11:14  sgreen
 * Initial revision
 *
 */

#include <string.h>
#include "types.h"
#include "makename.h"

/*
 * Make a filename with the given extension if one didn't already exist
 *
 * If force is true then any existing extension is replaced
 */

void makeFilename(char *dest, char *src, char *ext, BOOL force)
{
	char *s;

	strcpy(dest, src);

	/*
    * Find start of filename (lose path or drive)
    */

	s = strrchr(dest, '\\');
	if(s == NULL)
		s = strrchr(dest, ':');
	if(s == NULL)
		s = dest;

	/*
    * See if there is an extension
	 */

	s = strchr(s, '.');

	/*
	 * Add the new extension
	 */

	if(s == NULL)
		strcat(dest, ext);
	else if(force)
		strcpy(s, ext);
}

