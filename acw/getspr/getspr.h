/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef H_GETSPR
#define H_GETSPR
/*
 * $Id$
 *
 * Header used by sprite extraction utility
 * Began life as TILE.H (from Mark Mccubbin)
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:20:08  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:10:25  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1993/12/16  16:42:54  Steven_Green
 * Initial revision
 *
 * 
 *    Rev 1.1   25 Jun 1993 15:44:18   sgreen
 * No change.
 * 
 *    Rev 1.0   28 Apr 1993 16:33:54   sgreen
 * Initial revision.
 * Revision 1.2  1992/10/15  23:00:43  sgreen
 * Added RCS Stuff
 * Added #ifdef H_GETSPR
 *
 */

#define	mskNone				0
#define  mskHasMask				1
#define  mskHasTransparentColor	2
#define  mskLasso				3

#define  cmpNone		0
#define  cmpByteRun1	1

struct {
	byte	ckID[4];
	long	ckSize;
} chunk;

struct {
	word	w, h;
	word	x, y;
	byte	nPlanes;
	byte	masking;
	byte	compression;
	byte	pad1;
	word	transparentColor;
	byte	xAspect, yAspect;
	word	pageWidth, pageHeight;
} BMHD;

void WaitTicks(unsigned int ticks);
void GoodBye();
void bye(void);
int rfile(char far *rbuffer,unsigned long rsize,FILE *rf1 );
int wfile(char far *wbuffer,unsigned long wsize,FILE *wf1 );
void readBODY(FILE *ilbm_file);
void readBMHD(FILE *ilbm_file,unsigned long size_of);
byte *readlongs(byte *s,FILE *ilbm_file);
unsigned long readlong(FILE *ilbm_file);
char ilbm_to_raw (char *name);
int raw_to_tile (char *source,char *name);
void grab_tile (uint tcount,unsigned int x_i,unsigned int y_i);
void grab_qtile (unsigned int x_i,unsigned int y_i);
void build_filenames (char *namesv[]);
void suss_options ( int optc, char *optv[]);
void write_tile (uint x_i,uint x1_i,uint x2_i,uint x_size,uint y_i,uint y1_i,uint y2_i,uint y_start);
void get_tile (uint x,uint y,uint x_sz,uint y_sz);
void nibble_reset (void);
int nibble_flush (FILE *nibble_file);
int nibble_write (uchar nibble,FILE *nibble_file);
void copy_palette (uint start_colour, uint no_of_colours, uint to_colour);

uint user_def (void);

uint squeeze (void);
uint rle0 (void);
void remove_mcc_header (void);
void make_asm (void);
void make_remap (void);


#endif	/* H_GETSPR */
