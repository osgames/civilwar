/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Machine independant file format
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:20:08  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:10:25  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1993/12/16  16:42:54  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "filedata.h"

void putWord(IWORD* dest, UWORD src)
{
	dest->w = src;
}

void putByte(IBYTE* dest, UBYTE src)
{
	dest->b = src;
}

void putLong(ILONG* dest, ULONG src)
{
	dest->l = src;
}

