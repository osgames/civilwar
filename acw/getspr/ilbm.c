/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * $Id$
 *
 * Read in LBM files
 *
 * Written by Steven Green
 *
 * Credit where credit's due...
 *   This was mostly stolen from the source to TILE.C, which was written
 *   by Mark McCubbins, who in turn borrowed bits from Dave Shea.  I
 *   of course hacked it about a lot since...
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:20:08  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:10:25  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1994/04/20  22:26:56  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/12/16  16:42:54  Steven_Green
 * Initial revision
 *
 * 
 *    Rev 1.1   25 Jun 1993 15:44:12   sgreen
 * No change.
 * 
 *    Rev 1.0   28 Apr 1993 16:33:48   sgreen
 * Initial revision.
 * Revision 1.4  1992/12/09  16:10:52  sgreen
 * Palette defined as array of [256][3] instead of [256*3]
 *
 * Revision 1.3  1992/10/23  16:22:38  sgreen
 * transparent color and other fields are taken from BMHD chunk.
 *
 * Revision 1.2  1992/10/19  20:46:24  sgreen
 * error and report are used for any screen display to be neater
 *
 * Revision 1.1  1992/10/17  00:11:21  sgreen
 * Initial revision
 *
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
// #include <malloc.h>
#include <io.h>

#include "types.h"
#include "ilbm.h"
#include "makename.h"
#include "verbose.h"

#define farmalloc(s) malloc(s)
#define farfree(m) free(m)

/*
 * Global variables
 */

char *ilbm_ptr = NULL;			/* Pointer to store decompressed screen */
uchar ilbm_palette[256][3];	/* Where palette is stored */
BMHD bmhd;

/****
* GET IN 4 BYTES FROM FILE TERMINATE WITH \0
* Also a David Shea hack!
****/
byte *readlongs(byte *s,FILE *ilbm_file)
{
	byte *q;
	
	q = s;
	*s++ = (char)fgetc(ilbm_file);
	*s++ = (char)fgetc(ilbm_file);
	*s++ = (char)fgetc(ilbm_file);
	*s++ = (char)fgetc(ilbm_file);
	*s++ = '\0';

	return(q);
}

unsigned long readlong(FILE *ilbm_file)
{
	byte x[5];

	readlongs(x,ilbm_file);
	return(	0x1000000L*(long)x[0] + 0x10000L*(long)x[1] +
				0x100L*(long)x[2] + (long)x[3] );
}

/* Read in BODY of ILBM file
 * Decrunch if it's crunch else just copy to memory (unlikely!)
 * Following this convert from ILBM to PBM if file loaded was "ILBM"
 * This means that you have to alloc enough mem for a byte per pixel
 *
 * returns:
 *   0= OK
 */

int readBODY(FILE *ilbm_file)
{
	unsigned long body_size=readlong(ilbm_file);
	char *buffer;
	long l;

	// unsigned long expanded=(unsigned long)((((unsigned long)bmhd.w/8)*(unsigned long)bmhd.h)*(unsigned long)bmhd.nPlanes);

	unsigned long expanded = bmhd.w * bmhd.h;


	/* Allocate memory for decompressed ILBM */

	if(ilbm_ptr != NULL)		/* Free any old buffer */
		farfree(ilbm_ptr);

	ilbm_ptr=farmalloc(expanded);
	buffer=ilbm_ptr;
	if (ilbm_ptr==NULL) {
		error(0, NULL, NULL, "Couldn't Alloc ILBM space");
		return -1;
		}
	
	report(V_VERBOSE, "  Memory usage: %lu \n", expanded);

	switch(bmhd.compression)
	{
	case cmpNone:
		if(expanded != body_size)
				error(0, NULL, NULL, "Expanded size (%ld) and Body size (%ld) are different", expanded, body_size);

		fread(ilbm_ptr, expanded, 1, ilbm_file);
		break;

	case cmpByteRun1:


		l = expanded;

		while(l > 0)
		{
			uchar v = fgetc(ilbm_file);

			if (v > 127)
			{
				int count = 256 - v;

				v = fgetc(ilbm_file);
				l -= count + 1;

				do
					*buffer++ = v;
				while(count--);
			}
			else
			{
				size_t count = v + 1;

				fread(buffer, count, 1, ilbm_file);
				buffer += count;
				l -= count;
	  		}
	  	}

		break;

	default:
		error(0, NULL, NULL, "Unknown compression type %d", bmhd.compression);
		break;
	}

	return 0;
}

/* Read in BMAP header from ILBM file */
/* Hacked around from David Shea's genspr.c program (which means I can't */
/* blame him if it doesn't work......) */
/* This should return the file position to the correct address after the */
/* BMHD */

void readBMHD(FILE *ilbm_file,unsigned long size_of)
{
	bmhd.w = (word)fgetc(ilbm_file)*256; size_of-=2;
	bmhd.w += (word)fgetc(ilbm_file);
	bmhd.h = (word)fgetc(ilbm_file)*256; size_of-=2;
	bmhd.h += (word)fgetc(ilbm_file);

	bmhd.x = (word)fgetc(ilbm_file)*256; size_of-=2;
	bmhd.x += (word)fgetc(ilbm_file);
	bmhd.y = (word)fgetc(ilbm_file)*256; size_of-=2;
	bmhd.y += (word)fgetc(ilbm_file);

	bmhd.nPlanes     = fgetc(ilbm_file); size_of -= 1;
	bmhd.masking     = fgetc(ilbm_file); size_of -= 1;
	bmhd.compression = fgetc(ilbm_file); size_of -= 1;
	bmhd.pad1        = fgetc(ilbm_file); size_of -= 1;
	bmhd.transparentColor = fgetc(ilbm_file) * 256;
	bmhd.transparentColor += fgetc(ilbm_file); size_of -= 2;
	
	bmhd.xAspect = fgetc(ilbm_file); size_of -= 1;
	bmhd.yAspect = fgetc(ilbm_file); size_of -= 1;
	
	while (size_of!=0){
		fgetc(ilbm_file);
		size_of--;
		}

	report(V_VERBOSE, "  Reading ILBM/PBM xsize:%u,ysize:%u,%u planes\n", bmhd.w, bmhd.h, bmhd.nPlanes);
}

/*
 * Decompress an ILBM to raw data
 *
 * returns:
 *   0 = OK
 */

int ilbm_to_raw (char *name)
{
	FILE *ilbm_file;
	unsigned long file_size;
	char ilbm_string[5];
	ulong	pad_size,palette_size;
		
	char fileName[FILENAME_MAX];

	makeFilename(fileName, name, ".LBM", FALSE);

	ilbm_file=fopen(fileName,"rb");

	if(!ilbm_file)
	{
		error(0, fileName, NULL, "Could not open file");
		return -1;
	}

	file_size=filelength(fileno(ilbm_file));
	if (file_size==0)
	{
		error(0, fileName, NULL, "Couldn't open file %s", fileName);
		return (-1);
	}

	if (!strncmp( (readlongs(ilbm_string,ilbm_file)) ,"FORM",4))
	{
		readlong(ilbm_file);	/* size of FORM */

		/* which type 'ILBM' / 'PBM '*/

		if (!strncmp( (readlongs(ilbm_string,ilbm_file)) ,"ILBM",4))
		{
			error(0, fileName, NULL, "Use IFFCONV to fix ILBM(Amiga) to PBM(256 colour)\n");
			fclose(ilbm_file);
			return(-1);
		}

		while (!feof(ilbm_file))
		{
			readlongs(ilbm_string,ilbm_file);
			if (!strncmp(ilbm_string,"BMHD",4))
			{
				readBMHD(ilbm_file,readlong(ilbm_file));
			}
			else if (!strncmp(ilbm_string,"BODY",4))
			{
				if(readBODY(ilbm_file) != 0)
				{
					fclose(ilbm_file);
					return -1;
				}
				fclose(ilbm_file);
				return (0);
			}
			else if (!strncmp(ilbm_string,"CMAP",4))
			{
				palette_size = ((readlong(ilbm_file)) + 1) & -2L;
				fread(ilbm_palette, palette_size, 1, ilbm_file);
				// for (i=0;i<palette_size;ilbm_palette[i]=(uchar)fgetc(ilbm_file), i++);
			}
			else
			{
				pad_size = ((readlong(ilbm_file)) + 1) & -2L;
				fseek(ilbm_file, pad_size, SEEK_CUR);
			}
		}
		fclose(ilbm_file);
		error(0, fileName, NULL, "ILBM/PBM file doesn't seem to have a BODY chunk!\n");
		return (-1);
	}
	else
	{
		error(0, fileName, NULL, "%s is not an ILBM/PBM file", fileName);
		return (-1);
	}
}


