/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef H_ILBM
#define H_ILBM
/*
 * $Id$
 *
 * Header for ILBM... functions for reading LBM files
 *
 * Written by Steven Green based on Mark McCubbin's stuff from TILE
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:20:08  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:10:25  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1993/12/16  16:42:54  Steven_Green
 * Initial revision
 *
 * 
 *    Rev 1.1   25 Jun 1993 15:44:18   sgreen
 * No change.
 * 
 *    Rev 1.0   28 Apr 1993 16:33:54   sgreen
 * Initial revision.
 * Revision 1.2  1992/12/09  16:10:20  sgreen
 * palette defined as 2D array instead of 1D
 *
 * Revision 1.1  1992/10/17  00:12:17  sgreen
 * Initial revision
 *
 *
 */

/*
 * Constants and structures used by ILBM files
 */

#define	mskNone				0
#define  mskHasMask				1
#define  mskHasTransparentColor	2
#define  mskLasso				3

#define  cmpNone		0
#define  cmpByteRun1	1

typedef struct {
	byte	ckID[4];
	long	ckSize;
} CHUNK;

typedef struct {
	word	w, h;
	word	x, y;
	byte	nPlanes;
	byte	masking;
	byte	compression;
	byte	pad1;
	word	transparentColor;
	byte	xAspect, yAspect;
	word	pageWidth, pageHeight;
} BMHD;

/*
 * Global variables
 */

extern char *ilbm_ptr;					/* Pointer to store decompressed screen */
extern uchar ilbm_palette[256][3];	/* Where palette is stored */
extern BMHD bmhd;

/*
 * Functions
 */

int ilbm_to_raw(char *name);				/* Load an ILBM file */

#endif	/* H_ILBM */
