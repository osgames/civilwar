/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *-------------------------------------------------------------
 * $Id$
 *-------------------------------------------------------------
 *
 * Utility program to grab fonts from LBM files
 *
 * It started life as Mark Mccubbin's tile grabber program
 * but I wanted to make so many changes that it was difficult
 * to maintain backwards compatibility, so decided to give
 * it a new name and continue development unhindered by the
 * past.
 *
 * Written by Steven Green
 *
 *-------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:20:08  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:10:25  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/07/15  13:18:29  Steven_Green
 * Initial revision
 *
 *
 *-------------------------------------------------------------
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <conio.h>
#include <sys/stat.h>

#include "types.h"
#include "swgspr.h"
#include "ilbm.h"
#include "makename.h"
#include "verbose.h"

/*
 * Error values that may be returned by functions
 */

typedef enum {
	ERROR_OK,
	ERROR_BADPARAMETER,
	ERROR_MISSING_LIST_FILE,
	ERROR_INTERNAL,
	ERROR_CANT_CREATE_TMPFILE,
	ERROR_WRITING_TMPFILE,
	ERROR_READING_TMPFILE,
	ERROR_BAD_CLOSE_TMPFILE,
	ERROR_CANT_LOAD_ILBM,
	ERROR_CLOSING_DEFFILE,
	ERROR_OPENING_DEFFILE,
	ERROR_NOMEMORY,
	ERROR_WRITING_OUTPUT,
	ERROR_MORE_THAN_16_COLORS,
	ERROR_WRITING_RMP,

	ERROR_LAST_ERROR
} MainError;

/*
 * Options and settings structure
 */

typedef struct {
	BOOL useDefFile;
	BOOL squeeze;
	BOOL shadow; 						/* Work out shadow offset */
	BOOL useAnchor;	  				/* Work out anchorpoint with anchorColor */
	BOOL nybble;						/* Output should be nybble packed */
	BOOL remap;							/* Nybble screens should be remapped */
	BOOL rle;							/* Image is RLE encoded */
	BOOL givenBackground;			/* Background colour was given by user */
	UBYTE backGroundColor;
	UBYTE shadowColor;
	UBYTE anchorColor;	 			/* Color of pixel to indicate Y position */
	enum { OUT_SWG, OUT_MCC } outputFormat;
	enum { LANG_C, LANG_ASM } language;
	enum { BIT_IMAGE } type;
	Verbose verbosity;
} OPTIONS;

OPTIONS globalOptions;	/* Global options restored for each file */
OPTIONS options;			/* Temporary options changeable for each file */

/*
 * Variables and constants
 */

char sprite_id[] = SPRITE_ID;

static const char whiteSpace[] = " \t\r\n";

FILE *outFD = NULL;						/* File handle for temporary outfile */
char *tmpFileName = "getspr.tmp";	/* Filename used for temporary files */
FILE *defFD = NULL;						/* Def File */
char *defFileName = NULL;				/* Name of DEF file */

enum { READING, WRITING } defFileMode = WRITING;

int tileCount = 0;

typedef struct {
	SpriteHeader header;
	ULONG dataSize;
	REMAP_TABLE remapTable;
} TmpSpriteHeader;

/*-------------------------------
 * Give the user an error message
 */

void error(int line, char *fileName, char *lineBuf, char *fmt, ...)
{
	va_list vaList;

	printf("\n\aError");
	if(line != 0)
		printf(" in line %d", line);
	if(fileName != NULL)
	{
		if(line == 0)
			printf(" in");
		else
			printf(" of");
		printf(" file %s", fileName);
	}
	if(lineBuf != NULL)
		printf("\n: '%s'", lineBuf);
	printf("\n");

	va_start(vaList, fmt);
	vprintf(fmt, vaList);
	va_end(vaList);
	printf("\n");

	/* Close output file if it is open and then delete it */

	if(outFD)
	{
		fclose(outFD);
		outFD = NULL;
	}
	remove(tmpFileName);

}
/*
 * Display a message to the user depending on verbosity level
 */

void report(Verbose level, char *fmt, ...)
{
	va_list vaList;

	if(level <= options.verbosity)
	{
		va_start(vaList, fmt);
		vprintf(fmt, vaList);
		va_end(vaList);
	}
}

/*-------------------------------------------
 * Find what version of the program this is!
 */

char *getVersion(void)
{
	static char rcsRevision[] = "$Revision$";
	char *start;
	char *s;

	/* Find first digit */

	s = rcsRevision;
	while(*s && !isdigit(*s))
		s++;
	start = s;

	/* Find end of version */

	while(*s && (*s != ' ') && (*s != '$'))
		s++;
	*s = 0;

	return start;
}

/*
 * Introduce ourselves
 */

#define bannerWidth 76

void cPrintf(char *fmt, ...)
{
	va_list vaList;
	char buffer[120];
	int count;
	int spaces;

	va_start(vaList, fmt);
	vsprintf(buffer, fmt, vaList);
	va_end(vaList);

	spaces = bannerWidth - strlen(buffer);
	if(spaces < 0)
		spaces = 0;
	printf("|");
	count = spaces / 2;
	spaces -= count;
	while(count--)
		printf(" ");
	printf(buffer);
	while(spaces--)
		printf(" ");
	printf("|\n");
}

void bannerLine(void)
{
	int count = bannerWidth;
	printf("+");
	while(count--)
		printf("-");
	printf("+\n");
}

void banner(void)
{
	printf("\n");
	bannerLine();
	cPrintf("Sprite Extraction Utility version %s", getVersion());
	cPrintf("Compiled on " __DATE__ " at " __TIME__);
	cPrintf("Copyright (c) 1992 Dagger Interactive Technologies Ltd");
	cPrintf("Written by Steven Green");
#if defined(__BCPLUSPLUS__)
	cPrintf("Compiled with Borland C++ version %d.%d",
		(__BCPLUSPLUS__ >> 8) & 0xff, __BCPLUSPLUS__ & 0xff);
#elif defined(__BORLANDC__)
	cPrintf("Compiled with Borland C version %d.%d",
		(__BORLANDC__ >> 8) & 0xff, __BORLANDC__ & 0xff);
#elif defined(__WATCOMC__)
	cPrintf("Compiled with Watcom C version %d.%02d",
		__WATCOMC__  / 100, __WATCOMC__ % 100);
#endif
	bannerLine();
	printf("\n");
}

/*
 * Ask the user to press a key
 */

void waitKey(void)
{
	cPrintf("Press a key to continue");
	if(getch() == 0)	/* Extended key if 0 */
		getch();
}


/*==============================================================
 * Open up the temporary file
 */

MainError openTmpFile(void)
{
	if(outFD == NULL)
	{
		outFD = fopen(tmpFileName, "wb");
		if(outFD == NULL)
		{
			error(0, NULL, NULL, "Can't create temporary file '%s'\n", tmpFileName);
			return ERROR_CANT_CREATE_TMPFILE;
		}
	}
	else
	{
		error(0, NULL, NULL, "tmpFile '%s' is already open!\n", tmpFileName);
		return ERROR_INTERNAL;
	}

	return ERROR_OK;
}

/*
 * Close the temporary file
 */

MainError closeTmpFile(void)
{
	if(outFD != NULL)
	{
		int err = fclose(outFD);
		outFD = NULL;
		if(err != 0)
		{
			error(0, NULL, NULL, "error %d closing temporary file %s\n", err, tmpFileName);
			return ERROR_BAD_CLOSE_TMPFILE;
		}
	}
	else
	{
		error(0, NULL, NULL, "tmpFile %s was not opened!\n", tmpFileName);
		return ERROR_INTERNAL;
	}

	return ERROR_OK;
}

/*
 * DEF file handling
 */

/*
 * Make up a name for def file
 * if chop is set then any extension should be ignored
 * otherwise it has been provided by the user
 */

void makeDefFileName(char *s, BOOL chop)
{
	char buffer[FILENAME_MAX];

	if(defFileName != NULL)
		free(defFileName);

	makeFilename(buffer, s, ".def", chop);

	defFileName = strdup(buffer);
}


MainError closeDefFile(void)
{
	MainError errorNum = ERROR_OK;

	if(defFD != NULL)
	{
		if(fclose(defFD) != 0)
			errorNum = ERROR_CLOSING_DEFFILE;
		defFD = NULL;
	}
	return errorNum;
}

MainError openDefFile(void)
{
	MainError errorNum = ERROR_OK;

	if(options.useDefFile && (defFD == NULL))		/* If not already open */
	{
		/* See if file exists */

		char fileName[FILENAME_MAX];
		struct stat statBuf;

		makeFilename(fileName, defFileName, ".DEF", TRUE);

		if(stat(fileName, &statBuf) == 0)		/* It exists */
		{
			report(V_QUIET, "Reading Def File %s\n", fileName);
			defFileMode = READING;
			defFD = fopen(fileName, "r");
		}
		else
		{
			report(V_QUIET, "Writing Def File %s\n", fileName);
			defFileMode = WRITING;
			defFD = fopen(fileName, "w");
		}
		if(defFD == NULL)
		{
			error(0, fileName, NULL, "Couldn't open Def File\n");
			errorNum = ERROR_OPENING_DEFFILE;
		}
	}

	return errorNum;
}

/*
 * Write the actual data to disk
 * The buffer always has the data moved to the top left occupying
 * width,height
 */

MainError writeData(TmpSpriteHeader *sprite, UBYTE *data)
{
	if(fwrite(data, sprite->dataSize, 1, outFD) != 1)
			return ERROR_WRITING_TMPFILE;
	return ERROR_OK;
}

/*
 * Pixel manipulation functions... defined as macros for speed
 */

#define pixelOffset(x,y) (((unsigned long) (y) * bmhd.w) + (x))
#define getPixel(x,y) ilbm_ptr[pixelOffset((x),(y))]
#define isBackground(x,y) (getPixel((x),(y)) == options.backGroundColor)
#define putPixel(x,y,c) ilbm_ptr[pixelOffset((x),(y))] = c

/*
 * Remove white space from a sprite
 */

void squeeze(TmpSpriteHeader *sprite, UBYTE *data)
{
	uint top, bottom, left, right;

	/* Remove from top */

	uint x,y;
	UBYTE *line = data;
	BOOL found = FALSE;
	y = 0;
	while(!found && (y < sprite->header.height))
	{
		x = 0;
		while(x < sprite->header.width)
		{
			if(*line++ != 0)
			{
				found = TRUE;
				break;
			}
			x++;
		}
		if(!found)
			y++;
	}

	top = y;

	/* Remove from bottom */

	y = sprite->header.height;
	found = FALSE;

	line = data + y * sprite->header.width;
	while(!found && (--y > top))
	{
		x = sprite->header.width;

		while(x--)
		{
			if(*--line != 0)
			{
				found = TRUE;
				break;
			}
		}
	}
	bottom = y;

	/* Remove left */

	found = FALSE;
	x = 0;
	while(!found && (x < sprite->header.width))
	{
		y = top;
		for(y = top; y <= bottom; y++)
		{
			if(data[y * sprite->header.width + x] != 0)
			{
				found = TRUE;
				break;
			}
		}
		if(!found)
			x++;
	}
	left = x;

	/* Remove right */

	x = sprite->header.width;
	found = FALSE;
	while(!found && (--x > left))
	{
		y = top;
		while(y < bottom)
		{
			if(data[y * sprite->header.width + x] != 0)
			{
				found = TRUE;
				break;
			}
			y++;
		}
	}			  
	right = x;

	/* Shift data to top left */

	line = data;		/* Where data must go to */
	y = top;
	while(y <= bottom)
	{
		x = left;
		while(x <= right)
		{
			*line++ = data[y * sprite->header.width + x];
			x++;
		}
		y++;
	}

	/* Update header */

	sprite->header.xOffset = left;
	sprite->header.yOffset = top;
	sprite->header.width = right - left + 1;
	sprite->header.height = bottom - top + 1;
	sprite->header.anchorX -= left;
	sprite->header.anchorY -= top;

	if(options.useAnchor && options.shadow)
		sprite->header.fullHeight -= top;

	sprite->dataSize = sprite->header.width * sprite->header.height;

	report(V_VERBOSE, "Squeezed from 0,0,%d,%d to %d,%d,%d,%d\n",
				sprite->header.fullWidth, sprite->header.fullHeight,
				sprite->header.xOffset, sprite->header.yOffset,
				sprite->header.width, sprite->header.height);
}

/*
 * Calculate baseY based on shadow
 */

void shadow(TmpSpriteHeader *sprite, UBYTE *data)
{
	/* Remove from bottom */

	uint y = sprite->header.height;
	BOOL found = FALSE;

	UBYTE *line = data + y * sprite->header.width;
	while(!found && --y)
	{
		uint x = sprite->header.width;

		while(x--)
		{
			UBYTE val = *--line;
			if( (val != 0) && (val != options.shadowColor) )
			{
				found = TRUE;
				break;
			}
		}
	}
	sprite->header.anchorY = y;

	report(V_VERBOSE, "shadowed bottom anchor is %d\n", sprite->header.anchorY);
}

void anchor(TmpSpriteHeader *sprite, UBYTE *data)
{
	BOOL anchorFound = FALSE;
	uint y = 0;

	while(y < sprite->header.height)
	{
		uint x = 0;

		while(x < sprite->header.width)
		{
			UBYTE val = *data;
			if(val == options.anchorColor)
			{
				*data = 0;		/* Replace pixel with 0 */
				if(y == 0)		/* if on top line then it is the X anchor */
				{
					sprite->header.anchorX = x;
					report(V_VERBOSE, "X Anchor is at %d\n", sprite->header.anchorX);
					anchorFound = TRUE;
				}
				else
				{
					sprite->header.anchorY = y;
					report(V_VERBOSE, "Y Anchor is at %d\n", sprite->header.anchorY);
					return;
				}
			}
			data++;
			x++;
		}
		y++;
	}
	if(!anchorFound)
		report(V_VERBOSE, "Y Anchor pixel was not found\n");
}

/*
 * Nybble pack a sprite
 *
 * the remap table will already have been created when the
 * lbm was loaded
 */

static UBYTE remapColors[256];	/* Space for all the colors */
static int maxRemapColor = 0;

void initRemap(void)
{
	maxRemapColor = 1;
	remapColors[0] = 0;

	report(V_DEBUG, "---------- Colour Remap Table ------------\n");
	report(V_DEBUG, "Color %2d: %3d ($%02x) => r=%3d, g=%3d, b=%3d [Transparent]\n",
		0, 0, 0,
		ilbm_palette[0][0],
		ilbm_palette[0][1],
		ilbm_palette[0][2]);
}

MainError checkRemap(void)
{
	report(V_DEBUG, "------------------------------------------\n");
	if(maxRemapColor > RemapColorCount)
	{
		error(0, NULL, NULL,
			"Sprite has more than %d colours (%d) so can not be nybble packed\n",
			RemapColorCount, maxRemapColor);
		return ERROR_MORE_THAN_16_COLORS;
	}

 	return ERROR_OK;
}

UBYTE findRemap(UBYTE color)
{
	int i;

	/*
	 * Disable background and anchor colours from being
	 * remapped
	 */

	if(color == options.backGroundColor)
		color = 0;
	if(options.useAnchor && (color == options.anchorColor))
		color = 0;
	if(color == 0)
		return 0;

	i = 0;
	while(i < maxRemapColor)
	{
		if(remapColors[i] == color)
			return i;
		i++;
	}

	/*
	 * If it wasn't in the table then add it to the end
	 */


	if(maxRemapColor == RemapColorCount)
		report(V_DEBUG, "----------- Too Many Colours! ------------\n");
	report(V_DEBUG, "Color %2d: %3d ($%02x) => r=%3d, g=%3d, b=%3d",
		maxRemapColor, color, color,
		ilbm_palette[color][0],
		ilbm_palette[color][1],
		ilbm_palette[color][2]);

	remapColors[maxRemapColor++] = color;

	if(options.shadow && (color == options.shadowColor))
		report(V_DEBUG, " [Shadow]");

	report(V_DEBUG, "\n");

	return maxRemapColor - 1;
}

MainError nybblePack(TmpSpriteHeader *sprite, UBYTE *data)
{
	UBYTE *dest = data;
	UBYTE *src = data;
	int lineCount = sprite->header.height;
	int columns = sprite->header.width / 2;
	MainError error = ERROR_OK;

	report(V_VERBOSE, "Nybble Packing\n");

	if(options.remap)
		initRemap();

	sprite->dataSize = lineCount * columns;
	if(sprite->header.width & 1)
		sprite->dataSize += lineCount;

	while(lineCount--)
	{
		int columnCount = columns;

		while(columnCount--)
		{
			UBYTE col1, col2;

			col1 = *src++;
			col2 = *src++;
	
			if(options.remap)
			{
				col1 = findRemap(col1);
				col2 = findRemap(col2);
			}
			else
			{
				col1 &= 0xf;
				col2 &= 0xf;
			}

			*dest++ = ((col1 << 4) & 0xf0) | col2;
		}

		/* Handle remaining nybble */

		if(sprite->header.width & 1)
		{
			UBYTE col = *src++;

			if(options.remap)
				col = findRemap(col);
			else
				col &= 0xf;
			*dest++ = (col << 4);
		}
	}
	if(options.remap)
	{
		error = checkRemap();
		memcpy(sprite->remapTable, remapColors, sizeof(REMAP_TABLE));
	}

	return error;
}

#if 0
/*
 * Create the remap table
 */

MainError makeRemap(char *name)		/* Name to use as base for RMP */
{
	UBYTE *pic = ilbm_ptr;
	ULONG count = (long)bmhd.h * bmhd.w;

	maxRemapColor = 0;
	
	while(count--)
	{
		UBYTE color = *pic++;
		int colCount;
		BOOL found;

		/* See if its already in the table */

		if(color == options.backGroundColor)
			color = 0;
		if(options.useAnchor && (color == options.anchorColor))
			color = 0;

		colCount = 0;
		found = FALSE;
		while(colCount < maxRemapColor)
		{
			if(remapColors[colCount] == color)
			{	
				found = TRUE;
				break;
			}
			colCount++;
		}

		if(!found)	/* Its a new color */
		{
			if(maxRemapColor >= 16)
			{
				error(0, name, NULL, "Picture has more than 16 colours so can not be nybble packed\n");
				return ERROR_MORE_THAN_16_COLORS;
			}			
			remapColors[maxRemapColor++] = color;
			report(V_DEBUG, "Color %d: %d\n", maxRemapColor, color);
		}
	}

	/*
 	 * Write an RMP file
	 */

	{
		char rmpName[FILENAME_MAX];
		FILE *rmpFD;

		makeFilename(rmpName, name, ".RMP", TRUE);

		report(V_QUIET, "Writing Color remap table %s\n", rmpName);

		rmpFD = fopen(rmpName, "w");
		if(rmpFD != NULL)
		{
			int i;
			int xCount;

			if(options.language == LANG_C)
			{
				fprintf(rmpFD, "/* Color remap table for %s */\n", name);
				fprintf(rmpFD, "{");
			}
			else if(options.language == LANG_ASM)
			{
				fprintf(rmpFD, "; Color remap table for %s\n", name);
			}
			i = 0;
			xCount = 0;
			while(i < 16)
			{
				if(xCount == 0)
				{
					if(options.language == LANG_C)
						fprintf(rmpFD, "\n\t");
					else if(options.language == LANG_ASM)
						fprintf(rmpFD, "\n\t\tdb\t");
					xCount = 7;
				}
				else
				{
					fprintf(rmpFD, ", ");
					xCount--;
				}
				if(options.language == LANG_C)
					fprintf(rmpFD, "0x");
				else if(options.language == LANG_ASM)
					fprintf(rmpFD, "0");
				fprintf(rmpFD, "%02x", remapColors[i]);
				if(options.language == LANG_ASM)
					fprintf(rmpFD, "h");
				i++;
			}
			if(options.language == LANG_C)
				fprintf(rmpFD, "\n}\n");
	 
			fclose(rmpFD);
		}
		else
		{
			error(0, rmpName, NULL, "Can't create color remap file\n");
			return ERROR_WRITING_RMP;
		}
	}

	return ERROR_OK;
}
#endif


/*
 * Actually process a tile
 */

MainError takeTile(int x, int y, int w, int h)
{
	MainError errorNum = ERROR_OK;
	UBYTE *tileBuffer;

	tileCount++;
	report(V_VERBOSE, "Tile %3d: %3d,%3d -> %3d,%3d\n", tileCount, x, y, w, h);
	if(options.verbosity < V_VERBOSE)
		report(V_QUIET, ".");

	tileBuffer = malloc(w * h);
	if(tileBuffer != NULL)
	{
		/* Copy it to buffer and rub it off main screen */

		int y1 = y;
		int yCount = h;
		UBYTE *ptr = tileBuffer;

		TmpSpriteHeader header;

		while(yCount--)
		{
			int x1 = x;
			int xCount = w;

			while(xCount--)
			{
				UBYTE pixel = getPixel(x1, y1);
				if(pixel == options.backGroundColor)
					pixel = 0;
				*ptr++ = pixel;
				putPixel(x1, y1, options.backGroundColor);

				x1++;
			}

			y1++;
		}

		/*
		 * Initialise header
		 */

		header.dataSize = w * h;
		header.header.width = w;
		header.header.height = h;
		header.header.fullHeight = h;
		header.header.fullWidth = w;
		header.header.xOffset = 0;
		header.header.yOffset = 0;
		header.header.anchorY = h/2;
		header.header.anchorX = w/2;
		header.header.shadowColor = options.shadowColor;
#if 0
		header.header.transparentColor = options.backGroundColor;
#else
		header.header.transparentColor = 0;
#endif
		header.header.flags.type = options.type;
		header.header.flags.rle = options.rle;
		header.header.flags.nybble = options.nybble;
		header.header.flags.squeeze = options.squeeze;
		header.header.flags.shadow = options.shadow;
		header.header.flags.remap = options.remap;

		/*
 		 * Do whatever processing is needed!
		 */

		if(options.shadow)
			shadow(&header, tileBuffer);
		if(options.useAnchor)
		{
			/*
			 *	Bodge so that if shadow and anchor is enabled
			 * full height is the height to the top of the shadow
			 */

			if(options.shadow)
				header.header.fullHeight = header.header.anchorY;

			anchor(&header, tileBuffer);
		}
		if(options.squeeze)
			squeeze(&header, tileBuffer);

		if(options.nybble)
		{
			errorNum = nybblePack(&header, tileBuffer);
			if(errorNum != ERROR_OK)
				goto processError;
		}


		/*
		 * Add info to def file
		 *
		 * x y w h xoffset yoffset w1 h1 xanchor yanchor
		 */

		if(options.useDefFile && (defFileMode == WRITING) && (defFD != NULL))
			fprintf(defFD, "%-3u %-3u %-3u %-3u %-3u %-3u %-3u %-3u %-3d %-3d\n",
				x, y, w, h,
				header.header.xOffset, header.header.yOffset,
				header.header.width, header.header.height,
				header.header.anchorX, header.header.anchorY);

		/*
		 * Write out the sprite to temporary file
		 * This can be in machine dependant format!
		 */

		if(fwrite(&header, sizeof(header), 1, outFD) != 1)
		{
			error(0, tmpFileName, NULL, "Error writing to temporary file\n");
			errorNum = ERROR_WRITING_TMPFILE;
		}
		else
			errorNum = writeData(&header, tileBuffer);

	processError:
		free(tileBuffer);
	}
	else
	{
		error(0, NULL, NULL, "Couldn't get memory for tile buffer\n");
		errorNum = ERROR_NOMEMORY;
	}

	return errorNum;
}



/*
 * Grab sprites using existing DEF file, which is already open
 */

MainError grabUsingDef(void)
{
	char buffer[120];
	int x,y,w,h;
	BOOL haveRead = FALSE;
	MainError errorNum = ERROR_OK;

	while(fgets(buffer, 120, defFD) != NULL)
	{
		if(isdigit(buffer[0]))
		{
			sscanf(buffer, "%d%d%d%d", &x, &y, &w, &h);

			errorNum = takeTile(x, y, w, h);
			if(errorNum != ERROR_OK)
				return errorNum;

			haveRead = TRUE;
		}
		else
		{
			if(haveRead)
				return errorNum;		/* Stop this file when we reach a file name */
		}
	}
	return errorNum;
}

/*
 * Scan the raw picture for tiles
 */

MainError grabTiles(void)
{
	int y;
	MainError errorNum = ERROR_OK;

	/* Scan from top to bottom for non-background pixel */

	for(y = 0; y < bmhd.h; y++)
	{
		int x;

		for(x = 0; x < bmhd.w; x++)
		{
			if(!isBackground(x, y))
			{
				/*
				 * A non-background pixel has been found
				 * Try to work out a rectangle containing connected pixels
				 */

				int minX, maxX, minY, maxY;
				int y1;

				minX = x;
				maxX = x;
				minY = y;
				maxY = y;

				for(y1 = y + 1; y1 < bmhd.h; y1++)
				{
					int leftX, rightX;
					BOOL finished = FALSE;

					rightX = 0;

					do
					{
						leftX = rightX;
						while((leftX <= maxX) && isBackground(leftX,y1))
							leftX++;

						if(leftX > maxX)
						{
							finished = TRUE;
							break;
						}

						rightX = leftX;
						while((rightX < bmhd.w) && !isBackground(rightX,y1))
							rightX++;
					}
					while(!finished && (rightX < bmhd.w) && (rightX < minX));

					if(finished)
						break;

					if(rightX < minX)		/* Off bottom */
						break;

					// if(rightX >= bmhd.w)
					//	break;

					if(leftX < minX)
						minX = leftX;
					if(rightX > maxX)
						maxX = rightX;
				}			  
				maxY = y1;	

				/*
				 * We have a tile at (minX,minY) -> (maxX,maxY)
				 */

				errorNum = takeTile(minX, minY, maxX-minX, maxY-minY);
				if(errorNum != ERROR_OK)
					return errorNum;
			}
		}
	}
	return errorNum;
}


/*===================================================================
 * Process a file, sending a processed tile to the temporary output file
 */

MainError processFile(char *inName)
{
	MainError errorNum = ERROR_OK;

	/* Make sure temporary file is open */

	if(outFD == NULL)
	{
		errorNum = openTmpFile();
  		if(errorNum != ERROR_OK)
	  		return errorNum;
	}
	  
 	report(V_QUIET, "Processing %s\n", inName);

	if(ilbm_to_raw(inName) == 0)
	{
		if(!options.givenBackground)
		{
			options.backGroundColor = bmhd.transparentColor;
			report(V_VERBOSE, "Taken background color as %d from %s\n",
				options.backGroundColor, inName);
		}

		if(options.useDefFile && (defFD == NULL))
			openDefFile();

#if 0
		if(options.remap)
		{
			errorNum = makeRemap(inName);
			if(errorNum != ERROR_OK)
				return errorNum;
		}
#endif

		if(options.useDefFile && (defFileMode == READING) && (defFD != NULL))
			errorNum = grabUsingDef();
		else
		{
			if(options.useDefFile && (defFileMode == WRITING) && (defFD != NULL))
				fprintf(defFD, "<%s\n", inName);
			errorNum = grabTiles();
		}
		if(options.verbosity < V_VERBOSE)
			report(V_QUIET, "\n");
	}
	else
	{
		error(0, inName, NULL, "Could not load ILBM file\n");
		errorNum = ERROR_CANT_LOAD_ILBM;
	}

	return errorNum;
}

/*
 * Copy data from one file to another
 */

MainError copyData(FILE *dest, FILE *src, size_t howMuch)
{
	while(howMuch--)
	{
		int c = fgetc(src);
		fputc(c, dest);
	}
	if(ferror(dest) || ferror(src))
		return ERROR_WRITING_OUTPUT;

	return ERROR_OK;
}

/*
 * Convert temporary file to SWG format
 */

MainError outputSWG(char *name)
{
	MainError errorNum = ERROR_OK;
	int err;
	FILE *tmpFD;
	FILE *outFD;
	char outName[FILENAME_MAX];

	makeFilename(outName, name, ".SPR", TRUE);

	report(V_QUIET, "Writing final file to %s\n", outName);

	/* Open output file */
	
	outFD = fopen(outName, "wb");
	if(outFD != NULL)
	{
		fpos_t offsetPos;
		ILONG ilong;
		int count;

		/* Create the file header */

		SpriteFileHeaderD fileHead;

		memcpy(fileHead.id, sprite_id, sizeof(fileHead.id));
		putWord(&fileHead.headerSize, sizeof(fileHead));
		putByte(&fileHead.versionOrder, IsIntel | SPRITE_VERSION);
		putWord(&fileHead.spriteCount, tileCount);
		if(fwrite(&fileHead, sizeof(fileHead), 1, outFD) != 1)
		{
		outErr1:
			error(0, outName, NULL, "Error writing file header\n");
			goto outErr;
		}

		/* Write a dummy offset table */

		fgetpos(outFD, &offsetPos);		/* Position of 1st offset */
		count = tileCount;
		putLong(&ilong, 0);
		while(count--)
		{
			if(fwrite(&ilong, sizeof(ilong), 1, outFD) != 1)
				goto outErr1;
		}

		/* Open temporary file */

		tmpFD = fopen(tmpFileName, "rb");
		if(tmpFD != NULL)
		{
			int count = tileCount;
			while(count--)
			{
				TmpSpriteHeader head;
				SpriteHeaderD dHead;
				fpos_t pos;
				UWORD dataSize;

				report(V_QUIET, ".");	/* Show progress */

				/* Update the offset table */

				fgetpos(outFD, &pos);
				fsetpos(outFD, &offsetPos);
				putLong(&ilong, pos);
				if(fwrite(&ilong, sizeof(ilong), 1, outFD) != 1)
				{
					error(0, outName, NULL, "Error writing output file\n");
					errorNum = ERROR_WRITING_OUTPUT;
					break;
				}
				offsetPos += sizeof(ilong);
				fsetpos(outFD, &pos);

				/* Read header */

				if(fread(&head, sizeof(head), 1, tmpFD) != 1)
				{
				 	error(0, tmpFileName, NULL, "Error reading temporary file\n");
				 	errorNum = ERROR_READING_TMPFILE;
				 	break;
				}

				/* make new header */
	
				dataSize = head.dataSize;
				if(head.header.flags.remap)
					dataSize += sizeof(REMAP_TABLE);

				putByte(&dHead.flags,
								((head.header.flags.type    & 7) << TYPE_B)    | 
								((head.header.flags.rle     & 1) << RLE_B)     |
								((head.header.flags.squeeze & 1) << SQUEEZE_B) |	
								((head.header.flags.shadow  & 1) << SHADOW_B)  |	
								((head.header.flags.nybble  & 1) << NYBBLE_B)  |
								((head.header.flags.remap   & 1) << REMAP_B) );
				putByte(&dHead.headerSize,       sizeof(dHead));
				putWord(&dHead.dataSize,         dataSize);
				putWord(&dHead.width,            head.header.width);
				putWord(&dHead.height,           head.header.height);
				putWord(&dHead.fullWidth,        head.header.fullWidth);
				putWord(&dHead.fullHeight,       head.header.fullHeight);
				putWord(&dHead.xOffset,          head.header.xOffset);
				putWord(&dHead.yOffset,          head.header.yOffset);
				putWord(&dHead.anchorX,          head.header.anchorX);
				putWord(&dHead.anchorY,          head.header.anchorY);
				putByte(&dHead.shadowColor,      head.header.shadowColor);
				putByte(&dHead.transparentColor, head.header.transparentColor);
				
				if(fwrite(&dHead, sizeof(dHead), 1, outFD) != 1)
				{
				 	error(0, outName, NULL, "Error writing output file\n");
				 	errorNum = ERROR_WRITING_OUTPUT;
				 	break;
				}

				/* Copy remap table if present */

				if(head.header.flags.remap)
					if(fwrite(head.remapTable, sizeof(REMAP_TABLE), 1, outFD) != 1)
					{
						error(0, outName, NULL, "Error writing remap table\n");
			 			errorNum = ERROR_WRITING_OUTPUT;
			 			break;
					}

				/* Copy the data */

				if(copyData(outFD, tmpFD, head.dataSize) != ERROR_OK)
		 		{
		 			error(0, outName, NULL, "Error copying data\n");
		 			errorNum = ERROR_WRITING_OUTPUT;
		 			break;
		 		}
		 	}
			fclose(tmpFD);
		}
		else
		{
			error(0, tmpFileName, NULL, "Couldn't reopen temp file for reading\n");
			errorNum = ERROR_READING_TMPFILE;
		}
	outErr:
		err = fclose(outFD);
		if(err && (errorNum == ERROR_OK))
		{
			error(0, outName, NULL, "Writing output file\n");
			errorNum = ERROR_WRITING_OUTPUT;
		}
	}
	else
	{
		error(0, outName, NULL, "Can't create output file\n");
		errorNum = ERROR_WRITING_OUTPUT;
	} 	

	report(V_QUIET, "\n");	/* Show progress */
	return errorNum;		
}


/*
 * Convert temporary file to MCC format
 */

MainError outputMCC(char *name)
{
	MainError errorNum = ERROR_OK;
	int err;
	char mccName[FILENAME_MAX];
	char offName[FILENAME_MAX];
	char *ext = NULL;
	FILE *tmpFD;
	FILE *mccFD;
	FILE *offFD;

	/* work out extension based on options */

	if(options.shadow)
		ext = ".SQS";
	else if(options.squeeze)
		ext = ".SQZ";
	else
		ext = ".MCC";

	makeFilename(mccName, name, ext, TRUE);
	makeFilename(offName, name, ".OFF", TRUE);

	report(V_QUIET, "Writing final files to %s and %s\n", mccName, offName);

	mccFD = fopen(mccName, "wb");
	if(mccFD != NULL)
	{
		offFD = fopen(offName, "wb");
		if(offFD != NULL)
		{
			tmpFD = fopen(tmpFileName, "rb");
			if(tmpFD != NULL)
			{
				int count = tileCount;
				while(count--)
				{
					TmpSpriteHeader header;
					fpos_t pos;
					ILONG ilong;
					struct {
						UBYTE width_h;
						UBYTE width_l;
						UBYTE height_h;
						UBYTE height_l;
						UBYTE xOffset_l;
						UBYTE xOffset_h;
						UBYTE yOffset_l;
						UBYTE yOffset_h;
						UBYTE shadowOffset;
						UBYTE shadowColor;
						UBYTE spare1;
						UBYTE spare2;
					} mccHead;

					if(fread(&header, sizeof(header), 1, tmpFD) != 1)
					{
						error(0, tmpFileName, NULL, "Error reading temporary file\n");
						errorNum = ERROR_READING_TMPFILE;
						break;
					}

					/* Write the position to the OFF file */

					fgetpos(mccFD, &pos);
					putLong(&ilong, pos);
					if(fwrite(&ilong, sizeof(ilong), 1, offFD) != 1)
					{
						error(0, offName, NULL, "Error writing offset file\n");
						errorNum = ERROR_WRITING_OUTPUT;
						break;
					}

					/* Write the sprite header */

					mccHead.width_l = header.header.width & 0xff;
					mccHead.width_h = (header.header.width >> 8) & 0xff;
					mccHead.height_l = header.header.height & 0xff;
					mccHead.height_h = (header.header.height >> 8) & 0xff;
					mccHead.xOffset_l = header.header.xOffset & 0xff;
					mccHead.xOffset_h = (header.header.xOffset >> 8) & 0xff;
					mccHead.yOffset_l = header.header.yOffset & 0xff;
					mccHead.yOffset_h = (header.header.yOffset >> 8) & 0xff;
					mccHead.shadowColor = header.header.shadowColor;
					mccHead.shadowOffset = header.header.anchorY;
					mccHead.spare1 = 0;
					mccHead.spare2 = 0;

					if(fwrite(&mccHead, sizeof(mccHead), 1, mccFD) != 1)
					{
						error(0, mccName, NULL, "Error writing output file\n");
						errorNum = ERROR_WRITING_OUTPUT;
						break;
					}
					
					/* Copy data */

					if(copyData(mccFD, tmpFD, header.dataSize) != ERROR_OK)
					{
						error(0, mccName, NULL, "Error copying data\n");
						errorNum = ERROR_WRITING_OUTPUT;
						break;
					}
				}

				fclose(tmpFD);
			}
			else
			{
				error(0, tmpFileName, NULL, "Couldn't reopen temp file for reading\n");
				errorNum = ERROR_READING_TMPFILE;
			}

			err = fclose(offFD);
			if(err && (errorNum == ERROR_OK))
			{
				error(0, mccName, NULL, "Error Writing output file\n");
				errorNum = ERROR_WRITING_OUTPUT;
			}										
		}
		else
		{
			error(0, offName, NULL, "Can't create output file\n");
			errorNum = ERROR_WRITING_OUTPUT;
		}

		err = fclose(mccFD);
		if(err && (errorNum == ERROR_OK))
		{
			error(0, mccName, NULL, "Writing output file\n");
			errorNum = ERROR_WRITING_OUTPUT;
		}
	}
	else
	{
		error(0, mccName, NULL, "Can't create output file\n");
		errorNum = ERROR_WRITING_OUTPUT;
	} 	
	return errorNum;		
}


/*
 * Convert temporary file to final file
 */

MainError makeFinalFile(char *name)
{
	MainError errorNum = ERROR_OK;

	report(V_QUIET, "%d Sprites grabbed\n", tileCount);

	/* Close the temporary file if it is open */

	if(outFD != NULL)
	{
		errorNum = closeTmpFile();
		if(errorNum != ERROR_OK)
			return errorNum;
	}

	switch(options.outputFormat)
	{
	case OUT_SWG:
		errorNum = outputSWG(name);
		break;
	case OUT_MCC:
		errorNum = outputMCC(name);
		break;
	}

	/* Get rid of the temporary file */

	remove(tmpFileName);

	return errorNum;
}



/*
 * Tell the user how silly s/he is and tell them how it should
 * be used
 */

void usage(void)
{
	printf(
			"\nUsage:\n"
			"\tGETSPR [@listfile] [[[source] destination] defFile] [options]\n\n"
			"Options are:\n"
			"\t-h\n"
			"\t-? : Help (displays this!)\n"
			"\t-d[filename] : Use Def file for coordinates\n"
			"\t-b<n> : Use colour n as background colour [default taken from picture]\n"
			"\t-o<format> : Define output format, format is:\n"
			"\t\tM : MCC compatible format with .OFF file\n"
			"\t\tS : SWG format [DEFAULT]\n"
			"\t-v<level> : Define verbosity of text output from:\n"
			"\t\tS : Silent\n"
			"\t\tQ : Quiet [Default]\n"
			"\t\tV : Verbose\n"
			"\t\tD : Debug\n"
			"\t-s : Remove white space and store animation offsets\n"
			"\t-sh<n> : Calculate base of sprite using n as a shadow color\n"
			"\t-a<n> : Define Y anchor point using a pixel of color n\n"
			"\t-n : Nybble packing (16 colours per sprite) with no remapping\n"
			"\t-nM : Nybble pack with remapped colours\n"
			"\t\l<language> : Select language for output files:\n"
			"\t\tC : the C programming language [default]\n"
			"\t\tS : Assembler\n"
			"\t\tH : Hacker (raw data)\n"
			"\n"
	);
}

/*------------------------------------
 * Process an argument
 *
 * Return 0 if it was a valid argument
 */

enum { ARG_PROCESSED, ARG_UNKNOWN, ARG_ERROR } processArg(char *s)
{
	char c = *s++;
	int retVal = ARG_UNKNOWN;
	
	if((c == '-') || (c == '/'))
	{
		retVal = ARG_PROCESSED;

		c = *s++;

		switch(toupper(c))
		{
		case 'D':		/* use Def File */
			closeDefFile();			/* Make sure any existing def file is closed */
			options.useDefFile = TRUE;

			if(*s)
				makeDefFileName(s, FALSE);
			else
				if(defFileName != NULL)
				{
					free(defFileName);
					defFileName = NULL;
				}

			break;
		case 'B':		/* Background color */
			if(isdigit(*s))
			{
				options.backGroundColor = atoi(s);
				options.givenBackground = TRUE;
				report(V_VERBOSE, "Background Color is %d\n", options.backGroundColor);
			}
			else
			{
				error(0,NULL,NULL, "/B must be followed by a number, e.g. /b3\n");
				return ARG_ERROR;
			}
			break;

		case 'A':		/* Anchor point */
			if(isdigit(*s))
			{
				options.anchorColor = atoi(s);
				options.useAnchor = TRUE;
				report(V_VERBOSE, "Anchor color is %d\n", options.anchorColor);
			}
			else
			{
				error(0, NULL, NULL, "/A must be followed by a color\n");
				return ARG_ERROR;
			}
			break;

		case 'O':		/* Output format */
			c = *s++;
			switch(toupper(c))
			{
			case 'S':
				options.outputFormat = OUT_SWG;
				report(V_VERBOSE, "SWG sprite format enabled\n");
				break;
			case 'M':
				options.outputFormat = OUT_MCC;
				report(V_VERBOSE, "MCC tile format enabled\n");
				break;
			default:
				error(0,NULL,NULL, "/O must be followed by a format type\n");
				return ARG_ERROR;
			}
			break;

		case 'V':
			c = *s++;
			switch(toupper(c))
			{
			case 'S':
				options.verbosity = V_SILENT;
				break;
			case 'Q':
				options.verbosity = V_QUIET;
				break;
			case 'V':
				options.verbosity = V_VERBOSE;
				report(V_VERBOSE, "Verbose mode\n");
				break;
			case 'D':
				options.verbosity = V_DEBUG;
				report(V_DEBUG, "Debug mode\n");
				break;
			default:
				error(0,NULL,NULL,
						"/V must be followed by a verbosity level of\n"
						"Silent, Quiet, Verbose or Debug\n");
				return ARG_ERROR;

			}
		 	break;

		case 'S': 	/* Squeeze... remove white space from edges */
			c = *s++;
			switch(toupper(c))
			{
			case 0:
				options.squeeze = TRUE;
				report(V_VERBOSE, "Squeeze option enabled\n");
				break;
			case 'H':	/* shadow */
				if(isdigit(*s))
				{
					options.shadow = TRUE;
					options.shadowColor = atoi(s);
					report(V_VERBOSE, "Shadow Color is %d\n", options.shadowColor);
				}
				else
				{
					error(0, NULL, NULL, "-SH must be followed by the shadow color\n");
					return ARG_ERROR;
				}
			 	break;
			default:
				retVal = ARG_UNKNOWN;
			}
			break;
		case 'N':			/* Nybble pack */
			options.nybble = TRUE;
			report(V_VERBOSE, "Nybble packing enabled\n");
			c = *s++;
			switch(toupper(c))
			{
			case 0:
				break;
			case 'M':
				options.remap = TRUE;
				report(V_VERBOSE, "Colour remapping enabled\n");
				break;
			default:
				error(0, NULL, NULL, "Unknown option following -N (nybble pack)\n");
				return ARG_ERROR;
			}
			break;

		case 'L':			/* Programming language */
			c = *s++;
			switch(toupper(c))
			{
			case 'C':
				options.language = LANG_C;
				report(V_VERBOSE, "Output language is 'C'\n");
				break;
			case 'S':
				options.language = LANG_ASM;
				report(V_VERBOSE, "Output language is 'ASM'\n");
				break;
			default:
				error(0, NULL, NULL, "-L language option must be followed by C or S\n");
				return ARG_ERROR;
			}
			break;

		default:
			retVal = ARG_UNKNOWN;
			break;
		}
	}

	return retVal;
}

MainError processListFile(char *fileName)
{
	char fName[FILENAME_MAX];
	FILE *fd;
	MainError errorNum = ERROR_OK;

	makeFilename(fName, fileName, ".LST", FALSE);
 	report(V_QUIET, "Processing list file %s\n", fName);
	
	fd = fopen(fName, "r");		/* ascii file */
	if(fd != NULL)
	{
		char buffer[120];
		char copyBuffer[120];

		int lineCount = 1;	/* Line counter used for reporting errors */

		while(fgets(buffer, 119, fd) != NULL)
		{
			char *s;

#ifdef DEBUG
			report(V_DEBUG, "%3d: %s", lineCount, buffer);
#endif
			strcpy(copyBuffer, buffer);
			strtok(copyBuffer, "\n\r");	/* Remove CR/LF */

			s = buffer;
			while(*s && isspace(*s))
				s++;

			if( (*s != '\0') && (*s != ';') && (*s != '#') )	/* Comment/blank line */
			{
				char *picName = NULL;
				options = globalOptions;

				s = strtok(s, whiteSpace);
				while(s)
				{
#ifdef DEBUG
					report(V_DEBUG, "Token: %s\n", s);
#endif
					if((*s == '-') || (*s == '/'))
					{
						int err = processArg(s);
						if(err == ARG_UNKNOWN)
						{
							error(lineCount, fName, copyBuffer,
								"Unknown option '%s'\n", s);
							return ERROR_BADPARAMETER;
						}
						else if(err == ARG_ERROR)
						{
							error(lineCount, fName, copyBuffer,
								"Error in opt '%s' of line", s);
							return ERROR_BADPARAMETER;
						}
					}
					else	/* Assume its a filename */
					{
						if(picName == NULL)
							picName = s;
						else
						{
							error(lineCount, fName, copyBuffer,
								"You have put more than one filename on the line\n"
								"'%s' and '%s'\n", picName, s);
							return ERROR_BADPARAMETER;
						}
					}
					s = strtok(NULL, whiteSpace);
				}

				if(picName)
				{
					/*
					 * Make a def file name if a new one was requested
					 * and no name was given
					 */

					if(options.useDefFile && (defFileName == NULL))
						makeDefFileName(picName, TRUE);

					errorNum = processFile(picName);
					if(errorNum != ERROR_OK)
						return errorNum;
				}
			}

			lineCount++;
		}
		fclose(fd);
	}
	else
	{
		error(0, fName, NULL, "Couldn't open list file\n");
		errorNum = ERROR_MISSING_LIST_FILE;
	}

	return errorNum;
}

/*-------------------------------------------------------------------
 * main function
 *
 * Returns:
 *  0 = OK
 *  1 = Bad parameters
 */

int main(int argc, char *argv[])
{
	int i;
	char *listFile = NULL;		/* Name of list file being processed */
	char *inFile = NULL;
	char *outFile = NULL;
	MainError errorNum = ERROR_OK;

	banner();

	if(argc < 2)
	{
		usage();
		return ERROR_BADPARAMETER;
	}

	/*
 	 * Initialise options
	 */

	options.useDefFile = FALSE;
	options.squeeze = FALSE;
	options.shadow = FALSE;
	options.nybble = FALSE;
	options.remap = FALSE;
	options.rle = FALSE;
	options.type = BIT_IMAGE;
	options.backGroundColor = 0;
	options.givenBackground = FALSE;
	options.shadowColor = 0;
	options.outputFormat = OUT_SWG;
	options.language = LANG_C;
	options.verbosity = V_QUIET;

	/*
	 * Process command line
	 */

	i = 0;
	while(++i < argc)
	{
		char *currentArg = argv[i];
		char *arg = currentArg;

		if((*arg == '-') || (*arg == '/'))	/* Option */
		{	
			int err = processArg(arg);
			if(err == ARG_ERROR)
			{
				error(0, NULL, NULL, "Bad Command Line arguments\n");
				return ERROR_BADPARAMETER;
			}
			else if(err == ARG_UNKNOWN)	/* Not a general option */
			{
				char c;			
				arg++;
				c = *arg++;

				switch(toupper(c))
				{
				default:
					error(0, NULL, NULL, "Unknown option '%s'\n", currentArg);
					waitKey();
				case 'H':
				case '?':
					usage();
					return ERROR_BADPARAMETER;
				}
			}
		}
		else if(*arg == '@')	/* List file */
		{
			arg++;

			if(listFile == NULL)
				listFile = arg;
			else
			{
				error(0, NULL, NULL,
					"You have tried to define more than one listFile\n%s and %s\n",
					listFile, arg);
				return ERROR_BADPARAMETER;
			}
		}
		else	/* Must be a filename */
		{
			if(inFile == NULL)
				inFile = arg;
			else if(outFile == NULL)
				outFile = arg;
			else if(defFileName == NULL)
				makeDefFileName(arg, FALSE);
			else
			{
				error(0, NULL, NULL, "There are two many filenames on the command line '%s'\n", arg);
				return ERROR_BADPARAMETER;
			}
		}
	}

	globalOptions = options;

	if(listFile)
	{
		if(outFile == NULL)				/* Set the output file name */
			outFile = listFile;

		if(defFileName == NULL)
			makeDefFileName(listFile, TRUE);

		errorNum =  processListFile(listFile);
		if(errorNum == ERROR_OK)
			errorNum = makeFinalFile(outFile);
		closeDefFile();
	} 
	else		/* Process the filename given on the command line */
	{
		if(inFile == NULL)
		{
			error(0, NULL, NULL, "You did not specify any filenames on the command line\n");
			return ERROR_BADPARAMETER;
		}
		if(outFile == NULL)
			outFile = inFile;
		if(defFileName == NULL)
			makeDefFileName(inFile, TRUE);

		errorNum = processFile(inFile);
		if(errorNum != ERROR_OK)
			return errorNum;
		closeDefFile();

		options = globalOptions;
 
		makeFinalFile(outFile);
		if(errorNum != ERROR_OK)
			return errorNum;
	}

	return errorNum;
}
