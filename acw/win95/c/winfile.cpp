/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Windows File Reader/Writer classes Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1996/06/22 19:06:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/11/22 10:43:46  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "globals.hpp"
#include "winfile.hpp"

WinFileRWBase::WinFileRWBase()
{
	error = FALSE;
#ifdef DEBUG
	fName = NULL;
#endif
	h =INVALID_HANDLE_VALUE;
}

WinFileRWBase::~WinFileRWBase()
{
	if(h != INVALID_HANDLE_VALUE)
		CloseHandle(h);
}

Boolean WinFileRWBase::getError()
{
	return error;
}

Boolean WinFileRWBase::seekTo(SeekPos where)
{
	if(error)
		return FALSE;

	if(SetFilePointer(h, where, NULL, FILE_BEGIN) == INVALID_FILE_SIZE)
	{
		error = TRUE;
#ifdef DEBUG
		debugMessage("Error seeking %s to %ld", fName, where);
#endif
		throw FileError();
		// return FALSE;
	}
	return TRUE;
}

SeekPos WinFileRWBase::getPos()
{
	if(error)
		return -1;

	DWORD value = SetFilePointer(h, 0, NULL, FILE_CURRENT);

	if(value == INVALID_FILE_SIZE)
	{
		error = TRUE;
#ifdef DEBUG
		debugMessage("Error in getPos() %s", fName);
#endif
		throw FileError();
		// return -1;
	}
	else
		return value;
}

/*
 * Helper for file reading
 */

WinFileReader::WinFileReader(LPCSTR name)
{
	ASSERT(name != 0);

#ifdef DEBUG
	fName = name;

	debugLog("WinFileReader::Opening '%s'\n", name);
#endif

	ASSERT(fileExists(name));

	h = CreateFile(name,
		GENERIC_READ, 
		FILE_SHARE_READ, 
		(LPSECURITY_ATTRIBUTES) NULL, 
		OPEN_EXISTING, 
		// FILE_ATTRIBUTE_READONLY | FILE_FLAG_SEQUENTIAL_SCAN, 
		FILE_ATTRIBUTE_NORMAL,
		(HANDLE) NULL); 

	ASSERT(h != INVALID_HANDLE_VALUE);

	if(h == INVALID_HANDLE_VALUE)
	{
		error = TRUE;
#ifdef DEBUG
		debugMessage("Can't open %s", fName);
#endif
		throw FileError();
	}
}

WinFileReader::~WinFileReader()
{
}

Boolean WinFileReader::read(LPVOID ad, DataLength length)
{
	if(error)
		return FALSE;

	DWORD bytesRead;
	if(!ReadFile(h, ad, length, &bytesRead, NULL) ||
		(bytesRead != length))
	{
		error = TRUE;
#ifdef DEBUG
		debugMessage("Error reading %s", fName);
#endif
		throw FileError();
		// return FALSE;
	}
	return TRUE;
}

Boolean WinFileReader::read(LPVOID ad, DataLength length, DataLength& bytesRead)
{
	if(error)
		return FALSE;

	DWORD amountRead;
	if(!ReadFile(h, ad, length, &amountRead, NULL) ||
		(amountRead <= 0))
	{
		error = TRUE;
#ifdef DEBUG
		debugMessage("Error reading %s", fName);
#endif
		throw FileError();
		// return FALSE;
	}

	bytesRead = amountRead;
	return TRUE;
}


/*
 * Helper for file writing
 */

WinFileWriter::WinFileWriter(LPCSTR name)
{

#ifdef DEBUG
	fName = name;
#endif

	h = CreateFile(name,
		GENERIC_WRITE, 
		0, 
		NULL, 
		CREATE_ALWAYS, 
		FILE_ATTRIBUTE_NORMAL, 
		(HANDLE) NULL); 

	if(h == INVALID_HANDLE_VALUE)
	{
		error = TRUE;
#ifdef DEBUG
		debugMessage("Can't open %s", fName);
#endif
		throw FileError();
	}
}

WinFileWriter::~WinFileWriter()
{
}

Boolean WinFileWriter::write(const void* ad, DataLength length)
{
	DWORD bytesWritten;
	if(!WriteFile(h, ad, length, &bytesWritten, NULL) ||
		(bytesWritten == 0))
	{
		error = TRUE;
#ifdef DEBUG
		debugMessage("Error Writing %s", fName);
#endif
		throw FileError();
		// return FALSE;
	}
	return TRUE;
}

/*
 * Ascii Reader
 */

const int WinFileAsciiReader::bufLength = 500;
const int WinFileAsciiReader::lineBufLength = 200;

WinFileAsciiReader::WinFileAsciiReader(LPCSTR name) : 
	WinFileReader(name) 
{ 
	buffer = new char[bufLength];
	ASSERT(buffer != 0);
	lineBuf = new char[lineBufLength];
	ASSERT(lineBuf != 0);
	bufIndex = 0;
	bufRead = 0;
}

Boolean WinFileAsciiReader::seekTo(SeekPos where)
{
	bufIndex = 0;
	bufRead = 0;
	return WinFileReader::seekTo(where);
}

char* WinFileAsciiReader::readLine()
{
	int lineLength = 0;
	char prevC = 0;

	for(;;)
	{
		if(bufIndex >= bufRead)
		{
			DataLength bytesRead;

			if(!read(buffer, bufLength, bytesRead))
				return 0;

			if(bytesRead <= 0)
				return 0;

			bufRead = bytesRead;
			bufIndex = 0;
		}

		char c= buffer[bufIndex++];

		switch(c)
		{
		case '\r':
			while( (lineLength > 0) && (lineBuf[lineLength-1] == ' ') )
				lineLength--;

			if(lineLength)
			{
				lineBuf[lineLength] = 0;
				return lineBuf;
			}
			break;

		case '\n':
			break;

		case '\t':
			c = ' ';
			// Drop through
		case ' ':
			if(prevC != ' ')
				goto addChar;
			break;

		default:
		addChar:
			if(lineLength >= lineBufLength)
				return 0;
			else
				lineBuf[lineLength++] = c;
			break;
		}

		prevC = c;
	}
}

/*
 * Check if a folder exists
 *
 * Return TRUE if it exists, FALSE if not.
 */

BOOL dirExists(LPCSTR name)
{
	ASSERT(name != 0);

	DWORD attr = GetFileAttributes(name);

	if(attr == 0xffffffff)
		return FALSE;
	if(!(attr & FILE_ATTRIBUTE_DIRECTORY))
		return FALSE;

	return TRUE;

}

/*
 * Check if a file exists
 * Return TRUEif it exists, FALSE if not.
 */

BOOL fileExists(LPCSTR name)
{
	ASSERT(name != 0);
#if 0
	OFSTRUCT openBuf;

	HFILE h = OpenFile(name, &openBuf, OF_EXIST);

	return (h != HFILE_ERROR);
#endif

	DWORD attr = GetFileAttributes(name);

	if(attr == 0xffffffff)
		return FALSE;
	if(attr & FILE_ATTRIBUTE_DIRECTORY)
		return FALSE;

	return TRUE;
}

