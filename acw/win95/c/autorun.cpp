/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Autorun program for Civil War Master's edition
 *
 * Starts up from Windows 95, Autorun feature
 *
 * Displays windows containing a picture and buttons:
 *		Install : Runs install program, sets up program group, registry
 *		Play : Runs the game if already installed
 *		Quit : Exit autorun program
 *		View Readme
 *		View Update
 *
 * Uses code developed for wargamer to handle the creation of windows
 * and things.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "globals.hpp"
#include "autorun.hpp"
#include "mainwind.hpp"

#if defined(DEBUG)
#define DEBUG_MESSAGES
#endif

#ifdef DEBUG_MESSAGES
#include "msgenum.hpp"
#include "clog.hpp"
LogFile mLog("messages.log");
#endif

/*
 * Global Application Data
 */

AutorunApp gApp;

/*
 * Implementation of class to represent autorun application
 */

AutorunApp::AutorunApp()
{
	active = FALSE;
	hDibDC = NULL;
	hPalette = NULL;
	mainWindow = 0;
}

AutorunApp::~AutorunApp()
{
	if(active)
		endApplication();
}

BOOL AutorunApp::initApplication()
{
	if(!MainWindow::registerClass())
		return FALSE;
	return TRUE;
}

BOOL AutorunApp::initInstance()
{
	InitCommonControls();
	hDibDC = CreateCompatibleDC(NULL);
	mainWindow = new MainWindow(nCmdShow);
	active = TRUE;
	return TRUE;
}

void AutorunApp::endApplication()
{
	active = FALSE;
	
	if(hDibDC)
	{
		DeleteDC(hDibDC);
		hDibDC = 0;
	}
}

int AutorunApp::messageLoop()
{
	MSG msg;

	while(GetMessage(&msg, NULL, 0, 0))
	{
#ifdef DEBUG_MESSAGES
		mLog.printf("%s %lu %ld,%ld",
			getWMdescription(msg.hwnd, msg.message, msg.wParam, msg.lParam),
			msg.time,
			msg.pt.x, msg.pt.y);
#endif

#if 0		// Include this we use dialog boxes
		if(activeDialogue && IsDialogMessage(activeDialogue, &msg))
			continue;

		if(activePropsheet)
		{
			if(PropSheet_IsDialogMessage(activePropsheet, &msg))
				continue;

			if(PropSheet_GetCurrentPageHwnd(activePropsheet) == NULL)
			{
				DestroyWindow(activePropsheet);
				clearPropsheet(activePropsheet);
			}
		}	
#endif
		
		TranslateMessage(&msg);
		DispatchMessage(&msg);

	}

	return msg.wParam;
}

BOOL AutorunApp::doCmdLine(LPCSTR cmd)
{
	return TRUE;
}

/*
 * Main Windows Function
 */

int PASCAL WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	int retVal = 0;

	try {
		if(!gApp.init(hInstance, hPrevInstance, lpszCmdLine, nCmdShow))
			return FALSE;

		retVal = gApp.messageLoop();
	}
	catch(GeneralError e)
	{
		MessageBox(0, "General Error Exception", e.get(), MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
	}
	catch(QuitProgram)
	{
		MessageBox(0, "Exception", "Quit Program", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
	}
	catch(...)
	{
		MessageBox(0, "Exception", "Unknown", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
	}

	gApp.endApplication();

	return retVal;
}




/*------------------------------------------------
 * DC Settings save and restore
 */

DCsave::DCsave()
{
	set = FALSE;
}

DCsave::~DCsave()
{
	ASSERT(set == FALSE);

	if(set)
		restore(hdc);
}

void DCsave::save(HDC _hdc)
{
	ASSERT(set == FALSE);

	hdc = _hdc;

	oldBM = (HBITMAP) GetCurrentObject(hdc, OBJ_BITMAP);
	oldBkMode = GetBkMode(hdc);
	oldTextCol = GetTextColor(hdc);
	oldTextAlign = GetTextAlign(hdc);
	oldFont = (HFONT) GetCurrentObject(hdc, OBJ_FONT);

	set = TRUE;
}

void DCsave::restore(HDC _hdc)
{
	ASSERT(set == TRUE);
	ASSERT(hdc == _hdc);

	if(set)
	{
		SelectObject(hdc, oldFont);
		SetTextColor(hdc, oldTextAlign);
 		SetTextColor(hdc, oldTextCol);
		SetBkMode(hdc, oldBkMode);
		SelectObject(hdc, oldBM);

		set = FALSE;
	}
}



void AutorunApp::setPalette(HPALETTE hp) 
{ 
	ASSERT(hPalette == NULL);
	hPalette = hp; 

	if(mainWindow)
	{
		HWND hwnd = mainWindow->getHWND();
		SetActiveWindow(hwnd);
		HDC hdc = GetDC(hwnd);
		HPALETTE oldP = SelectPalette(hdc, hp, FALSE);
		RealizePalette(hdc);


		SelectPalette(hdc, oldP, TRUE);
		RealizePalette(hdc);

		ReleaseDC(hwnd, hdc);
	}
}

