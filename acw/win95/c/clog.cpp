/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Combat Log
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1996/02/08 17:33:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1996/01/19 11:30:05  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG

#include "globals.hpp"
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <time.h>
#include "clog.hpp"

struct _Log {
	HANDLE log;
	const char* name;
	Boolean created;
};


LogFile::LogFile(const char* s)
{
	logFile = new _Log;
	ASSERT(logFile != 0);

	logFile->name = s;
	logFile->log = NULL;
	logFile->created = False;
}

LogFile::~LogFile()
{
	if(logFile->log != NULL)
	{
		CloseHandle(logFile->log);
		delete logFile;
	}
}

/*
 * Helper Function
 */

inline wfprintf(HANDLE fp, LPSTR fmt, ...)
{
	char buffer[500];

	va_list vaList;
	va_start(vaList, fmt);
	_vbprintf(buffer, 500, fmt, vaList);
	va_end(vaList);

	DWORD bw;

	WriteFile(fp, buffer, strlen(buffer), &bw, NULL);
}

void LogFile::printf(const char* fmt, ...)
{
	if(logFile->log == NULL)
	{
		if(logFile->created)
		{
			logFile->log = CreateFile(
				logFile->name,
				GENERIC_WRITE,
				0,
				NULL,
				OPEN_ALWAYS,
				FILE_ATTRIBUTE_NORMAL,
				NULL);

				if(logFile->log != NULL)
					SetFilePointer(logFile->log, 0, (LPLONG) NULL, FILE_END);
		}
		else
		{
			logFile->log = CreateFile(
				logFile->name,
				GENERIC_WRITE,
				0,
				NULL,
				CREATE_ALWAYS,
				FILE_ATTRIBUTE_NORMAL,
				NULL);

			if(logFile->log)
			{
				logFile->created = True;

				time_t t;
				time(&t);

				wfprintf(logFile->log, "%s created at %s\r\n", logFile->name, ctime(&t));
			}
		}
	}

	if(logFile->log != NULL)
	{
		char buffer[500];

		va_list vaList;
		va_start(vaList, fmt);
		_vbprintf(buffer, 500, fmt, vaList);
		va_end(vaList);

#if 0
		// Replace all \n with \r\n

		size_t l = strlen(buffer);
		for(int i = 0; i < l; i++)
		{
			if(buffer[i] == '\n')
			{
				for(int j = l; j > i; j--)
					buffer[j] = buffer[j-1];
				if(l < 499)
					l++;
				buffer[i] = '\n';
			}
		}
#endif

		DWORD bw;
		WriteFile(logFile->log, buffer, strlen(buffer), &bw, NULL);
		wfprintf(logFile->log, "\r\n");
	}
}

/*
 * Temporarily close a log file
 */

void LogFile::close()
{
	if(logFile->log != NULL)
	{
		CloseHandle(logFile->log);
		logFile->log = NULL;
	}
}

#endif		// DEBUG

