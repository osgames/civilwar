/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Load in .BMP file
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.4  1995/11/22 10:43:46  Steven_Green
 * File Classes changed
 *
 * Revision 1.3  1995/10/25 09:52:13  Steven_Green
 * Bitmaps are top down
 *
 * Revision 1.2  1995/10/20 11:04:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/10/17 12:02:04  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "globals.hpp"
#include "bmp.hpp"
#include "dib.hpp"
#include "palette.hpp"
#include "winfile.hpp"
#include "autorun.hpp"


BOOL readBMP(LPCSTR fileName, ReadBMPMode mode, DIB** pdib)
{
	ASSERT(fileName != 0);
	ASSERT(mode < RBMP_HowMany);

	/*
	 * Open the file (Use a class so automatically closed)
	 */

	WinFileBinaryReader fr(fileName);

	if(fr.getError())
	{
#ifdef DEBUG
		debugMessage("Can't open %s", fileName);
#endif
		return FALSE;
	}

	/*
	 * Read BITMAPFILEHEADER
	 */

	BITMAPFILEHEADER header;
	if(!fr.read(&header, sizeof(header)))
		return FALSE;

	/*
	 * Read BITMAPINFO
	 */

	BITMAPINFOHEADER bmh;
	if(!fr.read(&bmh, sizeof(bmh)))
		return FALSE;

	/*
	 * Check for valid type
	 */

	if((bmh.biPlanes != 1) ||
	   (bmh.biBitCount != 8) ||
		(bmh.biCompression != BI_RGB))
	{
#ifdef DEBUG
		debugMessage("%s is not 256 colour RGB Windows BMP", fileName);
#endif
		return FALSE;
	}

	/*
	 * Read and Copy Colour Table
	 */

	RGBQUAD pal[256];

	if(!fr.read(pal, sizeof(pal)))
		return FALSE;

	if((gApp.getPalette() == NULL) || (mode == RBMP_ForcePalette) || (mode == RBMP_OnlyPalette))
		gApp.setPalette(makeIdentityPalette(pal, 256));

	if(mode == RBMP_OnlyPalette)
	{
		return TRUE;
	}
	else
	{
		/*
	 	 * Create DIB
	 	 */

		DIB* dib = new DIB(bmh.biWidth, -bmh.biHeight);

		/*
	 	 * Read and Copy Bit Table
		 *
		 * Do it line by line so that it is right way up
	 	 */

#if 0
		if(!fr.read(dib->getBits(), dib->getStorageWidth() * dib->getHeight()))
		{
			delete dib;
			return FALSE;
		}
#endif

		PUCHAR dest = dib->getBits() + dib->getHeight() * dib->getStorageWidth();
		int y = dib->getHeight();
		while(y--)
		{
			dest -= dib->getStorageWidth();

			if(!fr.read(dest, dib->getStorageWidth()))
			{
				delete dib;
				return FALSE;
			}
		}



		/*
	 	 * Remap it
	 	 */

		dib->remap(pal);

		*pdib = dib;

		return TRUE;
	}
}

