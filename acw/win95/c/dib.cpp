/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Device Independant Bitmap support functions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.8  1995/11/22 10:43:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1995/11/07 10:39:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1995/11/03 13:26:14  Steven_Green
 * Line Drawing and Clipping added
 *
 * Revision 1.5  1995/10/29 16:01:13  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1995/10/25 09:52:13  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1995/10/18 10:59:19  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/10/17 12:02:04  Steven_Green
 * Works properly with palette manager and BMP loader
 *
 * Revision 1.1  1995/10/16 11:31:51  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "globals.hpp"
#include "myassert.hpp"
#include "dib.hpp"
#include "palette.hpp"
#include "autorun.hpp"
// #include "misc.hpp"


DIB::DIB(int width, int height)
{
	makeBitmapInfo(width, height);
}

DIB::~DIB()
{
	if(handle != NULL)
		DeleteObject(handle);
	if(binfo != NULL)
		GlobalFreePtr(binfo);
}

/*
 * Create DIB from a BMP resource
 */

DIB::DIB(const char* bmpResName)
{
	/*
	 * Load in the bitmap
	 */

	HRSRC hRes = FindResource(gApp.getInstance(), bmpResName, RT_BITMAP);
	HGLOBAL hGlobal = LoadResource(gApp.getInstance(), hRes);
	LPBITMAPINFO bm = (LPBITMAPINFO) LockResource(hGlobal);

	makeBitmapInfo(bm->bmiHeader.biWidth, bm->bmiHeader.biHeight);

	/*
	 * Copy the image into bits
	 */

	PUCHAR dest = getBits();
	PUCHAR src = (PUCHAR) ( ((PUCHAR)bm->bmiColors) + getColours()*sizeof(RGBQUAD));

	memcpy(dest, src, getStorageWidth() * getHeight());

	/*
	 * Remap the images colours
	 */

	remap(bm->bmiColors);

	/*
	 * Don't care what the SDK says, I'm going to unlock and free them
	 * otherwise how can system know whether I've finished with it so 
	 * it can be freed from memory?
	 */

	// UnlockResource(bm);
	FreeResource(hGlobal);
}

void DIB::remap(LPRGBQUAD col)
{
	/*
	 * Make remap table
	 */

#ifdef DEBUG_PALETTE
		char buffer[100];
		OutputDebugString("Remap Table:\n");
#endif

	UCHAR imap[256];

	for(int i = 0; i < 256; i++)
	{
		imap[i] = (UCHAR) GetNearestPaletteIndex(gApp.getPalette(), RGB(col->rgbRed, col->rgbGreen, col->rgbBlue));

#ifdef DEBUG_PALETTE
		wsprintf(buffer, "%d: %d (%d,%d,%d)\n",
			i, imap[i], 
			(int) col->rgbRed,
			(int) col->rgbGreen,
			(int) col->rgbBlue);
		OutputDebugString(buffer);
#endif

		col++;
	}

	/*
	 * Remap bits
	 */

	PUCHAR p = getBits();
	LONG l = getStorageWidth() * getHeight();
	while(l--)
	{
		*p = imap[*p];
		p++;
	}
}

/*
 * To make it top down, set height negative.
 */

void DIB::makeBitmapInfo(int width, int height)
{
	ASSERT(width > 0);
	ASSERT(height != 0);

	/*
	 * Set up bitmapinfo
	 */

	long storage = sizeof(BITMAPINFOHEADER) + 256 * sizeof(RGBQUAD);
	binfo = (BITMAPINFO*) GlobalAllocPtr(GMEM_MOVEABLE, storage);

	if(!binfo)
		return;

	binfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	binfo->bmiHeader.biWidth = width;
	binfo->bmiHeader.biHeight = height;
	binfo->bmiHeader.biPlanes = 1;
	binfo->bmiHeader.biBitCount = 8;
	binfo->bmiHeader.biCompression = BI_RGB;
	binfo->bmiHeader.biSizeImage = 0;
	binfo->bmiHeader.biXPelsPerMeter = 0;
	binfo->bmiHeader.biYPelsPerMeter = 0;
	binfo->bmiHeader.biClrUsed = 0;
	binfo->bmiHeader.biClrImportant = 0;

	/*
	 * Set up identity table (0..256)
	 */

	PUSHORT col = (PUSHORT) binfo->bmiColors;
	for(int i = 0; i < 256; i++)
		*col++ = (USHORT) i;

	/*
	 * Get a handle
	 */

	HDC hdc = GetDC(NULL);
	HPALETTE oldPal = SelectPalette(hdc, gApp.getPalette(), FALSE);
	RealizePalette(hdc);
	handle = CreateDIBSection(hdc, binfo, DIB_PAL_COLORS, (VOID**)&bits, NULL, 0);
	SelectPalette(hdc, oldPal, FALSE);
	ReleaseDC(NULL, hdc);

	ASSERT(handle != NULL);
	ASSERT(bits != NULL);
}

#if !defined(EXCLUDE_DRAW_DIB)

/*=================================================================
 * DIB Drawing Functions
 *
 * These need optimising.
 */

/*
 * Get pixel address of a given coordinate
 */

PUCHAR DrawDIB::getAddress(LONG x, LONG y)
{
	ASSERT(x >= 0);
	ASSERT(x < getWidth());
	ASSERT(y >= 0);
	ASSERT(y < getHeight());

	/*
	 * Tell GDI to finish anything it's still doing
	 * I may be slow to put it here, in which case anything that
	 * does things to bitmaps should call GdiFlush() and remove
	 * it from here.
	 */

	GdiFlush();

	return getBits() + x + y * getStorageWidth();
}

ColourIndex DrawDIB::getColour(COLORREF cRef)
{
	return (ColourIndex) GetNearestPaletteIndex(gApp.getPalette(), cRef);
}

/*
 * Do some clipping
 */

BOOL DrawDIB::clip(LONG& x, LONG& y, LONG& width, LONG& height)
{
	RECT r1;

	r1.left = x;
	r1.right = x + width;
	r1.top = y;
	r1.bottom = y + height;

	RECT r2;

	r2.left = 0;
	r2.right = getWidth();
	r2.top = 0;
	r2.bottom = getHeight();

	RECT r3;

	if(IntersectRect(&r3, &r1, &r2))
	{
		x = r3.left;
		y = r3.top;
		width = r3.right - r3.left;
		height = r3.bottom - r3.top;

		/*
		 * Check that values are indeed clipped!
		 */

		ASSERT(x >= 0);
		ASSERT((x + width) <= getWidth());
		ASSERT(y >= 0);
		ASSERT((y + height) <= getHeight());

		return TRUE;
	}
	else
		return FALSE;
}

/*
 * Rather pathetic plot pixel routine for anything that
 * wants to use it.
 *
 * It will be useful for prototyping graphics routines because at least
 * it is safe and won't write all over memory if the coordinates are bad.
 */

void DrawDIB::plot(LONG x, LONG y, ColourIndex colour)
{
	if( (x >= 0) && (x < getWidth()) && (y >= 0) && (y < getHeight()))
	{
		PUCHAR dest =  getBits() + x + y * getStorageWidth();
		*dest = colour;
	}
}


/*
 * Draw a Horizontal line in given colour
 *
 * WARNING: No clipping is done!
 */

void DrawDIB::hLineFast(LONG x, LONG y, LONG length, ColourIndex colour)
{
	ASSERT(x >= 0);
	ASSERT((x + length) <= getWidth());
	ASSERT(y >= 0);
	ASSERT(y < getHeight());
	ASSERT(length > 0);

	PUCHAR pb = getAddress(x,y);
	memset(pb, colour, length);
}

/*
 * Draw a vertical line in given colour
 *
 * WARNING: No clipping is done!
 */

void DrawDIB::vLineFast(LONG x, LONG y, LONG length, ColourIndex colour)
{
	ASSERT(x >= 0);
	ASSERT(x < getWidth());
	ASSERT(y >= 0);
	ASSERT((y + length) <= getHeight());
	ASSERT(length > 0);

	PUCHAR pb = getAddress(x,y);

	while(length--)
	{
		*pb = colour;
		pb += getStorageWidth();
	}
}

/*
 * Clipped versions of hLine and vLine
 */

void DrawDIB::hLine(LONG x, LONG y, LONG length, ColourIndex colour)
{
	LONG height = 1;

	ASSERT(length > 0);

	if(clip(x, y, length, height))
		hLineFast(x, y, length, colour);
}

void DrawDIB::vLine(LONG x, LONG y, LONG length, ColourIndex colour)
{
	LONG width = 1;
	
	ASSERT(length > 0);

	if(clip(	x, y, width, length))
		vLineFast(x, y, length, colour);
}

/*
 * Draw a filled rectangle
 *
 * WARNING: No clipping is done!
 *
 */

void DrawDIB::rect(LONG x, LONG y, LONG width, LONG height, ColourIndex colour)
{
	if(clip(x, y, width, height))
	{
		ASSERT(x >= 0);
		ASSERT((x + width) <= getWidth());
		ASSERT(y >= 0);
		ASSERT((y + height) <= getHeight());

		PUCHAR pb = getAddress(x,y);

		while(height--)
		{
			memset(pb, colour, width);
			pb += getStorageWidth();
		}
	}
}

/*
 * Draw a rectangle
 */

void DrawDIB::frame(LONG x, LONG y, LONG width, LONG height, ColourIndex colour)
{
	LONG oldX = x;
	LONG oldY = y;
	LONG oldW = width;
	LONG oldH = height;

	/*
	 * Clip it and do the job
	 */

	if(clip(x, y, width, height))
	{
		if(oldY == y)
			hLineFast(		  x, 			  y,  width, colour);
		if( (y + height) <= getHeight())
			hLineFast(		  x, y+height-1,  width, colour);
		if(oldX == x)
			vLineFast(		  x, 			  y, height, colour);
		if( (x + width) <= getWidth())
			vLineFast(x+width-1, 			  y, height, colour);
	}
}

/*
 * Draw a rectangle, but do top/left in 1 colour and bottom/right in another
 */

void DrawDIB::frame(LONG x, LONG y, LONG width, LONG height, ColourIndex colour1, ColourIndex colour2)
{
	LONG oldX = x;
	LONG oldY = y;
	LONG oldW = width;
	LONG oldH = height;

	/*
	 * Clip it and do the job
	 */

	if(clip(x, y, width, height))
	{
		if(oldY == y)
			hLineFast(		  x, 			  y,  width, colour1);
		if( (y + height) <= getHeight())
			hLineFast(		  x, y+height-1,  width, colour2);
		if(oldX == x)
			vLineFast(		  x, 			  y, height, colour1);
		if( (x + width) <= getWidth())
			vLineFast(x+width-1, 			  y, height, colour2);
	}
}

/*
 * Draw an outline circle
 */

void DrawDIB::circle(LONG x, LONG y, LONG radius, ColourIndex colour)
{
	LONG x1 = x;
	LONG y1 = y;
	LONG w1 = radius;
	LONG h1 = radius;

	if(clip(x1, y1, w1, h1))
	{
		if((x1 != x) || (y1 != y) || (w1 != radius) || (h1 != radius))
		{
			frame(x - radius, y - radius, radius*2, radius*2, colour);
			return;
		}

		/*
		 * Proper Circle Routine
		 */

		int dx = radius;
		int dy = 0;

		int value = dx/2;

		/*
		 * Fill Middle Pixels in in advance to save time
		 */

		plot(x - dx, y, colour);
		plot(x + dx, y, colour);
		plot(x, y + dx, colour);
		plot(x, y - dx, colour);

		while(dx > dy)		// While slope < 45 degrees
		{
			value += dy;
			while(value >= dx)
			{
				value -= dx;
				dx--;
			}

			dy++;

			if(dx < dy)
				break;

			/*
			 * Fill in pixels
			 */

			plot(x - dx, y + dy, colour);
			plot(x + dx, y + dy, colour);
			plot(x - dx, y - dy, colour);
			plot(x + dx, y - dy, colour);
			plot(x - dy, y + dx, colour);
			plot(x + dy, y + dx, colour);
			plot(x - dy, y - dx, colour);
			plot(x + dy, y - dx, colour);


		}
	}
}

/*
 * Draw an outline circle
 */

void DrawDIB::circle(LONG x, LONG y, LONG radius, ColourIndex colour1, ColourIndex colour2)
{
	LONG x1 = x;
	LONG y1 = y;
	LONG w1 = radius;
	LONG h1 = radius;

	if(clip(x1, y1, w1, h1))
	{
		if((x1 != x) || (y1 != y) || (w1 != radius) || (h1 != radius))
		{
			frame(x - radius, y - radius, radius*2, radius*2, colour1, colour2);
			return;
		}

		/*
		 * Proper Circle Routine
		 */

		int dx = radius;
		int dy = 0;

		int value = dx/2;

		/*
		 * Fill Middle Pixels in in advance to save time
		 */

		plot(x - dx, y, colour1);
		plot(x + dx, y, colour2);
		plot(x, y + dx, colour2);
		plot(x, y - dx, colour1);

		while(dx > dy)		// While slope < 45 degrees
		{
			value += dy;
			while(value >= dx)
			{
				value -= dx;
				dx--;
			}

			dy++;

			if(dx < dy)
				break;

			/*
			 * Fill in pixels
			 */

			plot(x - dx, y + dy, colour1);
			plot(x + dx, y + dy, colour2);
			plot(x - dx, y - dy, colour1);
			plot(x + dx, y - dy, colour2);
			plot(x - dy, y + dx, colour2);
			plot(x + dy, y + dx, colour2);
			plot(x - dy, y - dx, colour1);
			plot(x + dy, y - dx, colour1);


		}
	}
}


/*
 * Draw a filled circle
 *
 * Diameter is (radius * 2) + 1, because this means it is more
 * easily centred on the given coordinates.
 *
 * e.g.			  ***
 *	 r=2			 *****
 *              **+**
 *              *****
 * 				  ***
 */

void DrawDIB::disc(LONG x, LONG y, LONG radius, ColourIndex colour)
{
	/*
	 * For debugging when we are not bothered about clipping
	 * call the frame function if it needs to be clipped
	 */

	LONG x1 = x;
	LONG y1 = y;
	LONG w1 = radius;
	LONG h1 = radius;

	if(clip(x1, y1, w1, h1))
	{
		if((x1 != x) || (y1 != y) || (w1 != radius) || (h1 != radius))
		{
			rect(x - radius, y - radius, radius*2, radius*2, colour);
			return;
		}

		/*
		 * This is the proper circle routine
		 * To get it up and running it will be calling hLine
		 * Later on we'll copy the scan line filling here
		 *
		 * Use a bresenam circle algorithm
		 * This works by calculating values for a 45 degree segment
		 * and then drawing 4 scan lines for each of these based
		 * on knowing that a circle is symetrical
		 *
		 * Basic algorithm is:
		 *		For each scan line:
		 *			y := y + 1
		 *			x := x - y/x
		 *
		 * y/x is always less than 1 for angles below 45 degrees
		 * so a bresenham style remainder is used.
		 */

		int dx = radius;
		int dy = 0;

		int value = dx/2;

		/*
		 * Fill Middle line in in advance to save time
		 */

		hLine(x - dx, y, dx+dx+1, colour);

		while(dx > 0)		// While slope < 90 degrees
		{
			value += dy;
			while(value >= dx)
			{
				value -= dx;
				dx--;
				if(dx < 0)
					break;
			}
			if(dx < 0)
				break;

			dy++;


			/*
			 * Fill in a scan line
			 */

			hLine(x - dx, y + dy, dx+dx+1, colour);
			hLine(x - dx, y - dy, dx+dx+1, colour);


		}
	}
}

/*
 * Straight line between (and including) 2 points
 */

void DrawDIB::line(LONG x1, LONG y1, LONG x2, LONG y2, ColourIndex colour)
{
	/*
	 * Pull out special cases of horizontal or vertical
	 */

	if(x1 == x2)
	{
		if(y1 > y2)
			qswap(y1, y2);
		vLine(x1, y1, y2-y1+1, colour);
	}
	else if(y1 == y2)
	{
		if(x1 > x2)
			qswap(x1, x2);
		hLine(x1, y1, x2-x1+1, colour);
	}
	else
	{
		/*
		 * The real thing
		 */


		if(clipLine(x1, y1, x2, y2))
		{

			// For now just put a dot at each end

			// plot(x1, y1, colour);
			// plot(x2, y2, colour);

			/*
	 		 * Work out Bresenham terms
	 		 */

			LONG dx = x2 - x1;
			LONG dy = y2 - y1;

			ULONG adx = (dx < 0) ? -dx : +dx;
			ULONG ady = (dy < 0) ? -dy : +dy;

			/*
	 		 * Which is the major axis?
	 		 */

			if(adx > ady)
			{
				// X is the main axis

				// Make sure we are going from left to right

				if(dx < 0)
				{
					// swap(p1, p2);
					swap(x1, x2);
					swap(y1, y2);
					dy = -dy;
				}

				UBYTE* ad = getAddress(x1,y1);

				*ad = colour;

				if(x1 != x2)
				{
					LONG value = 0x8000;								// Initial Bresenham term
					LONG add = (ady * 0x10000) / adx;				// Amount to add
					LONG ddy = (dy < 0) ? -getWidth() : +getWidth();		// Line offset

					while(x1++ < x2)
					{
						value += add;
						if(value >= 0x10000)
						{
							ad += ddy;
							value -= 0x10000;
						}
						ad++;

						*ad = colour;
					}
				}
			}
			else
			{
				// Y is the main axis

				// Make sure we are going from Top to Bottom

				if(dy < 0)
				{
					swap(x1,x2);
					swap(y1,y2);
					// Point tPoint = p1;
					// p1 = p2;
					// p2 = tPoint;

					dx = -dx;
				}

				UBYTE* ad = getAddress(x1,y1);

				*ad = colour;

				if(y1 != y2)
				{
					LONG value = 0x8000;								// Initial Bresenham term
					LONG add = (adx * 0x10000) / ady;				// Amount to add
					LONG ddx = (dx < 0) ? -1 : +1;					// Offset to move left/right

					while(y1++ < y2)
					{
						value += add;
						if(value >= 0x10000)
						{
							ad += ddx;
							value -= 0x10000;
						}
						ad += getWidth();

						*ad = colour;
					}
				}
			}
		}
	}

}

/*
 * Line Clipping
 */


static LONG clipEdge(LONG edge, LONG x1, LONG y1, LONG x2, LONG y2)
{
	LONG leftX;
	LONG leftY;
	LONG rightX;
	LONG rightY;

	if(x1 > x2)
	{
		leftX = x2;
		leftY = y2;
		rightX = x1;
		rightY = y1;
	}
	else
	{
		leftX = x1;
		leftY = y1;
		rightX = x2;
		rightY = y2;
	}

	while( (leftX != edge) && (leftY != rightY))
	{
		// Point mid = left + ((right - left + Point(1,1)) >> 1);
		LONG midX;
		LONG midY;

		midX = leftX + ((rightX - leftX + 1) >> 1);
		midY = leftY + ((rightY - leftY + 1) >> 1);

		if(midX <= edge)
		{
			leftX = midX;
			leftY = midY;
		}
		else
		{
			rightX = midX;
			rightY = midY;
		}

	}

	return leftY;
}

/*
 * Clip a line to a region's boundary
 * return True if anything left on screen
 *        False if nothing left
 *
 * Copy CohenSutherland (see Foley/VanDam/etc pg. 116)
 *
 * Note that points are relative to window, so are clipped
 * to (0,0,w,h) rather than (x,y,maxX,maxY)
 */

#define CLIP_TOP		1
#define CLIP_BOTTOM	2
#define CLIP_LEFT		4
#define CLIP_RIGHT	8

inline UBYTE DrawDIB::getOutCode(LONG x, LONG y)
{
	UBYTE value = 0;

	if(y >= getHeight())
		value = CLIP_TOP;
	else if(y < 0)
		value = CLIP_BOTTOM;

	if(x >= getWidth())
		value |= CLIP_RIGHT;
	else if(x < 0)
		value |= CLIP_LEFT;

	return value;
}

Boolean DrawDIB::clipLine(LONG& x1, LONG& y1, LONG& x2, LONG& y2)
{
#ifdef DEBUG_CLIP
	debugLog("Clip(%ld,%ld -> %ld,%ld)\n",
		x1, y1, x2, y2);
#endif

	for(;;)
	{
		UBYTE outcode0 = getOutCode(x1, y1);
		UBYTE outcode1 = getOutCode(x2, y2);

		/*
		 * Both points on screen if outcodes are both 0
		 */

		if( (outcode0 | outcode1) == 0)
		{
#ifdef DEBUG_CLIP
			debugLog("Clip Passed: %ld,%ld -> %ld,%ld\n",
				x1, y1, x2, y2);
#endif
			return True;
		}

		/*
		 * Points in same section, so it is all clipped!
		 */

		if( (outcode0 & outcode1) != 0)
		{
#ifdef DEBUG_CLIP
			debugLog("Clip Failed: %ld,%ld -> %ld,%ld\n",
				x1, y1, x2, y2);
#endif
			return False;
		}

		/*
		 * Find one of the points outside the rectangle
		 */

		UBYTE outCode;

		LONG* x;
		LONG* y;
		
		if(outcode0)
		{
			outCode = outcode0;
			x = &x1;
			y = &y1;
		}
		else
		{
			outCode = outcode1;
			x = &x2;
			y = &y2;
		}

		if(outCode & CLIP_TOP)
		{
			*x = clipEdge(getHeight() - 1, y1,x1, y2,x2);
			*y = getHeight() - 1;
		}
		else if(outCode & CLIP_BOTTOM)
		{
			*x = clipEdge(0, y1,x1, y2,x2);
			*y = 0;
		}
		else if(outCode & CLIP_RIGHT)
		{
			*y = clipEdge(getWidth() - 1, x1,y1, x2,y2);
			*x = getWidth() - 1;
		}
		else if(outCode & CLIP_LEFT)
		{
			*y = clipEdge(0, x1,y1, x2,y2);
			*x = 0;
		}
	}	// Loop again with clipped segment
}


void DrawDIB::blitFrom(DIB* from)
{
	ASSERT(this != 0);
	ASSERT(from != 0);
	ASSERT(getWidth() == from->getWidth());
	ASSERT(getHeight() == from->getHeight());

	PUCHAR dest = getBits();
	PUCHAR src = from->getBits();

	ASSERT(src != 0);
	ASSERT(dest != 0);

 	memcpy(dest, src, getStorageWidth() * getHeight());
}

#endif	// EXCLUDE_DRAW_DIB

