/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	My Assertion Function
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.9  1996/06/22 19:06:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1996/02/15 10:50:34  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1995/11/22 10:43:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1995/11/14 11:24:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1995/11/13 11:08:49  Steven_Green
 * Altered MessageBox usage
 *
 * Revision 1.4  1995/10/29 16:01:13  Steven_Green
 * Added GeneralError
 * Added \n\r to output
 * Setup debug.log and error.log
 *
 * Revision 1.3  1995/10/18 10:59:19  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/10/17 12:02:04  Steven_Green
 * debugMessage added
 *
 * Revision 1.1  1995/10/16 11:31:51  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "globals.hpp"
#include <stdio.h>
#include <time.h>
#include "myassert.hpp"

/*
 * Names of files
 */

static char szErrorLog[] = "error.log";
static char szDebugLog[] = "debug.log";

/*
 * Helper Function
 */

inline wfprintf(HANDLE fp, LPSTR fmt, ...)
{
	char buffer[500];

	va_list vaList;
	va_start(vaList, fmt);
	_vbprintf(buffer, 500, fmt, vaList);
	va_end(vaList);

	DWORD bw;

	WriteFile(fp, buffer, strlen(buffer), &bw, NULL);
}

inline void messageBox(const char* title, const char* text)
{
	MessageBox(0, text, title, MB_ICONSTOP | MB_SETFOREGROUND);
}

/*
 * Write a message to error.log
 */

static void addToErrorLog(const char* s, const char* title)
{
#ifdef NOT_WINDOWS
	FILE* fp = fopen(szErrorLog, "a");

	if(fp)
	{
		time_t t;
		time(&t);

		fprintf(fp, "------------------------------------------\r\n");
		fprintf(fp, "General Error Report\r\n");
		fprintf(fp, "at %s\r\n", ctime(&t));
		fprintf(fp, "%s\r\n", s);
		fclose(fp);
	}
#else
	HANDLE fp = CreateFile(
		szErrorLog,
		GENERIC_WRITE, 
		0, 
		NULL, 
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if(fp)
	{
		SetFilePointer(fp, 0, (LPLONG) NULL, FILE_END);
		time_t t;
		time(&t);

		wfprintf(fp, "------------------------------------------\r\n");
		wfprintf(fp, "%s\r\n", title);
		wfprintf(fp, "at %s\r\n", ctime(&t));
		wfprintf(fp, "%s\r\n", s);
		CloseHandle(fp);
	}
#endif
}

#if defined(DEBUG)

static void addToDebugLog(const char* s)
{
#ifdef NOT_WINDOWS
	FILE* fp = fopen(szDebugLog, "a");

	if(fp)
	{
		time_t t;
		time(&t);

		fprintf(fp, "%s", s);
		fclose(fp);
	}
#else

	/*
	 * These 2 lines ensure that 2 threads can't write to 
	 * the logfile at the same time
	 */

#if 0
	// static SharedData critic;
	// LockData lock(&critic);
	// critic.enter();
	static CRITICAL_SECTION critic;
	static BOOL init = FALSE;
	if(!init)
		InitializeCriticalSection(&critic);
	EnterCriticalSection(&critic);
#endif

	HANDLE fp = CreateFile(
		szDebugLog,
		GENERIC_WRITE, 
		0, 
		NULL, 
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if(fp)
	{
		SetFilePointer(fp, 0, (LPLONG) NULL, FILE_END);

		DWORD bw;

		while(*s)
		{
			if(*s == '\n')
			{
				static char eol[] = { '\r', '\n' };

				WriteFile(fp, eol, 2, &bw, NULL);
			}
			else
				WriteFile(fp, s, 1, &bw, NULL);

			s++;
		}

		CloseHandle(fp);
	}

#if 0
	// critic.leave();
	LeaveCriticalSection(&critic);
#endif

#endif
}

void _myAssert(int value, char* expr, char* file, int line)
{
	static BOOL doingAssert = False;

	if(!doingAssert)
	{
		doingAssert = True;

#if 0
		throw GeneralError("Assertion Failed in\nFile: %s Line: %d\n\"%s\"",
			file, line, expr);
#endif

		char buf[500];

		DWORD err = GetLastError();

		wsprintf(buf, "Assertion Failed in\r\nFile: %s Line: %d\r\n\"%s\"\r\nLast error: %lu",
			file, line, expr, err);

		addToErrorLog(buf, "Assertion Failure");
		messageBox("Assertion Failure", buf);

		// PostQuitMessage(0);
		// ExitProcess();

		throw QuitProgram();
	}
}
#endif		// DEBUG

/*
 * A Simple message box
 */

void alert(const char* title, const char* fmt, ...)
{
	char buffer[1000];

	va_list vaList;
	va_start(vaList, fmt);
	vsprintf(buffer, fmt, vaList);

	messageBox(title, buffer);
}

#if defined(DEBUG)

/*
 * A Debug Message in a window
 */

static char* d_file;
static int d_line;

void _setDebugInfo(char* file, int line)
{
	d_file = file;
	d_line = line;
}

void _myDebugMessage(const char* fmt, ...)
{
	char buffer[1000];
	char buf2[200];

	va_list vaList;
	va_start(vaList, fmt);
	// wvsprintf(buffer, fmt, vaList);
	vsprintf(buffer, fmt, vaList);

	wsprintf(buf2, "Error in %s, line %d", d_file, d_line);

	addToErrorLog(buffer, buf2);

	messageBox(buf2, buffer);
}


/*
 * Add message to debug log
 */

void debugLog(const char* fmt, ...)
{
	static int indent = 0;
	int useIndent = indent;

	char buffer[1000];

	char* ptr = buffer;

	char newIndent = 0;

	if(fmt[0] == '+')
	{
		indent++;
		fmt++;
		newIndent = '/';
	}
	else if(fmt[0] == '-')
	{
		indent--;
		useIndent = indent;
		fmt++;
		newIndent = '\\';
	}

	for(int i = 0; i < useIndent; i++)
	{
		*ptr++ = '|';
		*ptr++ = ' ';
	}

	if(newIndent != 0)
	{
		*ptr++ = newIndent;
	}

	va_list vaList;
	va_start(vaList, fmt);
	vsprintf(ptr, fmt, vaList);

	// OutputDebugString(buffer);
	addToDebugLog(buffer);
}

#endif	// DEBUG

/*
 * Exception Handling
 */

static Boolean GeneralError::doingError = False;
static Boolean GeneralError::firstError = False;

GeneralError::GeneralError()
{
	describe = 0;
}

GeneralError::GeneralError(const char* fmt, ...)
{
	char buffer[500];

	va_list vaList;
	va_start(vaList, fmt);
	_vbprintf(buffer, 500, fmt, vaList);
	va_end(vaList);

	/*
	 * Convert any \r's into \r\n
	 */

	char* s = buffer;
	while(*s)
	{
		if(*s++ == '\r')
		{
			char* s1 = s;
			char last = '\n';

			do
			{
				char c = *s1;
				*s1 = last;
				last = c;

				s1++;
			}  while(last);
			*s1 = 0;
		}
	}

	setMessage(buffer);
	messageBox("Fatal Error!", buffer);
}

void GeneralError::setMessage(const char* s)
{
	firstError = doingError;
	doingError = True;

	describe = strdup(s);

	if(!firstError)
		addToErrorLog(s, "General Error Report");
}

const char* GeneralError::get()
{
	return describe;
}

/*
 * A Static class used to make sure debug.log is deleted when program
 * starts up.
 */

class KillDebugLog {
public:
	KillDebugLog()
	{
		DeleteFile(szDebugLog);
	}
};

static KillDebugLog debugLogKiller;
