/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Miscellaneous support classes
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1996/02/29 11:01:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1996/02/16 17:33:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/10/18 10:59:19  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "globals.hpp"
#include <stdio.h>
#include "wmisc.hpp"
#include "autorun.hpp"
// #include "misc.hpp"

ResString::ResString(UINT resID)
{
	char buffer[256];

	LoadString(gApp.getInstance(), resID, buffer, 256);

	text = new char[strlen(buffer) + 1];
	strcpy(text, buffer);
}

ResString::~ResString()
{
	delete[] text;
}

#if 0
void wTextOut(HDC hdc, int x, int y, LPCTSTR text)
{
	TextOut(hdc, x, y, text, lstrlen(text));
}

void PASCAL wTextPrintf(HDC hdc, int x, int y, LPCTSTR text, ...)
{
	MemPtr<char> buffer(1024);

	va_list vaList;
	va_start(vaList, text);
	_vbprintf(buffer, 1024, text, vaList);
	// wvsprintf(buffer, text, vaList);
	va_end(vaList);

	wTextOut(hdc, x, y, buffer);
}
#endif
