/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Palette Manager for Windows 95
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1995/11/07 10:39:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/10/17 12:02:04  Steven_Green
 * Identity palette works properly.
 *
 * Revision 1.1  1995/10/16 11:31:51  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "globals.hpp"
#include "palette.hpp"
#include "autorun.hpp"

/*
 * Clear system palette so that makeIndentityPalette will work
 */

void clearSystemPalette()
{
	struct {
		WORD version;
		WORD nEntries;
		PALETTEENTRY aEntries[256];
	} pal = {
		0x300,
		256
	};

	for(int count = 0; count < 256; count++)
	{
		pal.aEntries[count].peRed = 0;
		pal.aEntries[count].peGreen = 0;
		pal.aEntries[count].peBlue = 0;
		pal.aEntries[count].peFlags = PC_NOCOLLAPSE;
	}

	HDC hdc = GetDC(NULL);
	HPALETTE blackP = CreatePalette((LOGPALETTE*) &pal);
	if(blackP)
	{
		HPALETTE oldPal = SelectPalette(hdc, blackP, FALSE);
		RealizePalette(hdc);
		SelectPalette(hdc, oldPal, FALSE);
		DeleteObject(blackP);
	}
	ReleaseDC(NULL, hdc);

}


/*
 * Set up identity palette that can be selected into a DC
 */

HPALETTE makeIdentityPalette(LPRGBQUAD aRGB, int nColors)
{
	clearSystemPalette();

	struct Palette {
		WORD version;
		WORD entryCount;
		PALETTEENTRY aEntries[256];
	};

	Palette palette = { 0x300, 256 };

	HDC hdc = GetDC(NULL);

	int nStaticColors = GetDeviceCaps(hdc, NUMCOLORS);
	GetSystemPaletteEntries(hdc, 0, 256, palette.aEntries);
	nStaticColors = nStaticColors / 2;
	for(int i = 0; i < nStaticColors; i++)
		palette.aEntries[i].peFlags = 0;

	int nUsableColors = nColors - nStaticColors;
	LPRGBQUAD lc = aRGB;
	int nc = 0;

	for(; (i < nUsableColors) && (nc < nColors); lc++, nc++)
	{
		/*
		 * Check if colour already added
		 */

		BOOL exists = FALSE;
		for(int n = 0; n < i; n++)
		{
			if( (palette.aEntries[n].peRed == lc->rgbRed) &&
 			    (palette.aEntries[n].peGreen == lc->rgbGreen) &&
 			    (palette.aEntries[n].peBlue == lc->rgbBlue) )
			{
				exists = TRUE;
			 	break;
			}
		}

		if(exists)
			continue;

		/*
		 * Check Top set of system colours
		 */

		for(n = 256 - nStaticColors; n < nColors; n++)
		{
			if( (palette.aEntries[n].peRed == lc->rgbRed) &&
 			    (palette.aEntries[n].peGreen == lc->rgbGreen) &&
 			    (palette.aEntries[n].peBlue == lc->rgbBlue) )
			{
				exists = TRUE;
			 	break;
			}
		}

		if(exists)
			continue;

		palette.aEntries[i].peRed = lc->rgbRed;
		palette.aEntries[i].peGreen = lc->rgbGreen;
		palette.aEntries[i].peBlue = lc->rgbBlue;
		palette.aEntries[i].peFlags = PC_NOCOLLAPSE;
		i++;
	}

	for(; i < (256 - nStaticColors); i++)
		palette.aEntries[i].peFlags = 0;

#ifdef DEBUG_PALETTE
	OutputDebugString("Palette\n");
	for(i = 0; i < 256; i++)
	{
		char buffer[100];
		wsprintf(buffer, "%d: %d,%d,%d [%d]\n",
			i,
			(int) palette.aEntries[i].peRed,
			(int) palette.aEntries[i].peGreen,
			(int) palette.aEntries[i].peBlue,
			(int) palette.aEntries[i].peFlags);
		OutputDebugString(buffer);
	}
#endif

	/*
	 * Right... we've finished fiddling with the palette
	 *
	 * Make a HPALETTE
	 * Realize it and find what colours we really have.
	 */

	HPALETTE hPal = CreatePalette((LOGPALETTE*) &palette);

	ReleaseDC(NULL, hdc);

	#if 0
	if(copyTable)
	{
		for(int i = 0; i < nColors; i++)
		{
			aRGB[i].rgbRed = palette.aEntries[i].peRed;
			aRGB[i].rgbGreen = palette.aEntries[i].peGreen;
			aRGB[i].rgbBlue = palette.aEntries[i].peBlue;
		}
	}
#endif


	return hPal;

}

#if 0
/*
 * Set up a palette
 */

HPALETTE makePalette(const char* bmpResName)
{

	HRSRC hRes = FindResource(gApp.getInstance(), bmpResName, RT_BITMAP);
	HGLOBAL hGlobal = LoadResource(gApp.getInstance(), hRes);
	LPBITMAPINFO bm = (LPBITMAPINFO) LockResource(hGlobal);

	LPRGBQUAD pal = bm->bmiColors;
	return makeIdentityPalette(pal, 256, TRUE);
}

HPALETTE makePaletteFromBMP(LPCSTR fileName)
{
	RGBQUAD pal[256];
	if(readBMPpalette(fileName, pal))
		return makeIdentityPalette(pal, 256, TRUE);
	else
		return NULL;
}
#endif
