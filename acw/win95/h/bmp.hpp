/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef BMP_H
#define BMP_H

#ifndef __cplusplus
#error bmp.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	BMP File Loader
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1995/10/25 09:52:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/10/17 12:03:08  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

class DIB;

enum ReadBMPMode {
	RBMP_Normal,
	RBMP_ForcePalette,
	RBMP_OnlyPalette,

	RBMP_HowMany
};

BOOL readBMP(LPCSTR fileName, ReadBMPMode mode, DIB** pdib);

#endif /* BMP_H */

