/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */

#ifndef CLOG_H
#define CLOG_H

#ifndef __cplusplus
#error clog.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Combat Log
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1996/06/22 19:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1996/02/08 17:34:11  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1996/01/19 11:30:29  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

class _Log;

class LogFile {
	_Log* logFile;
public:
	LogFile(const char* name);
	virtual ~LogFile();

	void printf(const char* fmt, ...);
	void close();			// Temporarily close log file
};


#endif /* CLOG_H */

