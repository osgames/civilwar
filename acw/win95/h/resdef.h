/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef RESDEF_H
#define RESDEF_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Resource Definitions for Civil War autorun
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Icons
 */

#define ICON_AUTORUN 		 1

/*
 * Bitmaps
 */

#define BM_STARTUP				1
#define BM_BUTTON_ON				2
#define BM_BUTTON_OFF			3
#define BM_BUTTON_DISABLED		4

/*
 * Strings
 */

#define IDS_Caption				10
// #define IDS_STARTUP_BMP			11

#define IDS_INSTALL				12
#define IDS_PLAY					13
#define IDS_QUIT					14
#define IDS_README				15
#define IDS_UPDATES				16


#ifdef __cplusplus
};
#endif

#endif /* RESDEF_H */

