/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef AUTORUN_HPP
#define AUTORUN_HPP

#ifndef __cplusplus
#error autorun.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Class to represent autorun application
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "app.hpp"
#include "myassert.hpp"

/*
 * Fowrward References
 */

class MainWindow;

/*
 * Structure to hold all values of a Device Context
 */


class DCsave {
	BOOL set;

	HDC hdc;

	HBITMAP oldBM;
	int oldBkMode;
	COLORREF oldTextCol;
	UINT oldTextAlign;
	HFONT oldFont;

public:
	DCsave();
	~DCsave();

	void save(HDC _hdc);
	void restore(HDC _hdc);
};

/*
 * Global variables in use by game
 */

class AutorunApp : 
	public APP
{
	BOOL active;					// Set if Windows main loop is running
	HDC  hDibDC;				// Display Context for DIBs
	HPALETTE hPalette;		// Colour palette!
	DCsave dibSave;

	MainWindow* mainWindow;
public:
	AutorunApp();
	~AutorunApp();

	BOOL initApplication();
	BOOL initInstance();
	void endApplication();
	int messageLoop();
	BOOL doCmdLine(LPCSTR cmd);

	HDC getDibDC() const { return hDibDC; }
	void saveDibDC() { dibSave.save(hDibDC); }
	void restoreDibDC() { dibSave.restore(hDibDC); }

	HPALETTE getPalette() const { return hPalette; }
	void setPalette(HPALETTE hp);
};

extern AutorunApp gApp;


#endif /* AUTORUN_HPP */

