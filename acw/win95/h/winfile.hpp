/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef WINFILE_H
#define WINFILE_H

#ifndef __cplusplus
#error winfile.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Classes to simplify File Handling
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.4  1996/06/22 19:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1995/11/22 10:45:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/10/29 16:02:49  Steven_Green
 * Use of Base Class to reduce duplicated code.
 *
 * Revision 1.1  1995/10/25 09:52:49  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG
#include "myassert.hpp"				// Needed for debugMessage
#endif

#include "filebase.hpp"

/*---------------------------------------------------------------
 * Windows File Handling Implementations
 *
 * Base Class for File operations
 */

class WinFileRWBase {
protected:
	HANDLE h;
	Boolean error;
#ifdef DEBUG
	LPCSTR fName;
#endif

public:
	WinFileRWBase();
	virtual ~WinFileRWBase();
	Boolean getError();
	Boolean seekTo(SeekPos where);
	SeekPos getPos();
	Boolean isOK() { return !error; }
};

class FileError {
};

/*
 * Helper for file reading
 */

class WinFileReader : public FileReader, public WinFileRWBase {
public:
	WinFileReader(LPCSTR name);
	~WinFileReader();
	Boolean read(LPVOID ad, DataLength length);
	Boolean read(LPVOID ad, DataLength length, DataLength& bytesRead);

	Boolean seekTo(SeekPos where) { return WinFileRWBase::seekTo(where); }
	SeekPos getPos() { return WinFileRWBase::getPos(); }
	Boolean isOK() { return WinFileRWBase::isOK(); }
};

/*
 * Helper for file writing
 */

class WinFileWriter : public FileWriter, public WinFileRWBase {
public:
	WinFileWriter(LPCSTR name);
	~WinFileWriter();
	Boolean write(const void* ad, DataLength length);

	Boolean seekTo(SeekPos where) { return WinFileRWBase::seekTo(where); }
	SeekPos getPos() { return WinFileRWBase::getPos(); }
	Boolean isOK() { return WinFileRWBase::isOK(); }
};

class WinFileAsciiReader : public WinFileReader {
	char* buffer;
	char* lineBuf;
	int bufIndex;
	int bufRead;

	static const int bufLength;
	static const int lineBufLength;
public:
	WinFileAsciiReader(LPCSTR name);
	Boolean isAscii() { return True; }
	char* readLine();
	Boolean seekTo(SeekPos where);
	Boolean rewind() { return seekTo(0); }
};

class WinFileAsciiWriter : public WinFileWriter {
public:
	WinFileAsciiWriter(LPCSTR name) : WinFileWriter(name) { }
	Boolean isAscii() { return True; }
};

class WinFileBinaryReader : public WinFileReader {
public:
	WinFileBinaryReader(LPCSTR name) : WinFileReader(name) { }
	Boolean isAscii() { return False; }
	char* readLine() { return 0; }
};

class WinFileBinaryWriter : public WinFileWriter {
public:
	WinFileBinaryWriter(LPCSTR name) : WinFileWriter(name) { }
	Boolean isAscii() { return False; }
};

/*
 * Stand alone helper functions
 */

BOOL dirExists(LPCSTR name);		// Return TRUE if exists
BOOL fileExists(LPCSTR name);		// Return TRUE if exists


#endif /* WINFILE_H */

