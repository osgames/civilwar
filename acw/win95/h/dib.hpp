/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef DIB_H
#define DIB_H

#ifndef __cplusplus
#error dib.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Device Independant BitMap support functions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.7  1995/11/22 10:45:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1995/11/07 10:39:54  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1995/11/03 13:26:31  Steven_Green
 * Line Drawing and clipping functions added to DrawDIB
 *
 * Revision 1.4  1995/10/29 16:02:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1995/10/25 09:52:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/10/17 12:03:08  Steven_Green
 * Better integration with palette manager
 *
 * Revision 1.1  1995/10/16 11:33:16  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "grtypes.hpp"

class DIB {
	PUCHAR bits;
	HBITMAP handle;
	BITMAPINFO* binfo;
public:
	DIB(int width, int height);
	DIB(const char* bmpResName);
	~DIB();

	PUCHAR getBits() const { return bits; }

	int getWidth() const { return binfo->bmiHeader.biWidth; }
	int getStorageWidth() const { return (getWidth() + 3) & ~3; }
	int getHeight() const
	{
		if(binfo->bmiHeader.biHeight < 0)
			return -binfo->bmiHeader.biHeight;
		else
			return binfo->bmiHeader.biHeight;
	}

	int getPlanes() const { return binfo->bmiHeader.biPlanes; }
	int getBitCount() const { return binfo->bmiHeader.biBitCount; }
	int getClrUsed() const { return binfo->bmiHeader.biClrUsed; }


	int getColours() const
	{
		if(getClrUsed() == 0)
			return 1 << getBitCount();
		else
			return getClrUsed();
	}

	LPRGBQUAD getColourTable() const { return binfo->bmiColors;	}

	HBITMAP getHandle() const { return handle; }
	void remap(LPRGBQUAD col);

private:
	void makeBitmapInfo(int width, int height);
};

#if !defined(EXCLUDE_DRAW_DIB)

/*
 * Class for DIBS that can be drawn in
 */

class DrawDIB : public DIB {
public:
	DrawDIB(int width, int height) : DIB(width, height) { }

	void hLineFast(LONG x, LONG y, LONG length, ColourIndex colour);
	void vLineFast(LONG x, LONG y, LONG length, ColourIndex colour);
	void hLine(LONG x, LONG y, LONG length, ColourIndex colour);
	void vLine(LONG x, LONG y, LONG length, ColourIndex colour);
	void rect(LONG x, LONG y, LONG width, LONG height, ColourIndex colour);
	void frame(LONG x, LONG y, LONG width, LONG height, ColourIndex colour);
	void frame(LONG x, LONG y, LONG width, LONG height, ColourIndex colour1, ColourIndex colour2);

	void circle(LONG x, LONG y, LONG radius, ColourIndex colour);
	void circle(LONG x, LONG y, LONG radius, ColourIndex colour1, ColourIndex colour2);
	void disc(LONG x, LONG y, LONG radius, ColourIndex colour);

	void line(LONG x1, LONG y1, LONG x2, LONG y2, ColourIndex colour);

	void plot(LONG x, LONG y, ColourIndex colour);

	PUCHAR getAddress(LONG x, LONG y);
	ColourIndex getColour(COLORREF cRef);

	void blitFrom(DIB* from);		// Complete copy

private:
	BOOL clip(LONG& x, LONG& y, LONG& width, LONG& height);
	UBYTE DrawDIB::getOutCode(LONG x, LONG y);
	Boolean clipLine(LONG& x1, LONG& y1, LONG& x2, LONG& y2);
};

#endif		// EXCLUDE_DRAW_DIB

#endif /* DIB_H */

