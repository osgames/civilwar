/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef FILEBASE_H
#define FILEBASE_H

#ifndef __cplusplus
#error filebase.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Virtual Class for File readers/writers
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1995/11/22 10:45:25  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "mytypes.h"

typedef ULONG SeekPos;
typedef size_t DataLength;

/*---------------------------------------------------------------
 * Generic File Classes
 */

class FileBase {
public:
	virtual Boolean seekTo(SeekPos where) = 0;
	virtual SeekPos getPos() = 0;
	virtual Boolean isAscii() = 0;
	virtual Boolean isOK() = 0;
};

class FileReader : public FileBase {
public:
	// virtual ~FileReader() = 0;
	virtual Boolean read(void* data, DataLength length) = 0;
	virtual Boolean read(void* data, DataLength length, DataLength& bytesRead) = 0;

#if !defined(EXCLUDE_FILEHELP)
	/*
	 * Asci helper functions
	 */

	virtual char* readLine() = 0;

	/*
	 * Binary helper functions
	 */

	char* getString();
	Boolean getUByte(UBYTE& b);
	Boolean getSByte(SBYTE& b) { return getUByte( (UBYTE&) b); }
	Boolean getUWord(UWORD& w);
	Boolean getSWord(SWORD& w) { return getUWord( (UWORD&) w); }
	Boolean getULong(ULONG& l);
	Boolean getSLong(SLONG& l) { return getULong( (ULONG&) l); }
#endif
};

class FileWriter : public FileBase {
	int indent;			// Used with ascii writer
public:
	FileWriter() { indent = 0; }
	// virtual ~FileWriter() = 0;
	virtual Boolean write(const void* data, DataLength length) = 0;

#if !defined(EXCLUDE_FILEHELP)
	/*
	 * Useful helper functions
	 */

	Boolean putString(const char* s);
	Boolean printf(const char* fmt, ...);

	Boolean putUByte(UBYTE b);
	Boolean putSByte(SBYTE b) { return putUByte(UBYTE(b)); }
	Boolean putUWord(UWORD w);
	Boolean putSWord(SWORD w) { return putUWord(UWORD(w)); }
	Boolean putULong(ULONG l);
	Boolean putSLong(SLONG l) { return putULong(ULONG(l)); }
#endif
};


#endif /* FILEBASE_H */

