/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef PALETTE_H
#define PALETTE_H

#ifndef __cplusplus
#error palette.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Palette Manager for Windows 95
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1995/10/17 12:03:08  Steven_Green
 * Identity palettes work
 *
 * Revision 1.1  1995/10/16 11:33:16  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

HPALETTE makeIdentityPalette(LPRGBQUAD aRGB, int nColors);

#endif /* PALETTE_H */

