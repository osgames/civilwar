/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MSGENUM_H
#define MSGENUM_H

#ifndef __cplusplus
#error msgenum.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Useful info for debug messages
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1996/06/22 19:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/11/14 11:25:57  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

const char* getWMname(int message);		// return "WM_CLOSE"... etc
const char* getWMdescription(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

const char* getNotifyName(int value);

#endif /* MSGENUM_H */

