/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef APP_H
#define APP_H

#ifndef __cplusplus
#error app.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Data Global To Application
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1996/06/22 19:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/11/07 10:39:54  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/10/16 11:33:16  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <windows.h>

class APP {
protected:
	HINSTANCE hinst;				// Global Instance
	LPSTR lpCmdLine;			// Command Line
	int nCmdShow;				// Initial show state
public:
	virtual ~APP() { }

	BOOL init(HINSTANCE inst, HINSTANCE prev, LPSTR cmd, int show);

	HINSTANCE getInstance() const { return hinst; }


private:
	virtual BOOL initApplication() = 0;
	virtual BOOL initInstance() = 0;
	virtual BOOL doCmdLine(LPCSTR cmd) { return TRUE; }
};

#endif /* APP_H */

