/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MAINWIND_H
#define MAINWIND_H

#ifndef __cplusplus
#error mainwind.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Main Window for Civil War Autorun
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "wind.hpp"

#define DISABLE_PLAY


class DIB;

class MainWindow : public WindowBase {
	static ATOM classAtom;
	static const char className[];

	HBITMAP bmp;			// Picture
	// DIB* dib;

	HBITMAP btOn; 				// Button bitmaps
	HBITMAP btOff;
	HBITMAP btDisabled;

	HANDLE installProcess;		// Used to keep track of DOS install program

	HWND instButton;
#if !defined(DISABLE_PLAY)
	HWND playButton;
#endif
	HWND quitButton;
	HWND readmeButton;
	HWND updateButton;

	char* gameDir;

public:
	MainWindow(int nCmdShow);
	~MainWindow();
	static ATOM registerClass();

private:
	LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	static LPCSTR getClassName() { return className; }

	void onDestroy(HWND hWnd);
	BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
	void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
	BOOL onQueryNewPalette(HWND hwnd);
	void onGetMinMaxInfo(HWND hWnd, LPMINMAXINFO lpMinMaxInfo);
	LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
	void onParentNotify(HWND hwnd, UINT msg, HWND hwndChild, int idChild);
	void onClose(HWND hWnd);
	void onPaint(HWND hWnd);
	void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem);

	HWND addButton(HWND parent, int id, int x, int y, UINT rid);
	void runInstallProgram();
	BOOL runGame();

	void getPlayDirectory();
};


#endif /* MAINWIND_H */

