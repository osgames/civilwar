/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef GRTYPES_H
#define GRTYPES_H

#ifndef __cplusplus
#error grtypes.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Useful types for Graphics Operations
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 14:08:48  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1996/02/16 17:34:14  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/11/22 10:45:25  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

typedef UBYTE ColourIndex;

class PixelPoint {
	// LONG x;
	// LONG y;
	POINT p;
public:
	PixelPoint() { }
	PixelPoint(LONG _x, LONG _y) { set(_x, _y); }
	PixelPoint(const POINT& pt) { p = pt; }

	void setX(LONG _x) { p.x = _x; }
	void setY(LONG _y) { p.y = _y; }
	void set(LONG _x, LONG _y) { p.x = _x; p.y = _y; }

	LONG getX() const { return p.x; }
	LONG getY() const { return p.y; }

	operator POINT() const { return p; }
	operator POINT&() { return p; }

	PixelPoint& operator = (const POINT& pt) { p = pt; return *this; }
};

#endif /* GRTYPES_H */

