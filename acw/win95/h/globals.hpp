/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef GLOBALS_H
#define GLOBALS_H

#ifndef __cplusplus
#error globals.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Include File for every source file!
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

/*
 * Disable unused parameter warnings
 */

#pragma off(unreferenced)

#define STRICT
#if defined(__NT__)
#define _WIN32
#endif

/*
 * Include Windows classes and include files
 */

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>

#include "wind.hpp"				// Base Windows Class
#include "app.hpp"				// Application Specific Data
#include "myassert.hpp"


#endif /* GLOBALS_H */

