/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef IPX_ASM_H
#define IPX_ASM_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Interface to IPX
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/05/04  22:12:01  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif


int IPX_install();
int IPX_call(UWORD function, void* buffer);

Socket IPX_openSocket(Socket socket);

inline IPX_GetInternetworkAddress(IPX_NetAddress *address)
{
	IPX_call(9, address);
}

inline IPX_sendPacket(ECB_Header* ecb)
{
	IPX_call(3, ecb);
}

inline IPX_listenForPacket(ECB_Header* ecb)
{
	IPX_call(4, ecb);
}

inline IPX_relinquishControl()
{
 	IPX_call(0x0a, 0);
}

void IPX_closeSocket(Socket socket);

#ifdef __cplusplus
};
#endif

#endif /* IPX_ASM_H */

