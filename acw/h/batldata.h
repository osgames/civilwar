/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef BATLDATA_H
#define BATLDATA_H

#ifndef __cplusplus
#error batldata.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Global Battle field data
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:12  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.15  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.9  1994/04/22  22:16:24  Steven_Green
 * Icon Handling Tidied Up
 *
 * Revision 1.1  1994/03/18  15:08:40  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#define STATS_INFO		// So can remove code if doesn't work

#ifndef BATEDIT

#include "gametime.h"
#include "data3d.h"
#include "gamelib.h"
#include "micelib.h"
#include "ob.h"
#include "battrack.h"

#ifdef OLD_TESTING
#include "gamedef.h"
#include "measure.h"
#endif

#ifdef DEBUG
#include "clog.h"
extern LogFile bLog;
#endif

/*
 * Undefined classes
 */

class MenuData;
class Icon;
class IconSet;
class SubMenu;
class ClockIcon;
class BattleControl;
class BattleMap;

class Terrain;
class MapGrid;
class SpriteLibrary;

class StaticObjectData;
class Sprite3DList;
class LineList;
class BattleDisplayList;

class Event;
class MessageWindow;
class DispCommandList;

class MarkedBattle;
class OrderBattle;
struct BattleCreate;
class AI_Battle;

class TemporaryBattleSpriteList;

class BattleSound;
class BattleControl;



/*
 * Values that can be returned from BattleData::play
 */

enum {
	BattleEndDay = 1,
	BattleSurrender,
	BattleNoTroops
};



/*
 * shared data used within battle game
 */

class BattleData {
friend class BattleControl;
friend class BattleOrderIcon;
friend class BattleSM1;

private:
	unsigned int lastCount;		// Last timer count
	Unit* lastMovedUnit;			// Unit that was moved last!
	SubMenu* subMenu;
	IconSet* viewIcons;
	OrderMode currentOrder;
	PointerIndex oldModePointer;
	MenuData* menuData;			// Controlling class
	Region* infoArea;				// Where to display info about Units
	Boolean mouseOnMap;
	enum { Tracking, GetDestination } trackMode;
	BattleLocation destination;
	Boolean gotDestination;
	PointerIndex oldZoomPointer;
	Boolean zooming;				// Set if in Magnify mode
	ZoomLevel newZoomLevel;		// zoom level to be set when zooming
	Boolean ownOb;
	Boolean CSAoffBattle;		// Cleared if anyone left on battlefield
	Boolean USAoffBattle;

	GameTicks nextRedraw;

	/*
	 * Statistics Information
	 */

	Boolean statsChanged;
	Strength currentStrength[2][3];		// [side][type]
	Strength losses[2][3];					// [side][type]

public:
	AI_Battle* ai;
	Location campaignLocation;			// Where on the campaign is this battle?
	OrderBattle* ob;
	GameTime gameTime;
	TimeInfo timeInfo;
	Sprite3DList* spriteList;
	StaticObjectData* localTrees;
	StaticObjectData* miscObjects;
	StaticObjectData* corpseObjects;
	// TemporaryBattleSpriteList* tempSprites;
	LineList* earthFace;
	BattleDisplayList* displayedObjects;
	DispCommandList* displayedCommanders;
	ClockIcon* clockIcon;
	MessageWindow* message;
	SpriteLibrary* spriteLib;
	BattleSound* sounds;
	System3D data3D;
	MapGrid* grid;
	BattleMap* mapWindow;
	Unit* currentUnit;							// Unit being tracked or ordered
	// PtrDList<Unit> unitList;					// List of units near mouse pointer
	TrackedUnitList unitList;
#ifdef DEBUG
	Boolean showDisplayInfo;
	Boolean gotMouseLocation;
	BattleLocation mouseLocation;
#endif

public:
	BattleData(OrderBattle* startOB);
	~BattleData();

	int play(Boolean noDeploy);

	void setTroops(OrderBattle* ob, MarkedBattle* batl);
	void makeBattleField(BattleCreate* parameters);
	void initTroops();
	
	void process(BattleControl* control);
	void drawBattleMap();
	void drawObjects(System3D& drawData);
	void overMap(Event* e, const Icon* icon, MenuData* d);

	void makeNewBattle();
#ifdef DEBUG
	President* makeTestSide(Side side, const Location& l);
#endif

	void zoomIn(ZoomLevel level, const BattleLocation& location);
	void zoomIn();
	void zoomOut();
	void updateView();
	void startZoomMode(ZoomLevel newLevel);
	void startZoomMode();
	void stopZoomMode();

	void OKOrder();
	void openOrders();
	void closeOrders();
	void sendOrder(const Location& location);

	void rxBattleOrder(UBYTE* packet);

	void runCAL();

#if defined(STATS_INFO)
	void statsUpdate(const Regiment* r, Strength loss);
	void statsUpdate() { statsChanged = True; }
#endif

	void prevStack();
	void nextStack();

private:
	void drawUnits(Unit* ob, System3D& drawData);
	void setLight();
#if defined(STATS_INFO)
	void showStats(Boolean force);
	void setupStats();
	void putStatWindow(Side side);
#else
	void clearInfo();
#endif
	void deploySide(President* p);
	void makeLocalTrees();
	void trackUnits(const Point& p);
	void chooseStackedObject();

};


extern BattleData* battle;

#else		// Battle Editor Version!

#include "data3d.h"
#include "gametime.h"
// #include "iconlib.h"

class BattleMap;
class Mapgrid;
class Sprite3DList;
class StaticObjectData;
class LineList;
class Event;
class Icon;
class MenuData;
class OrderBattle;
class SpriteLibrary;
class BattleDisplayList;
class DispCommandList;
class Unit;
class IconSet;

class BattleData {
protected:
	// Region* infoArea;				// Where to display info about Units
public:
	System3D data3D;
	MapGrid* grid;
	BattleMap* mapWindow;
	IconSet* viewIcons;
	Sprite3DList* spriteList;
	SpriteLibrary* spriteLib;
	StaticObjectData* localTrees;
	StaticObjectData* miscObjects;
	BattleDisplayList* displayedObjects;
	DispCommandList* displayedCommanders;
	LineList* earthFace;
	Unit* currentUnit;			// Unit being tracked or ordered
	GameTime gameTime;
	TimeInfo timeInfo;
	OrderBattle* ob;

	Boolean gotMouseLocation;
	BattleLocation mouseLocation;
public:
	BattleData();
	virtual ~BattleData();

	void drawObjects(System3D& drawData);
	void drawUnits(Unit* ob, System3D& drawData);
	virtual void overMap(Event* e, const Icon* icon, MenuData* d) = 0;

	void zoomIn(ZoomLevel level, const BattleLocation& location);
	void zoomIn();
	void zoomOut();
	void updateView();
	virtual void startZoomMode(ZoomLevel newLevel) = 0;
	virtual void startZoomMode() = 0;
	virtual void stopZoomMode() = 0;

	void makeBattleField();

private:
	void setLight();
	void makeLocalTrees();
};

extern BattleData* battle;

#endif		// BATEDIT

/*==========================================================
 * Constants
 */

const Distance BattleFieldSize = Mile(8);


inline Boolean onBattleField(const Location& l)
{
	return (abs(l.x) < BattleFieldSize/2) && (abs(l.y) < BattleFieldSize/2);
}

inline Boolean onMainBattleField(const Location& l)
{
	const Distance edge = BattleFieldSize/2 - Mile(1)/4;

	return (abs(l.x) < edge) && (abs(l.y) < edge);
}

inline Boolean onLargeBattleField(const Location& l)
{
	const Distance edge = BattleFieldSize/2 + Mile(1);

	return (abs(l.x) < edge) && (abs(l.y) < edge);
}

#endif /* BATLDATA_H */
