/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef LAYOUT_H
#define LAYOUT_H

#ifndef __cplusplus
#error layout.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Screen coordinates of things on screen
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

/*
 * Sub Menu Sizes and locations
 */

#define MENU_X			518
#define MENU_Y			0
#define MENU_WIDTH 	122		// Also 640-518
#define MENU_HEIGHT	424


#define INFO_X			518
#define INFO_Y			0
#define INFO_WIDTH	122
#define INFO_HEIGHT	312

// #define MENU_ICON_W	113
#define MENU_ICON_W	MENU_WIDTH
#define MENU_ICON_H	53

#define MENU_ICON_SIZE Point(MENU_ICON_W, MENU_ICON_H)

#define MENU_ICON_Y1	0
#define MENU_ICON_Y2	MENU_ICON_H			// 53
#define MENU_ICON_Y3	(MENU_ICON_H*2)	// 106
#define MENU_ICON_Y4	(MENU_ICON_H*3)	// 159
#define MENU_ICON_Y5	(MENU_ICON_H*4)	// 212
#define MENU_ICON_Y6	(MENU_ICON_H*5)	// 265
#define MENU_ICON_Y7	(MENU_ICON_H*6)	// 318
#define MENU_ICON_Y8	(MENU_ICON_H*7)	// 371

// #define MENU_ICON_X ((MENU_WIDTH - MENU_ICON_W)/2)
#define MENU_ICON_X 0

/*
 * Side Icons on Main Menu
 */


#define SIDE_ICON_W 120
#define SIDE_ICON_H 105
#define SIDE_ICON_X 518
#define SIDE_ICON_Y1 2
#define SIDE_ICON_Y2 109
#define SIDE_ICON_Y3 216
#define SIDE_ICON_Y4 323


/*
 * Icons at bottom
 */

#define MAIN_ICON_Y 372
#define MAIN_ICON_X1 0
#define MAIN_ICON_X2 129
#define MAIN_ICON_X3 258
#define MAIN_ICON_X4 387
#define MAIN_ICON_X5 516

#define MAIN_ICON_W 129
#define MAIN_ICON_H 107

/*
 * Campaign Maps
 */

#define ZoomX 16
#define ZoomY 0
#define ZoomW 486
#define ZoomH 331

#define MapX 0			// Location of map window on physical screen
#define MapY 22
#define MapW 518
#define MapH 350

#define FullX 16
#define FullY 0
#define FullW 486
#define FullH 350

#define BMapX 16
#define BMapY 19
#define BMapW 486
#define BMapH 312

#define HScrollX 16
#define HScrollY 331
#define HScrollW 486
#define HScrollH 19

#define VScrollX1 0
#define VScrollX2 502
#define VScrollY 19
#define VScrollW 16
#define VScrollH 312

#define VBorderX1 0
#define VBorderX2 502
#define VBorderY 0
#define VBorderW 16
#define VBorderH 350


/*
 * Message Window area
 */

#define MSGW_X 0
#define MSGW_Y 0
#define MSGW_W 518
#define MSGW_H 22

#endif /* LAYOUT_H */

