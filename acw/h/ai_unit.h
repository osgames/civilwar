/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef AI_UNIT_H
#define AI_UNIT_H

#ifndef __cplusplus
#error ai_unit.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Unit AI class Definition
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:12  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

class Unit;
class Facility;

/*
 * Actions that AI units can be carrying out
 * If changed... Update actionNames[] in ai_unit
 */

enum AI_Action {
	AIA_None,
	AIA_AttackTown,
	AIA_DefendTown,
	AIA_AttackUnit,
	AIA_ReinforceUnit
};

#ifdef DEBUG
extern const char* actionNames[];
#endif

/*
 * Extra Variables for units needed by Campaign AI
 */

class UnitAI {
public:
	union {

		/*
		 * These members used for units on AI side
		 */

		struct {
			AI_Action action;
			UBYTE priority;
			Facility* where;			// Where are we attacking?
			Unit* who;					// Who are we attacking?

			void clear()
			{
				action = AIA_None;
				priority = 0;
				where = 0;
				who = 0;
			}
		} ai;

		/*
		 * These members when on player's side
		 */

		struct {
		} player;

#if defined(NOBITFIELD)
		Boolean needAction;			// Set if action needed (reinforce or attack)
#else
		Boolean needAction:1;			// Set if action needed (reinforce or attack)
#endif
	};

public:

	/*-----------------------------------------------
	 * Following used by Battle AI and shouldn't really
	 * be here!  Use a variable in UnitBattle instead (unit3d.h)
	 */

public:
#if defined(NOBITFIELD)
	Boolean ToBeAttacked;
#else
	Boolean ToBeAttacked:1;
#endif
};

#endif /* AI_UNIT_H */

