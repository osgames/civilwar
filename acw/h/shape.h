/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SHAPE_H
#define SHAPE_H

#ifndef __cplusplus
#error shape.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Base Class for 2D Shapes
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.4  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.3  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/11/26  22:32:24  Steven_Green
 * virtual destructor added to shape3d
 *
 * Revision 1.1  1993/11/24  09:34:12  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

class Region;
class ObjectDrawData;

class Shape {
public:
	virtual ~Shape() { };
	virtual void render(Region* bm, ObjectDrawData* d) const { bm=bm; d=d; };		// Put it on the screen
};

#endif /* SHAPE_H */

