/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef GAMEDEF_H
#define GAMEDEF_H

#ifndef __cplusplus
#error gamedef.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Miscellaneous Game Definitions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.8  1994/09/23  13:29:31  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/07/28  19:01:36  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/07/25  20:34:58  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/07/13  13:52:35  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/06/07  18:33:23  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/03/15  15:17:16  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

typedef UBYTE AttributeLevel;
#define MaxAttribute 0xff

typedef UWORD FacilityID;		// Assume less than 65536 facilities
#define NoFacility 0xffff		// Value to represent illegal value

typedef UWORD RailSectionID;
#define NoRailSection 0xffff

typedef UWORD FacilityListID;
typedef UWORD WaterZoneListID;

typedef UWORD WaterZoneID;
#define NoWaterZone 0xffff


typedef ULONG Strength;			// Number of men!
typedef UWORD RegimentStrength;
typedef ULONG CombatValue;

typedef AttributeLevel Fatigue;

enum FatigueEnum {
	Fresh,
	Weary,
	Tired,
	Exhausted,

	Fatigue_Max
};

typedef AttributeLevel Morale;

enum MoraleEnum {
	Routing,
	Demoralised,
	Dispirited,
	Normal,
	Good,
	High,

	Morale_Max
};

typedef AttributeLevel Experience;

enum ExperienceEnum {
	Untried,
	Tried,
	Seasoned,
	Veteran,
	BattleWeary,

	Experience_Max
};

/*
 * Rank Enumeration
 */

enum Rank_enum {
	Rank_President,
	Rank_Army,
	Rank_Corps,
	Rank_Division,
	Rank_Brigade,
	Rank_Regiment
};

typedef UBYTE Rank;

inline Rank promoteRank(Rank r) { return Rank(r-1); }
inline Rank demoteRank(Rank r) { return Rank(r+1); }

enum Realism { Simple, Average, Complex1, Complex2, Complex3, RealismLevels };

#endif /* GAMEDEF_H */

