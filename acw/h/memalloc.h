/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MEMALLOC_H
#define MEMALLOC_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Memory Allocation to replace malloc/free
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

// #define SYSTEM_MALLOC
// #define NO_DOS			// Don't use DOS memory

#if !defined(SYSTEM_MALLOC)

#ifndef _STDDEF_H_INCLUDED
#include <stddef.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

void* malloc(size_t amount);
void free(void* ad);

/*
 * Memory Information set up on 1st call to malloc
 */

typedef struct {
	unsigned long totalPhysical;
	unsigned long freePhysical;
	unsigned long freeDOS;
} MemInfo;

extern MemInfo memInfo;

#ifdef DEBUG
void showPoolInfo();
#endif


#ifdef DEBUG
extern size_t currentAllocated;
extern size_t maximumAllocated;
extern long mallocCalls;
extern long freeCalls;
#endif

#ifdef __cplusplus
};
#endif

#endif	// SYSTEM_MALLOC

#endif /* MEMALLOC_H */

