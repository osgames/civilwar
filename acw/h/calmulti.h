/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CALMULTI_H
#define CALMULTI_H

#ifndef __cplusplus
#error calmulti.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	CAL Multiplayer functions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"
#include "con_msg.h"

class OrderBattle;
class Unit;
class General;
class Packet;


void txCALTransferUnit(OrderBattle* ob, Unit* unit, Unit* dest);
void txCALTransferGeneral(OrderBattle* ob, General* general, Unit* dest);
void txCALCal(OrderBattle* ob, PacketType type, Unit* unit);
void txCALMakeNew(OrderBattle* ob, Unit* unit, General* general);

void rxCALTransferUnit(Packet* packet);
void rxCALTransferGeneral(Packet* packet);
void rxCALMakeNew(Packet* packet);

#if !defined(BATEDIT) && !defined(TESTBATTLE) && !defined(CAMPEDIT)
void rxCALRejoinCampaign(Packet* packet);
void rxCALReattachCampaign(Packet* packet);
void rxCALReattachAllCampaign(Packet* packet);
#endif

#if !defined(CAMPEDIT) && !defined(TESTCAMP) && !defined(BATEDIT)
void rxCALRejoinBattle(Packet* packet);
void rxCALReattachBattle(Packet* packet);
void rxCALReattachAllBattle(Packet* packet);
#endif


#endif /* CALMULTI_H */

