/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef STATICOB_H
#define STATICOB_H

#ifndef __cplusplus
#error staticob.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Static Objects on Battle Map
 *
 * Also moveable objects!
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/09/23  13:29:31  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/09/02  21:27:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/08/31  15:26:29  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types3d.h"
#include "batlib.h"
#include "sprlist.h"

#ifdef BATEDIT
class Region;
#endif

class ViewPoint;

enum StaticSpriteType {
	ChurchObject,
	Church45Object,
	HouseObject,
	House45Object,
	TavernObject,
	Tavern45Object,
	Church1Object,
	Church451Object,
	House1Object,
	House451Object,
	Tavern1Object,
	Tavern451Object,
	BuildingCount,

	Tree1Object = BuildingCount,
	Tree2Object,
	Tree3Object,
	Tree4Object,
	Tree5Object,
	Tree6Object,

	DeadHorse,
	DeadArtillery,
	DeadU,
	DeadC,

	TentObject,

	StaticSpriteTypeCount
};

/*
 * structures used by load/save game
 */

enum StaticObjectType {
	SO_None,
	SO_Tree,
	SO_Corpse
};

struct ObjectSaveData {
	UBYTE type;
	BattleLocation where;
	StaticSpriteType what;
	Qangle facing;
};

/*
 * Battle Object should be a virtual class
 * Each object can have its own routines to draw and move, etc.
 *
 * The draw routine will usually add sprites to battle->spriteList;
 */

class BattleObject {
	friend class StaticObjectList;
	friend class StaticObjectData;
	friend class StaticObjectIter;

	BattleObject* next;

#ifdef BATEDIT
public:
	UBYTE highlight;
	UBYTE animation;

	virtual void showInfo(Region* r) = 0;
#endif

public:
	BattleLocation where;

public:
	BattleObject(const BattleLocation& p)
	{
		where = p;
#ifdef BATEDIT
		highlight = 0;
#endif
	}

	virtual ~BattleObject() { }
	virtual void draw(const ViewPoint& view) = 0;
	virtual void reset() = 0;
	virtual void getSaveData(ObjectSaveData& data);
};

/*
 * This should be implemented using a sperate list for each grid of the
 * play area.  This should speed things up considerably!
 */

class StaticObjectList {
	friend class StaticObjectIter;

	BattleObject* entry;

public:
	StaticObjectList();
	~StaticObjectList();

	void add(BattleObject* ob);

	void draw(const ViewPoint& view);
	void reset();
	void clear();
#ifdef BATEDIT
	void remove(BattleObject* ob);
#endif
};

#define ObjectGridSize 16		// 16 Grids in area

class StaticObjectData {
	friend class StaticObjectIter;

	StaticObjectList grids[ObjectGridSize][ObjectGridSize];

public:
	StaticObjectData();
	~StaticObjectData();

	void add(BattleObject* ob);

	void draw(const ViewPoint& view);
	void reset();
	void clear();
#ifdef BATEDIT
	void makeRandom();
	void remove(BattleObject* ob);
#endif
};

class StaticObjectIter {
	int x;
	int y;
	BattleObject* ob;
	StaticObjectData* owner;
public:
	StaticObjectIter(StaticObjectData* obData);
	BattleObject* next();
	void reset();
};


class TreeObject : public BattleObject {
	Sprite3D sprite;
	SpriteID id;

#ifdef BATEDIT
public:
	void showInfo(Region* r);
#endif
	Qangle facing;
public:
	StaticSpriteType what;
public:
	TreeObject(StaticSpriteType n, const BattleLocation& p, Qangle facing);
	~TreeObject();

	void draw(const ViewPoint& view);
	void reset();
	void getSaveData(ObjectSaveData& data);
};


#endif /* STATICOB_H */

