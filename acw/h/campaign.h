/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CAMPAIGN_H
#define CAMPAIGN_H

#ifndef __cplusplus
#error campaign.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Global Variables and general routines
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.24  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.1  1994/01/17  20:15:30  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */


#ifndef CAMPEDIT

#include "gametime.h"
#include "moblist.h"
#include "campmode.h"
#include "mapwind.h"
#include "gamedef.h"
#include "side.h"

class CampaignAI;
class MessageWindow;
class ClockIcon;
class SubMenu;
class CampaignWorld;
class OrderBattle;
class MarkedBattle;
class Unit;
class MenuData;
class Facility;
class WaterZone;

class AlertBase {
public:
	virtual ~AlertBase() = 0;

	virtual int doAlert(const char* text, const char* buttons, int defaultButton, Facility* f, WaterZone* w, MarkedBattle* mb) = 0;
};

struct MapSettings {
	MapWindow::MapStyle style;
	Location centre;
	CampaignMode::CM_Mode mode;

	MapSettings();
};

/*
 * How has the game been won?
 */

enum CampWinHow {
	TimeOut,		// Lincoln re-elected
	BigTimeOut,	// Huge Victory at re-election time
	Victory		// Took over land
};

/*
 * New improved Campaign structure for cleaner loop
 */

class Campaign {

private:
	unsigned int lastCount;		// Timer value last time campaign processed
	char* workFileName;			// Name of file saved when playing a battle
public:
#if defined(NOBITFIELD)
	Boolean givingOrders;			// Set when in order mode, clear in movement mode
#else
	Boolean givingOrders:1;			// Set when in order mode, clear in movement mode
#endif
	CampaignWorld* world;					// World data (facilities, troops, etc)
	CampaignAI* aiControl[SideCount];	// Used by AI
	MapWindow* mapArea; 						// The map being displayed
	MapSettings mapSettings;				// Settings for the map display
	MobIList tempObjects;					// Temporary objects to display on the map
	SubMenu* subMenu;							// sub menu area used for mobilising and ordering
	MessageWindow* msgArea;					// Area for in-game messages
	Region* infoArea;							// Where to display info about cities (same as submenu)
	AlertBase* alertArea;					// How to do dial Alerts
	MenuData* interface;						// Controlling interface class

	GameTime gameTime;						// Time as seconds since Jan 1st 1800
	TimeInfo timeInfo;						// Time broken down into hours/mins/etc
	ClockIcon* clockIcon;					// Clock Icon

	Boolean newDay;							// Set for 1 game frame after day end
	Boolean showEmptyWaterZones;

	ULONG CSAVictory;
	ULONG USAVictory;

	UBYTE CSAWinVictory;
	UBYTE USAWinVictory;
	UBYTE CSAStartVictory;

public:
	Campaign();
	~Campaign();

	void init(const char* fileName);
	void smallSetup();
	int runCampaign();

	void beforeLoad();
	void afterLoad();

	void ordersPhase();
	void movementPhase(Boolean inBattle);

	void drawObjects(MapWindow* map);

	void finishOrders();

	void clearInfo();
	void updateMap();

#ifdef CHRIS_AI
	Boolean CheckAI( Unit* u );
#endif
	// Side getAISide() const;

	void rxOrder(UBYTE* packet);
	void rxFleet(UBYTE* packet);
	void rxMobilise(UBYTE* packet);

	int doAlert(const char* text, const char* buttons, int defaultButton, Facility* f, WaterZone* w, MarkedBattle* mb);

	void getMapSettings();

#if !defined(TESTCAMP) && !defined(CAMPEDIT)
	char* getWorkFileName() const { return workFileName; }
	char* makeWorkFileName();

	void afterBattle(OrderBattle* battleOB);
#endif

private:
	void drawUnits(MapWindow* map, Unit* ob);
	Boolean EndGameDetection();
	void doEndGame(Side winner, CampWinHow how);

	void setVictory();

#if !defined(TESTCAMP) && !defined(CAMPEDIT)
	OrderBattle* beforeBattle(MarkedBattle* batl);
#endif
	void calculateBattle(MarkedBattle* batl);
};


/*
 * Global variable
 */

extern Campaign* campaign;

#else	// CAMPEDIT

#include "gametime.h"

class CampaignWorld;
class MapWindow;
class Unit;

class Campaign {
public:
	CampaignWorld* world;
	MapWindow* mapArea; 			// The map being displayed

	GameTime gameTime;			// Time as seconds since Jan 1st 1800
	TimeInfo timeInfo;			// Time broken down into hours/mins/etc

	Boolean showEmptyWaterZones;

public:
	Campaign();
	virtual ~Campaign();
	void init();

	void display();
	void drawObjects(MapWindow* map);

private:
	void drawUnits(MapWindow* map, Unit* ob);
};


/*
 * Global variable
 */

extern Campaign* campaign;

#endif	// CAMPEDIT


#endif /* CAMPAIGN_H */
