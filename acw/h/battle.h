/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef BATTLE_H
#define BATTLE_H

#ifndef __cplusplus
#error battle.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battle Game definition
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/03/18  15:08:40  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/03/17  14:30:48  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/02/28  23:05:43  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

class MarkedBattle;
class OrderBattle;



#endif /* BATTLE_H */

