/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef RESOURCE_H
#define RESOURCE_H

#ifndef __cplusplus
#error resource.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Resource Management
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.6  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/06/09  23:36:46  Steven_Green
 * Added writeData()
 *
 * Revision 1.4  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.2  1994/05/25  23:33:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/05/21  13:18:12  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

typedef UWORD ResourceVal;
#define MaxResource 0x1000 			//  0x3fff

enum ResourceType {
	R_Human,
	R_Horses,
	R_Food,
	R_Materials,

	ResourceCount
};

class ResourceSet {
public:
	ResourceVal values[ResourceCount];

	ResourceSet(ResourceVal human, ResourceVal horse, ResourceVal food, ResourceVal raw);
	ResourceSet();

#ifndef CAMPEDIT
	ResourceSet operator / (unsigned int divisor) const;
	ResourceSet& operator /= (unsigned int divisor);
	ResourceSet& operator += (const ResourceSet& set);
	ResourceSet& operator -= (const ResourceSet& set);
	ResourceSet operator + (const ResourceSet& set) const;

	void multiplyRatio(int top, int bottom);

	void adjust();

	int canAfford(const ResourceSet* local, const ResourceSet* imported) const;
	void takeCost(ResourceSet* local, ResourceSet* imported, int howMany = 1) const;

	void addTax(ResourceSet& from, unsigned short proportion);

	ResourceVal getValue(ResourceType type) const;
	static ResourceVal getMaxVal(ResourceType type);
#ifdef DEBUG
	void logPrint();
	void print(char* buffer);
#endif
#endif	// CAMPEDIT
};



#endif /* RESOURCE_H */

