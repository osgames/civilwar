/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef GAMELIB_H
#define GAMELIB_H

#ifndef __cplusplus
#error gamelib.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Enumeration of sprites in game.spr
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "gamespr.h"

enum GameSprites {
	/*
	 * Main Menu Icons From icon_1
	 */

	MM_Campaign = SPR_icon_1,
	MM_Battle,
	MM_SinglePlayer,
	MM_MultiPlayer,
	MM_SideUSA,
	MM_SideCSA,
	MM_Load,
	MM_Save,
	MM_Database,
	MM_Dos,
	MM_MainMenu,
	MM_Music,
	MM_BigOK,
#if 0
	// MM_BigHelp,
	MM_MiniOK,
	MM_MiniLogo,
	MM_MainMenuGerman,
	MM_About,
#else
	MM_MainMenuGerman,
	MM_MiniOptions,
	MM_MiniOK,
	MM_MiniLogo,
	MM_MiniMusic,
	MM_About,
#endif

	// MM_InfoBar,

	// From icon_2

	RS_Diff1 = SPR_icon_2,
	RS_Diff2,
	RS_Diff3,
	RS_Diff4,
  	RS_Diff5,
	RS_Diff1b,
	RS_Diff2b,
	RS_Diff3b,
	RS_Diff4b,
  	RS_Diff5b,

	RS_DiffS1,
	RS_DiffS2,
	RS_DiffS3,
	RS_DiffS4,
  	RS_DiffS5,
	RS_DiffS1b,
	RS_DiffS2b,
	RS_DiffS3b,
	RS_DiffS4b,
  	RS_DiffS5b,

	RS_Resource,
	RS_Supply,
	RS_RegType,
	RS_Facilities,
	RS_Command,
	RS_Fatigue,
	RS_Terrain,
	RS_Morale,
	RS_Victory,

	// From Icon_3

	C1_MobInfantry = SPR_icon_3,
	C1_MobCavalry,
	C1_MobArtillery,
	C1_MobSupply,
	C1_MobNaval,
	C1_BuildFacilities,
	C1_Promote,

	C_SelectMap,

	C2_Advance,
	C2_Cautious,
	C2_Attack,
	C2_Stand,
	C2_Hold,
	C2_Defend,
	C2_Withdraw,
	C2_Retreat,
	C2_HastyRetreat,
	
	C2_AnyMove,
	C2_Land,
	C2_Rail,
	C2_Water,

	C2_MiniOK,
	C2_EndBattle,
	C2_TinyCal,
	C2_TinyStats,

	C2_BigRail,
	C2_BigWater,

	/*
	 * CityInfo screen
	 */

	CI_Industrial1 = SPR_cityinfo,
	CI_Industrial2,
	CI_Industrial3,
	CI_Industrial4,
	CI_Agricultural1,
	CI_Agricultural2,
	CI_Agricultural3,
	CI_Agricultural4,
	CI_Mixed1,
	CI_Mixed2,
	CI_Mixed3,
	CI_Mixed4,
	CI_KeyCity,
	CI_RailFacility,
	CI_FortFacility,
	CI_BoatFacility,

	CI_Port,

	CI_Fort1,
	CI_Fort2,
	CI_Fort3,
	CI_Fort4,
	CI_RailHead,
	CI_Recruitment,
	CI_Training,
	CI_Hospital,
	CI_POW,
	CI_Depot,
	CI_BuildWagon,
	CI_Wagon,
	CI_Resource1,
	CI_Resource2,
	CI_Resource3,
	CI_Resource4,
	CI_Infantry,
	CI_Cavalry,
	CI_Artillery,
	CI_Naval,

	/*
	 * Little Attribute Icons
	 */


	AT_Strength,
	AT_Morale,
	AT_Fatigue,
	AT_Supply,
	AT_Experience,
	AT_Efficiency,
	AT_Aggression,
	AT_Ability,


	/*
	 * ArmyInfo
	 */

	AI_Advance = SPR_armyinfo,
	AI_Cautious,
	AI_Attack,
	AI_Stand,
	AI_Hold,
	AI_Defend,
	AI_Withdraw,
	AI_Retreat,
	AI_HastyRetreat,
	AI_DontKnow,			// A replacement for one of the orders on the battlefield?

	/*
	 * From cpsm1rc2 (Campaign Resource Icons)
	 */

	C1_InfReg = SPR_cpsm1rc2,
	C1_InfMil,
	C1_Sharpshooter,
	C1_Engineer,
	C1_RailEngineer,

	C1_CavReg,
	C1_CavMil,
	C1_CavRegMounted,
	C1_CavMilMounted,
	C1_CavRaiders,

	C1_Smoothbore,
	C1_ArtLight,
	C1_ArtRifled,
	C1_ArtSiege,

	C1_Naval,
	C1_NavalIronClad,
	C1_Riverine,
	C1_RiverineIronClad,
	C1_Blockade,

	C1_Fortification,
	C1_SupplyDepot,
	C1_Recruitment,
	C1_Training,
	C1_Railhead,
	C1_Hospital,
	C1_POW,
	C1_Railway,


	/*
	 * From CALicon
	 */

	I_CAL_Infantry = SPR_calicon,
	I_CAL_Cavalry,
	I_CAL_Artillery,
	I_CAL_New,
	I_CAL_Left,
	I_CAL_Right,

	I_CAL_Army1,
	I_CAL_Army2,
	I_CAL_Corps1,
	I_CAL_Corps2,
	I_CAL_Division1,
	I_CAL_Division2,
	I_CAL_Brigade1,
	I_CAL_Brigade2,

	I_CAL_Army1U,
	I_CAL_Army2U,
	I_CAL_Corps1U,
	I_CAL_Corps2U,
	I_CAL_Division1U,
	I_CAL_Division2U,
	I_CAL_Brigade1U,
	I_CAL_Brigade2U,

	/*
	 * Flags for campaign map
	 */

	SPR_ArmyFlag1 = SPR_flags,		// USA
	SPR_ArmyFlag2,		// USA Highlighted
	SPR_ArmyFlag3,		// CSA
	SPR_ArmyFlag4,		// CSA Highlighted
	SPR_CorpsFlag1,
	SPR_CorpsFlag2,
	SPR_CorpsFlag3,
	SPR_CorpsFlag4,
	SPR_DivisionFlag1,
	SPR_DivisionFlag2,
	SPR_DivisionFlag3,
	SPR_DivisionFlag4,
	SPR_BrigadeFlag1,
	SPR_BrigadeFlag2,
	SPR_BrigadeFlag3,
	SPR_BrigadeFlag4,
	SPR_Naval1,			// Used Normal
	SPR_Naval2,			// Used Highlighted
	SPR_Naval3,			// Silouette Normal
	SPR_Naval4,			// Silouette Highlighted

	/*
	 * Clock graphics
	 */

	CLOCK_Face = SPR_clock,
	CLOCK_Day,
	CLOCK_Day_Night,
	CLOCK_Night,
	CLOCK_Night_Day,

	/*
	 * Sub menu stuff
	 */

	AI_Outer = SPR_submenu,

	// SM_OuterUSA = SPR_submenu,
	// SM_OuterCSA,
	// AI_Outer,
	// CI_Outer,

	/*
	 * Fill Patterns
	 */

	FillPattern_Flag = SPR_fill,
	FillUSA_Colour,
	FillCSA_Colour,
	FillUSA_Grey,
	FillCSA_Grey,
	LogoSprite,
	TinyFlagUSA,
	TinyFlagCSA,



	GameLibCount,


	/*
	 * Undrawn icons
	 */

	C_City = C1_BuildFacilities,
	C_Troop = C1_InfReg,


};
#endif /* GAMELIB_H */

