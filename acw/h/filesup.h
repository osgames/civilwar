/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef FILESUP_H
#define FILESUP_H

#ifndef __cplusplus
#error filesup.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Support for Chunk Files
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "filedata.h"

class Location;
class BattleLocation;
// class GameTime;
class TimeBase;

struct D_Location {
	ILONG x;
	ILONG y;
};

struct D_BattleLocation {
	ILONG x;
	ILONG y;
};

void putLocation(D_Location* dest, const Location& l);
void getLocation(D_Location* src, Location& l);

void putBattleLocation(D_BattleLocation* dest, const BattleLocation& l);
void getBattleLocation(D_BattleLocation* src, BattleLocation& l);

/*
 * Game Time structure
 */

struct D_GameTime {
	IWORD day;				// Time
	ILONG ticks;
};

void putGameTime(D_GameTime* dest, const TimeBase& t);
void getGameTime(D_GameTime* src, TimeBase& t);

#endif /* FILESUP_H */

