/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SAVEGAME_H
#define SAVEGAME_H

#ifndef __cplusplus
#error savegame.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Save Game Files
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

class Campaign;
class BattleData;
class MenuData;

void saveCampaignGame(const char* fileName, Campaign* campaign);
void saveBattleGame(const char* fileName, BattleData* battle);

int playSavedGame(const char* loadName);

const char* getSaveGameWildCard();

#endif /* SAVEGAME_H */

