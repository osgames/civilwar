/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SER_CFG_H
#define SER_CFG_H

#ifndef __cplusplus
#error ser_cfg.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Modem and Serial setup dialogues
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "serial.h"

enum CommDialID {
	CI_None,
	CI_Com1,
	CI_Com2,
	CI_Com3,
	CI_Com4,
	CI_Com1IRQ,
	CI_Com2IRQ,
	CI_Com3IRQ,
	CI_Com4IRQ,
	CI_Baud,
	CI_OK,
	CI_Cancel
};

struct SerialConfig {
	BaudRate baud;
	UWORD ioPort;
	UBYTE irq;
	PortType portType;

	CommDialID getSerialConfig();
};

enum ModemDialID {			// Results from modemSetupItems[]
	MDI_None,
	MDI_Dial,
	MDI_Answer,
	MDI_Cancel
};

ModemDialID getModemConfig(char** initString, char** dialString, char** answerString, char** phoneString);

#endif /* SER_CFG_H */

