/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SCANBUF_H
#define SCANBUF_H

#ifndef __cplusplus
#error scanbuf.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Scan Line Z Buffering
 *
 * The screen is pre-calculated with each scan line containing a list
 * of horizontal lines.
 *
 * Each line segment includes information about:
 *   x1..x2 (where it is)
 *   z1..z2 (Z Coordinate)
 *   i1..i2 (Intensity)
 *   colour (Material Type)
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.5  1994/09/23  13:29:31  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/09/02  21:27:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/08/31  15:26:29  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/08/24  15:07:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/08/19  17:30:21  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "terrain.h"

class System3D;

/*
 * A segment (takes up 16 bytes)
 * Z coordinate is truncated to 16 bits
 */

struct ScanSegment {
	struct {
		SDimension x;				// 2 bytes
		WORD z;						// 2 bytes -> 6 bytes * 2 = 12 bytes
		Intensity i;				// 2 bytes
	} from, to;
	TerrainType material;		// 1 bytes => 13 bytes
};

struct ScanSegmentList {
	UWORD next;					// Linked list
	ScanSegment seg;
};

// #define MaxScanSegments (340*32)	// Nearly 200k of memory required!!!
#define MaxScanSegments (340*64)	// Nearly 200k of memory required!!!

#define NoScanSegment 0xffff

class ScanBuffer {
	ScanSegmentList* segments;
	UWORD* sections;	 		 	// Segments used per line
	UWORD freeSegment;
	SDimension height;
	Boolean paletteSet;			// Set if the palette has been created

public:
	 ScanBuffer();
	~ScanBuffer();

	void init(SDimension h);

	void clear();
	void add(SDimension y, ScanSegment* seg);
	void draw(System3D& drawData);
	void blitSprite(Region* region, const Sprite* sprite, const Point3D& pt) const;

	Boolean findPoint(const Point& p, Point3D& result);
private:
	void putLineSeg(UWORD* info, ScanSegment* seg);
	void putLineSegQuick(UWORD* info, ScanSegment* seg, ScanSegmentList* prev);
};


#endif /* SCANBUF_H */

