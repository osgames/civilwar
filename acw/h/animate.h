/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef ANIMATE_H
#define ANIMATE_H

#ifndef __cplusplus
#error animate.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Sprite Animation Definition
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:12  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1993/12/21  00:34:28  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifndef SPRLIB_H
#include "sprlib.h"
#endif

typedef UBYTE SpriteSet;
typedef UBYTE Animation;
typedef UBYTE AnimCycle;


class SprAnimate : SpriteLibrary {
public:
	SprAnimate(const char* name), SpriteLibrary(name) { };
	

	Sprite3D* get(SpriteSet set, SDimension height, Wangle view, Animation anim, AnimCycle cycle);
	void release(Sprite3D* sprite);
};

#endif /* ANIMATE_H */

