/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef BATFILE_H
#define BATFILE_H

#ifndef __cplusplus
#error batfile.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Reading and writing of battlegame save game and historical battles
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:12  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

class BattleData;
class WriteWithPopup;
class ReadWithPopup;

void writeBattleGame(WriteWithPopup& file, BattleData* battle);
void writeHistoricalBattle(const char* fileName, BattleData* battle);
void readBattleGame(ReadWithPopup& file, BattleData* battle);
void readHistoricalBattle(const char* fileName, BattleData* battle);

#endif /* BATFILE_H */

