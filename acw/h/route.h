/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef ROUTE_H
#define ROUTE_H

#ifndef __cplusplus
#error route.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Route Planner
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "orders.h"
#include "myassert.h"
#include "side.h"

/*
 * Undefined classes / structures
 */

class RouteQueue;
struct CampaignTerrainCoord;
class MapWindow;
class Region;

// #define DO_RAIL_WATER
// #define USE_LINEMODE		// Allow Line mode

/*
 * Route Cost
 */

typedef UWORD RouteCost;

#define MaxNodeCost 0x1fff
#define InQueue		0x8000
#if defined(DO_RAIL_WATER)
#define HasEnemy		0x4000
#define HasTransport	0x2000
#endif

struct RouteNode {
	UWORD value;

	void init() { value = MaxNodeCost; }

	RouteCost getCost() const { return value & MaxNodeCost; }
	void setCost(RouteCost cost)
	{
		ASSERT(cost <= MaxNodeCost);

		value = (value & ~MaxNodeCost) + cost;
	}

#if defined(DO_RAIL_WATER)
	void setTransport() { value |= HasTransport; }
	Boolean getTransport() const { return (value & HasTransport) != 0; }

	void setEnemy() { value |= HasEnemy; }
	Boolean getEnemy() const { return (value & HasEnemy) != 0; }
#endif

	void setInQueue() { value |= InQueue; }
	void clearInQueue() { value &= ~InQueue; }
	Boolean getInQueue() { return (value & InQueue) != 0; }
};


class RoutePlan {
private:
	RouteNode* nodes;
	int width;
	int height;
#ifdef DEBUG
	int timeTaken;
	int iterations;
	RouteCost maxCost;
	int dupCount;		// Savings made by having inQueue flag
#endif
#if defined(DO_RAIL_WATER)
	Boolean allowRail;
	Boolean allowWater;
	OrderHow moveMethod;
	Side side;
	Boolean allowSide[4];
#endif

public:
	Location local;			// Next location to walk to.
#if defined(DO_RAIL_WATER)
	OrderHow how;				// What happens at destination (e.g. rail/water).
	FacilityID on;				// Where we get onto rail/water
	FacilityID off;			// Where we get off rail/water
#endif

public:
	RoutePlan();
	~RoutePlan();
	void init();

	void planRoute(const Location& from, const Location& to, OrderHow how, Side allowSide);
	void mapDraw(MapWindow* map, Region* bm, const Location& from, const Location& to);

private:
	RouteNode* getNode(UWORD x, UWORD y)
	{
		ASSERT(x < width);
		ASSERT(y < height);
		return &nodes[x + y * width];
	}

	void consider(RouteQueue& queue, long x, long y, RouteCost cost);
	void getBest(RouteCost& best, CampaignTerrainCoord& bestTCord, long x, long y);
	Boolean complexRoute(const CampaignTerrainCoord& fromTCord, const CampaignTerrainCoord& destTCord);
	Boolean getNextPoint(const CampaignTerrainCoord& fromTCord, const CampaignTerrainCoord& destTCord);
	void getWayPoint(const CampaignTerrainCoord& fromTCord, const CampaignTerrainCoord& destTCord, CampaignTerrainCoord& result);
#ifdef USE_LINEMODE
	Boolean lineRoute(const CampaignTerrainCoord& from, const CampaignTerrainCoord& to, CampaignTerrainCoord& result);
#ifdef DEBUG
	Boolean _lineRoute(const CampaignTerrainCoord& from, const CampaignTerrainCoord& to, CampaignTerrainCoord& result);
#endif
#endif
};


#ifdef DEBUG
#include "clog.h"
extern LogFile routeLog;
#endif

#endif /* ROUTE_H */

