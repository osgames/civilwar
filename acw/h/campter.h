/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CAMPTER_H
#define CAMPTER_H

#ifndef __cplusplus
#error campter.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Terrain
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/06/07  18:33:23  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/05/04  22:12:01  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "filedata.h"
#ifdef CAMPEDIT
#include "image.h"
#include "palette.h"
#endif

class Location;

/*
 * Terrain File header
 */

struct TerrainHeaderF {
	IWORD width;
	IWORD height;
	ILONG dataSize;
};

/*
 * Campaign Terrain Coordinates
 */

struct CampaignTerrainCoord {
	UWORD x;
	UWORD y;

	Boolean operator != (const CampaignTerrainCoord& c)
	{
		return (x != c.x) || (y != c.y);
	}

	Boolean operator == (const CampaignTerrainCoord& c)
	{
		return (x == c.x) && (y == c.y);
	}

};


/*
 * In Memory class
 */


typedef UBYTE CTerrainVal;

class CampaignTerrain {

#ifdef CAMPEDIT
	Image image;
	Palette pal;
#else
	UWORD width;
	UWORD height;
	UBYTE* data;

	UBYTE** linePtr;
#endif

public:
	CampaignTerrain();

#ifdef CAMPEDIT
	void init();
	void write();
	void set(const Location& l, CTerrainVal type);
#else
	void init(const char* fileName);
#endif

	~CampaignTerrain();

	CTerrainVal get(const Location& l);

	void locationToCampTCord(CampaignTerrainCoord& g, const Location& l);
	void campTCordToLocation(Location& l, const CampaignTerrainCoord& g);

#if !defined(CAMPEDIT)
	void makeGrid(CTerrainVal* grid, const Location& l, int size);
	CTerrainVal getVal(int column, int line);
	int getWidth() const { return width; }
	int getHeight() const { return height; }
#endif
};

/*
 * Enumeration of types
 */

enum CTerrainTypes {
	CT_Unused0,
	CT_Sea,
	CT_Coastal,
	CT_River,
	CT_Stream,
	CT_Swamp,				// CT_Wooded,
	CT_Wooded,				// CT_Farmland,
	CT_Farmland,			// CT_Rough,
	CT_Mountain,			// CT_Swamp,
	CT_Rough,				// CT_Mountain,
	CT_Impassable,
	CT_Unused11,
	CT_Unused12,
	CT_Unused13,
	CT_Unused14,
	CT_Unused15
};


#endif /* CAMPTER_H */

