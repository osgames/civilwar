/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */

#ifndef CLOG_H
#define CLOG_H

#ifndef __cplusplus
#error clog.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Combat Log
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG

#include <stdio.h>
#include "types.h"

class TimeInfo;

class LogFile {
	FILE* log;
	static TimeInfo* theTime;
	Boolean showTime;

	const char* name;
	short lastDay;
	Boolean created;

public:
	LogFile(const char* name);
	~LogFile();

	void printf(const char* fmt, ...);

	void close();			// Temporarily close log file

	void setRealTime()
	{
		theTime = 0;
		showTime = True;
	}

	void setTime(TimeInfo* t)
	{
		if(t)
		{
			theTime = t;
			showTime = True;
		}
		else
			showTime = False;
	}
};

#endif	// DEBUG

#endif /* CLOG_H */

