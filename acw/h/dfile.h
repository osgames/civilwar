/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef DFILE_H
#define DFILE_H

#ifndef __cplusplus
#error dfile.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Binary Data Files used in Rally Round the Flag
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include "types.h"
#include "filedata.h"
#include "micelib.h"
#include "error.h"

/*
 * Chunk Id's
 */

typedef ULONG ChunkID;

/*
 * Errors
 */

enum ChunkError {
	CE_OK,
	CE_GeneralError
};

/*
 * Other Types
 */

typedef fpos_t ChunkPos;

#define RRFC_ID 'RRFC'

enum FileType {
	FT_DontCare,				// Value when we just want to open a file to see what it is

	FT_StartCampaign,
	FT_SavedCampaign,

	FT_HistoricalBattle,
	FT_SavedBattle,
	FT_BattleInCampaign,

	FT_DynamicCampaign,		// Temporary file created when playing battle
};

struct ChunkDir {
	ChunkID id;
	ChunkPos pos;
	size_t length;
};

struct D_ChunkDir {
	ILONG id;
	ILONG pos;
	ILONG length;
};

struct D_FileHeader {
	ILONG id;			// RRFC
	IWORD headerVersion;	// Version of this file Header
	IWORD fileType;	// Campaign, Battle, etc...
	IWORD version;		// Version Number
	IWORD nChunks;		// Number of Chunks in directory
	char name[32];		// A description
};


struct D_FileHeader_00 {
	ILONG id;			// RRFC
	char name[16];
	IWORD version;	// Version Number
	IWORD nChunks;	// Number of Chunks in directory
};

/*
 * Base Class for Files
 */

class ChunkFileBase {
protected:
	FILE* fp;
	ChunkError lastError;
	PointerIndex oldPointer;
public:
	ChunkFileBase();
	~ChunkFileBase();
	ChunkError setPos(ChunkPos pos);
	ChunkPos getPos();
	ChunkError getError() const;
	Boolean isGood() const;

	virtual void showInfo(const char* fmt) { }
#ifdef DEBUG
	virtual void putInfo(const char* fmt) { }
#endif

	/*
	 * Exception handling
	 */

	class ChunkFileError : public GeneralError {
	public:
		ChunkFileError() : GeneralError("Chunk File Error") {	}
		ChunkFileError(const char* fmt, ...);
	};

protected:
	void setError(ChunkError err);
};

/*
 * Class Used for reading Binary files
 *
 * We'll use Standard C Buffered Files, because they are easier to
 * work with and quicker than C++ streams.
 */

class ChunkFileRead : public ChunkFileBase {
	int nChunks;
	ChunkDir* dir;
	char* id;
public:
	FileType fileType;
	UWORD fileVersion;
public:
	ChunkFileRead(const char* fileName, FileType wantType);
	~ChunkFileRead();

	ChunkError startReadChunk(ChunkID id);
	ChunkError read(void* ad, size_t size);
	char* readString();
#ifdef CAMPEDIT
	char* readString(char* s);
#endif
};

/*
 * Class Used for writing Binary Files
 */

class ChunkFileWrite : public ChunkFileBase {
	int chunkCount;
	int nextChunk;
	D_ChunkDir* dir;

public:
	ChunkFileWrite(const char* fileName, FileType fType, const char* fileID, UWORD version, int nChunks);
	~ChunkFileWrite();

	ChunkError startWriteChunk(ChunkID id);
	ChunkError endWriteChunk();

	ChunkError write(const void* ad, size_t size);
	ChunkError writeString(const char* s);
};

void putName(IBYTE* dest, const char* src, size_t length);

/*
 * Class for reading a file whilst displaying info in a popup box
 */

class PopupText;

class ReadWithPopup : public ChunkFileRead {
	PopupText* popup;
#ifdef DEBUG
	char* popupBuffer;
	char* sPtr;
#endif

public:
	ReadWithPopup(const char* fileName, FileType wantType);
	~ReadWithPopup();

#ifdef DEBUG
	void showInfo(const char* fmt);
	void putInfo(const char* fmt);
#endif
};

class WriteWithPopup : public ChunkFileWrite {
	PopupText* popup;
#ifdef DEBUG
	char* popupBuffer;
#endif

public:
	WriteWithPopup(const char* fileName, FileType fType, const char* fileID, UWORD version, int nChunks);
	~WriteWithPopup();

#ifdef DEBUG
	void showInfo(const char* fmt);
#endif
};


#endif /* DFILE_H */

