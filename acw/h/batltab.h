/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef BATLTAB_H
#define BATLTAB_H

#ifndef __cplusplus
#error batltab.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Tables Used in battle game
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:12  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "measure.h"
// #include "terrain.h"

extern Distance effectiveRanges[3][4];
extern Distance decisiveRange[];

#define NearEnemyRange Yard(500)		// Distance that units stop marching
#define CannisterRange Yard(500)
#define PanicRange Yard(50)

extern Distance battleCommandRange[];

extern UBYTE loadingTimes[3][4];

extern int infantryRanges[];
extern int artilleryRanges[];
extern UBYTE firingModifiers[3][4][8];

extern BYTE fireExperience[];
extern BYTE fireSupply[];
extern BYTE fireFatigue[];
extern BYTE fireMorale[];

extern BYTE rrcExperience[];
extern BYTE rrcFatigue[];
extern BYTE rrcSupply[];
extern BYTE rrcBrigCommander[];
extern BYTE rrcTopCommander[];

extern const UWORD MeleeFightTime;

extern UBYTE meleeMountedType[3][4];
extern UBYTE meleeUnmountedType[3][4];
extern BYTE meleeExperience[];
extern BYTE meleeFatigue[];
extern BYTE meleeMorale[];

extern UBYTE staticObjectSizes[];

#endif /* BATLTAB_H */

