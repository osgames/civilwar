/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef POLY3D_H
#define POLY3D_H

#ifndef __cplusplus
#error poly3d.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	3D Polygon shapes
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.19  1994/08/19  17:30:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.18  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.17  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.16  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/06/07  18:33:23  Steven_Green
 * Wangle class simplified
 *
 * Revision 1.14  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.12  1994/05/21  13:18:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/04/20  22:24:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/04/11  21:31:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/04/11  13:38:20  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/03/18  15:08:40  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/02/28  23:05:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/02/17  20:00:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/12/14  23:30:30  Steven_Green
 * Preparation for Textured Polygons
 *
 * Revision 1.1  1993/11/25  04:40:34  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


#ifndef SHAPE3D_H
#include "shape3d.h"
#endif

class Image;


class Edge;
class Face;

class Poly3D : public Shape3D {
	Point3DIndex nPoints;
	Point3D* points;

	EdgeIndex nEdges;
	Edge* edges;

	FaceIndex nFaces;
	Face* faces;

public:
				Poly3D() { nPoints = nEdges = nFaces = 0; points = 0; edges = 0; faces = 0; };


				Poly3D(Point3DIndex nPoints, Point3D* points,
						   EdgeIndex nEdges,  Edge* edges,
						   FaceIndex nFaces,  Face* faces)
						 	{
						  		Poly3D::nPoints 	= nPoints;
								Poly3D::points 	= points;
								Poly3D::nEdges 	= nEdges;
								Poly3D::edges 	= edges;
								Poly3D::nFaces 	= nFaces;
								Poly3D::faces		= faces;
						  	};

	// friend void drawPoly3D(ObjectDrawData* drawData, const Shape3D* shape, const Position3D& position);
	
	Point3D draw(ObjectDrawData* drawData, const Position3D& position);
};

enum EdgeDirection { F, R };

class Edge {
	Point3DIndex p1;
	Point3DIndex p2;
public:
			Edge(Point3DIndex from, Point3DIndex to) { p1 = from; p2 = to; };

	Point3DIndex first(EdgeDirection direction) { return (direction == F) ? p1: p2; }; 
	Point3DIndex last(EdgeDirection direction)  { return (direction == F) ? p2: p1; }; 

	Point3DIndex from() const { return p1; };
	Point3DIndex to() const { return p2; };

	// friend void drawShape3D(ObjectDrawData* drawData, const Shape3D* shape, const Position3D& position);
	
	friend Point3D Poly3D::draw(ObjectDrawData *drawData, const Position3D& position);
};



class EdgeFace {
	EdgeDirection direction;
	EdgeIndex index;
public:
			EdgeFace(EdgeIndex i, EdgeDirection d = F) { direction = d; index = i; };
	// friend void drawShape3D(ObjectDrawData* drawData, const Shape3D* shape, const Position3D& position);
	friend Point3D Poly3D::draw(ObjectDrawData *drawData, const Position3D& position);
};

class Face {
	EdgeIndex nEdges;
	EdgeFace *edges;

	Colour colour;
	Image* pattern;
public:
				Face(EdgeIndex n, EdgeFace* e, Colour c, Image* p = 0):
					colour(c), pattern(p), nEdges(n), edges(e) { }
	friend Point3D Poly3D::draw(ObjectDrawData *drawData, const Position3D& position);
};

#endif /* POLY3D_H */

