/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MAPICON_H
#define MAPICON_H

#ifndef __cplusplus
#error mapicon.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Icons used with Map Window
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "menuicon.h"
#include "layout.h"

class MapWindow;

/*
 * Class for icons making use of MapWindow
 */

class MapWindowIcon {
protected:
	MapWindow* map;
public:
	MapWindowIcon(MapWindow* m) { map = m; }
};

/*
 * Icon containing the displayable part of the map excluding scroll bars
 */

class MapViewIcon : public MenuIcon, public MapWindowIcon {
	// Colour hintColour;
public:
	MapViewIcon(IconSet* parent, MapWindow* map) :
		MenuIcon(parent, C_SelectMap, Rect(MENU_X,426,MENU_ICON_W, MENU_ICON_H)),
		MapWindowIcon(map)
	{
	}

	void execute(Event* event, MenuData* d);
};

/*
 * Keyboard shortcuts for scrolling
 */

class CampaignScrollLeftIcon : public NullIcon {
public:
	CampaignScrollLeftIcon(IconSet* parent) : NullIcon(parent, KEY_Left) { }

	void execute(Event* event, MenuData* data);
};

class CampaignScrollRightIcon : public NullIcon {
public:
	CampaignScrollRightIcon(IconSet* parent) : NullIcon(parent, KEY_Right) { }

	void execute(Event* event, MenuData* data);
};

class CampaignScrollUpIcon : public NullIcon {
public:
	CampaignScrollUpIcon(IconSet* parent) : NullIcon(parent, KEY_Up) { }

	void execute(Event* event, MenuData* data);
};

class CampaignScrollDownIcon : public NullIcon {
public:
	CampaignScrollDownIcon(IconSet* parent) : NullIcon(parent, KEY_Down) { }

	void execute(Event* event, MenuData* data);
};



#endif /* MAPICON_H */

