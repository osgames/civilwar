/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CAMPBATL_H
#define CAMPBATL_H

#ifndef __cplusplus
#error campbatl.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battles within the campaign game
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "gametime.h"
#include "measure.h"
#include "gamedef.h"
#include "side.h"

class Unit;
class OrderBattle;

struct BattleUnit {
	BattleUnit* next;
	Unit* unit;				// What unit are we talking about?
	TimeBase when;			// What time does it arrive on the battlefield?
};

class MarkedBattle {
public:
	MarkedBattle* next;
	Location where;	// Where is it?
	TimeBase when;		// When did it start?
	BattleUnit* units;	// Who is involved?
	Boolean playable;
public:
	MarkedBattle();
	MarkedBattle(const Location& l, TimeBase t, Boolean play);
	MarkedBattle(Boolean play);
	~MarkedBattle();

	void addUnit(Unit* u, TimeBase t, Boolean force);
	void addChildren(Unit* b, TimeBase t );
	void addCloseUnits();

	int nUnits() const;
	void append(BattleUnit* bu);

	Unit* getSeniorUnit(Side side);
	Strength getSideTotal(Side si);

	void updateExperience(int n);
};

class BattleList {
	MarkedBattle* battles;
	MarkedBattle* lastBattle;
public:
	BattleList() { battles = 0; lastBattle = 0; }

	void clear();			// Remove battles
	MarkedBattle* getBattle();
	MarkedBattle* find(const Location& l, TimeBase t, Boolean play);
	void checkUnitNearBattle(Unit* u);
	int entries() const;

	void append(MarkedBattle* mb);

	MarkedBattle* next(MarkedBattle* mb)
	{
		if(mb)
			return mb->next;
		else
			return battles;
	}

#if !defined(TESTBATTLE)
#ifdef DEBUG
	void check();
#endif
#endif
};

MarkedBattle* makeMarkedBattle(OrderBattle* ob);

#endif /* CAMPBATL_H */

