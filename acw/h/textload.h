/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
	 'textload' loads a ascii file called 'filename'.

*/

#ifndef fstream
#include<fstream.h>
#endif

class TextLoad		
{

 protected:

	char *textfile;
	ifstream txt;
 //	fstream keyw;
 //	char *keywfile;		// ? add as parameter
	long tsize;
	char *texdata;
	char error;

 public:

	TextLoad( const char *filename );
	void Textdistroy();
	void getdata();

	long getSize()
		{ return	tsize;	}

	char *gettexdata()
		{ return texdata; }

	void setpointer( int pointer )
		{ txt.seekg ( pointer ); tsize = tsize - pointer; }
	
	int bad()
		{ return txt.fail();	}
				
  	const char geterr()
		{ return error; }

};

