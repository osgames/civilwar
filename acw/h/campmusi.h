/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CAMPMUSI_H
#define CAMPMUSI_H

#ifndef __cplusplus
#error campmusi.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Play Medley of songs during Campaign Game
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "menudata.h"
#include "globproc.h"

#if defined(SOUND_DSMI)
#include "amp.h"


class CampaignMusic : public GlobalProc {
	UBYTE disabled;			// 0 = enabled
	Boolean killMe;
	Boolean forced;
	MODULE* song;
	int songCount;
public:
	CampaignMusic();
	~CampaignMusic();

	void enable();			// Stop Campaign Music from playing
	void disable();		// Enable Campaign Music

	Boolean forceMusic(const char* fileName);		// Overide random medley
	void endForce();

	Boolean proc();
private:
	void stopSong();
	MODULE* loadSong(const char* songName);
};

#else
// Dummy Music Module

class CampaignMusic : public GlobalProc {
public:
	CampaignMusic() {}
	~CampaignMusic() {}

	void enable() {}			// Stop Campaign Music from playing
	void disable() {}		// Enable Campaign Music

	Boolean forceMusic(const char* fileName) { return false; }		// Overide random medley
	void endForce() {}

	Boolean proc() { return false; }
};


#endif

#endif /* CAMPMUSI_H */

