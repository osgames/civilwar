/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef BITMAP_H
#define BITMAP_H

#ifndef __cplusplus
#error bitmap.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Uncompressed Bitmap File format
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/06/21  18:45:38  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

class Image;
class Point;

struct BM_Header {
	UBYTE fileType;		// 0x00 (Reserved for future use)
	UBYTE version;			// 0x00
	UBYTE reserved1;
	UBYTE reserved2;
	UBYTE width_l;
	UBYTE width_h;
	UBYTE height_l;
	UBYTE height_h;
public:

	void setSize(UWORD width, UWORD height)
	{
		width_l = UBYTE(width & 0xff);
		width_h = UBYTE(width >> 8);

		height_l = UBYTE(height & 0xff);
		height_h = UBYTE(height >> 8);
	}

	UWORD getWidth() const
	{
		return UWORD(width_l + (width_h << 8));
	}

	UWORD getHeight() const
	{
		return UWORD(height_l + (height_h << 8));
	}
};

void readBMsection(Image* img, const char* name, const Point& p);

#endif /* BITMAP_H */

