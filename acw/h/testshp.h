/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef TESTSHP_H
#define TESTSHP_H

#ifndef __cplusplus
#error testshp.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Test Shapes
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.17  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.16  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.14  1994/06/07  18:33:23  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.11  1994/05/21  13:18:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/04/20  22:24:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/04/11  21:31:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/04/11  13:38:20  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/03/18  15:08:40  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/02/28  23:05:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/02/17  20:00:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/30  02:57:48  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifndef SHAPE3D_H
#include "shape3d.h"
#endif
 
extern WorldObject ob1;
extern WorldObject ob2;
extern WorldObject ob3;
extern WorldObject ob4;
extern WorldObject obFloor;

#endif /* TESTSHP_H */

/* Depends on h\shape3d.h , touched on 03-17-94 at 11:21:09 */
/* Depends on h\shape3d.h , touched on 03-17-94 at 11:24:33 */
/* Depends on h\shape3d.h , touched on 03-17-94 at 11:51:48 */
/* Depends on h\shape3d.h , touched on 03-17-94 at 12:03:53 */
/* Depends on h\shape3d.h , touched on 03-17-94 at 15:09:22 */
/* Depends on h\shape3d.h , touched on 03-17-94 at 16:02:15 */
/* Depends on h\shape3d.h , touched on 03-18-94 at 11:12:33 */
/* Depends on h\shape3d.h , touched on 03-21-94 at 12:03:19 */
/* Depends on h\shape3d.h , touched on 03-21-94 at 12:52:13 */
/* Depends on h\shape3d.h , touched on 03-24-94 at 12:18:36 */
/* Depends on h\shape3d.h , touched on 03-24-94 at 17:05:05 */
/* Depends on h\shape3d.h , touched on 03-25-94 at 15:40:16 */
/* Depends on h\shape3d.h , touched on 03-29-94 at 12:29:01 */
/* Depends on h\shape3d.h , touched on 04-06-94 at 11:21:51 */
/* Depends on h\shape3d.h , touched on 04-07-94 at 10:24:20 */
/* Depends on h\shape3d.h , touched on 04-07-94 at 12:55:48 */
/* Depends on h\shape3d.h , touched on 04-11-94 at 14:59:50 */
/* Depends on h\shape3d.h , touched on 04-15-94 at 12:15:18 */
/* Depends on h\shape3d.h , touched on 04-15-94 at 14:58:22 */
/* Depends on h\shape3d.h , touched on 05-17-94 at 14:55:33 */
/* Depends on h\shape3d.h , touched on 05-17-94 at 15:12:00 */
/* Touched on 05-19-94 at 16:41:18 */
/* Touched on 05-20-94 at 10:19:22 */
/* Touched on 05-31-94 at 14:08:21 */
/* Touched on 05-31-94 at 17:21:15 */
/* Touched on 06-01-94 at 10:59:24 */
/* Touched on 06-01-94 at 14:16:30 */
/* Touched on 06-03-94 at 17:26:28 */
/* Touched on 06-03-94 at 18:01:32 */
/* Touched on 06-07-94 at 11:52:33 */
/* Touched on 06-08-94 at 12:32:53 */
/* Touched on 06-09-94 at 12:24:48 */
/* Touched on 06-09-94 at 18:33:32 */
/* Touched on 06-10-94 at 14:42:38 */
/* Touched on 06-13-94 at 14:27:49 */
/* Touched on 06-13-94 at 16:11:24 */
/* Touched on 06-23-94 at 15:59:48 */
/* Touched on 06-23-94 at 16:31:10 */
