/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef QUEUE_H
#define QUEUE_H

#ifndef __cplusplus
#error queue.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Template for simple queue structure
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/08/09  15:46:26  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG
#include "error.h"
#endif

template<class T>
class Queue {
	T* items;
	int howMany;
	int head;
	int tail;
public:
	Queue(int n);
	~Queue();

#ifdef DEBUG
	Queue(Queue<T>& t)
	{
		throw GeneralError("Queue<T> copy constructor unwritten");
	}

	Queue<T>& operator = (Queue<T>& t)
	{
		throw GeneralError("Queue<T> = function unwritten");
	}
#endif


	void put(const T& item);
	Boolean get(T& item);
	Boolean isEmpty() const { return head==tail; }
};

template<class T>
Queue<T>::Queue(int n)
{
	items = new T[n];
	howMany = n;
	head = 0;
	tail = 0;
}
 

template<class T>
Queue<T>::~Queue()
{
	delete[] items;
}

template<class T>
void Queue<T>::put(const T& item)
{
	int nextHead = head+1;
	if(nextHead >= howMany)
		nextHead = 0;

	if(nextHead == tail)
		throw GeneralError("Queue full");

	items[head] = item;
	head = nextHead;
}

template<class T>
Boolean Queue<T>::get(T& item)
{
	if(head == tail)
		return False;
	else
	{
		item = items[tail];

		if(++tail >= howMany)
			tail = 0;

		return True;
	}
}


/*
 * Simple Stack Structure
 */

template<class T>
class Stack {
	T* items;
	int howMany;
	int head;
public:
	Stack(int n)
	{
		items = new T[n];
		howMany = n;
		head = 0;
	}

	~Stack()
	{
		delete[] items;
	}

#ifdef DEBUG
	Stack(Stack<T>& t)
	{
		throw GeneralError("Stack<T> copy constructor unwritten");
	}

	Stack<T>& operator = (Stack<T>& t)
	{
		throw GeneralError("Stack<T> = function unwritten");
	}
#endif

	void put(const T& item)
	{
		if(head >= howMany)
			throw GeneralError("Stack Full");

		items[head++] = item;
	}

	Boolean get(T& item)
	{
		if(head == 0)
			return False;
		else
		{
			item = items[--head];
			return True;
		}
	}

	Boolean isEmpty() const
	{
		return head == 0;
	}
};

#endif /* QUEUE_H */

