/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef GAME_H
#define GAME_H

#ifndef __cplusplus
#error game.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Outer level game definition
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.25  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.20  1994/04/22  22:16:24  Steven_Green
 * Icon Handling Tidied Up
 *
 * Revision 1.4  1994/01/11  22:30:12  Steven_Green
 * Split into seperate files for each screen
 *
 * Revision 1.3  1994/01/11  21:14:28  Steven_Green
 * Realism/Difficulty levels added
 *
 * Revision 1.1  1994/01/06  22:40:53  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#if !defined(CAMPEDIT) && !defined(BATEDIT)

#include "gr_types.h"
#include "gamedef.h"
#include "random.h"
#include "side.h"

/*
 * Unknown classes
 */

class Connection;
class Packet;
class Region;
class SpriteLibrary;
class CampaignMusic;

/*
 * Some enumerations and types
 */

enum GameType { CampaignGame, BattleGame };

class DifficultyClass {
	Realism value;
public:
#ifdef DEBUG		// Default to complex level
	DifficultyClass() { value = Complex3; }
#else
	DifficultyClass() { value = Simple; }
#endif

	DifficultyClass& operator = (Realism v) { value = v; return *this; }

	operator Realism() const { return value; }

	DifficultyClass& operator ++ () {
		if(value == Complex3)
			value = Simple;
		else
		{
			int v = value;
			v++;
			value = Realism(v);
		}
		return *this;
	}

	DifficultyClass& operator -- () {
		if(value == Simple)
			value = Complex3;
		else
		{
			int v = value;
			v--;
			value = Realism(v);
		}

		return *this;
	}
};

enum DifficultyType {

	Diff_Resource,
	Diff_Supply,					// +Battle
	Diff_RegimentTypes,			// +Battle
	Diff_Facilities,
	Diff_CommandControl,			// +Battle
	Diff_Fatigue,					// +Battle
	Diff_TerrainEffects,			// +Battle
	Diff_Morale,					// +Battle
	Diff_Victory,

	// Marker

	DifficultyCount,
	Diff_Global = DifficultyCount,		// Special case used to mean ALL values
	Diff_First = 0
};

/*
 * Global Game Data Class
 */

class GameVariables {
#if defined(NOBITFIELD)
	Boolean quit;
#else
	Boolean quit:1;
#endif

	DifficultyClass difficulty[DifficultyCount];		// Selective Difficulty settings
	// Region* mapArea;

public:
	GameType gameType;					// Campaign or Battle?

private:
	/*
	 * Controller updated so that:
	 *		AI can play both sides
	 *		AI can help the player by setting up default moves
	 */

	struct {
#if defined(NOBITFIELD)
		Boolean player;			// Set if player can control this side
		Boolean ai;				// Set if AI can control this side
		Boolean remote;			// Set if remote player can control this side.
#else
		Boolean player:1;			// Set if player can control this side
		Boolean ai:1;				// Set if AI can control this side
		Boolean remote:1;			// Set if remote player can control this side.
#endif
	} sideControl[SideCount];

	Side playerSide;
	Side remoteSide;

public:
	Connection* connection;				// Multiplayer connection class
	SpriteLibrary* sprites;
	CampaignMusic* gameMusic;

	RandomNumber gameRand;				// Game Random Number, must be same on both machines
	RandomNumber miscRand;				// Random Number that can be used anywhere


	Boolean musicInGame;			// Set to enable in-game Music
	Boolean musicInCampaign;
	Boolean musicInBattle;
	Boolean musicInDatabase;
	Boolean soundInGame;
	Boolean inCampaign;
	Boolean inBattle;

public:
	// Rect mapRect;

public:
	GameVariables();
	~GameVariables();

	void play(Boolean skipMenu, const char* loadName);

	void setQuit() { quit = True; }
	Boolean isQuit() const { return quit; }

	void setGameType(GameType t) { gameType = t; }
	GameType getGameType() const { return gameType; }

	void setSides(Side pSide, Side aiSide, Side rSide);
	void setupPlayerSide(Side side);
#ifdef DEBUG
	void toggleAI(Side side);
#endif

	void setRemote();			// Set up controller and remoteActive
	Side getRemoteSide() const { return remoteSide; }
	Side getLocalSide() const { return playerSide; }
	Boolean isRemoteActive() const { return remoteSide != SIDE_None; }
	Boolean isAIactive() const;
	Boolean isAI(SideIndex i) const { return sideControl[i].ai; }
	Boolean isRemote(SideIndex i) const { return sideControl[i].remote; }
	Boolean isPlayer(SideIndex i) const { return sideControl[i].player; }
	Boolean isPlayer(Side s) const
	{
		if(s == SIDE_None)
			return False;
		return sideControl[sideIndex(s)].player;
	}

#ifdef DEBUG
	void checkControl() const;
#endif



	void setDifficulty(DifficultyType which, Realism value) { difficulty[which] = value; }
	Realism getDifficulty(DifficultyType which) const { return Realism(difficulty[which]); }
	void incDifficulty(DifficultyType w) { ++difficulty[w]; }
	void decDifficulty(DifficultyType w) { --difficulty[w]; }

	Boolean invisibilityON()
	{
		return getDifficulty(Diff_TerrainEffects) > Average;
	}



	void simpleMessage(const char* fmt = 0, ...);

	Packet* receiveData();
	void disconnect();
	void makeSinglePlayer();

	void getConfigDifficulty();
	void setConfigDifficulty();
};


#ifdef DEBUG
extern Boolean disableAI;
#endif

#else		// CAMPEDIT || BATEDIT

#include "random.h"

class SpriteLibrary;
// class Connection;

class GameVariables {
public:
	// Connection* connection;				// Multiplayer connection class
	SpriteLibrary* sprites;
	RandomNumber gameRand;				// Game Random Number
public:
	GameVariables();
	~GameVariables();
};

#endif	// CAMPEDIT

void showMainMenuScreen();

/*========================================================================
 * Global Variables
 */

extern GameVariables* game;


#endif /* GAME_H */
