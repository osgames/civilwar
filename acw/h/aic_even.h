/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef AIC_EVEN_H
#define AIC_EVEN_H

#ifndef __cplusplus
#error aic_even.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign AI: possible events to consider
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:12  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"
#include "gamedef.h"
#include "ai_unit.h"

class Location;

typedef UBYTE AIC_Priority;
#define AIC_PriorityMax 0xff


class AIC_Event {
	friend class AIC_EventList;
private:
	AIC_Event* next;			// Singly linked list

public:
	AI_Action action;
	Facility* where;
	Unit* who;
	AIC_Priority priority;
	CombatValue enemyCV;
	CombatValue friendlyCV;
	AIC_Priority nearPriority;
	Facility* closeFriend;			// Nearest friendly town (to gather when attacking)
	Boolean canAttack;				// Set if enough forces already gathered to attack

	void getLocation(Location& l) const;
#ifdef DEBUG
	void print();
#endif
};

class AIC_EventList {
	AIC_Event entry;

	AIC_Event* current;		// Last value returned
	AIC_Event* prev;			// For easy removal of current
public:
	AIC_EventList();
	~AIC_EventList() { clear(); }

	void clear();

	AIC_Event* add(AIC_Priority priority);		// Add keeping sorted by priority

	void rewind();								// Next call to next() will start at beginning
	Boolean next(AIC_Event*& value);		// Get next in list
	void remove();								// Remove last node that was returned

	CombatValue addEnemyCV() const;

#ifdef DEBUG
	void print();
#endif

private:
	inline Boolean AIC_EventList::eventCompare(AIC_Event* event, AIC_Priority priority);

};


#endif /* AIC_EVEN_H */

