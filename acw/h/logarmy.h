/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef LOGARMY_H
#define LOGARMY_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Log Army to log file definition file
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.4  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.3  1994/04/11  13:38:20  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/04/06  12:42:17  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/02/09  15:01:35  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif


#undef LOG_ARMIES
#undef LOG_GENERALS

#if defined(DEBUG) && !defined(BATEDIT) && !defined(CAMPEDIT)
 #define LOG_ARMIES
 #if !defined(TESTBATTLE)
  #define LOG_GENERALS
 #endif
#endif


#ifdef LOG_ARMIES

class Unit;
void logArmies(Unit* top);

#endif	// LOG_ARMIES

#ifdef LOG_GENERALS

void logGenerals();

#endif	// LOG_GENERALS

#ifdef __cplusplus
};
#endif


#endif /* LOGARMY_H */

