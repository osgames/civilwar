/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef LANGUAGE_H
#define LANGUAGE_H

#ifndef __cplusplus
#error language.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	This routine gets the text asscoiated with a particular language
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

enum LGE	{
	lge_NULL = -1,				// Value to indicate No String (as in NULL)

	lge_cavalry = 0,
	lge_naval,
	lge_battle_title,
	lge_feature_title,
	lge_songs_title,
	lge_army_of,

	/*
	 * 	The following section is for calcbat
	 */

	lge_cbats,
	lge_ubats,
	lge_cbat_run,
	lge_cbat_win,
  	lge_cbat_draw,
	lge_cbat_loose,
	lge_cbat_conwin,

 	lge_ubat_run,
	lge_ubat_win,
	lge_firing,
	lge_ubat_draw,
	lge_ubat_loose,
	lge_ubat_conwin,

	lge_nocas,

	lge_OK,
	lge_hip,
	lge_drat,

	lge_cbrave,
	lge_cinde,
	lge_chordes,

	lge_ubrave,
	lge_uinde,
	lge_uhordes,

	lge_uArmyCas,
	lge_uArmyOne,
	lge_uNoArmyCas,

	lge_cArmyCas,
	lge_cArmyOne,
	lge_cNoArmyCas,

	lge_YouArmyCas,
	lge_YouArmyOne,
	lge_YouNoArmyCas,

	lge_Versus,
	lge_FortAt,

	/*
	 * 	Language.cpp
	 */

	lge_FileMissing,
	lge_RetryCancel,

	/*
	 * 	Gameicon
	 */

	lge_quitmm,   // Quit gane and return to main menu
	lge_Happy,
	lge_yesno,
	lge_cont,
	lge_play,
	lge_save,
	lge_diskfn,
	lge_saveCancel,
	lge_saveAs,
	lge_abort,
	lge_mmCancel,
	lge_musicSound,
	lge_quitProg,
	lge_Doquit,
	lge_quitCancel,
	lge_QuitToDOS,

	lge_ViewDB,

	/*
	 * 	Realism screen
	 */

	lge_simple,
	lge_average,
	lge_complex1,
	lge_complex2,
	lge_complex3,

	lge_resources,
	lge_supplies,
	lge_unit,
	lge_command,
	lge_facility,
	lge_fatigue,
	lge_terrain,
	lge_morale,
	lge_victory,
	lge_battleRealism,

	lge_RemotechangedRealism,

	lge_RemoteStart,
	lge_RemoteGoMM,

	/*
	 * 	campscn
	 */

	lge_clock,
	lge_campMode,
	lge_promoteGen,

	lge_RemoteNotReady,
	lge_RemoteEndDay,
	lge_EndDayCont,

	lge_RemoteMM,
	lge_RemoteNo,
	lge_TryAgain,

	/*
	 * 	Mapwind
	 */

	lge_FullZoom,
	lge_Zoomed,

	/*
	 * 	SCROLL
	 */

	lge_scrollLeft,
	lge_scrollRight,
	lge_scrollUp,
	lge_scrollDown,

	lge_shiftLeft,
	lge_shiftRight,
	lge_shiftUp,
	lge_shiftDown,

	/*
	 * 	Clock
	 */

	lge_batClock,

	/*
	 * 	Campunit
	 */

	lge_OrderAd,
	lge_OrderCA,
	lge_OrderAt,

	lge_OrderSt,
	lge_OrderHo,
	lge_OrderDe,

	lge_Orderwd,
	lge_OrderRe,
	lge_OrderHR,

	lge_OrderByRail,
	lge_OrderByWater,
	
	lge_cancel,

	lge_OrderNavalUnit,
	lge_OrderNavalIronClad,
	lge_OrderRiverine,
	lge_OrderRiverineIronClad,

	lge_NotAvailable,

	/*
	 *  Campcity
	 */

	lge_mobRI,
	lge_mobMI,
	lge_mobSI,
	lge_mobEng,

	lge_mobRC,
	lge_mobMC,
	lge_mobRMI,
	lge_mobMMI,

	lge_mobAS,
	lge_mobAL,
	lge_mobAR,
	lge_mobAsiege,

	lge_mobNaval,
	lge_mobNavalIC,
	lge_mobRiverine,
	lge_mobriverIC,

	lge_buildSW,
	lge_buildFort,
	lge_buildSD,
	lge_buildRC,
	lge_buildTC,
	lge_buildRail,
	lge_mobRailEng,
	lge_buildHosp,
	lge_buildPOW,
	lge_blockadeRun,

	lge_createI,
	lge_createC,
	lge_createA,
	lge_createN,

	lge_latestO,

	/*
	 * 	Mainmenu
	 */

	lge_multi,
	lge_connectionType,
	lge_load,
	lge_chooseSide,
	lge_campaignOrBattle,
	lge_startCampaign,
	lge_campaignReady,

	lge_historical,
	lge_historicalReady,


	lge_ShowCredits,
	lge_StartGame,

	lge_remoteNoleave,
	lge_remoteload,
	lge_remotebattle,
	lge_remotecampaign,

	lge_startcancel,

	lge_remoteGametype,
	lge_remoteSidechanged,

	/*
	 * 	Mobilise
	 */

	lge_build,
	lge_cantAfford,

	/*
	 * 	CALICON
	 */

	lge_MergeRegs,
	lge_OKCancel,


	/*
	 * 	GAME
	 */

	lge_RemoteQuit,

	/*
	 * 	CAMPUNIT
	 */

	lge_InvalidDest,

	/*
	 * 	WATER
	 */

	lge_WtakenOver,
	lge_Wunion,
	lge_Wconfed,
	lge_Bombarded,
	lge_BombardedByFort,
	lge_AndIsWithdrawing,
	lge_AndWereSunk,
	lge_FortDestroyed,
	lge_WasSunk,
	lge_WereSunk,

	/*
	 * 	BATWIND
	 */

	lge_rotFieldLeft,
	lge_rotFieldRight,

	lge_Zoom,
	lge_ZoomIn,
	lge_ZoomOut,
	
	lge_ZoomRightIn,
	lge_ZoomCentre,
	lge_ZoomOutTwo,
	lge_ZoomInTwo,
	lge_ZoomRightOut,
	lge_ZoomMiddle,


	/*
	 * 	BATTLE
	 */

	lge_endBattle,

	/*
	 * 	CALPICK
	 */

	lge_rejoinCommand,
	lge_ReattachUnits,
	lge_ReattachAllUnits,
	lge_FindonMap,
	lge_IssueOrders,

	lge_TransferGeneral,
	lge_TransferUnits,

	lge_EditArmyName,
	lge_EditRegiment,
	lge_EditGeneral,
	lge_NewGeneral,

	/*
	 * 	BATWIND
	 */

	lge_FacingNorth,
	lge_FacingEast,
	lge_FacingSouth,
	lge_FacingWest,

	/*
	 * 	CONNECT
	 */

	lge_RemoteNotRespond,
	lge_RetryQuit,
	lge_RemoteWaiting,
	lge_RemoteReply,

	/*
	 * 	MOUSE
	 */

	lge_NoMouse,

	/*
	 *		BATSETUP
	 */

	lge_CreateBF,

	/*
	 * 	GAMETIME
	 */

	lge_Jan,
	lge_Feb,
	lge_Mar,
	lge_Apr,
	lge_May,
	lge_Jun,
	lge_Jul,
	lge_Aug,
	lge_Sep,
	lge_Oct,
	lge_Nov,
	lge_Dec,

	/*
	 * 	TABLES
	 */

	lge_RegularInfantry,
	lge_MilitiaInfantry,
	lge_Sharpshooter,
	lge_Engineer,
	lge_RailEngineer,

	lge_RegularCavalry,
	lge_MilitiaCavalry,
	lge_RegularMountedInfantry,
	lge_MilitiaMountedInfantry,
	lge_Raiders,

	lge_SmoothboreArtillery,
	lge_LightArtillery,
	lge_RifledArtillery,
	lge_SiegeArtillery,

	lge_Fresh,
	lge_Weary,
	lge_Tired,
	lge_Exhausted,

	lge_Untried,
	lge_Tried,
	lge_Seasoned,
	lge_Veteran,
	lge_BattleWeary,

	lge_Routing,
	lge_Demoralised,
	lge_Dispirited,
	lge_Normal,
	lge_Good,
	lge_High,

	lge_Unefficient,
	lge_Bumbling,
	lge_Adequate,
	lge_Efficient,
	lge_VeryEfficient,

	lge_Useless, 
	lge_ability1,
	lge_ability2,
	lge_ability3,
	lge_Veryable,

	lge_Peaceful,
	lge_Slightlyaggressive, 
	lge_aggressive,
	lge_Veryaggressive,	  
	lge_Extremelyaggressive,

	lge_Unsupplied,
	lge_PoorlySupplied,		
	lge_PartiallySupplied,	
	lge_Supplied,

	lge_Advance,
	lge_CautiousAdvance,
	lge_Attack,
	lge_Stand,
	lge_Hold,
	lge_Defend,
	lge_Withdraw,
	lge_Retreat,
	lge_HastyRetreat,

	lge_IllegalOrder,

	lge_MovebyLand,
	lge_MovebyRail,
	lge_MovebyRiver,
	lge_MovebySea,

	/*
	 * 	BATSEL
	 */

	lge_nighttime,
	lge_NobodyOnBF,
	lge_LoadHisBat,

	/*
	 *  	DFILE
	 */

	lge_LoadingData,
	lge_SavingData,

	/*
	 * 	SOUND
	 */

	lge_SoundSetup,
	lge_ChooseSoundCard,
	lge_EnterRate,

	lge_None,
	lge_On,
	lge_Off,

	/*
	 * 	SERIAL
	 */

	lge_NoPorts,
	lge_TrySerial,

	/*
	 * 	IPX
	 */

	lge_TryIPX,
	lge_NoIPX,

	/*
	 * 	MODEM
	 */

	lge_SerialSetup,

	lge_ModemSetup,
	lge_ModemCommand,
	lge_Dial,
	lge_Answer,
	lge_Initialise,
	lge_PhoneNumber,

	/*
	 * New Strings
	 */

	lge_BaudRate,			// MODEM
	lge_Music,				// GameIcon
	lge_FX,					// GameIcon
	lge_ModemNoConnect,	// serial

	/*
	 * Newer Strings
	 */

	lge_unNamed,
	lge_errorReading,
	lge_builtIn,

	lge_NavalFleet,

	lge_uWin_HL,				// Victory before end Date
	lge_uWin_TEXT,
	lge_cWin_HL,
	lge_cWin_TEXT,
	lge_uWinLot_HL,			// Big victory at end Date
	lge_uWinLot_TEXT,
	lge_cWinLot_HL,
	lge_cWinLot_TEXT,
	lge_uWinEnd_HL,			// Normal Victory at end Date
	lge_uWinEnd_TEXT,
	lge_cWinEnd_HL,
	lge_cWinEnd_TEXT,

	/*
	 * Mobilisation descriptions
	 */

	lge_NavalUnit,
	lge_NavalIronClad,
	lge_Riverine,
	lge_RiverineIronClad,
	lge_SupplyWagon,
	lge_Fortification,
	lge_SupplyDepot,
	lge_RecruitmentCentre,
	lge_TrainingCamp,
	lge_RailheadCapacity,
	lge_Hospital,
	lge_POWCamp,
	lge_Blockade,

	/*
	 * calinfo
	 */

	lge_CurrentOrders,
	lge_unattached,

	/*
	 * sound
	 */

	lge_badMCP,
	lge_badAMP,
	
	/*
	 * unitinfo
	 */

	lge_CommandedBy,
	lge_NoCommander,

	/*
	 * battle
	 */

	lge_ReallyEnd,
	lge_EndContinue,
	lge_RemoteEndBattle,
	lge_EndBattle,

	/*
	 * IPX
	 */

	lge_makeIPX,

	/*
	 * campscn
	 */

	lge_ProcessAI,
	lge_Union,
	lge_Confederate,

	lge_troopMovement,
	lge_battleAt,
	lge_between,
	lge_playCalculate,

	/*
	 * testmus
	 */
	
	lge_soundCard,
	lge_rate,
	lge_quality,
	lge_baseIO,
	lge_soundIRQ,
	lge_soundDMA,
	lge_finished,
	lge_soundConfig,
	lge_canHear,
	lge_yesNo,

	/*
	 * scrndump
	 */

	lge_writeDump,

	/*
	 * Serial
	 */

	lge_initModem,
	lge_waitCall,

	/*
	 * Generals
	 */

	lge_Infantry,
	lge_Cavalry,
	lge_Artillery,
	lge_Inactive,
	lge_Mixed,

	/*
	 * map3d
	 */

	lge_CreatingBattleField,
	lge_RenderingLandscape,

	/*
	 * MainMenu
	 */

	lge_Version,
	lge_SerialModemIpxCancel,
	lge_SerialEstablished,
	lge_SerialNoConnect,
	lge_ModemEstablished,
	lge_IPXEstablished,
	lge_IPXNoConnect,
	lge_MoreCancel,
	lge_Credits1,
	lge_Credits2,
	lge_Credits3,
	lge_Credits4,
	lge_Credits5,

	/*
	 * calicon
	 */

	lge_Scroll,
	lge_NewUnit,


	/*
	 * mobilise
	 */

	lge_Build_TimeToBuild,

	/*
	 * tables
	 */

	// lge_President,
	lge_General,
	lge_Army,
	lge_Corps,
	lge_Division,
	lge_Brigade,
	lge_Regiment,

	lge_NamesDivision,
	lge_NamesBrigade,

	/*
	 * Filesel
	 */

	lge_NoFiles,

	/*==========================================
	 * New Strings for version 1.1
	 */

	// campopt

	lge_options,
	lge_CampaignOptions,
	lge_CampaignClock,
	lge_RoutePlanner,
	lge_AIpasses,
	lge_Quick,
	lge_Best,

	// Water

	lge_NavalBattle,
	lge_CSAflotilla,
	lge_attackUSAflotilla,
	lge_USAflotilla,
	lge_attackCSAflotilla,
	lge_forceRetreat,
	lge_hadToWithdraw,
	lge_USAlost,
	lge_USAnoDamage,
	lge_CSAlost,
	lge_CSAnoDamage,
	lge_nothing,

	/*
	 * campunit
	 */

	lge_OrderByLand,
	lge_OrderByAny,

	/*
	 * New for master's edition
	 */


	lge_RemoteDifferentVersion,

	/*
	 * Stats screen
	 */

	lge_statHint,
	lge_statStrength,
	lge_statCasualties,
	lge_statTotal



};


char* language(LGE msg);
void setup_language();
Boolean isLanguage(const char* ext);


#endif /* LANGUAGE_H */

