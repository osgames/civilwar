/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef DB_DPLAY_H
#define DB_DPLAY_H

#ifndef __cplusplus
#error db_dplay.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Database dPlay definition
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "db_wins.h"
#include "sprlib.h"

#if defined(SOUND_DSMI)
#include "amp.h"
#else
struct MODULE { };
#endif

/*
 * Undefined classes
 */

class Index;

/*
 * Typedefs
 */

typedef int arpos[32][5];

/*
 *   Handles the main program and the screen setup data;;
 */


class Dplay : public SetUpWins {

	protected:
		enum status { YES, NO };
		status quit;
		
		enum flag { ON, OFF };
		flag picbit;

#ifdef SWG_REMOVED
		GameSprites *di;
#endif
		char skipPtr;

		Index *index;
		char ind;
 		short int *totalWidth ;
		short int *totalHeight;
		short int *text_xpos  ;
		short int *text_ypos  ;
		char page;
		char tret;
		unsigned char keytotal;
		FontID n;
		//Image *under;
		//Image *untitle;
		//Image *unjob;
		char HowMany;
		arpos positions; // [10][5]; 
		// int BasePtrOffset;

		SpriteLibrary *biolib;
		SpriteLibrary *opiconlib;

   	int sprlibflags;
		SpriteIndex sprIndex;

		MODULE* song;
		int playState;

		long int *Newspage;
		unsigned char NewsNum;

	public:
		Dplay();
		void SetUpWindows();
		void terminate();
		void mainloop();
		void SetNonText();
		void SetText( char dis = 0, int reset = 1 );
		void getIndex();
		void Dplay::getRetIndex();
		// void displayIndex();
		void PrevPara();
		void NextPara();
		void getKeywordDisplay();
		void getIcon();
		void UseMemory();
		char SetUpDi( int count, int reset = 1 );
		int getBPO();
		void getPic();
		void sortKeytotal();
		
		void playsong( int songnumber );
		void disFlic( int flicNum );
						
		short int getTW( char loop ) { return totalWidth [ loop ]; }
		short int getTH( char loop ) { return totalHeight [ loop ]; }
		short int getTX( char loop ) { return text_xpos [ loop ]; }
		short int getTY( char loop ) { return text_ypos [ loop ]; }

		void waitOnMouse();
		void wipeOut( Boolean returnFlag = 0 );
		void wipeIn();
};

#endif /* DB_DPLAY_H */

