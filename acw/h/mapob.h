/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MAPOB_H
#define MAPOB_H

#ifndef __cplusplus
#error mapob.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Map Objects
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.23  1994/09/23  13:29:31  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.22  1994/07/25  20:34:58  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.21  1994/07/13  13:52:35  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.20  1994/06/29  21:40:42  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.19  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.18  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.17  1994/06/09  23:36:46  Steven_Green
 * Added constructor without LOcation
 *
 * Revision 1.16  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.14  1994/05/21  13:18:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/04/20  22:24:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1994/04/11  13:38:20  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/04/06  12:42:17  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/03/11  23:14:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/03/10  14:29:08  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/03/01  22:30:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/02/28  23:05:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/02/17  20:00:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/02/15  23:24:13  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/02/03  15:05:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/01/20  20:04:54  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifndef GR_TYPES_H
#include "gr_types.h"
#endif

#ifndef MEASURE_H
#include "measure.h"
#endif
#include "ptrlist.h"
#include "gametime.h"

/*
 * Undefined Classes
 */

class Region;
class MapWindow;

class System3D;


typedef UBYTE ObjectType;

enum ObjectType_enum {
	OT_Unit,							// Regiment/Brigade/etc
	OT_Facility,					// City/etc
	OT_WaterZone,					// Water Zone
	OT_State,
	OT_Temporary,					// Temporary object
	OT_Other							// Something else!
};

enum MapPriority {
	Map_NotVisible,	// Special case for non-displayed object

	/*
	 * These are in order with top priority 1st.
	 */

	Map_Highlight,		// Highlighted objects get top priority
	Map_WaterZone,
	Map_Army,
	Map_Corps,
	Map_Division,
	Map_Brigade,
	Map_City,
	Map_Facility,
	Map_State
};


/*
 * An object somewhere in the play world.
 *
 * This is intended to be used as a base class for real objects
 */

class MapObject {
public:
	Location location;
#if defined(NOBITFIELD)
	ObjectType obType;
	Boolean highlight;			// Used to be 5 bits, but seems to only be 0 or 1
	Boolean alwaysDraw;		// Set to always call mapDraw regardless of window
#else
	ObjectType obType:3;
	Boolean highlight:1;			// Used to be 5 bits, but seems to only be 0 or 1
	Boolean alwaysDraw:1;		// Set to always call mapDraw regardless of window
#endif
public:
	MapObject();
	MapObject(ObjectType type);
	MapObject(const Location& l, ObjectType type);
	virtual ~MapObject();
	virtual void mapDraw(MapWindow* map, Region* bm, Point where);				// Class dependant draw function
	virtual MapPriority isVisible(MapWindow* map) const;		// Returns Display Priority
#if defined(CAMPEDIT) || defined(BATEDIT)
	virtual void showInfo(Region* window);
#else
	virtual void showCampaignInfo(Region* window) { }

	virtual void moveCampaign(GameTicks ticks, const TimeBase& timeNow) { }
	virtual void moveBattle(GameTicks ticks, const TimeBase& timeNow) { }
	virtual void draw3D(System3D& drawData) { }
#endif
};

#endif /* MAPOB_H */
