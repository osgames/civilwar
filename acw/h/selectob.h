/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SELECTOB_H
#define SELECTOB_H

#ifndef __cplusplus
#error selectob.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	 Selectob is a child class of CLTO, which is used to select an AI
 *  objective. i.e To defend what AI has, and whether to attack a nearby
 *  Victory Point Valued facility.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

class CampaignAI;

enum retValue { FAIL = 0, EQUAL, PASS, FAIL_BADLY };

enum CLTOtype {
	USA_Quick,
	USA_Economic,
	USA_BF_long,
	USA_NF_long,
	USA_BF_short,
	USA_NF_short,
	USA_Attrition,

 	CSA_Quick,
	CSA_NF_long,
	CSA_NF_short,
	CSA_Defend_all,
	CSA_Rolling_Defend
};

class SelectOB {

		CLTOtype cltobj;

	public:
		CampaignAI *AI;

		SelectOB();
		~SelectOB();

		void SetCLTO( const CLTOtype cltob ) {	cltobj = cltob; }
		void setAI( CampaignAI *campaignAI ) { AI = campaignAI; }

		void AmIOccopyingEnemyF();
		void SelectTarget();

};

#endif /* SELECTOB_H */
