/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef GAMETIME_H
#define GAMETIME_H

#ifndef __cplusplus
#error gametime.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Game Time definition
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.10  1994/09/02  21:27:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/07/28  19:01:36  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/07/25  20:34:58  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.5  1994/05/21  13:18:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/03/11  23:14:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/03/10  14:29:08  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/02/28  23:05:43  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

/*
 * Basic Unit of Time is Ticks since 1st Jan 1860
 */

typedef unsigned long GameTicks;

#define UndefinedTime GameTicks(-1)


typedef UBYTE Hour;
typedef UBYTE Minute;
typedef UBYTE Second;
typedef UWORD Day;

enum Month {
	January,
	February,
	March,
	April,
	May,
	June,
	July,
	August,
	September,
	October,
	November,
	December
};

typedef UWORD Year;

const Year startYear = 1860;
const Second SecondsPerMinute = 60;
const Minute MinutesPerHour = 60;
const Hour HoursPerDay = 24;
const Day DaysPerYear = 365;
const int SecondsPerHour = SecondsPerMinute*MinutesPerHour;
const int SecondsPerDay = SecondsPerHour*HoursPerDay;
const int SecondsPerYear = SecondsPerDay*DaysPerYear;

const GameTicks GameTicksPerSecond = 100;
const GameTicks GameTicksPerMinute = (SecondsPerMinute * GameTicksPerSecond);
const GameTicks GameTicksPerHour   = (SecondsPerHour   * GameTicksPerSecond);
const GameTicks GameTicksPerDay    = (SecondsPerDay    * GameTicksPerSecond);
const GameTicks GameTicksPerYear   = (SecondsPerYear   * GameTicksPerSecond);


Day 		 dateToDays (Day d,  Month m,  Year y);
GameTicks timeToTicks(Hour h, Minute m, Second s);


class TimeBase {
public:
	Day days;						// Days since start date (1st Jan 1860)
	GameTicks ticks;				// Ticks since midnight tonight
public:
	TimeBase(Day day, GameTicks v) { set(day, v); }
	TimeBase() { days = 0; ticks = 0; }
	TimeBase(Day d, Month m, Year y, Hour h, Minute min) { set(d,m,y,h,min); }

	operator GameTicks() const { return days * GameTicksPerDay + ticks; }

	void set(Day day, GameTicks t) { ticks = t; days = day; }
	void set(Day d, Month m, Year y, Hour h, Minute min)
	{
		days = dateToDays(d, m, y);
		ticks = timeToTicks(h,min,0);
	}

	GameTicks getTime();

	void addGameTicks(GameTicks v);	// { ticks += v; }

	void advanceTime(GameTicks t);
	void setTime(GameTicks t);

	// GameTicks operator -(const TimeBase& t);	// { ticks -= t.ticks; return ticks; }
	const TimeBase& operator -= (const TimeBase& t);
	TimeBase operator - (const TimeBase& t) const
	{
		TimeBase nt = *this;
		nt -= t;
		return nt;
	}
	
	const TimeBase& operator -= (GameTicks v);
	TimeBase operator - (GameTicks v) const
	{
		TimeBase nt = *this;
		nt -= v;
		return nt;
	}

	const TimeBase& operator += (GameTicks v);
	TimeBase operator + (GameTicks v) const
	{
		TimeBase t = *this;
		t += v;
		return t;
	}


	int operator <=(const TimeBase& t);
	int operator >(const TimeBase& t);

	int operator ! () const { return ((days == 0) && (ticks == 0)); }
};

class GameTime : public TimeBase {
	int timeRate;			// How many times faster than real time?

public:
	GameTime();
	GameTime(Day d, GameTicks t);

	GameTime& operator = (const TimeBase& t)
	{
		days = t.days;
		ticks = t.ticks;
		return* this;
	}

	void addRealTime(unsigned int timerTicks);
	GameTicks realToGameTicks(unsigned int timerTicks);

	void setCampaignTime();

	// void setBattleTime() { timeRate = (60 / 5); }			// 1 Hour = 5 minutes
	void setRate(int rate) { timeRate = rate; }

	// void speedUp() { if ( timeRate < 32768 ) timeRate <<= 1; }
	// void slowDown() { if ( timeRate > 1 ) timeRate >>= 1; }
};

class TimeInfo {
public:
	Day day;
	Month month;
	Year year;
	Hour hour;
	Minute minute;
	Second second;

	char monthStr[5];

	GameTicks ticksSinceMidnight;

	TimeInfo(const TimeBase &v);
	TimeInfo() { }

	TimeInfo& operator = (const TimeBase &v) { *this = TimeInfo(v); return *this; }

	void setRealTime();
};


inline GameTicks timeToTicks(Hour h, Minute m, Second s)
{
	return GameTicksPerSecond * (s + SecondsPerMinute * (m + h * MinutesPerHour));
}



const GameTicks SunSet  = timeToTicks(20, 0, 0);		// 8pm
const GameTicks SunRise = timeToTicks(7, 0, 0);			// 7am

#endif /* GAMETIME_H */

