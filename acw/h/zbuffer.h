/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef ZBUFFER_H
#define ZBUFFER_H

#ifndef __cplusplus
#error zbuffer.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	3D ZBuffer Implementation
 *
 * This replaces the scanline section buffer that I originally used
 * which turned out to use more memory and be slower!
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */


#include "types3d.h"
class Region;
class Sprite;

#ifdef FOURMEG
typedef UBYTE ZSIZE;
#else
typedef UWORD ZSIZE;
#endif


class ZBuffer {
	SDimension width;
	SDimension height;
	ZSIZE* zValues;

public:
	ZBuffer();
	~ZBuffer();
	
	void init(const Point& size);
	void clear();

	ZSIZE& get(SDimension x, SDimension y)
	{
		return zValues[x + y * width];
	}

	ZSIZE& get(const Point& p)
	{
		return zValues[p.x + p.y * width];
	}

	// void add(SDimension y, ScanSegment* seg);
	// void draw(System3D& drawData);
	void blitSprite(Region* region, const Sprite* sprite, const Point3D& pt) const;

	Boolean findPoint(const Point& p, Point3D& result);
};


#define BackZ ZSIZE(-1)		// Maximum possible Z Value

#ifdef FOURMEG

#ifdef BATEDIT
  inline ZSIZE zToScanZ(Cord3D z) { return z >> 13; }
  inline Cord3D scanZtoZ(ZSIZE z) { return z << 13; }
#else
  inline ZSIZE zToScanZ(Cord3D z) { return z >> 10; }
  inline Cord3D scanZtoZ(ZSIZE z) { return z << 10; }
#endif

#else

#ifdef BATEDIT
  inline ZSIZE zToScanZ(Cord3D z) { return z >> 5; }
  inline Cord3D scanZtoZ(ZSIZE z) { return z << 5; }
#else
  inline ZSIZE zToScanZ(Cord3D z) { return z >> 2; }
  inline Cord3D scanZtoZ(ZSIZE z) { return z << 2; }
#endif

#endif

#endif /* ZBUFFER_H */

