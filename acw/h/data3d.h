/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef DATA3D_H
#define DATA3D_H

#ifndef __cplusplus
#error data3d.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	3D System data
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.5  1994/09/23  13:29:31  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/08/31  15:26:29  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/08/19  17:30:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/08/09  21:31:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/08/09  15:46:26  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "view3d.h"
#include "terrain.h"

#ifdef USE_SCANBUF
#include "scanbuf.h"
#else
#include "zbuffer.h"
#include "zpoly.h"
#endif

/*
 * Overall structure with things required for 3D drawing
 */

struct System3D {
public:
	ViewPoint  view;
	LightSource light;
	Terrain terrain;
#ifdef USE_SCANBUF
	ScanBuffer scanBuffer;
#else
	ZBuffer zBuffer;
	TriangleList triangles;
#endif
	Region* bitmap;

#ifdef USE_SCANBUF
	void render(Region* bm);
#else
	void render();
#endif

	void renderSquare(const Point3DI& p1, const Point3DI& p2, const Point3DI& p3, const Point3DI& p4, TerrainType terrain);
	void renderTriangle(const Point3DI& p1, const Point3DI& p2, const Point3DI& p3, TerrainType terrain);

	Boolean screenToBattleLocation(const Point& p, BattleLocation& location);

private:
#ifdef USE_SCANBUF
	void   clipTriangle(const Point3DI& p1, const Point3DI& p2, const Point3DI& p3, TerrainType ter);
#else
	void drawTriangle(const Point3DI& p1, const Point3DI& p2, const Point3DI& p3, TerrainType ter);
#endif
};

#endif /* DATA3D_H */

