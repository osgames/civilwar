/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef DBICON_H
#define DBICON_H

#ifndef __cplusplus
#error dbicon.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Used to be testicon.h
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "icon.h"
#include "datab.h"
#include "menudata.h"

/*										 
 * Define an Icon class called ChrisIcon
 *
 * This is derived from the Icon class (see icon.h).
 *
 * execute() and drawIcon() are overloaded virtual functions
 *
 * The colour variables are just an example of how an icon might work.
 * This icon will change its colour when clicked with the left mouse
 * button, and exit the program when right clicked.
 */

class ChrisIcon : public Icon {
	
	Colour backColour;
	char chicon;
#ifdef SWG_REMOVED
	GameSprites kpic;
	GameSprites highlite;
#endif
	char choice;
	char *test;
	char ind;
	Index *index;
	char maxIcon;
	Point mousepos;
	static int IconNum;
	
	short int iconflag;
	short int extendif;

	static int lastkey;

public:
	ChrisIcon(IconSet* parent, Point where, char loop, char *which, short int iflag, short int extendif ); 
#ifdef SWG_REMOVED
	ChrisIcon(IconSet* parent, Point where, GameSprites pic, GameSprites bac, char* which, int again, short int iflag, short int extendif);
#endif
	ChrisIcon(IconSet* parent, Point where, char* which, char loop, char numkeys, short int iflag, short int extendif);
	ChrisIcon(IconSet* parent, Point where, char* which, int positions[][5], char loop );
	ChrisIcon(IconSet* parent, Point where, char* which, char loop, Index* indptr, short int iflag );

	void execute(Event* event, MenuData* d);

	char getChoice()
				{ return choice; }
	
	void drawIcon();
	void setIndex( Index *ind ) { index = ind; }
	// void clearIndex() { delete index; }
};

					 
/*
 * Define a control class for the testIcon function
 *
 * It is derived from MenuData (see menudata.h), which is
 * a handy class for handling an icon based screen, which
 * automatically looks after the mouse clicking, icon management
 * and screen updating.
 */

 
class TestIconControl : public MenuData {

public:
 
 char which;

protected:
 
 char iconNumber;
 char keynum;
	
public:

#ifdef SWG_REMOVED
	TestIconControl( GameSprites di[], char HowMany, char numIcons, int positions[][5] );
	TestIconControl( GameSprites di[], char HowMany, char numIcons, int positions[][5], Index *ind );
#else
	TestIconControl(char HowMany, char numIcons, int positions[][5] );
	TestIconControl(char HowMany, char numIcons, int positions[][5], Index *ind );
#endif
};

#endif /* DBICON_H */

