/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef ARRAY_H
#define ARRAY_H

#ifndef __cplusplus
#error array.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Array class
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:12  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.5  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/06/07  18:33:23  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/05/21  13:18:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/05/19  17:47:49  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <string.h>
#include "types.h"
#include "error.h"

template<class Type>
class Array {
public:
	UWORD count;
	Type* items;
public:
	Array() { count = 0; items = 0; }
	~Array();

#ifdef DEBUG
	Array(Array<Type>& t)
	{
		throw GeneralError("Array<Type> copy constructor unwritten");
	}
#endif


	void init(UWORD howMany);

	Type& get(UWORD n) const
	{
		if(n < count)
			return items[n];
#if 1
		else
			throw GeneralError("Array out of range (id=%d)", (int)n);
#else
		else
			return items[0];
#endif
	}

	void set(UWORD n, Type& f) { if(n < count) items[n] = f; }
	void set(UWORD n, Type* f) { if(n < count) items[n] = *f; }

	Type& operator[] (UWORD n)
	{
		if(n < count)
			return items[n];
#if 1
		else
			throw GeneralError("Array out of range (id=%d)", (int)n);
#else
		else
			return items[0];
#endif
	}

	Array<Type>& operator = (Array<Type>& src)
	{
		count = src.count;

		if(count)
		{
			items = new Type[count];
			for(int i = 0; i < count; i++)
				items[i] = src.items[i];
		}
		else
			items = 0;
		return *this;
	}

	UWORD entries() const { return count; }

	int getID(const Type* item) const
	{
		int id = item - items;
		if( (id >= 0) && (id < count) )
			return id;
		else
			return -1;
	}

	void clear()
	{
		if(items)
		{
			delete[] items;
			items = 0;
			count = 0;
		}
	}
};

template<class Type>
class ArrayIter {
	Array<Type>* container;
	UWORD index;
public:
	ArrayIter(Array<Type>& list) { container = &list; index = -1; }

	int operator++()
	{
		index++;

		if(index < container->entries())
			return 1;
		else
			return 0;
	}

	Type& current() const { return container->get(index); }
};

template<class Type>
void Array<Type>::init(UWORD howMany)
{
	if(items)
		delete[] items;

	items = new Type[howMany];
	if(items)
		count = howMany;
	else
		count = 0;
}

template<class Type>
Array<Type>::~Array()
{
	if(items)
		delete[] items;
}


#if 0
/*
 * Same as above, but is an array of pointers to Type
 */

template<class Type>
class ArrayPtr {
	UWORD count;
	Type** items;
public:
	ArrayPtr() { count = 0; items = 0; }
	~ArrayPtr();

#ifdef DEBUG
	ArrayPtr(ArrayPtr<Type>& t)
	{
		throw GeneralError("ArrayPtr<Type> copy constructor unwritten");
	}

	ArrayPtr<Type>& operator = (ArrayPtr<Type>& t)
	{
		throw GeneralError("ArrayPtr<Type> = function unwritten");
	}
#endif

	void init(UWORD howMany);

	Type* get(UWORD n) const
	{
		if(n < count)
			return items[n];
		else
		{
			throw GeneralError("Array out of range");
		}
	}

	void set(UWORD n, Type* f) { if(n < count) items[n] = f; }

	UWORD entries() const { return count; }
};

template<class Type>
class ArrayPtrIter {
	ArrayPtr<Type>* container;
	UWORD index;
public:
	ArrayPtrIter(ArrayPtr<Type>& list) { container = &list; index = -1; }

	int operator++()
	{
		index++;

		if(index < container->entries())
			return 1;
		else
			return 0;
	}

	Type* current() const { return container->get(index); }
};

template<class Type>
void ArrayPtr<Type>::init(UWORD howMany)
{
	if(items)
		delete[] items;

	items = new Type*[howMany];
	if(items)
		count = howMany;
	else
		count = 0;
}

template<class Type>
ArrayPtr<Type>::~ArrayPtr()
{
	if(items)
		delete[] items;
}

#endif

/*===========================================================
 * Array designed for adding/removing items
 */

template<class Type>
class DynamicArray {
	UWORD used;				// How many entries are used
	UWORD alloced;			// How many entries are allocated
	Type* items;
public:
	DynamicArray() { used = 0; alloced = 0; items = 0; }
	~DynamicArray() { clear(); }

#ifdef DEBUG
	DynamicArray(DynamicArray<Type>& t)
	{
		throw GeneralError("DynamicArray<Type> copy constructor unwritten");
	}

#if 0
	DynamicArray<Type>& operator = (DynamicArray<Type>& t)
	{
		throw GeneralError("DynamicArray<Type> = function unwritten");
	}
#endif

#endif

	void init(UWORD howMany);

	void clear()
	{
		if(items)
		{
			delete[] items;
			items = 0;
			used = 0;
			alloced = 0;
		}
	}

	DynamicArray<Type>& operator = (DynamicArray<Type>& src)
	{
		used = src.used;
		alloced = src.alloced;

		if(alloced)
		{
			items = new Type[alloced];
			for(int i = 0; i < used; i++)
				items[i] = src.items[i];
		}
		else
			items = 0;
		return *this;
	}

	Type& get(UWORD n) const
	{
		if(n < used)
			return items[n];
		else
		{
			throw GeneralError("DynamicArray out of range");
		}
	}

	void set(UWORD n, Type& f) { if(n < used) items[n] = f; }
	void set(UWORD n, Type* f) { if(n < used) items[n] = *f; }

	Type& operator[] (UWORD n)
	{
		if(n < used)
			return items[n];
#if 1
		else
			throw GeneralError("Array out of range (id=%d)", (int)n);
#endif
	}

	UWORD entries() const { return used; }

	void removePtr(Type* item);	// Remove an item
	void removeID(UWORD id);

	Type* add();					// Add a new item
	void add(Type& t);

	int getID(const Type* item) const
	{
		int id = item - items;
		if( (id >= 0) && (id < used) )
			return id;
		else
			return -1;
	}
};

template<class Type>
class DynamicArrayIter {
	DynamicArray<Type>* container;
	UWORD index;
public:
	DynamicArrayIter(DynamicArray<Type>& list) { container = &list; index = -1; }

	int operator++()
	{
		index++;

		if(index < container->entries())
			return 1;
		else
			return 0;
	}

	Type& current() const { return container->get(index); }
};

template<class Type>
void DynamicArray<Type>::init(UWORD howMany)
{
	if(items)
	{
		delete[] items;
		items = 0;
	}

	if(howMany)
	{
		items = new Type[howMany];
		if(items)
		{
			used = alloced = howMany;
		}
		else
			used = alloced = 0;
	}
	else
		used = alloced = 0;
}

template<class Type>
void DynamicArray<Type>::removeID(UWORD id)
{
	if( (id >= 0) && (id < used) )
	{
		used--;

		while(id < used)
		{
			items[id] = items[id+1];
			id++;
		}
	}
	else
		throw GeneralError("Trying to remove illegal item (id=%d)\n"
								"from Dynamic Array",
									id);
}

template<class Type>
void DynamicArray<Type>::removePtr(Type* item)
{
	int id = item - items;
	if( (id >= 0) && (id < used) )
	{
		used--;
		Type* t = item;

		while(id < used)
		{
			t[0] = t[1];
			t++;
			id++;
		}
	}
	else
		throw GeneralError("Trying to remove illegal item (id=%d)\n"
								"from Dynamic Array",
									id);
}


template<class Type>
Type* DynamicArray<Type>::add()
{
	if(used == alloced)
	{
		Type* newItems = new Type[++alloced];

		if(items)
		{
			int i = 0;
			while(i < used)
			{
				newItems[i] = items[i];
				i++;
			}
			delete[] items;
		}

		items = newItems;
	}

	Type* it = &items[used++];

	return it;
}

template<class Type>
void DynamicArray<Type>::add(Type& t)
{
	Type* ptr = add();
	*ptr = t;
}

#endif /* ARRAY_H */

