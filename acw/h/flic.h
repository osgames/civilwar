/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef FLIC_H
#define FLIC_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	General structures used by flic files
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/09/23  13:29:31  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * This section taken and modified from:
 *----------------------------------------------------------------------
 * Flic.h - header file containing structure of a flic file. 
 *
 * Copyright (c) 1992 Jim Kent.  This file may be freely used, modified,
 * copied and distributed.  This file was first published as part of
 * an article for Dr. Dobb's Journal March 1993 issue.
 *----------------------------------------------------------------------
 */


/* Flic Header */

struct FlicHead {
	LONG	size;		/* Size of flic including this header. */
	UWORD type;		/* Either FLI_TYPE or FLC_TYPE below. */
	UWORD	frames;		/* Number of frames in flic. */
	UWORD	width;		/* Flic width in pixels. */
	UWORD	height;		/* Flic height in pixels. */
	UWORD	depth;		/* Bits per pixel.  (Always 8 now.) */
	UWORD	flags;		/* FLI_FINISHED | FLI_LOOPED ideally. */ 
	LONG 	speed;		/* Delay between frames. */
	WORD	reserved1;	/* Set to zero. */
	ULONG	created;	/* Date of flic creation. (FLC only.) */
	ULONG	creator;	/* Serial # of flic creator. (FLC only.) */
	ULONG	updated;	/* Date of flic update. (FLC only.) */
	ULONG	updater;	/* Serial # of flic updater. (FLC only.) */
	UWORD	aspect_dx;	/* Width of square rectangle. (FLC only.) */
	UWORD	aspect_dy;	/* Height of square rectangle. (FLC only.) */
	BYTE 	reserved2[38];/* Set to zero. */
	LONG 	oframe1;	/* Offset to frame 1. (FLC only.) */
	LONG 	oframe2;	/* Offset to frame 2. (FLC only.) */
	BYTE 	reserved3[40];/* Set to zero. */
};

/* Values for FlicHead.type */

#define FLI_TYPE 0xAF11u	/* 320x200 .FLI type ID */
#define FLC_TYPE 0xAF12u	/* Variable rez .FLC type ID */
#define FLZ_TYPE 0xAF1fu	/* Variable rez LZ encoded type ID */

/* Values for FlicHead.flags */

#define FLI_FINISHED 0x0001
#define FLI_LOOPED	 0x0002

/* Optional Prefix Header */

struct PrefixHead {
	LONG size;		/* Size of prefix including header. */
	UWORD type;	/* Always PREFIX_TYPE. */
	WORD chunks;	/* Number of subchunks in prefix. */
	BYTE reserved[8];/* Always 0. */
};

/* Value for PrefixHead.type */

#define PREFIX_TYPE  0xF100u


/* Frame Header */

struct FrameHead {
	LONG size;		/* Size of frame including header. */
	UWORD type;	/* Always FRAME_TYPE */
	WORD chunks;	/* Number of chunks in frame. */
	BYTE reserved[8];/* Always 0. */
};

/* Value for FrameHead.type */

#define FRAME_TYPE 0xF1FAu


/* Chunk Header */

struct ChunkHead {
	LONG size;		/* Size of chunk including header. */
	UWORD type;	/* Value from ChunkTypes below. */
};

enum ChunkTypes 
{
	COLOR_256 = 4,	/* 256 level color pallette info. (FLC only.) */
	DELTA_FLC = 7,	/* Word-oriented delta compression. (FLC only.) */
	COLOR_64 = 11,	/* 64 level color pallette info. */
	DELTA_FLI = 12,	/* Byte-oriented delta compression. */
	BLACK = 13,		/* whole frame is color 0 */
	BYTE_RUN = 15,	/* Byte run-length compression. */
	LITERAL = 16,	/* Uncompressed pixels. */
	PSTAMP = 18,	/* "Postage stamp" chunk. (FLC only.) */

	/*
	 * These are extensions for .FLZ files
	 * They are similar to the above chunks but have LZW compression
	 * applied to them.
	 */

	DELTA_FLC_LZW = 0x100,
	DELTA_FLI_LZW = 0x101,
	BYTE_RUN_LZW  = 0x102,
	LITERAL_LZW   = 0x103,

};

/*
 * End of bit stolen from Jim Kent's reader
 *----------------------------------------------------------------------
 */



#ifdef __cplusplus
};
#endif

#endif /* FLIC_H */

