/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef TEST3D_H
#define TEST3D_H

#ifndef __cplusplus
#error test3d.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	3D Shape Tester
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.5  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1993/12/14  23:30:30  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1993/12/13  14:11:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/12/10  16:08:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/30  02:57:48  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifndef TYPES_H
#include "types.h"
#endif


extern Boolean quitProgram;


#define SHOW_RENDER
// #undef SHOW_RENDER

#ifdef SHOW_RENDER
class Mouse;
extern Mouse* globalMouse;
#endif

#endif /* TEST3D_H */

