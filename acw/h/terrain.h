/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef TERRAIN_H
#define TERRAIN_H

#ifndef __cplusplus
#error terrain.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Terrain Class
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.10  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.3  1993/12/23  09:29:10  Steven_Green
 * 256 Terrain types... colour and texture arrays added.
 *
 * Revision 1.2  1993/12/21  00:34:28  Steven_Green
 * Algorithm for colours changed to use only 4 colours
 *
 * Revision 1.1  1993/12/15  20:58:04  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include "gr_types.h"
#include "image.h"

/*
 * Constants
 */

#define MAX_TERRAIN_COL 8				// Maximum different colours
#define TEXTURE_COUNT	4				// 4 Different textures


#define RiverTerrain 0
#define RiverTerrainCount 16

#define Road1Terrain (RiverTerrain+RiverTerrainCount)
#define Road1TerrainCount 16

#define Road2Terrain (Road1Terrain+Road1TerrainCount)
#define Road2TerrainCount 16

#define RailwayTerrain (Road2Terrain+Road2TerrainCount)
#define RailwayTerrainCount 16

#define NormalTerrain_0_0 (Road2Terrain+Road2TerrainCount)
#define NormalTerrain (RailwayTerrain+RailwayTerrainCount)

/*
 * Old terrain enumerations
 */

enum TerrainType_0_0 {		// For version 0.0
	BuiltupTerrain_0_0 = NormalTerrain_0_0,
	HeavyWoodTerrain_0_0,
	LightWoodTerrain_0_0,
	HeavyOrchardTerrain_0_0,
	LightOrchardTerrain_0_0,
	Field1aTerrain_0_0,			// -> Pasture
	Field1bTerrain_0_0,			// -> Ploughed Field
	Field1cTerrain_0_0,			// -> Cornfield
	Field1dTerrain_0_0,			// -> Wheatfield
	Field2aTerrain_0_0,			// -> Orchard
	Field2bTerrain_0_0,			// -> Medium Wood
	Field2cTerrain_0_0,			// -> Meadow
	CampSiteTerrain_0_0,
	DeepWaterTerrain_0_0,		// -> Water
	ShallowWaterTerrain_0_0,	// -> Water
	MarshaTerrain_0_0,			// -> Marsh
	MarshbTerrain_0_0,			// -> Rough
	MarshcTerrain_0_0,			// -> Rocky Desert
	MarshdTerrain_0_0,			// -> Desert

	NormalTerrainCount_0_0
};

/*
 * Current Enumeration Types
 */

enum {
	BuiltupTerrain = NormalTerrain,
	HeavyWoodTerrain,
	LightWoodTerrain,
	HeavyOrchardTerrain,
	LightOrchardTerrain,
	PastureTerrain,						// Field1aTerrain,
	PloughedFieldTerrain,				//	Field1bTerrain,
	CornFieldTerrain,						// Field1cTerrain
	WheatFieldTerrain,					// Field1dTerrain,
	MeadowTerrain,
	CampSiteTerrain,
	WaterTerrain,
	MarshTerrain,
	RoughTerrain,
	RockyDesertTerrain,
	DesertTerrain,

#ifdef BATEDIT
	IllegalTerrain,
#endif

	NormalTerrainCount,
};

// #define NormalTerrainCount 8		// Set to match table in batltab

#define TerrainCount (NormalTerrain + NormalTerrainCount)


#define RiverBankTerrain MarshTerrain
#define RoadsideTerrain PastureTerrain
#define RailsideTerrain PastureTerrain


/*
 *  Types
 */

typedef UBYTE TerrainType;

struct HueSaturation {
	Hue hue;
	Saturation saturation;
};

class TerrainCol {
public:
	Hue hue;					// Hue and saturation are constant
	Saturation saturation;

	/*
	 * The following are variable
	 */

	Intensity minVol;
	Intensity maxVol;
	Boolean used;

	Colour baseColour;		// Where it has been allocated in the palette
	UBYTE colourRange;		// How many colours it has in the palette (0=unallocated)

	TerrainCol();

	void addIntensity(Intensity i);
};


struct TerrainVal {
#if defined(NOBITFIELD)
	UBYTE colour;			// 0..7
	UBYTE texture;			// 0..3
	UBYTE wooded;			// 0..3 0=no trees, 3=heavy trees
	UBYTE height;			// 0..3 (height of terrain in map grid heights (aprox 7 yards)
#else
	UBYTE colour:3;			// 0..7
	UBYTE texture:2;			// 0..3
	UBYTE wooded:2;			// 0..3 0=no trees, 3=heavy trees
	UBYTE height:2;			// 0..3 (height of terrain in map grid heights (aprox 7 yards)
#endif
	UBYTE infantrySpeed;		// 0..255 fraction of base speed
	UBYTE cavalrySpeed;
	UBYTE artillerySpeed;
	BYTE  fireModifier;		// Add to Fire Factor when a target.
	// Add more attributes here?
};


class PaletteManager {
	Colour minCol;
	Colour maxCol;

	Colour nextColour;


	TrueColour* colours;

public:
	PaletteManager(Colour min, Colour max);
	~PaletteManager();

	void makeColours(TerrainCol* c);
	void reset();
	void setPalette();
};


class Terrain {
	static HueSaturation colours[MAX_TERRAIN_COL];		// These are set up in batltab.cpp
	static TerrainVal terrains[];

	Image sampleTexture[TEXTURE_COUNT];
	TerrainCol colourInfo[MAX_TERRAIN_COL];		// These are set up in batltab.cpp


	PaletteManager colourUsage;

public:

	Terrain();
	~Terrain();

	const Image* getTexture(TerrainType t) const;

	TerrainCol* getColour(TerrainType t);
	void resetColours();

	void makeColours();
	void setPalette() { colourUsage.setPalette();	}

	UBYTE isWooded(TerrainType t) const
	{
		return terrains[t].wooded;
	}

	UBYTE getHeightAdjust(TerrainType t) const
	{
		return terrains[t].height;
	}

	const TerrainVal* getValues(TerrainType t) const
	{
		return &terrains[t];
	}
};

inline Boolean isWater(TerrainType t)
{
	return ((t >= RiverTerrain) &&
		     (t < (RiverTerrain + RiverTerrainCount))) ||
				(t == WaterTerrain);
}

inline Boolean isRiver(TerrainType t)
{
	return ((t >= RiverTerrain) &&
		     (t < (RiverTerrain + RiverTerrainCount)));
}


inline Boolean isRoad(TerrainType t)
{
	return ((t >= Road1Terrain) &&
		     (t < (Road2Terrain + Road2TerrainCount)));
}

inline Boolean isRoad1(TerrainType t)
{
	return ((t >= Road1Terrain) &&
		     (t < (Road1Terrain + Road1TerrainCount)));
}

inline Boolean isRoad2(TerrainType t)
{
	return ((t >= Road2Terrain) &&
		     (t < (Road2Terrain + Road2TerrainCount)));
}

inline Boolean isRail(TerrainType t)
{
	return ((t >= RailwayTerrain) &&
		     (t < (RailwayTerrain + RailwayTerrainCount)));
}


#endif /* TERRAIN_H */
