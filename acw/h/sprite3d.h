/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SPRITE3D_H
#define SPRITE3D_H

#ifndef __cplusplus
#error sprite3d.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	3D Sprites
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.18  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.17  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.16  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/06/07  18:33:23  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.14  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.12  1994/05/21  13:18:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/04/20  22:24:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/04/11  21:31:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/04/11  13:38:20  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/03/18  15:08:40  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/03/11  23:14:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/02/28  23:05:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/02/17  20:00:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/12/21  00:34:28  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


#ifndef SHAPE3D_H
#include "shape3d.h"
#endif

#ifndef SPRLIB_H
#include "sprlib.h"
#endif


class Sprite3D : public Shape3D {
	SpriteLibrary* library;
	SpriteIndex index;
public:
	Sprite3D(SpriteLibrary& l, SpriteIndex n) { library = &l; index = n; }

	Point3D draw(ObjectDrawData* drawData, const Position3D& position);
};

#endif /* SPRITE3D_H */

/* Depends on h\shape3d.h , touched on 04-15-94 at 11:59:50 */
/* Depends on h\shape3d.h , touched on 04-15-94 at 14:56:16 */
/* Depends on h\sprlib.h , touched on 04-20-94 at 17:46:10 */
/* Depends on h\shape3d.h , touched on 05-17-94 at 14:53:27 */
/* Depends on h\shape3d.h , touched on 05-17-94 at 15:10:09 */
/* Touched on 05-19-94 at 16:37:22 */
/* Touched on 05-19-94 at 17:41:08 */
/* Touched on 05-28-94 at 11:52:21 */
/* Touched on 05-28-94 at 12:29:05 */
/* Touched on 05-28-94 at 14:37:41 */
/* Touched on 05-31-94 at 13:45:19 */
/* Touched on 05-31-94 at 17:19:02 */
/* Touched on 06-01-94 at 10:57:04 */
/* Touched on 06-01-94 at 14:09:34 */
/* Touched on 06-03-94 at 17:23:20 */
/* Touched on 06-03-94 at 17:59:58 */
/* Touched on 06-07-94 at 11:51:05 */
/* Touched on 06-08-94 at 12:30:22 */
/* Touched on 06-09-94 at 12:22:31 */
/* Touched on 06-09-94 at 18:31:03 */
/* Touched on 06-10-94 at 14:33:50 */
/* Touched on 06-13-94 at 14:25:20 */
/* Touched on 06-13-94 at 16:08:55 */
/* Touched on 06-23-94 at 15:57:56 */
/* Touched on 06-23-94 at 16:30:08 */
