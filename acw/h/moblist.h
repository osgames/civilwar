/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MOBLIST_H
#define MOBLIST_H

#ifndef __cplusplus
#error moblist.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Map Object Intrusive List
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/07/13  13:52:35  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "mapob.h"

#if 0
class MapObjectList : public PtrDList<MapObject> { };
typedef PtrDListIter<MapObject> MapObjectIter;
#endif

/*
 * Intrusive Map Object List
 */

class MobIL : public MapObject {
friend class MobIListIter;
friend class MobIList;
	MobIL* next;
	MobIL* prev;
public:
	MobIL();
	MobIL(const Location& l);
	virtual ~MobIL() { };
	MapPriority isVisible(MapWindow* map) const;
};

class MobIList {
friend class MobIListIter;
	MobIL* first;
	MobIL* last;
public:
	MobIList();

	void add(MobIL* item);
	void remove(MobIL* item);

	Boolean isEmpty() const { return !first; }
	void destroy();
};

class MobIListIter {
	MobIL* item;
public:
	MobIListIter(MobIList* l);
	MobIL* next();
};

#endif /* MOBLIST_H */

