/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef AIC_MISC_H
#define AIC_MISC_H

#ifndef __cplusplus
#error aic_misc.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign AI: Miscellaneous Support functions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:12  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */


#include "gamedef.h"
#include "measure.h"
#include "side.h"

class Facility;

CombatValue calc_nearCV(const Location& l, Distance d, Side side);
CombatValue calc_orderedCV(const Location& l, Side side);

Facility* findCloseFacility(const Facility* home, Side side);

/*
 * Some simple functions
 */

template<class T>
inline void addPercent(T& value, int p)
{
	if(p && value)
		value += (p * value) / 100;
}

template<class T>
inline void subPercent(T& value, int p)
{
	if(p && value)
		value -= (p * value) / 100;
}


template<class T>
inline void percent(T& value, int p)
{
	if(p && value)
		value = (p * value) / 100;
	else
		value = 0;
}

#endif /* AIC_MISC_H */

