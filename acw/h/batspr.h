/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *====================================================
 *
 * Sprite Definitions for battle.lst
 *
 *====================================================
 */

#define SPR_batflag 0
#define SPR_i_reload 16
#define SPR_i_shoot 112
#define SPR_i_walk 160
#define SPR_i_charge 352
#define SPR_i_stand 544
#define SPR_horses 592
#define SPR_stnd_hrs 736
#define SPR_cavalry 808
#define SPR_mt_infnt 1096
#define SPR_limbers 1384
#define SPR_canons 1456
#define SPR_c_fire_1 1528
#define SPR_gunfire1 1768
#define SPR_deadgun 1864
#define SPR_deadmanc 1888
#define SPR_deadmanu 1912
#define SPR_dedhorse 1936
#define SPR_earth 1960
#define SPR_sky 1961
#define SPR_batzoom 1962
#define SPR_trees 1977
#define SPR_church1 2007
#define SPR_church1d 2047
#define SPR_chapel_1 2087
#define SPR_house01 2119
#define SPR_house1d 2159
#define SPR_house11 2199
#define SPR_tavern1 2239
#define SPR_tavern1d 2279
#define SPR_tavern11 2319
#define SPR_tent 2359
#define SPR_silly 2363
/* Total number of sprites = 2366 */
