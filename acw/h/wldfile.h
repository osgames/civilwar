/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef WLDFILE_H
#define WLDFILE_H

#ifndef __cplusplus
#error wldfile.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	World Chunk File reading and writing
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

class Campaign;
class CampaignWorld;
class WriteWithPopup;
class ReadWithPopup;
class OrderBattle;


void readStartCampaign(const char* fileName, Campaign* campaign);
void writeStartCampaign(const char* fileName, Campaign* campaign);
void readStaticWorld(const char* fileName, CampaignWorld* world);

void readDynamicWorld(const char* fileName, Campaign* campaign);
void writeDynamicWorld(const char* fileName, Campaign* campaign);

void readSavedCampaign(ReadWithPopup& file, Campaign* campaign);
void writeSavedCampaign(WriteWithPopup& file, Campaign* campaign);

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
void writeCampaignBattleInfo(WriteWithPopup& file, Campaign* campaign);
void writeCampaignTemp(WriteWithPopup& file, Campaign* campaign);
void readCampaignBattleInfo(ReadWithPopup& file, Campaign* campaign, OrderBattle* ob);
void readCampaignTemp(ReadWithPopup& file, Campaign* campaign);
#endif

#endif /* WLDFILE_H */

