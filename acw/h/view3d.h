/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef VIEW3D_H
#define VIEW3D_H

#ifndef __cplusplus
#error view3d.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * 3D Viewpoint definition
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.30  1994/09/23  13:29:31  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.29  1994/09/02  21:27:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.28  1994/08/31  15:26:29  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.27  1994/08/24  15:07:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.26  1994/08/19  17:30:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.25  1994/08/09  15:46:26  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.24  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.23  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.22  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.21  1994/06/07  18:33:23  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.20  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.19  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.18  1994/05/21  13:18:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.17  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.16  1994/04/20  22:24:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/04/11  21:31:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.14  1994/04/11  13:38:20  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/04/06  12:42:17  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/03/18  15:08:40  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/02/28  23:05:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/02/17  20:00:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1993/12/13  22:02:51  Steven_Green
 * Light source is defined using bearing and height angles, which are
 * converted to unit x,y,z format later.
 *
 * Revision 1.6  1993/12/10  16:08:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1993/12/01  15:12:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1993/11/26  22:32:24  Steven_Green
 * Viewpoint changed to rotate around viewd point instead of eyepoint.
 *
 * Revision 1.3  1993/11/24  09:34:12  Steven_Green
 * width of viewing plane added to allow magnification.
 *
 * Revision 1.2  1993/11/19  19:01:25  Steven_Green
 * Functions simplified into a single transform() function.
 *
 * Revision 1.1  1993/11/16  22:43:10  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "error.h"
#include "types3d.h"
// #include "shape3d.h"
#include "trig.h"
#include "measure.h"

/*
 * Undefined classes
 */

class MapGrid;

/*
 * Allowable zoom levels
 */

enum ZoomLevel {
	Zoom_Min,

	Zoom_Half = Zoom_Min,
	Zoom_1,				// Begin with most zoomed in
	Zoom_2,
	Zoom_4,
	Zoom_8,
#ifdef BATEDIT
	Zoom_16,
	Zoom_32,
	Zoom_64,
	Zoom_Max = Zoom_64,
#else
	Zoom_Max = Zoom_8,
#endif

	ZOOM_LEVELS
};

/*
 * Viewpoint defined as what you are looking at
 *
 * Position3D is a point that the display should be centred on.
 * The position's angles are the direction from the viewpoint to the point.
 *
 * e.g. to look at the point from straight above then:
 *   xRotate = 0xc000 : Adjusts viewing height (anticlockwise)
 *   yRotate = 0		 : Rotates map plane (0=facing North, 0x4000=facing east)
 *   zRotate = 0		 : Tilts map plane (unused)
 */

#if defined(BATEDIT)
typedef WORD BattleGrid;
#else
typedef UWORD BattleGrid;
#endif



/*
 * Viewpoint defined from view.
 */

class ViewPoint {
	Cord3D eyeToPlane;			// Physical distance between viewing plane and eye
	Cord3D planeWidth; 			// Physical Width of viewing plane
	Cord3D eyeToPoint;			// distance from eye to viewed point
public:
	Cord3D xCentre;				// Point that is required to be in the centre
	Cord3D zCentre;

	Point3D focus;				// Where and direction the viewpoint is focused
	Wangle yRotate;			// Viewing angle (must be multiple of 90 degrees
	Wangle xRotate;			// Banking angle

	Point viewSize;				// Screen Size

	/*
	 * Area currently being viewed
	 */

	ZoomLevel zoomLevel;
	BattleGrid gridX;	// Left edge that is being viewed
	BattleGrid gridZ;	// Bottom edge being viewed
	BattleGrid gridW;	// How many grid squares across
	BattleGrid gridH;	// How many grid squares up

	BattleGrid bigGridX;	// On max zoom terrain map
	BattleGrid bigGridZ;
	BattleGrid bigGridW;
	BattleGrid bigGridH;

	Cord3D minX;	// Physical boundary being viewed
	Cord3D maxX;
	Cord3D minZ;
	Cord3D maxZ;

#ifdef OLD_BATEDIT
	Cord3D realMinX;	// Physical boundary being viewed
	Cord3D realMaxX;
	Cord3D realMinZ;
	Cord3D realMaxZ;
#endif

	/*
	 * Constructor
	 */


	ViewPoint()
	{
		set(0, 0, 0x0000, 0x1555, Zoom_8);
	}

	void set(const Cord3D x, Cord3D z, Wangle yRotate, Wangle xRotate, ZoomLevel zoom);
	void setZoom();

	void setDisplaySize(Point p)
	{
		viewSize = p;
	}

	void setBoundaries(const MapGrid* grid);
	void zoomIn(ZoomLevel level, const BattleLocation& location);
	void zoomIn(ZoomLevel level);
	Boolean zoomIn();
	Boolean zoomOut();

	Boolean isOnDisplay(const Location& l) const;
	
	Boolean isOnDisplay(const BattleLocation& l) const
	{
		return ((l.x >= minX) && (l.x <= maxX) && (l.z >= minZ) && (l.z <= maxZ));
	}

	void transform(Point3D& p) const;
	void unTransform(Point3D& p) const;

	Cord3D howBig();			// How much of the viewing plane should we show?


	void setDistance(Cord3D d) { eyeToPoint = d; };
	void moveDistance(Cord3D d) { eyeToPoint += d; };
	Cord3D getDistance() const { return eyeToPoint; };

	void setViewWidth(Cord3D w) { planeWidth = w; };
	Cord3D getViewWidth() const { return planeWidth; };
	void moveViewWidth(Cord3D w) { planeWidth += w; };

	void setFront(Cord3D d) { eyeToPlane = d; };
	void moveFront(Cord3D d) { eyeToPlane += d; };
	Cord3D getFront() const { return eyeToPlane; };

	SDimension getPixelWidth() const { return viewSize.x; }
	SDimension getPixelHeight() const { return viewSize.y; }
};


/*
 * Light Source
 */

class LightSource : public Point3D {
	Wangle height;
	Wangle bearing;
	Intensity ambience;		// ambient light level
	Intensity intensity;		// brightness of this light source
public:
	// LightSource() { set(Degree(30), 0, intensityRange/2, intensityRange/8); }
	LightSource();
	
	void set(Wangle h, Wangle b, Intensity i, Intensity a)
	{
		height = h;
		bearing = b;
		intensity = i;
		ambience = a;
		update();
	}

	void update();


	Wangle getHeight() const { return height; }
	Wangle getBearing() const { return bearing; }
	Intensity getIntensity() const { return intensity; }
	// Intensity getAmbience() const { return ambience; }

	Intensity makeIntensity(const Point3D& n);

	void setBearing(Wangle b) { bearing = b; }
	void setHeight(Wangle a) { height = a; }
	void setIntensity(Intensity i) { intensity = i; }
	void setAmbience(Intensity i) { ambience = i; }
};

inline UBYTE facingOctant(Wangle menFace, Wangle viewFace)
{
	return Wangle(0x1000 + 0x4000 - menFace - viewFace) >> 13;
}

#endif /* VIEW3D_H */

