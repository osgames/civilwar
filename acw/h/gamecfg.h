/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef GAMECFG_H
#define GAMECFG_H

#ifndef __cplusplus
#error gamecfg.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Values used in config File
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

extern const char configFileName[];
extern const char cfgSoundID[];
extern const char cfgMusicID[];
extern const char cfgRealismID[];

extern const char cfgPhoneID[];
extern const char cfgModemInitID[];
extern const char cfgDialID[];
extern const char cfgAnswerID[];
extern const char cfgCommPortID[];
extern const char cfgBaudID[];

extern const char cfgComm1IRQ[];
extern const char cfgComm2IRQ[];
extern const char cfgComm3IRQ[];
extern const char cfgComm4IRQ[];

extern const char cfgCampRealTime[];
extern const char cfgRouteFinder[];
extern const char* cfgRouteEnum[];

extern const char cfgAIpasses[];



#endif /* GAMECFG_H */

