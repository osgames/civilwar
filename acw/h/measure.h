/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MEASURE_H
#define MEASURE_H

#ifndef __cplusplus
#error measure.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Measurements and Distances
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.17  1994/09/23  13:29:31  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.16  1994/08/31  15:26:29  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/08/09  15:46:26  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.14  1994/07/19  19:55:29  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/07/13  13:52:35  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.9  1994/05/21  13:18:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/04/20  22:24:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/04/11  13:38:20  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/03/11  23:14:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/02/03  15:05:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/01/24  21:21:20  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/01/20  20:04:54  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/01/17  20:15:30  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <limits.h>
#include "gametime.h"
#include "trig.h"

/*
 * Forward reference
 */

// class BattleCord;
class BattleLocation;

/*
 * In order to make displaying objects relatively simple in terms
 * of calculations, the unit of distance is based on the pixel size
 * of the drawn maps.
 *
 * The complete map of America is 480 by 340 pixels and represents
 * 1280 Miles across.
 *
 * A zoomed in campaign map is 256 Miles in 480 pixels.
 *
 * Distance Unit is 1/65536 of a pixel on the zoomed in campaign map.
 *
 */

/*
 * Define some constants
 */

#define UnitsPerSection 480
#define MilesPerSection 256
#define YardsPerMile		1760

#define UnitYardLCD		160		// Lowest Common Denominator of 480 and 1760

#define UnitMod			(UnitsPerSection/UnitYardLCD)
#define YardMod			(YardsPerMile/UnitYardLCD)
#define MileMod			(65536/MilesPerSection)


/*
 * Base class to facilitate convertine between units, yards, miles, etc
 */


/*
 * Simpler method using less base classes, etc
 */

typedef long Distance;


inline int unitToYard(Distance d)
{
	return (d * YardMod) / (MileMod * UnitMod);	// = d*11/768
}

inline int unitToMile(Distance d)
{
	return d / (MileMod * UnitsPerSection);	// = d/122880
}

inline Distance Mile(long y)
{
	return y * MileMod * UnitsPerSection;
}

inline Distance Yard(long y)
{
	return (y * MileMod * UnitMod) / YardMod;
}

class Location {
public:	
	Distance x, y;

	Location(Distance x1, Distance y1) { x = x1; y = y1; }
	Location() { }
	Location(const BattleLocation& l) { *this = l; }

	Location& operator = (const BattleLocation& src);

	Location operator -=(const Location& l)
	{
		x -= l.x;
		y -= l.y;

		return *this;
	}

	Location operator -(const Location& l) const
	{
		Location r = *this;

		return r -= l;
	}

	Location operator +=(const Location& l)
	{
		x += l.x;
		y += l.y;

		return *this;
	}

	Location operator +(const Location& l) const
	{
		Location r = *this;

		return r += l;
	}

	Location& operator /=(int i)
	{
		x /= i;
		y /= i;
		return *this;
	}
	
	Location operator /(int i) const
	{
		Location ret;

		ret.x = x / i;
		ret.y = y / i;

		return ret;
	}

	int operator !=(const Location& l) const
	{
		return (x != l.x) || (y != l.y);
	}

	int operator ==(const Location& l) const
	{
		return (x == l.x) && (y == l.y);
	}
};

/*
 * Speed is stored as Distance/256 per ticks
 */


typedef long Speed;

inline Speed MilesPerHour(int mph)
{
	return (Distance)Mile(mph) / ((60 * 60 * GameTicksPerSecond) / 256);
}

inline Speed YardsPerHour(int yph)
{
	return (Distance)Yard(yph) / ((60 * 60 * GameTicksPerSecond) / 256);
}


/*
 * Functions
 */

Boolean moveLocation(Location& from, const Location& to, Speed speed, GameTicks ticks, Distance* dp = 0);
Boolean moveLocation(Wangle dir, Location& from, const Location& to, Speed speed, GameTicks ticks, Distance* dp = 0);

/*====================================================================
 * Battle coordinate system
 *
 * This is set up so that the battle field and adjacent area fits comfortably
 * into a 16 bit word allowing calculations that use 32 bit values to work
 * reasonably well.
 *
 * Maximum displayed battle area is 8 miles, but allow a border of a further
 * 8 miles each side (thus values up to 24 miles need to be represented).
 * Dividing DistBase's value by 64 makes this possible:
 *   24 miles = 2949120.  /64 => 46080
 *
 * 1 Battle Unit then represents 0.92 Yards
 */

typedef long Cord3D;
const maxCord3D = LONG_MAX;
const minCord3D = LONG_MIN;

struct BattleLocation {
	Cord3D x;
	Cord3D z;

	BattleLocation() { }

	BattleLocation(Cord3D x1, Cord3D z1)
	{
		x = x1;
		z = z1;
	}

	BattleLocation(const Location& l) { *this = l; }

	BattleLocation& operator = (const Location& src);

};




inline long BattleToDist(Cord3D b) { return (b) << 6; }
inline Cord3D DistToBattle(long d) { return (d) >> 6; }

inline Cord3D BattleMile(long y)
{
	return DistToBattle(Mile(y));
}


inline Cord3D BattleYard(long y)
{
	return DistToBattle(Yard(y));
}

inline BattleLocation& BattleLocation::operator =(const Location& src)
{
	x = DistToBattle(src.x);
	z = DistToBattle(src.y);
	return *this;
}

inline Location& Location::operator =(const BattleLocation& src)
{
	x = BattleToDist(src.x);
	y = BattleToDist(src.z);
	return *this;
}

inline Distance distanceLocation(const Location& l1, const Location& l2)
{
	return distance(l2.x - l1.x, l2.y - l1.y);
}

inline Wangle directionLocation(const Location& l1, const Location& l2)
{
	return direction(l2.x - l1.x, l2.y - l1.y);
}

#endif /* MEASURE_H */













 
