/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef POLYGON_H
#define POLYGON_H

#ifndef __cplusplus
#error polygon.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Polygon Class definition
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.28  1994/08/24  15:07:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.27  1994/08/19  17:30:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.26  1994/08/09  21:31:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.25  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.24  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.23  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.22  1994/06/07  18:33:23  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.21  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.20  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.19  1994/05/21  13:18:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.18  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.17  1994/04/20  22:24:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.16  1994/04/11  21:31:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/04/11  13:38:20  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.14  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1994/03/18  15:08:40  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/02/28  23:05:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/02/17  20:00:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1993/12/15  20:58:04  Steven_Green
 * Support for texturing.
 *
 * Revision 1.8  1993/12/14  23:30:30  Steven_Green
 * Preparation for Textured Polygons
 *
 * Revision 1.7  1993/12/10  16:08:02  Steven_Green
 * Gourad Polygons
 *
 * Revision 1.6  1993/12/04  16:26:00  Steven_Green
 * PolygonError exception handling class added.
 *
 * Revision 1.5  1993/12/01  15:12:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1993/11/24  09:34:12  Steven_Green
 * Shape base object used.
 *
 * Revision 1.3  1993/11/19  19:01:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/11/16  22:43:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/05  16:52:08  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#if 0

#include "gr_types.h"
#include "shape.h"
#include "shape3d.h"
#include "error.h"
#include "memptr.h"

class System3D;

class Region;
class Image;
class TerrainCol;

typedef UBYTE PolyPointIndex;

class PolygonError: public GeneralError {
public:
	PolygonError(): GeneralError("Polygon Error") { }
	PolygonError(char* s): GeneralError(s) { }
};

class NoMemory : public PolygonError {
public:
	NoMemory(): PolygonError("Polygon: No Memory") { }
};


/*
 * Base for a polygon
 */


template<class P>
class Polygon : public Shape {
	PolyPointIndex 	nPoints;			// Number of points used
	Boolean 				pointsAlloced;		// Set if we created the points array
	PolyPointIndex 	maxPoints;		// Points allocated
	P* 					points;			// Array of points

	Colour				colour;			// Colour to use for straight colours
	const Image*	 	pattern;			// Pattern to use for texture mapping
	TerrainCol*			terrainCol;
public:
				Polygon() { nPoints = 0; pointsAlloced = False; maxPoints = 0; colour = 0; pattern=0; }
				Polygon(PolyPointIndex nPoints, P* points = 0, TerrainCol* t = 0);
			  ~Polygon();

#ifdef DEBUG
	Polygon(Polygon<P>& t)
	{
		throw GeneralError("Polygon<P> copy constructor unwritten");
	}

	Polygon<P>& operator = (Polygon<P>& t)
	{
		throw GeneralError("Polygon<P> = function unwritten");
	}
#endif

	void		add(const P& p) { points[nPoints++] = p; };
	PolyPointIndex countPoints() const { return nPoints; }

	void		setTexture(const Image* t) 	{ pattern = t; }
	void 		setTerrainCol(TerrainCol* t) { terrainCol = t; }
	void		setColour(Colour c) 		{ colour = c; }

	/*
	 * These functions vary between polygon types.
	 */

	void render(System3D& drawData) const;
	void clipPoly(Polygon* destPoly, const Region* bitmap) const;
	void setTerrainColMinMax();
};


template<class P>
Polygon<P>::Polygon(PolyPointIndex nPoints, P* points, TerrainCol* t):
	terrainCol(t), pattern(0)
{
	maxPoints = nPoints;

	if(points)
	{
		Polygon<P>::points = points;
		Polygon<P>::nPoints = nPoints;
		pointsAlloced = False;
	}
	else
	{
		Polygon<P>::points = new P[nPoints];

		if(!Polygon<P>::points)
			throw NoMemory();

		Polygon<P>::nPoints = 0;
		pointsAlloced = True;
	}
}


template<class P>
Polygon<P>::~Polygon()
{
	if(pointsAlloced)
		delete[] points;
}



typedef Polygon<Point> Polygon2D;
typedef Polygon<Point3D> Polygon3D;
typedef Polygon<Point3DI> Polygon3DI;


#endif

#endif /* POLYGON_H */
