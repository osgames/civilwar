/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef FLICREAD_H
#define FLICREAD_H

#ifndef __cplusplus
#error flicread.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	FLI/FLC reader class
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1994/09/23  13:29:31  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/09/07  14:15:13  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <io.h>
#include <fcntl.h>
#include "image.h"
#include "palette.h"
#include "flic.h"
#include "cd_open.h"

/*
 * The C++ class
 */

// Define some file handling values so that we can
// change the file reading method easily to increas speed

// This is for SYS_IO style (i.e. open, read, lseek, close)

class FileHandle {
	int handle;

public:

	int fileOpen(const char* name)
	{
		handle = ::fileOpen(name, O_RDONLY | O_BINARY);
		if(handle < 0)
			return -1;
		else
			return 0;
	}

	void fileClose()
	{
		close(handle);
	}

	int fileRead(void* data, long size)
	{
	  if(read(handle, data, size) != size)
			return -1;
		else
			return 0;
	}

	long fileSeek(long where)
	{
		return lseek(handle, where, SEEK_SET);
	}
};


class FlicRead {
	FlicHead header;			// Complete FLIC Header
	FileHandle file;
	int error;
	Boolean palette64;		// Set for 64 colour, False for True Colour
public:
	int loops;					// Number of times complete animation played
	int frame;					// Current frame
	Boolean finished;			// Set when last frame has been played
	Boolean paletteChanged;	// Set if palette has changed since last frame
	Image image;				// Image with last frame in

	Palette palette;			// Current palette (could be a pal64)

public:
	FlicRead(const char* fileName, Boolean use64 = False);
	~FlicRead();

	void nextFrame();

	int getError() const { return error; }
	const FlicHead* getHeader() const { return &header; }

private:
	void decodeFrame(FrameHead& frameHead, const UBYTE* data);
	void decodeColor256(const UBYTE* body);
	void decodeDeltaFLC(const UBYTE* body);
	void decodeColor64 (const UBYTE* body);
	void decodeDeltaFLI(const UBYTE* body);
	void decodeBlack   (const UBYTE* body);
	void decodeByteRun (const UBYTE* body);
	void decodeLiteral (const UBYTE* body);
	void decompressChunk(ChunkHead* chunk);
};

#endif /* FLICREAD_H */

