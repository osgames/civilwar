/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CAMPED_H
#define CAMPED_H

#ifndef __cplusplus
#error camped.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Editor
 *
 * Classes and definitions used by editor
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "campaign.h"
#include "menudata.h"
#include "campmode.h"
#include "city.h"
#include "water.h"
#include "dialogue.h"
#include "orders.h"
#include "campter.h"

class General;

/*
 * Some definitions
 */

#define Font_SimpleMessage	Font_EMFL10
#define Font_Title			Font_EMFL15
#define Font_Info				Font_EMMA14
#define Font_Heading			Font_EMFL_8

/*
 * Data types used in this module
 */

enum EditMode {
	EDIT_States,
	EDIT_Facilities,
	EDIT_Railway,
	EDIT_Supply,
	EDIT_Water,
	EDIT_Terrain,
	EDIT_Troops,

	EDITMODE_HowMany
};

enum DoWhat {
	DoNothing,
	DoNew,
	DoMove,
	DoDelete,
	DoEdit,
	DoCal,
	DoLinks,
	DoCapacity,
	DoOwner,
	DoMakeKey,
	DoOrder,
	DoOccupy,
	DoSiege,
};

/*
 * Classes used in this module
 * Defined here, to avoid forward references
 */

/*
 * Overall Campaign Data and Control
 */

class CampaignEditControl : public MenuData, public Campaign, public CampaignMode {

	/*
	 * Data
	 */


	enum {
		Tracking,
		Moving,
		GetState,
		GetLink,
		GetOwner,
		GetDestination,
		GetOccupy,
		GetSiege,
	} trackMode;
	Boolean objectIsNew;


	City edittedFacility;
	WaterZone edittedZone;
	OrderMode orderMode;

	CTerrainVal terrain;			// Current colour to put on terrain

public:
	Region* infoArea;				// Where to display info about cities
	TextWindow* textWin;

	EditMode mode;						// What edit mode are we in?
	PointerIndex zoomPointer;		// Pointer to restore after zooming
	PointerIndex overMapPointer;	// Pointer to use when over map
#if defined(NOBITFIELD)
	Boolean zooming;				// Set if in zooming mode
	Boolean overFlag;				// Set if over map
#else
	Boolean zooming:1;				// Set if in zooming mode
	Boolean overFlag:1;				// Set if over map
#endif

	MapObject* currentObject;
	Facility* edittingFacility;
	WaterZone* edittingZone;
	Unit* edittingUnit;
	Boolean showFacilities;
	Boolean showLink;					// Draw line between edittingFacility and currentObject

	/*
	 * Functions
	 */

	CampaignEditControl();
	~CampaignEditControl();

	void drawIcon();

	void overMap(MapWindow* map, Event* event, MenuData* d, const Location* l);
	void offMap();
	void doOptions(const Point& p);
	void startZoom();
	void setMode(EditMode m);

	void hint(const char* s, ...);
	void clearInfo();
	void setupInfo();

	void setCloseState(Facility* f, Boolean keepSide);
	void setCloseOwner(Facility* f, Boolean keepSide);

  	// void editUnit(Unit* oldUnit);
	// void editGeneral(General* oldGeneral);

	void saveTerrain();
	void showColour();
private:
	void loseObject();
	void setCurrentObject(MapObject* ob);
	void trackFor(MapWindow* map, Event* event, ObjectType whatType);
	Boolean isSuitableObject(MapObject* ob, ObjectType whatType);

	void stateOverMap		(MapWindow* map, Event* event, MenuData* d, const Location* l);
	void facilityOverMap	(MapWindow* map, Event* event, MenuData* d, const Location* l);
	void troopOverMap		(MapWindow* map, Event* event, MenuData* d, const Location* l);
	void waterOverMap		(MapWindow* map, Event* event, MenuData* d, const Location* l);
	void terrainOverMap	(MapWindow* map, Event* event, MenuData* d, const Location* l);
	
	void stateOffMap		();
	void facilityOffMap	();
	void troopOffMap		();
	void waterOffMap		();
	void terrainOffMap	();

	void stateOptions		(const Point& p);
	void facilityOptions	(const Point& p);
	void troopOptions		(const Point& p);
	void railwayOptions	(const Point& p);
	void waterOptions		(const Point& p);
	void supplyOptions 	(const Point& p);
	void terrainOptions	(const Point& p);

	State* editState(State* oldState);
	void editFacility(Facility* oldFacility);
	void editWaterZone(WaterZone* zone);
	void updateWaterZone();
	void makeCityStates(State* state, int noPrompt);

};

/*
 * Functions
 */

Side inputSide(Side start);
DoWhat getEditWhat(const char* title);


/*
 * Data
 */

// extern char* sideStr[];
extern MenuChoice whatChoices[];

/*
 * Variables
 */

extern Campaign* campaign;
extern CampaignEditControl* control;

extern Boolean changed;
extern Boolean stateChanged;
extern Boolean facilityChanged;
extern Boolean obChanged;
extern Boolean waterChanged;
extern Boolean terrainChanged;


#endif /* CAMPED_H */

