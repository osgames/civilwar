/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MSGWIND_H
#define MSGWIND_H

#ifndef __cplusplus
#error msgwind.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Simple Message Window Definition
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.13  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.1  1994/01/11  22:30:12  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "text.h"
#include "colours.h"

class MessageWindow {
	Region region;
	TextWindow textWind;
public:
	MessageWindow(Image* img, const Rect& r);
	~MessageWindow();

	void draw(const char* s);
	void printf(const char* fmt, ...);
};

#if 0	// Old Method

#include "text.h"
#include "icon.h"
 
class MessageWindow;
class MessageList;

class GameMessage {
	friend class MessageWindow;
	friend class MessageList;

	GameMessage* next;			// Linked list!
	char* text;
	int priority;
public:
	GameMessage(const char* s, int p);
	~GameMessage();
};

class MessageList {
	friend class MessageWindow;

	GameMessage* entry;
public:
	MessageList();
	~MessageList();

	void addMessage(const char* message, int priority);
};



class MessageWindow : public Region, public TextWindow, public IconSet {
#if defined(NOBITFIELD)
	Boolean infoUp;
#else
	Boolean infoUp:1;
#endif
	MessageList messages;

public:
	MessageWindow(IconSet* parent, Rect r, FontID f);

	void showInfo(const char* s);
	void clearInfo();

	void addMessage(const char* message, int priority);

	void drawIcon();
private:
	void setupHeadlines();
	void removeHeadlines();
	void drawHeadlines();

	void clear();
};

#endif

#endif /* MSGWIND_H */
