/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SPRLIST_H
#define SPRLIST_H

#ifndef __cplusplus
#error sprlist.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Sprite List on 3D View
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.1  1994/08/31  15:26:29  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types3d.h"
#include "pool.h"

#ifdef DEBUG
#define CHECK_RANGES
#endif

class System3D;
class SpriteBlock;
struct SpriteElement;

class Shape3D {
public:
	virtual ~Shape3D() { };
	virtual void draw(const System3D& drawData, const Point3D& where) = 0;
};

class Sprite3D : public Shape3D {
public:
	SpriteBlock* spriteBlock;

	Sprite3D() { spriteBlock = 0; }
	~Sprite3D();

	void draw(const System3D& drawData, const Point3D& where);
	void release();
};

/*
 * Important note:
 *   Sprites are ordered with descending Z coordinates, i.e. largest first!
 */

class SpriteRange;

struct SpriteElement {
	SpriteRange* range;			// What range it belongs to
	SpriteElement* next;		// Next sprite element in the current range
	SpriteElement* prev;		// Previous sprite element in the current range

	Shape3D* sprite;
	Point3D where;	// Where it is in screen space

public:
	void init(Shape3D* spr, const Point3D& p);
};

#define MaxInRange 32		// Maximum number of sprites per range

struct SpriteRange {
	SpriteRange* left;			// Simple binary tree
	SpriteRange* right;
#if defined(NOBITFIELD)
	Boolean leftChild;
	Boolean rightChild;
#else
	Boolean leftChild:1;
	Boolean rightChild:1;
#endif

	Cord3D minZ;
	Cord3D maxZ;
	
	SpriteElement* entry;		// Sorted list of sprites in this range
	UBYTE count;					// Number of sprites in this range

	SpriteRange();

#ifdef CHECK_RANGES
	void check();
#endif
};

struct SpriteID {
	SpriteElement* element;

	SpriteID() { element = 0; }
	~SpriteID() { remove(); }
	void remove();
};


class Sprite3DList {
	SpriteRange* root;			// Entry into Spriterange list

	Pool spritePool;

public:
	Sprite3DList();
	~Sprite3DList();

	void clear();
#ifdef USE_SPRITEID
	void add(Shape3D* spr, const Point3D& p, SpriteID* id);
	void remove(SpriteID* id);
#else
	SpriteElement* add(Shape3D* spr, const Point3D& p);
	void remove(SpriteElement* spr);
#endif
	void draw(const System3D& drawData);
private:
	void clearRange(SpriteRange* range);
	void drawRange(const System3D& drawData, SpriteRange* range);
};

extern Sprite3DList* spriteList;


#endif /* SPRLIST_H */

