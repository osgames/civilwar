/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef VGASCRN_H
#define VGASCRN_H

#ifndef __cplusplus
#error vgascrn.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id : CJW $
 *----------------------------------------------------------------------
 *
 *		Functions to handle VGA screens
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <graph.h>
#include "gr_types.h"

// void awaitVerticalBlank();

class Image;

class Vgascr {

   short oldmode;
	short currentmode;

	short xpix;
	short ypix;

	
	// Point flicPosition;

	unsigned char* screenPtr;

public:
	Vgascr( short newmode );

	~Vgascr();

	void setOldVideoMode();

	void setVideoInfo();

	void setFlicPosition( short height, short width, Boolean centre = True );

	void waitForVerBlank();

	void blit(Image* image );
#ifdef DEBUG
	void resetScreen();
#endif

};
#endif /* VGASCRN_H */

