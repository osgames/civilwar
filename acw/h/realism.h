/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef REALISM_H
#define REALISM_H

#ifndef __cplusplus
#error realism.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Set Realism/Difficulty Screens
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.16  1994/05/04  22:12:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/04/26  13:40:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.14  1994/04/22  22:16:24  Steven_Green
 * Icon Handling Tidied Up
 *
 * Revision 1.13  1994/04/20  22:24:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1994/04/11  21:31:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/04/11  13:38:20  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/03/17  14:30:48  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/03/15  15:17:16  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/03/11  23:14:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/03/10  14:29:08  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/03/01  22:30:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/02/28  23:05:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/02/17  20:00:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/01/11  22:30:12  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#if !defined(TESTCAMP)
int battleRealism();
#endif
#if !defined(TESTBATTLE)
int campaignRealism();
#endif

#endif /* REALISM_H */

