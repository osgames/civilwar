/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef TYPES3D_H
#define TYPES3D_H

#ifndef __cplusplus
#error types3d.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	3D Definitions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.6  1994/09/23  13:29:31  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/09/02  21:27:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/08/31  15:26:29  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/08/24  15:07:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/08/09  21:31:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/08/09  15:46:26  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdlib.h>
#include "measure.h"
#include "trig.h"
#include "gr_types.h"
#include "palette.h"
#include "error.h"

class Point3D {
public:
	Cord3D x;
	Cord3D y;
	Cord3D z;

			Point3D(Cord3D x, Cord3D y, Cord3D z)
			{
				Point3D::x = x;
				Point3D::y = y;
				Point3D::z = z;
			}

			Point3D(const Point3D& p) { x = p.x; y = p.y; z = p.z; }
			Point3D() { }

	Cord3D	getX() const { return x; }
	Cord3D	getY() const { return y; }
	Cord3D	getZ() const { return z; }

	void moveX(Cord3D dx) { x += dx; }
	void moveY(Cord3D dy) { y += dy; }
	void moveZ(Cord3D dz) { z += dz; }
	
	void setX(Cord3D dx) { x = dx; }
	void setY(Cord3D dy) { y = dy; }
	void setZ(Cord3D dz) { z = dz; }
	
	operator Point() const { return Point(x, y); }


	Point3D& operator += (const Point3D& p) { x += p.x; y += p.y; z += p.z; return *this; }
	Point3D operator +  (const Point3D& p) const { Point3D r = *this; return r += p; }
	Point3D& operator -= (const Point3D& p) { x -= p.x; y -= p.y; z -= p.z; return *this; }
	Point3D operator -  (const Point3D& p) const { Point3D r = *this; return r -= p; }

	Point3D& operator += (const Point& p) { x += p.x; y += p.y; return *this; }
	Point3D& operator -= (const Point& p) { x -= p.x; y -= p.y; return *this; }

	Point3D& operator =  (const Point3D& p) { x = p.x; y = p.y; z = p.z; return *this; }

	/*
	 * Mid Point operator
	 */

	Point3D operator ^ (const Point3D& p)
	{
		Point3D p1;

		p1.x = (x + p.x) / 2;
		p1.y = (y + p.y) / 2;
		p1.z = (z + p.z) / 2;

		return p1;
	}


	Point3D abs();
	Point3D sign();

	void normalise();

	friend void cross(Point3D& result, const Point3D& p1, const Point3D& p2);	// Cross Product
	friend Cord3D dot(const Point3D& p1, const Point3D& p2);	// Dot Product

	Cord3D distance();

	class Zero: public GeneralError {
	public:
		Zero(): GeneralError("Zero Viewpoint") { }
	};		// Exception for 0 light source given
};


inline Point::Point(Point3D& p)
{
	x = SDimension(p.x);
	y = SDimension(p.y);
}

inline Point3D Point3D::abs()
{
	Point3D newPoint;

	newPoint.x = ::abs(x);
	newPoint.y = ::abs(y);
	newPoint.z = ::abs(z);

	return newPoint;
}


inline Point3D Point3D::sign()
{
	Point3D newPoint;

	newPoint.x = sgn(x);
	newPoint.y = sgn(y);
	newPoint.z = sgn(z);

	return newPoint;
}



class Point3DI : public Point3D {
public:
	Intensity intensity;

	Point3DI() { }
	Point3DI(Cord3D x, Cord3D y, Cord3D z, Intensity i) : Point3D(x, y, z), intensity(i) { }

	Point3DI& operator = (const Point3D& p)
	{
		x = p.x;
		y = p.y;
		z = p.z;

		return *this;
	}

	Point3DI& operator -= (const Point3DI& p)
	{
		x -= p.x;
		y -= p.y;
		z -= p.z;
		intensity -= p.intensity;
		return *this;
	}

	Point3DI operator -  (const Point3DI& p) const
	{
		Point3DI r = *this;
		return r -= p; 
	}
	
	Point3DI abs();
	Point3DI sign();
};

inline Point3DI Point3DI::abs()
{
	Point3DI newPoint;

	newPoint.x = ::abs(x);
	newPoint.y = ::abs(y);
	newPoint.z = ::abs(z);
	newPoint.intensity = ::abs(intensity);

	return newPoint;
}


inline Point3DI Point3DI::sign()
{
	Point3DI newPoint;

	newPoint.x = sgn(x);
	newPoint.y = sgn(y);
	newPoint.z = sgn(z);
	newPoint.intensity = sgn(intensity);

	return newPoint;
}


/*
 * Trigonometry stuff
 *
 * Rotate point x,y clockwise by angle a.
 */


inline rotate(Cord3D &x, Cord3D &y, Wangle a)
{
	Cord3D x1 = x;
	Cord3D y1 = y;

	x = Cord3D(mcos(x1, a) + msin(y1, a));
	y = Cord3D(mcos(y1, a) - msin(x1, a));
}


#endif /* TYPES3D_H */

