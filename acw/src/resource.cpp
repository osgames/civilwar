/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Resource Management
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.5  1994/07/19  19:53:03  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/06/06  13:17:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.2  1994/05/25  23:31:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/05/21  13:16:19  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG
#include <stdio.h>
#endif

#include "resource.h"
#include "game.h"
#include "myassert.h"
#ifdef DEBUG
#include "log.h"
#endif

ResourceSet::ResourceSet(ResourceVal human, ResourceVal horse, ResourceVal food, ResourceVal raw)
{
	values[R_Human] = human;
	values[R_Horses] = horse;
	values[R_Food] = food;
	values[R_Materials] = raw;
}

ResourceSet::ResourceSet()
{
	values[R_Human] = 0;
	values[R_Horses] = 0;
	values[R_Food] = 0;
	values[R_Materials] = 0;
}

#ifndef CAMPEDIT

static ResourceType realismResourceMapping[RealismLevels][ResourceCount] =
{
	{ R_Human, R_Human,	R_Human, R_Human		},	// Simple
	{ R_Human, R_Human,	R_Food,  R_Food		},	// Average
	{ R_Human, R_Human,	R_Food,  R_Materials },	// Complex1
	{ R_Human, R_Horses,	R_Food,  R_Materials },	// Complex2
	{ R_Human, R_Horses,	R_Food,  R_Materials	}	// Complex3
};

static ResourceVal maxValues[RealismLevels][ResourceCount] =
{
	{ MaxResource * 4, 0, 				0, 					0 },
	{ MaxResource * 2, 0, 				MaxResource * 2,	0 },
	{ MaxResource * 2, 0, 				MaxResource, 		MaxResource },
	{ MaxResource,		 MaxResource,	MaxResource,		MaxResource },
	{ MaxResource,		 MaxResource,	MaxResource,		MaxResource }
};

/*
 * Add a resource set to resources
 * How it works depends on the difficulty setting
 *
 * Values must be checked for overflow
 */

ResourceSet& ResourceSet::operator +=(const ResourceSet& set)
{
	ASSERT(this != 0);

	Realism realism = game->getDifficulty(Diff_Resource);

	for(int i = 0; i < ResourceCount; i++)
	{
		ResourceType type = realismResourceMapping[realism][i];

		ResourceVal& val = values[type];
		ResourceVal addValue = set.values[i];

		if(val > (maxValues[realism][type] - addValue))
			val = maxValues[realism][type];
		else
			val += addValue;
	}

	return *this;
}

ResourceSet ResourceSet::operator + (const ResourceSet& set) const
{
	ASSERT(this != 0);
	ASSERT(&set != 0);

	ResourceSet result = *this;
	result += set;
	return result;
}

ResourceSet& ResourceSet::operator -=(const ResourceSet& set)
{
	ASSERT(this != 0);
	ASSERT(&set != 0);

	Realism realism = game->getDifficulty(Diff_Resource);

	for(int i = 0; i < ResourceCount; i++)
	{
		ResourceVal& val = values[realismResourceMapping[realism][i]];
		ResourceVal subValue = set.values[i];

		if(val < subValue)
			val = 0;
		else
			val -= subValue;
	}

	return *this;
}

/*
 * Divide all values in resource by a value
 *
 * values will be rounded by 0.5, e.g. 32/64 = 1, 31/64 = 0
 */

ResourceSet ResourceSet::operator / (unsigned int divisor) const
{
	ASSERT(this != 0);

	ResourceSet result = *this;
	unsigned int round = divisor >> 1;

	for(int i = 0; i < ResourceCount; i++)
		result.values[i] = ResourceVal((values[i] + round) / divisor);

	return result;
}

ResourceSet& ResourceSet::operator /= (unsigned int divisor)
{
	ASSERT(this != 0);

	unsigned int round = divisor >> 1;

	for(int i = 0; i < ResourceCount; i++)
		values[i] = (values[i] + round) / divisor;

	return *this;
}

void ResourceSet::multiplyRatio(int top, int bottom)
{
	ASSERT(this != 0);

	int adjust = bottom / 2;		// To round up

	for(int i = 0; i < ResourceCount; i++)
		values[i] = (values[i] * top + adjust) / bottom;
}


/*
 * Convert Resource Set into one suitable for difficulty level
 */

void ResourceSet::adjust()
{
	ASSERT(this != 0);

	Realism realism = game->getDifficulty(Diff_Resource);

	for(int i = 0; i < ResourceCount; i++)
	{
		int val = values[i];

		values[i] -= val;
		values[realismResourceMapping[realism][i]] += val;
	}
}

/*
 * See if there are enough resource points to spend
 * Return the number that can be afforded (0 if none)
 */

int ResourceSet::canAfford(const ResourceSet* local, const ResourceSet* imported) const
{
	Realism realism = game->getDifficulty(Diff_Resource);

	ResourceSet r1;		// This initialises them to 0
	
	if(local)
		r1 = *local;

	if(imported)
		r1 += *imported;

	r1.adjust();

	ResourceSet cost = *this;
	cost.adjust();

	int howMany = 0;
	Boolean setOne = False;

	for(int i = 0; i < ResourceCount; i++)
	{
		if(cost.values[i])
		{
			int n = r1.values[i] / cost.values[i];
			if(!setOne || (n < howMany))
			{
				howMany = n;
				setOne = True;
			}
		}
	}

	return howMany;
}

/*
 * Take off the resources from local and imported
 * (Assume we can afford it!)
 */

void ResourceSet::takeCost(ResourceSet* local, ResourceSet* imported, int howMany) const
{
	Realism realism = game->getDifficulty(Diff_Resource);

	for(int i = 0; i < ResourceCount; i++)
	{
		ResourceType type = realismResourceMapping[realism][i];

		ResourceVal cost = values[i] * howMany;

		if(cost && local)
		{
			if(cost <= local->values[type])
			{
				local->values[type] -= cost;
				cost = 0;
			}
			else
			{
				cost -= local->values[type];
				local->values[type] = 0;
			}
		}

		if(cost && imported)
		{
		 	if(cost <= imported->values[type])
		 		imported->values[type] -= cost;
		 	else
		 		imported->values[type] = 0;		// This is an error situation
		}
	}
}



/*
 * Move a proportion of a resource set into another
 */

void ResourceSet::addTax(ResourceSet& from, unsigned short proportion)
{
	ASSERT(this != 0);
	ASSERT(&from != 0);

	Realism realism = game->getDifficulty(Diff_Resource);

	for(int i = 0; i < ResourceCount; i++)
	{
		ResourceType t = realismResourceMapping[realism][i];
		ResourceVal amount = ResourceVal(from.values[i] / proportion);

		/*
		 * Restrict key city from overflowing
		 */

		if(values[t] > (maxValues[realism][t] - amount))
			amount = ResourceVal(maxValues[realism][t] - values[t]);

		values[t] += amount;
		from.values[i] -= amount;
	}
}

ResourceVal ResourceSet::getValue(ResourceType type) const
{
	ASSERT(this != 0);

	Realism realism = game->getDifficulty(Diff_Resource);
 	ResourceType t = realismResourceMapping[realism][type];
	return values[t];
}

static ResourceVal ResourceSet::getMaxVal(ResourceType type)
{
	Realism realism = game->getDifficulty(Diff_Resource);
	return maxValues[realism][type];
}



#ifdef DEBUG

/*
 * Display resource into logfile
 */


void ResourceSet::logPrint()
{
	ASSERT(this != 0);
	
	if(logFile)
	{
		*logFile << "[";
		for(int i = 0; i < ResourceCount; i++)
		{
			if(i != 0)
				*logFile << ",";
			*logFile << values[i];
		}
		*logFile << "]";
	}
}

void ResourceSet::print(char* buffer)
{
	ASSERT(buffer != 0);
	
	*buffer++ = '[';
	if(this)
	{
		for(int i = 0; i < ResourceCount; i++)
		{
			if(i != 0)
				*buffer++ = ',';
			sprintf(buffer, "%5d", (int) values[i]);
			buffer += strlen(buffer);
		}
	}
	else
	{
		sprintf(buffer, "%-23s", "None");
		buffer += strlen(buffer);
	}
	*buffer++ = ']';
	*buffer = 0;
}

#endif

#endif	// EDITCAMP
