/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battles within the campaign game
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "campbatl.h"

#ifdef TESTBATTLE
#include "ob.h"
#else
#include "camptab.h"
#include "campwld.h"
#include "campaign.h"
#endif


#ifdef DEBUG
#include "game.h"
#ifndef TESTBATTLE
#include "clog.h"
#endif
#endif

MarkedBattle::MarkedBattle(const Location& l, TimeBase t, Boolean play)
{
	next = 0;
	units = 0;
	where = l;
	when = t;
	playable = play;
}

MarkedBattle::MarkedBattle(Boolean play)
{
	next = 0;
	units = 0;
	playable = play; 
}

MarkedBattle::MarkedBattle()
{
	next = 0;
	units = 0;
	playable = False; 
}

MarkedBattle::~MarkedBattle()
{
	BattleUnit* bu = units;
	while(bu)
	{
		BattleUnit* b = bu;
		bu = bu->next;

		delete b;
	}
}

int MarkedBattle::nUnits() const
{
	int count = 0;

	BattleUnit* bu = units;
	while(bu)
	{
		count++;
		bu = bu->next;
	}
	return count;
}

/*
 * If force is set, then the inBattle flag is ignored.
 */

void MarkedBattle::addUnit(Unit* u, TimeBase t, Boolean force)
{
	if(force || !u->inBattle)
	{
#if defined(DEBUG) && !defined(TESTBATTLE)
		cLog.printf("Adding %s to battle", u->getName(True));
#endif
		BattleUnit* bu = new BattleUnit();
		bu->unit = u;
		bu->when = t;
		bu->next = units;
		units = bu;
#ifndef TESTBATTLE
		u->setInBattle();
#endif
	}
}

void MarkedBattle::append(BattleUnit* bu)
{
	bu->next = 0;

	BattleUnit* b = units;

	if(b)
	{
		while(b->next)
			b = b->next;
		b->next = bu;
	}
	else
		units = bu;
}

#ifdef DEBUG
void BattleList::check()
{
	if(battles == (MarkedBattle*)-1)
		GeneralError("Silly value in battle list");
}
#endif

int BattleList::entries() const
{
	int count = 0;
	MarkedBattle* b = battles;
	while(b)
	{
		count++;
		b = b->next;
	}
	return count;
}

/*
 * Clear any battles
 */

void BattleList::clear()
{
	MarkedBattle* b = battles;

	while(b)
	{
		MarkedBattle* b1 = b;
		b = b->next;

		delete b1;
	}
	battles = 0;
	lastBattle = 0;
}

void BattleList::append(MarkedBattle* mb)
{
	if(lastBattle)
		lastBattle->next = mb;
	else
		battles = mb;
	lastBattle = mb;
}

#ifndef TESTBATTLE

/*
 * Remove the 1st battle from the list and return it
 * The caller should delete it when its finished with it.
 */

MarkedBattle* BattleList::getBattle()
{
	MarkedBattle* result;
	Boolean validBattle;
	
	do
	{
		result = battles;

		if(result)
		{
			battles = result->next;

			if(result == lastBattle)
				lastBattle = 0;
	
		}

		/*
		 * Check if battle is valid, i.e. are 2 sides involved?
		 */

		if(result)
		{
			Side involved = SIDE_None;

			BattleUnit* units = result->units;
			while(units)
			{
				involved |= units->unit->getSide();
				units = units->next;
			}

			validBattle = (involved == SIDE_Both);

#if defined(DEBUG)
			if(!validBattle)
			{
				cLog.printf("Battle at %ld,%ld not played because only 1 side is involved",
					result->where.x, result->where.y);
#endif

				if(!validBattle)
					delete result;
#ifdef DEBUG
			}
#endif


		}
		else
			validBattle = True;

	} while(!validBattle);

	return result;
}

/*
 * Find existing battle at given location or create a new one
 */

MarkedBattle* BattleList::find(const Location& l, TimeBase t, Boolean play)
{
#ifdef DEBUG
	check();
#endif


	MarkedBattle* bestBattle = 0;
	Distance bestDist;

	MarkedBattle* battle = battles;
	while(battle)
	{
		Distance d = distanceLocation(l, battle->where);

		if(d < sameBattleDistance)
		{
			if(!bestBattle | (d < bestDist))
			{
				bestBattle = battle;
				bestDist = d;
			}
		}

		battle = battle->next;
	}

	if(!bestBattle)
	{
#ifdef DEBUG
		cLog.printf("New Battle Started at %ld,%ld", l.x, l.y);
#endif

		bestBattle = new MarkedBattle(l, t, play);
 		append(bestBattle);
#if 0
		/*
		 * Tag it onto the end of the battle list so that they will
		 * come back in chronological order
		 */

		if(lastBattle)
			lastBattle->next = bestBattle;
		else
			battles = bestBattle;

		lastBattle = bestBattle;
#endif
		/*
		 * Scan for any units close to this new battlefield
		 */

		bestBattle->addCloseUnits();
	}
#ifdef DEBUG
	else
		cLog.printf("Added to existing battle at %ld,%ld", bestBattle->where.x, bestBattle->where.y);
#endif

#ifdef DEBUG
	check();
#endif

	return bestBattle;
}

/*
 * Add all units that have brigades within a certain radius of the battlefield
 * to the battle unless they have withdraw orders (or are routing?)
 */


void MarkedBattle::addCloseUnits()
{
	for(Unit* p = campaign->world->ob.getSides(); p; p = p->sister)
	{
		for(Unit* a = p->child; a; a = a->sister)
		{
			for(Unit* c = a->child; c; c = c->sister)
			{
				for(Unit* d = c->child; d; d = d->sister)
				{
					for(Brigade* b = (Brigade*)d->child; b; b = (Brigade*)b->sister)
					{
						if(!b->inBattle)
						{
							Unit* u = b->getTopCommand();

							if(u->realOrders.basicOrder() != ORDERMODE_Withdraw)
							{
								Distance d = distanceLocation(b->location, where);
								if(d <= involvedBattleDistance)
									addUnit(b->getTopCommand(), campaign->gameTime, False);
							}
						}
					}	// Brigade
				}	// Division
			}	// Corps
		}	// Army
	}	// President
}


#if 0		// What does this do?

void MarkedBattle::addChildren(Unit* d1, TimeBase t )
{
 	while ( d1 )
	{
	  	if(!d1->isCampaignIndependant() )
		{
			d1->inBattle = False;
			addUnit(d1, t, False);
	
			if ( d1->rank < Rank_Brigade ) addChildren( d1->child, t );
		}

		d1 = d1->sister;
	}
}

#endif

/*
 * Create a battle near a facility
 *
 * Should examine all units within radius and make them
 * part of the battle.
 *
 * Plus if the defender's orders are advance/attack/cautiousAdvance
 * Then they are added to the battle.
 */

MarkedBattle* CampaignWorld::makeBattle(Facility* f, Unit* b, Boolean play)
{
#ifdef DEBUG
	battles.check();
#endif

	b = b->getTopCommand();

#ifdef DEBUG
	game->simpleMessage("%s triggers battle near %s",
		b->getName(False),
		f->getNameNotNull());
	cLog.printf("%s triggers battle near %s",
		b->getName(True),
		f->getNameNotNull());
#endif

	/*
	 * Set battle location to be midway between unit and city
	 */

	Location l = (f->location + b->location) / 2;

	MarkedBattle* battle = battles.find(l, campaign->gameTime, play);


	/*
	 * Now search world for all units and facilities within
	 * involvedBattleDistance and add them to the battle
	 */

	if(!b->inBattle)
	{
		battle->addUnit(b, campaign->gameTime, False);
	}

#ifdef DEBUG
	battles.check();
#endif

	return battle;
}

/*
 * Battle between 2 sides
 */

MarkedBattle* CampaignWorld::makeBattle(Unit* b, Unit* b1)
{
#ifdef DEBUG
	battles.check();
#endif


	b = b->getTopCommand();
	b1 = b1->getTopCommand();

#ifdef DEBUG
	game->simpleMessage("%s triggers battle with %s",
		b->getName(False),
		b1->getName(False));
	cLog.printf("%s triggers battle with %s",
		b->getName(True),
		b1->getName(True));
#endif

	/*
	 * Set battle location to be midway between units
	 */

	Location l = (b->location + b1->location) / 2;

	MarkedBattle* battle = battles.find(l, campaign->gameTime, True);

#if 0	// Can't remember what this is for!
	/*
	 * This next bit shouldn't be necessary because
	 * battle.find automatically adds all close units.
	 */

	if(!b->inBattle)
	{
		if ( b->rank < Rank_Brigade ) battle->addChildren( b->child, campaign->gameTime );

		battle->addUnit(b, campaign->gameTime);
	}

	if(!b1->inBattle)
	{
		if ( b1->rank < Rank_Brigade ) battle->addChildren( b1->child, campaign->gameTime );
		
		battle->addUnit(b1, campaign->gameTime);

	}
#endif

#ifdef DEBUG
	battles.check();
#endif

	return battle;
}

void BattleList::checkUnitNearBattle(Unit* u)
{
	MarkedBattle* bat = battles;

	while(bat)
	{
		if(distanceLocation(bat->where, u->location) < involvedBattleDistance)
		{
			bat->addUnit(u->getTopCommand(), campaign->gameTime, False);
			return;
		}

		bat = bat->next;
	}
}
#endif

/*
 * Create a marked Battle List from an Order of Battle
 *
 * Question: Should it add all independant units or just the armies?
 */

MarkedBattle* makeMarkedBattle(OrderBattle* ob)
{
	MarkedBattle* bat = new MarkedBattle(False);
	TimeBase t;

	for(Unit* p = ob->getSides(); p; p = p->sister)
	{
		for(Unit* a = p->child; a; a = a->sister)
		{
			bat->addUnit(a, t, True);

			if(a->hasCampaignReallyIndependant())
			{
				for(Unit* c = a->child; c; c = c->sister)
				{
					if(c->isCampaignReallyIndependant())
						bat->addUnit(c, t, True);
					if(c->hasCampaignReallyIndependant())
					{
						for(Unit* d = c->child; d; d = d->sister)
						{
							if(d->isCampaignReallyIndependant())
								bat->addUnit(d, t, True);
							if(d->hasCampaignReallyIndependant())
							{
								for(Unit* b = d->child; b; b = b->sister)
								{
									if(b->isCampaignReallyIndependant())
										bat->addUnit(b, t, True);
								}
							}
						}
					}
				}
			}
		}
	}

	return bat;
}


/*
 * Find the most senior unit of the given side involved in the battle
 */

Unit* MarkedBattle::getSeniorUnit(Side side)
{
	Unit* best = 0;

	BattleUnit* bu = units;
	while(bu)
	{
		Unit* u = bu->unit;

		if(u->getSide() == side)
		{
			if(!best || (u->getRank() < best->getRank()))
				best = u;
		}


		bu = bu->next;
	}

	return best;
}

Strength MarkedBattle::getSideTotal(Side si)
{
	BattleUnit* battleunit = units;

	Strength count = 0;

	UnitInfo ui(True, False);		// Independant only.

	while(battleunit)
	{
		if(battleunit->unit->getSide() == si)
		{
			battleunit->unit->getInfo(ui);

			count += ui.strength;
		}
		
		battleunit = battleunit->next;
	}

	return count;
}



