/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Polygon Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.20  1994/08/31  15:23:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.19  1994/08/24  15:05:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.18  1994/08/19  17:29:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.17  1994/08/09  21:29:58  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.16  1994/06/24  14:43:30  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.14  1994/04/05  12:28:09  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1993/12/16  22:19:35  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1993/12/15  20:56:36  Steven_Green
 * Texturing works
 *
 * Revision 1.11  1993/12/14  23:29:01  Steven_Green
 * Modifications to prepare for textured polygons
 *
 * Revision 1.10  1993/12/13  17:11:43  Steven_Green
 * Fast HLine used if colours same (not just intensities).
 *
 * Revision 1.9  1993/12/13  14:09:20  Steven_Green
 * Gourad Polygon Rendering debugged
 *
 * Revision 1.8  1993/12/10  16:06:39  Steven_Green
 * Gourad shading added
 *
 * Revision 1.7  1993/12/04  16:22:51  Steven_Green
 * Exceptions added for running out of memory.
 *
 * Revision 1.6  1993/12/01  15:11:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1993/11/30  02:57:11  Steven_Green
 * Some attempted optimisation
 *
 * Revision 1.4  1993/11/24  09:32:52  Steven_Green
 * Combination of 2 and 3D polygons.
 * Removed clipping to seperate file.
 *
 * Revision 1.3  1993/11/19  18:59:26  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/11/16  22:42:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/05  16:50:53  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "polygon.h"
#include "image.h"
#include "terrain.h"
#include "data3d.h"

#ifdef DEBUG
#include "options.h"
#include "mouse.h"
#endif


/*
 * Scan line class for storing min/max values
 */

struct ScanPoint {
	SDimension x;
	Cord3D z;
	Intensity i;
};

class Scan {
	SDimension yTop;				// Actual Y coordinate after clipping
	SDimension lineCount;		// Actual number of lines after clipping

	ScanPoint* left;	 
	ScanPoint* right;

public:
			Scan(SDimension y1, SDimension y2);
		  ~Scan();
	void			addPoint(const Point3DI& p);
	void			addLine(const Point3DI& p1, const Point3DI& p2);
	void 			draw(System3D& drawData, TerrainType material) const;

#if 0		  
	ScanLine*	get(SDimension y) const { return &lines[y - yOffset]; };
	ScanLine*	get() const { return lines; };

	void			setMin(SDimension y, SDimension x) { get(y)->min = x; };
	void			setMax(SDimension y, SDimension x) { get(y)->max = x; };

	SDimension	getH() const { return count; };
	SDimension  getY() const { return yOffset; };
#endif
};


Scan::Scan(SDimension y1, SDimension y2)
{
	yTop = y1;
	lineCount = y2 - y1 + 1;


	left = new ScanPoint[lineCount * 2];
	right = left + lineCount;

	ScanPoint* l = left;
	ScanPoint* r = right;

	int i = lineCount;
	while(i--)
	{
		l++->x = maxSDimension;
		r++->x = 0;
	}

}

Scan::~Scan()
{
	delete[] left;
}

/*
 * Update a scan line
 */

void Scan::addPoint(const Point3DI& p)
{
	if(p.y >= yTop)
	{
		int line = p.y - yTop;

		if(line < lineCount)
		{
			ScanPoint& l = left[line];
			ScanPoint& r = right[line];

			if(p.x < l.x)
			{
				l.x = p.x;
				l.z = p.z;
				l.i = p.intensity;
			}

			if(p.x > r.x)
			{
				r.x = p.x;
				r.z = p.z;
				r.i = p.intensity;
			}
		}
	}
}

/*
 * Draw a line into the scanline buffer
 */

void Scan::addLine(const Point3DI& p1, const Point3DI& p2)
{
	/*
	 * Force "from" to be left of "to" to ensure the same pixels are
	 * drawn regardless of which direction it is drawn from
	 */

	Point3DI from;
	Point3DI to;

	if(p1.x < p2.x)
	{
		from = p1;
		to = p2;
	}
	else
	{
		from = p2;
		to = p1;
	}

	Point3DI diff = to - from;
	Point3DI absDiff = diff.abs();
	Point3DI dir = diff.sign();

	// ScanLine* line = get(from.y);
	// addPoint(line, from);
	addPoint(from);

	/*
	 * Which is major axis?
	 */

	if(absDiff.x > absDiff.y)
	{
		// X is main axis

		if(from.x != to.x)
		{
			int value = 0x8000;
			int add = (absDiff.y * 0x10000) / absDiff.x;

			int iSlope = diff.intensity / (absDiff.x + 1);		// Always add this
			int iRem = absDiff.intensity % (absDiff.x + 1);		// use for bresenham
			int iVal = 0;

			while(from.x != to.x)
			{
				/*
				 * Alter Y
				 */

				value += add;
				if(value >= 0x10000)
				{
					from.y += dir.y;
					// line += dir.y;
					value -= 0x10000;
				}

				/*
				 * Alter Intensity
				 */

				from.intensity += iSlope;

				iVal += iRem;
				if(iVal >= absDiff.x)
				{
					from.intensity += dir.intensity;
					iVal -= absDiff.x;
				}

				from.x += dir.x;

				// addPoint(line, from);
				addPoint(from);
			}
		}
	}
	else
	{
		// Y is main axis

		if(from.y != to.y)
		{
			int value = 0x8000;
			int add = (absDiff.x * 0x10000) / absDiff.y;

			int iSlope = diff.intensity / (absDiff.y + 1);
			int iRem = absDiff.intensity % (absDiff.y + 1);
			int iVal = 0;

			while(from.y != to.y)
			{
				value += add;
				if(value >= 0x10000)
				{
					from.x += dir.x;
					value -= 0x10000;
				}

				from.y += dir.y;
				// line += dir.y;

				/*
				 * Alter Intensity
				 */

				from.intensity += iSlope;
				iVal += iRem;
				if(iVal >= absDiff.y)
				{
					from.intensity += dir.intensity;
					iVal -= absDiff.y;
				}

				// addPoint(line, from);
				addPoint(from);
			}
		}
	}
}

/*
 * add scan lines to the global scan buffer
 */

void Scan::draw(System3D& drawData, TerrainType material) const
{
	const ScanPoint* l = left;
	const ScanPoint* r = right;

	int count = lineCount;
	Cord3D y = yTop;

	while(count--)
	{
		if(l->x <= r->x)
		{
			/*
			 * Simple clipping
			 */

			SDimension x1 = l->x;
			SDimension x2 = r->x;

			if(x1 < 0)
				x1 = 0;
			if(x2 >= drawData.bitmap->getW())
				x2 = drawData.bitmap->getW() - 1;

			if( (x1 < drawData.bitmap->getW()) && (x2 >= 0))
			{
				/*
				 * Make a segment and add it to system scan buffer
				 */

				ScanSegment seg;

				seg.from.x = x1;
				seg.from.z = l->z;
				seg.from.i = l->i;
				seg.to.x = x2;
				seg.to.z = r->z;
				seg.to.i = r->i;
				seg.material = material;

				drawData.scanBuffer.add(y, &seg);
			}
		}

		l++;
		r++;
		y++;
	}

}


/*
 * Take a triangle and make it into scan lines
 *
 * Do a quick back side test and throw it out
 * If the triangle goes over the edge of the viewing area then clip
 * it into several triangles.
 */

void System3D::renderTriangle(const Point3DI& p1, const Point3DI& p2, const Point3DI& p3, TerrainType ter)
{
	/*
	 * Do a cross product and remove if back face
	 */

	Point3D n;

	cross(n, p3 - p1, p2 - p1);
	if(n.z < 0)
		return;

	clipTriangle(p1, p2, p3, ter);
}

void System3D::clipTriangle(const Point3DI& p1, const Point3DI& p2, const Point3DI& p3, TerrainType ter)
{
	/*
	 * Get the min/max Y coordinates
	 */

	SDimension minY = p1.y;
	SDimension maxY = p1.y;

	if(p2.y < minY)
		minY = p2.y;
	if(p2.y > maxY)
		maxY = p2.y;
	if(p3.y < minY)
		minY = p3.y;
	if(p3.y > maxY)
		maxY = p3.y;

	/*
	 * Ignore cases where triangle is all off screen!
	 */

	if(maxY < 0)
		return;
	if(minY >= bitmap->getH())
		return;

	/*
	 * Clip to screen
	 */

	if(maxY >= bitmap->getH())
		maxY = bitmap->getH() - 1;
	if(minY < 0)
		minY = 0;

	Scan scanLines(minY, maxY);

	scanLines.addLine(p1, p2);
	scanLines.addLine(p2, p3);
	scanLines.addLine(p3, p1);

	TerrainCol* t = terrain.getColour(ter);

	t->addIntensity(p1.intensity);
	t->addIntensity(p2.intensity);
	t->addIntensity(p3.intensity);

#if 0
	if(!t->colourRange)
		terrain.makeColours(t);
#endif

	scanLines.draw(*this, ter);
}

void System3D::renderSquare(const Point3DI& p1, const Point3DI& p2, const Point3DI& p3, const Point3DI& p4, TerrainType terrain)
{
	renderTriangle(p3, p2, p1, terrain);
	renderTriangle(p2, p3, p4, terrain);
}


#if 0		// Old stuff

/*
 * Scan line class for storing min/max values
 */

class Scan {
	
	struct ScanLine {
		SDimension min;
		SDimension max;
	} *lines;

	SDimension count;
	SDimension yOffset;

public:
			Scan(const SDimension count, const SDimension yOffset = 0);
		  ~Scan();
	ScanLine*	get(SDimension y) const { return &lines[y - yOffset]; };
	ScanLine*	get() const { return lines; };

	void			setMin(SDimension y, SDimension x) { get(y)->min = x; };
	void			setMax(SDimension y, SDimension x) { get(y)->max = x; };

	SDimension	getH() const { return count; };
	SDimension  getY() const { return yOffset; };

	void			addLine(Point p1, Point p2);
	void			addPoint(Point p);

	void 			draw(Region* image, Colour color, const Image* texture) const;
};


inline Scan::Scan(const SDimension count, const SDimension yOffset)
{
	Scan::count = count;
	Scan::yOffset = yOffset;
	lines = new ScanLine[count];

	if(!lines)
		throw NoMemory();

	int i = count;
	ScanLine* line = lines;
	while(i--)
	{
		line->min = maxSDimension;
		line->max = 0;

		line++;
	}

}

inline Scan::~Scan()
{
	delete[] lines;
}

/*
 * Update a scan line
 */

void Scan::addPoint(Point p)
{
	ScanLine* line = get(p.y);

	if(p.x < line->min)
		line->min = p.x;

	if(p.x > line->max)
		line->max = p.x;
}

/*
 * Draw a line into the scanline buffer
 */

void Scan::addLine(Point from, Point to)
{
	Point diff = to - from;
	Point absDiff = diff.abs();
	Point dir = diff.sign();

	/*
	 * Which is major axis?
	 */

	if(absDiff.x > absDiff.y)
	{
		// X is main axis

		addPoint(from);

		if(from.x != to.x)
		{
			int value = 0x8000;
			int add = (absDiff.y * 0x10000) / absDiff.x;

			while(from.x != to.x)
			{
				value += add;
				if(value >= 0x10000)
				{
					from.y += dir.y;
					value -= 0x10000;
				}

				from.x += dir.x;

				addPoint(from);
			}
		}
	}
	else
	{
		// Y is main axis

		addPoint(from);

		if(from.y != to.y)
		{
			int value = 0x8000;
			int add = (absDiff.x * 0x10000) / absDiff.y;

			while(from.y != to.y)
			{
				value += add;
				if(value >= 0x10000)
				{
					from.x += dir.x;
					value -= 0x10000;
				}

				from.y += dir.y;

				addPoint(from);
			}
		}
	}
}



/*
 * Draw the scan lines
 */

void Scan::draw(Region* image, Colour color, const Image* texture) const
{
	ScanLine* line = get();
	int count = getH();
	SDimension y = getY();

	if(texture)
	{
		while(count--)
		{
			if(line->min <= line->max)
				image->HLineFast(line->min, y, line->max - line->min + 1, color, texture);

			line++;
			y++;
		}
	}
	else
	{
		while(count--)
		{
			if(line->min <= line->max)
				image->HLineFast(line->min, y, line->max - line->min + 1, color);

			line++;
			y++;
		}
	}
}

/*
 * Draw a polygon onto a bitmap
 *
 * This is confusing now:
 *   Should a routine like this belong to the BitMap class or the
 *   Polygon class?  Throughout the rest of the program, I have
 *   made functions belong to the class that they are written to,
 *   but for this function we have the strangeness that it is more
 *   logical to place it in this module in the polygon implementation.
 */

void Polygon2D::render(System3D& drawData) const
{
#ifdef DEBUG1
	/*
	 * Draw outline for testing
	 */

	{
		Rect oldClip = *this;
		setClip();


		PolyPointIndex i = poly.nPoints;
		Point* p = poly.points;
		Point* lastPoint = &p[i-1];
		while(i--)
		{
			line(*lastPoint, *p, color+1);
			lastPoint = p;

			p++;
		}

		setClip(oldClip.getX(), oldClip.getY(), oldClip.getW(), oldClip.getH());

	}
#endif

	/*
	 * Clip it
	 */

	Polygon2D clippedPoly(nPoints * 2 + 4);		// Rest of function will use this

	clipPoly(&clippedPoly, drawData.bitmap);

	if(clippedPoly.nPoints < 3)
		return;

#ifdef DEBUG1
	/*
	 * Draw outline for testing
	 */

	{
		PolyPointIndex i = clippedPoly.nPoints;
		Point* p = clippedPoly.points;
		Point* lastPoint = &p[i-1];
		while(i--)
		{
			line(*lastPoint, *p, color+1);
			lastPoint = p;

			p++;
		}
	}
#endif

	/*
	 * Work out rectangle enclosing the polygon
	 */

	Point minP = drawData.bitmap->max;			// Start with clipping rectangle extremes
	Point maxP(0,0);

	{
		PolyPointIndex i = clippedPoly.nPoints;
		Point* p = clippedPoly.points;
		while(i--)
		{
			if(p->x < minP.x)
				minP.x = p->x;
			if(p->x > maxP.x)
				maxP.x = p->x;
			if(p->y < minP.y)
				minP.y = p->y;
			if(p->y > maxP.y)
				maxP.y = p->y;

			p++;
		}
	}

	Point size = maxP - minP + Point(1,1);

	/*
	 * Make up scan line table
	 */

#ifdef DEBUG1
	// Draw a frame for testing

	frame(minP, size + Point(1,1), 64);
#endif

	Scan lines(size.y, minP.y);

	{
		PolyPointIndex i = clippedPoly.nPoints;
		Point* p = clippedPoly.points;
		Point* lastPoint = &p[i-1];
		while(i--)
		{
			lines.addLine(*lastPoint, *p);
			lastPoint = p;

			p++;
		}
	}

	lines.draw(drawData.bitmap, colour, pattern);
}

void Polygon2D::setTerrainColMinMax()
{
}


/*
 * 3D Polygon (points already converted to 2D viewing cube
 */

/*
 * Scan line class for storing min/max values
 */

#ifndef STANDARD_ALLOCATION
struct ScanLine3D {
 	Cord3D min;
	Cord3D max;
	Cord3D z;
};
#endif

class Scan3D {
#ifdef STANDARD_ALLOCATION
	struct ScanLine {
		Cord3D min;
		Cord3D max;
		Cord3D z;
	} *lines;
#else
	typedef struct ScanLine3D ScanLine;
	ScanLine* lines;
#endif

	Cord3D count;
	Cord3D yOffset;

#ifndef STANDARD_ALLOCATION
	static ScanLine3D lineArray[480];		// Preset memory area
#endif

public:
			Scan3D(const Cord3D count, const Cord3D yOffset = 0);
		  ~Scan3D();
	ScanLine*	get(Cord3D y) const { return &lines[y - yOffset]; };
	ScanLine*	get() const { return lines; };

	void			setMin(Cord3D y, Cord3D x) { get(y)->min = x; };
	void			setMax(Cord3D y, Cord3D x) { get(y)->max = x; };
	void			setZ(Cord3D y, Cord3D z) { get(y)->z = z; };

	Cord3D	getH() const { return count; };
	Cord3D  getY() const { return yOffset; };

	void			addLine(const Point3D& p1, const Point3D& p2);
	void			addPoint(ScanLine* line, const Point3D& p);

	void 			draw(Region* image, Colour color, const Image* texture) const;
};

#ifndef STANDARD_ALLOCATION
static ScanLine3D Scan3D::lineArray[480];
#endif


inline Scan3D::Scan3D(const Cord3D count, const Cord3D yOffset)
{
	Scan3D::count = count;
	Scan3D::yOffset = yOffset;

#ifdef STANDARD_ALLOCATION
	lines = new ScanLine[count];
	if(!lines)
		throw NoMemory();
#else
	lines = lineArray;
#endif

	int i = count;
	ScanLine* line = lines;
	while(i--)
	{
		line->min = maxCord3D;
		line->max = 0;
		line->z = 0;

		line++;
	}

}

inline Scan3D::~Scan3D()
{
#ifdef STANDARD_ALLOCATION
	delete[] lines;
#endif
}

/*
 * Update a scan line
 */


inline void Scan3D::addPoint(ScanLine* line, const Point3D& p)
{
	if(p.x < line->min)
		line->min = p.x;

	if(p.x > line->max)
		line->max = p.x;

	line->z = p.z;
}

/*
 * Draw a line into the scanline buffer
 *
 * This needs updating to adjust the Z coordinates as it moves across
 */

void Scan3D::addLine(const Point3D& p1, const Point3D& p2)
{
	/*
	 * Force "from" to be left of "to" to ensure the same pixels are
	 * drawn regardless of which direction it is drawn from
	 */

	Point3D from;
	Point3D to;

	if(p1.x < p2.x)
	{
		from = p1;
		to = p2;
	}
	else
	{
		from = p2;
		to = p1;
	}

	Point3D diff = to - from;
	Point3D absDiff = diff.abs();
	Point3D dir = diff.sign();

	ScanLine* line = get(from.y);
	addPoint(line, from);

	/*
	 * Which is major axis?
	 */

	if(absDiff.x > absDiff.y)
	{
		// X is main axis

		if(from.x != to.x)
		{
			int value = 0x8000;
			int add = (absDiff.y * 0x10000) / absDiff.x;

			while(from.x != to.x)
			{
				value += add;
				if(value >= 0x10000)
				{
					from.y += dir.y;
					line += dir.y;
					value -= 0x10000;
				}

				from.x += dir.x;

				addPoint(line, from);
			}
		}
	}
	else
	{
		// Y is main axis

		if(from.y != to.y)
		{
			int value = 0x8000;
			int add = (absDiff.x * 0x10000) / absDiff.y;

			while(from.y != to.y)
			{
				value += add;
				if(value >= 0x10000)
				{
					from.x += dir.x;
					value -= 0x10000;
				}

				from.y += dir.y;
				line += dir.y;

				addPoint(line, from);
			}
		}
	}
}



/*
 * Draw the scan lines
 *
 * This can be updated to make use of the Z values for shading, etc
 */

void Scan3D::draw(Region* image, Colour colour, const Image* texture) const
{
	ScanLine* line = get();
	int count = getH();
	Cord3D y = getY();

	if(texture)
	{
		while(count--)
		{
			if(line->min <= line->max)
				image->HLineFast(line->min, y, line->max - line->min + 1, colour, texture);

			line++;
			y++;
		}
	}
	else
	{
		while(count--)
		{
			if(line->min <= line->max)
				image->HLineFast(line->min, y, line->max - line->min + 1, colour);

			line++;
			y++;
		}
	}
}




/*
 * Draw a polygon onto a bitmap
 *
 * This is confusing now:
 *   Should a routine like this belong to the BitMap class or the
 *   Polygon class?  Throughout the rest of the program, I have
 *   made functions belong to the class that they are written to,
 *   but for this function we have the strangeness that it is more
 *   logical to place it in this module in the polygon implementation.
 */

void Polygon3D::render(System3D& drawData) const
{
	/*
	 * No need to clip, because that was already done when the polygon
	 * was created.
	 */

	if(nPoints < 3)
		return;

	/*
	 * Work out rectangle enclosing the polygon
	 */

	Cord3D minY = drawData.bitmap->getH();
	Cord3D maxY = 0;

	{
		PolyPointIndex i = nPoints;
		Point3D* p = points;
		while(i--)
		{
			if(p->y < minY)
				minY = p->y;
			if(p->y > maxY)
				maxY = p->y;

			p++;
		}
	}

	Cord3D size = maxY - minY + 1;

	/*
	 * Make up scan line table
	 */

	Scan3D lines(size, minY);

	{
		PolyPointIndex i = nPoints;
		Point3D* p = points;
		Point3D* lastPoint = &p[i-1];
		while(i--)
		{
			lines.addLine(*lastPoint, *p);
			lastPoint = p;

			p++;
		}
	}

	lines.draw(drawData.bitmap, colour, pattern);
}

void Polygon3D::setTerrainColMinMax()
{
}

/*========================================================
 * Gourad shaded Polygons
 */


/*
 * Scan line class for storing min/max values
 */

#ifndef STANDARD_ALLOCATION
struct ScanLine3DI {
 	Cord3D min;
	Cord3D max;
	Cord3D z;
	Intensity cMin;
	Intensity cMax;
};
#endif

class Scan3DI {
#ifdef STANDARD_ALLOCATION
	struct ScanLine {
		Cord3D min;
		Cord3D max;
		Cord3D z;
		Intensity cMin;
		Intensity cMax;
	} *lines;
#else
	typedef struct ScanLine3DI ScanLine;
	ScanLine* lines;
#endif

	Cord3D count;
	Cord3D yOffset;

#ifndef STANDARD_ALLOCATION
	static ScanLine3DI lineArray[480];		// Preset memory area
#endif

public:
			Scan3DI(const Cord3D count, const Cord3D yOffset = 0);
		  ~Scan3DI();
	ScanLine*	get(Cord3D y) const { return &lines[y - yOffset]; };
	ScanLine*	get() const { return lines; };

	void			setMin(Cord3D y, Cord3D x) { get(y)->min = x; };
	void			setMax(Cord3D y, Cord3D x) { get(y)->max = x; };
	void			setZ(Cord3D y, Cord3D z) { get(y)->z = z; };

	Cord3D	getH() const { return count; };
	Cord3D  getY() const { return yOffset; };

	void			addLine(const Point3DI& p1, const Point3DI& p2);
	void			addPoint(ScanLine* line, const Point3DI& p);

	void 			draw(Region* image, TerrainCol* terrainCol, const Image* texture, Terrain* terrain) const;
};



#ifndef STANDARD_ALLOCATION
static ScanLine3DI Scan3DI::lineArray[480];
#endif

inline Scan3DI::Scan3DI(const Cord3D count, const Cord3D yOffset)
{
	Scan3DI::count = count;
	Scan3DI::yOffset = yOffset;

#ifdef STANDARD_ALLOCATION
	lines = new ScanLine[count];
	if(!lines)
		throw NoMemory();
#else
	lines = lineArray;
#endif

	int i = count;
	ScanLine* line = lines;
	while(i--)
	{
		line->min = maxCord3D;
		line->max = 0;
		line->z = 0;

		line++;
	}

}

inline Scan3DI::~Scan3DI()
{
#ifdef STANDARD_ALLOCATION
	delete[] lines;
#endif
}

/*
 * Update a scan line
 */


inline void Scan3DI::addPoint(ScanLine* line, const Point3DI& p)
{
	if(p.x < line->min)
	{
		line->min = p.x;
		line->cMin = p.intensity;
	}

	if(p.x > line->max)
	{
		line->max = p.x;
		line->cMax = p.intensity;
	}

	line->z = p.z;
}


/*
 * Draw a line into the scanline buffer
 *
 * This needs updating to adjust the Z coordinates as it moves across
 */

void Scan3DI::addLine(const Point3DI& p1, const Point3DI& p2)
{
	/*
	 * Force "from" to be left of "to" to ensure the same pixels are
	 * drawn regardless of which direction it is drawn from
	 */

	Point3DI from;
	Point3DI to;

	if(p1.x < p2.x)
	{
		from = p1;
		to = p2;
	}
	else
	{
		from = p2;
		to = p1;
	}

	Point3DI diff = to - from;
	Point3DI absDiff = diff.abs();
	Point3DI dir = diff.sign();

	ScanLine* line = get(from.y);
	addPoint(line, from);

	/*
	 * Which is major axis?
	 */

	if(absDiff.x > absDiff.y)
	{
		// X is main axis

		if(from.x != to.x)
		{
			int value = 0x8000;
			int add = (absDiff.y * 0x10000) / absDiff.x;

			int iSlope = diff.intensity / (absDiff.x + 1);		// Always add this
			int iRem = absDiff.intensity % (absDiff.x + 1);		// use for bresenham
			int iVal = 0;

			while(from.x != to.x)
			{
				/*
				 * Alter Y
				 */

				value += add;
				if(value >= 0x10000)
				{
					from.y += dir.y;
					line += dir.y;
					value -= 0x10000;
				}

				/*
				 * Alter Intensity
				 */

				from.intensity += iSlope;

#if 0
				iVal += iRem;
				if(iVal >= absDiff.intensity)
				{
					from.intensity += dir.intensity;
					iVal -= absDiff.intensity;
				}
#else

				iVal += iRem;
				if(iVal >= absDiff.x)
				{
					from.intensity += dir.intensity;
					iVal -= absDiff.x;
				}

#endif

				from.x += dir.x;

				addPoint(line, from);
			}
		}
	}
	else
	{
		// Y is main axis

		if(from.y != to.y)
		{
			int value = 0x8000;
			int add = (absDiff.x * 0x10000) / absDiff.y;

			int iSlope = diff.intensity / (absDiff.y + 1);
			int iRem = absDiff.intensity % (absDiff.y + 1);
			int iVal = 0;

			while(from.y != to.y)
			{
				value += add;
				if(value >= 0x10000)
				{
					from.x += dir.x;
					value -= 0x10000;
				}

				from.y += dir.y;
				line += dir.y;

				/*
				 * Alter Intensity
				 */

				from.intensity += iSlope;
#if 0
				iVal += iRem;
				if(iVal >= absDiff.intensity)
				{
					from.intensity += dir.intensity;
					iVal -= absDiff.intensity;
				}
#else
				iVal += iRem;
				if(iVal >= absDiff.y)
				{
					from.intensity += dir.intensity;
					iVal -= absDiff.y;
				}
#endif

				addPoint(line, from);
			}
		}
	}
}



/*
 * Draw the scan lines
 *
 * This can be updated to make use of the Z values for shading, etc
 */

void Scan3DI::draw(Region* image, TerrainCol* t, const Image* texture, Terrain* terrain) const
{
	ScanLine* line = get();
	int count = getH();
	Cord3D y = getY();

	if(!t->colourRange)
		terrain->makeColours(t);

	if(texture)
	{
		while(count--)
		{
			if(line->min <= line->max)
			{
#if 0
				Intensity cDiff = Intensity(t->maxVol - t->minVol);

				Colour c1;
				Colour c2;

				if(cDiff == 0)
				{
					c1 = t->baseColour;
					c2 = t->baseColour;
				}
				else
				{
					c1 = Colour(t->baseColour + ((line->cMin - t->minVol) * t->colourRange) / cDiff);
					c2 = Colour(t->baseColour + ((line->cMax - t->minVol) * t->colourRange) / cDiff);
				}

				if(c1 == c2)
					image->HLineFast(line->min, y, line->max - line->min + 1, c1, texture);
				else
					image->HLineFast(line->min, y, line->max - line->min + 1, t->baseColour, line->cMin, line->cMax, texture);
#else
				image->HLineFast(line->min, y, line->max - line->min + 1, t, line->cMin, line->cMax, texture);
#endif
			}

			line++;
			y++;
		}
	}
	else
	{
		while(count--)
		{
			if(line->min <= line->max)
			{
#if 0
				Intensity cDiff = Intensity(t->maxVol - t->minVol);

				Colour c1;
				Colour c2;

				if(cDiff == 0)
				{
					c1 = t->baseColour;
					c2 = t->baseColour;
				}
				else
				{
					c1 = Colour(t->baseColour + ((line->cMin - t->minVol) * t->colourRange) / cDiff);
					c2 = Colour(t->baseColour + ((line->cMax - t->minVol) * t->colourRange) / cDiff);
				}

				if(c1 == c2)
					image->HLineFast(line->min, y, line->max - line->min + 1, c1);
				else
					image->HLineFast(line->min, y, line->max - line->min + 1, t, line->cMin, line->cMax);
#else
				image->HLineFast(line->min, y, line->max - line->min + 1, t, line->cMin, line->cMax);
#endif
			}

			line++;
			y++;
		}
	}

}

void Polygon3DI::setTerrainColMinMax()
{
	if(terrainCol)
	{
		PolyPointIndex i = nPoints;
		Point3DI* p = points;
		while(i--)
		{
			terrainCol->addIntensity(p->intensity);
			p++;
		}
	}
}

void Polygon3DI::render(System3D& drawData) const
{
	/*
	 * No need to clip, because that was already done when the polygon
	 * was created.
	 */

	if(nPoints < 3)
		return;

	/*
	 * Work out rectangle enclosing the polygon
	 */

	Cord3D minY = drawData.bitmap->getH();
	Cord3D maxY = 0;

	{
		PolyPointIndex i = nPoints;
		Point3DI* p = points;
		while(i--)
		{
			if(p->y < minY)
				minY = p->y;
			if(p->y > maxY)
				maxY = p->y;

			p++;
		}
	}

	Cord3D size = maxY - minY + 1;

	/*
	 * Make up scan line table
	 */

	Scan3DI lines(size, minY);

	{
		PolyPointIndex i = nPoints;
		Point3DI* p = points;
		Point3DI* lastPoint = &p[i-1];

		while(i--)
		{
			lines.addLine(*lastPoint, *p);
			lastPoint = p++;
		}
	}

	lines.draw(drawData.bitmap, terrainCol, pattern, &drawData.terrain);

#ifdef DEBUG1
	if(optShow())
		globalMouse->blit(*bm, Point(32+16, 32+19));
#endif
}

#endif	// OLD Stuff

