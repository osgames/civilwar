/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Generals
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.17  1994/06/09  23:32:59  Steven_Green
 * Changed Fonts
 *
 * Revision 1.14  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.1  1994/02/03  23:31:36  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "generals.h"
#include "error.h"
#include "ob.h"
#include "strutil.h"

#if !defined(TESTBATTLE) && !defined(BATEDIT)
#include "campaign.h"
#include "campwld.h"
#endif

#include "memptr.h"
#include "game.h"
#include "tables.h"
#include "ai_unit.h"


#if !defined(TESTCAMP) && !defined(CAMPEDIT)
#include "unit3d.h"
#endif

#ifndef BATEDIT
#include "camptab.h"
#endif

#if !defined(BATEDIT) && !defined(TESTBATTLE) && !defined(CAMPEDIT)
#include "campcomb.h"
#include "ai_camp.h"
#endif


#ifdef CHRIS_AI
#if !defined(TESTBATTLE) && !defined(BATEDIT)
#include "ai_camp.h"
#endif
#endif


#ifdef OLD_TESTING
#include "trig.h"
#include "miscfunc.h"
#endif

#if defined(DEBUG) && (defined(BATEDIT) || defined(CAMPEDIT))
#include "dialogue.h"
#endif

#ifdef DEBUG
#include "memlog.h"
#endif

#ifdef DEBUG
#if !defined(BATEDIT) && !defined(CAMPEDIT)
#include "clog.h"
LogFile unitLog("unit.log");

static char* attachModeStr[] = {
	"Attached",
	"Detached",
	"Joining"
};


#endif
#endif

#if defined(DEBUG) && !defined(BATEDIT) && !defined(CAMPEDIT)
#include "patchlog.h"
#endif


/*==========================================================================
 * General Functions
 */

General::General(char* n, Side s)
{
	// name = copyString(n);
	setName(copyString(n));

	allocation = 0;
	rank = Rank_Brigade;
	side = s;
	inactive = False;

#ifdef DEBUG
	efficiency = game->gameRand(MaxAttribute);
	ability    = game->gameRand(MaxAttribute);
	aggression  = game->gameRand(MaxAttribute);
	experience = game->gameRand(MaxAttribute);
#endif
}

/*
 * Constructor set things up for default values
 */

General::General()
{
	// name = 0;
	allocation = 0;
	rank = Rank_Brigade;
	side = SIDE_None;
	inactive = False;

#ifdef DEBUG
	efficiency = game->gameRand(MaxAttribute);
	ability    = game->gameRand(MaxAttribute);
	aggression  = game->gameRand(MaxAttribute);
	experience = game->gameRand(MaxAttribute);
#endif
}
 
General::~General()
{
	// delete[] name;
}


/*
 * Is general g1 better than g2?
 */

inline int generalValue(const General* g)
{
	return g->ability + 
			 g->efficiency + 
			 g->experience +
			 game->gameRand(50) - game->gameRand(50); 
}


Boolean generalBetter(const General* g1, const General* g2)
{
	int a1 = generalValue(g1);
	int a2 = generalValue(g2);

	return a1 > a2;
}


/*==========================================================================
 * Unit functions
 */

/*
 * Constructor
 */

Unit::Unit(Rank r, const Location& l) :
	MapObject(l, OT_Unit),
	realOrders(ORDER_Stand, ORDER_Land, l),
	givenOrders(ORDER_Stand, ORDER_Land, l)
{
	rank = r;
	initValues();
}

Unit::Unit(Rank r) :
	MapObject(OT_Unit)
{
	rank = r;
	initValues();
}

void Unit::initValues()
{
	nextDestination = location;
	localDestination = location;
	needLocalDestination = True;

	superior = 0;
	general = 0;

	childCount = 0;
	child = 0;
	sister = 0;
	
	facing = 0;

	if(rank <= Rank_Army)			// Army and President's are independant
	{
		seperated = True;
		reallySeperated = True;
	}
	else
	{
		seperated = False;
		reallySeperated = False;
	}

	if(rank <= Rank_President)
	{
		hasSeperated = True;
		hasReallySeperated = True;
	}
	else
	{
		hasSeperated = False;
		hasReallySeperated = False;
	}
	joining = False;
	moveMode = MoveMode_Standing;
	moveMethod = ORDER_Land;
	newOrder = True;
	givenNewOrder = False;

	canBeSeen = True;

#if !defined(CAMPEDIT)

#if !defined(TESTCAMP)
	battleInfo = 0;
#endif	// TESTCAMP

#if defined(TESTBATTLE)
	aiInfo = 0;
#elif !defined(BATEDIT)
	// if( (game->playerCount == 1) && (rank < Rank_Regiment))
	if(game->isAIactive() && (rank < Rank_Regiment) && (rank > Rank_President))
		aiInfo = new UnitAI;
	else
		aiInfo = 0;
#endif	// TESTBATTLE
#endif	// CAMPEDIT

	occupying = 0;
	siegeAction = SIEGE_None;
	inBattle = False;
	convertedForBattle = False;
}



Unit::~Unit()
{
#if !defined(CAMPEDIT) && !defined(TESTCAMP)
	delete battleInfo;
#endif	// CAMPEDIT
#if !defined(TESTBATTLE) && !defined(BATEDIT) && !defined(CAMPEDIT)
	delete aiInfo;
#endif

	if(general)
		delete general;
}


#if !defined(CAMPEDIT) && !defined(BATEDIT)
void Unit::setCanBeSeenFlag()
{
#if 0
	if( game->playersSide != getSide() )
		canBeSeen = True;
#else
	if(game->isPlayer(getSide()))
		canBeSeen = True;
#endif
}
#else
void Unit::setCanBeSeenFlag()
{
}
#endif

/*
 * Set a Unit and all his non-independant subordinates to a set location
 * This is typically used when being moved by transport
 */

void Unit::setLocation(const Location& l)
{
	// Patch... 7th February 1996
	ASSERT(this != 0);
	if(this == 0)
	{
#if defined(PATCHLOG)
		patchLog.printf("Patch 1.1.6: setLocation with NULL this");
#endif
		return;
	}
	//.... end of patch

#ifdef RECURSIVE_METHOD
	location = l;
	if(child)
	{
		Unit* u = child;
		while(u)
		{
			if(!u->isCampaignReallyIndependant())
				u->setLocation(l);
			u = u->sister;
		}
	}
#else	// ITERATIVE

	location = l;

	Unit* u = child;

	while(u && (u != this))
	{
		// if(!u->isCampaignReallyIndependant())
		if(u->getCampaignAttachMode() == Attached)
			u->location = l;

		// if(!u->isCampaignReallyIndependant() && u->child)
		if((u->getCampaignAttachMode() == Attached) && u->child)
			u = u->child;
		else if(u->sister)
			u = u->sister;
		else
		{
			do
				u = u->superior;
			while(!u->sister && (u != this));

			if(u == this)
				break;

			u = u->sister;
		}
	}

#endif
}

void Unit::setMoveMode(MoveMode newMode)
{
#ifdef RECURSIVE_METHOD
	moveMode = newMode;
	if(child)
	{
		Unit* u = child;
		while(u)
		{
			if(!u->isCampaignReallyIndependant())
				u->setLocation(l);
			u = u->sister;
		}
	}
#else	// ITERATIVE

	moveMode = newMode;

	Unit* u = child;

	while(u && (u != this))
	{
		// if(!u->isCampaignReallyIndependant())
		if(u->getCampaignAttachMode() == Attached)
			u->moveMode = newMode;

		if((u->getCampaignAttachMode() == Attached) && u->child && (u->rank < Rank_Brigade))
			u = u->child;
		else if(u->sister)
			u = u->sister;
		else
		{
			do
				u = u->superior;
			while(!u->sister && (u != this));

			if(u == this)
				break;

			u = u->sister;
		}
	}

#endif
}


#if !defined(CAMPEDIT) && !defined(TESTCAMP)
Boolean Unit::isBattleDetached()
{
	if(battleInfo)
		return battleInfo->bSeperated;
	else
		return seperated;
}

Boolean Unit::hasBattleDetached()
{
	if(battleInfo)
		return battleInfo->bHasSeperated;
	else
		return hasSeperated;
}

Boolean Unit::isBattleReallyDetached()
{
	if(battleInfo)
		return battleInfo->bReallySeperated;
	else
		return reallySeperated;
}

Boolean Unit::hasBattleReallyDetached()
{
	if(battleInfo)
		return battleInfo->bHasReallySeperated;
	else
		return hasReallySeperated;
}
#endif	// CAMPEDIT

Boolean Unit::isJoining()
{
#if !defined(CAMPEDIT) && !defined(TESTCAMP)
	if(battleInfo)
		return battleInfo->bJoining;
	else
#endif	// CAMPEDIT
		return joining;
}

void Unit::makeCampaignDetached(Boolean flag)
{
#if defined(DEBUG_INDEPENDANT) && !defined(CAMPEDIT) && !defined(BATEDIT)
	unitLog.printf("makeCampaignDetached(%s, %s)",
		getName(True),
		flag ? "True" : "False");
#endif

	if(flag != seperated)
	{
		ASSERT(getRank() > Rank_Army);
		ASSERT(getRank() < Rank_Regiment);

		if(getRank() == Rank_Regiment)
#ifdef TESTING
#ifdef DEBUG
			throw
#endif
			GeneralError("Making regiment %s Detached!", getName(True));
#else
			return;
#endif

		seperated = flag;

#if 0
		/*
		 * If it is becomming Detached, then clear the joining flag
		 */

#if 0
		if(flag)
			joining = False;
#endif

		if(flag)
		{
			// Go up tree and mark superiors
			Unit* u = superior;

			while(u)
			{
				u->hasSeperated = True;
				u = u->superior;
			}
		}
		else
		{
			// Go up and unmark superiors

			Unit* u = superior;

			while(u)
			{
				Unit* c = u->child;
				Boolean has = False;

				while(c)
				{
					if(c->hasSeperated || c->seperated)
					{
						has = True;
						break;
					}
					c = c->sister;
				}

				if(has)
					break;
				else
					u->hasSeperated = False;

				u = u->superior;
			}
		}
#else

		superior->updateHasSeperated();

#endif

	}
}


void Unit::makeCampaignReallyDetached(Boolean flag)
{
#if defined(DEBUG_INDEPENDANT) && !defined(CAMPEDIT) && !defined(BATEDIT)
	unitLog.printf("makeCampaignReallyDetached(%s, %s)",
		getName(True),
		flag ? "True" : "False");
#endif

	if(flag != reallySeperated)
	{
		ASSERT(getRank() > Rank_Army);
		ASSERT(getRank() < Rank_Regiment);

		reallySeperated = flag;
#if 0
		if(flag)
		{
			// Go up tree and mark superiors
			Unit* u = superior;

			while(u)
			{
				u->hasReallySeperated = True;
				u = u->superior;
			}
		}
		else
		{
			// Go up and unmark superiors

			Unit* u = superior;

			while(u)
			{
				Unit* c = u->child;
				Boolean has = False;

				while(c)
				{
					if(c->hasReallySeperated || c->reallySeperated)
					{
						has = True;
						break;
					}
					c = c->sister;
				}

				if(has)
					break;
				else
					u->hasReallySeperated = False;

				u = u->superior;
			}
		}
#else

		superior->updateHasSeperated();

#endif
	}
}


#if !defined(CAMPEDIT) && !defined(TESTCAMP)

void Unit::makeBattleDetached(Boolean flag)
{
#if defined(DEBUG_INDEPENDANT) && !defined(CAMPEDIT) && !defined(BATEDIT)
	unitLog.printf("makeBattleDetached(%s, %s)",
		getName(True),
		flag ? "True" : "False");
#endif


	if(battleInfo)
	{
		ASSERT(flag || (getRank() > Rank_Army));			// Armies must be detached
		ASSERT(!flag || (getRank() < Rank_Regiment));	// Regiments must be attached

		if(flag != battleInfo->bSeperated)
		{
			battleInfo->bSeperated = flag;

			if(flag)
			{
				// Go up tree and mark superiors
				Unit* u = superior;

				while(u && u->battleInfo)
				{
					u->battleInfo->bHasSeperated = True;
					u = u->superior;
				}
			}
			else
			{
				// Go up and unmark superiors

				Unit* u = superior;

				while(u && u->battleInfo)
				{
					Unit* c = u->child;
					Boolean has = False;

					while(c)
					{
						if(c->battleInfo && (c->battleInfo->bHasSeperated || c->battleInfo->bSeperated))
						{
							has = True;
							break;
						}
						c = c->sister;
					}

					if(has)
						break;
					else
						u->battleInfo->bHasSeperated = False;

					u = u->superior;
				}
			}
		}
	}
}

void Unit::makeBattleReallyDetached(Boolean flag)
{
#if defined(DEBUG_INDEPENDANT) && !defined(CAMPEDIT) && !defined(BATEDIT)
	unitLog.printf("makeBattleReallyDetached(%s, %s)",
		getName(True),
		flag ? "True" : "False");
#endif


	if(battleInfo)
	{
		ASSERT(flag || (getRank() > Rank_Army));			// Armies must be detached
		ASSERT(!flag || (getRank() < Rank_Regiment));	// Regiments must be attached

		if(flag != battleInfo->bReallySeperated)
		{
			battleInfo->bReallySeperated = flag;

			if(flag)
			{
				// Go up tree and mark superiors
				Unit* u = superior;

				while(u && u->battleInfo)
				{
					u->battleInfo->bHasReallySeperated = True;
					u = u->superior;
				}
			}
			else
			{
				// Go up and unmark superiors

				Unit* u = superior;

				while(u && u->battleInfo)
				{
					Unit* c = u->child;
					Boolean has = False;

					while(c)
					{
						if(c->battleInfo && (c->battleInfo->bHasReallySeperated || c->battleInfo->bReallySeperated))
						{
							has = True;
							break;
						}
						c = c->sister;
					}

					if(has)
						break;
					else
						u->battleInfo->bHasReallySeperated = False;

					u = u->superior;
				}
			}
		}
	}
}
#endif

/*
 * Simplified method of handling attaching/detaching/joining
 */

AttachMode Unit::getCampaignAttachMode() const
{

	int val = 0;

	if(reallySeperated)
		val |= 2;
	if(joining)
		val |= 1;

	if(val == 0)
		return Attached;
	else if(val == 2)
		return Detached;
	else if(val == 3)
		return Joining;

#ifdef DEBUG
	else
		 throw GeneralError("Invalid Attached flags for %s (%d)",
		 	getName(True),
			val);
#else
	return Joining;
#endif

}

void Unit::setCampaignAttachMode(AttachMode newMode)
{
#ifdef DEBUG
	if(newMode != getCampaignAttachMode())
	{
		ASSERT(getRank() > Rank_Army);
		ASSERT(getRank() < Rank_Regiment);
	}
#endif

	switch(newMode)
	{
	case Attached:	// 0,0,0
		// makeCampaignDetached(False);
		joining = False;
		makeCampaignReallyDetached(False);
		break;
		
	case Detached:	// 1,1,0
		// makeCampaignDetached(True);
		joining = False;
		makeCampaignReallyDetached(True);
		break;

	case Joining:	// 0,1,1
		// makeCampaignDetached(False);
		makeCampaignReallyDetached(True);
		joining = True;
		break;

#if 0
	case Ordered:	// 1,0,0
		// makeCampaignDetached(True);
		makeCampaignReallyDetached(False);
		joining = False;
		break;
#endif

#ifdef DEBUG
	default:
		throw GeneralError("Illegal AttachMode (%d) for %s", (int) newMode, getName(True));
#endif
	}
}


#if !defined(CAMPEDIT) && !defined(TESTCAMP)

AttachMode Unit::getBattleAttachMode() const
{
	ASSERT(battleInfo != 0);

	int val = 0;
#if 0
	if(battleInfo->bSeperated)
		val |= 4;
	if(battleInfo->bReallySeperated)
		val |= 2;
	if(battleInfo->bJoining)
		val |= 1;

	if(val == 0)
		return Attached;
	else if(val == 6)
		return Detached;
	else if(val == 3)
		return Joining;
	else if(val == 4)
		return Ordered;
#else
	if(battleInfo->bReallySeperated)
		val |= 2;
	if(battleInfo->bJoining)
		val |= 1;

	if(val == 0)
		return Attached;
	else if(val == 2)
		return Detached;
	else if(val == 3)
		return Joining;
#endif

#ifdef DEBUG
	else
#if defined(BATEDIT)
		GeneralError("Invalid Battle Attached flags for %s (%d)",
		 		getName(True), val);
		return Joining;
#else
		throw GeneralError("Invalid Battle Attached flags for %s (%d)",
		 		getName(True), val);
#endif

#else
	else
		return Joining;
#endif

}

void Unit::setBattleAttachMode(AttachMode newMode)
{
#ifdef DEBUG
	ASSERT(battleInfo != 0);

	if(newMode != getBattleAttachMode())
	{
		ASSERT(getRank() > Rank_Army);
		ASSERT(getRank() < Rank_Regiment);
	}
#endif

	switch(newMode)
	{
	case Attached:	// 0,0,0
		// makeBattleDetached(False);
		battleInfo->bJoining = False;
		makeBattleReallyDetached(False);
		break;
		
	case Detached:	// 1,1,0
		// makeBattleDetached(True);
		battleInfo->bJoining = False;
		makeBattleReallyDetached(True);
		break;

	case Joining:	// 0,1,1
		// makeBattleDetached(False);
		battleInfo->bJoining = True;
		makeBattleReallyDetached(True);
		break;

#if 0
	case Ordered:	// 1,0,0
		// makeBattleDetached(True);
		battleInfo->bJoining = False;
		makeBattleReallyDetached(False);
		break;
#endif

#ifdef DEBUG
	default:
		throw GeneralError("Illegal AttachMode (%d) for %s", (int) newMode, getName(True));
#endif
	}
}
#endif

/*
 * Return the most senior Detached commander for a unit
 */

Unit* Unit::getTopCommand()
{
	Unit* u = this;

	// while(!u->isReallyIndependant() && u->superior)
	while((u->getAttachMode() == Attached) && u->superior)
	{
		u = u->superior;
	}

	return u;
}

President* Unit::getPresident()
{
	Unit* u = this;

	while(u->superior)
		u = u->superior;

	ASSERT(u->getRank() == Rank_President);

	return (President*) u;
}


/*
 * Find out what type of a unit it is
 */

const char* getUnitTypeName(UnitInfo& info)
{
	const char* s = language(lge_Inactive);
	int count = 0;

	if(info.infantryStrength)
	{
		s = language(lge_Infantry);
		count++;
	}

	if(info.cavalryStrength)
	{
		s = language(lge_Cavalry);
		count++;
	}

	if(info.artilleryStrength)
	{
		s = language(lge_Artillery);
		count++;
	}

	if(count > 1)
		s = language(lge_Mixed);

	return s;
}


/*
 * Get or set Information (recursively) about a unit
 *
 * If independant is True then only dependant units are totalled up.
 */

void Unit::getInfo(UnitInfo& info) const
{
	info.clear();

	if(childCount)
	{
		int count = 0;

		ULONG moraleTotal			= 0;
		ULONG fatigueTotal		= 0;
		ULONG experienceTotal	= 0;
		ULONG supplyTotal = 0;

		Unit* u = child;

		while(u)
		{
			// if(!onlyDependant || !u->isReallyIndependant())

			Boolean recurse = True;

#if 0
			if(info.onlyCampaignDependant && info.onlyBattleDependant && u->isReallyIndependant() /* && !u->isJoining() */)
				recurse = False;
			else
#endif
			// if(info.onlyCampaignDependant && u->isCampaignReallyIndependant() /* && !u->isJoining() */)
			if(info.onlyCampaignDependant && (u->getCampaignAttachMode() != Attached))
				recurse = False;
#if !defined(CAMPEDIT) && !defined(TESTCAMP)
			else
			if(info.onlyBattleDependant && u->battleInfo && (u->getBattleAttachMode() != Attached))
				recurse = False;
#endif

			if(recurse)
			{
				UnitInfo info1(info.onlyCampaignDependant, info.onlyBattleDependant);

				u->getInfo(info1);

				if(info1.strength)
				{
					moraleTotal			+= info1.morale * info1.strength;
					fatigueTotal		+= info1.fatigue * info1.strength;
					experienceTotal	+= info1.experience * info1.strength;
					supplyTotal += info1.supply * info1.strength;
				}

				/*
			 	 * Minimum speed
			 	 */

				if((count == 0) || (info1.speed < info.speed))
					info.speed = info1.speed;

				info.strength		+= info1.strength;
				info.stragglers	+= info1.stragglers;
				info.casualties	+= info1.casualties;

				info.infantryStrength	+= info1.infantryStrength;
				info.cavalryStrength		+= info1.cavalryStrength;
				info.artilleryStrength	+= info1.artilleryStrength;

				for(int i = 0; i < 3; i++)
				{
					for(int j = 0; j < Inf_TypeCount; j++)
					{
						info.strengths[i][j] += info1.strengths[i][j];
						info.regimentCounts[i][j] += info1.regimentCounts[i][j];
					}
				}

				count++;
			}
			u = u->sister;
		}

		if(count)
		{
			if(info.strength)
			{
				info.morale			= Morale(moraleTotal / info.strength);
				info.fatigue 		= Fatigue(fatigueTotal / info.strength);
				info.experience	= Experience(experienceTotal / info.strength);
				info.supply = Supply(supplyTotal / info.strength);
			}
		}

	}
}


Strength Unit::getStrength() const
{
	Unit* u = child;
	Strength strength = 0;

	while(u)
	{
		strength += u->getStrength();
		u = u->sister;
	}

	return strength;
}

Strength Unit::getDependantStrength() const
{
	Unit* u = child;
	Strength strength = 0;

	while(u)
	{
		// if(!u->isReallyIndependant())
		if(u->getAttachMode() == Attached)
			strength += u->getStrength();
		u = u->sister;
	}

	return strength;
}


Unit* Unit::makeNewUnder(const Location& l)
{
#ifdef DEBUG
	throw GeneralError("virtual Unit::makeNewUnder() undefined");
#else
	return 0;
#endif
}


/*
 * Remove a subordinate from a Unit
 */

void Unit::removeChild(Unit* u)
{
	// u->makeCampaignIndependant(False);
	// u->makeCampaignReallyIndependant(False);

	// u->setCampaignAttachMode(Attached);

	Unit* last = 0;
	Unit* c = child;

	while(c)
	{
		if(c == u)
		{
			if(last)
				last->sister = u->sister;
			else
				child = u->sister;
			u->sister = 0;
			u->superior = 0;

			childCount--;
			break;
		}

		last = c;
		c = c->sister;
	}

	/*
	 * Update hasSeperated and hasReallySeperated flags
	 */

	updateHasSeperated();

}


/*
 * Update a unit's hasSeperated and hasReallySeperated flags
 * continuing upwards.
 */


void Unit::updateHasSeperated()
{
	ASSERT(this != 0);

	Unit* u = this;

	while(u)
	{
		u->hasSeperated = False;
		u->hasReallySeperated = False;

		Unit* c = u->child;
		while(c)
		{
			if(c->hasReallySeperated || c->reallySeperated)
				u->hasReallySeperated = True;

			if(c->hasSeperated || c->seperated)
				u->hasSeperated = True;
			c = c->sister;
		}

		u = u->superior;
	}

#if !defined(TESTBATTLE) && !defined(BATEDIT)
#ifdef DEBUG
	if(campaign && campaign->world)
		campaign->world->ob.check(__FILE__, __LINE__);
#endif
#endif
}

/*
 * Add a unit below another
 */

void Unit::addChild(Unit* u)
{
	ASSERT(u->superior == 0);
	ASSERT(u->sister == 0);

	/*
	 * Master's version patch to stop regiments attaching to regiments
	 */

	ASSERT(getRank() < Rank_Regiment);
	if(getRank() >= Rank_Regiment)
	{
		ASSERT(superior != 0);
		if(superior)
			superior->addChild(u);
		return;
	}

	if(getRank() == Rank_Brigade)
	{
#if defined(DEBUG)
		if(childCount >= MaxRegimentsPerBrigade)
		{
#if defined(BATEDIT) || defined(CAMPEDIT)
			MemPtr<char> buffer(500);
			sprintf(buffer, "%s\rhas more than %d regiments\rThrowing away\r%s",
				getName(True),
				(int) MaxRegimentsPerBrigade,
				u->getName(False));
			dialAlert(0, buffer, "OK");
#else
			patchLog.printf("%s has more than %d regiments, throwing away %s",
				getName(True),
				(int) MaxRegimentsPerBrigade,
				u->getName(False));
#endif
		}

#if !defined(BATEDIT)
		ASSERT(childCount < MaxRegimentsPerBrigade);
#endif

#endif
		if(childCount >= MaxRegimentsPerBrigade)
			return;
	}

	u->superior = this;

	Unit* last = child;

	if(last)
	{
		while(last->sister)
			last = last->sister;
		last->sister = u;
	}
	else
		child = u;

	childCount++;

#ifdef DEBUG
	int cCount = 0;
	last = child;
	while(last && (cCount < 20))		// 20 for looping check
	{
		cCount++;
		last = last->sister;
	}
	ASSERT(cCount == childCount);
#endif


#if 0
	if(u->hasSeperated || u->seperated)
	{
		last = this;
		while(last)
		{
			last->hasSeperated = True;
			last = last->superior;
		}
	}
	
	if(u->hasReallySeperated || u->reallySeperated)
	{
		last = this;
		while(last)
		{
			last->hasReallySeperated = True;
			last = last->superior;
		}
	}

#if defined(DEBUG) && !defined(BATEDIT)
	if(campaign && campaign->world)
		campaign->world->ob.check(__FILE__, __LINE__);
#endif
#else

	updateHasSeperated();

#endif

}

/*
 * Find out if a unit has any unattached units
 * If recurse is True, then check the complete tree below the unit
 */

Boolean Unit::hasUnattached(Boolean recurse) const
{
#ifdef OLD_VERSION

	if(recurse)
	{
#ifndef CAMPEDIT
		if(battleInfo)
			return battleInfo->bHasSeperated;
		else
#endif
			return hasSeperated;
	}
	else
	{
		Unit* u = child;
		while(u)
		{
			if(battleInfo)
			{
				if(u->battleInfo && u->isBattleIndependant())
					return True;
			}
			else
			{
				if(u->isIndependant())
					return True;
			}
			u = u->sister;
		}
		return False;
	}

#else

	/*
	 * Slower version, but is more likely to be correct
	 *
	 * This routine is only ever used in CAL to see if units can
	 * reattach.
	 */

	Unit* u = child;
	while(u)
	{
#if !defined(CAMPEDIT) && !defined(TESTCAMP)
		if(battleInfo)		// Battle version
		{
			// if(u->battleInfo && u->isBattleIndependant() && !u->isJoining())
			if(u->battleInfo && (u->getBattleAttachMode() == Detached))
				return True;
		}
		else
#endif
		{
			// if(u->isIndependant() && !u->isJoining())
			if(u->getAttachMode() == Detached)
				return True;
		}
		 
		if(recurse)
			if(u->hasUnattached(True))
				return True;

		u = u->sister;
	}

	return False;
#endif
}

/*
 * Reattach all of a unit's troops.
 */

#if !defined(CAMPEDIT) && !defined(TESTCAMP)

void Unit::reattachBattle(Boolean recurse)
{
#if defined(DEBUG) && !defined(BATEDIT)
	unitLog.printf("reattach(%s,%s)",
		getName(True), recurse ? "True" : "False");
#endif


	Unit* unit = child;

	while(unit)
	{
		if(unit->battleInfo)
		{
			unit->rejoinBattle();

			if(recurse && unit->child)
				unit->reattachBattle(True);
		}

		unit = unit->sister;
	}
}

#endif

#if !defined(BATEDIT) && !defined(TESTBATTLE)
void Unit::reattachCampaign(Boolean recurse)
{
#if defined(DEBUG) && !defined(CAMPEDIT)
	unitLog.printf("reattachCampaign(%s,%s)",
		getName(True), recurse ? "True" : "False");
#endif


	Unit* unit = child;

	while(unit)
	{
		unit->rejoinCampaign();

		if(recurse && unit->child)
			unit->reattachCampaign(True);

		unit = unit->sister;
	}
}
#endif

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
void Unit::rejoinBattle()
{
	ASSERT(rank > Rank_Army);
	ASSERT(superior != 0);

#if defined(DEBUG) && !defined(BATEDIT)
	unitLog.printf("rejoinBattle(%s,%s)",
		getName(True),
		attachModeStr[getBattleAttachMode()]);
#endif

	if(battleInfo)
	{
		if(isBattleReallyDetached())
			setBattleAttachMode(Joining);
		else
			setBattleAttachMode(Attached);

		if(superior && superior->battleInfo)
			battleInfo->battleOrder = superior->battleInfo->battleOrder;
	}
}
#endif


#if !defined(BATEDIT) && !defined(TESTBATTLE)
void Unit::rejoinCampaign()
{
#ifdef DEBUG
	if(campaign && campaign->world)
		campaign->world->ob.check(__FILE__, __LINE__);
#endif

	ASSERT(rank > Rank_Army);
	ASSERT(superior != 0);

#if defined(DEBUG) && !defined(CAMPEDIT)
	unitLog.printf("rejoinCampaign(%s)",
		getName(True));
#endif

	/*
	 * If commander currently has no dependants, then set up the
	 * commanders orders to be the child
	 */

	int cCount = 0;
	Unit* c = superior->child;
	while(c)
	{
		// if(!c->isCampaignIndependant() || c->isJoining())
		if(c->getCampaignAttachMode() != Detached)
			cCount++;
		c = c->sister;
	}


#ifdef CAMPEDIT
	// makeCampaignIndependant(False);
	// makeCampaignReallyIndependant(False);
	setCampaignAttachMode(Attached);
#else
#if 0
	if(isCampaignReallyIndependant())
	{
		joining = True;
	}
	makeCampaignIndependant(False);
#else
	if(isCampaignReallyDetached())
		setCampaignAttachMode(Joining);
	else
		setCampaignAttachMode(Attached);
#endif

	if(occupying)
		actOnCity(occupying, this, SIEGE_None);


	if(realOrders.how == ORDER_Land)
	{
#if 0
		realOrders = superior->realOrders;
		givenOrders = superior->givenOrders;
#else

		/*
		 * Bodge... give march orders
		 */

		if(cCount >= 1)
		{
			// realOrders = Order(ORDER_Advance, ORDER_Land, superior->realOrders.destination);
			// givenOrders = realOrders;
			setOrderNow(Order(ORDER_Advance, ORDER_Land, superior->realOrders.destination));
		}
		else
		{
			// superior->realOrders = Order(ORDER_Advance, ORDER_Land, realOrders.destination);
			// superior->givenOrders = superior->realOrders;
			superior->setOrderNow(Order(ORDER_Advance, ORDER_Land, realOrders.destination));
		}
		
#endif
	}
	orderList.clear();

#ifdef DEBUG
	if(campaign && campaign->world)
		campaign->world->ob.check(__FILE__, __LINE__);
#endif

#endif	// CAMPEDIT
}
#endif	// BATEDIT && TESTBATTLE

/*
 * Find what position unit is in organisation
 * e.g. 1st Corps, 2nd Corps..etc
 * 1st Unit returns 0...
 */

int Unit::getOrgPosition() const
{
	Unit* unit;		// Set this to the 1st child

	if(superior)
		unit = superior->child;
	else
		return getSide();		// President special case

	int count = 0;

	while(unit && (unit != this))
	{
		unit = unit->sister;
		count++;
	}

	// Handle case of unit not found (shouldn't happen!)

	if(!unit)
		count = 0;

	return count;
}

#if 0
/*
 * Amalgamate 2 units
 *
 * If they are the same rank, then they are "tied" together.
 * otherwise, the lower rank is transferred to the higher rank
 */

Unit* joinForces(Unit* u1, Unit* u2)
{

	/*
	 *   Check unit to be removed has more than one child
 	 */

	Boolean outFlag = False;

	while ( u2->rank > Rank_Army && !outFlag && !u2->isCampaignReallyIndependant() ) 
	{
		Unit* temp = u2->superior->child;

		int count = 0;

		while ( temp )
		{
		 	if ( temp->isCampaignReallyIndependant() )
				count++;

			temp = temp->sister;
		}

		// if ( count < (u2->superior->childCount - 1) )
		if(count > 1)
			outFlag = 1;
		else
			u2 = u2->superior;
	}


	outFlag = False;

	while ( u1->rank > Rank_Army && !outFlag  && !u1->isCampaignReallyIndependant()) 
	{
		Unit* temp = u1->superior->child;

		int count = 0;

		while ( temp )
		{
		 	if(temp->isCampaignReallyIndependant())
				count++;

			temp = temp->sister;
		}

		// if ( count < (u1->superior->childCount - 1) )
		if(count > 1)
			outFlag = 1;
		else
			u1 = u1->superior;
	}

	/*
	 * If equal rank, then tie together
	 */

	if(u1->rank == u2->rank)
	{
		;
	}
	else
	{
		/*
		 * Set it so that u1 has the highest rank
		 */

		if(u1->rank > u2->rank)
			swap(u1, u2);

		/*
		 * Remove from organisation
		 */

		u2->superior->removeChild(u2);

		/*
		 * Find suitable place to add new unit
		 * For now this simply adds to 1st child of apropriate rank
		 * It really ought to try to balance the tree by adding it
		 * the organisation with the fewest of the given rank.
		 */

		Rank wantedRank = promoteRank(u2->rank);

		Unit* uTemp = u1;

		while(uTemp->rank < wantedRank)
			uTemp = uTemp->child;

		uTemp->addChild(u2);


	}
	return u1;
}
#endif


#if !defined(CAMPEDIT) && !defined(BATEDIT)
/*
 * Send an order to a unit... this may be delayed!
 */

void Unit::sendOrder(const Order& order)
{
#if 0
#if defined(DEBUG) && !defined(TESTBATTLE) && !defined(BATEDIT)
	unitLog.printf("%s given order to %s",
		getName(True), getOrderStr(order));
#endif
#endif

	/*
	 * Find the top most commander that is not a lone independant unit
	 */

	Unit* u = this;

	/*
	 * Check that this is not the last remaining unit in its organisation
	 * If it is then go up a level and order them instead.
	 *
	 * This is to try and prevent generals with no dependant units below them
	 */

 		while(u->rank > Rank_Army)
		{
			Unit* top = u->superior->child;

			while(top)
			{
			 	if( ( !top->isCampaignDetached() ) && (top != u) ) 
					goto getOut;

				top = top->sister;
			}

			u->makeCampaignDetached(False);

			u = u->superior;
		}

getOut:

#if 0
#if defined(DEBUG) && !defined(TESTBATTLE)
	unitLog.printf("Order being given to %s", u->getName(True));
#endif
#endif

	u->giveOrders(order);
	u->givenNewOrder = True;
	u->makeCampaignDetached(True);
	// u->setCampaignAttachMode(Detached);
}
#endif	// CAMPEDIT

void Unit::giveOrders(const Order& order)
{
	givenOrders = order;

	if(givenOrders.basicOrder() == ORDERMODE_Hold)
		givenOrders.destination = location;


	Unit* u = child;
	while(u)
	{
		// if(!u->isCampaignReallyIndependant() && !u->isCampaignIndependant())
		if( (u->getCampaignAttachMode() != Detached) &&
			 !u->isCampaignDetached())
			u->giveOrders(order);
		u = u->sister;
	}
}

/*
 * Set an order immediately
 */

void Unit::setOrder(OrderMode mode, const Location& destination)
{
	realOrders.setOrder(mode, destination); 

	if(realOrders.basicOrder() == ORDERMODE_Hold)
		realOrders.destination = location;

	setCampaignAttachMode(Detached);

	/*
	 * Also update given orders...
	 */

	givenOrders = realOrders;

}

void Unit::setOrderNow(const Order& order)
{
#if defined(DEBUG) && !defined(TESTBATTLE) && !defined(BATEDIT) && !defined(CAMPEDIT)
	unitLog.printf("setOrderNow(%s, %s)",
		getName(True),
		getOrderStr(order));
#endif

	realOrders = order;
	givenOrders = order;
	newOrder = True;

	if(givenOrders.basicOrder() == ORDERMODE_Hold)
	{
		givenOrders.destination = location;
		realOrders.destination = location;
	}

	Unit* u = child;
	while(u)
	{
		if(!u->isCampaignReallyDetached())
			u->setOrderNow(order);
		u = u->sister;
	}
}

/*==========================================================================
 * Named Units
 */

void NamedUnit::setName(char* s)
{
	if(name)
		delete[] name;
	name = s;
}


/*==========================================================================
 * President Functions
 */

/*
 * Constructor (Add to sides list)
 */

President::President(Side sd) :
	Unit(Rank_President, Location(0,0))
{
	side = sd;
	freeGenerals = new GeneralList;
	freeRegiments = 0;		// new RegimentList;
}

President::President() :
	Unit(Rank_President)
{
	freeGenerals = new GeneralList;
	freeRegiments = 0;		// new RegimentList;
}

President::~President()
{
	freeGenerals->clearAndDestroy();

	Unit* u = freeRegiments;
	while(u)
	{
		Unit* u1 = u;
		u = u->sister;
		delete u1;
	}

	delete freeGenerals;
}

/*
 * Free General support
 */

void President::addFreeGeneral(General* g)
{
	freeGenerals->append(g); 
}

void President::removeFreeGeneral(General* g)
{
	freeGenerals->remove(g);
}

Unit* President::makeNewUnder(const Location& l)
{
	Army* army = new Army(l, 0);

	addChild(army);

#if defined(BATEDIT)
	army->makeBattleInfo();
#endif

	return army;
}

void President::addFreeRegiment(Regiment* r)
{
	Unit* u = freeRegiments;

	while(u && u->sister)
		u = u->sister;

	if(u)
		u->sister = r;
	else
		freeRegiments = r;

	r->sister = 0;
}

/*
 * Remove Regiment from free Regiment List
 * Returns:
 *		True if it was really removed
 *		False if not found in list
 */

Boolean President::removeFreeRegiment(Regiment* r)
{
	Unit* u = freeRegiments;
	Unit* last = 0;

	r->joining = False;			// No longer inactive

	while(u && (u != r))
	{
		last = u;
		u = u->sister;
	}

	if(u)
	{
		if(last)
			last->sister = u->sister;
		else
			freeRegiments = (Regiment*) u->sister;

		u->sister = 0;

		return True;
	}

	return False;
}

int President::freeRegimentCount() const
{
	int count = 0;
	Unit* u = freeRegiments;
	while(u)
	{
		u = u->sister;
		count++;
	}
	return count;
}

/*
 * Daily president update
 *
 * Clears unallocated general/regiment's inactive flags
 */

void President::dailyUpdate()
{
	Unit* u = freeRegiments;
	while(u)
	{
		u->joining = False;
		u = u->sister;
	}

	GeneralIter glist = *freeGenerals;

	while(++glist)
		glist.current()->inactive = False;
}

/*==========================================================================
 * Army Functions
 */

Army::Army(const Location& l, const char* n) :
	Unit(Rank_Army, l)
{
	if(n)
		setName(copyString(n));

	// if(n)
	//	name = copyString(n);
	// else
	//	name = 0;		// copyString(language(lge_Army));

#ifdef LIMIT_ARMY_MOVEMENT
	memset(NDefensiveFO, 0, sizeof(NDefensiveFO));
	memset(NoffensiveFO, 0, sizeof(NoffensiveFO));
#endif
}

Army::Army() : 
	Unit(Rank_Army)
{
	// name = 0;

#ifdef LIMIT_ARMY_MOVEMENT
	memset(NDefensiveFO, 0, sizeof(NDefensiveFO));
	memset(NoffensiveFO, 0, sizeof(NoffensiveFO));
#endif
}


Army::~Army()
{
	// delete[] name;
}

Unit* Army::makeNewUnder(const Location& l)
{
	Corps* corps = new Corps(l);
	addChild(corps);

#if defined(BATEDIT)
	corps->makeBattleInfo();
#endif

	return corps;
}


/*=====================================================================
 * Army Names
 */


#if !defined(TESTBATTLE) && !defined(BATEDIT)
/*
 * Make an army name...
 *
 * It will be called:
 *   <nth> Army of <state>
 *
 * state will be the state closest to where it is being created on the right side!
 * If an army already exists with this name then the <nth> will be made.
 */

void Army::makeArmyName()
{
#if defined(DEBUG) && !defined(CAMPEDIT)
	unitLog.printf("Makeing up a new army name");
#endif

	ASSERT(this != 0);
	ASSERT(getRealName() == 0);

	const Location& l = location;
	Side side = getSide();

	StateListIter iter = campaign->world->states;

	State* bestState = 0;
	Distance bestD = 0;

	while(++iter)
	{
		State& state = iter.current();

		if(state.side == side)
		{
			Distance d = distanceLocation(l, state.location);
		
			if(!bestState || (d < bestD))
			{
				bestState = &state;
				bestD = d;
			}
		}
	}

	ASSERT(bestState != 0);

	if(bestState)
	{
#if defined(DEBUG) && !defined(CAMPEDIT)
		unitLog.printf("Army in state: %s", bestState->fullName);
#endif

		char* stateName = copyString(bestState->fullName);
		strupr(stateName);

		/*
		 * We have the closest state now...
		 */

		// Unit* u = ob.getPresident(side);
		Unit* u = superior;

		ASSERT(u != 0);

		u = u->child;

		int maxUsed = 0;

		while(u)
		{
			ASSERT(u->getRank() == Rank_Army);

			const char* s = u->getName(False);

			// Copy state and army name, so that we can make them upper case

			char* armyName = copyString(s);
			strupr(armyName);

			if(strstr(armyName, stateName) != 0)
			{
				// Scan for digits...
#if defined(DEBUG) && !defined(CAMPEDIT)
				unitLog.printf("%s already in state", s);
#endif
				int i = 1;

				char* number = strpbrk(armyName, "0123456789");
				if(number)
				{
					i = atoi(number);
#if defined(DEBUG) && !defined(CAMPEDIT)
					unitLog.printf("Army count = %d", i);
#endif
				}

				if(i > maxUsed)
					maxUsed = i;
			}

			delete[] armyName;

			u = u->sister;
		}

		delete[] stateName;

		/*
		 * Make up our new name
		 */

#if 1
		MemPtr<char> newName(200);

		if(maxUsed)
			sprintf(newName, "%s ", makeNths(maxUsed + 1));
		else
			newName[0] = 0;

		sprintf(newName + strlen(newName), language(lge_army_of), bestState->fullName);
#else
		String newName;

		if(maxUsed)
		{
			newName = makeNths(maxUsed + 1);
			newName += " ";
		}

		newName += language(lge_army_of);
		newName += " ";
		newName += bestState->fullName;

#endif

#if defined(DEBUG) && !defined(CAMPEDIT)
		unitLog.printf("Army name = %s", (const char*)newName);
#endif

		setName(copyString(newName));
	}
}

#endif 	// TESTBATTLE



/*==========================================================================
 * Corps Functions
 */

Corps::Corps(const Location& l) :
	Unit(Rank_Corps, l)
{

}

Corps::~Corps()
{
}

Unit* Corps::makeNewUnder(const Location& l)
{
	Division* division = new Division(l);
	addChild(division);

#if defined(BATEDIT)
	division->makeBattleInfo();
#endif

	return division;
}


/*==========================================================================
 * Division Functions
 */

Division::Division(const Location& l) :
	Unit(Rank_Division, l)
{

}

Division::~Division()
{
}

Unit* Division::makeNewUnder(const Location& l)
{
	Brigade* brig = new Brigade(l);
	addChild(brig);

#if defined(BATEDIT)
	brig->makeBattleInfo();
#endif

	return brig;
}

/*==========================================================================
 * Brigade Functions
 */

Brigade::Brigade(const Location& l) :
	Unit(Rank_Brigade, l)
{
	resting = False;
	holdBack = False;
	moved = False;
	movedTime = 0;
}

Brigade::~Brigade()
{
}

Unit* Brigade::makeNewUnder(const Location& l)
{
	ASSERT(childCount < MaxRegimentsPerBrigade);

	Regiment* reg = new Regiment();
	reg->location = l;
	addChild(reg);

#if defined(BATEDIT)
	reg->makeBattleInfo();
#endif

	return reg;
}

/*==========================================================================
 * Regiment Functions
 */

Regiment::Regiment(const Location& l, const char* n, RegimentType t) :
	Unit(Rank_Regiment, l)
{
	initValues();
	// name = copyString(n);
	setName(copyString(n));
	type = t;
}

Regiment::Regiment() : Unit(Rank_Regiment)
{
	initValues();
	type.basicType = Infantry;
	type.subType = Inf_Militia;
	// name = 0;
}


void Regiment::initValues()
{
	strength = 0;
	stragglers = 0;
	casualties = 0;
	fatigue = 0;
	morale = MaxAttribute;	// / 2;
	supply = MaxAttribute;
	experience = 0;
}

Regiment::~Regiment()
{
	// delete[] name;
}

void Regiment::setInfo(UnitInfo& info)
{
	strength		= info.strength;
	stragglers	= info.stragglers;
	casualties	= info.casualties;
	fatigue		= info.fatigue;
	morale		= info.morale;
	supply 		= info.supply;
	experience	= info.experience;
}

void Regiment::getInfo(UnitInfo& info) const
{
	info.clear();

	info.regimentCount++;

	info.strength		= strength;
	info.stragglers	= stragglers;
	info.casualties	= casualties;
	info.fatigue		= fatigue;
	info.morale			= morale;
	
	// info.speed			= speed;
	info.speed = regimentMarchSpeeds[type.basicType][type.subType];

#if !defined(CAMPEDIT) && !defined(BATEDIT)
	/*
	 * Alter speed based on fatigue and stragglers
	 */

	if(game->getDifficulty(Diff_Fatigue) >=  Complex1)
		info.speed -= (info.speed * info.fatigue) / MaxAttribute;
#endif

	if(info.strength == 0)
		info.speed = 0;
	else if(info.stragglers)
	{
		if(info.strength > info.stragglers)
			info.speed -= (info.stragglers * info.speed) / (info.strength * 2);
		else
			info.speed = (info.strength * info.speed) / (info.stragglers * 2);
	}

#if 0
	/*
	 * Bodge to prevent zero speed
	 */

	const Speed MinSpeed = MilesPerHour(1) / 2;

	if(info.speed < MinSpeed)
		info.speed = MinSpeed;
#endif

#if !defined(CAMPEDIT) && !defined(BATEDIT)
	if(game->getDifficulty(Diff_Supply) == Simple)
		info.supply = MaxSupply;
	else
#endif
		info.supply 		= supply;

	info.experience	= experience;

	info.infantryStrength  = 0;
	info.cavalryStrength   = 0;
	info.artilleryStrength = 0;

	memset( info.strengths, 0, sizeof(info.strengths));

	info.strengths[type.basicType][type.subType] = strength;
	info.regimentCounts[type.basicType][type.subType]++;

	switch(type.basicType)
	{
	case Infantry:
		info.infantryStrength  = strength;
		break;
	case Cavalry:
		info.cavalryStrength   = strength;
		break;
	case Artillery:
		info.artilleryStrength = strength;
		break;
	}
}

/*
 * UnitInfo
 */

UnitInfo::UnitInfo(Boolean campDep, Boolean batDep)
{
	onlyCampaignDependant = campDep;
	onlyBattleDependant = batDep;
}

void UnitInfo::clear()
{
	// name 				= 0;
	strength				= 0;
	stragglers			= 0;
	casualties			= 0;
	fatigue				= Fresh;
	morale				= High;
	speed					= 0;

	infantryStrength  = 0;
	cavalryStrength   = 0;
	artilleryStrength = 0;

	supply = 0;

	experience			= Untried;

	memset(strengths, 0, sizeof(strengths));
	memset(regimentCounts, 0, sizeof(regimentCounts));

	regimentCount = 0;
}


/*
 * Merge Info (needs work).
 */

UnitInfo& UnitInfo::operator += (UnitInfo& i)
{
	strength += i.strength;
	stragglers += i.stragglers;
	casualties += i.casualties;
	fatigue = Fatigue((fatigue + i.fatigue) / 2);
	morale = Morale((morale + i.morale) / 2);
	if(i.speed < speed)
		speed = i.speed;

	supply = Supply((supply + i.supply) / 2);

	experience = Experience((experience + i.experience) / 2);

	for(int l = 0; l < 3; l++)
	{
		for(int j = 0; j < Inf_TypeCount; j++)
		{
			strengths[l][j] += i.strengths[l][j];
		}
	}

	return *this;
}



/*
 * Naming Functions
 */

static char* getNameBuf()
{
	static int which = False;

	static char nameBuf[4][80];

	which = (which + 1) & 3;

	return nameBuf[which];
}


const char* Army::getName(Boolean full) const
{
	const char* name = getRealName();

	if(name)
		return name;
	else
	{
		char buf[80];
		int position = getOrgPosition() + 1;

		if(getSide() == SIDE_USA)
			sprintf(buf, "%s Army", romanNumber(position));
		else	// assume CSA
			sprintf(buf, "%d%s Army", position, makeNths(position));

		if(full && superior)
			sprintf(buf+strlen(buf), ", %s", superior->getName(False));

		char* nameBuf = getNameBuf();
		strcpy(nameBuf, buf);

		return nameBuf;
	}
}

const char* Corps::getName(Boolean full) const
{
	/*
	 * USA:
	 *		"<Roman Number> Corps", e.g. "II Corps"
	 * CSA:									  
	 *    "<Spelt Number> Corps", e.g. "Second Corps"
	 */

	char buf[80];
	int position = getOrgPosition() + 1;

#if 0
	if(getSide() == SIDE_USA)
		sprintf(buf, "%s Corps", romanNumber(position));
	else	// assume CSA
		sprintf(buf, "%d%s Corps", position, makeNths(position));
#else
	if(getSide() == SIDE_USA)
		sprintf(buf, "%s %s", romanNumber(position), language(lge_Corps));
	else	// assume CSA
		sprintf(buf, "%d%s %s", position, makeNths(position), language(lge_Corps));
#endif

	if(full && superior)
		sprintf(buf+strlen(buf), ", %s", superior->getName(False));

	char* nameBuf = getNameBuf();
	strcpy(nameBuf, buf);

	return nameBuf;
}

const char* Division::getName(Boolean full) const
{
	/*
	 * USA:
	 *		"<nth> Division, <Corps Name>", e.g. "2nd Division, III Corps"
	 * CSA:
	 *		"<General>'s Division", e.g. "Adrian's Division"
	 */

	char* nameBuf = getNameBuf();

	if(getSide() == SIDE_USA)
	{
		char buf[80];
		int position = getOrgPosition() + 1;
#if 0
		sprintf(buf, "%d%s Division", position, makeNths(position));
#else
		sprintf(buf, "%d%s %s", position, makeNths(position), language(lge_Division));
#endif

		if(full && superior)
			sprintf(buf+strlen(buf), ", %s", superior->getName(False));

		strcpy(nameBuf, buf);
	}
	else
	{
		const char* s;
		if(general)
			s = general->getName();
		else
			s = "Nobody";

#if 0
		sprintf(nameBuf, "%s's Division", s);
#else
		sprintf(nameBuf, language(lge_NamesDivision), s);
#endif
	}

	return nameBuf;
}

const char* Brigade::getName(Boolean full) const
{
	/*
	 * USA:
	 *		"<nth> Brigade, <Division Name>", e.g. "2nd Brigade, 3rd Division"
	 * CSA:
	 *		"<General>'s Brigade", e.g. "Adrian's Brigade"
	 */

	char buf[80];

	if(getSide() == SIDE_USA)
	{
		int position = getOrgPosition() + 1;
#if 0
		sprintf(buf, "%d%s Brigade", position, makeNths(position));
#else
		sprintf(buf, "%d%s %s", position, makeNths(position), language(lge_Brigade));
#endif
	}
	else
	{
		const char* s;
		if(general)
			s = general->getName();
		else
			s = "Nobody";

#if 0
		sprintf(buf, "%s's Brigade", s);
#else
		sprintf(buf, language(lge_NamesBrigade), s);
#endif
	}

	if(full && superior)
		sprintf(buf+strlen(buf), ", %s", superior->getName(False));

	char* nameBuf = getNameBuf();
	strcpy(nameBuf, buf);

	return nameBuf;
}


const char* Regiment::getTypeStr() const
{
	switch(type.basicType)
	{
	case Infantry:
		return language( infantryTypeNames[type.subType] );
	case Cavalry:
		return language( cavalryTypeNames[type.subType] );
	case Artillery:
		return language( artilleryTypeNames[type.subType] );
#ifdef DEBUG
	default:
		return "Illegal regiment type";
#endif
	}
	return 0;
}

const char* Regiment::getName(Boolean full) const
{
	if(full)
	{
		char* nameBuf = getNameBuf();
		sprintf(nameBuf, "%s %s %s", getRealName(), getTypeStr(), language(lge_Regiment));
		return nameBuf;
	}
	else
		return getRealName();
}

const char* Unit::getGeneralName() const
{
	if(general)
		return general->getName();
	else
		return getName(True);
}

#ifdef DEBUG
/*
 * Get full name... going all way up to top commander
 *
 * e.g. "CSA 1st Brigade, 2nd Division, 3rd Corps, Army of Texas"
 */

const char* Unit::getFullName() const
{
	const int nNames = 4;
	static char fullNames[nNames][120];
	static int nextName = 0;

	// Get a buffer

	char* buf = fullNames[nextName];
	nextName++;
	if(nextName >= nNames)
		nextName = 0;

	// Put in the side

	strcpy(buf, "\"");
	strcat(buf, sideStr[getSide()]);

	const Unit* u = this;
	Boolean first = True;

	while(u && (u->getRank() >= Rank_Army))
	{
		if(!first)
			strcat(buf, ",");
		else
			first = False;

		strcat(buf, " ");	
		strcat(buf, u->getName(False));
		u = u->superior;
	}
	strcat(buf, "\"");

	return buf;
}
#endif

/*
 * Find regiment for another to merge with
 *
 * Finds smallest regiment in same organisation
 */

Regiment* Regiment::findBestMergeable(Unit* topMan, Regiment* us)
{
 	Regiment* best = 0;
	Unit* c1 = topMan->child;

	while( c1 )
	{
		Regiment* reg2;

	 	if(c1->rank == Rank_Regiment)
			reg2 = (Regiment*) c1;
		else
			reg2 = findBestMergeable(c1, us);

		if(reg2 && 
		  (reg2 != us) &&
		  (us->type.basicType == reg2->type.basicType) &&
		  reg2->strength && 
		  ((reg2->strength + us->strength + reg2->stragglers + us->stragglers) < MaxMenPerRegiment) &&
		  (!best || ((reg2->strength + reg2->stragglers) < (best->strength + best->stragglers))))
		{
			best = reg2;
		}

		c1 = c1->sister;
	}

	return best;
}

Experience updateExperience(Experience old, int n)
{
#if 0
	if(old < 50)		// This is from when tried was 50 instead of 1
		return 50;
	else
#endif
	{
		int newExperience = old + n;
		if(newExperience < MaxAttribute)
			return newExperience;
		else
			return MaxAttribute;
	}
}

void Regiment::incExperience(int n)
{
	experience = updateExperience(experience, n);
}

void Unit::incExperience(int n)
{
	if(general)
		general->experience = updateExperience(general->experience, n);
	Unit* u = child;
	while(u)
	{
		u->incExperience(n);
		u = u->sister;
	}
}


#if defined(DEBUG) && !defined(TESTCAMP) && !defined(CAMPEDIT)

General* GeneralGenerator::make(Side side, OrderBattle* ob)
{
	char buffer[40];
	sprintf(buffer, "%s General %d",
		(side==SIDE_CSA) ? "CSA" : "USA",
		++howMany);

	General* g = new General(buffer, side);

	g->efficiency = game->gameRand(MaxAttribute);
	g->ability    = game->gameRand(MaxAttribute);
	g->aggression  = game->gameRand(MaxAttribute);
	g->experience = game->gameRand(MaxAttribute);

	// ob->addGeneral(g);

	return g;
}


Regiment* RegimentGenerator::make(const Location& l, char* s)
{
	howMany++;

	/*
	 * Make up a random type (bias towards infantry, etc)
	 */

	static BasicRegimentType biasTable[] = {
		Infantry,  Infantry,  Infantry,  Infantry,
		Infantry,  Infantry,  Cavalry,   Cavalry,
		Cavalry,   Cavalry,   Cavalry,   Artillery,
		Artillery, Artillery, Artillery, Artillery
	};

	static UBYTE campTypes[3] = {
		Inf_TypeCount,
		Cav_TypeCount,
		Art_TypeCount
	};

	static UBYTE battleTypes[3] = {
		3,		// Don't allow engineers on the battlefield!
		4,		// No raiders
		3		// No siege artillery
	};

	RegimentType type;
	UnitInfo info(False, False);

	UBYTE* table = battleField ? battleTypes : campTypes;

	type.basicType = biasTable[game->gameRand(16)];
	switch(type.basicType)
	{
	case Infantry:
		type.subType = RegimentSubType(game->gameRand(table[0]));
		info.strength 		= game->gameRand(500, 1000);
		break;
	case Cavalry:
		type.subType = RegimentSubType(game->gameRand(table[1]));
		info.strength 		= game->gameRand(100, 500);
		break;
	case Artillery:
		type.subType = RegimentSubType(game->gameRand(table[2]));
		info.strength 		= game->gameRand(4,6) * MenPerGun;
		break;
	}

	if(!s)
	{
		s = new char[40];
		sprintf(s, "%d%s Misctown", howMany, makeNths(howMany));
	}

	Regiment* r = new Regiment(l, s, type);
	delete[] s;

	/*
	 * Set up random values for attributes
	 */

	info.stragglers	= game->gameRand(info.strength);
	info.casualties	= game->gameRand(info.strength-info.stragglers);
	info.fatigue		= Fatigue(game->gameRand(MaxAttribute));
	info.morale			= Morale(game->gameRand(MaxAttribute));

	info.supply = Supply(game->gameRand(MaxSupply));

	info.experience	= Experience(game->gameRand(MaxAttribute));

	r->setInfo(info);

	return r;
}

#endif	// DEBUG

