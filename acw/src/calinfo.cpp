/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	CAL Info Area
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "calmain.h"
#include "system.h"
#include "screen.h"
#include "text.h"
#include "game.h"
#include "sprlib.h"
#include "generals.h"
#include "tables.h"

#if !defined(TESTCAMP) && !defined(CAMPEDIT)
#include "unit3d.h"
#endif

#include "barchart.h"


/*
 * Constants
 */

/*
 * Support Functions
 */

void putLittleIcon(Region* bm, const Point& p, GameSprites icon)
{
	game->sprites->drawSprite(bm, p, icon);
}

/*
 * Constructor
 */

CAL_InfoWindow::CAL_InfoWindow(CAL* c) :
	Icon(c, Rect(CAL_INFO_X, CAL_INFO_Y, CAL_INFO_W, CAL_INFO_H)),
	CALBase(c)
{
}

void CAL_InfoWindow::drawIcon()
{
	clear();
}

void CAL_InfoWindow::clear()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	bm.fill(cal->fillPattern);
	machine.screen->setUpdate(bm);
}


void CAL_InfoWindow::showText(const char* s)
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	bm.fill(cal->fillPattern);

	TextWindow window(&bm, Font_EMFL10);
	window.setFormat(TW_CH | TW_CV);
	window.draw(s);

	machine.screen->setUpdate(bm);
}


void CAL_InfoWindow::showInfo(Unit* p)
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	bm.fill(cal->fillPattern);

	Region bm1 = bm;
	TextWindow window(&bm1, Font_JAME08);

	/*
	 * Display Unit Name
	 */

	bm1.setClip(bm.getX(), bm.getY(), 315, 10);
	window.setFormat(TW_CH | TW_CV);
	window.draw(p->getName(True));

	/*
	 * Display Current Order
	 */

	bm1.setClip(bm.getX() + 315, bm.getY(), getW() - 315, 10);
	window.setFormat(TW_CV);
	window.setPosition(Point(0,0));
	if(p->superior)
	{
#if !defined(CAMPEDIT) && !defined(TESTCAMP)
		if(p->battleInfo)
			window.wprintf("%s: %s", language(lge_CurrentOrders), getOrderStr(p->battleInfo->battleOrder));
		else
#endif	// CAMPEDIT
			// window.wprintf("%s: %s", language(lge_CurrentOrders), getOrderStr(p->realOrders));
			window.wprintf("%s: %s", language(lge_CurrentOrders), getOrderStr(p->givenOrders));
	}
	else
		window.draw(language(lge_unattached));


	/*
	 * Display General
	 */


	if(p->general)
		showGeneralInfo(p->general);

	/*
	 * Display Attributes
	 */

	// UnitInfo info(False, False);
	UnitInfo info(True, True);
	p->getInfo(info);

	// Strength/Casualties/Stragglers

	showStrengthBar(&bm, &info, Rect(11,10,  302,8));
	showBar(&bm, info.morale,  	 MaxAttribute, Rect( 11,19,145,8), moraleStr(info.morale));
	showBar(&bm, MaxAttribute - info.fatigue, 	 MaxAttribute, Rect(168,19,145,8), fatigueStr(info.fatigue));
	showBar(&bm, info.supply,  	 MaxAttribute, Rect( 11,28,145,8), supplyStr(info.supply));
	showBar(&bm, info.experience,  MaxAttribute, Rect(168,28,145,8), experienceStr(info.experience));
	
	putLittleIcon(&bm, Point(  1, 9), AT_Strength);
	putLittleIcon(&bm, Point(  1,18), AT_Morale);
	putLittleIcon(&bm, Point(158,18), AT_Fatigue);
	putLittleIcon(&bm, Point(  1,27), AT_Supply);
	putLittleIcon(&bm, Point(158,27), AT_Experience);

	machine.screen->setUpdate(bm);

}

void CAL_InfoWindow::showGeneralInfo(General* g)
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));

	bm.HLine(315, 9, getW() - 315, Black);
	bm.VLine(315, 9, getH() - 9, Black);

	Region bm1 = bm;
	TextWindow window(&bm1, Font_JAME08);

	bm1.setClip(bm.getX() + 315, bm.getY() + 10, 315, 10);

	window.setFormat(TW_CH | TW_CV);
	window.wprintf("%s %s", language(lge_General), g->getName());

	showBar(&bm, g->efficiency,  	MaxAttribute, Rect(327,20,145,8), efficiencyStr(g->efficiency));
	showBar(&bm, g->ability,  		MaxAttribute, Rect(494,20,145,8), abilityStr(g->ability));
	showBar(&bm, g->aggression,  	MaxAttribute, Rect(327,29,145,8), aggressionStr(g->aggression));
	showBar(&bm, g->experience,  	MaxAttribute, Rect(494,29,145,8), experienceStr(g->experience));

	putLittleIcon(&bm, Point(317,19), AT_Efficiency);
	putLittleIcon(&bm, Point(484,19), AT_Ability);
	putLittleIcon(&bm, Point(317,28), AT_Aggression);
	putLittleIcon(&bm, Point(484,28), AT_Experience);
}

void CAL_InfoWindow::showInfo(General* g)
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	bm.fill(cal->fillPattern);

	showGeneralInfo(g);

	machine.screen->setUpdate(bm);
	
}

