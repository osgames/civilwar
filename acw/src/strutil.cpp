/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	String Utilities
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/03/10  14:27:18  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <string.h>
#include "strutil.h"

/*
 * Copy a string using the new operator for memory allocation
 * [C's strdup() uses malloc()!]
 */

char* copyString(const char* s)
{
	char* s1 = new char[strlen(s)+1];

	strcpy(s1, s);

	return s1;
}


