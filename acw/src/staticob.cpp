/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Stationary objects on battle map
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "staticob.h"
#include "batldata.h"
#include "sprlib.h"
#include "sprlist.h"
#include "map3d.h"
#ifdef BATEDIT
#include "game.h"
#endif

/*
 * Types of sprites
 */

struct SpriteType {
	BattleSprite baseSprite;
	UBYTE offsets[5][4];
};


/*
 * This table must match the StaticSpriteType enumeration in staticob.h
 */

SpriteType spriteTypes[] = {
	{	// Church
		ChurchSprite,
		{
		  { 3,5,7,1 },
		  { 11,13,15,9 },
		  { 19,21,23,17 },
		  { 27,29,31,25 },
		  { 35,37,39,33 }
		}
	},
	{	// Church 45
		ChurchSprite,
		{
		  { 4,6,0,2 },
		  { 12,14,8,10 },
		  { 20,22,16,18 },
		  { 28,30,24,26 },
		  { 36,38,32,34 }
		}
	},
	{	// House
		HouseSprite,
		{
		  { 3,5,7,1 },
		  { 11,13,15,9 },
		  { 19,21,23,17 },
		  { 27,29,31,25 },
		  { 35,37,39,33 }
		}
	},
	{	// House	 45
		HouseSprite,
		{
		  { 4,6,0,2 },
		  { 12,14,8,10 },
		  { 20,22,16,18 },
		  { 28,30,24,26 },
		  { 36,38,32,34 }
		}
	},
	{	// Tavern
		TavernSprite,
		{
		  { 3,5,7,1 },
		  { 11,13,15,9 },
		  { 19,21,23,17 },
		  { 27,29,31,25 },
		  { 35,37,39,33 }
		}
	},
	{	// Tavern 45
		TavernSprite,
		{
		  { 4,6,0,2 },
		  { 12,14,8,10 },
		  { 20,22,16,18 },
		  { 28,30,24,26 },
		  { 36,38,32,34 }
		}
	},
	{	// Church1
		ChurchSprite1,
		{
		  { 3,5,7,1 },
		  { 11,13,15,9 },
		  { 19,21,23,17 },
		  { 27,29,31,25 },
		  { 0xff,0xff,0xff,0xff }
		}
	},
	{	// Church 451
		ChurchSprite1,
		{
		  { 4,6,0,2 },
		  { 12,14,8,10 },
		  { 20,22,16,18 },
		  { 28,30,24,26 },
		  { 0xff,0xff,0xff,0xff }
		}
	},
	{	// House1
		HouseSprite1,
		{
		  { 3,5,7,1 },
		  { 11,13,15,9 },
		  { 19,21,23,17 },
		  { 27,29,31,25 },
		  { 35,37,39,33 }
		}
	},
	{	// House	 451
		HouseSprite1,
		{
		  { 4,6,0,2 },
		  { 12,14,8,10 },
		  { 20,22,16,18 },
		  { 28,30,24,26 },
		  { 36,38,32,34 }
		}
	},
	{	// Tavern1
		TavernSprite1,
		{
		  { 3,5,7,1 },
		  { 11,13,15,9 },
		  { 19,21,23,17 },
		  { 27,29,31,25 },
		  { 35,37,39,33 }
		}
	},
	{	// Tavern 451
		TavernSprite1,
		{
		  { 4,6,0,2 },
		  { 12,14,8,10 },
		  { 20,22,16,18 },
		  { 28,30,24,26 },
		  { 36,38,32,34 }
		}
	},
	{	// Tree1
		Tree1,
		{ { 0,0,0,0 }, { 1,1,1,1 }, { 2,2,2,2 }, { 3,3,3,3 }, { 4,4,4,4 } }
	},
	{	// Tree2
		Tree2,
		{ { 0,0,0,0 }, { 1,1,1,1 }, { 2,2,2,2 }, { 3,3,3,3 }, { 4,4,4,4 } }
	},
	{	// Tree3
		Tree3,
		{ { 0,0,0,0 }, { 1,1,1,1 }, { 2,2,2,2 }, { 3,3,3,3 }, { 4,4,4,4 } }
	},
	{	// Tree4
		TreeDead1,
	 	{ { 0,0,0,0 }, { 1,1,1,1 }, { 2,2,2,2 }, { 3,3,3,3 }, { 4,4,4,4 } }
	},
	{	// Tree5
		TreeDead2,
		{ { 0,0,0,0 }, { 1,1,1,1 }, { 2,2,2,2 }, { 3,3,3,3 }, { 4,4,4,4 } }
	},
	{	// Tree6
		TreeDead3,
		{ { 0,0,0,0 }, { 1,1,1,1 }, { 2,2,2,2 }, { 3,3,3,3 }, { 4,4,4,4 } }
	},
	{	// Dead Horse
		deadHorse,
		{ { 0,2,4,6 }, { 8,10,12,14 }, { 16,18,20,22 }, { 0xff, 0xff, 0xff, 0xff }, { 0xff, 0xff, 0xff, 0xff } }
	},
	{	// Dead Artillery
		deadGun,
		{ { 0,2,4,6 }, { 8,10,12,14 }, { 16,18,20,22 }, { 0xff, 0xff, 0xff, 0xff }, { 0xff, 0xff, 0xff, 0xff } }
	},
	{	// Dead Union
		deadUnion,
		{ { 0,2,4,6 }, { 8,10,12,14 }, { 16,18,20,22 }, { 0xff, 0xff, 0xff, 0xff }, { 0xff, 0xff, 0xff, 0xff } }
	},
	{	// Dead confederate
		deadConfed,
		{ { 0,2,4,6 }, { 8,10,12,14 }, { 16,18,20,22 }, { 0xff, 0xff, 0xff, 0xff }, { 0xff, 0xff, 0xff, 0xff } }
	},
	{	// Tent
		TentSprite,
		{ { 0,0,0,0 }, { 1,1,1,1 }, { 2,2,2,2 }, { 3,3,3,3 }, { 0xff,0xff,0xff,0xff } }
	},
};

/*
 * Object List
 */


StaticObjectList::StaticObjectList()
{
	entry = 0;
}

StaticObjectList::~StaticObjectList()
{
	BattleObject* ob = entry;
	while(ob)
	{
		BattleObject* next = ob->next;

		delete ob;

		ob = next;
	}
}

void StaticObjectList::add(BattleObject* ob)
{
	ob->next = entry;
	entry = ob;
}

#ifdef BATEDIT
void StaticObjectList::remove(BattleObject* ob)
{
	BattleObject* last = 0;
	BattleObject* next = entry;

	while(next)
	{
		if(next == ob)
		{
			if(last)
				last->next = ob->next;
			else
				entry = ob->next;
			ob->next = 0;
			return;
		}

		last = next;
		next = next->next;
	}

	throw GeneralError("BattleObject not in list");
}
#endif

void StaticObjectList::draw(const ViewPoint& view)
{
	BattleObject* ob = entry;

	while(ob)
	{
		if( (ob->where.x >= view.minX) &&
			 (ob->where.x < view.maxX) &&
			 (ob->where.z >= view.minZ) &&
			 (ob->where.z < view.maxZ) )
		{
			ob->draw(view);
		}
		else
			ob->reset();

		ob = ob->next;
	}
}

void StaticObjectList::reset()
{
	BattleObject* ob = entry;

	while(ob)
	{
 		ob->reset();

		ob = ob->next;
	}
}

void StaticObjectList::clear()
{
	BattleObject* ob = entry;
	entry = 0;
	while(ob)
	{
		BattleObject* next = ob->next;

		delete ob;
		ob = next;
	}
}

/*
 * StaticObjectData functions
 */

StaticObjectData::StaticObjectData()
{
}

StaticObjectData::~StaticObjectData()
{
}

void StaticObjectData::add(BattleObject* ob)
{
	/*
	 * Determine which grid it is
	 */

	const Cord3D gridSize = BattleMile(8) / ObjectGridSize;
	const Cord3D gridCentre = BattleMile(4);

	int x = (ob->where.x + gridCentre) / gridSize;
	int y = (ob->where.z + gridCentre) / gridSize;

	if( (x >= 0) && (x < ObjectGridSize) && (y >= 0) && (y < ObjectGridSize))
		grids[x][y].add(ob);

}

#ifdef BATEDIT
void StaticObjectData::remove(BattleObject* ob)
{
	const Cord3D gridSize = BattleMile(8) / ObjectGridSize;
	const Cord3D gridCentre = BattleMile(4);

	int x = (ob->where.x + gridCentre) / gridSize;
	int y = (ob->where.z + gridCentre) / gridSize;

	if( (x >= 0) && (x < ObjectGridSize) && (y >= 0) && (y < ObjectGridSize))
		grids[x][y].remove(ob);
}
#endif

void StaticObjectData::draw(const ViewPoint& view)
{
	for(int y = 0; y < ObjectGridSize; y++)
		for(int x = 0; x < ObjectGridSize; x++)
			grids[x][y].draw(view);
}

void StaticObjectData::reset()
{
	for(int y = 0; y < ObjectGridSize; y++)
		for(int x = 0; x < ObjectGridSize; x++)
			grids[x][y].reset();
}

void StaticObjectData::clear()
{
	for(int y = 0; y < ObjectGridSize; y++)
		for(int x = 0; x < ObjectGridSize; x++)
			grids[x][y].clear();
}

/*===================================================
 * Iterator
 */

StaticObjectIter::StaticObjectIter(StaticObjectData* obData)
{
	owner = obData;
	reset();
}

void StaticObjectIter::reset()
{
	x = 0;
	y = 0;
	ob = owner->grids[x][y].entry;

	if(!ob)
	{
		do
		{
			x++;
			if(x >= ObjectGridSize)
			{
				x = 0;
				y++;
				if(y >= ObjectGridSize)
					return;
			}

			ob = owner->grids[x][y].entry;

		} while(!ob);
	}
}

BattleObject* StaticObjectIter::next()
{
	BattleObject* retVal = ob;

	if(ob)
	{
		ob = ob->next;

		while(!ob)
		{
			x++;
			if(x >= ObjectGridSize)
			{
				x = 0;
				y++;
				if(y >= ObjectGridSize)
					goto getOut;
			}

			ob = owner->grids[x][y].entry;
		}
	}

getOut:
	return retVal;
}


/*
 * Tree Sprite Object
 */

TreeObject::TreeObject(StaticSpriteType n, const BattleLocation& p, Qangle q) :
	BattleObject(p)
{
	what = n;
	facing = q;
}

TreeObject::~TreeObject()
{
	reset();
}

void TreeObject::reset()
{
	if(id.element)
		id.remove();

	sprite.release();
}

void TreeObject::draw(const ViewPoint& view)
{
	SpriteType* stype = &spriteTypes[what];
	Qangle dir = (facing - wangleToQangle(view.yRotate)) & 3;
	UBYTE offset = stype->offsets[view.zoomLevel][dir];

	reset();

	if(offset != 0xff)
	{
		SpriteIndex snum = stype->baseSprite + offset;

		sprite.spriteBlock = battle->spriteLib->read(snum);

		Point3D p;

		p.x = where.x;
		p.z = where.z;
		p.y = battle->grid->getHeightView(view, p.x, p.z);

		view.transform(p);

		p.x -= sprite.spriteBlock->anchorX;
		p.y -= sprite.spriteBlock->anchorY;

#ifdef BATEDIT
		if(highlight)
		{
			animation = (animation + 1) & 1;

			p.y -= animation * 4;
		}
#endif

#ifdef USE_SPRITEID
		battle->spriteList->add(&sprite, p, 0);
#else
		id.element = battle->spriteList->add(&sprite, p);
#endif
	}
}


#ifdef BATEDIT

void StaticObjectData::makeRandom()
{
#if 0
	/*
	 * Make some random trees
	 */

	int count = 2000;
	while(count--)
	{
		BattleLocation ar = BattleLocation(game->gameRand(BattleMile(8)) - BattleMile(4), game->gameRand(BattleMile(8)) - BattleMile(4));
		add(new TreeObject(StaticSpriteType(Tree1Object + game->gameRand(6)), ar, game->gameRand(4)));
	}
#endif

	/*
	 * Make some random buildings
	 */

	int count = 100;
	while(count--)
	{
		BattleLocation ar = BattleLocation(game->gameRand(BattleMile(8)) - BattleMile(4), game->gameRand(BattleMile(8)) - BattleMile(4));
		add(new TreeObject(StaticSpriteType(ChurchObject + game->gameRand(12)), ar, game->gameRand(4)));
	}
}

#endif

void BattleObject::getSaveData(ObjectSaveData& data)
{
	data.type = SO_None;
	data.where = where;
}

void TreeObject::getSaveData(ObjectSaveData& data)
{
	BattleObject::getSaveData(data);
	data.type = SO_Tree;
	data.what = what;
	data.facing = facing;
}

