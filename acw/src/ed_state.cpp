/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Editor - State Editting
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>

#include "camped.h"
#include "campwld.h"
#include "mapwind.h"
#include "text.h"
#include "colours.h"
#include "system.h"
#include "screen.h"
#include "mouselib.h"
#include "tables.h"

void State::showInfo(Region* r)
{
	control->setupInfo();

	TextWindow& textWind = *control->textWin;

	textWind.setFont(Font_Title);
	textWind.setColours(Black);

	textWind.setHCentre(True);
	textWind.setPosition(Point(4,4));
	textWind.wprintf("States\r");
	textWind.moveDown(4);				// Move down 4 pixels

	textWind.setFont(Font_Heading);
	textWind.draw("Full Name");
	textWind.newLine();

	textWind.setFont(Font_Info);
	textWind.draw(fullName);
	textWind.newLine();
	textWind.moveDown(4);				// Move down 4 pixels

	if(shortName[0])
	{
		textWind.setFont(Font_Heading);
		textWind.draw("Short Name");
		textWind.newLine();

		textWind.setFont(Font_Info);
		textWind.draw(shortName);
		textWind.newLine();
		textWind.moveDown(4);				// Move down 4 pixels
	}

	textWind.setHCentre(False);
	textWind.setLeftMargin(4);

	textWind.setFont(Font_Heading);
	textWind.draw("Side:");
	textWind.setFont(Font_Info);
	textWind.draw(sideStr[side]);
	textWind.newLine();
}

State* CampaignWorld::makeNewState()
{
	stateChanged = True;

	State* state = states.add();

	strcpy(state->fullName, "New State");

	return state;
}

void CampaignWorld::deleteState(State* state)
{
	StateID stateID = states.getID(state);

	if(stateID != NoState)
	{
		stateChanged= True;

		campaign->mapArea->onScreenMoveable.clear();
		campaign->mapArea->onScreenStatic.clear();
		states.removeID(stateID);

		/*
		 * Go through facility list and remove references to this state
		 */

		FacilityIter list = facilities;

		while(++list)
		{
			Facility* f = list.current();

			if(f->state == stateID)
			{
				control->setCloseState(f, False);
			}
			else if(f->state > stateID)
			{
				f->state--;
			 	facilityChanged = True;
			}
		}
	}
#ifdef DEBUG
	else
		throw GeneralError("State %s is not in state list", state->fullName);
#endif
}

/*
 * Go through facility list and update state if closer
 */

void CampaignEditControl::makeCityStates(State* state, int noPrompt)
{
	StateID stateID = world->states.getID(state);

	FacilityIter list = world->facilities;

	while(++list)
	{
		Facility* f = list.current();

		StateID best = NoState;

		/*
		 * Set it to the nearest state
		 */

		if(world->states.entries())
		{
			Distance dist = 0;

			StateListIter slist = world->states;
			StateID id = 0;
			while(++slist)
			{
				State* st = &slist.current();

				Distance d = distance(f->location.x - st->location.x,
										 	f->location.y - st->location.y);

				if((best == NoState) || (d < dist))
				{
					best = id;
					dist = d;
				}

				id++;
			}
		}

		if(f->state != best)
			facilityChanged = True;

		if(f->state != best)
		{
			if( (best == stateID) || (f->state == stateID) )
			{
				Boolean doChange = True;

				if(!noPrompt)
				{
					char buffer[200];

					sprintf(buffer, "Move %s\rfrom %s\rto %s",
						f->getName(),
						world->states[f->state].fullName,
						world->states[best].fullName);
					switch(dialAlert(0, buffer, "Move|Leave|Move All|Stop"))
					{
					case 0:	// Move
						doChange = True;
						break;
					case 1:
						doChange = False;
						break;
					case 2:
						doChange = True;
						noPrompt = True;
						break;
					case 3:
						return;
					}

				}

				if(doChange)
				{
					f->state = best;
					f->side = world->states[best].side;
					facilityChanged = True;
				}
			}
		}
	}
}


State* CampaignEditControl::editState(State* oldState)
{
	State state;

	state = *oldState;

	/*
	 * User dialogue to edit state!
	 */

	Boolean finished = False;
	Boolean cancelled = False;
	do
	{
		enum StateChoice {
			State_None,
			State_FullName,
			State_ShortName,
			State_Side,
			State_NextReg,
			State_Accept,
			State_Cancel
		};

		static MenuChoice choices[] = {
			MenuChoice("Edit State",0 						),	// "Edit State"
			MenuChoice("-",			0 						),	// "----------"
			MenuChoice("Full Name", 0 						),	// "Full Name"
			MenuChoice(0,				State_FullName 	),	// "New Jersey"
			MenuChoice("-",			0 						),	// "----------"
			MenuChoice("Short Name",0 						),	// "Short Name"
			MenuChoice(0,				State_ShortName	),	// "N J"
			MenuChoice("-",			0 						),	// "----------"
			MenuChoice(0,				State_Side 			),	// "Side: USA"
			MenuChoice(0,				State_NextReg		),	// "Regiment: 1"
			MenuChoice("-",			0 				 		),	// "---------"
			MenuChoice("Cancel",    State_Cancel 		),	// "Cancel"
			MenuChoice("Accept",    State_Accept 		),	// "Accept"
			MenuChoice(0,0)
		};

		choices[3].text = state.fullName;
		choices[6].text = state.shortName;
		
		char sideBuffer[80];
		sprintf(sideBuffer, "Side: %s", sideStr[state.side]);
		choices[8].text = sideBuffer;

		char regBuffer[80];
		sprintf(regBuffer, "Regiment: %d", (int) state.regimentCount);
		choices[9].text = regBuffer;

		switch(menuSelect(0, choices, machine.mouse->getPosition()))
		{
		case State_FullName:
			{
				char buffer[MaxStateName];
				strcpy(buffer, state.fullName);
				if(dialInput(0, "Edit State's Full Name", "OK|Cancel", buffer, MaxStateName-1) == 0)
					strcpy(state.fullName, buffer);
			}
			break;

		case State_ShortName:
			{
				char buffer[MaxStateName];
				strcpy(buffer, state.shortName);
				if(dialInput(0, "Edit State's Abreviated Name", "OK|Cancel", buffer, MaxStateName-1) == 0)
					strcpy(state.shortName, buffer);
			}
			break;
		case State_Side:
			state.side = inputSide(state.side);
			break;

		case State_NextReg:
			{
				char buffer[4];
				sprintf(buffer, "%d", (int) state.regimentCount);
				if(dialInput(0, "Enter number of next mobilised regiment", "OK|Cancel", buffer, sizeof(buffer), True) == 0)
				{
					int n = atoi(buffer);

					if(n)
						state.regimentCount = atoi(buffer);
				}
			}

		case State_Accept:
			finished = True;
			cancelled = False;
			break;

		case State_Cancel:
			finished = True;
			cancelled = True;
			break;
		}
	} while(!finished);

	/*
	 * Copy changed data if user didn't cancel
	 */

	if(!cancelled)		// User didn't cancel
	{
		stateChanged = True;
		changed = True;

		*oldState = state;
	}

	return oldState;
}

void CampaignEditControl::stateOverMap		(MapWindow* map, Event* event, MenuData* d, const Location* l)
{
	if(trackMode == Tracking)
	{
		trackFor(map, event, OT_State);

		if(event->buttons)
		{
			DoWhat what;
			State* state = 0;

			if(currentObject)
			{
				state = (State*) currentObject;

				if(event->buttons & Mouse::LeftButton)
					what = getEditWhat(state->fullName);
				else
					what = DoEdit;
			}
			else
				what = DoNew;

			if(what == DoNew)
			{
				state = 0;
				loseObject();
				objectIsNew = True;
				trackMode = Moving;
				hint("Click at location for New State");
			}

			/*
		 	 * edit the data
		 	 */

			if(what == DoEdit)
				editState(state);

			/*
		 	 * Put into move mode
		 	 */

			if(what == DoMove)
			{
				trackMode = Moving;
				hint("Click on new location for %s", state->fullName);
			}

			if(what == DoDelete)
			{
				world->deleteState(state);
				loseObject();
			}
		}
	}
	else if(trackMode == Moving)
	{
		if(event->buttons)
		{
			hint(0);

		 	State* state= (State*)currentObject;

			if(event->buttons & Mouse::LeftButton)
			{	// Move it!

				if(objectIsNew)
					state = world->makeNewState();

				if(state)
				{
					state->location = *l;
					stateChanged = True;
					changed = True;

					/*
				 	 * Update close cities
				 	 */

					int updateHow = dialAlert(0, "Update cities?", "Prompt|All|None");
	
					if( (updateHow == 0) || (updateHow == 1))
						makeCityStates(state, updateHow);
				}

				trackMode = Tracking;

				if(objectIsNew)
					editState(state);

				objectIsNew = False;
			}
			else
			{
				trackMode = Tracking;
				objectIsNew = False;
			}
		}
	}
}

void CampaignEditControl::stateOptions		(const Point& p)
{
	static MenuChoice choose[] = {
		MenuChoice("State Options",						0),
		MenuChoice("-",										0),
		MenuChoice("Remove All States",					1),
		MenuChoice("Connect all cities to closest",	2),
		MenuChoice("-",										0),
		MenuChoice("Cancel",									-1),
		MenuChoice(0,											0),
	};

	switch(menuSelect(0, choose, machine.mouse->getPosition()))
	{
	case 1:	// Remove
		{
			while(world->states.entries())
				world->deleteState(&world->states[0]);
			break;
		}
	case 2:
		{
			for(int i = 0; i < world->facilities.entries(); i++)
				setCloseState(world->facilities[i], True);
			break;
		}
	}
}

void CampaignEditControl::stateOffMap		()
{
}


