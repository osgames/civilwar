/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	System Control
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.10  1994/06/09  23:32:59  Steven_Green
 * Changed Fonts
 *
 * Revision 1.8  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.1  1994/03/10  14:27:18  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdarg.h>
#include <stdio.h>
#include <signal.h>

#include "system.h"
#include "mouselib.h"

#ifdef DEBUG
#include "memlog.h"
#include "log.h"
#endif

#include "timer.h"
#include "text.h"

#include "screen.h"
#include "ilbm.h"
#include "colours.h"
#include "trap.h"

/*
 * Global Variable
 */

System machine;

/*
 * Functions
 */

void System::init()
{
#if !defined(DEBUG)
	/*
	 * Replace MSDOS critical error and Control-C interrupts
	 */

	installErrorHandlers();
	// signal(SIGBREAK, SIG_IGN);
	// signal(SIGINT, SIG_IGN);
#endif

#ifdef DEBUG
	memLog = new MemoryLog();
	// if(debugLevel)
	//	logFile = new LogStream("debug.log", debugLevel);
#endif

	timer = new Timer;

	fonts = new FontSet;

	screen = new Screen;
	mouseLibrary = new SpriteLibrary("art\\mice");
	systemLibrary = new SpriteLibrary("art\\system");
	mouse = new MouseLibrary(screen, mouseLibrary);
	screen->setMouse(mouse);

	/*
	 * Set up a default palette
	 */

	readILBM("art\\palette.lbm", screen->getImage(), screen->getDeferredPalette());
	screen->updatePalette(40);
	screen->setUpdate();
	screen->update();

	// sound.init();

	initialised = True;
}

void System::reset()
{
	initialised = False;

	delete mouse;
	mouse = 0;
	delete screen;
	screen = 0;
	delete mouseLibrary;
	mouseLibrary = 0;
	delete systemLibrary;
	systemLibrary = 0;

	delete fonts;
	fonts = 0;

	delete timer;
	timer = 0;

#ifdef DEBUG
	delete logFile;
	logFile = 0;
#endif
}
/*================================================================
 * Some miscellaneous functions that really should be elsewhere!
 */

#ifdef DEBUG
/*
 * Display the palette as 16 by 16 boxes of 4x4
 */

void drawPalette(Region* bm, const Point& where)
{
	int lineCount = 16;
	Colour col = 0;
	Point lineP = where;

	while(lineCount--)
	{
		int colCount = 16;

		Point p = lineP;

		while(colCount--)
		{
			bm->frame(p.getX(), p.getY(), 5, 5, 0);
			bm->box(p.getX()+1, p.getY()+1, 3, 3, col++);

			p.y += 4;
		}

		lineP.x += 4;
	}

}
#endif


void showFullScreen(const char* name)
{
	// BitMap full(*machine.screen);

	machine.screen->fadePalette(40);		// 1/2 second fade down time
	readILBM(name, machine.screen->getImage(), machine.screen->getDeferredPalette());
	machine.screen->setUpdate();
#ifdef DEBUG
	drawPalette(machine.screen->getBitmap(), Point(560, 140));
#endif

	machine.screen->updatePalette(20);		// quarter second fade up
}

#if 0
void drawSprite(Region* bm, Point where, IconNumbers i)
{
	machine.graphicsLibrary->drawSprite(bm, where, i);
}

void drawAnchoredSprite(Region* bm, Point where, IconNumbers i)
{
	machine.graphicsLibrary->drawAnchoredSprite(bm, where, i);
}

void drawCentredSprite(Region* bm, IconNumbers i)
{
	machine.graphicsLibrary->drawCentredSprite(bm, i);
}

#endif


void showString(const Point& p, const char* fmt, ...)
{
	char buffer[500];

	va_list vaList;
	va_start(vaList, fmt);
	vsprintf(buffer, fmt, vaList);
	va_end(vaList);

	TextWindow win(machine.screen->getBitmap(), Font_EMFL_8);
	win.setColours(Black);
	win.setFormat(False, False);
	win.setPosition(p);
	win.draw(buffer);

	machine.screen->setUpdate();		// Update whole screen!
}

#ifdef DEBUG

/*
 * A routine that is called when the B key is pressed
 * useful as a method of breaking into the program.
 */

void doBreak()
{
}

#endif

void System::blit(Image* image, const Point& dest, const Rect& src)
{
 	screen->blit(image, dest, src);
}

void System::blit(Image* image, const Point& dest)
{
	screen->blit(image, dest, Rect(0,0,0,0));
}


void System::unBlit(Image* image, const Point& where)
{
	screen->unBlit(image, where);
}

void System::maskBlit(Sprite* image, const Point& dest)
{
	screen->maskBlit(image, dest);
}
