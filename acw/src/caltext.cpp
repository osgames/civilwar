/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	CAL Info Window
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.11  1994/06/09  23:32:59  Steven_Green
 * Changed Fonts
 *
 * Revision 1.9  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.5  1994/04/22  22:14:22  Steven_Green
 * Icon Handling tidied up.
 *
 * Revision 1.4  1994/04/20  22:21:40  Steven_Green
 * Use TextWindow class
 *
 * Revision 1.1  1994/02/17  14:57:44  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include "calmain.h"
#include "system.h"
#include "screen.h"
#include "game.h"
#include "colours.h"
#include "text.h"

#define Font_InfoArea Font_EMFL_8

/*
 * Constructor
 */

CAL_InfoWindow::CAL_InfoWindow(CAL* c) :
	Icon(c, Rect(CAL_INFO_X, CAL_INFO_Y, CAL_INFO_W, CAL_INFO_H)),
	CALBase(c)
{
}

/*
 * Draw Function
 */

void CAL_InfoWindow::drawIcon()
{
	/*
	 * Draw a Frame
	 */

	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	bm.fill(CAL_FillColour);
	TextWindow window(&bm, Font_InfoArea);
	window.setFormat(True, True);

	bm.frame(Point(0,0), getSize(), CAL_Frame1Colour, CAL_Frame2Colour);

	/*
	 * Put in the text
	 */

	char* s = buf.str();			// Get the string (and freeze)

	if(s)
		window.draw(s);

	buf.rdbuf()->freeze(0);					// Unfreeze

	/*
	 * Put it onto the screen
	 */

	machine.screen->setUpdate(bm);
}

void CAL_InfoWindow::initText()
{
	// buf.rdbuf()->seekoff(0, ios::beg, ios::out);
	buf.seekp(0);
}

void CAL_InfoWindow::endText()
{
	buf.put('\0');
	setRedraw();
}

#ifdef OLD_TESTING
void CAL::message(const char* s)
{
	infoWindow->initText();
	infoWindow->buf << s;
	infoWindow->endText();
}
#endif


