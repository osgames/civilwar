/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	CAL (Current Army List)
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.16  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.13  1994/04/22  22:14:22  Steven_Green
 * Icon Handling tidied up.
 *
 * Revision 1.2  1994/02/10  14:43:26  Steven_Green
 * Main Display mostly working
 *
 * Revision 1.1  1994/02/09  14:59:26  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "cal.h"
#include "calmain.h"
#include "system.h"
#include "sprlib.h"
#include "game.h"
#include "ob.h"

/*
 * Constructor for CAL
 */

CAL::CAL(OrderBattle* o, Unit* startUnit, MenuData* proc, Boolean inCampaign) :
	MenuData(proc)
{
	ob = o;
	currentIcon = 0;
	campaignMode = inCampaign;

	tops[Rank_President] = 0;
	tops[Rank_Army] = 0;
	tops[Rank_Corps] = 0;
	tops[Rank_Division] = 0;
	tops[Rank_Brigade] = 0;


	if(startUnit)
	{
		top = ob->getPresident(startUnit->getSide());
		setCurrentUnit(startUnit);
	}
	else
	{
#if defined(CAMPEDIT) || defined(BATEDIT)
		top = ob->getPresident(SIDE_USA);
#else
		// top = ob->getPresident(game->playersSide);
		top = ob->getPresident(game->getLocalSide());
#endif
		setCurrentUnit(top);
	}

	SpriteIndex sNum;

	if(top->getSide() == SIDE_USA)
		sNum = FillUSA_Grey;
	else
		sNum = FillCSA_Grey;
	fillPattern = game->sprites->read(sNum);

	icons = new IconList[7];

	Icon** iconPtr = icons;

	*iconPtr++ = new CAL_MainDisplay(this);

#if !defined(TESTBATTLE)
	// if(inCampaign)
	{
#ifndef CAMPEDIT
		*iconPtr++ = new CAL_Regiments(this);
#endif
		*iconPtr++ = new CAL_Generals(this);
	}
#endif

	*iconPtr++ = infoWindow = new CAL_InfoWindow(this);

	*iconPtr++ = new CAL_OkIcon(this);
	*iconPtr++ = new QuitIcon(this);
	*iconPtr++ = 0;

	setPickMode();
	// pickMode = Picking;
	// dragPointer = M_Arrow;

	// pickedUnit = 0;
	// pickedGeneral = 0;

	/*
	 * Set global CAL
	 */

	cal = this;
}

CAL::~CAL()
{
	cal = 0;		// Remove global CAL.
	fillPattern->release();
}

void CAL::setPickMode()
{
	pickMode = Picking;
	setPointer(M_Arrow);
	dragPointer = M_Arrow;
	pickedUnit = 0;
	pickedGeneral = 0;

	currentIcon = 0;		// Force info to be updated
}


void CAL::setDropMode(PointerIndex icon)
{
	setPointer(icon);
	dragPointer = icon;
	pickMode = Dropping;
}

void CAL::showDropPointer()
{
	setPointer(PointerIndex(dragPointer + M_CAL_Army1Drop - M_CAL_Army1));
}

void CAL::setCurrentUnit(Unit* unit)
{
	currentUnit = unit;
	updateTops(currentUnit);
}

/*
 * Entry point to do the CAL screen
 */

Unit* doCampaignCAL(OrderBattle* ob, Unit* startUnit, int* giveOrder, MenuData* proc)
{
#if !defined(BATEDIT) && !defined(CAMPEDIT)
	// if( (game->playerCount == 1) && proc)
	if(!game->isRemoteActive() && proc)
		proc->pause();
#endif

	CAL cal(ob, startUnit, proc, True);

	while(!cal.isFinished())
	{
		/*
		 * Display/update screen if required
		 */


		if(cal.needUpdate())
		{
			showFullScreen("art\\screens\\cal_scrn.lbm");

			cal.redrawIcon();
			cal.clearUpdate();
		}

		cal.process();

	}
#if !defined(BATEDIT) && !defined(CAMPEDIT)
	// if( (game->playerCount == 1) && proc)
	if(!game->isRemoteActive() && proc)
		proc->unPause();
#endif

	if(giveOrder)
	{
		if(cal.getMode() == CAL_GiveOrder)
			*giveOrder = 1;
		else
			*giveOrder = 0;
	}

	if(cal.currentUnit && (cal.currentUnit->getRank() == Rank_Regiment))
		cal.currentUnit = cal.currentUnit->superior;

	if(cal.currentUnit && (cal.currentUnit->getRank() == Rank_President))
		return 0;
	else
		return cal.currentUnit;
}

Unit* doBattleCAL(OrderBattle* ob, Unit* startUnit, int* giveOrder, MenuData* proc)
{
#if !defined(BATEDIT) && !defined(CAMPEDIT)
	// if( (game->playerCount == 1) && proc)
	if(!game->isRemoteActive() && proc)
		proc->pause();
#endif

#ifdef BATEDIT
	CAL cal(ob, startUnit, proc, True);
#else
	CAL cal(ob, startUnit, proc, False);
#endif

	while(!cal.isFinished())
	{
		/*
		 * Display/update screen if required
		 */


		if(cal.needUpdate())
		{
			showFullScreen("art\\screens\\cal_scrn.lbm");

			cal.redrawIcon();
			cal.clearUpdate();
		}

		cal.process();

	}

	// if( (game->playerCount == 1) && proc)
#if !defined(BATEDIT) && !defined(CAMPEDIT)
	if(!game->isRemoteActive() && proc)
		proc->unPause();
#endif

	if(giveOrder)
	{
		if(cal.getMode() == CAL_GiveOrder)
			*giveOrder = 1;
		else
			*giveOrder = 0;
	}

	if(cal.currentUnit && (cal.currentUnit->getRank() == Rank_Regiment))
		cal.currentUnit = cal.currentUnit->superior;

	if(cal.currentUnit && (cal.currentUnit->getRank() == Rank_President))
		return 0;
	else
		return cal.currentUnit;
}
