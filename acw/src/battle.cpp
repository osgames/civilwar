/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battle Game Main Loop
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.20  1994/06/09  23:32:59  Steven_Green
 * Changed Fonts
 *
 * Revision 1.18  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.12  1994/04/22  22:14:22  Steven_Green
 * Icon Handling tidied up.
 *
 * Revision 1.1  1994/02/28  23:04:17  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <ctype.h>
#include "battle.h"
#include "batldata.h"
#include "ai_bat.h"
#include "menudata.h"
#include "gameicon.h"
#include "system.h"
#include "screen.h"
#include "clock.h"
#include "language.h"
#include "dialogue.h"
#include "connect.h"
#include "game.h"
#include "hintstat.h"
#include "batwind.h"
#include "msgwind.h"
#include "savegame.h"
#include "files.h"
#include "unit3d.h"
#include "memptr.h"
#include "campmusi.h"
#include "batsnd.h"
#include "timer.h"
#include "map3d.h"
#include "staticob.h"
#include "earth.h"
#include "campbatl.h"
#include "unititer.h"
#include "cal.h"
#include "calmulti.h"
#include "logo.h"
#include "unitinfo.h"

#ifdef DEBUG
#include "options.h"
#include "bat_opt.h"
#include "tables.h"
#endif

#ifdef DEBUG
LogFile bLog("battle.log");
#endif

/*
 * Comment this line out to get zoom mode to continue after zooming
 */

#define ZOOM_ONE_SHOT		// Zoom stops after zooming.

/*
 * Global access to battle data
 */

BattleData* battle = 0;

/*
 * Packet of info when orders given
 */

struct BattleOrderPacket {
	UnitID unit;
	OrderMode order;
	Location location;
	TimeBase time;
};

struct TickPacket {
	GameTicks ticks;
};

class BattleKeys : public NullIcon {
public:
	BattleKeys(IconSet* parent) : NullIcon(parent) { }
	void execute(Event* event, MenuData* data);
};

void BattleKeys::execute(Event* event, MenuData* data)
{
	Key key = event->key;
	if(isascii(key))
		key = toupper(key);

	switch(key)
	{
	case KEY_F1:			// Next stacked item
		battle->prevStack();
		event->key = 0;
		break;
	case KEY_F2:
		battle->nextStack();
		event->key = 0;
		break;

#ifdef DEBUG
	case KEY_AltS:
		seeAll = !seeAll;
		event->key = 0;
		break;
	case KEY_AltL:			// Alt-L, Toggle standing infantry to always show reload sprites
		showInfantryLoading = !showInfantryLoading;
		event->key = 0;
		break;
	case 'I':
		battle->showDisplayInfo = !battle->showDisplayInfo;
		event->key = 0;
		break;
	case '1':
		battle->zoomIn();
		event->key = 0;
		break;
	case '2':
		battle->zoomOut();
		event->key = 0;
		break;
	case 'G':
		globalOptions.toggle(Options::Gourad);
		event->key = 0;
		break;
	case 'T':
		globalOptions.toggle(Options::Texture);
		event->key = 0;
		break;
	case 'S':
		globalOptions.toggle(Options::Show);
		event->key = 0;
		break;
	case 'H':
		globalOptions.toggle(Options::HSL);
		event->key = 0;
		break;
	case 'B':
		doBreak();
		event->key = 0;
		break;
	case 'M':
		globalOptions.toggle(Options::QuickMove);
		event->key = 0;
		break;
#endif
	}
}

#if 0

class ShowInfoIcon : public NullIcon {
public:
	ShowInfoIcon(IconSet* parent) : NullIcon(parent, 'I') { }
	void execute(Event* event, MenuData* data)
	{
		if(event->buttons)
			battle->showDisplayInfo = !battle->showDisplayInfo;
	}
};

class DetailIcon : public NullIcon {
public:
	DetailIcon(IconSet* parent) : NullIcon(parent, '1', '2') { }

	void execute(Event* event, MenuData* data)
	{
		if(event->buttons & Mouse::LeftButton)
			battle->zoomIn();
		else if(event->buttons & Mouse::RightButton)
			battle->zoomOut();
	}
};

class GouradIcon : public NullIcon {
public:
	GouradIcon(IconSet* parent) : NullIcon(parent, 'G') { }
	void execute(Event* event, MenuData* data)
	{
		if(event->buttons)
		{
			globalOptions.toggle(Options::Gourad);
			data=data;
		}
	}
};

class TextureIcon : public NullIcon {
public:
	TextureIcon(IconSet* parent) : NullIcon(parent, 'T') { }
	void execute(Event* event, MenuData* data)
	{
		if(event->buttons)
		{
			globalOptions.toggle(Options::Texture);
			data=data;
		}
	}
};

class ShowIcon : public NullIcon {
public:
	ShowIcon(IconSet* parent) : NullIcon(parent, 'S') { }
	void execute(Event* event, MenuData* data)
	{
		if(event->buttons)
		{
			globalOptions.toggle(Options::Show);
			data=data;
		}
	}
};

class HSLIcon : public NullIcon {
public:
	HSLIcon(IconSet* parent) : NullIcon(parent, 'H') { }
	void execute(Event* event, MenuData* data)
	{
		if(event->buttons)
		{
			globalOptions.toggle(Options::HSL);
			data=data;
		}
	}
};


class BreakIcon : public NullIcon {
public:
	BreakIcon(IconSet* parent) : NullIcon(parent, 'B') { }
	void execute(Event* event, MenuData* data)
	{
		if(event->buttons)
			doBreak();
		data=data;
	}
};


class QuickMoveIcon : public NullIcon {
public:
	QuickMoveIcon(IconSet* parent) : NullIcon(parent, 'M') { }
	void execute(Event* event, MenuData* data)
	{
		if(event->buttons)
		{
			globalOptions.toggle(Options::QuickMove);
			data=data;
		}
	}
};

#endif


class BattleSaveIcon : public SaveIcon {
public:
	BattleSaveIcon(IconSet* set, Point p, Key k1 = 0, Key k2 = 0) :
		SaveIcon(set, p, k1, k2) { }

	// void saveGame(const char* name);
};


class BattleClockIcon : public ClockIcon {
	UBYTE timerRate;

	static const int MaxRate;

public:
	BattleClockIcon(IconSet* set, Point p) :
		ClockIcon(set, p, &battle->timeInfo, &battle->gameTime, True)
		{
			timerRate = 3;
			setRate();
		}

	void execute(Event* event, MenuData* data);
private:
	void setRate();
};

static const int BattleClockIcon::MaxRate = 10;

void BattleClockIcon::execute(Event* event, MenuData* data)
{
	if(gameTime)
	{
		if(event->buttons & Mouse::LeftButton)
		{
			if(timerRate < MaxRate)
				timerRate++;
		}
		else if(event->buttons & Mouse::RightButton)
		{
			if(timerRate > 0)
				timerRate--;
		}

		setRate();
	}

	if ( event->overIcon && gameTime )
	  data->showHint( language( lge_batClock ) );
}

void BattleClockIcon::setRate()
{
	static int rates[MaxRate+1] = {
		1,
		3,
		6,
		15,			// (default)
		30,
		45,
		60,
		90,
		120,
		180,
		240
	};

	gameTime->setRate(rates[timerRate]);
}

/*================================================================
 * End Battle Icon
 */

class EndBattleIcon : public MenuIcon {
public:
	EndBattleIcon(IconSet* parent, Point p);
	void execute(Event* event, MenuData* data);
};

EndBattleIcon::EndBattleIcon(IconSet* parent, Point p) :
	MenuIcon(parent, C2_EndBattle, Rect(p, MENU_ICON_SIZE))
{
}

void EndBattleIcon::execute(Event* event, MenuData* data)
{
	if(event->overIcon)
		data->showHint( language( lge_endBattle ) );

	if(event->buttons)
	{
		if(dialAlert(data, language(lge_ReallyEnd), language(lge_EndContinue)) == 0)
		{
			FinishPKT *packet = new FinishPKT;
			packet->how = BattleSurrender;

		  	game->connection->sendMessage(Connect_Finish, (UBYTE*)packet, sizeof(FinishPKT));
		}
	}
}

class BattleCALIcon : public MenuIcon {
public:
	BattleCALIcon(IconSet* parent, Point p) : MenuIcon(parent, C1_Promote, Rect(p, MENU_ICON_SIZE)) { }
	void execute(Event* event, MenuData* data);
};

void BattleCALIcon::execute(Event* event, MenuData* data)
{
	if ( event->overIcon ) 
		data->showHint( language( lge_promoteGen ) );

	if(event->buttons)
	{
		battle->currentUnit = 0;
		battle->runCAL();
	}
}


class BattleSM1 : public SubMenu {
public:
	BattleSM1(IconSet* parent);

	void drawIcon();
};

BattleSM1::BattleSM1(IconSet* parent) :
	SubMenu(parent, Rect(MENU_X, MENU_Y, MENU_WIDTH, MENU_HEIGHT))
{
	icons = new IconList[3];

	icons[0] = new EndBattleIcon(this, Point(MENU_ICON_X, MENU_ICON_Y8));
	icons[1] = new BattleCALIcon(this, Point(MENU_ICON_X, MENU_ICON_Y7));
	icons[2] = 0;
}

void BattleSM1::drawIcon()
{
	Region bitmap(machine.screen->getImage(), Rect(getPosition(), getSize()));

	if(battle->trackMode == BattleData::GetDestination)
	{
		putPlayerPattern(&bitmap);

		/*
		 * Display unit name at top
		 */

		Point p = getPosition();
		bitmap.setClip(p.getX(), p.getY(), getW(), 57);
		showUnitName(&bitmap, battle->currentUnit);

		// putLogo(&bitmap);
	}
	else
	{
		putPlayerPattern(&bitmap);
		battle->statsUpdate();
	}

	SubMenu::drawIcon();
}


/*
 * control Data for battle play screen
 */


class BattleControl : public MenuData, public MenuConnection, public HintStatus {
	// Boolean waitSave;
public:
	Boolean gotTicks;
	GameTicks rxTicks;
public:
	BattleControl();
	~BattleControl();

	void processPacket(Packet* pkt);
	void proc();

	void drawIcon();
	void showHint(const char* s) { HintStatus::showHint(s); shownHint = True; }
	void showStatus(const char* s) { HintStatus::showStatus(s); }
	void showHintStatus(const char* s);
#if !defined(COMPUSA_DEMO)
	void saveGame(const char* name);
#endif
};


BattleControl::BattleControl() :
	MenuConnection("Battle")
{

	battle->menuData = this;

	gotTicks = False;
	rxTicks = 0;

	icons = new IconList[10];

	Icon** iconPtr = icons;

	*iconPtr++ = new MainMenuIcon						 (this, Point(MAIN_ICON_X1,MAIN_ICON_Y));
	*iconPtr++ = battle->clockIcon = new BattleClockIcon(this, Point(MAIN_ICON_X2,MAIN_ICON_Y));
	*iconPtr++ = new SoundIcon							 (this, Point(MAIN_ICON_X3,MAIN_ICON_Y));
	*iconPtr++ = new BattleSaveIcon					 (this, Point(MAIN_ICON_X4,MAIN_ICON_Y));

	*iconPtr++ = new QuitIcon		(this);
	*iconPtr++ = battle->viewIcons = new BattleViewIcons(this);
	*iconPtr++ = battle->subMenu = new BattleSM1(this);
	*iconPtr++ = battle->mapWindow = new BattleMap(this);

	*iconPtr++ = new BattleKeys(this);
#if 0
	*iconPtr++ = new ShowInfoIcon(this);
	*iconPtr++ = new DetailIcon(this);
	*iconPtr++ = new GouradIcon(this);
	*iconPtr++ = new TextureIcon(this);
	*iconPtr++ = new ShowIcon(this);
	*iconPtr++ = new HSLIcon(this);
	*iconPtr++ = new BreakIcon(this);
	*iconPtr++ = new QuickMoveIcon(this);
#endif
	*iconPtr++ = 0;

	battle->message = new MessageWindow(machine.screen->getImage(), Rect(MSGW_X,MSGW_Y,MSGW_W,MSGW_H));
#ifdef DEBUG
	showStatus("Battle Game");
#endif
	processMessages(True);
}

BattleControl::~BattleControl()
{
	processMessages(True);
	delete battle->message;
	battle->message = 0;
}

void BattleControl::showHintStatus(const char* s)
{
	battle->message->draw(s);
}

#if !defined(COMPUSA_DEMO)
void BattleControl::saveGame(const char* name)
{
	saveBattleGame(name, battle);
}
#endif	// DEMO

void BattleControl::drawIcon()
{
	showFullScreen(playScreenFileName);
	showHintStatus(0);
	battle->data3D.terrain.setPalette();

	IconSet::drawIcon();
}

/*
 * Orders Sub Menu
 */

class BattleOrderIcon : public Icon {
	GameSprites iconNumber;
	OrderMode order;
	// BattleOrderFunction* control;
public:
	BattleOrderIcon(IconSet* parent, GameSprites n, Rect r, OrderMode ord) :
		Icon(parent, r),
		iconNumber(n),
		order(ord)
	{
	}

	void drawIcon();
	void execute(Event* event, MenuData* data);
};


void BattleOrderIcon::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));

	SpriteBlock* spriteImage = game->sprites->read(iconNumber);
	bm.blit(spriteImage, Point(0,0));
	spriteImage->release();

	/*
	 * Highlight current selected order
	 */

	if(order == battle->currentOrder)
	{
		bm.frame(Point(1,1), getSize() - Point(2,2), Colours(LYellow));
	}

	if(battle->currentUnit)
	{
		/*
		 * Highlight Units current Order
		 */

		if(order == battle->currentUnit->battleInfo->battleOrder.mode)
		{
			bm.frame(Point(3,3), getSize() - Point(6,6), Colours(LRed));
		}

		/*
		 * Highlight units last sent order
		 */
	}

	machine.screen->setUpdate(bm);
}

void BattleOrderIcon::execute(Event* event, MenuData* data)
{
	if( event->overIcon )
	{
		MemPtr<char> buttons(200);
		strcpy(buttons, language(LGE(lge_OrderAd + order)));
		if(battle->currentOrder == order)
			strcat(buttons, language(lge_latestO));

		data->showHint( buttons );

		// data->showHint( language( message ) );

	}

	if(event->buttons & Mouse::LeftButton)
	{
		battle->stopZoomMode();

		battle->currentOrder = order;

		parent->setRedraw();
	}
}


class B_MiniOK : public MenuIcon {
public:
	B_MiniOK(IconSet* parent, const Rect& r) :
		MenuIcon(parent, C2_MiniOK, r)
		// MenuIcon(parent, MM_MiniOK, r)
	{
	}
		
	void execute(Event* event, MenuData* data);
};

void B_MiniOK::execute(Event* event, MenuData* data)
{
	if(event->overIcon)
		data->showHint( language( lge_cancel ) );

	if(event->buttons)
	{
		battle->stopZoomMode();

		battle->OKOrder();
	}
}

void BattleData::OKOrder()
{
	if(currentOrder != currentUnit->battleInfo->battleOrder.mode)
		sendOrder(currentUnit->battleInfo->battleOrder.destination);
	else
		closeOrders();
}

void BattleData::sendOrder(const Location& location)
{
	if(currentUnit && currentUnit->battleInfo)
	{
 		BattleOrderPacket* pkt = new BattleOrderPacket;

 		pkt->order = currentOrder;
 		pkt->location = location;
 		pkt->time = gameTime;
 		pkt->unit = ob->getID(currentUnit);

 		game->connection->sendMessage(Connect_BS_Order, (UBYTE*)pkt, sizeof(BattleOrderPacket));

	}
	closeOrders();
}

void BattleData::closeOrders()
{
	subMenu->kill();
	trackMode = Tracking;
	menuData->setPointer(oldModePointer);
}

void BattleData::openOrders()
{
	if(trackMode == GetDestination)
		closeOrders();

	if(currentUnit)
	{
		currentOrder = currentUnit->battleInfo->battleOrder.mode;
		trackMode = GetDestination;

		IconList* icons = new IconList[11];

#if 0
		icons[0]  = new BattleOrderIcon(subMenu, C2_Advance,			Rect(  4,  0,113, 52), ORDER_Advance);
		icons[1]  = new BattleOrderIcon(subMenu, C2_Cautious,			Rect(  4, 52, 57, 53), ORDER_CautiousAdvance);
		icons[2]  = new BattleOrderIcon(subMenu, C2_Attack,			Rect( 61, 52, 57, 53), ORDER_Attack);
		icons[3]  = new BattleOrderIcon(subMenu, C2_Stand,				Rect(  4,105,113, 52), ORDER_Stand);
		icons[4]  = new BattleOrderIcon(subMenu, C2_Hold,				Rect(  4,157, 57, 53), ORDER_Hold);
		icons[5]  = new BattleOrderIcon(subMenu, C2_Defend,			Rect( 61,157, 57, 53), ORDER_Defend);
		icons[6]  = new BattleOrderIcon(subMenu, C2_Withdraw,			Rect(  4,210,113, 52), ORDER_Withdraw);
		icons[7]  = new BattleOrderIcon(subMenu, C2_Retreat,			Rect(  4,262, 57, 53), ORDER_Retreat);
		icons[8]  = new BattleOrderIcon(subMenu, C2_HastyRetreat,	Rect( 61,262, 57, 53), ORDER_HastyRetreat);
		icons[9]  = new B_MiniOK(subMenu,                           Rect(  4,315,113,105));
#else		// Move down to make room for unit name
		icons[0]  = new BattleOrderIcon(subMenu, C2_Advance,			Rect(  4, 57,113, 52), ORDER_Advance);
		icons[1]  = new BattleOrderIcon(subMenu, C2_Cautious,			Rect(  4,110, 57, 53), ORDER_CautiousAdvance);
		icons[2]  = new BattleOrderIcon(subMenu, C2_Attack,			Rect( 61,110, 57, 53), ORDER_Attack);
		icons[3]  = new BattleOrderIcon(subMenu, C2_Stand,				Rect(  4,162,113, 52), ORDER_Stand);
		icons[4]  = new BattleOrderIcon(subMenu, C2_Hold,				Rect(  4,215, 57, 53), ORDER_Hold);
		icons[5]  = new BattleOrderIcon(subMenu, C2_Defend,			Rect( 61,215, 57, 53), ORDER_Defend);
		icons[6]  = new BattleOrderIcon(subMenu, C2_Withdraw,			Rect(  4,267,113, 52), ORDER_Withdraw);
		icons[7]  = new BattleOrderIcon(subMenu, C2_Retreat,			Rect(  4,320, 57, 53), ORDER_Retreat);
		icons[8]  = new BattleOrderIcon(subMenu, C2_HastyRetreat,	Rect( 61,320, 57, 53), ORDER_HastyRetreat);
		icons[9]  = new B_MiniOK(subMenu,                           Rect(  4,373,113, 53));
#endif
		icons[10] = 0;

		subMenu->add(icons);

		oldModePointer = menuData->getPointer();
		menuData->setPointer(M_WhereTo);
	}
}

void BattleData::rxBattleOrder(UBYTE* packet)
{
	BattleOrderPacket* pkt = (BattleOrderPacket*) packet;

	Unit* u = ob->getUnitPtr(pkt->unit);
	u->battleInfo->sendBattleOrder(new SentBattleOrder(pkt->order, pkt->location, pkt->time));

#if defined(DEBUG)
	netLog.printf("rxBattleOrder %s ordered to %s",
		u->getName(True),
		getOrderStr(pkt->order));

#endif
}


/*
 * Global Battle Data
 */

BattleData::BattleData(OrderBattle* startOB)
{
	/*
	 * Disable Music if not a lot of memory
	 */

	if(!game->musicInBattle)
		game->gameMusic->disable();

	clockIcon = 0;
	mapWindow = 0;

	subMenu = 0;
	lastMovedUnit = 0;
	menuData = 0;
	ai = 0;

	if(startOB)
	{
		ob = startOB;
		ownOb = False;
	}
	else
	{
		ob = new OrderBattle;
		ownOb = True;
	}

	grid = new MapGrid;

#ifdef DEBUG
	showDisplayInfo = False;
#endif

	infoArea = new Region(machine.screen->getImage(), Rect(INFO_X, INFO_Y, INFO_WIDTH, INFO_HEIGHT));

	// batMode = new BattleOrderFunction;

	currentOrder = ORDER_Stand;
	gotDestination = False;
	currentUnit = 0;

	mouseOnMap = False;
	trackMode = Tracking;

	/*
	 * The time really needs to be passed as a parameter, taken from
	 * the campaign game or the battle setup.  Or it can be setup in
	 * between creating Battle and executing Battle::play.
	 */

	// gameTime.setBattleTime();
#ifdef DEBUG
	bLog.setTime(&timeInfo);
#endif

	lastCount = timer->getCount();

	spriteLib = new SpriteLibrary(battleSpriteFileName);
	sounds = new BattleSound(battleSoundFileName);

	miscObjects = new StaticObjectData;
	localTrees = new StaticObjectData;
	corpseObjects = new StaticObjectData;
	spriteList = new Sprite3DList;
	earthFace = new LineList;
	displayedObjects = new BattleDisplayList;
	displayedCommanders = new DispCommandList;
	zooming = False;
	// tempSprites = new TemporaryBattleSpriteList;

	CSAoffBattle = False;
	USAoffBattle = False;

	nextRedraw = 0;
}

BattleData::~BattleData()
{
#ifdef DEBUG
	bLog.setRealTime();
#endif

	if(ai)
		delete ai;

	delete infoArea;
	delete grid;

	delete displayedCommanders;	displayedCommanders = 0;
	delete displayedObjects;		displayedObjects = 0;
	delete earthFace;
	delete corpseObjects;
	delete localTrees;
	delete miscObjects;
	delete spriteList;
	delete spriteLib;
	delete sounds;

	if(ownOb)
		delete ob;
	else
		ob->clearupBattle();

	/*
	 * Re-enable Music if not a lot of memory
	 */

	if(!game->musicInBattle)
		game->gameMusic->enable();
}

/*
 * Process the battle game each game frame
 */

void BattleData::process(BattleControl* control)
{

	const int MinUnitsPerFrame = 100;		// Minimum Number of units to move each game frame regardless of time
	const int MaxMoveTime = timer->ticksPerSecond / 10;

	/*
	 * Update and display the time
	 */

	unsigned int count = timer->getCount();
	unsigned int ticks = count - lastCount;
	lastCount = count;
	count += MaxMoveTime;				// Maximum time to spend processing units is 10th second
	TimeBase oldTime = gameTime;

	control->processMessages(True);

	if(!game->connection->isLocal())
	{
		GameTicks gTicks;

		if(control)
		{
			gTicks = gameTime.realToGameTicks(ticks);

			TickPacket* packet = new TickPacket;
			packet->ticks = gTicks;
			game->connection->sendMessage(Connect_BS_Tick, (UBYTE*) packet, sizeof(TickPacket));
			while(!control->gotTicks && !game->connection->isLocal())
			{
				control->processMessages(False);
			}
			control->gotTicks = False;

#ifdef DEBUG
			netLog.printf("Ticks: Tx=%d, Rx=%d", gTicks, control->rxTicks);
#endif

			if(control->rxTicks < gTicks)
				gTicks = control->rxTicks;
		}
		else
			gTicks = 1;

		gameTime.addGameTicks(gTicks);

	}
	else
		gameTime.addRealTime(ticks);

	timeInfo = gameTime;

	if(clockIcon)
		clockIcon->setRedraw();

	/*
	 * Move all the units
	 */

	Boolean finished = False;
	int unitCount = 0;

	// CSAoffBattle = False;
	// USAoffBattle = False;

	do	
	{
		/*
		 * Get next Unit
		 */

		Unit* last = ob->getNextBattleUnit(lastMovedUnit, Rank_President, Rank_Regiment);

		/*
		 * If NULL returned, then start at the beginning again
		 */

		if(!last)
		{
			/*
			 * Is game over because everyone off battlefield?
			 */

			if(CSAoffBattle || USAoffBattle)
			{
				control->setFinish(BattleNoTroops);
				break;
			}


			CSAoffBattle = True;
			USAoffBattle = True;

			last = ob->getSides();
		}

		lastMovedUnit = last;

		UnitBattle* info = last->battleInfo;
		if(info)
		{
			if(info->isDead)	// Delete Units that died recently
			{
				delete info;
				last->battleInfo = 0;
			}
			else
			{
				finished = info->moveBattle(gameTime);
				info->checkIndependant();
				unitCount++;

				/*
				 * If its alive and well and on the battlefield
				 * clear the offBattle flags.
				 */

				if(onMainBattleField(info->where) && !info->routed && !info->isDead)
				{
					if(last->getSide() == SIDE_CSA)
						CSAoffBattle = False;
					else
						USAoffBattle = False;
				}
			}
		}

		/*
		 * Process for up to 4 ticks (except in multi player where
		 * it must process everything
		 */

		if(unitCount >= MinUnitsPerFrame)
		{
			if(!game->connection->isLocal() || (timer->getCount() >= count))
				finished = True;
		}

	} while(!finished);

	/*
	 * Check for end of battle
	 */

	if(battle->gameTime.ticks >= SunSet)
	{
		control->setFinish(BattleEndDay);
	}
}

void BattleData::drawBattleMap()
{
	/*
	 * Force view to be updated every 30 minutes of game time
	 */

	GameTicks newTick = gameTime.getTime();
	if(newTick >= nextRedraw)
		updateView();

	mapWindow->mapArea->setRedraw();
}


void BattleData::drawObjects(System3D& drawData)
{
	displayedCommanders->clear();
	corpseObjects->draw(drawData.view);

	/*
	 * Remove highlighted unit if mouse not over map
	 */

	if(!mouseOnMap)
	{
		if(zooming)
			// zoomOb->hide();
			;
		else
		{
			if(trackMode == Tracking)
			{
#if defined(STATS_INFO)
				showStats(currentUnit != 0);
#else
				if(currentUnit)
					clearInfo();
#endif

				currentUnit = 0;
			}
			else	// Assume getDestination
			{
				;
			}
		}
	}

	Unit* u = ob->sides;
	while(u)
	{
		drawUnits(u, drawData);
		u = u->sister;
	}

	// tempSprites->process();

	mouseOnMap = False;

}

void BattleData::drawUnits(Unit* ob, System3D& drawData)
{
	/*
 	 * Draw this unit
 	 */

	if(ob->battleInfo && !ob->battleInfo->isDead)
		ob->draw3D(drawData);

	/*
	 * Depending on zoom level, go down and draw children
	 */

	static Rank rankView[ZOOM_LEVELS] = {
		Rank_Regiment,		// Half Mile
		Rank_Regiment,		// 1 Mile
		Rank_Regiment,		// 2 Mile
		Rank_Regiment,		// 4 Mile
		Rank_Brigade		// 8 Mile
	};

	if(ob->getRank() < rankView[drawData.view.zoomLevel])
	{
		/*
		 * Draw its children
		 */

		Unit* c = ob->child;
		while(c)
		{
			drawUnits(c, drawData);
			c = c->sister;
		}
	}
}

#if !defined(STATS_INFO)

void BattleData::clearInfo()
{
	putLogo(infoArea);
}

#endif



/*
 * Process the mouse being over the 3D landscape
 */

void BattleData::overMap(Event* event, const Icon* icon, MenuData* d)
{
	mouseOnMap = True;

#ifdef DEBUG
	gotMouseLocation = data3D.screenToBattleLocation(event->where, mouseLocation);
#endif

#ifdef TEST_LOS
	/*
	 * Line Of sight Checker
	 */

	static Boolean testingLOS = False;


	if(toupper(event->key) == 'L')
	{
		event->key = 0;

		testingLOS = !testingLOS;

		if(testingLOS)
			game->simpleMessage("Testing LOS");
		else
			game->simpleMessage("Battle Game");
		trackMode = Tracking;
	}

	if(testingLOS)
	{
		if(event->buttons & Mouse::LeftButton)
		{
			BattleLocation l = destination;

			gotDestination = data3D.screenToBattleLocation(event->where, destination);
			if(gotDestination)
			{
				if(trackMode == GetDestination)
				{
					Boolean seen = inLOS(l, destination, Yard(200));

					if(seen)
						dialAlert(d, "LOS in LOS", "OK");
					else
						dialAlert(d, "LOS Blocked", "Oh Dear");

					trackMode = Tracking;
					game->simpleMessage("Click on start for LOS Test");
				}
				else
				{
					trackMode = GetDestination;
					game->simpleMessage("Click on Destination for LOS Test");
				}
			}
		}
		return;
	}

#endif



	/*
	 * If in tracking mode, then get closest unit
	 * otherwise in destination mode, work out a position for
	 * the pointer
	 */

	if(zooming || (trackMode == GetDestination))
	{
		gotDestination = data3D.screenToBattleLocation(event->where, destination);

		if(gotDestination)
		{
			if(zooming)
				d->setTempPointer(M_Zoom1);
			else
				d->setTempPointer(M_WhereTo1);
		}

	}
	else	// Assume Tracking
	{
		trackUnits(event->where);
	}

	
	if(event->buttons & Mouse::RightButton)
	{	// Go to CAL!
		if(zooming)
		{
			stopZoomMode();
		}
		else
		if(trackMode == Tracking)
		{
			chooseStackedObject();
#ifdef DEBUG
			// if(currentUnit && (seeAll || (currentUnit->getSide() == game->playersSide)))
			if(currentUnit && (seeAll || game->isPlayer(currentUnit->getSide())))
#else
			// if(currentUnit && (currentUnit->getSide() == game->playersSide))
			if(currentUnit && game->isPlayer(currentUnit->getSide()))
#endif
				runCAL();
		}
		else
			// batMode->closeDown();
			closeOrders();
	}
	else if(event->buttons & Mouse::LeftButton)
	{
		if(zooming)
		{
			zoomIn(newZoomLevel, destination);
#if defined(ZOOM_ONE_SHOT)		// Zoom stops after zooming.
			stopZoomMode();
#else
			startZoomMode();		// Zoom continues...
#endif
		}
		else
		if(trackMode == Tracking)
		{
			chooseStackedObject();

			/*
			 * Display Order box if on player's side (else ignore)
			 */

			if(currentUnit)
			{
				// if(currentUnit->getSide() == game->playersSide)
				if(game->isPlayer(currentUnit->getSide()))
				{
					// currentUnit->showBattleInfo(battle->infoArea);
					openOrders();
				}
			}
		}
		else if(trackMode == GetDestination)
		{
			if(currentUnit && gotDestination)
			{
				Location location;

				location.x = BattleToDist(destination.x);
				location.y = BattleToDist(destination.z);

				sendOrder(location);
			}
			else
				closeOrders();
		}
	}
	
}

void BattleData::startZoomMode(ZoomLevel newLevel)
{
	stopZoomMode();
	if(newLevel > data3D.view.zoomLevel)
	{
		data3D.view.zoomIn(newLevel);
		updateView();
	}
	else
	{
		oldZoomPointer = menuData->getPointer();
		menuData->setPointer(M_Zoom);
		zooming = True;
		newZoomLevel = newLevel;
	}
}

void BattleData::startZoomMode()
{
	stopZoomMode();
	ZoomLevel zoom = data3D.view.zoomLevel;
	
	if(zoom > Zoom_Min)
	{
		DECREMENT(zoom);
		startZoomMode(zoom);
	}
}


void BattleData::stopZoomMode()
{
	if(zooming)
	{
		zooming = False;
		menuData->setPointer(oldZoomPointer);
	}
}

void BattleData::zoomIn()
{
	stopZoomMode();
	if(data3D.view.zoomIn())
		updateView();
}

void BattleData::zoomIn(ZoomLevel level, const BattleLocation& location)
{
	stopZoomMode();
	data3D.view.zoomIn(level, location);
	updateView();
}


void BattleData::zoomOut()
{
	stopZoomMode();
	if(data3D.view.zoomOut())
		updateView();
}

void BattleData::updateView()
{
	nextRedraw = gameTime.getTime() + GameTicksPerMinute * 30;

	displayedObjects->clear();
	localTrees->clear();
	miscObjects->reset();
	corpseObjects->reset();

	data3D.view.setBoundaries(grid);
	setLight();
	grid->updateViewGrid(data3D);

	makeLocalTrees();

	spriteList->clear();
	miscObjects->draw(data3D.view);
	localTrees->draw(data3D.view);


	viewIcons->setRedraw();
	mapWindow->setRedraw();
}


/*
 * Generate Battle Map
 */

#ifdef DEBUG

Location makeRandLocation(const Location& centre, Distance xDev, Distance yDev)
{
	Location val;

	val.x = centre.x + game->gameRand(xDev);
	val.y = centre.y + game->gameRand(yDev);

	return val;
}


/*
 * Make a Test Order of Battle
 */

President* BattleData::makeTestSide(Side side, const Location& l)
{
	GeneralGenerator gen;
	RegimentGenerator reg(True);

	President* p = new President(side);
	ob->addSide(p);

	Army* army = new Army(l, "Unnamed Army");
	ob->assignGeneral(gen.make(side, ob), army);
	ob->assignUnit(army, p);
  	army->facing = game->gameRand.getW();


	Distance meanax = 0;
	Distance meanay = 0;

	int corpsCount = (game->gameRand(4)) + 2;
	while(corpsCount--)
	{
		Location ac = makeRandLocation(l, Mile(2), Mile(2));

		Distance meancx = 0;
		Distance meancy = 0;

		Corps* corps = new Corps(ac);
		ob->assignGeneral(gen.make(side, ob), corps);
		ob->assignUnit(corps, army);
		corps->facing = game->gameRand.getW();

		int divCount = (game->gameRand(5)) + 1;

		while(divCount--)
		{
			Location ad = makeRandLocation(ac, Mile(1), Mile(1));

			Distance meandx = 0;
			Distance meandy = 0;

			Division* div = new Division(ad);
			// ob.addUnit(div);
			ob->assignGeneral(gen.make(side, ob), div);
			ob->assignUnit(div, corps);
			div->facing = game->gameRand.getW();

			int brigCount = (game->gameRand(5)) + 1;	// 1..5 Brigades
			while(brigCount--)
			{
				Location ab = makeRandLocation(ad, Yard(880), Yard(880));

				Distance meanX = 0;
				Distance meanY = 0;

				Brigade* brig = new Brigade(ab);
				// ob.addUnit(brig);
				ob->assignGeneral(gen.make(side, ob), brig);
				ob->assignUnit(brig, div);
				brig->facing = game->gameRand.getW();

				int regCount = (game->gameRand(6)) + 1;	// 1..6 Regiments
				while(regCount--)
				{
					Location ar = makeRandLocation(ab, Yard(440), Yard(440));
					// ostrstream aName;

					meanX += ar.x;
					meanY += ar.y;

					// aName << regCount+1 << '/' << brigCount+1 << '/' << divCount+1 << " Regiment" << '\0';

					Regiment* regiment = reg.make(ar);	// , aName.str());
					// regiment->facing = game->gameRand.getW();
					regiment->facing = brig->facing;

					ob->assignUnit(regiment, brig);
				}

				/*
				 * Put brigade at mean location of all units
				 */

				brig->location.x = meanX / brig->childCount;
				brig->location.y = meanY / brig->childCount;

				meandx += brig->location.x;
				meandy += brig->location.y;

			}

			div->location.x = meandx / div->childCount;
			div->location.y = meandy / div->childCount;

			meancx += div->location.x;
			meancy += div->location.y;

		}
		
		corps->location.x = meancx / corps->childCount;
		corps->location.y = meancy / corps->childCount;

		meanax += corps->location.x;
		meanay += corps->location.y;
	}

	army->location.x = meanax / army->childCount;
	army->location.y = meanay / army->childCount;

	return p;
}

/*
 * Create a new battlefield from user's choices or from a saved game
 */

void BattleData::makeNewBattle()
{
	campaignLocation = Location(0,0);

#ifdef DEBUG
	char popupBuffer[400];
	strcpy(popupBuffer, "Creating Test Armies");
	PopupText popup(popupBuffer);
#endif

#ifdef DEBUG
	strcat(popupBuffer, "\rCSA Army");
	popup.showText(popupBuffer);
#endif
	// ob->addSide(makeTestSide(SIDE_CSA, Location(Mile(5)/2, Mile( 0))));
	makeTestSide(SIDE_CSA, Location(Mile(5)/2, Mile( 0)));
#ifdef DEBUG
	strcat(popupBuffer, "\rUSA Army");
	popup.showText(popupBuffer);
#endif
	// ob->addSide(makeTestSide(SIDE_USA, Location(-Mile(5)/2, Mile(0))));
	makeTestSide(SIDE_USA, Location(-Mile(5)/2, Mile(0)));

#ifdef DEBUG
	strcat(popupBuffer, "\rWriting log file");
	popup.showText(popupBuffer);
	logArmies(ob->sides);
#endif

	ob->setupBattle();
	// process(True, 0);			// Force one pass through each object
}

#endif		// DEBUG

/*
 * Move Unit closer to centre of battle field
 */

void UnitBattle::moveCloser(Distance d, Wangle w)
{
	where.x -= mcos(d, w);
	where.y -= msin(d, w);
}

/*
 * Copy order of battle from campaign and setup units ready for battle
 */

void BattleData::deploySide(President* p)
{
	Unit* closest = 0;
	Distance closeDistance = 0;

	/*
	 * Pass 1: Find closest brigade to battlefield centre
	 */

	for(Unit* a = p->child; a; a = a->sister)
	{
		if(a->battleInfo || (a->hasCampaignReallyDetached()))
		{
			for(Unit* c = a->child; c; c = c->sister)
			{
				if(c->battleInfo || (c->hasCampaignReallyDetached()))
				{
					for(Unit* d = c->child; d; d = d->sister)
					{
						if(d->battleInfo || (d->hasCampaignReallyDetached()))
						{
							for(Unit* b = d->child; b; b = b->sister)
							{
								if(b->battleInfo)
								{
									Distance d = distance(b->battleInfo->where.x, b->battleInfo->where.y);

									if(!closest || (d < closeDistance))
									{
										closest = b;
										closeDistance = d;
									}

								}
							}	// Brigades
						}
					}	// Divisions
				}
			}	// Corps
		}
	}	// Armies

	if(closest)
	{
#ifdef DEBUG
		bLog.printf("Closest brigade to battlefield on side %d is %s",
			(int)p->getSide(), closest->getName(True));

		int yards = unitToYard(closeDistance);
		int miles = yards / YardsPerMile;
		int remainder = ((yards % YardsPerMile) * 100) / YardsPerMile;

		bLog.printf("They are %d.%02d miles from the battlefield",
			miles, remainder);

#endif

		/*
		 * Now calculate how far it needs to move
		 */

		Distance adjust;
		Wangle dir;
		
		if(closeDistance > Mile(2))
		{
			adjust = closeDistance - Mile(2);
			dir = direction(closest->battleInfo->where.x, closest->battleInfo->where.y);
		}
		else
			adjust = 0;


		/*
		 * Adjust everyone else's position and put regiments around their
		 * brigades.
		 */

		for(Unit* a = p->child; a; a = a->sister)
		{
			if(a->battleInfo || (a->hasCampaignReallyDetached()))
			{
				if(adjust && a->battleInfo)
					a->battleInfo->moveCloser(adjust, dir);

				for(Unit* c = a->child; c; c = c->sister)
				{
					if(c->battleInfo || (c->hasCampaignReallyDetached()))
					{
						if(adjust && c->battleInfo)
							c->battleInfo->moveCloser(adjust, dir);

						for(Unit* d = c->child; d; d = d->sister)
						{
							if(d->battleInfo || (d->hasCampaignReallyDetached()))
							{
								if(adjust && d->battleInfo)
									d->battleInfo->moveCloser(adjust, dir);

								for(Unit* b = d->child; b; b = b->sister)
								{
									if(b->battleInfo)
									{
										if(adjust)
											b->battleInfo->moveCloser(adjust, dir);

										BrigadeBattle* bb = (BrigadeBattle*) b->battleInfo;
										bb->placeRegiments();
									}
								}	// Brigades
							}
						}	// Divisions
					}
				}	// Corps
			}
		}	// Armies
	}
#ifdef DEBUG
	else
		throw GeneralError("Nobody on side %d is on the battlefield!!!!", (int)p->getSide());
#endif
}

void BattleData::setTroops(OrderBattle* orderBattle, MarkedBattle* batl)
{
	campaignLocation = batl->where;

	ob = orderBattle;

	ob->setupBattle(batl);
	/*
	 * Deploy troops (i.e. force them onto battlefield!)
	 *
	 * Method:
	 *   For each side
	 *      Determine closest brigade to battlefield
	 *      If not within distance from battlefield centre (2 miles?)
	 *        Calculate distance to move (in time?)
	 *        For every unit on side
	 *          Adjust position towards battlefield centre by above time
	 */

	President* p = ob->getSides();
	while(p)
	{
		deploySide(p);

		p = (President*) p->sister;
	}

	// process(True, 0);			// Force one pass through each object
}

#if 0
/*
 * Make sure any units off the battlefield have orders to march on
 */


void orderOntoBattle(Unit* u)
{
	if(u->battleInfo && 
		// u->isReallyIndependant() && 
		(u->getBattleAttachMode() == Detached) && 
		!onBattleField(u->battleInfo->where))
			u->battleInfo->battleOrder = BattleOrder(ORDER_Advance, Location(0,0));
}

#endif

/*
 * Bodge up Order of battle so that:
 *		1. Units off battlefield have orders to get onto the battlefield
 *		2. Units without any attached units are attached to at least one unit
 *		3. Units that are attached have the same orders as their parent
 */


void BattleData::initTroops()
{
#if 0
	for(Unit* p = ob->getSides(); p; p = p->sister)
	{
		for(Unit* a = p->child; a; a = a->sister)
		{
			if(a->battleInfo || (a->hasCampaignReallyDetached()))
			{
				orderOntoBattle(a);

				for(Unit* c = a->child; c; c = c->sister)
				{
					if(c->battleInfo || (c->hasCampaignReallyDetached()))
					{
						orderOntoBattle(c);

						for(Unit* d = c->child; d; d = d->sister)
						{
							if(d->battleInfo || (d->hasCampaignReallyDetached()))
							{
								orderOntoBattle(d);

								for(Unit* b = d->child; b; b = b->sister)
								{
									if(b->battleInfo)
									{
										orderOntoBattle(b);
									}
								}	// Brigades
							}
						}	// Divisions
					}
				}	// Corps
			}
		}	// Armies
	}	// President
#else
	for(Unit* p = ob->getSides(); p; p = p->sister)
	{
		UnitIter iter(p, False, False, Rank_Army, Rank_Regiment);

		while(iter.ok())
		{
			Unit* unit = iter.get();

			ASSERT(unit != 0);

			if(unit->battleInfo)
			{
				// Force onto battlefield

				if(!onBattleField(unit->battleInfo->where))
					unit->battleInfo->battleOrder = BattleOrder(ORDER_Advance, Location(0,0));

				// Check for any dependants

				if(unit->childCount)
				{
					Boolean hasChild = False;
					Unit* child = unit->child;
					while(child)
					{
						if(child->getBattleAttachMode() == Attached)
						{
							hasChild =True;
							break;
						}

						child = child->sister;
					}

					if(!hasChild)
					{
						child = unit->child;
						Unit* best = 0;
						Distance d = 0;

						while(child)
						{
							if(child->getBattleAttachMode() != Detached)
								best = child;
							else
							{
								Distance d1 = distance
									(
										child->battleInfo->where.x - unit->battleInfo->where.x,
										child->battleInfo->where.y - unit->battleInfo->where.y
									);

								if(!best || (d1 < d))
								{
									best = child;
									d = d1;
								}
							}

							child = child->sister;
						}

						ASSERT(best != 0);

#ifdef DEBUG
						bLog.printf("Patching %s to attach to %s",
							unit->getName(True),
							best->getName(True));
#endif

						best->setBattleAttachMode(Attached);
						unit->battleInfo->battleOrder = best->battleInfo->battleOrder;
					}
				}

				/*
				 * Make attached units have same orders as parent
				 */

				if(unit->getBattleAttachMode() != Detached)
				{
					Unit* sup = unit->superior;

					ASSERT(sup != 0);
					ASSERT(sup->battleInfo != 0);

					if(sup->battleInfo)
					{
#ifdef DEBUG
						if(unit->battleInfo->battleOrder != sup->battleInfo->battleOrder)
						{
							bLog.printf("Patching orders for %s", unit->getName(True));
						}
#endif
						unit->battleInfo->battleOrder = sup->battleInfo->battleOrder;
					}
				}
			}
		}
	}


#endif
}

/*
 * Procedure to call every game frame
 */

void BattleControl::proc()
{
	if(!isPaused())
		battle->process(this);
	else
		processMessages(False);

	soundManager.process();
}

/*
 * Entry point to the battle game
 *
 * Returns value depending on how game was ended
 *		MenuQuit : Player tried to quit to DOS
 *    BattleEndDay : Day finished
 *    BattleSurrender : Player clicked on flag
 *    BattleNoTroops : Everyone is dead or withdrew off the battlefield
 */

int BattleData::play(Boolean noDeploy)
{
	/*
	 * Get moving if off battlefield
	 */

	initTroops();
	setupStats();

	/*
	 * Setup Map Display stuff (viewpoint, lightsource).
	 */

	BattleControl control;

	/*
	 * Set up AI if playing
	 */


#ifdef DEBUG
	// if(!disableAI && (game->playerCount == 1))
	if(!disableAI && game->isAIactive())
#else
	// if(game->playerCount == 1)
	if(game->isAIactive())
#endif
	{
		ai = new AI_Battle();

		// ai->AI_Bat_Initiate(otherSide(game->playersSide), noDeploy);
		ai->AI_Bat_Initiate(otherSide(game->getLocalSide()), noDeploy);
	}


	battle->updateView();

	while(!control.isFinished())
	{
		battle->drawBattleMap();

		control.process();

	}

	soundManager.stopAll();

	return control.getMode();
}

void BattleControl::processPacket(Packet* packet)
{
	switch(packet->type)
	{
	case Connect_BS_Order:
		battle->rxBattleOrder(packet->data);
		break;

	case Connect_BS_Tick:
		if(packet->sender != RID_Local)
		{
			TickPacket* pkt = (TickPacket*) packet->data;

			gotTicks = True;
			rxTicks = pkt->ticks;
		}
		break;

	case Connect_Finish:
		if(!isFinished())
		{
			if(packet->sender == RID_Local)
			{
				if(waitRemoteReply())
				{
					FinishPKT* pkt = (FinishPKT*)packet->data;
					setFinish(pkt->how);
				}
				else
					dialAlert(0, language(lge_RemoteNotReady), language(lge_OK));
			}
			else
			{
				FinishPKT* pkt = (FinishPKT*)packet->data;

				const char* text;
				const char* buttons;

				switch(pkt->how)
				{
				case BattleSurrender:
					text = language(lge_RemoteEndBattle);
					buttons = language(lge_EndBattle);
					break;
				case MenuMainMenu:
				default:
					text = language(lge_RemoteGoMM);
					buttons = language(lge_mmCancel);
					break;
				}
			
				if(askRemoteReply(text, buttons))
					setFinish(pkt->how);
			}
		}

		break;

	case Connect_CAL_Rejoin:
		rxCALRejoinBattle(packet);
		break;

	case Connect_CAL_Reattach:
		rxCALReattachBattle(packet);
		break;

	case Connect_CAL_ReattachAll:
		rxCALReattachAllBattle(packet);
		break;


	default:
#ifdef DEBUG
		netLog.printf("Unknown packet received\rtype: %d, length:%d", (int) packet->type, (int)packet->length);
#endif
		break;
	}
}


/*
 * Run Battle CAL, automatically handling find & Give orders
 */

void BattleData::runCAL()
{
	ClockIcon* oldClock = battle->clockIcon;
	battle->clockIcon = 0;

	/*
	 * Close down anything going on
	 */

	if(zooming)
		stopZoomMode();

	if(trackMode == GetDestination)
		closeOrders();

	int giveOrders;
	Unit* newUnit = doBattleCAL(battle->ob, currentUnit, &giveOrders, menuData);
	menuData->setUpdate();

	/*
	 * Centre on new unit
	 */

	if(newUnit && (newUnit != currentUnit))
	{
		currentUnit = newUnit;

		static ZoomLevel unitZoomLevel[] = {
			Zoom_2,		// President
			Zoom_2,		// Army
			Zoom_2,		// Corps
			Zoom_2,		// Division
			Zoom_1,		// Brigade
			Zoom_Half	// Regiment
		};

		ZoomLevel wantLevel = unitZoomLevel[currentUnit->getRank()];

		if(wantLevel > data3D.view.zoomLevel)
			wantLevel = data3D.view.zoomLevel;

		zoomIn(wantLevel, currentUnit->battleInfo->where);
	}


	/*
	 * Put into Order Mode if appropriate
	 */

	if(newUnit && giveOrders)
		openOrders();
	else if(currentUnit)
		currentUnit->showBattleInfo(battle->infoArea);
	
	battle->clockIcon = oldClock;
}

