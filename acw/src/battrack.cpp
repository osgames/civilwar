/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Track Units on Battlefield
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include "batldata.h"
#include "batdisp.h"
#include "game.h"
#include "dialogue.h"
#include "options.h"

/*
 * Distance from mouse to be included in stacked units
 */

const int TrackDistance = 20;		// Was 32

/*
 * Linked List functions
 */

void LinkListBase::addAfter(LinkListBase* newNode)
{
	newNode->next = next;
	newNode->prev = this;
	next->prev = newNode;
	next = newNode;
}

void LinkListBase::addBefore(LinkListBase* newNode)
{
	newNode->next = this;
	newNode->prev = prev;
	prev->next = newNode;
	prev = newNode;
}

void LinkListBase::remove()
{
	LinkListBase* n = next;
	LinkListBase* p = prev;

	ASSERT(n != 0);
	ASSERT(p != 0);

	p->next = n;
	n->prev = p;
}

/*
 * TrackUnitList functions
 */

TrackedUnitList::TrackedUnitList()
{
	count = 0;
	entry.unit = 0;
	entry.d = 0;
	entry.inUse = False;
}

TrackedUnitList::~TrackedUnitList()
{
	while(count)
	{
		TrackObject* ob = entry.getNext();
		ob->remove();
		delete ob;
		count--;
	}
}

/*
 * Move head of list to back
 */

void TrackedUnitList::next()
{
	if(count > 1)
	{
		TrackObject* ob = entry.getNext();
		ob->remove();
		entry.addBefore(ob);
	}
}

/*
 * Move back of list to head
 */

void TrackedUnitList::prev()
{
	if(count > 1)
	{
		TrackObject* ob = entry.getPrev();
		ob->remove();
		entry.addAfter(ob);
	}
}

/*
 * Return the first unit
 */

Unit* TrackedUnitList::getTop() const
{
	TrackObject* ob = entry.getNext();

	return ob->unit;
}

void TrackedUnitList::append(TrackObject* ob)
{
	entry.addBefore(ob);
	count++;
}

void TrackedUnitList::remove(TrackObject* ob)
{
	ob->remove();
	count--;
}

/*
 * Clear all inUse flags
 */

void TrackedUnitList::clearInUse()
{
	for(TrackObject* ob = entry.getNext(); ob != &entry; ob = ob->getNext())
		ob->inUse = False;
}

/*
 * Remove objects which don't have inUse flag set.
 * Return number of removed items
 */

int TrackedUnitList::removeUnused()
{
	int n = 0;

	TrackObject* ob = entry.getNext();
	while(ob != &entry)
	{
		TrackObject* next = ob->getNext();

		if(!ob->inUse)
		{
			remove(ob);
			delete ob;
			n++;
		}

		ob = next;
	}

	return n;
}

/*
 * Find a unit in list
 * Return 0 if not in list
 */

TrackObject* TrackedUnitList::findUnit(Unit* u) const
{
	for(TrackObject* ob = entry.getNext(); ob != &entry; ob = ob->getNext())
	{
		if(ob->unit == u)
			return ob;
	}
	return 0;
}

/*
 * Sort list so that smaller distances come first
 */

void TrackedUnitList::sort()
{
	TrackObject* ob = entry.getNext();
	while(ob != &entry)
	{
		TrackObject *ob1 = ob;

		SDimension d = 0;
		TrackObject* best = 0;

		do
		{
			if(!best || (ob1->d < d) || 
				( (ob1->d == d) && (ob1->unit->getRank() < best->unit->getRank())) )
			{
				d = ob1->d;
				best = ob1;
			}
			ob1 = ob1->getNext();
		} while(ob1 != &entry);

		if(best != ob)
		{
			best->remove();
			ob->addBefore(best);
			ob = best;
		}

		ob = ob->getNext();
	}
}

Boolean TrackedUnitList::updatePosition(const Point& p)
{
	if(p != lastPosition)
	{
		lastPosition = p;
		return True;
	}
	else
		return False;
}

/*
 * Maintain list of units near mouse
 *
 * Rewritten for Masters edition
 */

void BattleData::trackUnits(const Point& p)
{
	Boolean changed = unitList.updatePosition(p);

	unitList.clearInUse();

	/*
	 * Step through units and add to list if not already in it
	 */

	DispCommander* com = 0;
	while( (com = displayedCommanders->getNext(com)) != 0)
	{
		Unit* u = com->u;
	 
		SDimension d = distance(p.x - com->p.x, p.y - com->p.y);

		if(d < TrackDistance)		// Must be fairly close
		{
			TrackObject* ob = unitList.findUnit(u);

			if(ob == 0)
			{
				ob = new TrackObject;
				ob->unit = u;
				unitList.append(ob);

				changed = True;
			}
			ob->d = d;
			ob->inUse = True;
		}
	}

	/*
	 * Check for unused items and remove them
	 */

	if(unitList.removeUnused() != 0)
		changed = True;

	/*
	 * If changed then sort the list
	 */

	if(changed)
		unitList.sort();

	/*
	 * Set up currentUnit and update display areas
	 */

	if(unitList.entries() > 0)
	{
		static GameTicks lastUpdateTime = 0;

		Unit* topUnit = unitList.getTop();

		if( (currentUnit != topUnit) ||
			 (gameTime >= lastUpdateTime) )
		{
			lastUpdateTime = gameTime + GameTicksPerMinute * 5;

			currentUnit = topUnit;
			currentUnit->showBattleInfo(infoArea);
		}
	}
	else
	{
		showStats(currentUnit != 0);
		currentUnit = 0;
	}

}

void BattleData::prevStack()
{
	Boolean finished = False;

	Unit* original = currentUnit;

	while(!finished)
	{
		finished = True;

		unitList.prev();
		currentUnit = unitList.getTop();

		ASSERT(currentUnit != 0);

		if(currentUnit)
		{
			if(trackMode == GetDestination)
			{
				if(seeAll || 
					game->isPlayer(currentUnit->getSide()) ||
					(currentUnit == original) )
						openOrders();
				else
					finished = False;
			}
			else
				currentUnit->showBattleInfo(infoArea);
		}
	}
}

void BattleData::nextStack()
{
	Boolean finished = False;

	Unit* original = currentUnit;

	while(!finished)
	{
		finished = True;

		unitList.next();
		currentUnit = unitList.getTop();

		ASSERT(currentUnit != 0);

		if(currentUnit)
		{
			if(trackMode == GetDestination)
			{
				if(seeAll || 
					game->isPlayer(currentUnit->getSide()) ||
					(currentUnit == original) )
						openOrders();
				else
					finished = False;
			}
			else
				currentUnit->showBattleInfo(infoArea);
		}
	}
}



Boolean TrackedUnitList::isValid(Unit* u) const
{
	ASSERT(u != 0);

	if(!seeAll && !game->isPlayer(u->getSide()))
		return False;

	/*
	 * Don't include attached units if their parent is there
	 */

	if(u->getAttachMode() == Attached)
	{
		Unit* top = getTop();
		ASSERT(top != 0);

		if(u != top)
		{
			Unit* parent = u->superior;

			ASSERT(parent != 0);

			if(parent)
			{
				do
				{
					if(findUnit(parent))
						return False;

					parent = parent->superior;
				} while(parent && (parent->getAttachMode() == Attached));
			}
		}
	}

	return True;
}

int TrackedUnitList::countValid() const
{
	int n = 0;

	for(TrackObject* ob = entry.getNext(); ob != &entry; ob = ob->getNext())
	{
		if(isValid(ob->unit))
			n++;
	}

	return n;
}

TrackObject* TrackedUnitList::getNextValid(TrackObject* ob) const
{
	while(ob != &entry)
	{
		if(ob == 0)
			ob = entry.getNext();
		else
			ob = ob->getNext();

		if(isValid(ob->unit))
			return ob;
	}

	return 0;
}




/*
 * Let player choose which of several stacked units to select
 */

void BattleData::chooseStackedObject()
{
	/*
	 * Count how many valid units there are
	 */

	int count = unitList.countValid();

	if(count > 1)
	{
		if(count > 10)
			count = 10;

		char** choices = new char*[count + 1];
		char** choice = choices;

		TrackObject* ob = 0;

		while(count-- && ((ob = unitList.getNextValid(ob)) != 0))
		{
			Unit* u = ob->unit;

			const char* name = u->getName(True);
			char* buf = new char[strlen(name) + 10];

			sprintf(buf, "%s (%ld)", name, u->getDependantStrength());

			*choice++ = buf;
		}

		*choice++ = 0;

		int result = chooseFromList(menuData, 0, (const char**) choices, 0);

		choice = choices;
		while(*choice)
		{
			delete[] *choice;
			*choice++ = 0;
		}

		delete[] choices;

		if(result > 0)
		{
			ob = 0;
			while(result--)
				ob = unitList.getNextValid(ob);

			currentUnit = ob->unit;
		}
	}
}


