/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *		Utility to convert betwwen a ascii file to the format of the 
 *		language facility within the Cival War game.
 *
 *		The function only uses text that is within spechmarks ( " ).
 *		Everything else is treated like REM staements and are ignored.
 *
 *		If you need to use a '"' then proceed it with a backslash i.e '\"'
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:54  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */


#ifndef CHRIS_VERSION

/*
 * New Steven's Version
 */

#include <stdio.h>
#include <ctype.h>
#include "types.h"

void usage()
{
	puts(	"\nUsage:\n"
			"\tText2lge TextFile.txt [Options]\n\n"
			"Options are:\n"
			"\t-o filename.asc : set output filename\n");
}

void main(int argc, char* argv[])
{
	/*
	 * Read arguments
	 */

	char* lgeName = 0;
	char* TextName = 0;
	int i = 0;

	while(++i < argc)
	{
		char* arg = argv[i];

		char c = *arg++;

		if((c == '-') || (c == '/'))
		{
			c = *arg++;

			switch(toupper(c))
			{
			case 'O':	// Set output filename
				if(++i < argc)
				{
					if(!lgeName)
					{
						lgeName = argv[i];
						break;
					}
					else
					{
						puts("There can only be one output filename");
						usage();
						return;
					}
				}
				puts("-O must be followed by a filename");
				usage();
				return;

			case 'H':
			case '?':
				usage();
				return;

			default:
				printf("unknown option %c\n", argv[i]);
				usage();
				return;
			}
		}
		else	// Assume input file
		{
			if(!TextName)
			{
				TextName = argv[i];
			}
			else
			{
				puts("There can only be one! ( Input file that is )");
				usage();
				return;
			}
		}
	}

	/*
	 * Open File for reading
	 * and count the strings
	 */

	FILE * fpIn = fopen(TextName, "r");
	if(fpIn)
	{
		printf("Reading %s\n", TextName);

		int stringCount = 0;

		Boolean waitStart = True;
		Boolean inSwitch = False;

		while(!feof(fpIn))
		{
			char c = fgetc(fpIn);

			if(waitStart)
			{
				if(c == '"')
					waitStart = False;
			}
			else
			{
				if(inSwitch)
				{
					inSwitch = False;
					//...
				}
				else
				{
					if(c == '\\')
						inSwitch = True;
					else if(c == '"')
					{
						waitStart = True;
						stringCount++;
					}
				}
			}
		}

		if(!waitStart)
			printf("Warning: Quote's not matched!\n");

		printf("There are %d strings\n", stringCount);

		rewind(fpIn);

		/*
		 * Now write the strings out
		 */

		if(lgeName)
		{
			FILE* fpOut = fopen(lgeName, "wb");
			if(fpOut)
			{
				printf("Writing %s\n", lgeName);

				fputc(stringCount & 0xff, fpOut);
				fputc(stringCount >> 8, fpOut);

				ULONG indexPos = ftell(fpOut);

				for(int i = 0; i < stringCount; i++)
				{
					fputc(0, fpOut);
					fputc(0, fpOut);
					fputc(0, fpOut);
					fputc(0, fpOut);
				}

				for(i = 0; i < stringCount; i++)
				{
					ULONG pos = ftell(fpOut);
					fseek(fpOut, indexPos, SEEK_SET);
					fputc(pos & 0xff, fpOut);
					fputc(pos >> 8, fpOut);
					fputc(pos >> 16, fpOut);
					fputc(pos >> 24, fpOut);

					indexPos = ftell(fpOut);

					fseek(fpOut, pos, SEEK_SET);

					/*
					 * Put in dummy length
					 */

					fputc(0, fpOut);
					fputc(0, fpOut);

					/*
					 * Parse String
					 */

					// Look for start of string

					while(!feof(fpIn))
					{
						char c = fgetc(fpIn);
						if(c == '"')
							break;
					}

					// Process String

					inSwitch = False;
					Boolean finishString = False;
					int stringLength = 0;

					while(!finishString && !feof(fpIn))
					{
						char c = fgetc(fpIn);

						if(inSwitch)
						{
							if(c == 'r')
							{
								fputc('\r', fpOut);
								stringLength++;
							}
							else if(c == 'n')
							{
								fputc('\n', fpOut);
								stringLength++;
							}
							else
							{
								fputc(c, fpOut);
								stringLength++;
							}
							inSwitch = False;
						}
						else
						{
							if(c == '\n')
								c = ' ';

							if(c == '"')
								finishString = True;
							else if(c == '\\')
								inSwitch = True;
							else
							{
								fputc(c, fpOut);
								stringLength++;
							}
						}
					}

					/*
					 * Rewrite the length
					 */

					ULONG pos1 = ftell(fpOut);
					fseek(fpOut, pos, SEEK_SET);
					fputc(stringLength, fpOut);
					fputc(stringLength >> 8, fpOut);
					fseek(fpOut, pos1, SEEK_SET);
				}

				fclose(fpOut);
			}
			else
				printf("Can't Create %s\n", lgeName);
		}
		else
			printf("No Output file specified\n");

		fclose(fpIn);
	}
	else
		printf("Can't open %s\n", TextName);
}



#else	// This is Chris's Old Version

#include <conio.h>
#include <stdlib.h>
#include <ctype.h>
#include <iostream.h>

#include "types.h"
#include "textload.h"

void usage()
{
	cout << "\nUsage:\n" <<
			  "\tText2lge TextFile.txt [Options]\n\n" <<
			  "Options are:\n" <<
			  "\t-o filename.asc : set output filename\n" <<
			  endl;
}

void main(int argc, char* argv[])
{
	char* TextName = 0;
	char* lgeName = 0;

	char error = 0;

	TextLoad* text;

	char* orig;

	long int tsize;

	/*
	 * Read arguments
	 */

	int i = 0;

	while(++i < argc)
	{
		char* arg = argv[i];

		char c = *arg++;

		if((c == '-') || (c == '/'))
		{
			c = *arg++;

			switch(toupper(c))
			{
			case 'O':	// Set output filename
				if(++i < argc)
				{
					if(!lgeName)
					{
						lgeName = argv[i];
#if 0
						cout << "Output file: " << lgeName << endl;
#endif
						break;
					}
					else
					{
						cout << "There can only be one output filename" << endl;
						usage();
						return;
					}
				}
				cout << "-O must be followed by a filename" << endl;
				usage();
				return;

			case 'H':
			case '?':
				usage();
				return;

			default:
				cout << "unknown option " << argv[i] << endl;
				usage();
				return;
			}
		}
		else	// Assume input file
		{
			if(!TextName)
			{
				TextName = argv[i];
#if 0
				cout << "Input file: " << TextName << endl;
#endif
			}
			else
			{
				cout << "There can only be one! ( Input file that is )" << endl;
				usage();
				return;
			}
		}
	}

	/*
	 * See what we've got
	 */

	if(!TextName)
	{
		cout << "\nNo ascii file specified" << endl;
		usage();
		return;
	}

	text = new TextLoad( TextName );

	if ( error = text->bad() )
	{
	 	cout << "Error "<< error << " with " << TextName << endl;
		return;
	}

	cout << "\nOpened " << TextName << endl;

	text->getdata();

	cout << "Loaded ascii data" << endl;

	orig = text->gettexdata();

	tsize = text->getSize();

	if ( !lgeName )
	{
		char lgen[ 13 ];

		for ( char loop = 0; loop < strlen( TextName ); loop++ )
		{
			lgen[loop] = TextName[loop];

			if ( TextName[loop] == '.' ) break;
		}

		lgen[loop + 1] = '\0';

		strcat( lgen, "lge" );

		lgeName = lgen;
	}

	fstream language;

	language.open( lgeName, ios::out | ios::binary );

	if ( !language )
	{
		cout << "Can't Open output file!!" << endl;
		return;
	}

	cout << "Outputing data to " << lgeName << endl;

	unsigned char element;

	long int count = 0;

	long int ptr = 0;

	long int store;

	long int maxstore = 0;

	Boolean state = 0;

	short int num_entries = 0;

	language.write ( ( char *)&num_entries, sizeof ( short int ) );

	while ( count++ < tsize )
	{
	 	element = *orig++;

		if ( state == 1 ) ptr++;

		if ( element == '"' )
		{
			if ( state == 0 )
			{
				language.write ( ( char *)&ptr, sizeof ( long int ) );
				num_entries++;
				store = ptr;
			}
			else
			{
				if ( (ptr - store - 2) > maxstore ) maxstore = ptr - store - 2;
			}

			ptr++;

			state ^= 1;
		}
		else if ( element == '\\' ) 
		{
			if ( *orig == '" ' ) orig++;
			else if ( *orig == 'r' ) 
			{
			 	orig++;
				element = 10;
			}
		}
	}

	cout << "Got " << num_entries << " pointers... Now copying text " << endl;

	orig = text->gettexdata();

	count = 0;

	while ( count++ < tsize )
	{
	 	element = *orig++;

		if ( element == '"' )
		{
			if ( state ) 
			{
				language.write ( (unsigned char* )&element, sizeof ( char ) );
			 	element = '\r';
				language.write ( (unsigned char* )&element, sizeof ( char ) );
			}

			state ^= 1;
		}
		else if ( element == '\\' ) 
		{
			if ( *orig == '" ' ) 
			{
				orig++;
				element = '"';
			}
			else if ( *orig == 'r' ) 
			{
			 	orig++;
				element = '\r';
			}
			// language.write ( (unsigned char* )&element, sizeof ( char ) );

			// element = *orig++;
		}

		if ( state ) language.write ( (unsigned char* )&element, sizeof ( char ) );
	}

	cout << "The maximum text length was " << maxstore << endl;

	cout << "\nTis done" << endl;

	// Store number of pointers
	
	language.seekg( 0, ios::beg );    // reset to begining position;

	language.write ( ( char *)&num_entries, sizeof ( short int ) );

	language.close();

	delete text;
}

#endif
