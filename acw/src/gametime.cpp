/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Game Time Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.7  1994/09/02  21:25:33  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.5  1994/05/21  13:16:19  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/04/05  12:28:09  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/03/11  23:12:15  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/03/10  14:27:18  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/02/28  23:04:17  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <time.h>
#include "gametime.h"
#include "timer.h"
#if !defined(CAMPEDIT) && !defined(BATEDIT)
#include "game.h"
#endif
#ifdef DEBUG
#include "error.h"
#endif

#include "language.h"
#include <string.h>
/*
 * Days since start of year to start of month
 */

static UWORD monthDays[] = {
	0,										  		// January
	31,											// February
	31+28,										// March
	31+28+31,									// April
	31+28+31+30,								// May
	31+28+31+30+31,							// June
	31+28+31+30+31+30,						// July
	31+28+31+30+31+30+31,					// August
	31+28+31+30+31+30+31+31,				// September
	31+28+31+30+31+30+31+31+30,			// October
	31+28+31+30+31+30+31+31+30+31,		// November
	31+28+31+30+31+30+31+31+30+31+30		// December
};

static LGE monthNames[] = {
	lge_Jan,
	lge_Feb,
	lge_Mar,
	lge_Apr,
	lge_May,
	lge_Jun,
	lge_Jul,
	lge_Aug,
	lge_Sep,
	lge_Oct,
	lge_Nov,
	lge_Dec
};


/*
 * Advance the time to specified time
 * time is in seconds since midnight
 */

void TimeBase::advanceTime(GameTicks newTime)
{
	// Get time since start of day

	GameTicks oldTime = ticks;

	// Set new time

	ticks = newTime;

	// Adjust Day if goes over day

	if(newTime < oldTime)
		days++;
}

/*
 * Same as above. but leaves date the same
 */

void TimeBase::setTime(GameTicks newTime)
{
	ticks = newTime;
}

GameTicks TimeBase::getTime()
{
	return ticks;
}

GameTime::GameTime()
{
	setCampaignTime();
}

GameTime::GameTime(Day d, GameTicks t) : TimeBase(d, t)
{
	setCampaignTime();
}

void GameTime::setCampaignTime()
{
#if !defined(CAMPEDIT) && !defined(BATEDIT)
	Realism rlevel = game->getDifficulty( Diff_CommandControl );

	timeRate = (24 * 60 / ( 25 - ( rlevel * 5 ) ) );  // 1 second = 5 minutes @ highest level of difficulty
#else
	timeRate = 24 * 60 / 5;  // 1 second = 5 minutes
#endif
}

void TimeBase::addGameTicks(GameTicks t)
{
	ticks += t;
	while(ticks >= GameTicksPerDay)
	{
		days++;
		ticks -= GameTicksPerDay;
	}

}

const TimeBase& TimeBase::operator -= (const TimeBase& t)
{
	while(ticks < t.ticks)
	{
		days--;
		ticks += GameTicksPerDay;
	}

	ticks -= t.ticks;
	days -= t.days;

	return *this;
}


const TimeBase& TimeBase::operator -= (GameTicks t)
{
	while(ticks < t)
	{
		days--;
		ticks += GameTicksPerDay;
	}

	ticks -= t;

	return *this;
}


const TimeBase& TimeBase::operator += (GameTicks t)
{
	ticks += t;
	while(ticks >= GameTicksPerDay)
	{
		days++;
		ticks -= GameTicksPerDay;
	}
	return *this;
}



int TimeBase::operator <= (const TimeBase& t)
{
	if(days < t.days)
		return 1;
	else if(days > t.days)
		return 0;
	else	// equal
		return ticks <= t.ticks;
}

int TimeBase::operator > (const TimeBase& t)
{
	if(days > t.days)
		return 1;
	else if(days < t.days)
		return 0;
	else	// equal
		return ticks > t.ticks;
}

/*
 * Adjust gametime depending on how many real-time clock ticks have passed
 */


void GameTime::addRealTime(unsigned int timerTicks)
{
	addGameTicks((timerTicks * timeRate * GameTicksPerSecond) / Timer::ticksPerSecond);
}

GameTicks GameTime::realToGameTicks(unsigned int timerTicks)
{
	return (timerTicks * timeRate * GameTicksPerSecond) / Timer::ticksPerSecond;
}


/*
 * Convert Date into GameTimeVal and set it
 */

Day dateToDays(Day d, Month m, Year y)
{
	Day v;

	y -= startYear;
	d--;			// Convert to 0..n instead of 1..

	/*
	 * Adjust day for leap years
	 */

	d += 1 + (y / 4);

	if(((y & 3) == 0) && (m <= February))
		--d;


	v = y * DaysPerYear + (monthDays[m] + d);

	return v;
}


/*
 * Break down time into seperate parts
 */

TimeInfo::TimeInfo(const TimeBase &v)
{
	/*
	 * Seperate into date and time
	 */

	GameTicks time = v.ticks;
	Day date = v.days;

	ticksSinceMidnight = time;

#ifdef DEBUG
	if(ticksSinceMidnight >= GameTicksPerDay)
		GeneralError("Gametime is %ld, which is greater than a day (%ld)",
			ticksSinceMidnight,
			GameTicksPerDay);
#endif

	/*
	 * Seperate the time
	 */

	hour = Hour(time / GameTicksPerHour);
	time = time % GameTicksPerHour;

	minute = Minute(time / GameTicksPerMinute);
	time = time % GameTicksPerMinute;

	second = Second(time / GameTicksPerSecond);

	/*
	 * Seperate the date
	 */

	int daysPerLeapYear = 365*4 + 1;
	// int leapYears = (date + daysPerLeapYear - monthDays[March] - 1) / daysPerLeapYear;

	date += 365*3;		// Round up so leap year is at end of 4 years
	int leapYears = date / daysPerLeapYear;

	date -= leapYears * daysPerLeapYear;

	// date -= leapYears;

	year = date / 365;
	if(year >= 4)
		year = 3;
	date -= year * 365;
	year = year + leapYears * 4 + startYear - 3;

	// year = Year(date / 365 + startYear + leapYears * 4 - 3);
	// date = date % 365;

	Boolean isLeapYear = ((year & 3) == 0);
	Boolean afterLeap = (isLeapYear && (date > monthDays[March]));

	if(isLeapYear && (date == monthDays[March]))
	{
		day = 29;
		month = February;
	}
	else
	{
		if(afterLeap)
		 	date--;

		month = January;
		while((month < December) && (date >= monthDays[month+1]))
			month = Month(month + 1);

		day = Day(date - monthDays[month] + 1);
	}

	strncpy( monthStr, language( monthNames[month] ), sizeof(monthStr)-1 );
	monthStr[sizeof(monthStr)-1] = 0;
}

void TimeInfo::setRealTime()
{
	time_t t;
	time(&t);
	struct tm* tm = localtime(&t);

	day = tm->tm_mday;
	month = Month(tm->tm_mon);
	year = tm->tm_year;
	hour = tm->tm_hour;
	minute = tm->tm_min;
	second = tm->tm_sec;

	// monthStr = language( monthNames[month] );

	strncpy( monthStr, language( monthNames[month] ), sizeof(monthStr)-1 );
	monthStr[sizeof(monthStr)-1] = 0;

	ticksSinceMidnight = timeToTicks(hour, minute, second);	// ((hour * 60 + minute) * 60 + second) * GameTicksPerSecond;
}
