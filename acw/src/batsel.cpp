/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Choose Battle Screen
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>		// Needed for FILENAME_MAX
#include <conio.h>

#include "batsel.h"
#include "battle.h"
#include "dialogue.h"
#include "filesel.h"
#include "batldata.h"
#include "batfile.h"

#include "system.h"
#include "screen.h"
#include "mouselib.h"
#include "game.h"

#include "generals.h"
#include "language.h"
#include "memptr.h"
#include "campbatl.h"
#include "calcbat.h"
#include "text.h"
#include "mp_file.h"
// #include "strutil.h"
// #include "darken.h"
#include "colours.h"
#include "timer.h"
#include "connect.h"

#ifdef DEBUG
#include "batsetup.h"
#endif

/*
 * Ask player for battle to play and play it
 * Returns False if user cancelled, True if game was played
 */

class HistoricBattleControl {
public:
	MarkedBattle* bat;

	HistoricBattleControl()
	{
		bat = 0;
	}

	~HistoricBattleControl()
	{
		delete bat;
		delete battle;
		battle = 0;
	}
};


Boolean playHistoricalBattle()
{
	MemPtr<char> fullFileName(FILENAME_MAX);

	fullFileName[0] = 0;

	if(!getFileName(fullFileName, language(lge_LoadHisBat), "gamedata\\*.btl", True))
		return False;

	HistoricBattleControl control;

	battle = new BattleData(0);		// Make battle with own OB

	readHistoricalBattle(fullFileName, battle);

	/*
	 * Make up a marked battle list
	 */

	control.bat = makeMarkedBattle(battle->ob);

	/*
	 * Run the Battle
	 */

	runBattle(control.bat, True);	// Run battle without deploying

	/*
	 * Remove the marked battle list
	 * (automatically done in control destructor
	 */

	// delete bat;
	// delete battle;
	// battle = 0;

	return True;
}
 

/*
 * Decide what type of battle to play
 */

void chooseBattle()
{
#if defined(DEBUG) && !defined(TESTBATTLE)
	if(!game->connection)
	{
		static char battleChoiceStr[] = "Do you want to play\ra historical battle\ror\ra generated scenario";
		static char battleChoiceButtonStr[] = "Historical|Generated|Cancel";


		/*
	 	 * 1st Choice is between historical or generated
	 	 */

		for(;;)
		{
			switch(dialAlert(0, battleChoiceStr, battleChoiceButtonStr))
			{
			case 0:	// Historical
				if(playHistoricalBattle())
					return;
				break;

			case 1:	// Generated
				// To Be Written... selection screen for parameters
				playGeneratedBattle();
				return;

			case 2:	// Cancel... go back to wherever we were called from!
				return;
			}
		}
	}
	else
		playHistoricalBattle();
#else
	playHistoricalBattle();
#endif
}


/*
 * Play battle and deal with end screens and results
 *
 * Returns reason why battle finished, which might be one of
 * the MenuData::MenuMode values or one of the battle reasons.
 * Normally this would only be used to check for return to main menu,
 * etc
 */

int runBattle(MarkedBattle* bat, Boolean noDeploy)
{
	/*
	 * Calculate pre-battle values
	 */

	PreBattleInfo battleInfo;
	battleInfo.make(bat);

	/*
	 * Play the battle and note the reason why it finished
	 */

	int battleResult = battle->play(noDeploy);

	if( (battleResult == BattleEndDay) ||
	    (battleResult == BattleNoTroops) ||
		 (battleResult == BattleSurrender) )
	{
		/*
		 * We've got a real reason to finish, so the results will
		 * need calculating or analysing.
		 */

		BattleResult results;

		results.make(bat, &battleInfo, battleResult == BattleSurrender);

		/*
		 * Place up the correct winning screen
		 */

		Side picSide = results.winner;

#if 0
		if(picSide == SIDE_None)
			picSide = game->playersSide;
#endif

		Rect titleR;
		Rect bodyR;
		Colour titleColour;
		Colour titleShadow;
		Colour bodyColour;
		Colour bodyShadow;


		if(picSide == SIDE_USA)
		{
			showFullScreen("art\\screens\\victoryu.lbm");
			titleR = Rect(286, 4, 350, 108);
			bodyR = Rect(0, 350, 640, 130);
			titleColour = Black;
			titleShadow = White;

			bodyColour = White;
			bodyShadow = Black;
		}
		else if(picSide == SIDE_CSA)
		{
			showFullScreen("art\\screens\\victoryc.lbm");
			titleR = Rect(252, 4, 384, 108);
			bodyR = Rect(4, 406, 632, 72);
			titleColour = Black;
			titleShadow = White;
			bodyColour = White;
			bodyShadow = Black;
		}
		else
		{
			showFullScreen("art\\screens\\victoryn.lbm");	// Neutral
			titleR = Rect(4, 4, 632, 108);
			bodyR = Rect(4, 403, 632, 74);
			titleColour = Black;
			titleShadow = White;
			bodyColour = White;
			bodyShadow = Black;
		}

		// machine.screen->update();
		// timer->wait(timer->ticksPerSecond);

		/*
		 * Put text on top
		 *		Title
		 *		Body
		 *		Button
		 */

		// Colour* gTable = makeGreyTable(machine.screen->getDeferredPalette());

		Region r = Region(machine.screen->getImage(), titleR);

		// r.remap(Rect(0,0,r.getW(),r.getH()), gTable);

		MemPtr<char> buffer(1000);
		MemPtr<char> buffer2(1000);
		TextWindow wind(&r, Font_JAME15);
		wind.setHCentre(True);
		wind.setVCentre(False);
		wind.setWrap(True);
		wind.setColours(titleColour);
		wind.setShadow(titleShadow);

		results.getTitle(buffer);
		// wind.setPosition(Point(0, 10));
		wind.draw(buffer);

		// machine.screen->setUpdate(titleR);
		// machine.screen->update();
		// timer->wait(timer->ticksPerSecond);


		wind.setClip(bodyR);
		// r.remap(Rect(0,0,r.getW(),r.getH()), gTable);
		wind.setHCentre(True);
		wind.setVCentre(True);
		wind.setWrap(True);
		wind.setColours(bodyColour);
		wind.setShadow(bodyShadow);
		wind.setFont(Font_EMFL10);

		results.getBody(buffer);
		results.getStats(buffer2);
		wind.wprintf("%s\r\r%s", (char*)buffer, (char*)buffer2);

		/*
		 * Update screen
		 */

		machine.mouse->setPointer(M_Arrow);
		machine.screen->update();

		/*
		 * Wait for half a second and clear mouse event queue
		 * to prevent accidently clicking past this screen
		 */

		timer->wait(timer->ticksPerSecond / 2);	// Wait half a second
		machine.mouse->clearEvents();				// Clear any mouse presses

		/*
		 * Wait for button to be pressed
		 */

		// machine.mouse->waitEvent();

		MenuConnection connect("EndBattle");
		connect.processMessages(True);
		while(!kbhit() && !machine.mouse->getEvent())
		{
			connect.processMessages(False);
		}

		if(kbhit())
		{
			if(getch() == 0)
				getch();
		}
		connect.processMessages(True);

		machine.mouse->setPointer(M_Busy);
	}

	return battleResult;

}



