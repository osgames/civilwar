/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	3D Map Implementation
 *
 * 3rd Complete restart... This time with seperate grids for different
 * zoom levels.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.22  1994/08/24  15:05:41  Steven_Green
 * *** empty log message ***
 *
 *
 *----------------------------------------------------------------------
 */

#include "map3d.h"
#include "data3d.h"
#include "colours.h"
#include "game.h"

#include "batldata.h"
#include "sprlist.h"
#include "staticob.h"
#include "earth.h"
#include "language.h"
#include "dialogue.h"

#ifdef DEBUG
#include "memptr.h"
#endif


#ifdef LOG_GRID
#include "log.h"
#endif

/*
 * Data types and tables
 */

static SmoothMatrix smooth_even = {
	1,1,1,
	1,1,1,
	1,1,1
};

enum Axis { Up, Left, Down, Right };
enum CurveBits {
	B_Up = 8,
	B_Left = 4,
	B_Down = 2,
	B_Right = 1
};


/*
 * Order of tiles for rivers and roads
 */


enum CurveType {
	CURVE_NONE,			// Solid block of river/road

	CURVE_UD,
	CURVE_LR,
	CURVE_UL,
	CURVE_UR,
	CURVE_DL,
	CURVE_DR,

	// T Junctions

	CURVE_UDR,
	CURVE_UDL,
	CURVE_LRU,
	CURVE_LRD,
	CURVE_UDLR,

	// Single entry points

	CURVE_L,
	CURVE_R,
	CURVE_U,
	CURVE_D
};

/*
 * What edges contain water?  Encoded Up=b3, Left=b2, Down=b1, Right=b0
 */

static UBYTE curveEdges[] = {
	0, 10, 5, 12, 9, 6, 3, 11, 14, 13, 7, 15, 4, 1, 8, 2
};

static CurveType curveBlockType[16] = {
	CURVE_NONE,					// Undefined
	CURVE_R,
	CURVE_D,
	CURVE_DR,
	CURVE_L,
	CURVE_LR,
	CURVE_DL,
	CURVE_LRD,
	CURVE_U,
	CURVE_UR,
	CURVE_UD,
	CURVE_UDR,
	CURVE_UL,
	CURVE_LRU,
	CURVE_UDL,
	CURVE_UDLR
};

/*
 * Block to use when moving from one direction to another
 */

static CurveType curveBlock[4][4] = {	// Olddir, newdir
	{ CURVE_UD, CURVE_DL, CURVE_UD, CURVE_DR },	// up -> ULDR
	{ CURVE_UR, CURVE_LR, CURVE_DR, CURVE_LR },	// Left -> ULDR
	{ CURVE_UD, CURVE_UL, CURVE_UD, CURVE_UR },	// down-> ULDR
	{ CURVE_UL, CURVE_LR, CURVE_DL, CURVE_LR }
};

/*=====================================================
 * MapGrid Functions
 */

/*
 * MapGrid Constructor
 *
 * Create space for grids
 */

MapGrid::MapGrid()
{
	/*
	 * Set the battle area size
	 */

	size = DistToBattle(BattleFieldSize);

	/*
	 * Set up the Grids
	 */

	int gCount = 128;						// Number of grids in most zoomed in
	Cord3D gSize = size / gCount;		// Size of a grid
	Grid* g = grids;

	int i = GridCount;
	while(i--)
	{
		g->gridCount = gCount;
		g->pointCount = gCount + 1;
		g->gridSize = gSize;
		g->heights = new Height8[g->pointCount * g->pointCount];
		g->terrains = new TerrainType[gCount * gCount];

		g++;
		gSize <<= 1;		// Double the grid size
		gCount >>= 1;		// Halve the number of grids
	}
}

/*
 * MapGrid Destructor
 */

MapGrid::~MapGrid()
{
}

static GridLevel MapGrid::gridNum[ZOOM_LEVELS] = {
	Grid_128,
	Grid_128,
	Grid_64,
	Grid_32,
	Grid_16
#ifdef BATEDIT
	, Grid_8,
	Grid_4,
	Grid_2
#endif
};

void Grid::clearTerrain(TerrainType t, Boolean riverProt, Boolean roadProt)
{
	for(int y = 0; y < gridCount; y++)
	{
		for(int x = 0; x < gridCount; x++)
		{
			TerrainType* tt = getTSquare(x, y);

			if( (!roadProt || !isRoad(*tt)) &&
			    (!riverProt || !isRiver(*tt)) )
					*tt = t;
		}
	}
	tidy();
}

void Grid::placeTerrain(TerrainType t, MapGridCord x, MapGridCord y, UBYTE size, Boolean riverProt, Boolean roadProt)
{
	int y1 = y - size/2;

	int h = size;
	while(h--)
	{
		if( (y1 >= 0) && (y1 < gridCount))
		{
			int x1 = x - size/2;
			int w = size;
			while(w--)
			{
				if((x1 >= 0) && (x1 < gridCount))
				{
					TerrainType* tt = getTSquare(x1, y1);

					if( (!roadProt || !isRoad(*tt)) &&
			    		(!riverProt || !isRiver(*tt)) )
							*tt = t;
				}
				x1++;
			}
		}

		y1++;
	}
}

void Grid::flatten(Cord3D height)
{
	Height8* hp = heights;
	int x = pointCount * pointCount;
	Height8 h;
	h.setHeight(height);

	while(x--)
		*hp++ = h;
}

/*
 * Flatten an area around a given point
 *
 * Modified algorithm:
 *  centre point is set to h
 *  Other points are set to average of h and what they were before
 */

void Grid::flatten(int x, int z, UBYTE h, int radius)
{
	int minX = x - radius;
	int minZ = z - radius;

	int maxX = x + radius + 1;
	int maxZ = z + radius + 1;

	Height8* hp = getHSquare(minX, minZ);

	while( (minZ <= maxZ) && (minZ < gridCount) )
	{
		if(minZ >= 0)
		{
			int x1 = minX;

			Height8* hp1 = hp;

			while( (x1 <= maxX) && (x1 < gridCount))
			{
				if(x1 >= 0)
				{
					if( ((x1 == x) || (x1 == (x+1))) &&
						 ((minZ == z) || (minZ == (z+1))) )
					{
						hp1->height = h;
					}
					else
						hp1->height = ((int)hp1->height + h) / 2;
				}

				x1++;
				hp1++;
			}
		}

		hp += pointCount;
		minZ++;
	}
}

void Grid::roughen(UBYTE amount)
{
	for(int y = 0; y < pointCount; y++)
	{
		Boolean lastRow = (y == (gridCount - 1));

		for(int x = 0; x < pointCount; x++)
		{
			int dH = game->gameRand(amount * 2) - amount;
			if(dH < 0)
				adjustHeight(x, y, -dH, False);
			else
				adjustHeight(x, y, dH, True);
		}
	}

	flattenRivers();
}

/*
 * Make a random hill at given location and height
 *
 * For Battle Editor:
 *   raise = True : Raise landscape
 * 			 False : Lower Landscape
 */

#ifdef BATEDIT
void Grid::makeHill(int x, int z, UBYTE height, int radius, Boolean raise)
#else
void Grid::makeHill(int x, int z, UBYTE height, int radius)
#endif
{
	struct LineElement {
		int z1;
		int z2;
		UBYTE h1;
		UBYTE h2;
	};

	LineElement lineStack[10];
	int n = 0;

	/*
	 * Push 2 lines onto stack
	 */

	lineStack[n].z1 = z + radius;
	lineStack[n].h1 = 0;
	lineStack[n].z2 = z;
	lineStack[n].h2 = height;
	n++;

	lineStack[n].z1 = z - radius;
	lineStack[n].h1 = 0;
	lineStack[n].z2 = z;
	lineStack[n].h2 = height;
	n++;

#ifdef BATEDIT
	makeHillLine(x, z, height, radius, raise);
#else
	makeHillLine(x, z, height, radius);
#endif

	while(n > 0)
	{
		LineElement line = lineStack[--n];

		z = (line.z1 + line.z2) / 2;
		height = (line.h1 + line.h2) / 2;
		int dH = abs(line.h1 - line.h2);
		height += game->gameRand(dH) - dH/2;

		if( (z >= 0) && (z < pointCount) )
#ifdef BATEDIT
			makeHillLine(x, z, height, radius, raise);
#else
			makeHillLine(x, z, height, radius);
#endif

		if(abs(line.z1 - z) > 1)
		{
			lineStack[n].z1 = line.z1;
			lineStack[n].z2 = z;
			lineStack[n].h1 = line.h1;
			lineStack[n].h2 = height;
			n++;
		}
		if(abs(line.z2 - z) > 1)
		{
			lineStack[n].z1 = z;
			lineStack[n].z2 = line.z2;
			lineStack[n].h1 = height;
			lineStack[n].h2 = line.h2;
			n++;
		}
	}

#ifdef BATEDIT
	flattenRivers();
#endif
}

/*
 * Add a single line of hill
 */

#ifdef BATEDIT
void Grid::makeHillLine(int x, int z, UBYTE height, int radius, Boolean raise)
#else
void Grid::makeHillLine(int x, int z, UBYTE height, int radius)
#endif
{
	if( (z < 0) || (z >= pointCount))
		return;

	struct RowElement {
		int x1;
		int x2;
		int h1;
		int h2;
	};

	RowElement rowStack[10];
	int n = 0;

	rowStack[n].x1 = x + radius;
	rowStack[n].h1 = 0;
	rowStack[n].x2 = x;
	rowStack[n].h2 = height;
	n++;

	rowStack[n].x1 = x - radius;
	rowStack[n].h1 = 0;
	rowStack[n].x2 = x;
	rowStack[n].h2 = height;
	n++;

	if( (x >= 0) && (x < pointCount))
#ifdef BATEDIT
		adjustHeight(x, z, height, raise);
#else
		setMaxHeight(x, z, height);
#endif

	while(n > 0)
	{
		RowElement row = rowStack[--n];	// Structure copy

		x = (row.x1 + row.x2) / 2;
		height = (row.h1 + row.h2) / 2;
		int dH = abs(row.h1 - row.h2);
		height += game->gameRand(dH) - dH/2;

		if( (x >= 0) && (x < pointCount))
#ifdef BATEDIT
			adjustHeight(x, z, height, raise);
#else
			setMaxHeight(x, z, height);
#endif

		if(abs(row.x1 - x) > 1)
		{
			rowStack[n].x1 = row.x1;
			rowStack[n].x2 = x;
			rowStack[n].h1 = row.h1;
			rowStack[n].h2 = height;
			n++;
		}

		if(abs(row.x2 - x) > 1)
		{
			rowStack[n].x1 = x;
			rowStack[n].x2 = row.x2;
			rowStack[n].h1 = height;
			rowStack[n].h2 = row.h2;
			n++;
		}
	}
}

void Grid::makeRiver()
{
	RiverMakeStruct rm;

	Height8* hp = heights;
	int x = pointCount * pointCount;
	UBYTE minHeight = 0xff;

	while(x--)
	{
		UBYTE newHeight = hp++->height;

		if(newHeight < minHeight)
			minHeight = newHeight;
	}

	rm.x1 = game->gameRand(gridCount / 2) + gridCount / 4;
	rm.y1 = 0;
	rm.x2 = game->gameRand(gridCount / 2) + gridCount / 4;
	rm.y2 = gridCount;
	rm.height = minHeight;
	rm.plainWidth = 8;
	rm.curvature = 60;

	makeRiver(&rm);
}


/*
 * Create a road between 2 points
 *
 * This is similar to the river generation, except:
 *   Roads can cross other roads
 *   Roads can cross rivers, but the terrain is not actually changed
 *   Roads do not flatten landscape, but they can adjust the heights so
 *     that the tile that the road is on is forced to be flat in the 
 *     direction of the road.
 */

void Grid::makeRoad(Cord3D startX, Cord3D startZ, Cord3D endX, Cord3D endZ, UBYTE curvature)
{
	UBYTE prevDirection;
	
	/*
	 * Limit curvature
	 */

	if(curvature < 4)
		curvature = 4;


	int deltaX = endX - startX;
	int deltaZ = endZ - startZ;
	int adx = abs(deltaX);
	int adz = abs(deltaZ);

	if(adz >= adx)
		prevDirection = (deltaZ < 0) ? Down : Up;
	else 
		prevDirection = (deltaX < 0) ? Left : Right;

	while( (startX != endX) || (startZ != endZ) )
	{
		/*
		 * Work out ideal direction to end point
		 */

		deltaX = endX - startX;
		deltaZ = endZ - startZ;
		adx = abs(deltaX);
		adz = abs(deltaZ);

		Axis direction;		// Primary direction
		Axis direction2;		// Secondary direction

		if(adz >= adx)
		{
			direction = (deltaZ < 0) ? Down : Up;
			direction2 = (deltaX < 0) ? Left : Right;
		}
		else 
		{
			direction = (deltaX < 0) ? Left : Right;
			direction2 = (deltaZ < 0) ? Down : Up;
		}

		/*
		 * If only one square away then may as well stop
		 */

		int newX;
		int newZ;
		UBYTE newDirection;

		if( ((adx == 0) && (adz == 1)) ||
		    ((adz == 0) && (adx == 1)) )
		{
			newX = endX;
			newZ = endZ;
			newDirection = direction;
		}
		else
		{
		
			/*
			 * Pick random direction biased towards given direction
			 */

			// do
			// {
#if 0
				newDirection = game->gameRand(100);
#else
				newDirection = game->gameRand(150);
#endif

				/*
			 	 * Disallow looping back on oneself
			 	 */

				if (prevDirection == ( (direction + 2) & 3))
					newDirection = direction2;
				else
				{
#if 0
					if(newDirection < 80)			// 80% of going in same direction
						newDirection = prevDirection;
					else
					if(newDirection < 90)			// 10% chance of heading towards destination
						newDirection = direction;
					else if(newDirection < 95)			// 5% chance left of where its going
						newDirection = (direction + 1) & 3;
					else
						newDirection = (direction -1) & 3;
#else
					if(newDirection > curvature)
						newDirection = direction;
					else
					if(newDirection > (curvature/2))
						newDirection = prevDirection;
					else if(newDirection >= (curvature/4))
						newDirection = (direction + 1) & 3;
					else
						newDirection = (direction -1) & 3;
#endif
				}


				newX = startX;
				newZ = startZ;

				if(newDirection == Up)
					newZ++;
				else if(newDirection == Down)
					newZ--;
				else if(newDirection == Left)
					newX--;
				else
					newX++;
			// }
			// while( (newX < 0) || (newX >= gridCount) ||
			//	 	(newZ < 0) || (newZ >= gridCount));
		}

		/*
		 * Place previous chunk of road down if its on the map
		 */

#if 0
		if( (startX >= 0) && (startX < gridCount) && 
			 (startZ >= 0) && (startZ < gridCount) )
#else
		if(isValidTCord(startX, startZ))
#endif
		{
			TerrainType* t = getTSquare(startX, startZ);

			if(!isRiver(*t))
			{
				CurveType newBlock = curveBlock[prevDirection][newDirection];

				if(isRoad(*t))
				{
					UBYTE edge = curveEdges[newBlock];

					if(isRoad1(*t))
						edge |= curveEdges[*t - Road1Terrain];
					else	// Must be Road2
						edge |= curveEdges[*t - Road2Terrain];

					newBlock = curveBlockType[edge];
				}

				*t = newBlock + Road1Terrain;

				averageSquare(startX, startZ);	// Attempt to flatten
			}
		}

		prevDirection = newDirection;

		startX = newX;
		startZ = newZ;
	}

#ifdef BATEDIT
	tidyRoads();
#endif

}

void Grid::tidy()
{
	tidyRoads();
	tidyRivers();
	flattenRivers();
}

void Grid::tidyRoads()
{
	for(int y = 0; y < gridCount; y++)
	{
		for(int x = 0; x < gridCount; x++)
		{
			/*
			 * Build bitmap of surrounding river bits
			 */

			TerrainType* t = getTSquare(x, y);

			if(isRoad(*t))
			{
				UBYTE bitMask = 0;

				if( (x > 0) && isRoad(t[-1]))
					bitMask |= B_Left;
				if( (x < (gridCount - 1)) && isRoad(t[1]))
					bitMask |= B_Right;
				if( (y > 0) && isRoad(t[-gridCount]))
					bitMask |= B_Down;
				if( (y < (gridCount - 1)) && isRoad(t[gridCount]))
					bitMask |= B_Up;

				TerrainType baseType;

				if(isRoad1(*t))
					baseType = Road1Terrain;
				else
					baseType = Road2Terrain;

				*t = curveBlockType[bitMask] + baseType;
			}
		}
	}
}



/*
 * Tidy Up river by replacing sections based on surrounding areas
 */

void Grid::tidyRivers()
{
	for(int y = 0; y < gridCount; y++)
	{
		for(int x = 0; x < gridCount; x++)
		{
			/*
			 * Build bitmap of surrounding river bits
			 */

			TerrainType* t = getTSquare(x, y);

			if(isRiver(*t))
			{
				UBYTE bitMask = 0;

				if( (x > 0) && isWater(t[-1]))
					bitMask |= B_Left;
				if( (x < (gridCount - 1)) && isWater(t[1]))
					bitMask |= B_Right;
				if( (y > 0) && isWater(t[-gridCount]))
					bitMask |= B_Down;
				if( (y < (gridCount - 1)) && isWater(t[gridCount]))
					bitMask |= B_Up;

				/*
				 * Enhancement so that solid areas of river are filled in
				 */

				static UBYTE bitCounts[16] = {
					0, 1, 1, 2, 1, 2, 2, 3,
					1, 2, 2, 3, 2, 3, 3, 4
				};

				if(bitCounts[bitMask] >= 3)
				{
					UBYTE mask2 = 0;

					if( (x < (gridCount - 1)) && (y < (gridCount - 1)) && isWater(t[gridCount + 1]))
						mask2 |= 1;
					if( (x < (gridCount - 1)) && (y > 0) && isWater(t[-gridCount + 1]) )
						mask2 |= 2;
					if( (x > 0) && (y > 0) && isWater(t[-gridCount - 1]) )
						mask2 |= 4;
					if( (x > 0) && (y < (gridCount - 1)) && isWater(t[gridCount - 1]))
						mask2 |= 8;

					static UBYTE pairBits[16] = {
						0, 0, 0, 1, 0, 0, 1, 1,
						0, 1, 0, 1, 1, 1, 1, 1
					};

					static UBYTE matchPair[16] = {
						0, 0, 0, 0, 0, 0, 0, 6,
						0, 0, 0, 3, 0, 9, 12, 15
					};

					if(bitMask == 15)
					{
						if(pairBits[mask2])
							bitMask = 0;
					}
					else if( (mask2 & matchPair[bitMask]) == matchPair[bitMask])
							bitMask = 0;
				}

				*t = curveBlockType[bitMask] + RiverTerrain;
			}
		}
	}
}


/*
 * Reflatten rivers
 */

void Grid::flattenRivers()
{
	UBYTE riverHeight = 0xff;
	UBYTE lowest = 0xff;
	Boolean foundRiver = False;

	for(int y = 0; y < gridCount; y++)
	{
		for(int x = 0; x < gridCount; x++)
		{
			TerrainType t = *(getTSquare(x, y));
			if(isWater(t))
			{
				foundRiver = True;

				UBYTE h = getHSquare(x, y)->height;
				if(h < riverHeight)
					riverHeight = h;
			}
			else if(!foundRiver)
			{
				UBYTE h = getHSquare(x, y)->height;
				if(h < lowest)
					lowest = h;
			}
		}
	}

	if(!foundRiver)
		riverHeight = lowest;

	for(y = 0; y < gridCount; y++)
	{
		for(int x = 0; x < gridCount; x++)
		{
			TerrainType t = *(getTSquare(x, y));
			if(isWater(t))
				flatten(x, y, riverHeight, 0);
			else if(isRoad(t) || (t == BuiltupTerrain))
				averageSquare(x, y);

		}
	}
}


void Grid::setMaxHeight(int x, int z, UBYTE newHeight)
{
	/*
	 * Added check to make sure don't corrupt memory.
	 */

	// if( (x < 0) || (x >= pointCount) || (z < 0) || (z >= pointCount))
	if(!isValidHCord(x, z))
		return;

	Height8* h = getHSquare(x, z);

	if(newHeight > h->height)
		h->height = newHeight;
}


void Grid::adjustHeight(int x, int z, UBYTE adjust, Boolean raise)
{
	/*
	 * Added check to make sure don't corrupt memory.
	 */

	// if( (x < 0) || (x >= pointCount) || (z < 0) || (z >= pointCount))
	if(!isValidHCord(x, z))
		return;

	Height8* h = getHSquare(x, z);

	int nh = h->height;

	if(raise)
	{
		nh += adjust;
		if(nh > 0xff)
			nh = 0xff;
	}
	else
	{
		nh -= adjust;
		if(nh < 0)
			nh = 0;
	}

	h->height = nh;

}

void Grid::makeRiver(RiverMakeStruct* rm)
{
	if((rm->x1 == rm->x2) && (rm->y1 == rm->y2))
		return;

	// Pickend place on z=0 row in middle half

	int riverX = rm->x1;
	int riverZ = rm->y1;

	// Pick a destination (Just off the grid)

	int riverEndX = rm->x2;
	int riverEndZ = rm->y2;

	UBYTE riverHeight = rm->height;

	UBYTE prevDirection = Up;	// This should be calculated

	int deltaX = riverEndX - riverX;
	int deltaZ = riverEndZ - riverZ;
	int adx = abs(deltaX);
	int adz = abs(deltaZ);

	// Axis direction;		// Primary direction
	// Axis direction2;		// Secondary direction

	if(adz >= adx)
		prevDirection = (deltaZ < 0) ? Down : Up;
	else 
		prevDirection = (deltaX < 0) ? Left : Right;

	/*
	 * 	Found instance where the while loop below was never
	 *		exited from. Hence added loopCounter. CJW
	 */

	int loopCounter = 0;

	while( (riverX != riverEndX) || (riverZ != riverEndZ) )
	{
		loopCounter++;

		if ( loopCounter > 2000 ) break;

		/*
		 * Set height around river to riverHeight.
		 */

		flatten(riverX, riverZ, riverHeight, game->gameRand(rm->plainWidth));

		/*
		 * Work out ideal direction to end point
		 */

		int deltaX = riverEndX - riverX;
		int deltaZ = riverEndZ - riverZ;
		int adx = abs(deltaX);
		int adz = abs(deltaZ);

		Axis direction;		// Primary direction
		Axis direction2;		// Secondary direction

		if(adz >= adx)
		{
			direction = (deltaZ < 0) ? Down : Up;
			direction2 = (deltaX < 0) ? Left : Right;
		}
		else 
		{
			direction = (deltaX < 0) ? Left : Right;
			direction2 = (deltaZ < 0) ? Down : Up;
		}

		UBYTE newDirection;

		int newX;
		int newZ;

		/*
		 * If only one square away then may as well stop
		 */

		if( ((adx == 0) && (adz == 1)) ||
		    ((adz == 0) && (adx == 1)) )
		{
			newX = riverEndX;
			newZ = riverEndZ;
			newDirection = direction;
		}
		else
		{
		
			/*
		 	 * Pick random direction biased towards given direction
		 	 */

			int loopCount = 200;		// Maximum times around this loop

			do
			{
				newDirection = game->gameRand(150);

				/*
			 	 * Disallow looping back on oneself
			 	 */

				if (prevDirection == ( (direction + 2) & 3))
					newDirection = direction2;
				else if(adx == adz)	// Special case for 45 degrees
				{
					if(newDirection < 50)
						newDirection = direction;
					else
						newDirection = direction2;
				}
				else
				{
					if(newDirection >= rm->curvature)
						newDirection = direction;
					else if(newDirection >= (rm->curvature / 2))
						newDirection = direction2;
					else if(newDirection >= (rm->curvature / 8))
						newDirection = (direction2 + 2) & 3;
					else
						newDirection = (direction + 2) & 3;
				}


				newX = riverX;
				newZ = riverZ;

				if(newDirection == Up)
					newZ++;
				else if(newDirection == Down)
					newZ--;
				else if(newDirection == Left)
					newX--;
				else
					newX++;
			}
			while( (!isValidTCord(newX, newZ) ||
		 			  isWater(*(getTSquare(newX, newZ)))) &&
					 loopCount--);
		}

		/*
		 * Place previous chunk of river down
		 */

#if 0
		if( (riverX >= 0) && (riverX < gridCount) &&
			 (riverZ >= 0) && (riverZ < gridCount) )
#else
		if(isValidTCord(riverX, riverZ))
#endif
		{
			*(getTSquare(riverX, riverZ)) = curveBlock[prevDirection][newDirection] + RiverTerrain;
		}

		prevDirection = newDirection;

		riverX = newX;
		riverZ = newZ;
	}

#ifdef BATEDIT
	tidyRivers();
#endif
}


/*
 * Smooth a grid with a given 3 by 3 matrix
 */

void Grid::smooth(const SmoothMatrix& sf)
{
	/*
	 * Slower but simpler to understand method
	 */

	for(int y = 0; y < pointCount; y++)
	{
		Boolean lastRow = (y == (pointCount - 1));

		for(int x = 0; x < pointCount; x++)
		{
			Boolean lastCol = (x == (pointCount - 1));
			int count = 1;
			int newHeight = getHSquare(x, y)->height * sf.v[1][1];

			if(x > 0)
			{
				newHeight += getHSquare(x-1, y)->height * sf.v[1][0];
				count++;
			}

			if(!lastCol)
			{
				newHeight += getHSquare(x+1, y)->height * sf.v[1][2];
				count++;
			}

			if(y > 0)
			{
				newHeight += getHSquare(x, y-1)->height * sf.v[0][1];
				count++;

				if(x > 0)
				{
					newHeight += getHSquare(x-1, y-1)->height * sf.v[0][0];
					count++;
				}

				if(!lastCol)
				{
					newHeight += getHSquare(x+1, y-1)->height * sf.v[0][2];
					count++;
				}
			}

			if(!lastRow)
			{
				newHeight += getHSquare(x, y+1)->height * sf.v[2][1];
				count++;

				if(x > 0)
				{
					newHeight += getHSquare(x-1, y+1)->height * sf.v[2][0];
					count++;
				}

				if(!lastCol)
				{
					newHeight += getHSquare(x+1, y+1)->height * sf.v[2][2];
					count++;
				}
			}


			getHSquare(x, y)->height = newHeight / count;
		}
	}

	flattenRivers();
}

void Grid::smooth()
{
	smooth(smooth_even);
}

/*
 * Set all points in a square to the same height
 * We know that x and z are valid
 *
 * Its not the average... it is in fact the minimum!
 */

void Grid::averageSquare(int x, int z)
{
	Height8* hp = getHSquare(x, z);

#if 1
	int best = (hp[0].height + hp[1].height + hp[pointCount].height + hp[pointCount+1].height) / 4;
#else
	UBYTE best = hp[0].height;
	if(hp[1].height < best)
		best = hp[1].height;
	if(hp[pointCount].height < best)
		best = hp[pointCount].height;
	if(hp[pointCount+1].height < best)
		best = hp[pointCount+1].height;
#endif

	hp[0].height = best;
	hp[1].height = best;
	hp[pointCount].height = best;
	hp[pointCount + 1].height = best;
}

/*
 * Make a half size grid from another
 */

void MapGrid::makeHalfGrid(System3D& drawData, Grid* dest, Grid* src)
{
	/*
	 * Do the heights first
	 */

	Height8* destH = dest->heights;
	Height8* srcH = src->heights;

	int srcW = src->pointCount;
	int destW = dest->pointCount;

	/*
	 * A bit more complicated than above because point grid has odd width
	 * Do it by averaging the 9 points around the new grid point
	 *
	 *   + + +
	 *   + + +
	 *   + + +
	 */

	// Special case for 1st row

	Height8* sh = srcH;
	Height8* nh = srcH + srcW;
	Height8* lh = 0;

	destH++->height = ((int) sh[0].height + sh[1].height + nh[0].height + nh[1].height) / 4;
	sh += 2;
	nh += 2;

	int xCount = destW - 2;
	while(xCount--)
	{
		destH++->height =
			((int) sh[-1].height + sh[0].height + sh[1].height +
			       nh[-1].height + nh[0].height + nh[1].height) / 6;
		sh += 2;
		nh += 2;
	}

	destH++->height =
			((int) sh[-1].height + sh[0].height +
			       nh[-1].height + nh[0].height) / 4;

	srcH += srcW * 2;

	/*
	 * Middle Rows
	 */

	int yCount = destW - 2;
	while(yCount--)
	{
		sh = srcH;
		nh = srcH + srcW;
		lh = srcH - srcW;

		destH++->height =
			((int) sh[0].height + sh[1].height +
					 nh[0].height + nh[1].height +
					 lh[0].height + lh[1].height) / 6;
		sh += 2;
		nh += 2;
		lh += 2;

		int xCount = destW - 2;
		while(xCount--)
		{
			destH++->height =
			((int) sh[-1].height + sh[0].height + sh[1].height +
			       nh[-1].height + nh[0].height + nh[1].height +
			       lh[-1].height + lh[0].height + lh[1].height) / 9;

			sh += 2;
			nh += 2;
			lh += 2;
		}

		destH++->height =
			((int) sh[0].height + sh[-1].height +
					 nh[0].height + nh[-1].height +
					 lh[0].height + lh[-1].height) / 6;

		srcH += srcW * 2;
	}


	// Special case for last row

	destH++->height = ((int) sh[0].height + sh[1].height + lh[0].height + lh[1].height) / 4;
	sh += 2;
	lh += 2;

	xCount = destW - 2;
	while(xCount--)
	{
		destH++->height =
			((int) sh[-1].height + sh[0].height + sh[1].height +
			       lh[-1].height + lh[0].height + lh[1].height) / 6;
		sh += 2;
		lh += 2;
	}

	destH->height =
			((int) sh[-1].height + sh[0].height +
			       lh[-1].height + lh[0].height) / 4;
	/*
	 * Do the terrains
	 */

	TerrainType* destT = dest->terrains;
	TerrainType* srcT = src->terrains;

	srcW = src->gridCount;
	destW = dest->gridCount;

	yCount = destW;
	while(yCount--)
	{
		int xCount = destW;
		while(xCount--)
		{
			/*
			 * The most common terrain type is used
			 *
			 * except Water which must overide the rest!
			 *
			 * This is ugly code!
			 */

			TerrainType t1 = srcT[0];
			TerrainType t2 = srcT[1];
			TerrainType t3 = srcT[srcW];
			TerrainType t4 = srcT[srcW+1];


			if(isRiver(t1) || isRiver(t2) || isRiver(t3) || isRiver(t4))
			{
				UBYTE edge = 0;

				if(isRiver(t1))
					edge |= curveEdges[t1 - RiverTerrain] & (B_Down | B_Left);
				if(isRiver(t2))
					edge |= curveEdges[t2 - RiverTerrain] & (B_Down | B_Right);
				if(isRiver(t3))
					edge |= curveEdges[t3 - RiverTerrain] & (B_Up | B_Left);
				if(isRiver(t4))
					edge |= curveEdges[t4 - RiverTerrain] & (B_Up | B_Right);

				*destT++ = curveBlockType[edge] + RiverTerrain;
			}
			else if(isRail(t1) || isRail(t2) || isRail(t3) || isRail(t4))
			{
				UBYTE edge = 0;

				if(isRail(t1))
					edge |= curveEdges[t1 - RailwayTerrain] & (B_Down | B_Left);
				if(isRail(t2))
					edge |= curveEdges[t2 - RailwayTerrain] & (B_Down | B_Right);
				if(isRail(t3))
					edge |= curveEdges[t3 - RailwayTerrain] & (B_Up | B_Left);
				if(isRail(t4))
					edge |= curveEdges[t4 - RailwayTerrain] & (B_Up | B_Right);

				*destT++ = curveBlockType[edge] + RailwayTerrain;
			}
			else if(isRoad(t1) || isRoad(t2) || isRoad(t3) || isRoad(t4))
			{
				UBYTE edge = 0;
				UBYTE count1 = 0;
				UBYTE count2 = 0;

				if(isRoad1(t1))
				{
					edge |= curveEdges[t1 - Road1Terrain] & (B_Down | B_Left);
					count1++;
				}
				else if(isRoad2(t1))
				{
					edge |= curveEdges[t1 - Road2Terrain] & (B_Down | B_Left);
					count2++;
				}

				if(isRoad1(t2))
				{
					edge |= curveEdges[t2 - Road1Terrain] & (B_Down | B_Right);
					count1++;
				}
				else if(isRoad2(t2))
				{
					edge |= curveEdges[t2 - Road2Terrain] & (B_Down | B_Right);
					count2++;
				}

				if(isRoad1(t3))
				{
					edge |= curveEdges[t3 - Road1Terrain] & (B_Up | B_Left);
					count1++;
				}
				else if(isRoad2(t3))
				{
					edge |= curveEdges[t3 - Road2Terrain] & (B_Up | B_Left);
					count2++;
				}

				if(isRoad1(t4))
				{
					edge |= curveEdges[t4 - Road1Terrain] & (B_Up | B_Right);
					count1++;
				}
				else if(isRoad2(t4))
				{
					edge |= curveEdges[t4 - Road2Terrain] & (B_Up | B_Right);
					count2++;
				}

				*destT++ = curveBlockType[edge] + ((count1 >= count2) ? Road1Terrain : Road2Terrain);
			}
			else
			{
#if 0
				t1 = drawData.terrain.getBaseTerrain(t1);
				t2 = drawData.terrain.getBaseTerrain(t2);
				t3 = drawData.terrain.getBaseTerrain(t3);
				t4 = drawData.terrain.getBaseTerrain(t4);
#endif

				int count1 = 1;
				int count2 = 1;
				int count3 = 1;

				if(t1 == t2)
					count1++;
				if(t1 == t3)
					count1++;
				if(t1 == t4)
					count1++;
				if(t2 == t3)
					count2++;
				if(t2 == t4)
					count2++;
				if(t3 == t4)
					count3++;

				if( (count1 >= count2) && (count1 >= count3) )
					*destT++ = t1;
				else if( (count2 >= count1) && (count2 >= count3) )
					*destT++ = t2;
				else
					*destT++ = t3;
			}

			srcT += 2;
		}

		srcT += srcW;		// Skip a line
	}
}


void MapGrid::makeZoomedGrids(System3D& drawData)
{
#ifdef OLD_TESTING
	// Remove any illegal terrain types

	Grid* g = &grids[Grid_Min];

	for(int y = 0; y < g->gridCount; y++)
	{
		for(int x = 0; x < g->gridCount; x++)
		{
			TerrainType* t = g->getTSquare(x, y);

			if(*t >= TerrainCount)
				*t = PastureTerrain;
		}
	}

#endif

#ifdef BATEDIT
	grids[Grid_Min].tidy();
#endif

	for(int i = Grid_Min; i < Grid_Max; i++)
		makeHalfGrid(drawData, &grids[i + 1], &grids[i]);
}

#ifdef BATEDIT

/*
 * Create a random Map grid
 */

#ifdef BATEDIT
void MapGrid::create(System3D& drawData, UBYTE hillHeight, int hillRadius)
#else
void MapGrid::create(System3D& drawData)
#endif
{
#ifdef DEBUG
	MemPtr<char> popupBuffer(400);
	strcpy(popupBuffer, language(lge_CreatingBattleField));
	PopupText popup(popupBuffer);
#else
	PopupText popup(language(lge_CreatingBattleField));
#endif

	/*
	 * Create a random landscape
	 *
	 * Algorithm is:
	 *  Fill with random numbers:
	 *  Average each 9x9 area (smoothing)
	 *
	 * Different smoothing algorithms can be used to create different
	 * types of terrain.
	 *   e.g. 1 1 1
	 *        1 1 1 is an average sort of smooth hill
	 *        1 1 1
	 *
	 *        1 1 1
	 *        1 2 1 will generate more variance between points
	 *        1 1 1
	 *
	 *        1 1 1
	 *        1 0 1 will create a very smooth hill.
	 *        1 1 1
	 *
	 * Alternative methods could include a fractal or recursive method
	 *
	 * Or build random hills (pick random point... spread outwards)
	 */

	/*
	 * Fill grid with random values
	 */


	Grid* g = &grids[Grid_Min];		// Fill in the 1st grid

	/*
	 * Make some random hills
	 */

#ifdef DEBUG
	strcat(popupBuffer, "\rHeights");
	popup.showText(popupBuffer);
#endif

	// First flatten the entire area

	Height8* hp = g->heights;
	int x = g->pointCount * g->pointCount;

	while(x--)
		hp++->height = 0;

	// Pick random points and heights ad create hills

	int hillCount = game->gameRand(10) + 5;		// between 5 and 10 hills

	while(hillCount--)
	{
		int hx = game->gameRand(g->pointCount);	// Random location
		int hz = game->gameRand(g->pointCount);

#ifdef BATEDIT
		g->makeHill(hx, hz, hillHeight, hillRadius, True);
#else
		UBYTE ht = game->gameRand() & 0xff;		// Maximum height
		int radius = game->gameRand(g->pointCount/2) + g->pointCount/8;

		g->makeHill(hx, hz, ht, radius);
#endif
	}

	// Determine the lowest point for use by the rivers

	hp = g->heights;
	x = g->pointCount * g->pointCount;
	UBYTE minHeight = 0xff;

	while(x--)
	{
		UBYTE newHeight = hp++->height;

		if(newHeight < minHeight)
			minHeight = newHeight;
	}


	/*
 	 * Make random terrain
	 */


#ifdef DEBUG
	strcat(popupBuffer, "\rTerrain");
	popup.showText(popupBuffer);
#endif

	TerrainType* tp = g->terrains;
	x = g->gridCount * g->gridCount;

	while(x--)
		*tp++ = game->gameRand(NormalTerrainCount) + NormalTerrain;

	/*
	 * Place Some random roads
	 */

#ifdef DEBUG
	strcat(popupBuffer, "\rRoads");
	popup.showText(popupBuffer);
#endif

	g->makeRoad(0, game->gameRand(g->gridCount), g->gridCount, game->gameRand(g->gridCount), 20);	// Across
	g->makeRoad(0, game->gameRand(g->gridCount), g->gridCount, game->gameRand(g->gridCount), 20);	// Across
	g->makeRoad(game->gameRand(g->gridCount), 0, game->gameRand(g->gridCount), g->gridCount, 20);	// Top to bottom
	g->makeRoad(game->gameRand(g->gridCount), 0, game->gameRand(g->gridCount), g->gridCount, 20);	// Top to bottom


	RiverMakeStruct rm;

	rm.x1 = game->gameRand(g->gridCount / 2) + g->gridCount / 4;
	rm.y1 = 0;
	rm.x2 = game->gameRand(g->gridCount / 2) + g->gridCount / 4;
	rm.y2 = g->gridCount;
	rm.height = minHeight;
	rm.plainWidth = 8;
	rm.curvature = 60;

	g->makeRiver(&rm);

	/*
	 * Apply smoothing function
	 */

#ifdef DEBUG
	strcat(popupBuffer, "\rSmoothing");
	popup.showText(popupBuffer);
#endif

	grids[Grid_Min].smooth(smooth_even);
	grids[Grid_Min].smooth(smooth_even);
	grids[Grid_Min].smooth(smooth_even);

	/*
	 * Create other grids by averaging 1 mile one!
	 */

#ifdef DEBUG
	strcat(popupBuffer, "\rZoomed out maps");
	popup.showText(popupBuffer);
#endif

	makeZoomedGrids(drawData);

#ifdef LOG_GRID
	logGrid();
#endif

}


void Grid::removeRivers()
{
	TerrainType lastTerrainType = NormalTerrain;
	for(int y = 0; y < gridCount; y++)
	{
		for(int x = 0; x < gridCount; x++)
		{
			TerrainType* t = getTSquare(x, y);
			if(isRiver(*t))
				*t = lastTerrainType;
			else
			{
				if(!isRoad(*t) && !isRail(*t))
					lastTerrainType = *t;
			}
		}
	}
}

void Grid::removeRoads()
{
	TerrainType lastTerrainType = NormalTerrain;
	for(int y = 0; y < gridCount; y++)
	{
		for(int x = 0; x < gridCount; x++)
		{
			TerrainType* t = getTSquare(x, y);
			if(isRoad(*t))
				*t = lastTerrainType;
			else
			{
				if(!isRiver(*t) && !isRail(*t))
					lastTerrainType = *t;
			}
		}
	}
}


void Grid::removeTerrain(TerrainType tt)
{
	TerrainType lastTerrainType = NormalTerrain;
	for(int y = 0; y < gridCount; y++)
	{
		for(int x = 0; x < gridCount; x++)
		{
			TerrainType* t = getTSquare(x, y);
			if(*t == tt)
				*t = lastTerrainType;
			else
			{
				if(!isRoad(*t) && !isRiver(*t) && !isRail(*t))
					lastTerrainType = *t;
			}
		}
	}
}

#ifdef LOG_GRID
void MapGrid::logGrid()
{
	if(logFile && logFile->isShown(50))
	{
		*logFile << "\n\nTerrain Grid Heights\n";

		for(int i = 0; i < GridCount; i++)
		{
			Grid* g = &grids[i];
			Height8* hp = g->heights;
			TerrainType* tp = g->terrains;

			*logFile << "\nGrid[" << i << "] has " << g->gridCount << " grids of size " << g->gridSize << "\n";
			for(int y = 0; y < g->pointCount; y++)
			{
				for(int x = 0; x < g->pointCount; x++)
				{
					*logFile << "[" << y << "][" << x << "] = " <<
						(int)hp++->height;
						
					if( (x < g->gridCount) && (y < g->gridCount))
					{
						*logFile << ", " << (int)*tp++;
					}

					*logFile << "\n";
				}
			}
		}
	}
}
#endif


void Grid::makePlateau(MapGridCord x, MapGridCord z, UBYTE height, int radius)
{
	int y1 = z - radius/2;

	int h = radius;
	while(h--)
	{
		if( (y1 >= 0) && (y1 < pointCount))
		{
			int x1 = x - radius/2;
			int w = radius;
			while(w--)
			{
				if((x1 >= 0) && (x1 < pointCount))
				{
					getHSquare(x1, y1)->height = height;
				}
				x1++;
			}
		}

		y1++;
	}

#ifdef BATEDIT
	flattenRivers();
#endif
}

#endif		// BATEDIT

MapGridCord MapGrid::cord3DtoGrid(Cord3D src)
{
	const Grid* g = &grids[Grid_Min];

	int n = (src + size / 2) / g->gridSize;

	if(n < 0)
		n = 0;
	else if(n >= g->gridCount)
		n = g->gridCount - 1;

	return n;
}

Cord3D MapGrid::gridToCord3D(MapGridCord val)
{
	const Grid* g = &grids[Grid_Min];

	return val * g->gridSize - size / 2;
}

/*
 * Get the height at specified location
 *
 * Now that we are using a seperate point array to terrain
 * then the correct height could be calculated as before based on
 * a function of the 4 surrounding points
 */

Cord3D MapGrid::getHeight(Cord3D x, Cord3D z) const
{
	const Grid* g = &grids[Grid_Min];

	x += size/2;
	z += size/2;

	int gx = x / g->gridSize;
	int gz = z / g->gridSize;

#if 0
	if( (gx < 0) || (gx >= g->gridCount) ||
	    (gz < 0) || (gz >= g->gridCount) )
#endif
	if(!g->isValidHCord(gx, gz))
		 	return 0;

	Height8* hp = g->getHSquare(gx, gz);

	Cord3D xr = x - gx * g->gridSize;		// Probably quicker than % calculation
	Cord3D zr = z - gz * g->gridSize;

	Cord3D h1 = hp[0].getHeight();
	Cord3D h2 = hp[1].getHeight();
	Cord3D h3 = hp[g->pointCount].getHeight();
	Cord3D h4 = hp[g->pointCount + 1].getHeight();

	h1 = h1 + (xr * (h2 - h1)) / g->gridSize;
	h3 = h3 + (xr * (h4 - h3)) / g->gridSize;

	h1 = h1 + (zr * (h3 - h1)) / g->gridSize;  

	return h1;
}

/*
 * Similar to getHeight, but uses the currently displayed grid
 */

Cord3D MapGrid::getHeightView(const ViewPoint& view, Cord3D x, Cord3D z) const
{
	const Grid* g = getGrid(view.zoomLevel);

	x += size/2;
	z += size/2;

	int gx = x / g->gridSize;
	int gz = z / g->gridSize;

#if 0
	if( (gx < 0) || (gx >= g->gridCount) ||
	    (gz < 0) || (gz >= g->gridCount) )
#else
	if(!g->isValidHCord(gx, gz))
#endif
		 	return 0;

	Height8* hp = g->getHSquare(gx, gz);

	Cord3D xr = x - gx * g->gridSize;		// Probably quicker than % calculation
	Cord3D zr = z - gz * g->gridSize;

	Cord3D h1 = hp[0].getHeight();
	Cord3D h2 = hp[1].getHeight();
	Cord3D h3 = hp[g->pointCount].getHeight();
	Cord3D h4 = hp[g->pointCount + 1].getHeight();

	h1 = h1 + (xr * (h2 - h1)) / g->gridSize;
	h3 = h3 + (xr * (h4 - h3)) / g->gridSize;

	h1 = h1 + (zr * (h3 - h1)) / g->gridSize;  

	return h1;
}


/*
 * Render a river grid
 *
 * Grids are stored as a set of triangles within a fixed size square
 * each point is mapped onto the destination rectangle and rendered
 */

void renderCurve(System3D& drawData, const Point3DI& p1, const Point3DI& p2, const Point3DI& p3, const Point3DI& p4, UBYTE riverType, TerrainType inTerrain, TerrainType outTerrain, int scale)
{
	const int MaxRPoints = 16;
	const int MaxRTriangles = 18;

	struct RiverPoint {
		UBYTE x;
		UBYTE y;
	};

	struct RiverTriangle {
		UBYTE p1;
		UBYTE p2;
		UBYTE p3;
		UBYTE t;		// 1 = fill (water/road), 0=outside (river bank)
	};

	struct RiverGrid {
		UBYTE nPoints;
		UBYTE nTriangles;
		RiverPoint points[MaxRPoints];
		RiverTriangle triangles[MaxRTriangles];
	};

	const UBYTE L = 0x40;
	const UBYTE R = 0xc0;

	const UBYTE L1 = 0x30;
	const UBYTE R1 = 0xd0;

	const UBYTE L2 = 0x60;
	const UBYTE R2 = 0xa0;

	static RiverGrid riverGrids[] = {

		{
			/*
			 * Solid colour
			 */

			4, 2,

			{ { 0,0 }, { 0,0xff }, { 0xff,0 }, { 0xff,0xff } },
			{
				{ 2,1,0, 1 },
				{ 1,2,3, 1 }
			},

		},
		{
			/*
			 * Up/Down
			 */

			8, 6,

			{ { 0,0 }, { L, 0 }, { R, 0 }, { 0xff, 0 },
			  { 0,0xff }, { L, 0xff }, { R, 0xff }, { 0xff, 0xff } },

			{
			 { 0,5,4, 0 },
			 { 5,0,1, 0 },
			 { 1,6,5, 1 },
			 { 6,1,2, 1 },
			 { 2,7,6, 0 },
			 { 7,2,3, 0 }
			}
		},
		{
			/*
			 * Left/Right
			 */

			8, 6,

			{
			  { 0,0 },
			  { 0,L },
			  { 0,R },
			  { 0,0xff },
			  { 0xff,0 },
			  { 0xff,L },
			  { 0xff,R },
			  { 0xff,0xff }
			},

			{
			 { 5,0,4, 0 },
			 { 0,5,1, 0 },
			 { 6,1,5, 1 },
			 { 1,6,2, 1 },
			 { 7,2,6, 0 },
			 { 2,7,3, 0 }
			}
		},
		{
			/*
			 * Up/Left
			 */

			11, 12,

			{
			  { 0,0 }, { 0xff,0 }, { 0,0xff }, { 0xff,0xff },
			  { L,0xff }, { R,0xff },
			  { 0,L }, { 0,R },
			  { L,L }, { R,R }, { L1,R1 }
			},
			
			{
			  { 6,8,7, 1 },
			  { 7,8,10, 1 },
			  { 8,9,10, 1 },
			  { 10,9,4, 1 },
			  { 9,5,4, 1 },
			  { 7,10,2, 0 },
			  { 2,10,4, 0 },
			  { 0,1,8, 0 },
			  { 8,1,9, 0 },
			  { 9,1,3, 0 },
			  { 9,3,5, 0 },
			  { 0,8,6, 0 }
			}
		},
		{
			/*
			 * Up/Right
			 */

			11, 12,

			{
			  { 0,0xff }, { 0,0 }, { 0xff,0xff }, { 0xff,0 },
			  { 0xff,R }, { 0xff,L },
			  { L,0xff }, { R,0xff },
			  { L,R }, { R,L }, { R1,R1 }
			},
			
			{
			  { 6,8,7, 1 },
			  { 7,8,10, 1 },
			  { 8,9,10, 1 },
			  { 10,9,4, 1 },
			  { 9,5,4, 1 },
			  { 7,10,2, 0 },
			  { 2,10,4, 0 },
			  { 0,1,8, 0 },
			  { 8,1,9, 0 },
			  { 9,1,3, 0 },
			  { 9,3,5, 0 },
			  { 0,8,6, 0 }
			}
		},
		{
			/*
			 * Down/Left
			 */

			11, 12,

			{
			  { 0xff,0 }, { 0xff,0xff }, { 0,0 }, { 0,0xff },
			  { 0,L }, { 0,R },
			  { R,0 }, { L,0 },
			  { R,L }, { L,R }, { L1,L1 }
			},
			
			{
			  { 6,8,7, 1 },
			  { 7,8,10, 1 },
			  { 8,9,10, 1 },
			  { 10,9,4, 1 },
			  { 9,5,4, 1 },
			  { 7,10,2, 0 },
			  { 2,10,4, 0 },
			  { 0,1,8, 0 },
			  { 8,1,9, 0 },
			  { 9,1,3, 0 },
			  { 9,3,5, 0 },
			  { 0,8,6, 0 }
			}
		},
		{
			/*
			 * Down/Right
			 */

			11, 12,

			{
			  { 0xff,0xff }, { 0,0xff }, { 0xff,0 }, { 0,0 },
			  { R,0 }, { L,0 },
			  { 0xff,R }, { 0xff,L },
			  { R,R }, { L,L }, { R1,L1 }
			},
			
			{
			  { 6,8,7, 1 },
			  { 7,8,10, 1 },
			  { 8,9,10, 1 },
			  { 10,9,4, 1 },
			  { 9,5,4, 1 },
			  { 7,10,2, 0 },
			  { 2,10,4, 0 },
			  { 0,1,8, 0 },
			  { 8,1,9, 0 },
			  { 9,1,3, 0 },
			  { 9,3,5, 0 },
			  { 0,8,6, 0 }
			}
		},
		{
			/*
			 * UDR
			 */

			12,12,

			{
			 { 0xff,0xff }, { 0,0xff }, { 0xff,0 }, { 0,0 },
			 { R,0 }, { L,0 }, { 0xff,R }, { 0xff,L }, { R,0xff }, { L,0xff },
			 { R1,R1 }, { R1,L1 }
			},

			{
			 { 2,7,11, 0 },
			 { 2,11,4, 0 },
			 { 0,10,6, 0 },
			 { 0,8,10, 0 },
			 { 9,3,5, 0 },
			 { 9,1,3, 0 },
			 { 6,11,7, 1 },
			 { 6,10,11, 1 },
			 { 10,4,11, 1 },
			 { 10,8,4, 1 },
			 { 8,5,4, 1 },
			 { 8,9,5, 1 }
			}
		},
		{
			/*
			 * UDL
			 */

			12,12,

			{
			 { 0,0 }, { 0xff,0 }, { 0,0xff }, { 0xff,0xff },
			 { L,0xff }, { R,0xff }, { 0,L }, { 0,R }, { L,0 }, { R,0 },
			 { L1,L1 }, { L1,R1 }
			},

			{
			 { 2,7,11, 0 },
			 { 2,11,4, 0 },
			 { 0,10,6, 0 },
			 { 0,8,10, 0 },
			 { 9,3,5, 0 },
			 { 9,1,3, 0 },
			 { 6,11,7, 1 },
			 { 6,10,11, 1 },
			 { 10,4,11, 1 },
			 { 10,8,4, 1 },
			 { 8,5,4, 1 },
			 { 8,9,5, 1 }
			}
		},
		{
			/*
			 * LRU
			 */

			12,12,

			{
			 { 0,0xff }, { 0,0 }, { 0xff,0xff }, { 0xff,0 },
			 { 0xff,R }, { 0xff,L }, { L,0xff }, { R,0xff }, { 0,R }, { 0,L },
			 { L1,R1 }, { R1,R1 }
			},

			{
			 { 2,7,11, 0 },
			 { 2,11,4, 0 },
			 { 0,10,6, 0 },
			 { 0,8,10, 0 },
			 { 9,3,5, 0 },
			 { 9,1,3, 0 },
			 { 6,11,7, 1 },
			 { 6,10,11, 1 },
			 { 10,4,11, 1 },
			 { 10,8,4, 1 },
			 { 8,5,4, 1 },
			 { 8,9,5, 1 }
			}
		},
		{
			/*
			 * LRD
			 */

			12,12,

			{
			 { 0xff,0 }, { 0xff,0xff }, { 0,0 }, { 0,0xff },
			 { 0,L }, { 0,R }, { R,0 }, { L,0 }, { 0xff,L }, { 0xff,R },
			 { R1,L1 }, { L1,L1 }
			},

			{
			 { 2,7,11, 0 },
			 { 2,11,4, 0 },
			 { 0,10,6, 0 },
			 { 0,8,10, 0 },
			 { 9,3,5, 0 },
			 { 9,1,3, 0 },
			 { 6,11,7, 1 },
			 { 6,10,11, 1 },
			 { 10,4,11, 1 },
			 { 10,8,4, 1 },
			 { 8,5,4, 1 },
			 { 8,9,5, 1 }
			}
		},
		{
			/*
			 * UDLR
			 */
			16, 18,

			{ { 0,0 }, { 0xff, 0 }, { 0,0xff }, { 0xff,0xff },
			  { L,0 }, { R,0 }, { L,0xff }, { R,0xff },
			  { 0,L }, { 0,R }, { 0xff,L }, { 0xff,R },
			  { L1,L1 }, { R1,L1 }, { L1,R1 }, { R1,R1 }
			},

			{
			 { 9,14,2, 0 },
			 { 6,2,14, 0 },
			 { 7,15,3, 0 },
			 { 3,15,11, 0 },
			 { 0,12,8,  0 },
			 { 12,0,4,  0 },
			 { 5,1,13,  0 },
			 { 13,1,10, 0 },
			 { 6,14,7, 1 },
			 { 7,14,15, 1 },
			 { 15,14,9, 1 },
			 { 15,9,11, 1 },
			 { 11,9,8, 1 },
			 { 11,8,10, 1 },
			 { 10,8,12, 1 },
			 { 10,12,13, 1 },
			 { 13,12,4, 1 },
			 { 13,4,5, 1 }
			}
		},
		{
			/*
			 * L
			 */

			8, 8,

			{
				{ 0,0 }, { 0xff,0 }, { 0,0xff }, { 0xff,0xff },
				{ 0,L2 }, { 0,R2 }, { 0x80,L2 }, { 0x80,R2 }
			},

			{
			 { 4,7,5, 1 },
			 { 4,6,7, 1 },
			 { 5,7,2, 0 },
			 { 7,3,2, 0 },
			 { 7,1,3, 0 },
			 { 6,1,7, 0 },
			 { 0,1,6, 0 },
			 { 0,6,4, 0 }
			}
		},
		{
			/*
			 * R
			 */

			8, 8,

			{
				{ 0xff,0xff }, { 0,0xff }, { 0xff,0 }, { 0,0 },
				{ 0xff,R2 }, { 0xff,L2 }, { 0x80,R2 }, { 0x80,L2 }
			},

			{
			 { 4,7,5, 1 },
			 { 4,6,7, 1 },
			 { 5,7,2, 0 },
			 { 7,3,2, 0 },
			 { 7,1,3, 0 },
			 { 6,1,7, 0 },
			 { 0,1,6, 0 },
			 { 0,6,4, 0 }
			}
		},
		{
			/*
			 * U
			 */

			8, 8,

			{
				{ 0,0xff }, { 0,0 }, { 0xff,0xff }, { 0xff,0 },
				{ L2,0xff }, { R2,0xff }, { L2,0x80 }, { R2,0x80 }
			},

			{
			 { 4,7,5, 1 },
			 { 4,6,7, 1 },
			 { 5,7,2, 0 },
			 { 7,3,2, 0 },
			 { 7,1,3, 0 },
			 { 6,1,7, 0 },
			 { 0,1,6, 0 },
			 { 0,6,4, 0 }
			}
		},

		{
			/*
			 * D
			 */

			8, 8,

			{
				{ 0xff,0 }, { 0xff,0xff }, { 0,0 }, { 0,0xff },
				{ R2,0 }, { L2,0 }, { R2,0x80 }, { L2,0x80 }
			},

			{
			 { 4,7,5, 1 },
			 { 4,6,7, 1 },
			 { 5,7,2, 0 },
			 { 7,3,2, 0 },
			 { 7,1,3, 0 },
			 { 6,1,7, 0 },
			 { 0,1,6, 0 },
			 { 0,6,4, 0 }
			}
		}
	};

	/*
	 * Convert points into mapped points
	 */

	RiverGrid* rg = &riverGrids[riverType];
	Point3DI points[MaxRPoints];

	Point3DI* destPt = points;
	RiverPoint* rp = rg->points;
	for(int count = rg->nPoints; count--; rp++, destPt++)
	{
		/*
		 * Do special case of corners first
		 */

		if( (rp->x == 0) && (rp->y == 0) )
			*destPt = p1;
		else if( (rp->x == 0xff) && (rp->y == 0) )
			*destPt = p2;
		else if( (rp->x == 0) && (rp->y == 0xff) )
			*destPt = p3;
		else if( (rp->x == 0xff) && (rp->y == 0xff) )
			*destPt = p4;
		else
		{
			int x = rp->x;
			int y = rp->y;

			/*
			 * Adjust for scale if not an edge point
			 */

			if(scale)
			{
				if( (x != 0) && (x != 0xff) )
					x = 0x80 + ((x - 0x80) >> scale);
				if( (y != 0) && (y != 0xff) )
					y = 0x80 + ((y - 0x80) >> scale);
			}


			if(rp->x == 0)
			{
				destPt->x = p1.x + ((p3.x - p1.x) * y) / 0x100;
				destPt->y = p1.y + ((p3.y - p1.y) * y) / 0x100;
				destPt->z = p1.z + ((p3.z - p1.z) * y) / 0x100;
				destPt->intensity = p1.intensity + ((p3.intensity - p1.intensity) * y) / 0x100;
			}
			else if(rp->x == 0xff)
			{
				destPt->x = p2.x + ((p4.x - p2.x) * y) / 0x100;
				destPt->y = p2.y + ((p4.y - p2.y) * y) / 0x100;
				destPt->z = p2.z + ((p4.z - p2.z) * y) / 0x100;
				destPt->intensity = p2.intensity + ((p4.intensity - p2.intensity) * y) / 0x100;
			}
			else if(rp->y == 0)
			{
				destPt->x = p1.x + ((p2.x - p1.x) * x) / 0x100;
				destPt->y = p1.y + ((p2.y - p1.y) * x) / 0x100;
				destPt->z = p1.z + ((p2.z - p1.z) * x) / 0x100;
				destPt->intensity = p1.intensity + ((p2.intensity - p1.intensity) * x) / 0x100;
			}
			else if(rp->y == 0xff)
			{
				destPt->x = p3.x + ((p4.x - p3.x) * x) / 0x100;
				destPt->y = p3.y + ((p4.y - p3.y) * x) / 0x100;
				destPt->z = p3.z + ((p4.z - p3.z) * x) / 0x100;
				destPt->intensity = p3.intensity + ((p4.intensity - p3.intensity) * x) / 0x100;
			}
			else
			{
				/*
			 	 * P1 = p1 + X/G * (p2 - p1)
			 	 */

				Point3DI a;

				a.x = p1.x + ((p2.x - p1.x) * x) / 0x100;
				a.y = p1.y + ((p2.y - p1.y) * x) / 0x100;
				a.z = p1.z + ((p2.z - p1.z) * x) / 0x100;
				a.intensity = p1.intensity + ((p2.intensity - p1.intensity) * x) / 0x100;

				/*
			 	* P2 = p3 + X/G * (p4 - p3)
			 	*/

				Point3DI b;

				b.x = p3.x + ((p4.x - p3.x) * x) / 0x100;
				b.y = p3.y + ((p4.y - p3.y) * x) / 0x100;
				b.z = p3.z + ((p4.z - p3.z) * x) / 0x100;
				b.intensity = p3.intensity + ((p4.intensity - p3.intensity) * x) / 0x100;

				/*
			 	* Result = P1 + Y/G * (P2 - P1)
			 	*/

				destPt->x = a.x + ((b.x - a.x) * y) / 0x100;
				destPt->y = a.y + ((b.y - a.y) * y) / 0x100;
				destPt->z = a.z + ((b.z - a.z) * y) / 0x100;
				destPt->intensity = a.intensity + ((b.intensity - a.intensity) * y) / 0x100;
			}
		}
	}

	/*
	 * Now draw the triangles between the calculated points!
	 */

	count = rg->nTriangles;
	RiverTriangle* rt = rg->triangles;
	while(count--)
	{
		drawData.renderTriangle(
			points[rt->p1],
			points[rt->p2],
			points[rt->p3],
			rt->t ? inTerrain : outTerrain);

		rt++;
	}


}


void MapGrid::updateViewGrid(System3D& drawData)
{
#ifdef DEBUGGING_MAPDRAW
	char popupBuffer[400];
	strcpy(popupBuffer, "Drawing Landscape");
	PopupText popup(popupBuffer);
#else
	PopupText popup(language(lge_RenderingLandscape));
#endif

#ifdef DEBUGGING_MAPDRAW
	strcat(popupBuffer, "\rSetting up Viewpoint");
	popup.showText(popupBuffer);
#endif

	drawData.terrain.resetColours();
	drawData.zBuffer.clear();

#ifdef OLD_BATEDIT
	/*
	 * Add outside area if on large zoom level
	 */

	if(drawData.view.zoomLevel >= Zoom_16)
	{
		Point3DI p[4];

		p[0].x = drawData.view.realMinX;	// Bottom Left
		p[0].y = 0;
		p[0].z = drawData.view.realMinZ;
		p[0].intensity = intensityRange/4;
	
		p[1].x = drawData.view.realMaxX;	// Bottom Right
		p[1].y = 0;
		p[1].z = drawData.view.realMinZ;
		p[1].intensity = intensityRange/2;

		p[2].x = drawData.view.realMinX;	// Top Left
		p[2].y = 0;
		p[2].z = drawData.view.realMaxZ;
		p[2].intensity = intensityRange/2;

		p[3].x = drawData.view.realMaxX;	// Top Right
		p[3].y = 0;
		p[3].z = drawData.view.realMaxZ;
		p[3].intensity = (intensityRange * 3) / 4;

		drawData.view.transform(p[0]);
		drawData.view.transform(p[1]);
		drawData.view.transform(p[2]);
		drawData.view.transform(p[3]);

		drawData.renderTriangle(p[0], p[1], p[2], PastureTerrain);
		drawData.renderTriangle(p[3], p[2], p[1], PastureTerrain);
	}

#endif

	/*
	 * Create an array of 3D Points
	 */

#ifdef DEBUGGING_MAPDRAW
	strcat(popupBuffer, "\rMaking Grid");
	popup.showText(popupBuffer);
#endif

	int gw = drawData.view.gridW + 1;
	int gh = drawData.view.gridH + 1;

	Point3DI* points = new Point3DI[gw * gh];
	Grid* g = getGrid(drawData.view.zoomLevel);

	/*
	 * Make up set of grid points
	 */

	int gx = drawData.view.gridX;
	int gz = drawData.view.gridZ;

	Cord3D z = gz * g->gridSize - size/2;
	Cord3D x = gx * g->gridSize - size/2;

	Point3DI* pt = points;

#if defined(BATEDIT)

	int zCount;

	// Battle Editor Version must handle being off map

	int maxZ = gz + gh;
	int maxX = gx + gw;

	for(int gz1 = gz; gz1 < maxZ; gz1++)
	{
		Cord3D x1 = x;

		for(int gx1 = gx; gx1 < maxX; gx1++)
		{
			pt->x = x1;
			pt->z = z;
		 	pt->y = g->getHeight(gx1, gz1).getHeight();

			x1 += g->gridSize;

			pt++;
		}

		z += g->gridSize;
	}

#else

	// Normal Version

	Height8* hp = g->getHSquare(gx, gz);
	int zCount = gh;

	while(zCount--)
	{
		Height8* hp1 = hp;
		Cord3D x1 = x;

		int xCount = gw;

		while(xCount--)
		{
			pt->x = x1;
			pt->z = z;
			pt->y = hp1->getHeight();

			x1 += g->gridSize;

			hp1++;
			pt++;
		}

		hp += g->pointCount;
		z += g->gridSize;
	}
#endif

	/*
	 * Calculate intensities
	 * This is done by calculating the normal of each point
	 * and then doing a scaler product with the light source
	 *
	 * The outer row does not need to be calculated because they
	 * are never shown.
	 */

#ifdef DEBUGGING_MAPDRAW
	strcat(popupBuffer, "\rNormals & Intensities");
	popup.showText(popupBuffer);
#endif

	zCount = gh;
	pt = points;

	Point3DI* ptp = 0;

	while(zCount--)
	{
		Point3DI* pt1 = pt;

		Point3DI* ptn = pt1 + gw;
		Point3DI* pt2 = 0;
		
		int xCount = gw;

		while(xCount--)
		{
			/*
			 * General case normal is average of 4 cross products
			 */

			Point3D crossP;
			Point3D total = Point3D(0,0,0);

			Point3D v1;
			Point3D v2;
			Point3D v3;
			Point3D v4;

#if 0
			if(pt2)	// Not 1st column
				v1 = pt1[-1] - pt1[0];

			if(ptp)	// Not 1st row
				v2 = ptp[0] - pt1[0];

			if(xCount)	// Not last column
				v3 = pt1[1] - pt1[0];

			if(zCount)	// Not last row
				v4 = ptn[0] - pt1[0];
#else
			if(pt2)	// Not 1st column
				v1 = pt2[0] - pt1[0];

			if(ptp)	// Not 1st row
			{
				v2 = ptp[0] - pt1[0];
				ptp++;
			}

			if(xCount)	// Not last column
				v3 = pt1[1] - pt1[0];

			if(zCount)	// Not last row
			{
				v4 = ptn[0] - pt1[0];
				ptn++;
			}
#endif
			if(ptp)
			{
				if(pt2)
				{
					cross(crossP, v2, v1);
					total += crossP;
				}

				if(xCount)
				{
					cross(crossP, v3, v2);
					total += crossP;
				}
			}

			if(zCount)
			{
				if(xCount)
				{
					cross(crossP, v4, v3);
					total += crossP;
				}
				if(pt2)
				{
					cross(crossP, v1, v4);
					total += crossP;
				}
			}

			total.normalise();

			pt1->intensity = drawData.light.makeIntensity(total);

			pt2 = pt1++;
		}

		ptp = pt;
		pt += gw;
	}

	/*
	 * Translate points
	 */

#ifdef DEBUGGING_MAPDRAW
	strcat(popupBuffer, "\rTranslating Coordinates");
	popup.showText(popupBuffer);
#endif

	zCount = gh;
	pt = points;

	while(zCount--)
	{
		int xCount = gw;

		while(xCount--)
		{
			drawData.view.transform(*pt);
			pt++;
		}
	}

	/*============================================================
	 * Sky and Earth Drawing
	 */

#ifdef DEBUGGING_MAPDRAW
	strcat(popupBuffer, "\rSky and Earth");
	popup.showText(popupBuffer);
#endif

	int deltaPoint;
	Point3D bottom;

	switch(drawData.view.yRotate & 0xc000)
	{
	case 0:	// Bottom Left
		deltaPoint = 1;
		pt = points;
		bottom.x = 0;
		bottom.z = drawData.view.minZ;
		break;
	case 0x4000:	// Top Left
		deltaPoint = -gw;
		pt = points + (gh - 1) * gw;
		bottom.x = drawData.view.minX;
		bottom.z = 0;
		break;
	case 0x8000:	// Top right
		deltaPoint = -1;
		pt = points + gh * gw - 1;
		bottom.x = 0;
		bottom.z = drawData.view.maxZ;
		break;
	case 0xc000:	// Bottom right
		deltaPoint = gw;
		pt = points + gw - 1;
		bottom.x = drawData.view.maxX;
		bottom.z = 0;
		break;
	}

	bottom.y = -DistToBattle(Yard(440));		// Go 1/4 mile below the surface
	drawData.view.transform(bottom);

	battle->earthFace->clear();
	battle->earthFace->setBottom(bottom.y);

	zCount = gh;
	while(zCount--)
	{
		battle->earthFace->add(*pt);

		pt += deltaPoint;
	}

	battle->earthFace->fillSky(drawData.bitmap);


	/*==================================================
	 * Draw triangles
	 */

#ifdef DEBUGGING_MAPDRAW
	strcat(popupBuffer, "\rDrawing triangles");
	popup.showText(popupBuffer);
#endif

#if defined(BATEDIT)

	maxZ = gz + gh - 1;
	maxX = gx + gw - 1;

	pt = points;

	for(gz1 = gz; gz1 < maxZ; gz1++)
	{
		Point3DI* pt1 = pt;

		for(int gx1 = gx; gx1 < maxX; gx1++)
		{
			TerrainType tt = g->getTerrain(gx1, gz1);

			if(isRiver(tt))
			{
				renderCurve(drawData,
					pt1[0], pt1[1], pt1[gw], pt1[gw+1],
					tt - RiverTerrain,
					RiverTerrain, RiverBankTerrain,
					drawData.view.zoomLevel);
			}
			else if(isRoad1(tt))
			{
				renderCurve(drawData,
					pt1[0], pt1[1], pt1[gw], pt1[gw+1],
					tt - Road1Terrain,
					Road1Terrain, RoadsideTerrain,
					drawData.view.zoomLevel);
			}
			else if(isRoad2(tt))
			{
				renderCurve(drawData,
					pt1[0], pt1[1], pt1[gw], pt1[gw+1],
					tt - Road2Terrain,
					Road2Terrain, RoadsideTerrain,
					drawData.view.zoomLevel);
			}
			else if(isRail(tt))
			{
				renderCurve(drawData,
					pt1[0], pt1[1], pt1[gw], pt1[gw+1],
					tt - RailwayTerrain,
					RailwayTerrain, RailsideTerrain,
					drawData.view.zoomLevel);
			}
			else
			{
				drawData.renderTriangle(pt1[0], pt1[gw+1], pt1[gw], tt);
				drawData.renderTriangle(pt1[gw+1], pt1[0], pt1[1], tt);
			}

			pt1++;
		}

		pt += gw;
	}

#else

	zCount = gh - 1;
	pt = points;
	TerrainType* tp = g->getTSquare(gx, gz);

	while(zCount--)
	{
		Point3DI* pt1 = pt;
		TerrainType* tp1 = tp;

		int xCount = gw - 1;

		while(xCount--)
		{
			if(isRiver(*tp1))
			{
				renderCurve(drawData,
					pt1[0], pt1[1], pt1[gw], pt1[gw+1],
					*tp1 - RiverTerrain,
					RiverTerrain, RiverBankTerrain,
					drawData.view.zoomLevel);
			}
			else if(isRoad1(*tp1))
			{
				renderCurve(drawData,
					pt1[0], pt1[1], pt1[gw], pt1[gw+1],
					*tp1 - Road1Terrain,
					Road1Terrain, RoadsideTerrain,
					drawData.view.zoomLevel);
			}
			else if(isRoad2(*tp1))
			{
				renderCurve(drawData,
					pt1[0], pt1[1], pt1[gw], pt1[gw+1],
					*tp1 - Road2Terrain,
					Road2Terrain, RoadsideTerrain,
					drawData.view.zoomLevel);
			}
			else if(isRail(*tp1))
			{
				renderCurve(drawData,
					pt1[0], pt1[1], pt1[gw], pt1[gw+1],
					*tp1 - RailwayTerrain,
					RailwayTerrain, RailsideTerrain,
					drawData.view.zoomLevel);
			}
			else
			{
				drawData.renderTriangle(pt1[0], pt1[gw+1], pt1[gw], *tp1);
				drawData.renderTriangle(pt1[gw+1], pt1[0], pt1[1], *tp1);
			}

			pt1++;
			tp1++;
		}

		pt += gw;
		tp += g->gridCount;
	}
#endif		// BATEDIT

#ifdef DEBUGGING_MAPDRAW
	strcat(popupBuffer, "\rRendering Triangles");
	popup.showText(popupBuffer);
#endif

	drawData.render();

	/*=================================================================
	 * Make static sprites
	 */

#ifdef DEBUGGING_MAPDRAW
	strcat(popupBuffer, "\rRendering Sprites");
	popup.showText(popupBuffer);
#endif

	delete[] points;
}
