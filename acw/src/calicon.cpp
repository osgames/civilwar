/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	CAL Main Area Icons
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.12  1994/04/22  22:14:22  Steven_Green
 * Icon Handling tidied up.
 *
 * Revision 1.1  1994/02/10  22:57:14  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "calicon.h"

#if !defined(CAMPEDIT)
#include "calmulti.h"
#endif

#include "ob.h"
#include "generals.h"

#if !defined(TESTCAMP) && !defined(CAMPEDIT)
#include "unit3d.h"
#endif

#include "error.h"
#include "game.h"
#include "dialogue.h"
#include "tables.h"
#include "language.h"

/*
 * Any Command box has been clicked on
 * Work out what we are doing and do whatever necessary
 */

void CAL_CommandIcon::execute(Event* event, CAL* cal)
{
	if(event->buttons & Mouse::LeftButton)
	{
#if !defined(TESTBATTLE)
		if(cal->getPickMode() == CAL::Picking)
		{
#endif
			// cal->showInfo(element);

			/*
			 * Do popup box with choices
			 */

			if(cal->unitIsValid(element))
				cal->doPickup(element, element->general, iconNumber, event->where);
#if !defined(TESTBATTLE)
		}
		else
		{
			if(canDrop(cal))
			{
				if(drop(cal, event->where))
				{
					cal->setPickMode();
					cal->pickedUnit = element;		// Highlight where it was dropped
					cal->setRedraw();
				}
			}
		}
#endif
	}
}

void CAL_CommandIcon::showInfo(CAL* cal)
{
#if !defined(TESTBATTLE)
	if(cal->getPickMode() == CAL::Dropping && canDrop(cal))
		cal->showDropPointer();
#endif

	// cal->setPointer(M_WhereTo);

	cal->infoWindow->showInfo(element);
}


#if !defined(TESTBATTLE)
#if 0

Boolean CAL_CommandIcon::pickup(CAL* cal)
{
	cal->setPointer(iconNumber);

	cal->pickedUnit    = element;
	cal->pickedGeneral = element->general;
	
	return True;
}

#endif

/*
 * Find if a unit can be dropped somewhere
 */

Boolean CAL_CommandIcon::canDrop(CAL* cal)
{
	/*
	 * Units can only be dropped on units of higher rank
	 */

	if( (cal->pickedUnit != 0) && cal->dragRecursive )
	{
		if((element->getRank() == Rank_Brigade) && 
		   (element->childCount >= MaxRegimentsPerBrigade))
		{
			return False;
		}

		if( (cal->pickedUnit->getRank() == Rank_Regiment) &&
			 (element->getRank() == Rank_Regiment) )
		{
			Regiment* r1 = (Regiment*) cal->pickedUnit;
			Regiment* r2 = (Regiment*) element;

			if(r1->type.basicType != r2->type.basicType)
				return False;

			if( (r1->strength + r1->stragglers + r2->strength + r2->stragglers) > MaxMenPerRegiment)
				return False;

			return True;
		}

		if(cal->pickedUnit->getRank() <= element->getRank())
			return False;

		if(cal->pickedUnit->superior == element)
			return False;
	}

	if((cal->pickedGeneral != 0) && !cal->dragRecursive)
	{
		if(element->getRank() == Rank_Regiment)
			return False;
	}

	return True;
}

/*
 * Drop something on a command icon
 */

Boolean CAL_CommandIcon::drop(CAL* cal, const Point& p)
{
#if 0		// All this done in canDrop
	if(element == cal->pickedUnit)	// Do nothing if dropped on self
		return True;
#endif

	/*
	 * Regiments handled as special case..
	 * Can either be dropped on a brigade commander to add to brigade
	 * Or merged with an existing regiment.
	 */

	if(cal->pickedUnit && (cal->pickedUnit->getRank() == Rank_Regiment))
	{

		/*
		 * Merge Regiment with another Regiment
		 */

		if(element->getRank() == Rank_Regiment)
		{
#if 0		// All this done in canDrop
			/*
			 * Check to see if new regiment would be > 1000 men
			 */

			Regiment* r1 = (Regiment*) cal->pickedUnit;
			Regiment* r2 = (Regiment*) element;

			if(r1->type.basicType != r2->type.basicType)
				return False;

			if( (r1->strength + r1->stragglers + r2->strength + r2->stragglers) > MaxMenPerRegiment)
				return False;
#endif

			if(dialAlert(cal, p, language( lge_MergeRegs ), language( lge_OKCancel ) ) == 0)
			{
#if defined(BATEDIT) || defined(CAMPEDIT)
				cal->ob->mergeRegiment((Regiment*)cal->pickedUnit, (Regiment*)element);
#else
				txCALTransferUnit(cal->ob, cal->pickedUnit, element);
#endif

				return True;
			}
		}

		/*
		 * Move Regiment to new brigade
		 */

		else		// if(element->getRank() == Rank_Brigade)
		{
#if 0		// All this done in canDrop
			/*
			 * Check that are less than 6 regiments in brigade
			 */

			if((element->getRank() == Rank_Brigade) && (element->childCount >= MaxRegimentsPerBrigade))
				return False;
#endif

			/*
		 	 * Remove the regiment from where it was
		 	 * and put it in new place
		 	 */

#if defined(BATEDIT) || defined(CAMPEDIT)
			cal->ob->assignUnit(cal->pickedUnit, element);
#else
			txCALTransferUnit(cal->ob, cal->pickedUnit, element);
#endif
			return True;
		}
	}
	else if(element->getRank() == Rank_Regiment)
	{
		;	// Can't drop anything except a regiment on a regiment
	}

	/*
	 * Transferring A General
	 */

	else if(cal->pickedGeneral && (!cal->dragRecursive))
	{
#if defined(BATEDIT) || defined(CAMPEDIT)
		cal->ob->assignGeneral(cal->pickedGeneral, element);
#else
		txCALTransferGeneral(cal->ob, cal->pickedGeneral, element);
#endif
		return True;
	}

	/*
	 * Transferring Units
	 */

	else if(cal->pickedUnit)
	{
#if defined(BATEDIT) || defined(CAMPEDIT)
		cal->ob->assignUnit(cal->pickedUnit, element);
#else
		txCALTransferUnit(cal->ob, cal->pickedUnit, element);
#endif
		return True;
	}

	return False;
}

#endif	// TESTBATTLE

/*
 * Scroll Icons
 */

void CAL_ScrollIcon::execute(Event* event, CAL* cal)
{
	if(event->buttons)
	{
		Boolean changed = False;

		if(dir == Left)
		{
			if(cal->tops[rank] > 0)
			{
				cal->tops[rank]--;
				changed = True;
			}
		}
		else
		{
			cal->tops[rank]++;
			changed = True;
		}

		if(changed)
		{
			int r = rank;
			while(r++ < Rank_Brigade)
				cal->tops[r] = 0;

			cal->updateTops(0);
			main->setRedraw();
		}
	}
}

void CAL_ScrollIcon::showInfo(CAL* cal)
{
	cal->infoWindow->showText(language(lge_Scroll));

	cal->setPointer(M_Arrow);
}


#if !defined(TESTBATTLE)
/*
 * Create a new brigade General
 */

void CAL_NewIcon::makeGeneral(CAL* cal, Unit* p)
{
	General* g;

	/*
	 * Check the unallocated Generals for suitable generals
	 */

	/*
	 * If No suitable generals, then create one
	 */

	g = new General("A New General", p->getSide());

	/*
	 * If exactly one general, then use him
	 */

	/*
	 * If more than one general, ask user to choose
	 */

	cal->ob->assignGeneral(g, p);
}

Boolean CAL_NewIcon::canDrop(CAL* cal)
{
	if(superior->childCount >= 31)
		return False;

	if( (cal->pickedUnit != 0) && (cal->dragRecursive) )
	{
		if(cal->pickedUnit->getRank() <= superior->getRank())
			return False;
	}

	return True;
}

/*
 * Drop something on a NEW Icon
 */

Boolean CAL_NewIcon::dropNew(CAL* cal)
{
#if 0		// All this done in canDrop
	/*
	 * Don't allow more than 32 units across
	 */

	if(superior->childCount >= 31)
		return False;
#endif

	/*
	 * General's can be dropped anywhere
	 * But create a chain of empty slots downwards
	 */

	if(cal->pickedGeneral && !cal->dragRecursive)
	{

#if defined(BATEDIT) || defined(CAMPEDIT)
		Unit* p = cal->ob->makeNew(superior, cal->pickedGeneral);
		cal->updateTops(p);	// Force unit onto screen
		cal->setRedraw();
#else
		txCALMakeNew(cal->ob, superior, cal->pickedGeneral);
#endif

		return True;
	}

	/*
	 * Unit's can be dropped anywhere, but will be added at the appropriate
	 * level..  e.g. dropping a corps on a regiment will add a new corps in
	 * whichever army the regiment is in.
	 *
	 * If at a higher rank, then a chain of command needs to
	 * be created.  e.g. dropping a regiment on a corps will create a
	 * new division and brigade slot.
	 */

	else if(cal->pickedUnit)
	{
#if defined(BATEDIT) || defined(CAMPEDIT)
		cal->ob->assignUnit(cal->pickedUnit, superior);
#else
		txCALTransferUnit(cal->ob, cal->pickedUnit, superior);
#endif

		return True;
	}

	return False;
}

/*
 * A "Make New unit" Icon has been clicked on
 */

void CAL_NewIcon::execute(Event* event, CAL* cal)
{
	if(cal->campaignMode)
	{
		if(event->buttons & Mouse::LeftButton)
		{
#ifdef DEBUG
			// cal->message("New Icon Left Clicked");
#endif

			if(cal->getPickMode() == CAL::Dropping)
			{
				if(canDrop(cal))
				{
					if(dropNew(cal))
					{
						cal->setPickMode();
						cal->setRedraw();
					}
				}
			}
#if defined(CAMPEDIT) || defined(BATEDIT)
			else
				createNewUnit(cal);
#endif
		}
	}
}

void CAL_NewIcon::showInfo(CAL* cal)
{
	if(cal->getPickMode() == CAL::Dropping)
	{
		if(canDrop(cal))
			cal->showDropPointer();
			// cal->setPointer(M_WhereTo);
	}
		
	cal->infoWindow->showText(language(lge_NewUnit));
}
#endif	// TESTBATTLE

/*
 * Mouse has been clicked over main display
 */

void CAL_MainDisplay::execute(Event* event, MenuData* data)
{
	data = data;

	if(event->overIcon)
	{
		/*
		 * Track for closest Icon
		 */

		CALIcon* closestIcon = 0;

		if(icons.entries())
		{
			SDimension dist;

			PtrSListIter<CALIcon> list(icons);

			const Point& mpos = event->where;

			while(++list)
			{
				CALIcon* icon = list.current();

				SDimension x1;
				SDimension y1;
				Boolean inX = False;
				Boolean inY = False;

				if(mpos.x < icon->getX())
					x1 = icon->getX();
				else if(mpos.x < (icon->getX() + icon->getW()))
				{
					x1 = icon->getX() + icon->getW() / 2;
					inX = True;
				}
				else
					x1 = icon->getX() + icon->getW();

				if(mpos.y < icon->getY())
					y1 = icon->getY();
				else if(mpos.y < (icon->getY() + icon->getH()))
				{
					y1 = icon->getY() + icon->getH() / 2;
					inY = True;
				}
				else
					y1 = icon->getY() + icon->getH();

				if(inX && inY)
				{
					dist = 0;
					closestIcon = icon;
				}
				else
				{
					SDimension d = distance(mpos.x - x1, mpos.y - y1);

					if((d < 8) && (!closestIcon || (d < dist)))	// was (d < 32)
					{
						dist = d;
						closestIcon = icon;
					}
				}
			}
		}

		/*
	 	 * Display Info about closest icon
	 	 */

		if(closestIcon != cal->currentIcon)
		{
			cal->setPointer(cal->dragPointer);		// showInfo can override this

			cal->currentIcon = closestIcon;
			if(closestIcon)
				closestIcon->showInfo(cal);
			else
				cal->infoWindow->clear();
		}

		/*
	 	 * Do whatever is required when clicked
	 	 */

		if(event->buttons)
		{
			/*
		 	 * Right button cancels dragging
		 	 */

			if( (cal->getPickMode() == CAL::Dropping) &&
			 	(event->buttons & Mouse::RightButton) )
			{
				cal->setPickMode();
				cal->setRedraw();
				return;
			}

			if(closestIcon)
				closestIcon->execute(event, cal);

#ifdef DEBUG1
			else
				cal->message("You didn't click on anything!");
#endif

		}
	}
}


/*
 * OK/Cancel Icon
 */

void CAL_OkIcon::execute(Event* event, MenuData* data)
{
	if(event->buttons)
	{
#if defined(CAMPEDIT) || defined(BATEDIT)
		if(event->buttons & Mouse::RightButton)
			cal->options();
		else
#endif
		if(cal->getPickMode() == CAL::Picking)
			data->setFinish(MenuData::MenuOK);
		else
		{
			cal->setPickMode();
			cal->setRedraw();
		}
	}
}

CAL_OkIcon::CAL_OkIcon(CAL* c) :
	MenuIcon(c, MM_BigOK, Rect(CAL_OK_X, CAL_OK_Y, CAL_OK_W, CAL_OK_H)),
	CALBase(c)
{ 
}
