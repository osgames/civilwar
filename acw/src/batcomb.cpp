/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battle Combat
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdlib.h>
#include "unit3d.h"
#include "batldata.h"
#include "batltab.h"
#include "tables.h"
#include "ai_bat.h"
#include "generals.h"
#include "los.h"
#include "game.h"
#include "batsnd.h"
#include "corpses.h"

#ifdef DEBUG
#include "clog.h"
#endif

/*
 * Some constants
 */

#define MoraleForKilling 2		// Killing 1/2 of enemy will fully increase morale
#define MoraleForDying 5		// Was 10  (1/5 of men killed will result in rout)


/*
 * Handy data structure to reduce recalculating combat related stuff
 */

struct CombatData {
	Regiment* r;					// Our regiment
	RegimentBattle* rb;			// Our regiment Battle Info
	Distance effRange;			// Our effective range
	Distance decRange;			// Our decisive Range
};

/*
 * Apply combat check for recently moved unit
 *
 * Algorithm is:
 *  Set closest to NULL
 *  FOR each enemy regiment
 *    Calculate distance between regiments
 *    IF within other unit's effective range and LOS THEN
 *      process other unit to see if it wants to do anything
 *    ELSE
 *      IF we are other unit's current target
 *        Clear other unit's target
 *    IF other unit in our effective range and our Line of Sight THEN
 *      IF closer than closest THEN
 *         update closest
 *    ELSE
 *      IF other unit is our current target THEN
 *        Clear our target
 *  IF any close units found
 *    Process unit to see if it wants to do anything.
 *
 */

void RegimentBattle::combatCheck(Location& oldLocation, Wangle dir)
{
	CombatData ourData;
	fillCombatData(ourData);

	Distance closeDist = 0;
	CombatData closeData;
	closeData.r = 0;

	// Distance dis;

	/*
	 * Get the other side's president and process regiments
	 *
	 * This can be speeded up by also doing a distance check at each level
	 * and not continuing down if too far away (further than command range + maximum range)
	 */

	President* p = battle->ob->getPresident(otherSide(unit->getSide()));
	for(Unit* a = p->child; a; a = a->sister)
	{
	 if(a->battleInfo || (a->hasCampaignReallyDetached()) || a->hasBattleReallyDetached() || a->hasBattleDetached() )
	 {
		for(Unit* c = a->child; c; c = c->sister)
		{
		 if(c->battleInfo || (c->hasCampaignReallyDetached()) || c->hasBattleReallyDetached() || c->hasBattleDetached() )
		 {
			for(Unit* d = c->child; d; d = d->sister)
			{
			 if(d->battleInfo || (d->hasCampaignReallyDetached()) || d->hasBattleReallyDetached() || d->hasBattleDetached() )
			 {
				for(Unit* b = d->child; b; b = b->sister)
				{
				 if(b->battleInfo)
				 {
					for(Regiment* r = (Regiment*)b->child; r; r = (Regiment*)r->sister)
					{
#ifdef DEBUG
						if(r->getRank() != Rank_Regiment)
							throw("Something wrong with OB... regiment is not a regiment!");
#endif

						if(!r->battleInfo || r->battleInfo->isDead)
						{
							if(r == target)
							{
#ifdef DEBUG
								bLog.printf("%s loses target %s because no battleInfo or dead",
									unit->getName(True),
									target->getName(True));
#endif
								loseTarget();
							}
						}
						else
						{
							RegimentBattle* rb = (RegimentBattle*)r->battleInfo;

							if(rb->isDead)
							{
								if(r == target)
								{
#ifdef DEBUG
									bLog.printf("%s loses target %s because it is dead",
										unit->getName(True),
										target->getName(True));
#endif
									loseTarget();
								}
							}
							else
							{
								CombatData enemyData;
								rb->fillCombatData(enemyData);

								Distance d = getRegimentDistance(this, rb);	

								/*
							 	 * Are we in contact?
							 	 */

								if(d <= 0)
								{
									doMelee(enemyData, ourData);
								}
								else
								{

									/*
						 	 	 	 * Can we be seen and fired at by enemy?
						 	 	 	 */

									Distance minDist = Yard(200);
									if(r->type.basicType == Artillery)
										minDist = Yard(880);

									if( (d < enemyData.effRange) && inLOS(where, rb->where, minDist))
									{
										// We are in an enemy regiment's range!!!
										enemyInRange(enemyData, ourData, d);
									}
									else
									{
										if(ourData.r == rb->target)
										{
#ifdef DEBUG
											bLog.printf("%s loses target %s because it is out of range",
												r->getName(True),
												rb->target->getName(True));
#endif
											rb->loseTarget();
										}
									}

									/*
						 	 	 	 * Can we see and fire at enemy?
						 	 	 	 */

									if( (d < ourData.effRange) && inLOS(rb->where, where, minDist) )
									{
										if(!closeData.r || (d < closeDist))
										{
											closeData = enemyData;
											closeDist = d;
										}
									}
									else
									{
										if(r == target)
										{
#ifdef DEBUG
											bLog.printf("%s loses target %s because it is out of range",
												unit->getName(True),
												target->getName(True));
#endif
											loseTarget();
										}
									}
								}
							}
						}
					}
				 }
				}	// brigade
			 }
			}	// Division
		 }
		}	// Corps
	 }
	}	// Army
	if(closeData.r)
		enemyInRange(ourData, closeData, closeDist);

	/*
	 * Process withdraw order logic
	 */

	if(battleOrder.basicOrder() == ORDERMODE_Withdraw)
	{
		if(moving)
		{
			if(combatMode == CM_Firing)	// AND if it has fired?
			{
				setCombatMode(CM_None);
				movedDistance = 0;
			}
			else
			{
				if(canTurn)
				{
					if(battleOrder.mode != ORDER_HastyRetreat)
					{
						Distance backedDistance;

						if(battleOrder.mode == ORDER_Withdraw)
							backedDistance = Yard(50);
						else if(battleOrder.mode == ORDER_Retreat)
							backedDistance = Yard(100);

						if(movedDistance >= backedDistance)
						{
							setCombatMode(CM_Firing);
						}
					}
				}
				else
				{
					if(ourData.r->type.basicType == Cavalry)
					{
						CavInfantryBattle* c = (CavInfantryBattle*)this;
						c->mount();
					}
					else if(ourData.r->type.basicType == Artillery)
					{
						ArtilleryBattle* a = (ArtilleryBattle*)this;
						a->limber();
					}
				}
			}
		}
		else
		{
			/*
			 * We've just stopped, so we should either turn to face closest
			 * enemy or the direction we started in.
			 */

			if(target)
			{
				OrderMode newOrder = ORDER_Stand;

				if(ourData.r->type.basicType == Cavalry)
				{
					CavInfantryBattle* c = (CavInfantryBattle*)this;
					c->unMount();

					if(!c->isMountedInfantry() && (battleOrder.mode != ORDER_HastyRetreat))
						newOrder = ORDER_Defend;
				}

				changeOrder(newOrder);
			}

		}
	}
}

/*
 * Apply combat check for recently stopped unit!
 */

void RegimentBattle::combatCheck()
{
	combatCheck(where, menFacing);
}

/*
 * Do a combat check for stationary objects
 * Just checks for target still being valid
 */

void RegimentBattle::combatCheckStationary()
{
	if(target)
	{
		if(target->battleInfo && !target->battleInfo->isDead)
		{
			/*
			 * We could if we wanted check distance and LOS, but this
			 * should already be checked when a moving unit moves.
			 */

			return;
		}

#ifdef DEBUG
		bLog.printf("%s loses target %s because it is dead or has no battleInfo",
			unit->getName(True),
			target->getName(True));
#endif
		loseTarget();
	}
}

/*
 * Get distance between 2 regiments
 * Returns 0 if overlapping.
 */

Distance getRegimentDistance(RegimentBattle* r1, RegimentBattle* r2)
{
	Distance d = distanceLocation(r1->where, r2->where);

	/*
	 * For big distances just return this... saves a bit of time
	 */

	if(d >= Mile(1))
		return d;

	/*
	 * Method asssuming they are ellipses
	 *
	 */

	Distance w1;
	Distance h1;
	Distance w2;
	Distance h2;

	r1->getArea(w1, h1);
	r2->getArea(w2, h2);

	w1 = w1/2;
	h1 = h1/2;
	w2 = w2/2;
	h2 = h2/2;

	// Get relative angle

	Wangle theta = r1->formationFacing - directionLocation(r1->where, r2->where);

	Distance d1 = distance( mcos(w1, theta), msin(h1, theta) );

	theta = r2->formationFacing - directionLocation(r2->where, r1->where);

	Distance d2 = distance( mcos(w2, theta), msin(h2, theta) );

	d -= (d1 + d2);

	if(d < 0)
		return 0;
	else
		return d;
}

/*
 * Get a regiment's effective range
 * depends on the regiment type and is modified by experience and supply
 */


Distance RegimentBattle::getEffectiveRange()
{
	Regiment* r = getRegiment();

	Distance d = effectiveRanges[r->type.basicType][r->type.subType];

	/*
	 * Apply modifiers
	 */

	return d;
}


/*
 * Decisive range depends on experience
 */

Distance RegimentBattle::getDecisiveRange()
{
	UBYTE exp;

	Regiment* r = getRegiment();

	if(game->getDifficulty(Diff_Morale) >= Complex1)
	{

		exp = getExperienceEnum(r->experience);
	}
	else
		exp = Seasoned;

	Distance d = decisiveRange[exp];

	if(r->type.basicType == Artillery)
		d = d * 4;

	return d;
}

void RegimentBattle::fillCombatData(CombatData& data)
{
	data.r = getRegiment();
	data.rb = this;
	data.effRange = getEffectiveRange();
	data.decRange = getDecisiveRange();
	if(data.effRange < data.decRange)
		data.effRange = data.decRange;
}

/*
 * Enemy is in range of us
 */

void enemyInRange(CombatData& viewer, CombatData& target, Distance d)
{
#ifdef DEBUG
	bLog.printf("enemyInRange: %s and %s",
		viewer.r->getName(True),
		target.r->getName(True));
#endif

	/*
	 * Can't fire at anyone if we are in melee!
	 */

	if(viewer.rb->combatMode == CM_Melee)
		return;

	Boolean repeat;

	/*
	 * Do different things depending on order
	 */

	do
	{
		repeat = False;

		switch(viewer.rb->battleOrder.mode)
		{
		case ORDER_Advance:
			repeat = sightEnemyAdvance(viewer, target, d);
			break;
		case ORDER_CautiousAdvance:
			repeat = sightEnemyCautious(viewer, target, d);
			break;
		case ORDER_Attack:
			repeat = sightEnemyAttack(viewer, target, d);
			break;

		case ORDER_Stand:
		case ORDER_Hold:
			repeat = sightEnemyStand(viewer, target, d);
			break;
		case ORDER_Defend:
			repeat = sightEnemyDefend(viewer, target, d);
			break;

		case ORDER_Withdraw:
		case ORDER_Retreat:
		case ORDER_HastyRetreat:
			repeat = sightEnemyWithdraw(viewer, target, d);
			break;
		}
	} while(repeat);
}

/*
 * Individual routines called for particular orders when enemy in sight
 * return true if the orders have changed and logic needs applying for new order
 */

Boolean sightEnemyAdvance(CombatData& viewer, CombatData& target, Distance d)
{
	/*
	 * If enemy with 500 yards, then into line and stand
	 */


	/*
	 * Only change targets if new one is 100 yards closer than previous one.
	 */

	Distance d1 = d + Yard(100);


	if(!viewer.rb->target || (d1 < viewer.rb->targetDistance) || (viewer.rb->target == target.r) )
	{
		if(d <= NearEnemyRange)
		{
			viewer.rb->setTarget(target.r, d);
			viewer.rb->changeFormation(Form_Line);
			viewer.rb->changeOrder(ORDER_Stand);

			/*
			 * Mounted Infantry must unmount and Artillery must unlimber
			 * ... this done automatically by movement logic
			 */

			return True;
		}
	}

	return False;
}

Boolean sightEnemyCautious(CombatData& viewer, CombatData& target, Distance d)
{
	/*
	 * Only change targets if new one is 100 yards closer than previous one.
	 */

	Distance d1 = d + Yard(100);

	if(!viewer.rb->target || (d1 < viewer.rb->targetDistance) || (viewer.rb->target == target.r) )
	{
		viewer.rb->setTarget(target.r, d);

		Distance range;

		if(viewer.r->type.basicType == Artillery)
		{
			/*
			 * If artillery within enemy's decisive range, then HOLD
			 */

			if(d <= target.decRange)
			{
				viewer.rb->changeOrder(ORDER_Hold);
				return True;
			}

			/*
			 * Or if within own decisive range, then consider standing
			 */

			if(!viewer.rb->doneDecideCheck2 && (d <= viewer.decRange))
			{
				viewer.rb->doneDecideCheck2 = True;
				UBYTE r = viewer.rb->doResponseCheck();
				if(r < 50)
				{
					viewer.rb->changeOrder(ORDER_Stand);
					return True;
				}
			}

			range = viewer.effRange / 2;
		}
		else
		{
			/*
			 * If within enemies decisive range
			 */

			if(!viewer.rb->doneDecideCheck2 && (d <= target.decRange))
			{
				viewer.rb->doneDecideCheck2 = True;

				// Do a response check

				UBYTE r = viewer.rb->doResponseCheck();

				if(r < 30)
					viewer.rb->changeOrder(ORDER_Stand);
				else if(r < 60)
				{
					if( (viewer.r->type.basicType != Cavalry) ||
					    ( ((CavInfantryBattle*)viewer.rb)->isMountedInfantry() ) )
					{
						viewer.rb->changeOrder(ORDER_Hold);
					}
					else
						viewer.rb->changeOrder(ORDER_Defend);
				}
				else
					viewer.rb->changeOrder(ORDER_Attack);

				return True;
			}

			range = viewer.effRange;
		}

		/*
		 * If within own range, then consider standing
		 */

		if(!viewer.rb->doneDecideCheck1 && (d <= range))
		{
			viewer.rb->doneDecideCheck1 = True;

			// Do response check

			int r = viewer.rb->doResponseCheck();

			if(r < 50)
			{
				viewer.rb->changeOrder(ORDER_Stand);
				return True;
			}
		}
	}

	return False;
}

/*
 * Attack (aka Charge or rapid advance) orders
 */

Boolean sightEnemyAttack(CombatData& viewer, CombatData& target, Distance d)
{
	/*
	 * Only change targets if new one is 100 yards closer than previous one.
	 */

	Distance d1 = d + Yard(100);

	if(!viewer.rb->target || (d1 < viewer.rb->targetDistance) || (viewer.rb->target == target.r) )
	{
		viewer.rb->setTarget(target.r, d);

		/*
		 * All types of unit do a check when in enemies decisive range
		 */

		if(!viewer.rb->doneDecideCheck1 && (d <= target.decRange))
		{
			viewer.rb->doneDecideCheck1 = True;

			UBYTE r = viewer.rb->doResponseCheck();

			if(viewer.r->type.basicType == Infantry)
			{
			doAsInfantry:
				if(r < 30)
				{
					viewer.rb->changeOrder(ORDER_Stand);
					return True;
				}
				else if(r < 60)
				{
					viewer.rb->changeOrder(ORDER_CautiousAdvance);
					return True;
				}
			}
			else if(viewer.r->type.basicType == Cavalry)
			{
				CavInfantryBattle* c = (CavInfantryBattle*)viewer.rb;
				if(c->isMountedInfantry())
				{
					c->unMount();
					goto doAsInfantry;
				}

				if(r < 25)
				{
					viewer.rb->changeOrder(ORDER_Withdraw);
					return True;
				}
				else if(r < 50)
				{
					viewer.rb->changeOrder(ORDER_Retreat);
					return True;
				}
				else if(r < 75)
				{
					viewer.rb->changeOrder(ORDER_HastyRetreat);
					return True;
				}
			}
			else	// assume artillery
			{
				if(r < 25)
				{
					viewer.rb->changeOrder(ORDER_Withdraw);
					return True;
				}
				else if(r < 50)
				{
					viewer.rb->changeOrder(ORDER_Retreat);
					return True;
				}
				else if(r < 75)
				{
					viewer.rb->changeOrder(ORDER_HastyRetreat);
					return True;
				}
				else
				{
					viewer.rb->changeOrder(ORDER_Stand);
					return True;
				}
			}
		}

		/*
		 * Infantry and Artillery also do a check to consider standing
		 */

		// if(viewer.r->type.basicType != Cavalry)
		if(viewer.r->type.basicType != Infantry)
		{
			Distance range;

			if(viewer.r->type.basicType == Infantry)
				range = PanicRange;
			else
				range = viewer.decRange;

			if(!viewer.rb->doneDecideCheck2 && (d < range))
			{
				viewer.rb->doneDecideCheck2 = True;

				UBYTE r = viewer.rb->doResponseCheck();

				if(r < 50)
				{
					viewer.rb->changeOrder(ORDER_Stand);
					return True;
				}
			}
		}

		/*
		 * Artillery always stand!
		 */

		if(viewer.r->type.basicType == Artillery)
		{
			if(d < viewer.decRange)
				viewer.rb->changeOrder(ORDER_Stand);
		}


	}

	return False;
}

/*
 * Enemy sighted while unit is standing or holding
 */

Boolean sightEnemyStand(CombatData& viewer, CombatData& target, Distance d)
{
	/*
	 * If this target is closer than our current target
	 */

	/*
	 * Only change targets if new one is 100 yards closer than previous one.
	 */

	Distance d1 = d + Yard(100);

	if(!viewer.rb->target || (d1 < viewer.rb->targetDistance) || (viewer.rb->target == target.r) )
	{
		viewer.rb->setTarget(target.r, d);
		viewer.rb->setCombatMode(CM_Firing);

		if(!viewer.rb->doneDecideCheck1)
		{
			viewer.rb->doneDecideCheck1 = True;

			Distance decideDist;

			if(viewer.r->type.basicType == Artillery)
				decideDist = PanicRange;
			else
				decideDist = viewer.decRange;

			if(d <= decideDist)
			{
				if(viewer.rb->doHoldResponseCheck())
					return True;
			}
		}
	}
	return False;
}

Boolean sightEnemyDefend(CombatData& viewer, CombatData& target, Distance d)
{
	/*
	 * Will not open fire until enemy within Decisive range
	 * or cannister range for artillery
	 */

	Distance decideDist;

	if(viewer.r->type.basicType == Artillery)
 		decideDist = CannisterRange;
 	else
 		decideDist = viewer.decRange;

	if(d <= decideDist)
	{
		/*
		 * If this target is closer than our current target
		 */

		/*
		 * Only change targets if new one is 100 yards closer than previous one.
		 */

		Distance d1 = d + Yard(100);

		if(!viewer.rb->target || (d1 < viewer.rb->targetDistance) || (viewer.rb->target == target.r) )
		{
			viewer.rb->setTarget(target.r, d);

			/*
			 * Infantry must make a response check before opening fire
			 */

			if( (viewer.r->type.basicType != Artillery) && !viewer.rb->doneDecideCheck1)
			{
				viewer.rb->doneDecideCheck1 = True;
#ifdef DEBUG
				bLog.printf("%s does response check before firing",
					viewer.r->getName(True));
#endif
				if(viewer.rb->doHoldResponseCheck())
					return True;
			}

			viewer.rb->setCombatMode(CM_Firing);

			/*
			 * Artillery must do a check when within 50 yards
			 */

			if((viewer.r->type.basicType == Artillery) && !viewer.rb->doneDecideCheck1)
			{
				viewer.rb->doneDecideCheck1 = True;

				if(d <= PanicRange)
				{
					if(viewer.rb->doHoldResponseCheck())
						return True;
				}
			}
		}
	}
	else
	{
		/*
		 * Check for enemy stopping within effective range but outside decisive range
		 */

		if(!viewer.rb->target && !target.rb->moving)
		{
#ifdef DEBUG
			bLog.printf("%s does response check to consider changing to hold",
				viewer.r->getName(True));
#endif
			if(game->gameRand(100) < 50)		// This is the response check
			{
				// Change orders to hold

				viewer.rb->changeOrder(ORDER_Hold);
				return True;
			}
		}
	}

	return False;
}

/*
 * Most of the withdraw/retreat logic is handled in the movement section (batmove)
 * All that happens here is that the closest enemy unit is set as the target
 * and a flag set if within the range that they should turn and fire.
 */
 
Boolean sightEnemyWithdraw(CombatData& viewer, CombatData& target, Distance d)
{
	/*
	 * Only change targets if new one is 100 yards closer than previous one.
	 */

	Distance d1 = d + Yard(100);

	if(!viewer.rb->target || (d1 < viewer.rb->targetDistance) || (viewer.rb->target == target.r) )
	{
		viewer.rb->setTarget(target.r, d);

		Distance range;

		if(viewer.r->type.basicType == Cavalry)
			range = target.decRange;
		else
			range = viewer.effRange;

		viewer.rb->canTurn = (range < d);
	}

	return False;
}


/*
 * Set a new target for a regiment
 */

void RegimentBattle::setTarget(Regiment* r, Distance d)
{
	if(r != target)
	{
		target = r;
		doneDecideCheck1 = False;
		doneDecideCheck2 = False;
#ifdef DEBUG
	bLog.printf("%s has set its target to %s at %d yards",
		unit->getName(True),
		r->getName(True),
		unitToYard(d));
#endif
	}
	targetDistance = d;

}


void RegimentBattle::loseTarget()
{
	if(target)
	{
#ifdef DEBUG
		bLog.printf("%s has lost its target %s",
			unit->getName(True),
			target->getName(True));
#endif 
	
		setCombatMode(CM_None);
		target = 0;
		doneDecideCheck1 = False;
		doneDecideCheck2 = False;
		canTurn = False;

		/*
	 	 * Reset order to commander's order
	 	 */

		if(unit->superior->battleInfo)
			battleOrder = unit->superior->battleInfo->battleOrder;
	}
}

void RegimentBattle::setCombatMode(CombatMode newMode)
{
	if(newMode != combatMode)
	{
#ifdef DEBUG
		static char* modeNames[] = {
			"None",
			"Firing",
			"Charging",
			"Melee"
		};

		bLog.printf("%s changes mode from %s to %s",
			unit->getName(True),
			modeNames[(unsigned)combatMode],
			modeNames[newMode]);
#endif

#if 0
		/*
		 * Clean up old mode if necessary
		 */

		if(combatMode != CM_None)
			adjustInCombat(-1);
#endif

		if(newMode == CM_None)
			setInCombat();
		else
			clearInCombat();


		/*
		 * set new mode
		 */

		combatMode = newMode;

		/*
		 * Initialise any variables for new mode (e.g. load weapons, etc)
		 */

		switch(newMode)
		{
		case CM_Firing:
			Regiment* r = getRegiment();

			/*
			 * Force cavalry to be unmounted, and artillery unlimbered
			 */

			if(r->type.basicType == Artillery)
			{
				ArtilleryBattle* a = (ArtilleryBattle*)this;
				a->unLimber();
			}
			else if(r->type.basicType == Cavalry)
			{
				CavInfantryBattle* c = (CavInfantryBattle*)this;
				c->unMount();
			}
			break;
		}
#if 0
		if(newMode != CM_None)
			adjustInCombat(+1);
#endif
	}
}

void RegimentBattle::changeOrder(OrderMode newOrder)
{
	if(newOrder != battleOrder.mode)
	{
#ifdef DEBUG
		bLog.printf("%s has changed its order from %s to %s",
			unit->getName(True),
			getOrderStr(battleOrder),
			getOrderStr(newOrder));
#endif
		battleOrder.mode = newOrder;

		doneDecideCheck1 = False;
		doneDecideCheck2 = False;
		canTurn = False;

		setCombatMode(CM_None);

		if( (getBasicOrder(newOrder) == ORDERMODE_Withdraw) &&
			unit->superior->battleInfo &&
			(unit->superior->battleInfo->battleOrder.mode != ORDER_Withdraw))
		{
			/*
			 * Find a suitable destination
			 *
			 * If has a target then make it a few hundred yards in opposite
			 *   direction.
			 * else
			 *   about face for a few hundred yards (better would be to scan for enemy)
			 */

			Wangle w = menFacing;
			if(target && target->battleInfo)
				w = directionLocation(where, target->battleInfo->where);
			w += 0x8000;

			Location l = where;

			Distance d;

			switch(newOrder)
			{
			case ORDER_Withdraw:
				d = Yard(50);
				break;

			case ORDER_Retreat:
				d = Yard(100);
				break;

			case ORDER_HastyRetreat:
				d = Yard(200);
				break;
			}

			l.x += mcos(d, w);
			l.y += msin(d, w);

			battleOrder.destination = l;


			// if(battle->ai && (unit->getSide() != game->playersSide))
			if(battle->ai && game->isAI(sideIndex(unit->getSide())))
			{
				battle->ai->UnitWithdrawing(unit->getTopCommand());
			}
		}
	}											
}

void RegimentBattle::changeFormation(Formation newFormation)
{
	if(newFormation != wantFormation)
	{
#ifdef DEBUG
		static char* formationStr[] = {
			"Column",
			"Line"
		};

		bLog.printf("%s has changed formation from %s to %s",
			unit->getName(True),
			formationStr[wantFormation],
			formationStr[newFormation]);
#endif
		wantFormation = newFormation;
	}
}

void RegimentBattle::startRouting()
{
#ifdef DEBUG
	bLog.printf("%s routs", unit->getName(True));
#endif

	routed = True;
	
	Regiment* r = getRegiment();
	r->morale = 0;

	/*
	 * Give it a new direction away from its target
	 */

	Wangle w = menFacing;
	if(target && target->battleInfo)
		w = directionLocation(where, target->battleInfo->where);
	w += 0x8000;

	menFacing = formationFacing = w;

	movedDistance = 0;

	/*
	 * Increase target's morale
	 */

	if(target && target->battleInfo)
	{
		RegimentBattle* rb = (RegimentBattle*) target->battleInfo;
		rb->increaseMorale(100);
	}

}

void RegimentBattle::stopRouting()
{
#ifdef DEBUG
	bLog.printf("%s stops routing", unit->getName(True));
#endif

	routed = False;

	Regiment* r = getRegiment();
	r->morale++;
	menFacing = formationFacing = menFacing + 0x8000;
	setCombatMode(CM_None);
	movedDistance = 0;
}

/*
 * Response check as defined for stand, hold and defend
 * returns True if order has changed
 */

Boolean RegimentBattle::doHoldResponseCheck()
{
	UBYTE r = doResponseCheck();
	OrderMode oldMode = battleOrder.mode;

	if(r < 10)
		changeOrder(ORDER_Stand);
	else if(r < 20)
		changeOrder(ORDER_Hold);
	else if(r < 30)
		changeOrder(ORDER_Withdraw);
	else if(r < 40)
		changeOrder(ORDER_Retreat);
	else if(r < 50)
		changeOrder(ORDER_HastyRetreat);
	else if(r < 60)
		startRouting();
		// routed = True;

	return (battleOrder.mode != oldMode);
}

void RegimentBattle::doWithdrawCheck()
{
	UBYTE r = doResponseCheck();
	if(r <= 50)
		startRouting();
	else if(r < 100)
		changeOrder(ORDER_HastyRetreat);
	else if(r < 150)
		changeOrder(ORDER_Retreat);
}

void RegimentBattle::doRetreatCheck()
{
	UBYTE r = doResponseCheck();
	if(r <= 80)
		startRouting();
	else if(r < 150)
		changeOrder(ORDER_HastyRetreat);
}

void RegimentBattle::doHastyRetreatCheck()
{
	UBYTE r = doResponseCheck();
	if(r <= 100)
		startRouting();
}

/*
 * Return response value between 0..255
 * This will depend on morale, etc.
 */

UBYTE RegimentBattle::doResponseCheck()
{
#ifdef DEBUG
	bLog.printf("%s does response check", unit->getName(False));
#endif

	Regiment* r = getRegiment();

	int value = r->morale;

#ifdef DEBUG
	bLog.printf("---base morale=%d", value);
#endif

	/*
	 * Apply modifiers
	 */

	value += rrcExperience[getExperienceEnum(r->experience)];
	if(game->getDifficulty(Diff_Fatigue) >= Complex3)
		value += rrcFatigue[getFatigueEnum(r->fatigue)];
	value += rrcSupply[getSupplyEnum(r->supply)];

	UBYTE val;

	/*
	 * Brigade's General
	 */

	General* g = r->superior->general;
	if(g)
		val = getAbilityEnum(g->ability);
	else
		val = 0;		// Worst ability
	value += rrcBrigCommander[val];

	/*
	 * Top Commander's General
	 */

	g = r->getTopCommand()->general;
	if(g)
		val = getAbilityEnum(g->ability);
	else
		val = 0;		// Worst ability
	value += rrcTopCommander[val];

#ifdef DEBUG
	bLog.printf("---after attribute modifiers=%d", value);
#endif

	/*
	 * Still need to do orders/terrain
	 */

#ifdef DEBUG
	bLog.printf("---after order/terrain modifiers=%d", value);
#endif

	/*
	 * Apply random stuff
	 *
	 * Using 2 random numbers biases it towards centre
	 * We could speed it up a tad if we want it linear by using:
	 *			game->gameRand(40) - 20
	 */

	value += game->gameRand(20) - game->gameRand(20);

#ifdef DEBUG
	bLog.printf("---final result=%d", value);
#endif

	if(value < 0)
		value = 0;
	else if(value > 0xff)
		value = 0xff;

	return value;
}

/*
 * Find which entry of a table a value belongs
 */

int getTableValue(int value, int* table)
{
	int i = 0;
	while(table[i] && (table[i] < value))
		i++;
	return i;
}

#ifdef DEBUG

int logFireFactor(int prevFF, int ff, const char* s)
{
	int change = ff - prevFF;
	if(change)
	{
		bLog.printf("FF %c%d=%d for %s",
			(change < 0) ? '-' : '+',
			change,
			ff,
			s);

	}

	prevFF = ff;
	return ff;
}

#define logFF(s) { prevFF = logFireFactor(prevFF, ff, s); }
			
#endif


/*
 * Perform some firing!
 */

void RegimentBattle::fireGuns()
{
	if(target && target->battleInfo)
	{
		if(inLOS(where, target->battleInfo->where, Yard(200)))
		{
			fired = True;			// Tell the display to add some smoke

#ifdef DEBUG
			game->simpleMessage("%s fires %d rounds\rat %s",
				unit->getName(False),
				(int)fireCount,
				target->getName(False));
			bLog.printf("%s fires %d rounds at %s",
				unit->getName(True),
				(int)fireCount,
				target->getName(True));
#endif


			/*
		 	 * Calculate results
		 	 * Set up fire factor based on the type of unit firing
		 	 */

			int ff = 0;			// Fire Factor
			RegimentBattle* rb = (RegimentBattle*) target->battleInfo;
			Regiment* r = getRegiment();

#ifdef DEBUG
			int prevFF = ff;
#endif

			/*
		 	 * Start a sound effect!.. but only allow it every 5 seconds of real time
		 	 */

			if(timer->getCount() >= soundCount)
			{
				if(r->type.basicType == Artillery)
					battle->sounds->playSound(SND_CannonFire, where);
				else
					battle->sounds->playSound(SND_GunFire, where);

				soundCount = timer->getCount() + timer->ticksPerSecond * 5;
			}

			Distance range = distanceLocation(where, rb->where);
			int rangeYards = unitToYard(range);
			int rangeType;

			if(r->type.basicType == Artillery)
				rangeType = getTableValue(rangeYards, artilleryRanges);
			else
				rangeType = getTableValue(rangeYards, infantryRanges);

			ff = firingModifiers[r->type.basicType][r->type.subType][rangeType];
#ifdef DEBUG
			logFF("Firing Modifier");
#endif

			ff += fireExperience[getExperienceEnum(r->experience)];
#ifdef DEBUG
			logFF("Experience");
#endif
			ff += fireSupply[getSupplyEnum(r->supply)];
#ifdef DEBUG
			logFF("Supply");
#endif
			if(game->getDifficulty(Diff_Fatigue) >= Complex2)
				ff += fireFatigue[getFatigueEnum(r->fatigue)];
#ifdef DEBUG
			logFF("Fatigue");
#endif
			ff += fireMorale[getMoraleEnum(r->morale)];
#ifdef DEBUG
			logFF("Morale");
#endif

			// Target type

			// Mounted?

#if 0		// Bug... fixed for master's version
			if(r->type.basicType == Cavalry)
			{
				CavInfantryBattle* cb = (CavInfantryBattle*) this;

				if(cb->isMounted())
				{
					/*
				 	 	* Charging Mounted -2
				 	 	*/

					if(combatMode == CM_Charging)
						ff -= 2;
					else
						ff += 2;
				}
			}
			else if(r->type.basicType == Artillery)
			{
				ArtilleryBattle* ab = (ArtilleryBattle*) this;

				if(ab->isLimbered())
					ff += 1;
			}
#else	// Supposed to be using target!!!
			if(target->type.basicType == Cavalry)
			{
				CavInfantryBattle* cb = (CavInfantryBattle*) rb;

				if(cb->isMounted())
				{
					/*
				 	 * Charging Mounted -2
				 	 */

					if(combatMode == CM_Charging)
						ff -= 2;
					else
						ff += 2;
				}
			}
			else if(target->type.basicType == Artillery)
			{
				ArtilleryBattle* ab = (ArtilleryBattle*) rb;

				if(ab->isLimbered())
					ff += 1;
			}
#ifdef DEBUG
			logFF("Target Type");
#endif

#endif

			// Target terrain
			// ...

			const TerrainVal* terrain = getTerrainType(rb->where);
			ff += terrain->fireModifier;
#ifdef DEBUG
			logFF("Target Terrain");
#endif

			/*
			 * Add something if higher than target
			 */

			if(getTerrainHeight(where) > getTerrainHeight(rb->where))
				ff += 3;
#ifdef DEBUG
			logFF("Terrain Height");
#endif

#ifdef DEBUG
			bLog.printf("Firing Factor = %d", ff);
#endif

			/*
	 	 	 * Calculate number of casualties
	 	 	 */

			while(fireCount)
			{
				fireCount--;

				// ff = ff + game->gameRand(5) - game->gameRand(5);
				int fireFactor = ff + game->gameRand(10) - 5;		// Allow more variation
#ifdef DEBUG
				bLog.printf("Firing Factor after rand5 = %d", fireFactor);
#endif

				if(fireFactor > 0)
				{
					Strength losses;

					if(r->type.basicType == Artillery)
					{
						// fireFactor is 1/10 chance of each gun hitting something

						// numberFiring = MenPerGun * 2;

						int nGuns = r->strength / MenPerGun;

#ifdef DEBUG
						bLog.printf("%ld men, %d guns",
							(long) r->strength, nGuns);
#endif

						losses = (nGuns * fireFactor) / 10;

					}
					else		// Infantry firing based on 100 men
					{
						int numberFiring = 100;

#ifdef DEBUG
						bLog.printf("Number firing = %ld / %d = %ld",
							r->strength, numberFiring, r->strength / numberFiring);
#endif

						losses = 1 + (fireFactor * r->strength - 1) / numberFiring;
					}

					/*
					 * Increase morale
					 */

					if(target->strength)
					{
						int mgain = (losses * MaxAttribute * MoraleForKilling) / target->strength;
						increaseMorale(mgain);
					}

					rb->kill(losses);
				}
			}
		}
#ifdef DEBUG
		else
			bLog.printf("%s's target (%s) is out of LOS",
				unit->getName(True), target->getName(True));
#endif
	}
#ifdef DEBUG
	else
		throw GeneralError("%s firing with no target", unit->getName(True));
#endif
}






/*==============================================================
 * Melee
 *
 * This is called whenever 2 regiments are overlapping
 *
 * If units already meleeing with one another
 *		Ignore
 * ELSE
 *    For each regiment
 *      IF not already meleeing
 *        Break off any previous targets
 *        Set Mode to Meleeing
 *        Set target
 *        Set melee timer
 *        Set sound flag
 * If sound flag was set
 *   Start a sound effect
 *
 * Elsewhere a routine is called which checks if a regiment is
 * meleeing and simply resets the combat mode to CM_None when the
 * time is up.
 *
 * Question is, when do we calculate results?
 *   If we do it at the start then it gives a withdrawing unit opportunity
 *   to escape before the meleeTime is up and any casulaties are shown
 *   immediately.
 *
 *   To do it incrementally is awkward and requires more variables
 *
 *   To do it at the end would require another flag to allow withdrawing
 *   units to escape... unless we don't allow melee to occur with
 *   withdrawing/routing units?
 */

void doMelee(CombatData& d1, CombatData& d2)
{
	Boolean newMelee = False;
	UWORD newTime = battle->gameTime.ticks / GameTicksPerMinute + MeleeFightTime;

	if(d1.rb->combatMode != CM_Melee)
	{
#ifdef DEBUG
		if(d1.rb->target)
			bLog.printf("%s loses target %s because it is in melee",
				d1.rb->unit->getName(True),
				d1.rb->target->getName(True));
#endif
		d1.rb->loseTarget();
		d1.rb->setCombatMode(CM_Melee);
		d1.rb->setTarget(d2.r, 0);
		d1.rb->meleeTime = newTime;
		newMelee = True;
	}
	if(d2.rb->combatMode != CM_Melee)
	{
#ifdef DEBUG
		if(d2.rb->target)
			bLog.printf("%s loses target %s because it is dead or has no battleInfo",
				d2.rb->unit->getName(True),
				d2.rb->target->getName(True));
#endif
		d2.rb->loseTarget();
		d2.rb->setCombatMode(CM_Melee);
		d2.rb->setTarget(d1.r, 0);
		d1.rb->meleeTime = newTime;
		newMelee = True;
	}

	if(newMelee)
		battle->sounds->playSound(SND_Melee, d1.rb->where);

	/*
	 * Work out some results...
	 */

	int damage1 = calculateMeleeDamage(d1, d2);
	int damage2 = calculateMeleeDamage(d2, d1);

	d1.rb->kill(damage2);
	d2.rb->kill(damage1);

	/*
	 * Increase morales for killing enemy
	 */

	if(d2.r->strength)
	{
		int mgain = (damage1 * MaxAttribute * MoraleForKilling) / d2.r->strength;
		d1.rb->increaseMorale(mgain);
	}

	if(d1.r->strength)
	{
		int mgain = (damage2 * MaxAttribute * MoraleForKilling) / d1.r->strength;
		d2.rb->increaseMorale(mgain);
	}

	/*
	 * Do some kind of morale check to see if they will followup/retreat, etc
	 */

}


/*
 * Work out how many casualties a regiment can cause if in melee with
 * someone else
 */

Strength calculateMeleeDamage(CombatData& attacker, CombatData& defender)
{
	int value;
	
#ifdef DEBUG
	bLog.printf("Melee results inflicted by %s against %s",
		attacker.r->getName(True),
		defender.r->getName(True));
#endif

	/*
	 * Attacker Type 
	 */

	if(attacker.rb->isMounted)
		value = meleeMountedType[attacker.r->type.basicType][attacker.r->type.subType];
	else
		value = meleeUnmountedType[attacker.r->type.basicType][attacker.r->type.subType];

#ifdef DEBUG
	bLog.printf("---Basic Effectiveness: %d", value);
#endif

	/*
	 * Experience
	 */

	value += meleeExperience[getExperienceEnum(attacker.r->experience)];
	if(game->getDifficulty(Diff_Fatigue) >= Complex2)
		value += meleeFatigue[getFatigueEnum(attacker.r->fatigue)];
	value += meleeMorale[getMoraleEnum(attacker.r->morale)];

#ifdef DEBUG
	bLog.printf("---After Attributes: %d", value);
#endif

	/*
	 * Opponent type
	 */




	/*
	 * Opponent Terrain
	 */

#ifdef DEBUG
	bLog.printf("---After opponent: %d", value);
#endif

	value += game->gameRand(5) - game->gameRand(5);

#ifdef DEBUG
	bLog.printf("---Final value: %d", value);
#endif

	Strength result;

	if(value < 0)
		result = 0;
	else
		result = (attacker.r->strength * value + 99) / 100;

#ifdef DEBUG
	bLog.printf("---Resulting in %ld casualties", result);
#endif

	return result;
}

/*
 * Called for any unit in CM_Melee mode
 *
 * Checks the current time and clears the melee flag at correct time
 */

void RegimentBattle::doOngoingMelee()
{
	GameTicks endTime = meleeTime * GameTicksPerMinute;

	if(battle->gameTime.ticks >= endTime)
	{
#ifdef DEBUG
		bLog.printf("Melee finishing with %s", unit->getName(True));
#endif
		loseTarget();
	}
}

/*
 * Kill some men in a regiment
 */

void RegimentBattle::kill(Strength losses)
{
	Regiment* r = getRegiment();

	if(losses > r->strength)
		losses = r->strength;

	/*
	 * Adjust morale
	 * Knock off morale depending on ratio of losses to  strength
	 */

	if(r->strength)
	{
		int mloss = (losses * MaxAttribute * MoraleForDying) / r->strength;
		reduceMorale(mloss);
	}

	/*
	 * Adjust strength/straggers/casualties
	 */

	r->strength -= losses;

	Strength cas = (losses * (game->gameRand(10) + 15)) / 100;
	Strength strag = losses - cas;

	r->casualties += cas;
	r->stragglers += strag;

#ifdef DEBUG
	bLog.printf("Casualties for %s", r->getName(True));
	bLog.printf("---Losses = %ld, stragglers: %ld, casualties: %ld",
		losses, strag, cas);
	bLog.printf("---Result is Strength: %ld, Stragglers: %ld, Casualties: %ld",
		r->strength,
		r->stragglers,
		r->casualties);
#endif

	if(r->strength < 50)
		startRouting();

	if(r->strength == 0)
	{
		Unit* u = r;
		Boolean notDead = False;

		if(combatMode != CM_None)
			// adjustInCombat(-1);
			clearInCombat();

		while(u && !notDead && u->battleInfo)
		{
#ifdef DEBUG
			bLog.printf("---%s is DEAD!", u->getName(True));
#endif
			u->battleInfo->isDead = True;


			u = u->superior;

			if(u)
			{
				Unit* c = u->child;
				while(c)
				{
					if(c->battleInfo && !c->battleInfo->isDead)
					{
						notDead = True;
						break;
					}
					c = c->sister;
				}
			}
		}

		/*
		 * Get the brigade to resort any remaining regiments
		 */

		if(r->superior->battleInfo)
		{
			BrigadeBattle* bb = (BrigadeBattle*) (r->superior->battleInfo);
			if(bb && !bb->isDead)
				bb->setupRegiments(False);
		}
	}

	/*
	 * Make some corpses
	 */

	Strength corpseRatio = menPerSprite();
	Strength corpses = corpseCount + losses;
	int newCorpses = corpses / corpseRatio;

#ifdef DEBUG_CORPSES
	bLog.printf("---corpses=%ld, ratio=%ld, corpseCount=%d, newCorpses=%d",
		corpses,
		corpseRatio,
		(int) corpseCount,
		newCorpses);
#endif

	if(newCorpses)
	{
		makeCorpses(newCorpses, r);
		corpses -= newCorpses * corpseRatio;
	}

	corpseCount = corpses ;

	// Also add to running total for battle

	battle->statsUpdate(r, losses);
}

/*
 * Adjust battle morale values
 */

void RegimentBattle::reduceMorale(int diff)
{
	Regiment* r = getRegiment();

	if(r->morale <= diff)
		startRouting();
	else
		r->morale -= diff;

#ifdef DEBUG
	bLog.printf("Morale reduced by %d to %d for %s", diff, (int) r->morale, r->getName(True));
#endif
}

void RegimentBattle::increaseMorale(int diff)
{
	Regiment* r = getRegiment();

	if(routed && diff)
		stopRouting();

	if((r->morale + diff) >= MaxAttribute)
		r->morale = MaxAttribute;
	else
		r->morale += diff;


#ifdef DEBUG
	bLog.printf("Morale increased by %d to %d for %s", diff, (int) r->morale, r->getName(True));
#endif
}

