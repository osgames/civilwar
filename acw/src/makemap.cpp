/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Create Raw Map Bitmap from hugemap.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/06/21  18:49:27  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <fstream.h>
#include <stdio.h>
#include "image.h"
#include "ilbm.h"
#include "makename.h"
#include "bitmap.h"

/*
 * Usage:
 *		makemap hugemap.lbm destname.map
 */

void main(int argc, char* argv[])
{
	try
	{
		/*
		 * Introduce ourselves
		 */


		cout << "MakeMap - Converts hugemap into map file used in game" << endl;

		if(argc != 3)
		{
			cout << "Usage:\nmakemap source dest\ne.g. makemap hugemap.lbm mapzoom.bm" << endl;
			return;
		}

		/*
	 	 * Create the destination bitmap
	 	 */

		Image srcBM;
		Palette palette;

		/*
	 	 * Read the hugemap
	 	 */

		char* inName = argv[1];
		cout << "Reading " << inName << flush;
		readILBM(inName, &srcBM, &palette);
		cout << " " << srcBM.getWidth() << " by " << srcBM.getHeight() << endl;

		/*
	 	 * Write out result
	 	 */

		char* outName = argv[2];
		char outFileName[FILENAME_MAX];
		makeFilename(outFileName, outName, ".BM", False);

		ofstream stream(outFileName, ios::out | ios::binary);
		if(stream)
		{
			cout << "Writing " << outFileName << endl;

			/*
			 * Make header
			 */

			BM_Header header;

 			memset(&header, 0, sizeof(header));
			
			header.setSize(srcBM.getWidth(), srcBM.getHeight());

			/*
			 * Write header
			 */

			stream.write((char*) &header, sizeof(header));

			/*
			 * Write data
			 */

			stream.write(srcBM.getAddress(), srcBM.getWidth() * srcBM.getHeight());

		}
		else
			cout << "Couldn't create " << outFileName << endl;
	}
 	catch(GeneralError e)
 	{
		cout << "Untrapped Error: ";
		if(e.get())
			cout << e.get();
		else
			cout << "No description";
		cout << endl;
 	}
 	catch(...)
 	{
 		cout << "Caught some kind of error" << endl;
 	}
}

