/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Main Display area of the CAL screen
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.1  1994/02/10  22:57:14  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


#include "calmain.h"
#include "calicon.h"
#include "calshow.h"
#include "ob.h"
#if !defined(CAMPEDIT) && !defined(TESTCAMP)
#include "unit3d.h"
#endif

/*
 * Global CAL Screen
 */

CAL* cal = 0;

/*
 * Display Constructor
 */


CAL_MainDisplay::CAL_MainDisplay(CAL* c) :
	Icon(c, Rect(CAL_X, CAL_Y, CAL_WIDTH, CAL_HEIGHT)),
	CALBase(c)
{
}

/*
 * Destruct CAL_MainDisplay
 */

CAL_MainDisplay::~CAL_MainDisplay()
{
	removeIcons();
}

void CAL_MainDisplay::removeIcons()
{
	// icons.clearAndDestroy();

	while(icons.entries())
		delete icons.get();

}

void CAL_MainDisplay::addIcon(CALIcon* i)
{
	icons.append(i);
}


/*
 * Is unit valid to be clicked on?
 */

Boolean CAL::unitIsValid(Unit* u)
{
	if(!u)
		return False;

	if(campaignMode)
		return True;

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
	if(!u->battleInfo)
		return False;
		
	if(u->battleInfo->isDead)
		return False;
#endif

	return True;
}
