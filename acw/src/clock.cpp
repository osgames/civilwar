/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Clock Display
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.15  1994/06/09  23:32:59  Steven_Green
 * Changed Fonts
 *
 * Revision 1.13  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.8  1994/04/22  22:14:22  Steven_Green
 * Icon Handling tidied up.
 *
 * Revision 1.1  1994/02/28  23:04:17  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
 
#include "clock.h"
#include "layout.h"
#include "colours.h"
#include "trig.h"
#include "text.h"
#include "game.h"
#include "sprlib.h"
#include "gamelib.h"
#include "system.h"
#include "screen.h"
#include "language.h"
#include "hintstat.h"
#include "menudata.h"

#define Font_Date Font_JAME_5

ClockIcon::ClockIcon(IconSet* set, Point p, TimeInfo* t, GameTime* gt, Boolean wantSecHand) :
	Icon(set, Rect(p, Point(MAIN_ICON_W,MAIN_ICON_H)))
{
	theTime = t;
	gameTime = gt;
	wantSeconds = wantSecHand;
}


void drawHand(Region& bm, const Point& centre, Wangle w, SDimension length, SDimension length2, Colour col)
{
	Point p1 = centre + Point(+msin(length, w), -mcos(length, w));
	Point p2 = centre + Point(-msin(length2, w), +mcos(length2, w));
	// bm.line(center + p2, center + p1, col);

	bm.line(centre + Point( 0, 1), p1, col+1);
	bm.line(centre + Point( 0,-1), p1, col-1);
	bm.line(centre + Point( 1, 0), p1, col+1);
	bm.line(centre + Point(-1, 0), p1, col-1);
	bm.line(centre + Point( 0, 1), p2, col+1);
	bm.line(centre + Point( 0,-1), p2, col-1);
	bm.line(centre + Point( 1, 0), p2, col+1);
	bm.line(centre + Point(-1, 0), p2, col-1);

	bm.line(centre, p1, col);
	bm.line(centre, p2, col);
}


void ClockIcon::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));

	game->sprites->drawSprite(&bm, Point(0,0), CLOCK_Face);

	/*
	 * Day/Night display
	 */

	int dayNight = ((theTime->hour + 15) % 24) / 6;

	game->sprites->drawAnchoredSprite(&bm, Point(65,60), GameSprites(CLOCK_Day + dayNight));


	/*
	 * Date
	 */

	Region bm1 = bm;
	bm1.setClip(bm.getX() + 43, bm.getY() + 66, 45, 6);
	TextWindow window(&bm1, Font_Date);
	// window.setPosition(Point(43,66));
	window.setFormat(TW_C);
	window.setColours(Black);
	window.wprintf("%d %s %02d",
		theTime->day,
		theTime->monthStr,
		theTime->year % 100);

	/*
	 * Draw Time with lines until graphics are done
	 */

	const Point center = Point(65,63);
	const SDimension hourLength = 26;
	const SDimension minLength = 31;
	const SDimension secLength = 33;

	/*
	 * Hour Hand
	 */

	Wangle w = (((theTime->hour % 12) * 60 + theTime->minute) * 0x10000) / (60 * 12);
#if 0
	Point p1 = Point(+msin(hourLength, w), -mcos(hourLength, w));
	Point p2 = Point(-msin(3, w), +mcos(3, w));
	bm.line(center + p2, center + p1, DGrey);
#else
	drawHand(bm, center, w, hourLength, 6, Greye);
#endif

	/*
	 * Minute Hand
	 */

	w = (theTime->minute * 0x10000) / 60;
#if 0
	p1 = Point(+msin(minLength, w), -mcos(minLength, w));
	p2 = Point(-msin(3, w), +mcos(4, w));
	bm.line(center + p2, center + p1, White);
#else
	drawHand(bm, center, w, minLength, 8, Greyc);
#endif

	/*
	 * Second hand
	 */

	if(wantSeconds)
	{
		w = (theTime->second * 0x10000) / 60;
#if 0
		p1 = Point(+msin(secLength, w), -mcos(secLength, w));
		p2 = Point(-msin(3, w), +mcos(5, w));
		bm.line(center + p2, center + p1, LGrey);
#else
	drawHand(bm, center, w, secLength, 10, Greya);
#endif
	}

	bm.plot(center, Black);


	machine.screen->setUpdate(bm);
}

