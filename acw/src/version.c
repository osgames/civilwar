/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Version Number
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:54  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

// comment this out for release version

// #define BETA

/*
 * To change the version number:
 *   Update V_VER V_REL V_MIN
 *   Update VERSION
 */

#define V_VER 2			// Version Number
#define V_REL 0         // Release Number
#define V_MIN 3         // Minor Release Number

#ifdef DEBUG
#define V_DEB 1
#define VS_DEB "d"
#else
#define V_DEB 0
#define VS_DEB ""
#endif

#define VERSION "2.0.3" VS_DEB

// #ifdef DEBUG
//  #define VERSIONSTRING VERSION " debug, " __DATE__ " " __TIME__
// #else
  #define VERSIONSTRING VERSION ", " __DATE__
// #endif

/*
 * Use this next bit for an external program to find what version it is
 * (i.e. do a search for $VERSION=)
 */

static const char versionID[] = "\r\n$VERSION=" VERSIONSTRING "\r\n";


#if defined(BETA)
static const char versionString[] = VERSIONSTRING ", Private beta version... do not distribute!";
#else
static const char versionString[] = VERSIONSTRING;
#endif

const char* getVersionStr()
{
 	return versionString;
}


#define LVERSION ((V_VER << 24) + (V_REL << 16) + (V_MIN << 8) + V_DEB)

unsigned long getVersionLong()
{
	return LVERSION;
}
