/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Modem and Serial Setup dialogues
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/03/20 04:23:40  dor10122
 * Renamed 'src/config.cpp' and 'include/config.h' to 'src/conffile.cpp' and
 * 'include/conffile.cpp' due to name conflict with 'config.h' generated by
 * autoconf.
 *
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#if !defined(COMPUSA_DEMO)

#include <stdlib.h>
#include "ser_cfg.h"
#include "dialogue.h"
#include "menudata.h"
#include "language.h"
#include "strutil.h"
#include "conffile.h"
#include "gamecfg.h"
#include "system.h"
#include "mouselib.h"

static char* portTypeNames[] = {
	"None",
	"8250",
	"16450",
	"16550"
};

static const char* baudNames[] = {
	"1200",
	"2400",
	"4800",
	"9600",
	"19200",
	"38400",
	"57600",
	"115200",
	0
};

static char defaultDialString[] = "ATD";
static char defaultAnswerString[] = "ATS0=1";
static char defaultInitString[] = "ATZ^M~~ATE0V1Q0";
static char defaultPhoneString[] = "";

static int baudRealRates[] = {
	1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200
};

/*
 * Dialogue box for selecting Comms Parameters
 */

static DialItem commSetupItems[] = {
	DialItem(Dial_FreeString,	CI_None,		Rect(  0,  0,256,16)),
	
	DialItem(Dial_Button,		CI_Com1,		Rect(  4, 20, 62,15)),	// Com1:
	DialItem(Dial_Button,		CI_Com1,		Rect( 66, 20, 62,15)),	// I/O: 02a8
	DialItem(Dial_Button,		CI_Com1IRQ,	Rect(128, 20, 62,15)),	// IRQ4
	DialItem(Dial_Button,		CI_Com1,		Rect(190, 20, 62,15)),	// 8250

	DialItem(Dial_Button,		CI_Com2,		Rect(  4, 36, 62,15)),
	DialItem(Dial_Button,		CI_Com2,		Rect( 66, 36, 62,15)),
	DialItem(Dial_Button,		CI_Com2IRQ,	Rect(128, 36, 62,15)),
	DialItem(Dial_Button,		CI_Com2,		Rect(190, 36, 62,15)),

	DialItem(Dial_Button,		CI_Com3,		Rect(  4, 52, 62,15)),
	DialItem(Dial_Button,		CI_Com3,		Rect( 66, 52, 62,15)),
	DialItem(Dial_Button,		CI_Com3IRQ,	Rect(128, 52, 62,15)),
	DialItem(Dial_Button,		CI_Com3,		Rect(190, 52, 62,15)),

	DialItem(Dial_Button,		CI_Com4,		Rect(  4, 68, 62,15)),
	DialItem(Dial_Button,		CI_Com4,		Rect( 66, 68, 62,15)),
	DialItem(Dial_Button,		CI_Com4IRQ,	Rect(128, 68, 62,15)),
	DialItem(Dial_Button,		CI_Com4,		Rect(190, 68, 62,15)),


	DialItem(Dial_Button,		CI_Baud,		Rect(  4, 88,248,16)),
	DialItem(Dial_Button,		CI_OK,		Rect( 32,108, 64,16)),
	DialItem(Dial_Button,		CI_Cancel,	Rect(160,108, 64,16)),
	DialItem(Dial_End)
};

class CommSetup : public Dialogue {
public:
	UBYTE portNumber;
	BaudRate baudRate;
public:
	CommSetup();
	~CommSetup();

	void doIcon(DialItem* item, Event* event, MenuData* d);
private:
	void makeBaudName();
	void setComHighlight();
};

inline Boolean isValidIRQ(int i)
{
	return ( (i >= 2) && (i <= 7) );
}

inline Boolean isValidComPort(int n)
{
	return ( (n >= 1) && (n <= 4) );
}

/*
 * Find the Mouse Comm Port's IRQ
 * Returns 0 if not found
 * 2..7 if found
 */

UBYTE getMouseIRQ();
#pragma aux getMouseIRQ = 			\
			"mov ax,24h"				\
			"int 033h"					\
			"xor eax,eax"				\
			"cmp ch,2"					\
			"jnz notSerial"			\
			"mov al,cl"					\
	"notSerial:"						\
			modify[ebx ecx edx]		\
			value [al]



CommSetup::CommSetup()
{
	/*
	 * Default values
	 */

	baudRate = BAUD_9600;
	portNumber = 0;

	Boolean gotOne = False;		// Already set default port

	/*
	 * Get values from config file
	 */

	ConfigFile config(configFileName);
	int n;
	if(config.getNum(cfgCommPortID, n))
	{
		if(isValidComPort)
		{
			portNumber = n - 1;
			gotOne = True;
		}
	}

	char* s = config.get(cfgBaudID);
	if(s)
	{
		for(int i = 0; baudNames[i]; i++)
		{
			if(strcmp(s, baudNames[i]) == 0)
			{
				baudRate = BaudRate(i);
				break;
			}
		}
		delete[] s;
	}

	if(config.getNum(cfgComm1IRQ, n))
		if(isValidIRQ(n))
			ports[0].irq = n;
	if(config.getNum(cfgComm2IRQ, n))
		if(isValidIRQ(n))
			ports[1].irq = n;
	if(config.getNum(cfgComm3IRQ, n))
		if(isValidIRQ(n))
			ports[2].irq = n;
	if(config.getNum(cfgComm4IRQ, n))
		if(isValidIRQ(n))
			ports[3].irq = n;

	commSetupItems[0].text = copyString(language(lge_SerialSetup));

	/*
	 * Try to find out where serial mouse is...
	 */

	UBYTE serialMouseIRQ = getMouseIRQ();

	Boolean gotMouse = False;

	for(int i = 0; i < 4; i++)
	{
		DialItem* item = &commSetupItems[i * 4 + 1];
		item[0].text = copyString(ports[i].name);
		char* buf = new char[12];
		sprintf(buf, "I/O: %04x", ports[i].port);
		item[1].text = buf;
		buf = new char[5];
		sprintf(buf, "IRQ%d", (int) ports[i].irq);
		item[2].text = buf;
		item[3].text = copyString(portTypeNames[ports[i].available]);

		if(!gotMouse && (ports[i].irq == serialMouseIRQ))
		{
			gotMouse = True;
			item[0].enabled = False;
			item[1].enabled = False;
			item[2].enabled = False;
			item[3].enabled = False;
		}

		if(ports[i].available != UART_None)
		{
			if(!gotOne)
			{
				gotOne = True;
				portNumber = i;
			}
		}
		else
		{
			item[0].enabled = False;
			item[1].enabled = False;
			item[2].enabled = False;
			item[3].enabled = False;
		}
	}


	setComHighlight();

	commSetupItems[17].text = new char[30];
	makeBaudName();

	commSetupItems[18].text = copyString(language(lge_OK));
	commSetupItems[19].text = copyString(language(lge_cancel));

	Dialogue::setup(commSetupItems);
}

CommSetup::~CommSetup()
{
	DialItem* item = commSetupItems;
	while(item->type != Dial_End)
	{
		delete[] item->text;
		item->text = 0;

		item++;
	}
}

void CommSetup::doIcon(DialItem* item, Event* event, MenuData* d)
{
	if(event->buttons)
	{
		switch(item->id)
		{
		case CI_Cancel:
			d->setFinish(CI_Cancel);
			break;
		case CI_OK:
			d->setFinish(CI_OK);
			break;
		case CI_Com1:
		case CI_Com2:
		case CI_Com3:
		case CI_Com4:
			{
				int p = item->id - CI_Com1;
				if(ports[p].available != UART_None)
				{
					portNumber = p;
					setComHighlight();
					d->setRedraw();
				}
			}
			break;
		case CI_Com1IRQ:
		case CI_Com2IRQ:
		case CI_Com3IRQ:
		case CI_Com4IRQ:
			{
				int p = item->id - CI_Com1IRQ;
				if(ports[p].available != UART_None)
				{
					/*
					 * Get IRQ Number
					 */

					static MenuChoice irqChoices[] = {
						MenuChoice(0, 		0),		// THis filled with Com Name
						MenuChoice("IRQ", 0),
						MenuChoice("-",   0),
						MenuChoice("2",	2),
						MenuChoice("3",	3),
						MenuChoice("4",	4),
						MenuChoice("5",	5),
						MenuChoice("6",	6),
						MenuChoice("7",	7),
						MenuChoice("-",   0),
						MenuChoice(0, 		-1),		// This filled with Cancel
						MenuChoice(0, 		0)
					};

					irqChoices[0].text = ports[p].name;
					irqChoices[10].text = language(lge_cancel);
					int i = menuSelect(0, irqChoices, machine.mouse->getPosition());

					if(isValidIRQ(i))
					{
						ports[p].irq = i;
						sprintf(item->text, "IRQ%d", (int) ports[p].irq);
					}

					portNumber = p;
					setComHighlight();
					d->setRedraw();
				}
			}
			break;
		case CI_Baud:
			{
				int b = chooseFromList(0, language(lge_BaudRate), baudNames, language(lge_cancel));
				if(b > 0)
				{
					baudRate = BaudRate(b - 1);
					makeBaudName();
					d->setRedraw();
				}
			}
			break;

		}
	}
}

void CommSetup::makeBaudName()
{
	sprintf(commSetupItems[17].text, "%s: %d", language(lge_BaudRate), baudRealRates[baudRate]);
}

void CommSetup::setComHighlight()
{
	for(int i = 0; i < 4; i++)
	{
		DialItem* item = &commSetupItems[i * 4 + 1];

		if(i == portNumber)
		{
			item[0].highlighted = True;
			item[1].highlighted = True;
			item[2].highlighted = True;
			item[3].highlighted = True;
		}
		else
		{
			item[0].highlighted = False;
			item[1].highlighted = False;
			item[2].highlighted = False;
			item[3].highlighted = False;
		}
	}
}

/*-------------------------------------------------------------------
 * Entry point to get serial configuration
 */

CommDialID SerialConfig::getSerialConfig()
{
	CommSetup edit;

	CommDialID result = CommDialID(edit.process());

	switch(result)
	{
		case CI_OK:
			baud = edit.baudRate;
			ioPort  	= ports[edit.portNumber].port;
			irq 		= ports[edit.portNumber].irq;
			portType = ports[edit.portNumber].available;

			{
				ConfigFile config(configFileName);
			
				config.setNum(cfgCommPortID, edit.portNumber + 1);
				config.set(cfgBaudID, baudNames[baud]);
				if(ports[0].available != UART_None)
					config.setNum(cfgComm1IRQ, ports[0].irq);
				if(ports[1].available != UART_None)
					config.setNum(cfgComm2IRQ, ports[1].irq);
				if(ports[2].available != UART_None)
					config.setNum(cfgComm3IRQ, ports[2].irq);
				if(ports[3].available != UART_None)
					config.setNum(cfgComm4IRQ, ports[3].irq);
			}

			break;
		default:
		case CI_Cancel:
			break;
	}
	return result;
}

/*-------------------------------------------------------------------
 * Dialogue Box for getting Modem Commands
 */



enum ModemDialEnum {		// Shows the order of items in modemSetupItems[]
	MD_Title,
	MD_PhoneTitle,
	MD_PhoneInput,
	MD_InitTitle,
	MD_InitInput,
	MD_DialTitle,
	MD_DialInput,
	MD_AnswerTitle,
	MD_AnswerInput,
	MD_Dial,
	MD_Answer,
	MD_Cancel

};

static DialItem modemSetupItems[] = {
	DialItem(Dial_FreeString,		MDI_None,			Rect(  0,  0,320,16)),
	DialItem(Dial_Button,			MDI_None,			Rect(  0, 18,160,16)),
	DialItem(Dial_TextInput,		MDI_None,			Rect(160, 18,160,16)),
	DialItem(Dial_Button,			MDI_None,			Rect(  0, 36,160,16)),
	DialItem(Dial_TextInput,		MDI_None,			Rect(160, 36,160,16)),
	DialItem(Dial_Button,			MDI_None,			Rect(  0, 52,160,16)),
	DialItem(Dial_TextInput,		MDI_None,			Rect(160, 52,160,16)),
	DialItem(Dial_Button,			MDI_None,			Rect(  0, 68,160,16)),
	DialItem(Dial_TextInput,		MDI_None,			Rect(160, 68,160,16)),
#ifdef BEFORE_DUTCH
	DialItem(Dial_Button,			MDI_Dial,			Rect( 32, 86, 64,16)),
	DialItem(Dial_Button,			MDI_Answer,			Rect(128, 86, 64,16)),
	DialItem(Dial_Button,			MDI_Cancel,			Rect(224, 86, 64,16)),
#else
	DialItem(Dial_Button,			MDI_Dial,			Rect( 24, 86, 80,16)),
	DialItem(Dial_Button,			MDI_Answer,			Rect(120, 86, 80,16)),
	DialItem(Dial_Button,			MDI_Cancel,			Rect(216, 86, 80,16)),
#endif
	DialItem(Dial_End)
};

static LGE md_stringID[] = {
	lge_ModemSetup,
	lge_PhoneNumber,
	lge_NULL,					// Phone Number
	lge_Initialise,
	lge_NULL,					// Initialise String
	lge_Dial,
	lge_NULL,					// Dial String
	lge_Answer,
	lge_NULL,					// Answer String
	lge_Dial,
	lge_Answer,
	lge_cancel,
};

class ModemSetup : public Dialogue {
public:
	char* initString;
	char* dialString;
	char* answerString;
	char* phoneString;
public:
	ModemSetup();
	~ModemSetup();

	void doIcon(DialItem* item, Event* event, MenuData* d);
};


ModemSetup::ModemSetup()
{
	DialItem* item = modemSetupItems;
	LGE* lgePtr = md_stringID;
	while(item->type != Dial_End)
	{
		if(*lgePtr != lge_NULL)
			item->text = language(*lgePtr);

		item++;
		lgePtr++;
	}

	/*
	 * Set up the input strings
	 */

	dialString = new char[80];
	answerString = new char[80];
	initString = new char[80];
	phoneString = new char[80];

#if 0
	strcpy((char*)dialString, 	 ::dialString);			// Normal Haye's Dial
	strcpy((char*)answerString, ::answerString);		// Answer on 1st ring
	strcpy((char*)initString, 	 ::initString);		// Reset and set for verbal result
	strcpy((char*)phoneString,  ::phoneString);				// No sensible default
#else
	ConfigFile config(configFileName);

	char* s = config.get(cfgDialID);
	strcpy(dialString, s ? s : defaultDialString);
	delete[] s;

	s = config.get(cfgAnswerID);
	strcpy(answerString, s ? s : defaultAnswerString);
	delete[] s;

	s = config.get(cfgModemInitID);
	strcpy(initString, s ? s : defaultInitString);
	delete[] s;

	s = config.get(cfgPhoneID);
	strcpy(phoneString, s ? s : defaultPhoneString);
	delete[] s;
#endif

	modemSetupItems[MD_InitInput].text				= (char*)initString;
	modemSetupItems[MD_InitInput].bufferSize		= 80;
	modemSetupItems[MD_DialInput].text				= (char*)dialString;
	modemSetupItems[MD_DialInput].bufferSize		= 80;
	modemSetupItems[MD_AnswerInput].text			= (char*)answerString;
	modemSetupItems[MD_AnswerInput].bufferSize	= 80;
	modemSetupItems[MD_PhoneInput].text				= (char*)phoneString;
	modemSetupItems[MD_PhoneInput].bufferSize		= 80;

	Dialogue::setup(modemSetupItems);
}

ModemSetup::~ModemSetup()
{
	delete[] initString;
	delete[] dialString;
	delete[] answerString;
	delete[] phoneString;
}

void ModemSetup::doIcon(DialItem* item, Event* event, MenuData* d)
{
	if(event->buttons)
	{
		switch(item->id)
		{
		case MDI_Dial:
		case MDI_Answer:
		case MDI_Cancel:
			d->setFinish(item->id);
			break;
		}
	}
}

/*=================================================
 * Entry point to get modem configuration
 */

ModemDialID getModemConfig(char** initString, char** dialString, char** answerString, char** phoneString)
{
	ModemSetup edit;

	ModemDialID result = ModemDialID(edit.process());

	if(result != MDI_Cancel)
	{
		delete[] *initString;
		*initString = copyString(edit.initString);
		delete[] *dialString;
		*dialString = copyString(edit.dialString);
		delete[] *answerString;
		*answerString = copyString(edit.answerString);
		delete[] *phoneString;
		*phoneString = copyString(edit.phoneString);

		ConfigFile config(configFileName);
		config.set(cfgDialID, edit.dialString);
		config.set(cfgAnswerID, edit.answerString);
		config.set(cfgModemInitID, edit.initString);
		config.set(cfgPhoneID, edit.phoneString);

	}

	return result;
}

#endif
