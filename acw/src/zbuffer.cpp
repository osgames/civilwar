/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	3D ZBuffer Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:54  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <string.h>
#include "zbuffer.h"
#include "data3d.h"

#if !defined(BATEDIT)
#include "membodge.h"
#endif

ZBuffer::ZBuffer()
{
	width = 0;
	height = 0;
	zValues = 0;
}

ZBuffer::~ZBuffer()
{
#if defined(BATEDIT)
	delete[] zValues;
#else
	memHeap->reset();
#endif
}

void ZBuffer::init(const Point& size)
{
	width = size.x;
	height = size.y;
#if defined(BATEDIT)
	zValues = new UWORD[width * height];
#else
	zValues = (ZSIZE*) memHeap->allocate(width * height * sizeof(ZSIZE));
#endif
	clear();
}


void ZBuffer::clear()
{
#if 0
	for(int i = 0; i < (width * height); i++)
		zValues[i] = BackZ;
#else
	memset(zValues, 0xff, width * height * sizeof(ZSIZE));
#endif
}


void ZBuffer::blitSprite(Region* region, const Sprite* sprite, const Point3D& pt) const
{
	region->maskBlit(sprite, pt);
}

Boolean ZBuffer::findPoint(const Point& p, Point3D& result)
{
	if( (p.y >= 0) && (p.y < height) &&
		 (p.x >= 0) && (p.x < width) )
	{
		ZSIZE z = get(p.x, p.y);

		if(z != BackZ)
		{
			result.x = p.x;
			result.y = p.y;
			result.z = scanZtoZ(z);
			return True;
		}
	}

	return False;
}

