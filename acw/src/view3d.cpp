/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Viewpoint calculations
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:54  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.15  1994/08/31  15:23:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.14  1994/08/24  15:05:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/08/19  17:29:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1994/08/09  15:42:15  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/04/20  22:21:40  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/04/11  13:36:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/03/21  21:03:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/03/18  15:07:16  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1993/12/13  21:59:58  Steven_Green
 * Lightsource is defined using height and bearing angles.
 *
 * Revision 1.6  1993/12/10  16:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1993/12/01  15:11:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1993/11/26  22:30:38  Steven_Green
 * Viewpoint method changed to rotate on viewed point rather than eye.
 *
 * Revision 1.3  1993/11/24  09:32:52  Steven_Green
 * Addition of width parameter to magnify image
 *
 * Revision 1.2  1993/11/19  18:59:26  Steven_Green
 * Combined into one function
 *
 * Revision 1.1  1993/11/16  22:42:06  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdlib.h>
#include "view3d.h"
#include "map3d.h"
#include "image.h"
#include "miscfunc.h"
#include "myassert.h"

/*
 * Transform a point so that viewpoint is origin
 */

void ViewPoint::transform(Point3D& p) const
{
	/*
	 * Adjust so viewed point is in centre
	 */

	p -= focus;

	/*
	 * Rotate around axes
	 *
	 * Simplified to not that yRotate is always 90 degrees and
	 * There is never any Z Rotations
	 */

	rotate(p.x, p.z, -yRotate);
	rotate(p.z, p.y, -xRotate);

	// p *= centre;

	/*
	 * Move back to viewpoint
	 */

	p.z += eyeToPoint;

	/*
	 * Adjust to fit into bitmap coordinates
	 */

	p.x = (p.x * getPixelWidth()) / planeWidth;
	p.y = (p.y * getPixelWidth()) / planeWidth;

	/*
	 * Adjust for perspective
	 */

	if(p.z)
	{
		Cord3D z = p.z;
		if(z < 0)
			z = Cord3D(-z);

		p.x = (p.getX() * eyeToPlane) / z;
		p.y = (p.getY() * eyeToPlane) / z;
	}
	else
		p = Point3D(0,0,0);

	/*
	 * Screen is top to bottom
	 */

	p.y = Cord3D(-p.y);

	/*
	 * Make origin centre of bitmap
	 */

	p += (viewSize/2);

}

/*
 * Convert a screen coordinate back to world coordinates by doing
 * a transform backwards;
 */

void ViewPoint::unTransform(Point3D& p) const
{
	p -= (viewSize / 2);
	p.y = Cord3D(-p.y);
	p.x = (p.x * p.z) / eyeToPlane;	// Assumes Z is +ve
	p.y = (p.y * p.z) / eyeToPlane;
	p.x = (p.x * planeWidth) / getPixelWidth();
	p.y = (p.y * planeWidth) / getPixelWidth();
	p.z -= eyeToPoint;
	rotate(p.z, p.y, xRotate);
	rotate(p.x, p.z, yRotate);
	p += focus;
}


Cord3D ViewPoint::howBig()
{
	// return (eyeToPoint * planeWidth) / eyeToPlane;
	return maxX - minX;
}


static Cord3D viewSizes[ZOOM_LEVELS] = {
	DistToBattle(Mile(1)/2),
	DistToBattle(Mile(1)),
	DistToBattle(Mile(2)),
	DistToBattle(Mile(4)),
	DistToBattle(Mile(8))
#ifdef BATEDIT
	,DistToBattle(Mile(16)),
	DistToBattle(Mile(32)),
	DistToBattle(Mile(64))
#endif
};

void ViewPoint::set(const Cord3D x, Cord3D z, Wangle rotate, Wangle bank, ZoomLevel zoom)
// void ViewPoint::set(const Position3D& p, ZoomLevel zoom)
{
	zoomLevel = zoom;
	eyeToPlane = 20;		// 1/4 lens to reduce perspective
	planeWidth = 5;

	xCentre = x;
	zCentre = z;
	yRotate = rotate;
	xRotate = bank;

	setZoom();
}

void ViewPoint::setZoom()
{
	eyeToPoint = viewSizes[zoomLevel] * eyeToPlane / planeWidth;
}

#if defined(BATEDIT)

/*
 * Battle Editor allows view around playfield area
 * so clipping is different
 */

void ViewPoint::setBoundaries(const MapGrid* grid)
{
	const Grid* g = grid->getGrid(zoomLevel);
	Cord3D mapCentre =  grid->getTotalWidth() / 2;

	int gridSize = viewSizes[zoomLevel] / g->gridSize;

	int minGX = (xCentre + mapCentre) / g->gridSize - gridSize/2;
	int minGZ = (zCentre + mapCentre) / g->gridSize - gridSize/2;

	int maxGX = minGX + gridSize;
	int maxGZ = minGZ + gridSize;

	gridX = minGX;
	gridZ = minGZ;
	gridW = maxGX - minGX;
	gridH = maxGZ - minGZ;

	ASSERT(gridW > 0);
	ASSERT(gridH > 0);

	/*
	 * And calculate low level grid coordinates
	 */

	int adjust = g->gridSize / grid->getGrid(Zoom_Min)->gridSize;
	bigGridX = gridX * adjust;
	bigGridZ = gridZ * adjust;
	bigGridW = gridW * adjust;
	bigGridH = gridH * adjust;

	/*
	 * Now convert the grid coordinates back into real world values
	 */

	minX = minGX * g->gridSize - mapCentre;
	maxX = maxGX * g->gridSize - mapCentre;
	minZ = minGZ * g->gridSize - mapCentre;
	maxZ = maxGZ * g->gridSize - mapCentre;

	/*
	 * And calculate a real viewpoint focus
	 */

	focus.x = (minX + maxX) / 2;
	focus.z = (minZ + maxZ) / 2;
	focus.y = grid->getHeightView(*this, focus.x, focus.z);
}

#elif defined(OLD_BATEDIT)

/*
 * Old Version of Battle Editor's clipping
 */


void ViewPoint::setBoundaries(const MapGrid* grid)
{
	const Grid* g = grid->getGrid(zoomLevel);
	Cord3D mapCentre =  grid->getTotalWidth() / 2;

	int gridSize = viewSizes[zoomLevel] / g->gridSize;

	int minGX = (xCentre + mapCentre) / g->gridSize - gridSize/2;
	int minGZ = (zCentre + mapCentre) / g->gridSize - gridSize/2;

	realMinX = minGX * g->gridSize - mapCentre;
	realMinZ = minGZ * g->gridSize - mapCentre;
	realMaxX = (minGX + gridSize) * g->gridSize - mapCentre;
	realMaxZ = (minGZ + gridSize) * g->gridSize - mapCentre;

	int maxGX = minGX + gridSize;
	int maxGZ = minGZ + gridSize;

	int maxGrid = g->gridCount - gridSize;

	if(maxGrid < 0)
		maxGrid = 0;

	if(minGX < 0)
		minGX = 0;
	if(minGZ < 0)
		minGZ = 0;

	if(minGX >= maxGrid)
		minGX = maxGrid;
	if(minGZ >= maxGrid)
		minGZ = maxGrid;

	if(maxGX < 0)
		maxGX = 0;
	if(maxGZ < 0)
		maxGZ = 0;

	if(maxGX > g->gridCount)
		maxGX = g->gridCount;
	if(maxGZ > g->gridCount)
		maxGZ = g->gridCount;

	gridX = minGX;
	gridZ = minGZ;
	gridW = maxGX - minGX;
	gridH = maxGZ - minGZ;

	ASSERT(gridW > 0);
	ASSERT(gridH > 0);

	/*
	 * And calculate low level grid coordinates
	 */

	int adjust = g->gridSize / grid->getGrid(Zoom_Min)->gridSize;
	bigGridX = gridX * adjust;
	bigGridZ = gridZ * adjust;
	bigGridW = gridW * adjust;
	bigGridH = gridH * adjust;

	/*
	 * Now convert the grid coordinates back into real world values
	 */

	minX = minGX * g->gridSize - mapCentre;
	maxX = maxGX * g->gridSize - mapCentre;
	minZ = minGZ * g->gridSize - mapCentre;
	maxZ = maxGZ * g->gridSize - mapCentre;

	/*
	 * And calculate a real viewpoint focus
	 */

	focus.x = (realMinX + realMaxX) / 2;
	focus.z = (realMinZ + realMaxZ) / 2;
	focus.y = grid->getHeightView(*this, focus.x, focus.z);

}
#else

/*
 * Normal version, keeps view within 8 mile area
 */

void ViewPoint::setBoundaries(const MapGrid* grid)
{
	const Grid* g = grid->getGrid(zoomLevel);
	Cord3D mapCentre =  grid->getTotalWidth() / 2;

	int gridSize = viewSizes[zoomLevel] / g->gridSize;

	int minGX = (xCentre + mapCentre) / g->gridSize - gridSize/2;
	int minGZ = (zCentre + mapCentre) / g->gridSize - gridSize/2;


	if(minGX < 0)
		minGX = 0;
	if(minGZ < 0)
		minGZ = 0;

	int maxGrid = g->gridCount - gridSize;

	if(minGX >= maxGrid)
		minGX = maxGrid;
	if(minGZ >= maxGrid)
		minGZ = maxGrid;

	int maxGX = minGX + gridSize;
	int maxGZ = minGZ + gridSize;

	if(maxGX > g->gridCount)
		maxGX = g->gridCount;

	if(maxGZ > g->gridCount)
		maxGZ = g->gridCount;

	gridX = minGX;
	gridZ = minGZ;
	gridW = maxGX - minGX;
	gridH = maxGZ - minGZ;

	/*
	 * And calculate low level grid coordinates
	 */

	int adjust = g->gridSize / grid->getGrid(Zoom_Min)->gridSize;
	bigGridX = gridX * adjust;
	bigGridZ = gridZ * adjust;
	bigGridW = gridW * adjust;
	bigGridH = gridH * adjust;

	/*
	 * Now convert the grid coordinates back into real world values
	 */

	minX = minGX * g->gridSize - mapCentre;
	maxX = maxGX * g->gridSize - mapCentre;
	minZ = minGZ * g->gridSize - mapCentre;
	maxZ = maxGZ * g->gridSize - mapCentre;

	/*
	 * And calculate a real viewpoint focus
	 */

	focus.x = (minX + maxX) / 2;
	focus.z = (minZ + maxZ) / 2;
	focus.y = grid->getHeightView(*this, focus.x, focus.z);
}

#endif		// BATEDIT

LightSource::LightSource()
{
	set(Degree(30), 0, (intensityRange * 3) / 4, intensityRange/8); 
}


/*
 * Set lightsource to specified direction
 */

void LightSource::update()
{
	Cord3D r = msin(256, height);

	y = -mcos(256, height);
	x = -msin(r, bearing);
	z = -mcos(r, bearing);

	normalise();
}

Boolean ViewPoint::isOnDisplay(const Location& l) const
{
	Cord3D x = DistToBattle(l.x);

#ifdef OLD_BATEDIT
	if((x >= realMinX) && (x <= realMaxX))
	{
		Cord3D y = DistToBattle(l.y);

		if((y >= realMinZ) && (y <= realMaxZ))
			return True;
	}
#else
	if((x >= minX) && (x <= maxX))
	{
		Cord3D y = DistToBattle(l.y);

		if((y >= minZ) && (y <= maxZ))
			return True;
	}
#endif

	return False;
}



Boolean ViewPoint::zoomIn()
{
	if(zoomLevel > Zoom_Min)
	{
		DECREMENT(zoomLevel);
		setZoom();
		// setBoundaries();
		return True;
	}
	else
		return False;
}

void ViewPoint::zoomIn(ZoomLevel level, const BattleLocation& l)
{
	zoomLevel = level;
	xCentre = l.x;
	zCentre = l.z;
	setZoom();
}

void ViewPoint::zoomIn(ZoomLevel level)
{
	zoomLevel = level;
	setZoom();
}

Boolean ViewPoint::zoomOut()
{
	if(zoomLevel < Zoom_Max)
	{
		INCREMENT(zoomLevel);
		setZoom();
		// setBoundaries();
		return True;
	}
	else
		return False;
}

/*
 * Calculate intensity of light falling on normal
 *
 * normal is pre-adjusted so that its scalar size is 256
 */

Intensity LightSource::makeIntensity(const Point3D& n)
{
	Cord3D i = ::abs(dot(n, *this));		// result is 0..65535

#if 0
	Intensity minI = ambience;
	Intensity ir = Intensity(intensityRange - minI);

	Intensity in = Intensity(((long)i * intensity) / 65536);
	in = Intensity((in * ir) / intensityRange);

	return Intensity(minI + in);
#else

	Intensity range = intensity - ambience;
	Intensity result = (range * i) / (256 * 256) + ambience;

	return result;
#endif
}

