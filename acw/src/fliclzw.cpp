/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/* 
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Convert FLI/FLC files to FLZ
 *
 * FLZ files are the same as FLI/FLC except that some of the chunks
 * are LZ compressed to reduce loading time.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <io.h>
#include "flic.h"
#include "lzhuf.h"

void makeFilename(char *dest, const char *src, const char *ext, Boolean force)
{
	char *s;

	if(src)
		strcpy(dest, src);

	/*
    * Find start of filename (lose path or drive)
    */

	s = strrchr(dest, '\\');
	if(s == NULL)
		s = strrchr(dest, ':');
	if(s == NULL)
		s = dest;

	/*
    * See if there is an extension
	 */

	s = strchr(s, '.');

	/*
	 * Add the new extension
	 */

	if(s == NULL)
		strcat(dest, ext);
	else if(force)
		strcpy(s, ext);
}

struct ChunkName {
	ChunkTypes type;
	const char* name;
} chunkNames[] = {
	{ COLOR_256, "COLOR_256" },
	{ DELTA_FLC, "DELTA_FLC" },
	{ COLOR_64, "COLOR_64" },
	{ DELTA_FLI, "DELTA_FLI" },
	{ BLACK, "BLACK" },
	{ BYTE_RUN, "BYTE_RUN" },
	{ LITERAL, "LITERAL" },
	{ PSTAMP, "PSTAMP" },
	{ DELTA_FLC_LZW, "DELTA_FLC_LZW" },
	{ DELTA_FLI_LZW, "DELTA_FLI_LZW" },
	{ BYTE_RUN_LZW, "BYTE_RUN_LZW" },
	{ LITERAL_LZW, "LITERAL_LZW" },
	{ ChunkTypes(0), 0 }
};

const char* chunkTypeToStr(UWORD type)
{
	ChunkName* ptr = chunkNames;
	while(ptr->name)
	{
		if(ptr->type == type)
			return ptr->name;

		ptr++;
	}

	return "UNKNOWN";
}

void usage()
{
	printf("Usage:\nfliclzw infile.flc\n\nCreates infile.flz\n\n");
}

void main(int argc, char* argv[])
{
	char* inName = 0;
	char* outName = 0;

	int i = 0;

	while(++i < argc)
	{
		if(inName == 0)
			inName = argv[i];
		else if(outName == 0)
			outName = argv[i];
		else
			usage();
	}

	if(!inName)
	{
		usage();
		return;
	}

	if(!outName)
		outName = inName;

 	char fullInName[FILENAME_MAX];
 	char fullOutName[FILENAME_MAX];

	makeFilename(fullInName, inName, ".FLI", False);
	makeFilename(fullOutName, outName, ".FLZ", True);

	int inHandle = open(fullInName, O_RDONLY | O_BINARY);
	if(inHandle < 0)
	{
		makeFilename(fullInName, inName, ".FLC", False);
		inHandle = open(fullInName, O_RDONLY | O_BINARY);
	}
	if(inHandle >= 0)
	{
		printf("Reading %s\n", fullInName);

		int outHandle = open(fullOutName, O_BINARY | O_CREAT | O_RDWR);
		if(outHandle >= 0)
		{
			printf("Writing %s\n", fullOutName);

			/*
			 * Copy the header
			 */

			FlicHead header;

			read(inHandle, &header, sizeof(header));
			write(outHandle, &header, sizeof(header));

			printf("Header says:\n");
			printf("Type: %04x (", (int) header.type);
			if(header.type == FLI_TYPE)
				printf("FLI");
			else if(header.type == FLC_TYPE)
				printf("FLC");
			else if(header.type == FLZ_TYPE)
				printf("FLZ");
			else
				printf("Unknown Type");
			printf(")\n");
			
			printf("frames: %d\n", (int) header.frames);
			printf("width: %d\n", (int) header.width);
			printf("height: %d\n", (int) header.height);

			lseek(inHandle, header.oframe1, SEEK_SET);

			int count = 0;

			while(count++ < (header.frames + 1))		// + 1 because extra frame stored at end?
			{
				if(count == 1)
					header.oframe1 = lseek(outHandle, 0, SEEK_CUR);
				else if(count == 2)
					header.oframe2 = lseek(outHandle, 0, SEEK_CUR);

				FrameHead frameHead;
				long framePos = lseek(outHandle, 0, SEEK_CUR);

				read(inHandle, &frameHead, sizeof(frameHead));
				write(outHandle, &frameHead, sizeof(frameHead));

				printf("frame %d, type %d, chunks %d, size %d\n",
					count, (int) frameHead.type, (int) frameHead.chunks, (int) frameHead.size);

				int cCount = 0;
				while(cCount++ < frameHead.chunks)
				{
					ChunkHead cHead;
					long chPos = lseek(outHandle, 0, SEEK_CUR);

					read(inHandle, &cHead, sizeof(cHead));

					printf("Chunk type %s, size %d\n",
						chunkTypeToStr(cHead.type), (int) cHead.size);

					long cSize = cHead.size - sizeof(cHead);
					UBYTE* buf = new UBYTE[cSize];
					read(inHandle, buf, cSize);

					UWORD newType;

					switch(cHead.type)
					{
					case DELTA_FLC:
						newType = DELTA_FLC_LZW;
						goto compressBlock;
					case DELTA_FLI:
						newType = DELTA_FLI_LZW;
						goto compressBlock;
					case BYTE_RUN:
						newType = BYTE_RUN_LZW;
						goto compressBlock;
					case LITERAL:
						newType = LITERAL_LZW;
					compressBlock:
						{
							UBYTE* buf2 = new UBYTE[cSize * 2];	// Just in case it gets bigger!

							long cLength = encode_lzhuf_mm(buf, buf2, cSize);

							if(cLength < (cSize + sizeof(long)))
							{
								cHead.type = newType;
								cHead.size = cLength + sizeof(cHead);
								write(outHandle, &cHead, sizeof(cHead));
								write(outHandle, &cSize, sizeof(cSize));
								write(outHandle, buf2, cLength);

								printf("Compressed from %ld to %ld = %d%%\n", cSize, cLength,
									(int) (cLength * 100) / cSize);
							}
							else
							{
								write(outHandle, &cHead, sizeof(cHead));
								write(outHandle, buf, cSize);

								printf("Compression was > 100%!!!\n");
							}
							delete[] buf2;
						}
						break;

					default:
						write(outHandle, &cHead, sizeof(cHead));
						write(outHandle, buf, cSize);
						break;
					}

					delete[] buf;
				}

				long curPos = lseek(outHandle, 0, SEEK_CUR);

				frameHead.size = curPos - framePos;
				lseek(outHandle, framePos, SEEK_SET);
				write(outHandle, &frameHead, sizeof(frameHead));
				lseek(outHandle, curPos, SEEK_SET);
			}

 			header.size = lseek(outHandle, 0, SEEK_CUR);
			lseek(outHandle, 0, SEEK_SET);
			write(outHandle, &header, sizeof(header));

			close(outHandle);
		}
		else
			printf("Couldn't create %s\n", fullOutName);

		close(inHandle);

	}
	else
		printf("Couldn't open %s\n", fullInName);

}
