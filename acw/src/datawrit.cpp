/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Writing of game datafile
 *
 * Split up from the rather large datafile.cpp
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.6  1994/09/23  13:28:28  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/07/28  18:57:04  Steven_Green
 * Added regimentCount to City
 *
 * Revision 1.4  1994/07/25  20:32:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/07/19  19:53:03  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/07/11  14:26:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/06/24  14:43:30  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <fstream.h>
#include <stdio.h>

#include "datafile.h"
#include "campwld.h"
#include "generals.h"
#include "error.h"

#ifdef TESTING
#include "dialogue.h"
#endif



#define ADD_COMMENTS
#define NICE_FORMAT


/*
 * Class for writing files
 */

class DataFileOutStream : public ofstream {

#ifdef NICE_FORMAT
	int indentation;
#endif

#ifdef TESTING
public:
	char popupBuffer[400];
	PopupText* popup;
#endif

public:
	CampaignWorld* world;

	DataFileOutStream(const char* name, CampaignWorld* w) :
		ofstream(name)
	{
		world = w;
		indentation = 0;
#ifdef TESTING
		sprintf(popupBuffer, "Writing %s", name);
		popup = new PopupText(popupBuffer);
#endif
	}

#ifdef TESTING
	~DataFileOutStream()
	{
		delete popup;
	}
#endif

	void beginChunk();
	void finishChunk();

#ifdef NICE_FORMAT
	void begl();
#else
	void begl() { }
#endif

	/*
	 * Specific functions for data types
	 */

	void writeStateList(StateList& list);
	void writeState(State& state);
	void writeFacilityList(FacilityList& list);
	void writeWaterNetwork(WaterNetwork& water);

	void writeOB(OrderBattle& ob);
	void writeGeneral(General* g);
	void writeUnit(Unit* unit);

	void writeName(const char* name);
	void writeLocation(Location& l);
	void writeResourceSet(ResourceSet& r);
	void writeOrders(Order& order);
	void writeSentOrder(SentOrder& order);

	void writeWaterZone(WaterZone& zone, WaterNetwork& water);

	void writeBoats(Flotilla& boats);

	void writeSide(Side side);
};

void DataFileOutStream::beginChunk()
{
	begl();
	*this << startChunk << endl;
#ifdef NICE_FORMAT
	indentation++;
#endif
}

void DataFileOutStream::finishChunk()
{
#ifdef NICE_FORMAT
	indentation--;
#endif
	begl();
	*this << endChunk << endl;
}

#ifdef NICE_FORMAT
void DataFileOutStream::begl()
{
	int count = indentation;
	while(count--)
		*this << " ";
}
#endif

/*
 * Write out game data to a file
 *
 * This includes:
 *		Facilities
 *		Railways
 *		Sea Zones
 *		Water Zones
 *
 * File is ascii with format:
 *		keyword [data]
 * Lines beginning with ; (semicolon) are comments
 * Strings are enclosed in quote marks "
 * Numbers are in decimal.
 *
 * c++ streams will be used
 */

void writeWorld(const char* fileName, CampaignWorld* world)
{
	DataFileOutStream stream(fileName, world);
	if(stream)
	{
#ifdef ADD_COMMENTS
		stream << ";---------------------------------------------" << endl;
		stream << "; Dagger ACW Wargame World Data File" << endl;
		stream << "; This file contains data about:" << endl;
		stream << ";   Facilities" << endl;
		stream << ";   Railways" << endl;
		stream << ";   Sea Zones" << endl;
		stream << ";   Water Zones" << endl;
		stream << ";   Order of Battle" << endl;
		stream << ";" << endl;
		stream << ";---------------------------------------------" << endl;
#endif

		/*
		 * Put in ID
		 */

		stream << keywords[KW_WorldFile] << " " << WorldVersion << endl;
		stream.beginChunk();

		/*
		 * States
		 */

#ifdef TESTING
		strcat(stream.popupBuffer, "\rStates");
		stream.popup->showText(stream.popupBuffer);
#endif

		stream.writeStateList(world->states);

		/*
		 * Write Facilities
		 */

#ifdef TESTING
		strcat(stream.popupBuffer, "\rFacilities");
		stream.popup->showText(stream.popupBuffer);
#endif

		stream.writeFacilityList(world->facilities);


#ifdef TESTING
		strcat(stream.popupBuffer, "\rWater Network");
		stream.popup->showText(stream.popupBuffer);
#endif

		stream.writeWaterNetwork(world->waterNet);


		/*
		 * Order of Battle
		 */

#ifdef TESTING
		strcat(stream.popupBuffer, "\rOrder of Battle");
		stream.popup->showText(stream.popupBuffer);
#endif

		stream.writeOB(world->ob);
		
		/*
		 * Finish off the file
		 */

#ifdef TESTING
		strcat(stream.popupBuffer, "\r\rFinished!");
		stream.popup->showText(stream.popupBuffer);
#endif

#ifdef ADD_COMMENTS
		stream << ";---------------------------------------------" << endl;
#endif

		stream.finishChunk();
	}
}

void DataFileOutStream::writeStateList(StateList& list)
{
	if(list.entries())
	{
#ifdef ADD_COMMENTS
		*this << ";---------------------------------------------" << endl;
		*this << "; States Chunk with " << list.entries() << " states" << endl;
		*this << ";---------------------------------------------" << endl;
#endif
		begl();
		*this << keywords[KW_StateSection] << " " << list.entries() << endl;
		beginChunk();

		for(StateID i = 0; i < list.entries(); i++)
			writeState(list.get(i));

		finishChunk();
	}
}

void DataFileOutStream::writeState(State& state)
{
	begl();
	*this << keywords[KW_State] << endl;
	beginChunk();

	writeLocation(state.location);

	writeName(state.fullName);
#ifdef CAMPEDIT
	if(state.shortName[0])
#else
	if(state.shortName)
#endif
		writeName(state.shortName);

	writeSide(state.side);

	if(state.regimentCount > 1)
	{
		begl();
		*this << keywords[KW_Regiment] << " " << (unsigned int)state.regimentCount << endl;
	}

	finishChunk();
}

/*
 * Write all the facilties and railways
 */

void DataFileOutStream::writeFacilityList(FacilityList& list)
{
	if(list.entries())
	{
#ifdef CAMPEDIT
		/*
		 * Count railway connections
		 */

		int railConnections = 0;
		for(int r = 0; r < list.entries(); r++)
		{
			Facility* f = list[r];
			railConnections += f->connections.entries();
		}
#endif

#ifdef ADD_COMMENTS
		*this << ";---------------------------------------------" << endl;
		*this << "; Facility Chunk with " << list.entries() << 
			" facilities and " <<
#ifdef CAMPEDIT
			railConnections <<
#else
			world->railways.entries() <<
#endif
			" Rail Connections" << endl;
		*this << ";---------------------------------------------" << endl;
#endif

		begl();
		*this << keywords[KW_FacilitySection] << " " << list.entries() <<
#ifdef CAMPEDIT
			" " << railConnections << endl;
#else
			" " << world->railways.entries() << endl;
#endif
		beginChunk();

#ifdef TESTING
		char* popad = popupBuffer + strlen(popupBuffer);
#endif

 		for(FacilityID i = 0; i < list.entries(); i++)
		{
#ifdef TESTING
			if( (i % 10) == 0)
			{
				sprintf(popad, " %d", (int) i);
				popup->showText(popupBuffer);
			}
#endif
 			((Facility*)list[i])->writeData(*this);
		}

		finishChunk();
	}
}

/*
 * Facility writing
 */

void Facility::writeData(DataFileOutStream& stream)
{
	stream.begl();
	stream << keywords[KW_Facility] << endl;
	stream.beginChunk();

	stream.writeName(name);

	if(owner != NoFacility)
	{
		stream.begl();
		stream << keywords[KW_KeyCity] << " " << owner << endl;
	}

	stream.writeLocation(location);
	stream.begl();
	stream << keywords[KW_State] << " " << (unsigned int) state << endl;

#if 0
	stream.begl();
	stream << keywords[KW_Flags] << " " << facilities << endl;
#else
	if(facilities & Facility::F_SupplyDepot)
	{
		stream.begl();
		stream << keywords[KW_Depot] << endl;
	}
	if(facilities & Facility::F_RailEngineer)
	{
		stream.begl();
		stream << keywords[KW_RailEngineer] << endl;
	}
#endif

	stream.writeSide(side);

	if(fortification)
	{
		stream.begl();
		stream << keywords[KW_Fortification] << " " << 
			(unsigned int) fortification << endl;
	}

#if 0
	if(wagons)
	{
		stream.begl();
		stream << keywords[KW_Wagons] << " " << (unsigned int) wagons << endl;
	}
#endif

	if(facilities & F_SupplyDepot)
	{
		stream.begl();
		stream << keywords[KW_Supply] <<
#if 0
			" " << (unsigned int) supplies.food << 
			" " << (unsigned int) supplies.forage << 
			" " << (unsigned int) supplies.ammo << endl;
#else
			" " << (unsigned int) supplies << endl;
#endif
	}

	if(railCapacity)
	{
		stream.begl();
		stream << keywords[KW_RailCapacity] << " " << (unsigned int) railCapacity;
#ifndef CAMPEDIT
		if(freeRailCapacity != railCapacity)
			stream << " " << (unsigned int) freeRailCapacity;
#endif
		stream << endl;
	}

#ifdef CAMPEDIT
	if(connections.entries())
	{
		stream.begl();
		stream << keywords[KW_RailLink];
		
		for(int i = 0; i < connections.entries(); i++)
			stream << " " << (unsigned int) connections[i];

		stream << endl;
	}
#else
	if(railCount)
	{
		stream.begl();
		stream << keywords[KW_RailLink];

		RailSectionID i = railSection;
		int count = railCount;
		while(count--)
			stream << " " << (unsigned int) stream.world->railways.get(i++);
		stream << endl;
	}
#endif

	if(waterCapacity)
	{
		stream.begl();
		stream << keywords[KW_WaterCapacity] << " " << (unsigned int) waterCapacity;
#ifndef CAMPEDIT
		if(freeWaterCapacity != waterCapacity)
			stream << " " << (unsigned int) freeWaterCapacity;
#endif
		stream << endl;
	}

	if(blockadeRunners)
	{
		stream.begl();
		stream << keywords[KW_Blockade] << " " << (int) blockadeRunners << endl;
	}

	stream.begl();
	stream << keywords[KW_Victory] << " " << (int) victory << endl;

	stream.finishChunk();
}

#if 0
void NamedFacility::writeData(DataFileOutStream& stream)
{
	stream.begl();
	stream << keywords[KW_NamedFacility] << endl;
	stream.beginChunk();
	stream.writeName(name);

	if(owner != NoFacility)
	{
		stream.begl();
		stream << keywords[KW_KeyCity] << " " << owner << endl;
	}

	Facility::writeData(stream);

	stream.finishChunk();
}
#endif

void City::writeData(DataFileOutStream& stream)
{
	stream.begl();
	stream << keywords[KW_City] << endl;

	stream.beginChunk();
	stream.begl();
	stream << keywords[KW_Type] << " " << (unsigned int)type << endl;
	stream.begl();
	stream << keywords[KW_Size] << " " << (unsigned int)size << endl;
	stream.writeResourceSet(resource);
	if(facilities & Facility::F_RecruitmentCentre)
	{
		stream.begl();
		stream << keywords[KW_Recruit] << endl;
	}
	if(facilities & Facility::F_TrainingCamp)
	{
		stream.begl();
		stream << keywords[KW_Training] << endl;
	}
	if(facilities & Facility::F_Hospital)
	{
		stream.begl();
		stream << keywords[KW_Hospital] << endl;
	}
	if(facilities & Facility::F_POW)
	{
		stream.begl();
		stream << keywords[KW_POW] << endl;
	}
	if(facilities & Facility::F_KeyCity)
	{
		stream.begl();
		stream << keywords[KW_Capital] << endl;
	}

	Facility::writeData(stream);
	stream.finishChunk();
}

void DataFileOutStream::writeResourceSet(ResourceSet& r)
{
	begl();
	*this << keywords[KW_Resources];
	for(int i = 0; i < ResourceCount; i++)
		*this << " " << r.values[i];
	*this << endl;
}

/*
 * Location Write
 */

void DataFileOutStream::writeLocation(Location& l)
{
	begl();
	*this << keywords[KW_Location] << " "  << l.x << " " << l.y << endl;
}


void DataFileOutStream::writeWaterNetwork(WaterNetwork& water)
{
#ifdef CAMPEDIT
	int facilityCount = 0;
	int linkCount = 0;

	for(int i = 0; i < water.zones.entries(); i++)
	{
		WaterZone& z = water.zones[i];

		facilityCount += z.facilityList.entries();
		linkCount += z.connections.entries();
	}
#endif

#ifdef ADD_COMMENTS
	*this << ";---------------------------------------------" << endl;
	*this << "; Water Network" << endl;
	*this << ";---------------------------------------------" << endl;
	*this << ";       Zones:" << water.zones.entries() << endl;
#ifdef CAMPEDIT
	*this << ";  Facilities:" << facilityCount << endl;
	*this << "; Connections:" << linkCount << endl;
#else
	*this << ";  Facilities:" << water.facilityList.entries() << endl;
	*this << "; Connections:" << water.connectList.entries() << endl;
#endif
	*this << ";---------------------------------------------" << endl;
#endif
	begl();
	*this << keywords[KW_WaterNetwork] << " " <<
		water.zones.entries() << " " <<
#ifdef CAMPEDIT
		facilityCount << " " << linkCount << endl;
#else
		water.facilityList.entries() << " " << water.connectList.entries() << endl;
#endif

	beginChunk();

#ifdef ADD_COMMENTS
	begl();
	*this << ";---------------------------------------------" << endl;
	begl();
	*this << "; Coastal Zones" << endl;
	begl();
	*this << ";---------------------------------------------" << endl;
#endif

	for(int z = 0; z < water.zones.entries(); z++)
		writeWaterZone(water.zones[z], water);

	finishChunk();



}


void DataFileOutStream::writeWaterZone(WaterZone& zone, WaterNetwork& water)
{
	static KeyWords waterType[] = {
		KW_Coast,
		KW_Sea,
		KW_River,
	};

	begl();
	*this << keywords[KW_WaterZone] << endl;
	beginChunk();

	begl();
	*this << keywords[waterType[zone.type]] << endl;

	writeLocation(zone.location);
	
	writeSide(zone.side);

#ifdef CAMPEDIT
	if(zone.connections.entries())
	{
		begl();
		*this << keywords[KW_WaterLink];

		for(int i = 0; i < zone.connections.entries(); i++)
			*this << " " << (int)zone.connections[i];
		*this << endl;
	}
#else
	if(zone.connectCount)
	{
		begl();

		*this << keywords[KW_WaterLink];

		int i = zone.connectCount;
		int f = zone.connections;
		while(i--)
			*this << " " << water.connectList[f++];

		*this << endl;
	}
#endif

#ifdef CAMPEDIT
	if(zone.facilityList.entries())
	{
		begl();
		*this << keywords[KW_Facilities];

		for(int i = 0; i < zone.facilityList.entries(); i++)
			*this << " " << zone.facilityList[i];

		*this << endl;
	}
#else 
	if(zone.facilityCount)
	{
		begl();
		*this << keywords[KW_Facilities];

		int i = zone.facilityCount;
		int f = zone.facilityList;
		while(i--)
			*this << " " << water.facilityList.get(f++);

		*this << endl;
	}
#endif

	writeBoats(zone.boats);

	finishChunk();
}


void DataFileOutStream::writeBoats(Flotilla& boats)
{
#if 0
	if(boats.totalBoats())
	{
		beginChunk();

		begl();
		*this << keywords[KW_Boats];
#endif

		for(int i = 0; i < Nav_TypeCount; i++)
		{
			if(boats.count[i])
			{
				begl();
				*this << keywords[KW_NavalUnit+i] << " " << (unsigned int)boats.count[i] << endl;
			}
		}

#if 0
		finishChunk();
	}
#endif
}

/*
 * Order of Battle Write
 */

void DataFileOutStream::writeOB(OrderBattle& ob)
{
#ifdef ADD_COMMENTS
	*this << ";---------------------------------------------" << endl;
	*this << "; Order of Battle Section" << endl;
	*this << ";---------------------------------------------" << endl;
#endif

	begl();
	*this << keywords[KW_OBSection] << endl;
	beginChunk();

	/*
	 * Sides
	 */

	if(ob.sides)
		writeUnit(ob.sides);
#if 0
	/*
	 * Free Generals
	 */

#ifdef ADD_COMMENTS
	*this << ";---------------------------------------------" << endl;
	*this << "; Free Generals" << endl;
	*this << ";---------------------------------------------" << endl;
#endif

	GeneralIter genI = *ob.freeGenerals;
	while(++genI)
	{
		General* g = genI.current();

		writeGeneral(g);
	}

	/*
	 * Free Regiments
	 */

#ifdef ADD_COMMENTS
	*this << ";---------------------------------------------" << endl;
	*this << "; Free Regiments" << endl;
	*this << ";---------------------------------------------" << endl;
#endif

	RegimentIter regI = *ob.freeRegiments;
	while(++regI)
	{
		Regiment* r = regI.current();

		writeUnit(r);
	}
#endif
	finishChunk();
}

void DataFileOutStream::writeGeneral(General* g)
{
	begl();
	*this << keywords[KW_General] << endl;
	beginChunk();

	writeName(g->name);
	begl();
	*this << keywords[KW_Rank] << " " << (unsigned int) g->rank << endl;
	begl();
	*this << keywords[KW_Efficiency] << " " << (unsigned int) g->efficiency << endl;
	begl();
	*this << keywords[KW_Ability] << " " << (unsigned int) g->ability << endl;
	begl();
	*this << keywords[KW_Aggression] << " " << (unsigned int) g->aggression << endl;
	begl();
	*this << keywords[KW_Experience] << " " << (unsigned int) g->experience << endl;
	finishChunk();
}

void DataFileOutStream::writeUnit(Unit* unit)
{
	do
	{
		switch(unit->rank)
		{
		case Rank_President:
			{
				President* p = (President *) unit;

				begl();
				*this << keywords[KW_President] << endl;
				beginChunk();

				writeSide(p->getSide());

				/*
	 			 * Free Generals
	 			 */

#ifdef ADD_COMMENTS
				begl();
				*this << ";---------------------------------------------" << endl;
				begl();
				*this << "; Free Generals" << endl;
				begl();
				*this << ";---------------------------------------------" << endl;
#endif

				GeneralIter genI = *p->freeGenerals;
				while(++genI)
				{
					General* g = genI.current();

					writeGeneral(g);
				}

				/*
	 			 * Free Regiments
	 			 */

#ifdef ADD_COMMENTS
				begl();
				*this << ";---------------------------------------------" << endl;
				begl();
				*this << "; Free Regiments" << endl;
				begl();
				*this << ";---------------------------------------------" << endl;
#endif

#if 0
				Unit* r = p->freeRegiments;
				while(r)
				{
					writeUnit(r);
					r = r->sister;
				}
#else
				if(p->freeRegiments)
					writeUnit(p->freeRegiments);
#endif

#ifdef ADD_COMMENTS
				begl();
				*this << ";---------------------------------------------" << endl;
#endif
			}
			break;

		case Rank_Army:
				{
					Army* a = (Army*) unit;
#ifdef TESTING
					strcat(popupBuffer, "\r");
					strcat(popupBuffer, a->name);
					popup->showText(popupBuffer);
#endif
					begl();
					*this << keywords[KW_Army] << endl;
					beginChunk();

					writeName(a->name);
				}
				break;

		case Rank_Corps:
				begl();
				*this << keywords[KW_Corps] << endl;
				beginChunk();
				break;

		case Rank_Division:
					begl();
					*this << keywords[KW_Division] << endl;
					beginChunk();
				break;

		case Rank_Brigade:
				begl();
				*this << keywords[KW_Brigade] << endl;
				beginChunk();
				break;

		case Rank_Regiment:
			{
				Regiment* r = (Regiment *) unit;

				begl();
				*this << keywords[KW_Regiment] << endl;
				beginChunk();

				writeName(r->name);

				begl();
 				*this << keywords[KW_Type] <<
					" " << (unsigned int) r->type.basicType <<
					" " << (unsigned int) r->type.subType << endl;
				begl();
 				*this << keywords[KW_Strength] << " " << r->strength << endl;
				begl();
 				*this << keywords[KW_Stragglers] << " " << r->stragglers << endl;
				begl();
 				*this << keywords[KW_Casualties] << " " << r->casualties << endl;
				begl();
 				*this << keywords[KW_Fatigue] << " " << (unsigned int) r->fatigue << endl;
				begl();
 				*this << keywords[KW_Morale] << " " << (unsigned int) r->morale << endl;
#if 0
				begl();
 				*this << keywords[KW_Speed] << " " << r->speed << endl;
#endif
				begl();
 				*this << keywords[KW_Supply] <<
#if 0
					" " << (unsigned int) r->supply.food << 
					" " << (unsigned int) r->supply.forage << 
					" " << (unsigned int) r->supply.ammo << endl;
#else
					" " << (unsigned int) r->supply << endl;
#endif
				begl();
 				*this << keywords[KW_Experience] << " " << (unsigned int) r->experience << endl;

			}
			break;

		default:
			return;
		}


		begl();
		*this << keywords[KW_Unit] << endl;
		beginChunk();

		// OrderList

		if(unit->orderList.entries())
		{
			begl();
			*this << keywords[KW_OrderList] << endl;
			beginChunk();

			PtrSListIter<SentOrder> iter = unit->orderList;

			while(++iter)
			{
				SentOrder* order = iter.current();

				writeSentOrder(*order);
			}

			finishChunk();
		}

		if(unit->general)
			writeGeneral(unit->general);

		if(unit->isCampaignReallyIndependant())
			writeOrders(unit->realOrders);

#if 0
		if(unit->lastMovedTime.value || unit->lastMovedTime.ticks)
		{
			begl();
			*this << keywords[KW_MoveTime] << " " << unit->lastMovedTime.value <<
				" " << unit->lastMovedTime.ticks << endl;
		}
#endif

		// Rank is already known because of what chunk this is in.

		writeLocation(unit->location);

		if(unit->occupying)
		{
			begl();
			if(unit->siegeAction == SIEGE_Occupying)
				*this << keywords[KW_Occupying];
			else // if(unit->siegeAction == SIEGE_Besieging)
				*this << keywords[KW_Sieging];

			FacilityID id = world->facilities.getID(unit->occupying);
			*this << " " << (int) id << endl;
		}

		finishChunk();

		/*
	 	 * Include subordinates
	 	 */

		if(unit->child)
			writeUnit(unit->child);

		finishChunk();

		unit = unit->sister;

	} while(unit);
}

void DataFileOutStream::writeName(const char* name)
{
#ifdef CAMPEDIT
	if(name && name[0])
#else
	if(name)
#endif
	{
		begl();
		*this << keywords[KW_Name] << " " << quote << name << quote << endl;
	}
}

void DataFileOutStream::writeSentOrder(SentOrder& order)
{
	begl();
	*this << keywords[KW_SentOrder] << endl;
	beginChunk();
	begl();
	*this << keywords[KW_Time] << " " << order.time.days << " " << order.time.ticks << endl;
	writeOrders(order);
	finishChunk();
}

void DataFileOutStream::writeOrders(Order& order)
{
	if( (order.mode != ORDER_Stand) || (order.how != ORDER_Land) ||
		 (order.destination.x != 0) || (order.destination.y != 0) )
	{

		begl();
		*this << keywords[KW_Order] << " " <<
			keywords[KW_OrderMode + order.mode] << " " <<
			keywords[KW_OrderHow + order.how] << endl;

		beginChunk();

		writeLocation(order.destination);

		finishChunk();
	}
}


void DataFileOutStream::writeSide(Side side)
{
	if(side)
	{
		begl();
		*this << keywords[KW_Side];

		if(side & SIDE_USA)
			*this << " " << keywords[KW_USA];
		if(side & SIDE_CSA)
			*this << " " << keywords[KW_CSA];
		*this << endl;
	}
}































