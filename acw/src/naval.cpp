/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Naval Units
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/07/28  18:57:04  Steven_Green
 * Naval Combat and Movement implemented
 *
 * Revision 1.2  1994/07/25  20:32:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/06/07  18:29:54  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "naval.h"
#include "camptab.h"
#include "trig.h"
#include "game.h"
#include "error.h"

#if defined(DEBUG) && !defined(CAMPEDIT)
#include "clog.h"
extern LogFile waterLog;
#endif


/*
 * Get values from the above tables
 */

UBYTE getNavalCombat(NavalType type)
{
	return navalCombat[type];
}

UBYTE getNavalBombardment(NavalType type)
{
	return navalBombardment[type];
}


Flotilla::Flotilla()
{
	for(int i = 0; i < Nav_TypeCount; i++)
		count[i] = 0;
}

Flotilla::~Flotilla()
{
}

int Flotilla::totalBoats() const
{
	int total = 0;

	for(int i = 0; i < Nav_TypeCount; i++)
		total += count[i];

	return total;
}

void Flotilla::clear()
{
	for(int i = 0; i < Nav_TypeCount; i++)
		count[i] = 0;
}

Flotilla& Flotilla::operator += (const Flotilla& f)
{
	for(int i = 0; i < Nav_TypeCount; i++)
		count[i] += f.count[i];
	return *this;
}

Flotilla& Flotilla::operator -= (const Flotilla& f)
{

	for(int i = 0; i < Nav_TypeCount; i++)
	{
		// Silly Chris code!!!
		// if ( ( count[i] - f.count[i] ) > count[i] )

		if(f.count[i] > count[i])
#ifdef DEBUG
			throw GeneralError( "Error in flotilla subtraction" );
#else
			count[i] = 0;
#endif
		else
			count[i] -= f.count[i];
	}
	return *this;
}

Flotilla& Flotilla::operator = (const Flotilla& f)
{
	for(int i = 0; i < Nav_TypeCount; i++)
		count[i] = f.count[i];
	return *this;
}

CombatValue Flotilla::totalCombatFactor() const
{
	CombatValue total = 0;

	for(int i = 0; i < Nav_TypeCount; i++)
		total += count[i] * getNavalCombat(NavalType(i));
	return total;
}


CombatValue Flotilla::totalBombardmentFactor() const
{
	CombatValue total = 0;

	for(int i = 0; i < Nav_TypeCount; i++)
		total += count[i] * getNavalBombardment(NavalType(i));
	return total;
}


UBYTE Flotilla::getSpeed() const
{
	int speed = 0;

	for(int i = 0; i < Nav_TypeCount; i++)
	{
		if(count[i] && (!speed || (navalSpeeds[i] < speed)))
			speed = navalSpeeds[i];
	}

	if(!speed)
		return navalSpeeds[0];

	return speed;
}


void Flotilla::sinkBoats(UBYTE percent, Boolean force, Flotilla& lostBoats)
{
	int total = totalBoats();

	int losses = (total * percent) / 100;
	if(force && !losses)
		losses = 1;

#if defined(DEBUG) && !defined(CAMPEDIT)
	waterLog.printf("sinkBoats(%d%%, %s), total=%d, losses=%d",
		(int)percent,
		force ? "True" : "False",
		total,
		losses);
#endif
	while(losses-- && total)
	{
		UBYTE which;

		do
		{
			which = game->gameRand(Nav_TypeCount);
		} while(!count[which]);

		lostBoats.count[which]++;
		count[which]--;
		total--;
	}
}

/*
 * Fleet Maintenance
 */

FleetList::~FleetList()
{
	/*
	 * Remove fleets
	 */

	while(entry)
	{
		Fleet* f = entry;
		entry = f->next;
		delete f;
	}

}

void FleetList::add(Fleet* fleet)
{
	fleet->next = entry;
	entry = fleet;
}

void FleetList::remove(Fleet* fleet)
{
	Fleet* f = entry;
	Fleet* last = 0;

	while(f)
	{
		if(f == fleet)
		{
			if(last)
				last->next = f->next;
			else
				entry = f->next;

			return;
		}

		last = f;
		f = f->next;
	}
}

void FleetList::total(Flotilla& f) const
{
	Fleet* fleet = entry;
	while(fleet)
	{
		f += fleet->boats;

		fleet = fleet->next;
	}
}

int FleetList::entries()
{								
	int count = 0;
	Fleet* fleet = entry;
	while(fleet)
	{
		count++;
		fleet = fleet->next;
	}
	return count;
}

#ifdef DEBUG
static int nextFleetID = 0;
#endif

Fleet::Fleet()
{
	next = 0;
#ifdef DEBUG
	id = nextFleetID++;
#endif
}

Fleet::Fleet(const Flotilla& b, Side s)
{
	boats = b;
	side = s;

	// where, destination, via and arrival time to be set up later

#ifdef DEBUG
	id = nextFleetID++;
#endif
}


