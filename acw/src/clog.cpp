/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Combat Log
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG

#include <string.h>
#include "clog.h"
#include "gametime.h"
#include "miscfunc.h"
#include "memptr.h"

/*
 * Combat Log stuff
 */

#include <stdarg.h>
#include <time.h>


TimeInfo* LogFile::theTime = 0;

LogFile::LogFile(const char* s)
{
	name = s;
	log = 0;
	lastDay = -1;		// Illegal value to force update
	showTime = True;
	created = False;
}

LogFile::~LogFile()
{
	if(log)
	{
	 	time_t t;
	 	time(&t);
	 	fprintf(log, "\nLogfile %s closed at %s\n", name, ctime(&t));
		fclose(log);
	}
}

void LogFile::printf(const char* fmt, ...)
{
	if(!log)
	{
		if(created)
		{
			log = fopen(name, "a");
		}
		else
		{
			log = fopen(name, "w");

			if(log)
			{
				time_t t;
				time(&t);
				fprintf(log, "Logfile %s opened at %s\n\n", name, ctime(&t));
				created = True;
			}
		}
	}

	if(log)
	{
		/*
		 * Display the battle time first
		 */

		TimeInfo* info;
		TimeInfo tTime;

		if(theTime)
			info = theTime;
		else
		{
			info = &tTime;
			info->setRealTime();
		}

		if(info->day != lastDay)
		{
			lastDay = info->day;

			const int bufLen = 500;


			// char buffer[80];
			MemPtr<char> buffer(bufLen);

			sprintf(buffer, " %s %d%s, %d ",
				info->monthStr,
				info->day,
				makeNths(info->day),
				info->year);

			ASSERT(strlen(buffer) < bufLen);

			fprintf(log, "\n");

			int j = 78 - strlen(buffer);
			int i = j / 2;
			j -= i;

			while(i--)
				fprintf(log, "-");
			fprintf(log, "%s", (char*) buffer);
			while(j--)
				fprintf(log, "-");
			fprintf(log, "\n");

		}

		if(showTime)
		{
			fprintf(log, "%02d:%02d:%02d: ",
				info->hour,
				info->minute,
				info->second);
		}

		va_list vaList;
		va_start(vaList, fmt);
		vfprintf(log, fmt, vaList);
		va_end(vaList);

		fprintf(log, "\n");
		fflush(log);
	}
}

/*
 * Temporarily close a log file
 */

void LogFile::close()
{
	if(log)
	{
		fclose(log);
		log = 0;
	}
}

#endif		// DEBUG

