/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	FLIC file reader
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
// Revision 1.2  1994/09/23  13:28:28  Steven_Green
// *** empty log message ***
//
// Revision 1.1  1994/09/07  14:14:16  Steven_Green
// Initial revision
//
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG1
#include <iostream.h>
#endif

#include "flicread.h"
#include "image.h"
#include "lzhuf.h"

/*
 * Constructor
 * Opens file, fills in header and sets up for 1st frame
 */


FlicRead::FlicRead(const char* fileName, Boolean use64)
{
#ifdef DEBUG1
	cout << "Reading " << fileName << endl;
#endif

	finished = True;	// Set so that even if error isn't detected it will quit
	paletteChanged = False;
	palette64 = use64;

	error = file.fileOpen(fileName);
	
	if(!error)
	{
#ifdef DEBUG1
	cout << "Opened" << endl;
#endif

		error = file.fileRead(&header, sizeof(header));

		if(!error)
		{
			if(header.type == FLC_TYPE)
			{
#ifdef DEBUG1
				cout << "It is an FLC" << endl;
#endif
			}
			else if(header.type == FLI_TYPE)
			{
#ifdef DEBUG1
				cout << "It is an FLI" << endl;
#endif
				header.oframe1 = sizeof(header);
				header.speed = (header.speed * 1000) / 70;
			}
			else
			{
#ifdef DEBUG1
				cout << "It is not an FLI or FLC!!!" << endl;
#endif
				error = -1;
				return;
			}

			/*
			 * Continue with setup
			 */
			 
			if(!header.oframe2)		// Useful for looping
			{
				file.fileSeek(header.oframe1);
				FrameHead frameHead;
				file.fileRead(&frameHead, sizeof(frameHead));
				header.oframe2 = header.oframe1 + frameHead.size;
			}

			file.fileSeek(header.oframe1);
			finished = False;
			frame = 0;
			loops = 0;

			return;
		}
		file.fileClose();		// Error so close down file
	}
}

/*
 * Destructor
 * Closes file
 */

FlicRead::~FlicRead()
{
	if(!error)
		file.fileClose();
}

/*
 * Next frame, reads the next frame into the give image.
 * The image must be the same image each time this is called because
 * it used for delta frames.
 * The first time it is called the given image will be filled in with
 * the correct sizes.
 */

void FlicRead::nextFrame()
{
	if(error)
	{
		finished = True;
		error = True;
		return;
	}

	if( (image.getWidth() < header.width) ||
		 (image.getHeight() < header.height) )
	{
		image.resize(header.width, header.height);
	}

	FrameHead frameHead;

	paletteChanged = False;

	error = file.fileRead(&frameHead, sizeof(frameHead));
	if(!error)
	{
#ifdef DEBUG1
		cout << "Frame " << frame <<
	  			  ", size:" << frameHead.size <<
				  ", type:" << hex << frameHead.type << dec <<
				  ", chunks:" << frameHead.chunks << endl;
#endif

		if(frameHead.type == FRAME_TYPE)
		{
			// Get size of data

			long size = frameHead.size - sizeof(frameHead);

			if(size > 0)
			{
				UBYTE* chunk = new UBYTE[size];
				file.fileRead(chunk, size);
				decodeFrame(frameHead, chunk);
				delete[] chunk;
			}
		}
		else
		{
#ifdef DEBUG1
			cout << "FrameHeader type is not a frame!" << endl;
#endif
			error = -1;
		}

		if(finished)
		{
			finished = False;
			frame = 1;
			file.fileSeek(header.oframe2);
		}
		else
		{
			frame++;
			if(frame >= header.frames)
			{
				finished = True;
				loops++;
			}
		}
	}
}

/*
 * Process a compressed chunk
 */

void FlicRead::decompressChunk(ChunkHead* chunk)
{
	union {
		UBYTE* bPtr;
		ULONG* lPtr;
	} body;

	body.bPtr = (UBYTE*)(chunk + 1);

	long finalSize = *body.lPtr++;

	UBYTE* dest = new UBYTE[finalSize];

	decode_lzhuf(body.bPtr, dest, chunk->size, finalSize);

	switch(chunk->type)
	{
		case DELTA_FLC_LZW:
			decodeDeltaFLC(dest);
			break;

		case DELTA_FLI_LZW:
			decodeDeltaFLI(dest);
			break;

		case BYTE_RUN_LZW:
			decodeByteRun(dest);
			break;

		case LITERAL_LZW:
			decodeLiteral(dest);
			break;

		default:
#ifdef DEBUG1
			cout << "Unknown Chunk" << endl;
#endif
			break;
	}

	delete[] dest;
}

/*
 * Decode a frame in memory
 */

void FlicRead::decodeFrame(FrameHead& frameHead, const UBYTE* data)
{
	for(int i = 0; i < frameHead.chunks; i++)
	{
		ChunkHead* chunk = (ChunkHead*) data;
		data += chunk->size;
		UBYTE* body = (UBYTE *)(chunk + 1);

#ifdef DEBUG1
		cout << "Chunk " << i << ", size:" << chunk->size << ", type:" << chunk->type << endl;
#endif

		switch(chunk->type)
		{
		case COLOR_256:
#ifdef DEBUG1
			cout << "Color 256 Chunk" << endl;
#endif
			decodeColor256(body);
			break;
		case DELTA_FLC:
#ifdef DEBUG1
			cout << "Delta FLC Chunk" << endl;
#endif
			decodeDeltaFLC(body);
			break;
		case COLOR_64:
#ifdef DEBUG1
			cout << "Color 64 Chunk" << endl;
#endif
			decodeColor64(body);
			break;
		case DELTA_FLI:
#ifdef DEBUG1
			cout << "Delta FLI Chunk" << endl;
#endif
			decodeDeltaFLI(body);
			break;
		case BLACK:
#ifdef DEBUG1
			cout << "Black Chunk" << endl;
#endif
			decodeBlack(body);
			break;
		case BYTE_RUN:
#ifdef DEBUG1
			cout << "Byte Run Chunk" << endl;
#endif
			decodeByteRun(body);
			break;
		case LITERAL:
#ifdef DEBUG1
			cout << "Literal Chunk" << endl;
#endif
			decodeLiteral(body);
			break;

		/*
		 * LZW extended types for .FLZ files
		 */

		case DELTA_FLC_LZW:
		case DELTA_FLI_LZW:
		case BYTE_RUN_LZW:
		case LITERAL_LZW:
			decompressChunk(chunk);
			break;

		default:
#ifdef DEBUG1
			cout << "Unknown Chunk" << endl;
#endif
			break;
		}
	}
}

/*================================================================
 * Specific Chunk decoders
 */

void FlicRead::decodeColor256(const UBYTE* body)
{
	WORD* wp = (WORD*)body;
	WORD ops = *wp;
	UBYTE* cbuf = (UBYTE*)(wp + 1);
	int start = 0;

	if(palette64)
	{
		Palette64* pal = (Palette64*)&palette;

		while(--ops >= 0)
		{
			start += *cbuf++;
			int count = *cbuf++;
			if(count == 0)
				count = 256;
			pal->setColours(start, (TrueColour*)cbuf, count);
			cbuf += 3 * count;
			start += count;
		}
	}
	else
	{
		while(--ops >= 0)
		{
			start += *cbuf++;
			int count = *cbuf++;
			if(count == 0)
				count = 256;
			palette.setColours(start, (TrueColour*)cbuf, count);
			cbuf += 3 * count;
			start += count;
		}
	}

	paletteChanged = True;
}

void FlicRead::decodeDeltaFLC(const UBYTE* body)
{
	/*
	 * Handy way to allow pointer to get words and bytes
	 */

	union {
		const UWORD* w;
		const UBYTE* b;
	} wpt;

	wpt.b = body;

	int lastX = image.getWidth() - 1;

	UBYTE* dest = image.getAddress();

	/*
	 * 1st word is number of packer lines
	 */

	UWORD lpCount = *wpt.w++;

	while(lpCount--)
	{
	top:
		UBYTE* lineAd = dest;

		UWORD opCount = *wpt.w++;

		if( (opCount & 0xc000) == 0xc000)	// Skip lines
		{
			dest += image.getWidth() * (65536 - opCount);
			goto top;
		}
		else if( (opCount & 0xc000) == 0x4000)
		{	// Put pixel at end of line and get new opCount
			dest[lastX] = opCount & 0xff;
			goto top;
		}
		else
		{
			while(opCount--)
			{
				dest += *wpt.b++;				// X adjustment
				UBYTE psize = *wpt.b++;		// RLE value
				if(psize < 0x80)	// Copy pixel pairs
				{
					int amount = psize * 2;
					memcpy(dest, wpt.b, amount);
					wpt.b += amount;
					dest += amount;
				}
				else					// Repeat pixel pair
				{
					psize = 256 - psize;
					UWORD pixels = *wpt.w++;
					UWORD* dw = (UWORD*)dest;
					while(psize--)
						*dw++ = pixels;
					dest = (UBYTE*)dw;
				}
			}
		}

		dest = lineAd + image.getWidth();
	}
}

void FlicRead::decodeColor64 (const UBYTE* body)
{
	WORD* wp = (WORD*)body;
	WORD ops = *wp;
	UBYTE* cbuf = (UBYTE*)(wp + 1);
	int start = 0;

	if(palette64)
	{
		Palette64* pal = (Palette64*)&palette;

		while(--ops >= 0)
		{
			start += *cbuf++;
			int count = *cbuf++;
			if(count == 0)
				count = 256;

			memcpy(&pal->colors[start], cbuf, count * 3);
			cbuf += count * 3;
		}
	}
	else
	{
		while(--ops >= 0)
		{
			start += *cbuf++;
			int count = *cbuf++;
			if(count == 0)
				count = 256;

			while(count--)
			{
				palette.setColour(start++, cbuf[0] << 2, cbuf[1] << 2, cbuf[2] << 2);
				cbuf += 3;
			}
		}
	}

	paletteChanged = True;
}

void FlicRead::decodeDeltaFLI(const UBYTE* body)
{
	UBYTE* dest = image.getAddress();

	WORD* wpt = (WORD*)body;

	/*
	 * 1st word is starting Y coordinate
	 */

	dest += *wpt++ * image.getWidth();

	/*
	 * 2nd word is number of lines
	 */

	int lines = *wpt++;

	UBYTE* cpt = (UBYTE*)wpt;

	while(lines--)
	{
		UBYTE* lineAd = dest;

		/*
		 * First byte is number of opcounts
		 */

		UBYTE opCount = *cpt++;
		while(opCount--)
		{
			/*
			 * X Coordinate offset
			 */

			dest += *cpt++;
			UBYTE psize = *cpt++;

			if(psize >= 0x80)		// Repeat byte
			{
				psize = 256 - psize;
				memset(dest, *cpt++, psize);
			}
			else			// Copy bytes
			{
				memcpy(dest, cpt, psize);
				cpt += psize;
			}
			dest += psize;
		}

		/*
		 * Get ready for next line
		 */

		dest = lineAd + image.getWidth();
	}
}

void FlicRead::decodeBlack   (const UBYTE* body)
{
	memset(image.getAddress(), 0, header.width * header.height);
}

/*
 * Byte Run Chunk Decompression
 */

void FlicRead::decodeByteRun (const UBYTE* src)
{
	UBYTE* dest = image.getAddress();

	int h = header.height;
	while(h--)
	{
		int w = header.width;
		UBYTE* dest1 = dest;

		src++;			// Skip opcount byte
		while(w > 0)
		{
			UBYTE psize = *src++;
			if(psize < 128)			// Copy a single value
			{
				if(psize <= w)
					memset(dest1, *src++, psize);
			}
			else						// Copy a section
			{
				psize = 256 - psize;
				if(psize <= w)
					memcpy(dest1, src, psize);
				src += psize;
			}
			dest1 += psize;
			w -= psize;
		}
		dest += image.getWidth();
	}
}

void FlicRead::decodeLiteral (const UBYTE* body)
{
	memcpy(image.getAddress(), body, header.width * header.height);
}


