/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Game Screen Interfaces
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.24  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.3  1994/01/24  21:19:13  Steven_Green
 * execute functions only activated when button pressed.
 *
 * Revision 1.1  1994/01/17  20:14:26  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include <conio.h>

#include "campscn.h"
#include "campcity.h"
#include "campunit.h"
#include "campaign.h"
#include "ai_camp.h"
#include "campwld.h"
#include "batsetup.h"
#include "savegame.h"
#include "system.h"
#include "screen.h"
#include "mouselib.h"
#include "menudata.h"
#include "clock.h"
#include "cal.h"
#include "connect.h"
#include "gameicon.h"
#include "hintstat.h"
#include "layout.h"
#include "language.h"
#include "mapwind.h"
#include "mapicon.h"
#include "msgwind.h"
#include "font.h"
#include "game.h"
#include "colours.h"
#include "calcbat.h"
#include "memptr.h"
#include "timer.h"
#include "calmulti.h"
#include "camptab.h"
#include "logo.h"
#include "dialogue.h"
#include "strutil.h"
#include "campopt.h"

#ifdef TESTING
#include "options.h"
#endif

#ifdef DEBUG
#include "memlog.h"
#endif

// #include "wsample.h"

#define Font_MsgWindow	Font_EMFL_8

#ifdef DEBUG
static Boolean forceWin = False;
static Boolean forceLose = False;
#endif

/*
 * Packet for keeping current time
 */

struct TimeupdatePKT {
	TimeBase theTime;
};

/*====================================================================
 * Class to hold together the campaign screen
 */

enum CampMenuMode {	// These are in addition to MenuData::MenuMode
	EndDay = 1
};

class CampaignOrderInterface : public MenuData, public MenuConnection, public HintStatus {
	TimeBase nextTimeUpdate;
public:
	CampaignMode mode;

	Colour hintColour;

	// Boolean dayStarted;

	/*
	 * Functions
	 */

	CampaignOrderInterface();
	~CampaignOrderInterface();

	void processPacket(Packet* pkt);	// virtual MenuConnection
	void drawIcon();						// virtual Icon
	void proc();							// virtual MenuData
#ifdef COMMS_TODO
	void checkFinished();
#endif	
	void showVictorySitu();

	void showHint(const char* s) { HintStatus::showHint(s); shownHint = True; }
	void showStatus(const char* s) { HintStatus::showStatus(s); }

	void showHintStatus(const char* s);
#if !defined(COMPUSA_DEMO)
	void saveGame(const char* name);
#endif
};

class MoveInfoArea;

class CampaignMoveInterface : public MenuData, public MenuConnection, public AlertBase, public HintStatus {
	Boolean gotResult;
	UBYTE rxResult;
public:
	MoveInfoArea* messageArea;

	Colour hintColour;

public:
	CampaignMoveInterface();
	~CampaignMoveInterface();

	void processPacket(Packet* pkt);	// virtual MenuConnection
	void drawIcon();						// virtual Icon
	void proc();							// virtual MenuData
#ifdef COMMS_TODO
	void checkFinished();
#endif
	int doAlert(const char* text, const char* buttons, int defaultButton, Facility* f, WaterZone* w, MarkedBattle* mb);

	void showHint(const char* s) { HintStatus::showHint(s); shownHint = True; }
	void showStatus(const char* s) { HintStatus::showStatus(s); }

	void showHintStatus(const char* s);

};

struct AlertPacket {
	UBYTE result;
};

#if !defined(COMPUSA_DEMO)

/*
 * Campaign Save Game
 */

void CampaignOrderInterface::saveGame(const char* name)
{
	saveCampaignGame(name, campaign);
}

#endif	// DEMO

/*====================================================================
 * Icons On Campaign Screen
 */



/*--------------------------------------------------------------------
 * Campaign Clock Icon
 */

class CampaignClockIcon : public ClockIcon {
public:
	CampaignClockIcon(IconSet* set, Point p, TimeInfo* t, GameTime* gt) : ClockIcon(set, p, t, gt) { }

	void execute(Event* event, MenuData* data);
};

/*
 * Click on clock to end days orders
 */

void CampaignClockIcon::execute(Event* event, MenuData* data)
{
	if(event->buttons)
	{
		// data->setFinish(EndDay);
		FinishPKT *packet = new FinishPKT;
		packet->how = EndDay;
		game->connection->sendMessage(Connect_Finish, (UBYTE*)packet, sizeof(FinishPKT));
	}

	if ( event->overIcon ) 
		data->showHint( language( lge_clock ) );
}



class CampaignSaveIcon : public SaveIcon {
public:
	CampaignSaveIcon(IconSet* set, Point p, Key k1 = 0, Key k2 = 0) :
		SaveIcon(set, p, k1, k2) { }

	// void saveGame(const char* name);
};

#ifdef COMMS_OLD
#if !defined(COMPUSA_DEMO)
void CampaignSaveIcon::saveGame(const char* name)
{
	// writeDynamicWorld(name, campaign);
	saveCampaignGame(name, campaign);
}
#endif
#endif


/*--------------------------------------------------------------------
 * Toggle between city and troop mode
 */

class CampModeIcon : public MenuIcon {
	CampaignOrderInterface* owner;
public:
	CampModeIcon(IconSet* parent, CampaignOrderInterface* control) :
		MenuIcon(parent, C_City, Rect(MENU_ICON_X, MENU_ICON_Y8, MENU_ICON_W, MENU_ICON_H), KEY_Space)
	{
		owner = control;
	}

	void execute(Event* event, MenuData* d);
	void drawIcon();
};

void CampModeIcon::drawIcon()
{
	setIcon(owner->mode.getModeIcon());
	MenuIcon::drawIcon();
}

void CampModeIcon::execute(Event* event, MenuData* d)
{
	d=d;

	if(event->buttons)
	{
		owner->mode.toggleMode();
		setRedraw();
	}

	if ( event->overIcon ) 
		d->showHint( language( lge_campMode ) );

}

/*--------------------------------------------------------------------
 * Promote General Icon
 */

class PromoteIcon : public MenuIcon {
	CampaignOrderInterface* owner;
public:
	PromoteIcon(IconSet* parent, CampaignOrderInterface* control) :
		MenuIcon(parent, C1_Promote, Rect(MENU_ICON_X,MENU_ICON_Y7,MENU_ICON_W,MENU_ICON_H))
	{
		owner = control;
	}

	void execute(Event* event, MenuData* d);
};

void PromoteIcon::execute(Event* event, MenuData* d)
{
	if(event->buttons)
	{
		ClockIcon* oldClock = campaign->clockIcon;
		campaign->clockIcon = 0;

		int giveOrders;
		Unit* newUnit = doCampaignCAL(&campaign->world->ob, 0, &giveOrders, d);

		if(newUnit)
		{

			/*
			 * Make sure in troop mode
			 */

			owner->mode.forceTroopMode(newUnit, giveOrders);
		}

		campaign->clockIcon = oldClock;

		d->setUpdate();					// Get the screen redrawn!
	}

	if ( event->overIcon ) 
		d->showHint( language( lge_promoteGen ) );

}

/*--------------------------------------------------------------------
 * Debug Keyboard presses to show grid and terrain
 */

#ifdef TESTING

void toggleBoolean(const char* description, Boolean& value)
{
	value = !value;

	MemPtr<char> buffer(80);

	sprintf(buffer, "%s\r\r%s", description, value ? "ON" : "OFF");
	dialAlert(0, buffer, "OK");
}


class DebugKeys : public NullIcon {
public:
	DebugKeys(IconSet* parent) : NullIcon(parent) { }
	void execute(Event* event, MenuData* data);
};

void DebugKeys::execute(Event* event, MenuData* data)
{
	Key key = event->key;
	if(isascii(key))
		key = toupper(key);

	switch(key)
	{
	case KEY_AltS:
		seeAll = !seeAll;
		campaign->updateMap();
		event->key = 0;
		break;
#ifdef DEBUG
	case KEY_AltA:
		showAI = !showAI;
		break;
	case KEY_AltM:
		dontMove = !dontMove;
		break;
	case KEY_AltP:				// Swap player's side
		game->setupPlayerSide(otherSide(game->getLocalSide()));
		break;
	case KEY_AltI:				// Toggle AI for players side
		{
#if 0
			if(game->isAI(sideIndex(game->getLocalSide())))
				game->setAIside(otherSide(game->getLocalSide()));
			else
				game->setAIside(SIDE_Both);
#else
			game->toggleAI(game->getLocalSide());
#endif
		}
		break;
	case KEY_AltK:				// Toggle Smooth clock during movement
		smoothClock = !smoothClock;
		break;

	case 'B':
		toggleBoolean("No Build Messages", noBuildMessages);
		break;

	case 'G':
		globalOptions.toggle(Options::Grid);
		campaign->mapArea->makeStaticMap();
		campaign->updateMap();
		event->key = 0;
		break;
	case 'T':
		globalOptions.toggle(Options::Terrain);
		campaign->mapArea->makeStaticMap();
		campaign->updateMap();
		event->key = 0;
		break;
	case 'R':
		globalOptions.toggle(Options::RailShow);
		campaign->mapArea->makeStaticMap();
		campaign->updateMap();
		event->key = 0;
		break;
	case 'W':
		globalOptions.toggle(Options::WaterShow);
		campaign->mapArea->makeStaticMap();
		campaign->updateMap();
		event->key = 0;
		break;
	case 'M':
		globalOptions.toggle(Options::QuickMove);
		campaign->updateMap();
		event->key = 0;
		break;
	case KEY_AltW:
		forceWin = True;
		break;
	case KEY_AltL:
		forceLose = True;
		break;
	case KEY_AltB:
		toggleBoolean("Instant Build", instantBuild);
		break;
	case KEY_AltF:
		if(routeMode >= RM_Line)
			routeMode = RM_Full;
		else
			INCREMENT(routeMode);

		{
			static char* routeModeName[] = {
				"Full",
				"Simple",
				"Line"
			};
		 	MemPtr<char> buffer(80);
			sprintf(buffer, "Route Mode is\r%s", routeModeName[routeMode]);
			dialAlert(0, buffer, "OK");
		}

		break;
	case KEY_AltR:
		toggleBoolean("Fast Resources", fastResources);
		break;
	case KEY_AltQ:
		toggleBoolean("Quick Movement", quickMove);
		break;
	case KEY_AltT:
		toggleBoolean("Real Time Campaign", realTimeCampaign);
		break;

	case '?':
		dialAlert(0, "Debug Keys\r\r"
  						 "\x01" "Alt A : Show AI info\r"
  						 "\x01" "Alt B : Instant Build\r"
						 "\x01" "Alt F : Fast Route Finder\r"
  						 "\x01" "Alt I : Enable or Disable AI from player's side\r"
  						 "\x01" "Alt K : Smooth Clock enable/disable\r"
  						 "\x01" "Alt L : Force player to lose\r"
  						 "\x01" "Alt M : Don't move campaign units\r"
  						 "\x01" "Alt P : Swap Player's Sides\r"
						 "\x01" "Alt Q : Quick Movement on terrain grid\r"
						 "\x01" "Alt R : Fast Resources\r"
  						 "\x01" "Alt S : See enemy\r"
						 "\x01" "Alt T : Campaign Real Time\r"
  						 "\x01" "Alt W : Force player to win\r\r"
						 "\x01" "B : Disable Build Messages\r"
      				 "\x01" "G : Overlay a grid on the campaign map\r"
      				 "\x01" "M : Quick move 1\r"
      				 "\x01" "R : Show railway network\r"
      				 "\x01" "T : Overlay terrain map\r"
      				 "\x01" "W : Show water network\r",

						 "OK");

#endif
	}
}

#endif	// TESTING


int Campaign::doAlert(const char* text, const char* buttons, int defaultButton, Facility* f, WaterZone* w, MarkedBattle* mb)
{
	if(alertArea)
		return alertArea->doAlert(text, buttons, defaultButton, f, w, mb);
	else
		return dialAlert(0, text, buttons);
}

/*--------------------------------------------------------------------
 * Default Icons up the right hand side
 */

class C1Menu : public SubMenu {
public:
	C1Menu(CampaignOrderInterface* parent);

	void drawIcon();
};


C1Menu::C1Menu(CampaignOrderInterface* parent) :
	SubMenu(parent, Rect(MENU_X, MENU_Y, MENU_WIDTH, MENU_HEIGHT))	// was 519,0,113,423 wide
{
	icons = new IconList[3];

	icons[0] = new PromoteIcon(this, parent);
	icons[1] = new CampModeIcon(this, parent);
	icons[2] = 0;
}
 
void C1Menu::drawIcon()
{
	{
		Region bitmap(machine.screen->getImage(), Rect(getPosition(), getSize()));

		putLogo(&bitmap);

	}

	SubMenu::drawIcon();
}

/*====================================================================
 * Implementation for Campaign screen
 */

/*
 * This function called all the time, regardless of dialogue boxes, etc
 */

void CampaignOrderInterface::proc()
{
	if(!isPaused())
	{
		if(campaign->gameTime > nextTimeUpdate)
		{
			nextTimeUpdate = campaign->gameTime + GameTicksPerMinute * 10;

			TimeupdatePKT* pkt = new TimeupdatePKT;
			pkt->theTime = campaign->gameTime;
			game->connection->sendMessage(Connect_CS_TimeUpdate, (UBYTE*)pkt, sizeof(TimeupdatePKT));
		}


		campaign->ordersPhase();
		if(!campaign->givingOrders)
			setFinish(EndDay);
	}
	processMessages(False);
}

/*
 * Create Campaign Screen
 */

CampaignOrderInterface::CampaignOrderInterface() :
	MenuData(),
	MenuConnection("CampaignOrder"),
	mode(this)
{
#ifdef DEBUG
	cLog.setTime(&campaign->timeInfo);
#endif
	// dayStarted = False;

#ifdef COMMS_OLD
	waitSave = False;
#endif

	// nextTimeUpdate = 0;

	/*
	 * Set up Icons
	 */

#ifdef TESTING
	icons = new IconList[16];
#else
	icons = new IconList[15];
#endif
	Icon** iconPtr = icons;

	*iconPtr++ = campaign->subMenu = new C1Menu(this);
	*iconPtr++ = campaign->mapArea = new MapWindow(this, &mode, campaign->mapSettings.style, campaign->mapSettings.centre);
	*iconPtr++ = new MapViewIcon(this, campaign->mapArea);
	*iconPtr++ = new CampaignScrollLeftIcon(this);
	*iconPtr++ = new CampaignScrollRightIcon(this);
	*iconPtr++ = new CampaignScrollUpIcon(this);
	*iconPtr++ = new CampaignScrollDownIcon(this);

	*iconPtr++ = new MainMenuIcon										(this, Point(MAIN_ICON_X1,MAIN_ICON_Y));
	*iconPtr++ = campaign->clockIcon = new CampaignClockIcon	(this, Point(MAIN_ICON_X2,MAIN_ICON_Y), &campaign->timeInfo, &campaign->gameTime);
	*iconPtr++ = new MiniSoundIcon	 								(this, Point(MAIN_ICON_X3,MAIN_ICON_Y+53));
	*iconPtr++ = new CampaignOptionsIcon							(this, Point(MAIN_ICON_X3,MAIN_ICON_Y));
	*iconPtr++ = new CampaignSaveIcon								(this, Point(MAIN_ICON_X4,MAIN_ICON_Y));

	*iconPtr++ = new QuitIcon(this);

	// *iconPtr++ = campaign->message = new MessageWindow(this, Rect(MSGW_X,MSGW_Y,MSGW_W,MSGW_H), Font_MsgWindow);

#ifdef TESTING
	*iconPtr++ = new DebugKeys(this);
#endif

#ifdef DEBUG1
	*iconPtr++ = new GridIcon(this);
	*iconPtr++ = new TerrainIcon(this);
	*iconPtr++ = new RailIcon(this);
	*iconPtr++ = new WaterIcon(this);
	*iconPtr++ = new QuickMoveIcon(this);
#endif
	*iconPtr++ = 0;

	campaign->interface = this;

	mode.setMode(campaign->mapSettings.mode);
	processMessages(True);		// Wait for Sync
}

/*
 * Remove Campaign Screen
 */

CampaignOrderInterface::~CampaignOrderInterface()
{
	processMessages(True);

	campaign->getMapSettings();

	campaign->interface = 0;

	campaign->subMenu->destroy();
	campaign->subMenu = 0;
	campaign->mapArea = 0;
	campaign->clockIcon = 0;
	// campaign->message = 0;
}

void CampaignOrderInterface::drawIcon()
{
	showFullScreen("art\\screens\\playscrn.lbm");
	MenuData::drawIcon();

	/*
	 * Get the hint colour from the screen
	 */

	hintColour = *machine.screen->getImage()->getAddress(10,1);

#ifdef DEBUG
	showHint("Campaign Screen");
#else
	showHint("");
#endif

}

void CampaignOrderInterface::showHintStatus(const char* s)
{
	campaign->msgArea->draw(s);

	if ( !s ) showVictorySitu();
}

/*
 * Display Victory Status Bar...
 *
 * Originally written by Chris
 * Tidied up by Steven.
 */

void CampaignOrderInterface::showVictorySitu()
{
	Colours VictoryMark = Red;

	const SDimension barX = 30;
	const SDimension barY = 6;
	const SDimension barW = MSGW_W - barX * 2;
	const SDimension barH = MSGW_H - barY * 2;

	/*
	 * Set up the screen area
	 */

	Region window = Region(machine.screen->getImage(), Rect(MSGW_X ,MSGW_Y,MSGW_W, MSGW_H));

	/*
	 * Draw a Frame
	 */

	window.frame(barX-1, barY-1, barW+2, barH+2, Black);

	/*
	 * Put on Some Flags (13 x 11 pixels)
	 */

	game->sprites->drawSprite(&window, Point(barX - 20,             (MSGW_H - 11)/2), TinyFlagUSA);
	game->sprites->drawSprite(&window, Point(barX + barW + 20 - 13, (MSGW_H-11)/2),   TinyFlagCSA);

	/*
	 * Get midpoint for chart
	 */

  	SDimension w1 = SDimension((campaign->USAVictory * barW) / ( campaign->CSAVictory + campaign->USAVictory ) );
	SDimension w2 = barW - w1;

	/*
	 * Draw The bar
	 */
  
	window.box (barX,      barY, w1, barH, USAColour);
	window.box (barX + w1, barY, w2, barH, CSAColour);


	/*
	 * Place the threshold points
	 */

	// w1 = SDimension( ( ( (campaign->USAVictory + campaign->CSAVictory) * CSAWinVictory * MSGW_W ) / 100 ) / ( campaign->CSAVictory + campaign->USAVictory ) );

	w1 = (campaign->CSAWinVictory * barW) / 100;
	
	window.VLine(barX + w1, barY - 3,        2, VictoryMark);
	window.VLine(barX + w1, barY + barH + 1, 2, VictoryMark );

	w1 = (campaign->USAWinVictory * barW) / 100;

	window.VLine(barX + w1, barY - 3,        2, VictoryMark);
	window.VLine(barX + w1, barY + barH + 1, 2, VictoryMark );
	
	// Starting Point

	w1 = (campaign->CSAStartVictory * barW) / 256;
	window.VLine(barX + w1, barY + 1, barH - 2, White);

	// w1 = SDimension( ( ( (campaign->USAVictory + campaign->CSAVictory) * USAWinVictory * MSGW_W ) / 100 ) / ( campaign->CSAVictory + campaign->USAVictory ) );
	// window.VLine( MSGW_X + 75 + w1, MSGW_Y + MSGW_H - 9, 2, VictoryMark );
	// window.VLine( MSGW_X + 75 + w1, MSGW_Y + + 4, 2, VictoryMark );

	// delete window;
}

/*
 * Multi-Player Packet Processor
 */

void CampaignOrderInterface::processPacket(Packet* packet)
{
	int result;
	int how;

	switch(packet->type)
	{
	case Connect_CAL_TransferUnit:
		rxCALTransferUnit(packet);
		break;

	case Connect_CAL_TransferGeneral:
		rxCALTransferGeneral(packet);
		break;

	case Connect_CAL_MakeNew:
		rxCALMakeNew(packet);
		break;

	case Connect_CAL_Rejoin:
		rxCALRejoinCampaign(packet);
		break;

	case Connect_CAL_Reattach:
		rxCALReattachCampaign(packet);
		break;

	case Connect_CAL_ReattachAll:
		rxCALReattachAllCampaign(packet);
		break;


	case Connect_CS_Order:
		campaign->rxOrder(packet->data);
		break;

	case Connect_CS_SendFleet:
		campaign->rxFleet(packet->data);
		break;

	case Connect_CS_Mobilise:
		campaign->rxMobilise(packet->data);
		break;

	case Connect_Finish:
		if(!isFinished())
		{
			if(packet->sender == RID_Local)
			{
				FinishPKT* pkt = (FinishPKT*)packet->data;

				if(waitRemoteReply())
				{
					setFinish(pkt->how);
				}
				else
				{
					if(pkt->how == EndDay)
						dialAlert(this, language( lge_RemoteNotReady ), language( lge_OK ) );
					else
						dialAlert(this, language( lge_remoteNoleave ), language( lge_OK ) );
				}
			}
			else
			{
				FinishPKT* pkt = (FinishPKT*)packet->data;

				const char* text;
				const char* buttons;

				switch(pkt->how)
				{
				case EndDay:
					text = language(lge_RemoteEndDay);
					buttons = language(lge_EndDayCont);
					break;
				default:
				case MenuOK:
					text = language(lge_RemoteStart);
					buttons = language(lge_startcancel);
					break;
				case MenuMainMenu:
					text = language(lge_RemoteMM);
					buttons = language(lge_mmCancel);
					break;
				}
				if(askRemoteReply(text, buttons))
					setFinish(pkt->how);
	
			}
		}
		break;

	case Connect_CS_TimeUpdate:
		{
			TimeupdatePKT* pkt = (TimeupdatePKT*) packet->data;

			if(pkt->theTime > campaign->gameTime)
			{
#ifdef DEBUG
				netLog.printf("Time updated from %d:%ld to %d:%ld",
					(int) campaign->gameTime.days, campaign->gameTime.ticks,
					(int) pkt->theTime.days, pkt->theTime.ticks);
#endif
				campaign->gameTime = pkt->theTime;
				campaign->timeInfo = campaign->gameTime;
				if(campaign->clockIcon)
					campaign->clockIcon->setRedraw();
			}
#ifdef DEBUG
			else
				netLog.printf("Time didn't need updating");
#endif
		}
		break;

	default:
#ifdef DEBUG
		netLog.printf("Unknown packet received\rtype: %d, length:%d", (int) packet->type, (int)packet->length);
#endif
		break;
	}
}

#ifdef COMMS_TODO
void CampaignOrderInterface::checkFinished()
{
	/*
	 * If we tried to exit, wait for the remote player to make up his mind
	 */

	if(isFinished() && game->connection && !hasRemoteAnswered())
	{
		if(getMode() != MenuQuit)
		{
			if(getMode() == EndDay)
			{
				game->connection->sendData(Connect_CS_EndDay, 0, 0);
			}
			else
			{
				FinishPKT *packet = new FinishPKT;
				packet->how = getMode();

			  	game->connection->sendData(Connect_Finish, (UBYTE*)packet, sizeof(FinishPKT));
			}

			if(!waitRemoteReply())
			{
				clearFinish();
				if(getMode() == EndDay)
					dialAlert(this, language( lge_RemoteNotReady ), language( lge_OK ) );
				else
					dialAlert(this, language( lge_remoteNoleave ), language( lge_OK ) );
			}
			else
				setFinish(getMode());
		}
	}

	if(getRemoteWantFinish())
	{
		clearRemoteWantFinish();

		int result;

		switch(getMode())
		{
		case EndDay:
			// result = dialAlert(this, language( lge_RemoteEndDay ), language( lge_EndDayCont));
			result = dialAlert(0, language( lge_RemoteEndDay ), language( lge_EndDayCont));
			break;
		case MenuOK:
			// result = dialAlert(this, language( lge_RemoteStart ), language( lge_startcancel ) );
			result = dialAlert(0, language( lge_RemoteStart ), language( lge_startcancel ) );
			break;
		case MenuMainMenu:
			// result = dialAlert(this, language( lge_RemoteMM ), language( lge_mmCancel ));
			result = dialAlert(0, language( lge_RemoteMM ), language( lge_mmCancel ));
			break;
		}

		switch(result)
		{
		case 0:	// Start Game
			game->connection->sendData(Connect_CS_ExitScreen, 0, 0);

#if 0
			if(getMode() == EndDay)
			{
				campaign->finishOrders();
				setRemoteAnswer(True);
				// clearRemoteAnswer();
			}
			else
#endif
			{
				setFinish(getMode());
				setRemoteAnswer(True);
			}
			break;

		case 1:	// Cancel
		case MenuQuit:
		default:
			if(game->connection)
				game->connection->sendData(Connect_CS_NoExitScreen, 0, 0);
			clearRemoteAnswer();
			break;
		}

		multiPlayer();
	}
}
#endif


/*------------------------------------------------------------------
 * Procedure to call every game frame
 */


/*=====================================================================
 * Play the campaign Game!
 */

int Campaign::runCampaign()
{
	Boolean finished = False;
	int retVal = 0;

	CampaignOrderInterface* interface = 0;

	while(!finished)
	{
		if(!interface)
		{
			interface = new CampaignOrderInterface;
			givingOrders = True;
			interface->drawIcon();
#ifdef COMMS_TODO
			interface->waitSync();
#endif
			newDay = True;
		}

		/*
		 * Check the time...
		 * If in order phase set up user interface and process until end of day
		 * (take into account being in CAL, etc)
		 *
		 * If in move phase close down interface and process movement
		 *
		 * If there is a battle list, then prompt player to play them
		 * if any is played then close down interface.
		 */

		while(!interface->isFinished())
		{
			interface->mode.overFlag = False;
			interface->process();		// This indirectly calls campaign::process

			if(!interface->mode.overFlag)
				interface->mode.offMap();
		}

#ifdef COMMS_TODO
		interface->waitSync();
#endif

		if(interface->getMode() == EndDay)
		{
#ifdef DEBUG1
			saveCampaignGame("backup.sav", campaign);
#endif

			campaign->finishOrders();

			delete interface;
			interface = 0;
#ifdef DEBUG
			if(forceWin || forceLose)
			{
				Side winner;

				if(forceWin)
					winner = game->getLocalSide();	// playersSide;
				else
					winner = otherSide(game->getLocalSide());

				doEndGame(winner, Victory);
				finished = True;
				break;
			}
#endif

			movementPhase(False);


			if(!finished)
				finished = EndGameDetection();
		}
		else
			finished = True;
	}

	if(interface)
	{
		retVal = interface->getMode();
		delete interface;
		interface = 0;
	}

	return retVal;
}

class CampaignPtr {
public:
	~CampaignPtr()
	{
		if(campaign)
		{
			delete campaign;
			campaign = 0;
		}
	}
};

int campaignGame()
{
	CampaignPtr cPtr;			// Forces campaign to be deleted on exit

	campaign = new Campaign;
	campaign->init(0);		// startWorldName;

	int retVal = campaign->runCampaign();
	// delete campaign;

	return retVal;
}

/*==============================================================
 * Movement Phase Interface
 */

class MoveInfoArea : public IconSet {
public:
	MoveInfoArea(CampaignMoveInterface* parent) :
		IconSet(parent, Rect(1, MAIN_ICON_Y, 638, MAIN_ICON_H))
	{
	}

	void drawIcon();

	void printf(const char* fmt, ...);
	int makeAlert(const char* text, const char* buttons, const Location* l);
	void clearAlert();
};

class MoveSideArea : public Icon {
public:
	MoveSideArea(CampaignMoveInterface* parent) :
		Icon(parent, Rect(MENU_X, MENU_Y, MENU_WIDTH, MAIN_ICON_Y - MENU_Y))
	{
	}

	void drawIcon();
};

class CampaignMoveClockIcon : public ClockIcon {
public:
	CampaignMoveClockIcon(IconSet* set, Point p, TimeInfo* t, GameTime* gt) : ClockIcon(set, p, t, gt) { }
	void execute(Event* event, MenuData* data) { }
};


void MoveInfoArea::drawIcon()
{
	Region bitmap(machine.screen->getImage(), Rect(getPosition(), getSize()));
	bitmap.fill(0x1a);
	bitmap.frame(0,0,getW(),getH(), 0x1f);

	if(icons)
	{
		bitmap.fill(0x1d);		// 0x1a
		bitmap.frame(0,0,getW(),getH(), 0x1f, 0x50);
		IconSet::drawIcon();
	}
	else
	{
		SpriteIndex sNum;

		// if(game->playersSide == SIDE_USA)
		if(game->getLocalSide() == SIDE_USA)
			sNum = FillUSA_Grey;
		else
			sNum = FillCSA_Grey;

		SpriteBlock* fillPattern = game->sprites->read(sNum);
		bitmap.fill(fillPattern);
		fillPattern->release();
		bitmap.frame(0,0,getW(),getH(), 0x1f, 0x50);
	}

	machine.screen->setUpdate(bitmap);
}

void MoveInfoArea::printf(const char* fmt, ...)
{
	MemPtr<char> buffer(1024);

	va_list vaList;
	va_start(vaList, fmt);
	vsprintf(buffer, fmt, vaList);
	va_end(vaList);

	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
#if 0
	bm.fill(0x1d);
#else
	SpriteIndex sNum;

	// if(game->playersSide == SIDE_USA)
	if(game->getLocalSide() == SIDE_USA)
		sNum = FillUSA_Grey;
	else
		sNum = FillCSA_Grey;

	SpriteBlock* fillPattern = game->sprites->read(sNum);
	bm.fill(fillPattern);
	fillPattern->release();
#endif
	bm.frame(0,0,getW(),getH(), 0x1f, 0x50);

	TextWindow win(&bm, Font_EMFL15);
	win.setFormat(True, True);
	win.setColours(Black);
	win.draw(buffer);

	machine.screen->setUpdate(bm);
}

void MoveSideArea::drawIcon()
{
	Region bitmap(machine.screen->getImage(), Rect(getPosition(), getSize()));

	putLogo(&bitmap);
}

CampaignMoveInterface::CampaignMoveInterface() :
	MenuConnection("CampaignMove")
{
#ifdef DEBUG
	cLog.setTime(&campaign->timeInfo);
#endif

	gotResult = False;

	icons = new IconList[5];
	Icon** iconPtr = icons;

	*iconPtr++ = campaign->mapArea = new MapWindow(this, 0, campaign->mapSettings.style, campaign->mapSettings.centre);
	*iconPtr++ = new MoveSideArea(this);
	*iconPtr++ = messageArea = new MoveInfoArea(this);
	// *iconPtr++ = campaign->message = new MessageWindow(this, Rect(MSGW_X,MSGW_Y,MSGW_W,MSGW_H), Font_MsgWindow);
	*iconPtr++ = campaign->clockIcon = new CampaignMoveClockIcon(this, Point(MENU_X,MAIN_ICON_Y - MAIN_ICON_H), &campaign->timeInfo, &campaign->gameTime);
	*iconPtr++ = 0;

	campaign->alertArea = this;

	campaign->interface = this;
	processMessages(True);

	setPointer(M_Busy);		// New 21st September 1996
}

CampaignMoveInterface::~CampaignMoveInterface()
{
	processMessages(True);
	campaign->mapArea = 0;
	campaign->clockIcon = 0;
	campaign->alertArea = 0;
	campaign->interface = 0;
}

void CampaignMoveInterface::processPacket(Packet* packet)
{
	switch(packet->type)
	{
	case Connect_CS_AlertResult:
		if(packet->sender != RID_Local)
		{
			AlertPacket* pkt = (AlertPacket*) packet->data;
			gotResult = True;
			rxResult = pkt->result;
#ifdef DEBUG
			netLog.printf("AlertResult = %d", (int) rxResult);
#endif
		}
		break;

	default:
#ifdef DEBUG
		netLog.printf("Unknown packet received\rtype: %d, length:%d", (int) packet->type, (int)packet->length);
#endif
		break;
	}
}

void CampaignMoveInterface::drawIcon()
{
	showFullScreen("art\\screens\\playscrn.lbm");
	MenuData::drawIcon();
}

void CampaignMoveInterface::proc()
{
#ifdef COMMS_TODO
	if(game->connection)
	{
		multiPlayer();
		checkFinished();
	}
#endif

	processMessages(False);
}

#ifdef COMMS_TODO
void CampaignMoveInterface::checkFinished()
{
}
#endif

void CampaignMoveInterface::showHintStatus(const char* s)
{
	campaign->msgArea->draw(s);
}

#define ZoomButtonID -1

int CampaignMoveInterface::doAlert(const char* text, const char* buttons, int defaultButton, Facility* f, WaterZone* w, MarkedBattle* mb)
{
	/*
	 * Highlight objects and find location
	 *
	 * Tests are done in this order, so that facility's location
	 * takes priority.
	 */


	const Location* l = 0;

	if(mb)
	{
		l = &mb->where;

		for(BattleUnit* bu = mb->units; bu; bu = bu->next)
			bu->unit->highlight = 1;
	}

	if(w)
	{
		l = &w->location;
		w->highlight = 1;
	}
	
	if(f)
	{
		l = &f->location;
		f->highlight = 1;
	}
	

	int buttonCount = messageArea->makeAlert(text, buttons, l);

	/*
	 * Force it to zoom to correct location
	 */

	if(l)
		campaign->mapArea->setStyle(MapWindow::Zoomed, *l);

	if(buttonCount > 1)
#ifdef COMMS_TODO
		waitSync();
#else
		processMessages(True);
#endif

	Boolean finished = False;

	while(!finished)
	{
		unsigned int timeOut = timer->getCount() + Timer::ticksPerSecond * 60;

		PointerIndex oldPointer = getPointer();
		setPointer(M_Arrow);		// New 21st September 1996

		while(!isFinished())
		{
			process();

			if(timer->getCount() >= timeOut)
				setFinish(defaultButton);
		}

		setPointer(oldPointer);

		clearFinish();

		if(getMode() == ZoomButtonID)
		{
			campaign->mapArea->toggleZoom(*l);
		}
		else
		{
			/*
			 * Send to other side and see what they say
			 */

			if(!game->connection->isLocal() && (buttonCount > 1))
			{
				AlertPacket* packet = new AlertPacket;
				packet->result = getMode();
				game->connection->sendMessage(Connect_CS_AlertResult, (UBYTE*)packet, sizeof(AlertPacket));

#ifdef DEBUG
				netLog.printf("Sent Alert Result %d, mode = %d", (int) packet->result, (int) getMode());
#endif

				ULONG timeOut = timer->getCount() + TimeOutSecs * timer->ticksPerSecond;

				while(!gotResult)
				{
					if(game->connection->isLocal())
					{
						gotResult = True;
						rxResult = getMode();
						finished = True;
						break;
					}

					if(timer->getCount() >= timeOut)
					{
						if(askRemoteNotRespond())
						{
							gotResult = True;
							rxResult = getMode();
							finished = True;
							break;
						}
						else
							timeOut = timer->getCount() + timer->ticksPerSecond * TimeOutSecs;
					}

					processMessages(False);
				}

				if(gotResult)
				{
					gotResult = False;
					if(rxResult == getMode())
						break;
					else
						dialAlert(this, language( lge_RemoteNo ), language( lge_TryAgain));
				}
				else
					break;
			}
			else
				break;
		}
	}

	messageArea->clearAlert();

	if(buttonCount > 1)
#ifdef COMMS_TODO
		waitSync();
#else
		processMessages(True);
#endif

	/*
	 * Unhighlight everything
	 */

	if(f)
		f->highlight = 0;
	
	if(w)
		w->highlight = 0;
	
	if(mb)
	{
		for(BattleUnit* bu = mb->units; bu; bu = bu->next)
			bu->unit->highlight = 0;
	}

	return getMode();
}

/*
 * Do an alert box in the bottom bit of screen
 * synchronised to multi-player game!
 *
 * If a location is given then a 'Zoom' button is added.
 * Clicking on any of the buttons returns that button
 * e.g. if "ok|Cancel" is in the buttons string, then
 * clickingon OK returns 0, Cancel returns 1.
 */

void MoveInfoArea::clearAlert()
{
	// delete[] icons;
	// icons = 0;
	killIcons();
	setRedraw();
}

class AlertKeysIcon : public NullIcon {
public:
	AlertKeysIcon(IconSet* parent) : NullIcon(parent) { }
	void execute(Event* event, MenuData* data);
};

void AlertKeysIcon::execute(Event* event, MenuData* data)
{
	if((event->key == '+') || (event->key == '-'))
		data->setFinish(ZoomButtonID);
}

/*
 * Setup the alert dialogue and return the number of buttons
 */

int MoveInfoArea::makeAlert(const char* text, const char* buttons, const Location* l)
{
	/*
	 * Create Icons for:
	 *   Text area
	 *   Buttons
	 *   Zoom Button
	 */

	/*
	 * Count the buttons
	 */

	FontID buttonFontID = Font_JAME15;
	Font* buttonFont = fontSet(buttonFontID);

	int buttonCount = 0;
	UWORD buttonWidth = 0;
	UWORD x = 0;
	const char* s = buttons;
	if(s)
	{
		buttonCount++;
		while(*s)
		{
			char c = *s++;

			if(c == ButtonSeperator)
			{
				buttonCount++;
				if(x > buttonWidth)
					buttonWidth = x;
				x = 0;
			}
			else
				x += buttonFont->getWidth(c) + 1;
		}
	}
	if(x > buttonWidth)
		buttonWidth = x;

	int iconCount = buttonCount + 3;	// Buttons + Text Area + Keyboard Icon + Null
	if(l)
	{
		iconCount++;

		x = buttonFont->getWidth(language(lge_ZoomIn));
		if(x > buttonWidth)
			buttonWidth = x;
	}

	/*
	 * calculate some button sizes
	 */

	buttonWidth += 8;			// Allow some space
	UWORD buttonX = getW() - buttonWidth - 4;
	UWORD textWidth = buttonX - 4;
	UWORD buttonHeight = buttonFont->getHeight() + 4;
	UWORD buttonSpacing = buttonHeight + 4;
	UWORD buttonY = (getH() - (buttonSpacing * buttonCount));

	/*
	 * Find out which font to use for the text box
	 */

#if 0
	int lineCount = 1;
	s = text;
	while(*s)
	{
		char c = *s++;

		if(c == '\r')
			lineCount++;
	}

	lineCount = (getH() - 4) / lineCount;	// Pixels Per line
#endif

	FontID textFont;

#if 0
	if(lineCount >= 32)
		textFont = Font_JAME32;
	else
#elif 0
	if( (lineCount >= 15) && (fontSet(Font_JAME15)->getWidth(text) < getW()))
		textFont = Font_JAME15;
	else if( (lineCount >= 10) && (fontSet(Font_EMFL10)->getWidth(text) < getW()))
		textFont = Font_EMFL10;
	else if( (lineCount >= 8) && (fontSet(Font_EMFL_8)->getWidth(text) < getW()))
		textFont = Font_EMFL_8;
	else
		textFont = Font_JAME_5;
#else
	textFont = Font_EMFL_8;
#endif

	/*
	 * Make some icons!
	 */

	icons = new IconList[iconCount];

	Icon** iconPtr = icons;

	*iconPtr++ = new TextBox(this, Rect(2, 2, textWidth, getH() - 4), text, textFont,
		0x1d, 0x1f, 0x18, Black);

	Rect r = Rect(buttonX, buttonY, buttonWidth, buttonHeight);

	s = buttons;
	int count = 0;
	while(count < buttonCount)
	{
		const char* wordStart = s;
		while(*s && (*s != ButtonSeperator))
			s++;

		size_t length = s - wordStart;
		char* word = new char[length + 1];
		memcpy(word, wordStart, length);
		word[length] = 0;

		Key key;
		if(count == 0)
			key = KEY_Enter;
		else if(count == 1)
#if defined(COMPUSA_DEMO)
			key = KEY_Space;
#else
			key = KEY_Escape;
#endif
		else
			key = word[0];

		*iconPtr++ = new ButtonIcon(this, r, buttonFontID, count, word, key, True);
		r.y += buttonSpacing;

		s++;
		count++;
	}

	if(l)
	{
		r.y = 4;
		*iconPtr++ = new ButtonIcon(this, r, buttonFontID, ZoomButtonID, copyString(language(lge_Zoom)), 0, True);
	}

	*iconPtr++ = new AlertKeysIcon(this);

	*iconPtr = 0;

	setRedraw();

	return buttonCount;
}

/*
 * Calculate a campaign battle
 */

void Campaign::calculateBattle(MarkedBattle* batl)
{
	BattleResult results;

	results.make(batl, 0, True);

	MemPtr<char> buffer(1000);
	MemPtr<char> buttons(100);

	results.getTitle(buffer);
	strcat(buffer, "\r\r");
	results.getBody(buffer + strlen(buffer));
	strcat(buffer, "\r\r");
	results.getStats(buffer + strlen(buffer));

	results.getButton(buttons);

	doAlert(buffer, buttons, 0, 0, 0, batl);
}

/*
 * Called to do movement Phase Interface
 */


void Campaign::movementPhase(Boolean inBattle)
{
	CampaignMoveInterface* interface;
	
	interface = new CampaignMoveInterface;
	interface->process();		// Get it drawn!
#ifdef COMMS_TODO
	interface->waitSync();
#endif

	if(inBattle)
		inBattle = False;
	else
	{
		/*
		 * It is the end of the day, do movement!
		 */

#ifdef DEBUG
		interface->showStatus("Campaign Movement Phase");
#else
		interface->showStatus("");
#endif

		MemPtr<char> progressStr(1000);

		sprintf(progressStr, "%s %d, %d",
			timeInfo.monthStr,
			timeInfo.day,
			timeInfo.year);
		interface->messageArea->printf("%s", (char*)progressStr);
		interface->process();

#if !defined(DEBUG)		// Done after movement so can see its moves
		for(int i = 0; i < SideCount; i++)
		{
			if(aiControl[i])
			{
				strcat(progressStr, "\r");
				sprintf(progressStr + strlen(progressStr), language(lge_ProcessAI), language(LGE(lge_Union+i)));
				interface->messageArea->printf("%s", (char*)progressStr);
				interface->process();
				aiControl[i]->dailyAI();
			}
		}
#endif

#ifdef TESTING_POPUP
		world->processDay(popup, popupBuffer);
#else
		world->processDay();
#endif

		strcat(progressStr, "\r");
		strcat(progressStr, language(lge_troopMovement));
		interface->messageArea->printf("%s", (char*)progressStr);
		interface->process();
	
		GameTicks mapUpdateTime = 0;

#ifdef DEBUG
		if(dontMove)
		{
			gameTime.advanceTime(SunSet);
			timeInfo = gameTime;
			clockIcon->setRedraw();
		}
		else
#endif
		while( (timeInfo.ticksSinceMidnight < SunSet) &&
			 	(timeInfo.ticksSinceMidnight >= SunRise) )
		{
#ifdef COMMS_TODO
			interface->waitSync();
#else
			interface->processMessages(True);
#endif

#if 0
			GameTicks elapsed = timeToTicks(1,0,0);	// 1 Hour
#else
			GameTicks elapsed = timeToTicks(0,10,0);	// 10 Minutes
#endif

			world->processHour(gameTime, elapsed);

			/*
			 * Update map every hour or so
			 */

#if 0
			mapUpdateTime += elapsed;
			if(mapUpdateTime > timeToTicks(1, 0, 0))
			{
				mapUpdateTime -= timeToTicks(1, 0, 0);
				campaign->updateMap();
			}
#else
			campaign->updateMap();
#endif

			/*
		 	 * Get the clock to update smoothly (as fast as possible)
		 	 */

			if(clockIcon)
			{
#ifdef DEBUG
				if(smoothClock)
				{
#endif
					GameTicks updateTicks = timeToTicks(0,1,0);	// 1 Minute
					while(elapsed)
					{
						if(elapsed > updateTicks)
						{
							elapsed -= updateTicks;
							gameTime.addGameTicks(updateTicks);
						}
						else
						{
							gameTime.addGameTicks(elapsed);
							elapsed = 0;
						}
						timeInfo = gameTime;
						clockIcon->setRedraw();
						interface->process();
					}
#ifdef DEBUG
				}
				else
				{
					gameTime.addGameTicks(elapsed);
					timeInfo = gameTime;
					clockIcon->setRedraw();
				}
#endif
			}
			else
			{
				gameTime.addGameTicks(elapsed);
				timeInfo = gameTime;
			}

			interface->process();
		}

#ifdef COMMS_TODO
		interface->waitSync();
#endif
		if(mapUpdateTime)
			campaign->updateMap();
		interface->process();

		world->UpdateStats();

		newDay = True;
		givingOrders = True;
	}

	/*
	 * Play any battles
	 */

	MarkedBattle* batl;
			
	while( (batl = world->battles.getBattle()) != 0)
	{
		if(batl->playable)
		{
			char buffer[1000];

			TimeInfo t = batl->when;

			Facility* nearFacility = campaign->world->facilities.findClose(batl->where, Mile(50));

			if(nearFacility)
				sprintf(buffer, "%s %d:%02d\r%s, %s\r%s",
					language(lge_battleAt),
					t.hour, t.minute,
					nearFacility->getNameNotNull(),
					campaign->world->states[nearFacility->state].fullName,
					language(lge_between));
			else
				sprintf(buffer, "%s %d:%02d\r%s",
					language(lge_battleAt),
					t.hour, t.minute,
					language(lge_between));


			Unit* u = batl->getSeniorUnit(SIDE_USA);
			if(u)
				sprintf(buffer + strlen(buffer), "\rUSA %s (%ld)",
					u->getName(True),
					batl->getSideTotal(SIDE_USA));
			u = batl->getSeniorUnit(SIDE_CSA);
			if(u)
				sprintf(buffer + strlen(buffer), "\rCSA %s (%ld)",
					u->getName(True),
					batl->getSideTotal(SIDE_CSA));

			TimeBase oldTime = campaign->gameTime;
			campaign->gameTime = batl->when;
			campaign->timeInfo = campaign->gameTime;
			campaign->clockIcon->setRedraw();
			int result = interface->doAlert(buffer, language(lge_playCalculate), 1, 0, 0, batl);
			campaign->gameTime = oldTime;
			campaign->timeInfo = campaign->gameTime;

#if !defined(TESTCAMP)
			if(result == 0)
			{
	#ifdef DEBUG
				memLog->track("Before battle");
	#endif

				if(interface)
				{
#ifdef COMMS_TODO
					interface->waitSync();
#endif
					delete interface;
					interface = 0;
				}

				/*
			 	 * Close down Campaign Info
			 	 */

				OrderBattle* battleOB = beforeBattle(batl);

	#ifdef DEBUG
				memLog->track("After beforeBattle()");
	#endif

				int result = playCampaignBattle(battleOB, batl);

				if(result == MenuData::MenuMainMenu)
				{
					delete batl;
					delete interface;
					interface = 0;
					throw GotoMainMenu();
				}

	#ifdef DEBUG
				memLog->track("After playBattle()");
	#endif
				/*
			 	 * Restore Campaign Information
			 	 */

				afterBattle(battleOB);

				interface = new CampaignMoveInterface;
#ifdef COMMS_TODO
				interface->waitSync();
#endif

	#ifdef DEBUG
				memLog->track("after afterBattle()");
	#endif

			}
			else
#endif	// TESTCAMP
			{
				// Calculate battle
			
				calculateBattle(batl);
			}

			// campaign->world->IncExperience( batl );

			batl->updateExperience(5);
	  	}

	  	delete batl;
	}

	/*
	 * Tidy up the order of battle
	 */

	campaign->world->ob.clearupDead();

#ifdef COMMS_TODO
	interface->waitSync();
#endif

#if defined(DEBUG)		// Done after movement so can see its moves
	for(int i = 0; i < SideCount; i++)
	{
		if(aiControl[i])
		{
			interface->messageArea->printf(language(lge_ProcessAI), language(LGE(lge_Union+i)));
			interface->process();
			aiControl[i]->dailyAI();
		}
	}
#endif


	delete interface;
	interface = 0;
}


/*==============================================================
 * Map Settings Implementation
 */

void Campaign::getMapSettings()
{
	if(mapArea)
	{
		mapSettings.style = mapArea->mapStyle;
		mapSettings.centre = mapArea->getMapCentre();
		if(mapArea->mapMode)
			mapSettings.mode = mapArea->mapMode->mode;
	}
}

MapSettings::MapSettings()
{
	style =  MapWindow::Full;
	centre = Location(MapMaxX/2, MapMaxY/2);
	mode = CampaignMode::CM_City;
}

/*=============================================================
 * Do The End Game sequence
 *
 * This ought to be a nice animation sequence or something.
 *
 * Unfortunately... nothing like this has been done!
 * So it's going to be the same old battle victory screens with
 * some text.
 */

void Campaign::doEndGame(Side winner, CampWinHow how)
{
	/*
	 * Newspaper
	 */

	showFullScreen("art\\screens\\endgame.lbm");

	/*
	 * Add the Date
	 */

	Region region(machine.screen->getImage(), Rect(4, 98, 632, 16));
	TextWindow wind(&region, Font_JAME15);
	wind.setVCentre(True);
	wind.setHCentre(False);
	wind.setWrap(True);
	wind.wprintf("%d %s %d", timeInfo.day, timeInfo.monthStr, timeInfo.year);

	/*
	 * Do a headline
	 */

	region.setClip(12, 124, 616, 352);
	wind.setFont(Font_JAME32);
	wind.setPosition(Point(0,0));
	wind.setVCentre(False);
	wind.setHCentre(True);

	Boolean bigVictory = False;

	static LGE headLines[3][2] = {
		{ lge_uWinEnd_HL, lge_cWinEnd_HL },
		{ lge_uWinLot_HL, lge_cWinLot_HL },
		{ lge_uWin_HL,		lge_cWin_HL		}
	};

	static LGE stories[3][2] = {
		{ lge_uWinEnd_TEXT, lge_cWinEnd_TEXT },
		{ lge_uWinLot_TEXT, lge_cWinLot_TEXT },
		{ lge_uWin_TEXT,	  lge_cWin_TEXT	 }
	};

	UBYTE winIndex = (winner == SIDE_CSA);

	wind.wprintf(language(headLines[how][winIndex]));

	/*
	 * Body
	 */

	wind.newLine();
	wind.newLine();
	wind.setFont(Font_JAME15);
	wind.setVCentre(False);
	wind.setHCentre(False);
	wind.setWrap(True);

	wind.wprintf(language(stories[how][winIndex]));


	machine.mouse->setPointer(M_Arrow);
	machine.screen->update();

	/*
	 * Wait for half a second and clear mouse event queue
	 * to prevent accidently clicking past this screen
	 */

	timer->wait(timer->ticksPerSecond / 2);	// Wait half a second
	machine.mouse->clearEvents();				// Clear any mouse presses

	/*
	 * Wait for button to be pressed
	 */

	MenuConnection connect("EndGame");
	connect.processMessages(True);
	while(!kbhit() && !machine.mouse->getEvent())
	{
		connect.processMessages(False);
	}

	if(kbhit())
	{
		if(getch() == 0)
			getch();
	}
	connect.processMessages(True);

	machine.mouse->setPointer(M_Busy);
}

