/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Data files
 * 
 * The ascii format is generally something like:
 *
 *		Cities 123  ; 123 different cities
 *    {
 *		  City
 *      {
 *        Name "New York"
 *        Side 1
 *        Resources 23 54 65 78
 *      }
 *    }
 *	
 *	Strings are always enclosed in quotes
 *
 * Numbers are stored in decimal ascii.
 *
 * A semicolon means ignore until end of line (except when inside quotes)
 *
 * Braces must always be the 1st non-white space character on a line
 *
 * Spaces and tabs are treated as white space
 *
 * The format is line based, so only one keyword may be on a line.
 * 
 * This means a reader can ignore unknown chunks by simply skipping to
 * the next matched brace.
 *
 * Extra fields can be added to data types, which can be ignored by
 * old readers, or filled in with defaults by new readers
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/09/23  13:28:28  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/07/25  20:32:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/06/29  21:42:34  Steven_Green
 * Initial revision
 *
 * Revision 1.1  1994/06/09  23:32:59  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdlib.h>
#include <string.h>

#include "datafile.h"

/*
 * Keywords defined in a table to make the parser easier
 */

const char* keywords[] = {
	"DWG_WLD",

	/*
	 * Main chunk types
	 */

	"StateSection",
	"FacilitySection",
	"WaterNetwork",

	/*
	 * Facility types
	 */

	"Facility",
	"City",

	/*
	 * Facility fields
	 */

	"State",
	"Flags",			// Redundant to be removed soon
	"Side",
	"USA",
	"CSA",

	"Fortification",
	"Wagons",
	"RailCapacity",
	"RailLink",
	"Name",
	"KeyCity",
	"Type",
	"Size",
	"Resources",

	"SupplyDepot",
	"RecruitmentCentre",
	"TrainingCamp",
	"Hospital",
	"POW",
	"Capital",
	"RailEngineer",

	"WaterCapacity",
	"Blockade",

	"Victory",

	/*
	 * Water Zone types
	 */

	"WaterZone",
	"WaterLink",

	"Sea",
	"Coast",
	"River",
	"Facilities",

	"NavalUnit",
	"Ironclad",
	"Riverine",
	"Monitor",

	"Location",

	/*
	 * Order of Battle Things
	 */

	"OBSection",
	"General",
	"President",
	"Army",
	"Corps",
	"Division",
	"Brigade",
	"Regiment",
	"Unit",

	"Rank",
	"Strength",
	"Stragglers",
	"Casualties",
	"Fatigue",
	"Morale",
	"Speed",
	"Supply",
	"Experience",
	"Time",
	"Order",
	"SentOrder",
	"OrderList",

	"Efficiency",
	"Ability",
	"Aggression",

	"Land",
	"Rail",
	"Water",

	"CautiousAdvance",
	"Advance",
	"Attack",

	"Stand",
	"Hold",
	"Defend",

	"Withdraw",
	"Retreat",
	"HastyRetreat",

	"Occupying",
	"Sieging",

	0
};

KeywordList keys;

int keywordCompare(const void* item1, const void* item2)
{
	return stricmp(((KeywordItem*)item1)->word, ((KeywordItem*)item2)->word);
}

/*
 * Find if keyword exists
 */

KeyWords KeywordList::find(const char* s)
{
	int left = 0;
	int right = howMany - 1;

	while(left <= right)
	{
		int mid = (left + right) / 2;

		KeywordItem* item = &items[mid];

		int cmp = stricmp(s, item->word);

		if(cmp == 0)
			return item->id;
		else if(cmp < 0)
			right = mid - 1;
		else
			left = mid + 1;
	}

	return KW_NotFound;
}

/*
 * Create the sorted list
 */

KeywordList::KeywordList()
{
	items = 0;
	howMany = 0;
}

void KeywordList::create(const char** words, int count)
{
	if(!items)
	{
		items = new KeywordItem[count];
		howMany = count;

		/*
	 	 * Start by simply copying them across in numerical order
	 	 */

		KeywordItem* item = items;

		for(int i = 0; i < howMany; i++)
		{
			item->word = *words++;
			item->id = KeyWords(i);
			item++;
		}

		/*
	 	* Now do a quick sort
	 	*/

		qsort(items, howMany, sizeof(KeywordItem), keywordCompare);
	}

}

/*
 * Remove the list
 */

KeywordList::~KeywordList()
{
	delete items;
}

