/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Editor
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "camped.h"
#include "gameicon.h"
#include "mapicon.h"
#include "layout.h"
#include "campwld.h"
#include "filesel.h"
#include "mapwind.h"
#include "text.h"
#include "colours.h"
#include "system.h"
#include "screen.h"
#include "mouselib.h"
// #include "datafile.h"
#include "wldfile.h"
#include "options.h"
#include "cal.h"
#include "game.h"
#ifdef DEBUG
#include "memlog.h"
#endif

static char* editStr[] = {
	"States",
	"Facilities",
	"Railway Network",
	"Supply Network",
	"Water Network",
	"Terrain Map",
	"Troops",
	0
};

static PointerIndex mapPointers[] = {
	M_WhereTo,
	M_TrackCity,
	M_MoveRail,
	M_Arrow,
	M_MoveWater,
	M_WhereTo,
	M_TrackUnit,
};



#if 0
char* sideStr[] = {
	"Neutral",
	"USA",
	"CSA",
	"Both"
};
#endif

Boolean changed = False;				// Has anything changed?
Boolean stateChanged = False;			// Have states been altered?
Boolean facilityChanged = False;		// Facilities have been changed
Boolean obChanged = False;				// OB has changed
Boolean waterChanged = False;			// Water network has changed
Boolean terrainChanged = False;		// Water network has changed

#if 0
/*
 * Options Icon
 */

class CampEditOptionIcon : public OptionsIcon {
public:
	CampEditOptionIcon(IconSet* set, Point p) : OptionsIcon(set, p) { }

	void doDiskFunctions(MenuData* data);
	void doSystemFunctions(MenuData* data);
};
#endif

/*
 * Mode Change Icon
 */

class EditModeIcon : public Icon {
public:
	EditModeIcon(CampaignEditControl* parent) :
		Icon(parent, Rect(INFO_X, MENU_ICON_Y8, MENU_ICON_W, MENU_ICON_H))
	{
	}

	void execute(Event* event, MenuData* d);
	void drawIcon();
};

/*==========================================================
 * Campaign Global Variables
 */

Campaign* campaign = 0;
CampaignEditControl* control = 0;
GameVariables* game = 0;


/*==========================================================
 * Initialise Campaign
 */

Campaign::Campaign()
{
	world = new CampaignWorld;
	world->init();
	mapArea = 0;
	// updateMapFlag = True;
	showEmptyWaterZones = False;
}

void Campaign::init()
{
	char fullFileName[FILENAME_MAX];

#if 0
	strcpy(fullFileName, "gamedata\\*.dat");
#else
	strcpy(fullFileName, "gamedata\\*.cam");
#endif

	if(fileSelect(fullFileName, "Load World Data file", True) == 0)
	{
#if 0
		readWorld(fullFileName, world);
#else
	  	readStartCampaign(fullFileName, this);
#endif
	}

	mapArea->setStyle(MapWindow::Full, Location(MapMaxX/2, MapMaxY/2));
	gameTime.setCampaignTime();
	gameTime.set(dateToDays(12, April, 1861), SunSet);
	timeInfo = gameTime;
}

Campaign::~Campaign()
{
	delete world;
}

GameVariables::GameVariables()
{
	sprites = new SpriteLibrary("art\\game");
}

GameVariables::~GameVariables()
{
	if(sprites)
		delete sprites;
}


/*==========================================================
 * Display Campaign Game
 */

void Campaign::display()
{
	mapArea->draw();		// Draw Middle
	// campaign->updateMapFlag = False;
}

/*
 * Display routes for resource control
 */

void showSupply(MapWindow* map, FacilityList& facilities)
{
	for(int i = 0; i < facilities.entries(); i++)
	{
		Facility* f = facilities[i];

		if(f->isVisible(map))
		{
			Point p = map->locationToPixel(f->location);

			if(f->facilities & Facility::F_KeyCity)
				map->drawBM->box(p.x - 4, p.y - 4, 9, 9, Yellow);

			if(f->owner != NoFacility)
			{
				Facility* f1 = facilities[f->owner];

				Point p1 = map->locationToPixel(f1->location);
			
				map->drawBM->line(p, p1, (f->facilities & Facility::F_City) ? Yellow : 0x1e);

				map->drawBM->box(p1.x - 3, p1.y - 3, 7, 7, 0x37);
			}
		}
	}
}

void showStateLinks(MapWindow* map, CampaignWorld* world)
{
	for(int i = 0; i < world->facilities.entries(); i++)
	{
		Facility* f = world->facilities[i];
	 
		if(f->isVisible(map) && (f->state != NoState))
		{
			State* st = &world->states[f->state];

			Point p = map->locationToPixel(f->location);
			Point p1 = map->locationToPixel(st->location);

			map->drawBM->line(p, p1, White);
		}
	}
}

/*
 * Function called from mapwindow to display objects
 *
 * It means the mapwindow functions do not need to know
 * about the campaigns object lists
 */

void Campaign::drawObjects(MapWindow* map)
{
	if(map->displayType == MapWindow::Disp_Moveable)
	{
		/*
		 * Place states
		 */

		if(control->mode == EDIT_States)
			showStateLinks(map, world);

		if(world->states.entries())
		{
			StateListIter list = world->states;

			while(++list)
				map->drawObject(&list.current());
		}

		if(control->mode == EDIT_Railway)
			showRailways(map, world->facilities);	// , &world->railways);

		if(control->mode == EDIT_Supply)
			showSupply(map, world->facilities);

		/*
		 * Display boats and things
		 */

		if( (control->mode == EDIT_Water) ||
			 (control->mode == EDIT_Troops) )
			world->waterNet.showWaterZones(map, world);

		/*
		 * Place Facilities
		 */

		if(control->showFacilities)
		{
			if(world->facilities.entries())
			{
				FacilityIter list = world->facilities;

				while(++list)
				{
					Facility* f = list.current();

					// Don't show minor facilities in terrain mode

					if((control->mode != EDIT_Terrain) ||
					   (f->facilities & Facility::F_City))
					{
						map->drawObject(f);
					}
				}
			}
		}
		else if(control->edittingFacility)
			map->drawObject(control->edittingFacility);

		/*
		 * Units
		 */

		if(control->mode == EDIT_Troops)
		{
			Unit* side = world->ob.sides;
			while(side)
			{
				drawUnits(map, side);
				side = side->sister;
			}
		}

#if 0
		if(control->mode == EDIT_Terrain)
			map->showTerrain(map->drawBM);
#endif
		/*
		 * Draw line between edittingFacility and currentObject
		 */

		if(control->showLink && control->edittingFacility && control->currentObject)
		{
			Point p1 = map->locationToPixel(control->edittingFacility->location);
			Point p2 = map->locationToPixel(control->currentObject->location);

			/*
			 * Colour depends on whether there is already a connection
			 */

			Facility* f = (Facility*) control->currentObject;
			FacilityID id = world->facilities.getID(control->edittingFacility);

			Colour c = White;

			for(int i = 0; i < f->connections.entries(); i++)
			{
				if(f->connections[i] == id)
					c = Yellow;
			}


			map->drawBM->line(p1, p2, c);
		}

		/*
		 * Draw line between edittingZone and currentObject
		 */

		if(control->showLink && control->edittingZone && control->currentObject)
		{
			Point p1 = map->locationToPixel(control->edittingZone->location);
			Point p2 = map->locationToPixel(control->currentObject->location);

			/*
			 * Colour depends on whether there is already a connection
			 */

			WaterZoneID zid = world->waterNet.getID(control->edittingZone);
			Colour c = White;

			if(control->currentObject->obType == OT_Facility)
			{
				Facility* f = (Facility*) control->currentObject;

				if(f->waterZone == zid)
					c = Yellow;
			}
			else if(control->currentObject->obType == OT_WaterZone)
			{
				WaterZone* z = (WaterZone*) control->currentObject;

				for(int i = 0; i < z->connections.entries(); i++)
				{
					if(z->connections[i] == zid)
						c = Yellow;
				}
			}

			map->drawBM->line(p1, p2, c);
		}
	}
}


void Campaign::drawUnits(MapWindow* map, Unit* ob)
{
	/*
	 * Draw children only if appropriate to map scale
	 *
	 * This needs adjusting so that highlighted units can display their
	 * subordinates.
	 *
	 * Zoomed: Army/Corps/Division/Brigade
	 *   Full: Army/Corps
	 */

	/*
	 * Go through all objects because independant objects need
	 * to be drawn
	 */

	if(ob->childCount && (ob->getRank() <= Rank_Brigade))
	{
		Unit* u = ob->child;
		while(u)
		{
			drawUnits(map, u);
			u = u->sister;
		}
	}

	/*
	 * Only draw Independant or highlighted units (not presidents)
	 */

	if( (ob->getRank() != Rank_President) &&
	    (ob->isCampaignReallyDetached() || ob->highlight) )
		map->drawObject(ob);
}

/*===============================================================
 * Options Icon to allow campaign specific Options
 */

void saveData()
{
	char fileName[FILENAME_MAX];
#if 0
	strcpy(fileName, "gamedata\\*.dat");
	if(fileSelect(fileName, "Save World Data file", False) == 0)
		writeWorld(fileName, campaign->world);
#else
	strcpy(fileName, "gamedata\\*.cam");
	if(fileSelect(fileName, "Save World Data file", False) == 0)
		writeStartCampaign(fileName, campaign);
#endif
	changed = False;
	stateChanged = False;
	facilityChanged = False;
	obChanged = False;
	waterChanged = False;
}

#if 0
void CampEditOptionIcon::doDiskFunctions(MenuData* data)
{
	switch(dialAlert(data, getPosition() + getSize() / 2,
		"Disk Functions",
		"Save World|Save Terrain|Cancel"))
	{
	case 0:		// Save
		saveData();
		break;
	case 1:		// Save terrain
		control->saveTerrain();
		break;
	case 2:		// Cancel
		break;
	}
}
#endif

class CampSaveIcon : public MenuIcon {
public:
	CampSaveIcon(IconSet* set, Point p, Key k1 = 0, Key k2 = 0) : MenuIcon(set, MM_Save, Rect(p, Point(MAIN_ICON_W,MAIN_ICON_H)), k1, k2) { }
	void execute(Event* event, MenuData* d);
};

void CampSaveIcon::execute(Event* event, MenuData* d)
{
	if(event->buttons)
		saveData();
}




void quitProg(MenuData* data)
{
	Boolean needPrompt = True;
	Boolean wantQuit = False;

	if(changed)
	{
		char buffer[500];

		sprintf(buffer, "Warning\r\rUnsaved Changes have been made to:");
		if(stateChanged)
			strcat(buffer, "\rStates");
		if(facilityChanged)
			strcat(buffer, "\rFacilities");
		if(obChanged)
			strcat(buffer, "\rOrder of Battle");
		if(waterChanged)
			strcat(buffer, "\rWater");

		switch(dialAlert(data,
			buffer,
			"Save|Quit|Cancel"))
		{
		case 0:
			saveData();
			// Drop through
		case 1:
			wantQuit = True;
			break;
		case 2:
			break;
		}
		needPrompt = False;
	}

	if(terrainChanged)
	{
		switch(dialAlert(data,
			"Warning\rTerrain has changed",
			"Save|Quit|Cancel"))
		{
		case 0:
			control->saveTerrain();
		case 1:
			wantQuit = True;
			break;
		case 2:
			wantQuit = False;
			break;
		}
		needPrompt = False;
	}


	if(needPrompt)
	{
		if(dialAlert(data,
			"Do you really want to quit?",
			"Quit to DOS|Cancel") == 0)
		{
			wantQuit = True;
		}
	}

	if(wantQuit)
		data->setFinish(MenuData::MenuQuit);
}


class ToDosIcon : public MenuIcon {
public:
	ToDosIcon(IconSet* set, Point p, Key k1 = 0, Key k2 = 0) : MenuIcon(set, MM_Dos, Rect(p, Point(MAIN_ICON_W,MAIN_ICON_H)), k1, k2) { }
	void execute(Event* event, MenuData* d);
};

void ToDosIcon::execute(Event* event, MenuData* data)
{
	if(event->buttons)
		quitProg(data);
}

#if 0
void CampEditOptionIcon::doSystemFunctions(MenuData* data)
{
	quitProg(data);
}
#endif

void QuitIcon::execute(Event* event, MenuData* data)
{
	if(event->buttons)
		quitProg(data);
}

/*====================================================
 * Mode Icon
 */


void EditModeIcon::execute(Event* event, MenuData* d)
{
	if(event->buttons & Mouse::LeftButton)
	{
		MenuChoice choices[EDITMODE_HowMany + 1];
		MenuChoice* m = choices;
		int i = 0;
		while(i < EDITMODE_HowMany)
		{
			m->text = editStr[i];
			m->value = ++i;
			m++;
		}
		m->text = 0;

		int sel = menuSelect(0, choices, getPosition() + getSize() / 2);

		if(sel > 0)
		{
			control->setMode(EditMode(sel -1));
			setRedraw();
		}
	}
	else if(event->buttons & Mouse::RightButton)
		control->doOptions(getPosition() + getSize() / 2);
}

void EditModeIcon::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	bm.fill(LBlue);
	TextWindow window(&bm, Font_EMMA14);
	window.setFormat(True, True);
	bm.frame(0, 0, getW(), getH(), Blue, LGrey);
	window.draw(editStr[control->mode]);
	machine.screen->setUpdate(bm);
}


/*=====================================================
 * Editor Control structure
 */

CampaignEditControl::CampaignEditControl() :
	MenuData(0),
	Campaign(),
	CampaignMode()
{
	game = new GameVariables;
	campaign = this;
	zoomPointer = M_Arrow;
	zooming = False;
	overFlag = False;
	trackMode = Tracking;
	currentObject = 0;
	objectIsNew = False;

	/*
	 * Set up some icons
	 */

	icons = new IconList[11];
	Icon** iconPtr = icons;

	*iconPtr++ = campaign->mapArea = new MapWindow(this, this);
	*iconPtr++ = new MapViewIcon(this, campaign->mapArea);
	*iconPtr++ = new CampaignScrollLeftIcon(this);
	*iconPtr++ = new CampaignScrollRightIcon(this);
	*iconPtr++ = new CampaignScrollUpIcon(this);
	*iconPtr++ = new CampaignScrollDownIcon(this);
#if 0
	*iconPtr++ = new CampEditOptionIcon (this, Point(MAIN_ICON_X2, MAIN_ICON_Y));
#else
	*iconPtr++ = new CampSaveIcon(this, Point(MAIN_ICON_X3,MAIN_ICON_Y));
	*iconPtr++ = new ToDosIcon(this, Point(MAIN_ICON_X1, MAIN_ICON_Y));
#endif
	*iconPtr++ = new QuitIcon(this);
	*iconPtr++ = new EditModeIcon(this);
	*iconPtr++ = 0;

	control = this;
	init();
	infoArea = new Region(machine.screen->getImage(), Rect(INFO_X,INFO_Y,INFO_WIDTH,INFO_HEIGHT));	// was 315 high
	textWin = new TextWindow(infoArea, Font_SimpleMessage);
	setMode(EDIT_States);

	edittingFacility = 0;
	showFacilities = True;
	edittingZone = 0;
	showLink = False;
	edittingUnit = 0;
	terrain = CT_Sea;
}

CampaignEditControl::~CampaignEditControl()
{
	delete game;
	game = 0;
}

void CampaignEditControl::drawIcon()
{
	showFullScreen("art\\screens\\playscrn.lbm");
	IconSet::drawIcon();
}

/*
 * Change to a new mode
 */

void CampaignEditControl::setMode(EditMode m)
{
	if(m != mode)
	{
		loseObject();

		/*
		 * Undo previous mode
		 */

		switch(mode)
		{
		case EDIT_States:
			break;
		case EDIT_Facilities:
			break;
		case EDIT_Troops:
			break;
		case EDIT_Railway:
			break;
		case EDIT_Water:
			globalOptions.clear(Options::WaterShow);
			break;
		case EDIT_Supply:
			break;
		case EDIT_Terrain:
			globalOptions.clear(Options::Terrain);
			mapArea->makeStaticMap();
			break;
		}

	}

	mode = m;
	overMapPointer = mapPointers[mode];
	trackMode = Tracking;
	edittingFacility = 0;
	edittingZone = 0;
	edittingUnit = 0;
	showFacilities = True;
	showLink = False;
	hint(0);

	/*
	 * Set up for new mode
	 */


	switch(mode)
	{
	case EDIT_States:
		break;
	case EDIT_Facilities:
		break;
	case EDIT_Troops:
		break;
	case EDIT_Railway:
		break;
	case EDIT_Water:
		globalOptions.set(Options::WaterShow);
		break;
	case EDIT_Supply:
		break;
	case EDIT_Terrain:
		globalOptions.set(Options::Terrain);
		mapArea->makeStaticMap();
		showColour();
		break;
	}
}

/*
 * Lose the current tracked object
 */

void CampaignEditControl::loseObject()
{
	if(currentObject)
	{
		currentObject->highlight = 0;
		currentObject = 0;
		objectIsNew = False;

		/*
		 * Remove info
		 */

		clearInfo();
	}
}

void CampaignEditControl::setCurrentObject(MapObject* ob)
{
	loseObject();
	currentObject = ob;
	currentObject->highlight = 1;

	/*
	 * Display Item's Info!
	 */

	currentObject->showInfo(infoArea);
}

/*
 * Draw hint line
 */

void CampaignEditControl::hint(const char* fmt, ...)
{
	Region bm(machine.screen->getImage(), Rect(0,0,516,20));
	bm.fill(0x1d);
	bm.frame(0,0,516,20, 0x1f,0x50);

	if(fmt == 0)
	{
		switch(mode)
		{
		case EDIT_States:
			fmt = "Select State to edit";
			break;
		case EDIT_Facilities:
			fmt = "Select Facility to edit";
			break;
		case EDIT_Troops:
			fmt = "Select Unit to edit";
			break;
		case EDIT_Railway:
			fmt = "Select Facility to connect railway";
			break;
		case EDIT_Water:
			fmt = "Select Waterzone to Edit";
			break;
		case EDIT_Supply:
			fmt = "Select Facility to connect";
			break;
		case EDIT_Terrain:
			fmt = "Click on terrain squares to edit";
			break;
		}
	}

	if(fmt)
	{
		char buffer[500];

		va_list vaList;
		va_start(vaList, fmt);
		vsprintf(buffer, fmt, vaList);
		va_end(vaList);

		TextWindow win(&bm, Font_SimpleMessage);
		win.setFormat(True, True);
		win.setColours(Black);
		win.draw(buffer);
	}
	machine.screen->setUpdate(bm);
}

void CampaignEditControl::setupInfo()
{
	infoArea->fill(0x1d);
	infoArea->frame(*infoArea, 0x1f, 0x50);

	textWin->setFormat(False, False);
	textWin->setColours(Black);
	textWin->setPosition(Point(2,2));
	textWin->setFont(Font_Title);

	machine.screen->setUpdate(*infoArea);
}

void CampaignEditControl::clearInfo()
{
	setupInfo();

	textWin->setFont(Font_Title);
	textWin->setColours(Black);
	textWin->setFormat(True,True);
	textWin->draw("Nothing\rselected");
}

void MapObject::showInfo(Region* r)
{
	TextWindow& textWind = *control->textWin;

	control->setupInfo();

	textWind.setFont(Font_Title);
	textWind.setColours(Black);

	textWind.setFormat(False, True);
	textWind.setPosition(Point(4,4));
	textWind.draw("Unknown Object\r");

	textWind.setFormat(False, False);
	textWind.setLeftMargin(4);

	textWind.wprintf("Object Type %d\r", (int)obType);

	textWind.wprintf("at %ld,%ld\r", location.x, location.y);
}

/*
 * Find if object is suitable for current mode
 */

Boolean CampaignEditControl::isSuitableObject(MapObject* ob, ObjectType whatType)
{
	/*
	 * Only allow suitable owners
	 *	Cities may only link to key cities (ignore this restriction)
	 * Facilities may only link to cities.
	 */


	if((mode == EDIT_Supply) && (trackMode == GetOwner) && (ob->obType == whatType))
	{
#if 0
		if(edittingFacility->facilities & Facility::F_KeyCity)
			return False;
#endif

		Facility* f = (Facility*)ob;
		if(edittingFacility->side == f->side)
		{
			if(edittingFacility->facilities & Facility::F_City)
			{
				if(f->facilities & Facility::F_KeyCity)
					return True;
			}
			else if(f->facilities & Facility::F_City)
			 	return True;
		}
		return False;
	}

	return (ob->obType == whatType);
}

void CampaignEditControl::trackFor(MapWindow* map, Event* event, ObjectType whatType)
{
	/*
	 * Find closest Unit to mouse
	 */

	MapObject* closeUnit = 0;

	MapDisplayObject* best = 0;
	SDimension dist = 0;
	MapPriority bestPriority = Map_NotVisible;

	if(map->onScreenMoveable.entries())
	{
		MapDisplayIter list = map->onScreenMoveable;

		while(++list)
		{
			MapDisplayObject* dob = list.current();
			MapPriorityObject* pob = list.currentPlist();

			if(isSuitableObject(dob->ob, whatType))
			{
				SDimension d = distance(dob->screenPos.x - event->where.x, dob->screenPos.y - event->where.y);

				/*
			 	 * Pick the highest ranking unit within 4 pixels
			 	 */

				Boolean pickIt = False;

				if(!best)					// This is the 1st object searched
					pickIt = True;
				else if((d + 8) < dist)	// It is more than 8 pixels from the previous best
					pickIt = True;
				else if( (pob->priority == bestPriority) &&
					   	(d < dist) )
					pickIt = True;
				else if( (pob->priority < bestPriority) &&
					   	(d < (dist + 8)) )
					pickIt = True;

				/*
			 	 * Store the closest distance even if it wasn't picked
			 	 */

				if(!best || (d < dist))
					dist = d;

				/*
			 	 * If it was picked then updated best values
			 	 */

				if(pickIt)
				{
					best = dob;
					bestPriority = pob->priority;
				}
			}
		}
	}
	if(best)
		closeUnit = (Unit*)best->ob;

	if(closeUnit)
	{
		if(closeUnit != currentObject)
		{
			setCurrentObject(closeUnit);
		}
	}
	else
		loseObject();
}

void CampaignEditControl::overMap(MapWindow* map, Event* event, MenuData* d, const Location* l)
{
	overFlag = True;

	if(zooming)
	{
		if(event->buttons)
		{
			map->setStyle(MapWindow::Zoomed, *l);
			zooming = False;
			setPointer(zoomPointer);
		}
	}
	else
	{
		/*
		 * else do whatever is apropriate for mode
		 */

		if(trackMode == Tracking)
			d->setTempPointer(overMapPointer);
		else
			d->setTempPointer(M_WhereTo);

		/*
		 * Do specific code for mode
		 */

		switch(mode)
		{
		case EDIT_States:
			stateOverMap(map, event, d, l);
			break;

		case EDIT_Facilities:
			facilityOverMap(map, event, d, l);
			break;

		case EDIT_Troops:
			troopOverMap(map, event, d, l);
			break;

		case EDIT_Railway:
			facilityOverMap(map, event, d, l);
			break;

		case EDIT_Water:
			waterOverMap(map, event, d, l);
			break;

		case EDIT_Supply:
			facilityOverMap(map, event, d, l);
			break;

		case EDIT_Terrain:
			terrainOverMap(map, event, d, l);
			break;

		}
	}
}

void CampaignEditControl::offMap()
{
	if(trackMode == Tracking)
	{
		loseObject();
	}

	switch(mode)
	{
	case EDIT_States:
		stateOffMap();
		break;

	case EDIT_Facilities:
		facilityOffMap();
		break;

	case EDIT_Troops:
		troopOffMap();
		break;

	case EDIT_Railway:
		facilityOffMap();
		break;

	case EDIT_Water:
		waterOffMap();
		break;

	case EDIT_Supply:
		facilityOffMap();
		break;

	case EDIT_Terrain:
		terrainOffMap();
		break;

	}
}

void CampaignEditControl::doOptions(const Point& p)
{
	switch(mode)
	{
	case EDIT_States:
		stateOptions(p);
		break;

	case EDIT_Facilities:
		facilityOptions(p);
		break;

	case EDIT_Troops:
		troopOptions(p);
		break;

	case EDIT_Railway:
		railwayOptions(p);
		break;

	case EDIT_Water:
		waterOptions(p);
		break;

	case EDIT_Supply:
		supplyOptions(p);
		break;

	case EDIT_Terrain:
		terrainOptions(p);
		break;

	}
}

void CampaignEditControl::startZoom()
{
	zooming = True;
	zoomPointer = getPointer();
	setPointer(M_Zoom);
}

/*=====================================================================
 * Ask user what he wants to do to the current object
 */

MenuChoice whatChoices[] = {
	MenuChoice(0, 			0 			),						// User fills in this
	MenuChoice("-", 		0 			),
	MenuChoice("New", 	DoNew 	),
	MenuChoice("Delete", DoDelete ),
	MenuChoice("Move", 	DoMove 	),
	MenuChoice("Edit", 	DoEdit 	),
	MenuChoice("-", 		0 			),
	MenuChoice("Cancel", -1 		),
	MenuChoice(0,			0 			)
};

DoWhat getEditWhat(const char* title)
{
	whatChoices[0].text = title;

	int result = menuSelect(0, whatChoices, machine.mouse->getPosition());

	return DoWhat(result);
}

/*=====================================================================
 * Miscellaneous functions
 */


void CampaignEditControl::setCloseState(Facility* f, Boolean keepSide)
{
	StateID best = NoState;

	/*
	 * Set it to the nearest state
	 */

	if(world->states.entries())
	{
		Distance dist = 0;

		StateListIter slist = world->states;
		StateID id = 0;
		while(++slist)
		{
			State* st = &slist.current();

			if(!keepSide || (st->side == f->side))
			{
				Distance d = distance(f->location.x - st->location.x,
											f->location.y - st->location.y);

				if((best == NoState) || (d < dist))
				{
					best = id;
					dist = d;
				}
			}

			id++;
		}
	}

	if(f->state != best)
		facilityChanged = True;

	f->state = best;

	if(!keepSide && (best != NoState))
		f->side = world->states[best].side;
}

void CampaignEditControl::setCloseOwner(Facility* f, Boolean keepSide)
{
	if(f->facilities & Facility::F_KeyCity)
	{
		f->owner = NoFacility;
		return;
	}

	Facility* best = 0;
	Distance closeDist = 0;

	for(int i = 0;i < world->facilities.entries(); i++)
	{
		Facility* f1 = world->facilities[i];

		if((f != f1) && (!keepSide || (f1->side == f->side)))
		{
			Distance d = distance(f->location.x - f1->location.x,
									 	f->location.y - f1->location.y);

			if(!best || (d < closeDist))
			{
				if(f->facilities & Facility::F_City)
				{
					if(f1->facilities & Facility::F_KeyCity)
					{
						best = f1;
						closeDist = d;
					}
				}
				else
				{
					if(f1->facilities & Facility::F_City)
					{
						best = f1;
						closeDist = d;
					}
				}
			}
		}
	}

	if(best)
	{
		f->owner = world->facilities.getID(best);
	}
	else
		f->owner = NoFacility;
}



Side inputSide(Side start)
{
	enum SideChoices {
		SC_None,
		SC_CSA,
		SC_USA,
		SC_Neutral,
		SC_Both,
		SC_Cancel,
		SC_Accept,
	};

	static MenuChoice choices[] = {
		MenuChoice("Which Side?",	0),
		MenuChoice("-",				0),
		MenuChoice("USA",				SC_USA),
		MenuChoice("CSA",				SC_CSA),
		MenuChoice("Neutral",		SC_Neutral),
		MenuChoice("-",				0),
		MenuChoice("Cancel",			SC_Cancel),
		MenuChoice(0,					0)
	};

	switch(menuSelect(0, choices, machine.mouse->getPosition()))
	{
	case SC_CSA:
		return SIDE_CSA;
	case SC_USA:
		return SIDE_USA;
	case SC_Neutral:
		return SIDE_None;
	case SC_Both:
		return SIDE_Both;
	case SC_Cancel:
	default:
		return start;
	}

}





/*=================================================================
 * Editor Main Loop
 */

void editCampaign()
{
	CampaignEditControl menuData;

	while(!menuData.isFinished())
	{
		menuData.overFlag = False;
		menuData.process();
		if(!menuData.overFlag)
			menuData.offMap();
		menuData.display();
	}
}


/*
 * main function
 */


int main(int argc, char *argv[])
{
	try
	{
		/*
		 * Process command line
		 */

#ifdef DEBUG
		MemoryLog::Mode memMode = MemoryLog::None;
		int debugLevel = 0;
#endif

		int i = 0;
		while(++i < argc)
		{
			const char* arg = argv[i];

			if((arg[0] == '-') || (arg[0] == '/'))
			{
				switch(toupper(arg[1]))
				{
#ifdef DEBUG
				case 'M':										// -M for Memory Logging
					if(toupper(arg[2]) == 'S')				// -MS for memory summary
						memMode = MemoryLog::Summary;
					else
						memMode = MemoryLog::Full;
					break;

				case 'D':										// -D for debug log
					if(isdigit(arg[2]))
						debugLevel = atoi(&arg[2]);
					else
						debugLevel = 1;
					break;
#endif
				}
			}
		}



		machine.init();
#ifdef DEBUG
		memLog->setMode("memory.log", memMode);
		if(debugLevel)
			logFile = new LogStream("debug.log", debugLevel);
#endif

		editCampaign();
	}
 	catch(GeneralError e)
 	{
		cout << "Untrapped Error: ";
		if(e.get())
			cout << e.get();
		else
			cout << "No description";
		cout << endl;
 	}
 	catch(...)
 	{
 		cout << "Caught some kind of error" << endl;
 	}

	return 0;
}


