/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Screen dump
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.2  1994/05/25  23:31:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/05/19  17:44:37  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include <direct.h>

#include "scrndump.h"
#include "system.h"
#include "screen.h"
#include "dialogue.h"
#include "ilbm.h"
#include "filesel.h"
#include "memptr.h"
#include "language.h"

#ifdef SCREENDUMP

void doScreenDump(MenuData* proc)
{
	/*
	 * Screen dump code
	 */

	MemPtr<char> fileName(FILENAME_MAX);

	strcpy(fileName, "dumps\\*.lbm");

	if(fileSelect(fileName, language(lge_writeDump), False) == 0)
	{
		mkdir("DUMPS");
		writeILBM(fileName, *machine.screen->getImage(), *machine.screen->getPhysicalPalette(), 0);
	}
}

#endif	// SCREENDUMP
