/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	3D Sprite Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.8  1994/07/11  14:26:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.6  1994/04/11  21:28:35  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/04/11  13:36:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/04/06  12:40:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/03/18  15:07:16  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/12/23  09:26:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/12/21  00:31:04  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "sprite3d.h"
#include "shape.h"
#include "sprite.h"
#include "shplist.h"
#include "view3d.h"

/*
 * A Sprite for putting on screen
 *
 * It has a base class of Shape so that the shapelist can use it.
 * The Sprite part is simply an image with transparent background colour.
 * where is the real screen coordinates
 */

class Sprite2D : public Shape {
	SpriteBlock* shape;
	Point where;
public:
	Sprite2D(SpriteBlock* s) { shape = s; }
	~Sprite2D() { delete shape; }

	void render(Region* bm, ObjectDrawData* d) const;
	void setPosition(Point p) { where = p; }
	void setPosition(SDimension x, SDimension y) { where.x = x; where.y = y; }
};


/*
 * Render a 2D sprite on screen
 */

void Sprite2D::render(Region* bm, ObjectDrawData* d) const
{
	d=d;

	bm->maskBlit(shape, where);
}

/*
 * Convert 3D Sprite into a 2D Sprite for rendering
 */

Point3D Sprite3D::draw(ObjectDrawData* drawData, const Position3D& position)
{
	Position3D p = position;

	/*
	 * Convert to screen coordinates
	 */

	drawData->view->transform(p);

	/*
	 * Find out if it is within the screen window
	 */

	// Off the right?

	if(p.x >= drawData->view->getPixelWidth())
		return Point3D(0,0,-1);

	// Off the bottom

	if(p.y >= drawData->view->getPixelHeight())
		return Point3D(0,0,-1);

	// A long way off left?

	if(p.x < -64)
		return Point3D(0,0,-1);

	// A long way off top

	if(p.y < -64)
		return Point3D(0,0,-1);

	/*
	 * Get the sprite into memory and create a Sprite2D
	 */

	SpriteBlock* shape = library->read(index);
	Sprite2D* s2d = new Sprite2D(shape);
	s2d->setPosition(shape->adjustX(p.x), shape->adjustY(p.y));

	drawData->add(s2d, p.z);

	return p;
}
