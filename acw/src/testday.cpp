/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Test the gametime to timeinfo convertor!
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>

#include "gametime.h"
#include "game.h"

void main()
{
	GameTime gameTime = GameTime(dateToDays(1, January, 1860), timeToTicks(0,0,0));
	TimeInfo ti;
	GameTime endTime = GameTime(dateToDays(1, January, 1862), timeToTicks(0,0,0));

	while(gameTime <= endTime)
	{
		ti = gameTime;

		printf("Day %3d: %2d %2d/%s %4d\n",
			(int) gameTime.days,
			(int) ti.day,
			(int) ti.month,
			ti.monthStr,
			ti.year);

		gameTime.addGameTicks(GameTicksPerDay);
	}
}


GameVariables* game;

