/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Map Object List
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/07/13  13:50:17  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "moblist.h"

/*
 * Constructor
 */

MobIList::MobIList()
{
	first = 0;
	last = 0;
}


void MobIList::add(MobIL* item)
{
	item->next = 0;
	item->prev = last;

	if(last)
		last->next = item;
	else
	{
		last = item;
		first = item;
	}
}

void MobIList::remove(MobIL* item)
{
	MobIL* next = item->next;
	MobIL* prev = item->prev;

	if(next)
		next->prev = prev;
	else
		last = prev;

	if(prev)
		prev->next = next;
	else
		first = next;
}

void MobIList::destroy()
{
	MobIL* ob = first;
	while(ob)
	{
		MobIL* ob1 = ob;
		ob = ob->next;
		delete ob1;
	}
	first = 0;
	last = 0;
}

/*
 * Mob Item constructor
 */

MobIL::MobIL()
{
	next = 0;
	prev = 0;
}

MobIL::MobIL(const Location& l) : MapObject(l, OT_Temporary)
{
	next = 0;
	prev = 0;
}


MapPriority MobIL::isVisible(MapWindow* map) const
{
	return Map_Highlight;
}

/*
 * Iterator functions
 */

MobIListIter::MobIListIter(MobIList* l)
{
	item = l->first;
}

MobIL* MobIListIter::next()
{
	MobIL* retVal = item;

	if(retVal)
		item = item->next;

	return retVal;
}
