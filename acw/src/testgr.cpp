/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 **********************************************************************
 * $Id$
 **********************************************************************
 *
 *	Test Graphics Primitives and Vesa routines
 *
 **********************************************************************
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.11  1994/03/10  14:27:18  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1993/12/14  23:29:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1993/12/13  14:09:20  Steven_Green
 * globalMouse added and test3d.h included
 *
 * Revision 1.8  1993/12/10  16:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1993/11/24  09:32:52  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1993/11/15  17:20:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1993/11/09  14:01:40  Steven_Green
 * Mouse test modification
 *
 * Revision 1.4  1993/11/05  16:50:53  Steven_Green
 * Tests for sprites and mice and polygons
 *
 * Revision 1.3  1993/10/27  21:08:17  Steven_Green
 * Line drawing and clipping tests
 *
 * Revision 1.2  1993/10/26  21:32:42  Steven_Green
 * Horizontal/Vertical Line routines added
 * Some clipping
 * Box and Frame
 *
 * Revision 1.1  1993/10/26  14:54:05  Steven_Green
 * Initial revision
 *
 *
 **********************************************************************
 */

#include <iostream.h>
#include <string.h>

#include "test3d.h"
#include "vesa.h"
#include "image.h"
#include "sprite.h"
#include "mouse.h"
#include "polygon.h"
#include "timer.h"

#ifdef SHOW_RENDER
Mouse* globalMouse = 0;
#endif

#ifdef DEBUG
/*
 * Test fill a screen bank by bank
 *
 * This is a friend of class Vesa
 */

void showVesaBank(Vesa& screen)
{
	int bank = 0;
	VesaBank maxBank = (VesaBank)((screen.yRes * screen.bytesPerScanLine + screen.bankSize - 1) / screen.bankSize); 
	while(bank < maxBank)
	{
		screen.setBank(bank);
		memset(screen.windowPtr, bank+1, screen.bankSize);
		bank++;
	}
}
#endif

void pattern(BitMap& bitmap, Rect& rect, Vesa* screen = NULL)
{
	SDimension x = 0;
	Colour col = 1;

	SDimension end = (rect.getW() > rect.getH()) ? rect.getH() : rect.getW();

	while(x < end)
	{
		bitmap.line(Point(x, 0), Point(0, rect.getH()-x),  col++);
		bitmap.line(Point(0, rect.getH()-x), Point(rect.getW()-x, rect.getH()), col++);
		bitmap.line(Point(rect.getW()-x, rect.getH()), Point(rect.getW(), x), col++);
		bitmap.line(Point(rect.getW(), x), Point(x, 0), col++);
	
		if(screen && ! (x & 15))
			screen->blit(bitmap, Point(640-320-8, 480-320-8));

		x++;
	}
}

/*
 * Main Function
 */

void main(void)
{
#ifdef DEBUG
	cout << "\n\nTest Vesa Graphics Program\n";
	cout << "Copyright (C) 1993, Steven Green and Dagger Interactive Technologies\n\n";
#endif

	/*
	 * Set up a screen with graphics mode 0x101 (640X480)
	 */

	Vesa screen(0x101);

#ifdef DEBUG
	showVesaBank(screen);
#endif

	/*
	 * Make a little Sprite using an image
	 */

	BitMap tinyBM(32,32);

	tinyBM.line(Point(1,0), Point(31,30), 1);
	tinyBM.line(Point(0,1), Point(30,31), 1);
	tinyBM.line(Point(0,15), Point(31,15), 1);
	tinyBM.line(Point(0,17), Point(31,17), 1);
	tinyBM.line(Point(15,0), Point(15,31), 1);
	tinyBM.line(Point(17,0), Point(17,31), 1);
	tinyBM.line(Point(30,0), Point(0,30), 1);
	tinyBM.line(Point(31,1), Point(1,31), 1);

	tinyBM.line(Point(0,0), Point(31,31), 10);		// Draw a cross
	tinyBM.line(Point(31,0), Point(0,31), 11);
	tinyBM.line(Point(0,16), Point(31,16), 12);
	tinyBM.line(Point(16,0), Point(16,31), 13);
	// screen.blit(tinyBM, Point(100, 100));		// Blit it to display it

	/*
	 * Copy the image into a sprite
	 */

	SuperSprite sprite = tinyBM;

	/*
	 * Make it into the mouse
	 */

	Mouse mouse(&screen, &sprite, Point(16,16));
#ifdef SHOW_RENDER
	globalMouse = &mouse;
#endif

#if 0	
	{
		BitMap bitmap(&screen, 20);			// Create a bitmap for the entire screen

		// Fill it with a pattern

		pattern(bitmap, Rect(0,0, 639, 479));

		screen.blit(bitmap, Point(0, 0));

	}
#endif

	/*
	 * Polygon test
	 */

	{
		BitMap bitmap(150, 150, Colour(15));		// White bitmap

		bitmap.frame(Rect(0, 0, 150, 150), Colour(32), Colour(47));

		static Point polyPoints[] = {
			Point(40, 20),
			Point(70, 40),
			Point(20, 80)
		};
		Polygon2D testPoly(3, polyPoints, Colour(96));

		bitmap.setClip(25, 25, 50, 50);
		bitmap.frame(25, 25, 50, 50, Colour(0));

		// bitmap.polygon(testPoly);
		testPoly.render(&bitmap);

		mouse.blit(bitmap, Point(200, 20));

		bitmap.setClip(50, 50, 50, 50);
		bitmap.frame(50, 50, 50, 50, Colour(0));

		unsigned int i = 0;

		while(!mouse.buttons)
		{
			i++;

			polyPoints[0].x = SDimension(25 + (i + 40) % 100);
			polyPoints[0].y = SDimension(25 + (i + 20) % 100);
			polyPoints[1].x = SDimension(25 + ((i + 70) * 2) % 100);
			polyPoints[1].y = SDimension(25 + ((i + 40) * 2) % 100);
			polyPoints[2].x = SDimension(25 + ((i + 20) / 2) % 100);
			polyPoints[2].y = SDimension(25 + ((i + 80) / 2) % 100);
		
			testPoly.setColour(Colour(i));
			
			// bitmap.polygon(testPoly);
			testPoly.render(&bitmap);

			timer->wait();
			mouse.blit(bitmap, Point(200, 200));
		}



	}

#if 0
	/*
	 * Mask Blit the sprite in various places
	 */

	for(int x = 10; x < 400; x++)
		// sprite.move(&screen, Point(x,x));
		mouse.moveTo(Point(x,x));
 


	/*
	 * General Line
	 */

	{
		BitMap bitmap(320,320);

 		/*
		 * Set clipping rectangle
		 */

		bitmap.setClip(20,20,280,280);

		pattern(bitmap, Rect(0, 0, 319, 319), &screen);

		screen.blit(bitmap, Point(640-320-8, 480-320-8));

		for(int i = 0; i < 7; i++)
			for(int j = 0; j < 7; j++)
				screen.blit(bitmap,
					Point(i * 50 + 200, j * 50 + 10),
					Rect(20 + i * 40, 20 + j * 40, 40,40));

	}

	{
		BitMap bitmap(480,320, 0);

		/*
	 	 * Horizontal/Vertical Line drawing
		 */

		int i = 0;
		while(i++ < 300)
		{
			SDimension x = SDimension(i + 10);
			SDimension y = SDimension(i + 5);
			SDimension length = 150;
			Colour color = Colour(i + 20);

			bitmap.HLine(x, y, length, color);

			bitmap.VLine(480 - x, 310 - length, length, color);

			if(! (i & 15))
				screen.blit(bitmap, Point(80, 80));
		}
		screen.blit(bitmap, Point(80, 80));
	}

	/*
	 * Box/Frame drawing
	 */

	{
		BitMap bitmap(480,320, 0);

		int i = 0;
		while(i++ < 250)
		{
			SDimension width = SDimension(400 - i);
			SDimension height = SDimension(10 + i);
			SDimension x = SDimension(i/2 + 40);
			SDimension y = SDimension(160 - height/2);

			bitmap.box(x, y, width, height, Colour(i+60) );
			bitmap.frame(x + 2, y + 2, width - 4, height - 4, Colour(i + 50));
 
			Rect rect(x + 4, y + 4, width - 8, height - 8);
			bitmap.frame(rect, Colour(i + 50), Colour(i + 51));

			if(! (i & 15))
				screen.blit(bitmap, Point(10, 150));
		}

		screen.blit(bitmap, Point(10, 150));
	}

#endif

	while(!mouse.buttons)
		;

}
