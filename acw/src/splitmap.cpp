/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Create tiny maps from hugemap
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/06/21  18:49:27  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/05/21  13:16:19  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/05/04  22:09:38  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include <fstream.h>
#include <stdio.h>
#include "image.h"
#include "ilbm.h"
#include "backup.h"

const int MapSectionWidth	= 480;				// Pixel Size of drawn map section
const int MapSectionHeight =	340;
const int xSections = 5;
const int ySections = 5;


void main()
{
	try
	{
		/*
	 	 * Introduce ourselves
	 	 */

		cout << "TinyMap - Splits hugemap into map sections" << endl;

		/*
	 	 * Create the destination bitmap
	 	 */

		Image srcBM = Image(MapSectionWidth * xSections, MapSectionHeight * ySections);
		Image destBM = Image(MapSectionWidth, MapSectionHeight);
		Palette palette;

		/*
	 	 * Read the hugemap
	 	 */

		char inName[] = "hugemap.lbm";
		cout << "Reading " << inName << flush;
		readILBM(inName, &srcBM, &palette);
		cout << " " << srcBM.getWidth() << " by " << srcBM.getHeight() << endl;


		/*
	 	 * Write out result
	 	 */

		/*
	 	 * Write each section
	 	 */

		for(int x = 0; x < xSections; x++)
			for(int y = 0; y < ySections; y++)
			{
				destBM.blit(&srcBM,
					Point(0,0),
					Rect(x * MapSectionWidth, y * MapSectionHeight, MapSectionWidth, MapSectionHeight));

				char fname[15];
				sprintf(fname, "map%c%c.lbm", y + '1', x + 'A');
				backup(fname);
				writeILBM(fname, destBM, palette);

			}
	}

 	catch(GeneralError e)
 	{
		cout << "Untrapped Error: ";
		if(e.get())
			cout << e.get();
		else
			cout << "No description";
		cout << endl;
 	}
 	catch(...)
 	{
 		cout << "Caught some kind of error" << endl;
 	}
}



