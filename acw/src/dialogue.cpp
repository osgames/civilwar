/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	General Purpose Dialogue boxes
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.6  1994/06/09  23:32:59  Steven_Green
 * Changed Fonts
 *
 * Revision 1.4  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.1  1994/05/04  22:09:38  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "dialogue.h"
#include "menudata.h"
#include "texticon.h"
#include "text.h"
#include "colours.h"
#include "mouselib.h"
#include "system.h"
#include "screen.h"

#ifdef DEBUG
#include "Timer.h"
#endif
// #include "game.h"

#if 0
#define Font_Text		Font_EMFL15
#define Font_Button 	Font_EMFL10
#define Font_Input	Font_EMMA14
#else
#define Font_Text		Font_JAME15
#define Font_Button 	Font_JAME08
#define Font_Input	Font_JAME08	// Font_JAME15
#define Font_DialInput	Font_JAME08
#endif

const char ButtonSeperator = '|';

class AlertDialogue : public MenuData {
	Image under;
	Point where;
public:
	AlertDialogue(MenuData* function, const Point& p, const char* text, const char* buttons, char* input=0, size_t inputSize=0, Boolean number = False);
	~AlertDialogue();
};

TextBox::TextBox(IconSet* parent, const Rect& r, const char* s, FontID f,
		Colour iCol, Colour b1Col, Colour b2Col, Colour tCol) :
	Icon(parent, r)
{
	text = s;
	font = f;

	interior = iCol;
	border1 = b1Col;
	border2 = b2Col;
	textCol = tCol;
	fillPattern = 0;
}


TextBox::TextBox(IconSet* parent, const Rect& r, const char* s, FontID f,
		SystemSprite fill, Colour b1Col, Colour b2Col, Colour tCol) :
	Icon(parent, r)
{
	text = s;
	font = f;

	interior = b1Col;
	border1 = b1Col;
	border2 = b2Col;
	textCol = tCol;
	fillPattern = machine.systemLibrary->read(fill);
}

TextBox::~TextBox()
{
	if(fillPattern)
		fillPattern->release();
}

void TextBox::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	
	if(fillPattern)
		bm.fill(fillPattern);
	else
		bm.fill(interior);
// #ifdef DEBUG
	bm.frame(0,0,getW(),getH(), border1, border2);
// #endif
	TextWindow window(&bm, font);
	window.setFormat(True, True);
	window.setWrap(True);
	window.setColours(textCol);
	window.draw(text);
	machine.screen->setUpdate(bm);
}

OuterBox::OuterBox(IconSet* parent, const Rect& r, SystemSprite fc, Colour e1, Colour e2)
	: IconSet(parent, r)
{
	edge1Col = e1;
	edge2Col = e2;
	fillPattern = machine.systemLibrary->read(fc);
}

OuterBox::~OuterBox()
{
	fillPattern->release();
}


void OuterBox::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));

#if 0
	bm.fill(fillCol);
#else
	bm.fill(fillPattern);
#endif

	bm.frame(0,0,getW(),getH(), edge1Col, edge2Col);
	machine.screen->setUpdate(bm);

	IconSet::drawIcon();
}


ButtonIcon::ButtonIcon(IconSet* set, const Rect& r, FontID f, int v, char* s, Key key, Boolean freeText) :
	Icon(set, r, key)
{
	text = s;
	value = v;
	font = f;
	freeIt = freeText;
	fillPattern = machine.systemLibrary->read(Fill_Button);
}

ButtonIcon::~ButtonIcon()
{
	if(freeIt)
		delete text;
	fillPattern->release();
}

void ButtonIcon::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	// bm.fill(LYellow);
	bm.fill(fillPattern);
	bm.frame(0,0,getW(),getH(), DGrey, LGrey);
	TextWindow window(&bm, font);
	window.setFormat(True, True);
	window.setColours(Black);
	window.draw(text);
	machine.screen->setUpdate(bm);
}

void ButtonIcon::execute(Event* event, MenuData* data)
{
	if(event->buttons)
		data->setFinish(value);
}

/*
 * Do a dialogue box
 * buttons is a string containing text to put in buttons
 * in a format such as:
 *   "OK|CANCEL|UNDO"
 *
 * Returns:
 *   Number of icon clicked: 0=1st, 1=2nd, etc
 */

AlertDialogue::AlertDialogue(MenuData* function, const Point& p, const char* text, const char* buttons, char* input, size_t inputSize, Boolean number) :
	MenuData(function)
{
	FontID textFontID   = Font_Text;
	FontID buttonFontID = Font_Button;
	FontID inputFontID  = Font_Input;
  
	Font* textFont = fontSet(textFontID);
	Font* buttonFont = fontSet(buttonFontID);
	Font* inputFont = fontSet(inputFontID);

	/*
	 * Work out size required for text section
	 */

	UWORD w, h;

	textFont->getSize(text, w, h);
	w += 8;		// Space for a border
	h += 8;

	/*
	 * Break down buttons
	 */

	UWORD buttonWidth = 0;
	UWORD buttonHeight = 1;
	UWORD x = 0;
	UWORD y = 1;
	int buttonCount = 1;
	const char* s = buttons;
	while(*s)
	{
		char c = *s++;

		if(c == ButtonSeperator)
		{
			buttonCount++;
			x = 0;
			y = 1;
		}
		else if(c == '\r')
		{
			x = 0;
			y++;
			if(y > buttonHeight)
				buttonHeight = y;
		}
		else
		{
			x += buttonFont->getWidth(c) + 1;
			if(x > buttonWidth)
				buttonWidth = x;
		}
	}

	buttonWidth += 8;	// Leave some space!

	buttonHeight = buttonHeight * buttonFont->getHeight() + 8;

	UWORD inputWidth = 0;
	UWORD inputHeight = 0;

	if(input)
	{
		inputWidth = UWORD((inputFont->getWidth('W') + 1) * (inputSize + 1) + 4);
		inputHeight = UWORD(inputFont->getHeight() + 8);
	}

	UWORD totalWidth = UWORD((buttonWidth + 2) * buttonCount);
	if(w > totalWidth)
		totalWidth = w;
	if(inputWidth > totalWidth)
		totalWidth = inputWidth;
	totalWidth += 8;

	UWORD totalHeight = UWORD(h + buttonHeight + 4 + 8);
	if(input)
		totalHeight += inputHeight + 4;

	if(totalWidth > getW())
		totalWidth = getW();
	if(totalHeight > getH())
		totalHeight = getH();

	/*
	 * Get data from underneath screen!
	 */

	SDimension rx = p.x;
	SDimension ry = p.y;
	if(rx < 0)
		rx = getW() / 2;
	if(ry < 0)
		ry = getH() / 2;
	rx -= totalWidth / 2;
	ry -= totalHeight / 2;

	if(rx < 0)
		rx = 0;
	if(ry < 0)
		ry = 0;
	if((rx + totalWidth) > getW())
		rx = getW() - totalWidth;
	if((ry + totalHeight) > getH())
		ry = getH() - totalHeight;

	Rect outerRect = Rect(rx, ry, totalWidth, totalHeight);

	where = outerRect;
	under.resize(totalWidth, totalHeight);
	machine.unBlit(&under, where);

	/*
	 * Set up the dialogue box itself
	 *
	 * We need the following icons:
	 *		Outer Box
	 *			Text Box
	 *			Input Box
	 *			Icon boxes[]
	 */

	icons = new IconList[2];		// Outer Box

	// IconSet* outer = new OuterBox(this, outerRect, White, DGrey, LGrey);
	IconSet* outer = new OuterBox(this, outerRect, Fill_Paper, DGrey, LGrey);

	icons[0] = outer;
	icons[1] = 0;

	int iconCount = buttonCount + 2;
	if(input)
		iconCount++;
	IconList* innerIcons = new IconList[iconCount];
	Icon** iconPtr = innerIcons;

	// *iconPtr++ = new TextBox(outer, Rect((totalWidth - w) / 2, 4, w, h), text, textFontID, White, DGrey, LGrey, Black);
	*iconPtr++ = new TextBox(outer, Rect((totalWidth - w) / 2, 4, w, h), text, textFontID, Fill_PaperWhite, DGrey, LGrey, Black);
	if(input)
	{
		if(number)
			*iconPtr++ = new NumberInputIcon(outer,
						Rect((totalWidth - inputWidth)/2, h + 4 + 4, inputWidth, inputHeight),
						input,
						inputSize,
						inputFontID);
		else
			*iconPtr++ = new InputIcon(outer,
						Rect((totalWidth - inputWidth)/2, h + 4 + 4, inputWidth, inputHeight),
						input,
						inputSize,
						inputFontID);
	}

	/*
	 * Put the buttons on!
	 */

	SDimension spacing = SDimension((totalWidth - buttonWidth * buttonCount) / (buttonCount + 1));
	SDimension xSpace = SDimension(spacing + buttonWidth);

	Rect r = Rect(spacing, totalHeight - buttonHeight - 4, buttonWidth, buttonHeight);

	int count = 0;
	s = buttons;

	while(count < buttonCount)
	{
		const char* wordStart = s;
		while(*s && (*s != ButtonSeperator))
			s++;

		size_t length = s - wordStart;
		char* word = new char[length + 1];
		memcpy(word, wordStart, length);
		word[length] = 0;

		Key key;
		if(count == 0)
			key = KEY_Enter;
		else if(count == 1)
#if defined(COMPUSA_DEMO)
			key = KEY_Space;
#else
			key = KEY_Escape;
#endif
		else
			key = word[0];

		*iconPtr++ = new ButtonIcon(outer, r, buttonFontID, count, word, key, True);
		r.x += xSpace;

		s++;
		count++;
	}
	
	/*
	 * Terminate the iconlist
	 */

	*iconPtr++ = 0;

	/*
	 * Attach icon list to the outer box.
	 */

	outer->setIcons(innerIcons);
}

AlertDialogue::~AlertDialogue()
{
	machine.blit(&under, where);
	machine.screen->update();
}


int dialAlert(MenuData* function, const Point& p, const char* text, const char* buttons )
{
	if(machine.initialised)
	{
#ifdef DEBUG
		int time = timer->getCount() + Timer::ticksPerSecond * 60;
#endif

		AlertDialogue dial(function, p, text, buttons);

		machine.screen->updatePalette(20);		// Get colours back if faded!

		dial.redrawIcon();
		while(!dial.isFinished())
		{
			// tempTime->waitRel(1);

			dial.process();

			#ifdef DEBUG
			if ( timer->getCount() > time && timer->getCount() - time < 32000 ) return 1;
			#endif
		}

		return dial.getMode();
	}
	else
	{
		throw GeneralError("%s", text);
	}
}

int dialAlert(MenuData* function, const char* text, const char* buttons)
{
	if(machine.initialised)
	{
		AlertDialogue dial(function, Point(-1,-1), text, buttons);

#ifdef DEBUG
		int time = timer->getCount() + Timer::ticksPerSecond * 60;
#endif

		machine.screen->updatePalette(20);		// Get colours back if faded!

		dial.redrawIcon();
		while(!dial.isFinished())
		{
			dial.process();
			#ifdef DEBUG
			if ( timer->getCount() > time && timer->getCount() - time < 32000) return 1;
			#endif
		}

		return dial.getMode();
	}
	else
	{
		throw GeneralError("%s", text);
		// puts(text);
		return 0;
	}
}


/*
 * Dialogue box with a text input field
 */

int dialInput(MenuData* function, const Point& p, const char* prompt, const char* buttons, char* buffer, size_t buflen, Boolean number)
{
	AlertDialogue dial(function, p, prompt, buttons, buffer, buflen, number);

	dial.redrawIcon();
	while(!dial.isFinished())
		dial.process();

	return dial.getMode();
}

int dialInput(MenuData* function, const char* prompt, const char* buttons, char* buffer, size_t buflen, Boolean number)
{
	AlertDialogue dial(function, Point(-1,-1), prompt, buttons, buffer, buflen, number);

	dial.redrawIcon();
	while(!dial.isFinished())
		dial.process();

	return dial.getMode();
}

/*
 * Popup text display boxes
 */

PopupText::PopupText(const char* s)
{
	textUp = False;
	fillPattern = machine.systemLibrary->read(Fill_Paper);
	if(s)
		showText(s);
}

PopupText::~PopupText()
{
	fillPattern->release();
	if(textUp)
	{
		clear();
		machine.screen->update();
	}
}

void PopupText::showText(const char* text)
{
#if 0
	if(textUp)
		clear();
#endif

	FontID textFontID = Font_Text;
	Font* textFont = fontSet(textFontID);

	/*
	 * Work out size required for text section
	 */

	UWORD w, h;

	textFont->getSize(text, w, h);
	w += 8;		// Space for a border
	h += 8;

	UWORD totalWidth = UWORD(w + 8);
	UWORD totalHeight = UWORD(h + 8);

	/*
	 * Get data from underneath screen!
	 *
	 * NAUGHTY: Absolute screen size!
	 */

	SDimension screenW = machine.screen->getW();
	SDimension screenH = machine.screen->getH();

	Rect outerRect = Rect((screenW - totalWidth) / 2, (screenH - totalHeight) / 2,
						totalWidth, totalHeight);

	if(textUp)
	{
		if( (totalWidth <= under.getWidth()) && (totalHeight <= under.getHeight()) )
		{
			totalWidth = under.getWidth();
			totalHeight = under.getHeight();

			outerRect = Rect(where.getX(), where.getY(), totalWidth, totalHeight);
		}
		else
			clear();		// NB This clears textUp
	}

	if(!textUp)
	{
		where = outerRect;
		under.resize(totalWidth, totalHeight);
		machine.unBlit(&under, where);
	}

	Region bm(machine.screen->getImage(), Rect(where, Point(totalWidth, totalHeight)));
	// bm.fill(White);
	bm.fill(fillPattern);
	bm.frame(0,0,totalWidth,totalHeight, DGrey, LGrey);

	TextWindow window(&bm, textFontID);
	window.setFormat(True, True);
	window.setColours(Black);
	window.draw(text);

	machine.screen->setUpdate(bm);
	machine.screen->update();

	textUp = True;
}

void PopupText::clear()
{
	if(textUp)
	{
		Region bm(machine.screen->getImage(), Rect(where, under.getSize()));
		machine.blit(&under, where);
		under.resize(0, 0);
		textUp = False;
		machine.screen->setUpdate(bm);
		// machine.screen->update();
	}
}

/*
 * Menu Selection
 */

class MenuAlert : public MenuData {
	Image under;
	Point where;
public:
	MenuAlert(MenuData* function, MenuChoice* choices, const Point& p);
	~MenuAlert();
};

class MenuWindow : public Icon {
	MenuChoice* items;
 	int currentItem;
	SDimension lineHeight;
	SpriteBlock* fillPattern;
public:
	MenuWindow(IconSet* parent, const Rect& r, MenuChoice* choices);
	~MenuWindow();

	void drawIcon();
	void execute(Event* event, MenuData* data);
};

int menuSelect(MenuData* function, MenuChoice* choices)
{
	MenuAlert dial(function, choices, Point(-1,-1));

	dial.redrawIcon();
	while(!dial.isFinished())
		dial.process();

	return dial.getMode();
}

int menuSelect(MenuData* function, MenuChoice* choices, const Point& p)
{
	MenuAlert dial(function, choices, p);

	dial.redrawIcon();
	while(!dial.isFinished())
		dial.process();

	return dial.getMode();
}

int chooseFromList(MenuData* func, const char* title, const char** choices, const char* cancel, const Point& where)
{
	/*
	 * Count the number of choices
	 */

	int howMany = 1;
	const char** sPtr = choices;
	while(*sPtr++)
		howMany++;

	if(title)
		howMany += 2;
	if(cancel)
		howMany += 2;

	MenuChoice* menu = new MenuChoice[howMany];
	MenuChoice* ch = menu;

	if(title)
	{
		*ch++ = MenuChoice(title, 0);
		*ch++ = MenuChoice("-", 0);
	}

	sPtr = choices;
	int i = 1;
	while(*sPtr)
		*ch++ = MenuChoice(*sPtr++, i++);

	if(cancel)
	{
		*ch++ = MenuChoice("-", 0);
		*ch++ = MenuChoice(cancel, -1);
	}
	*ch++ = MenuChoice(0, 0);

	int choice = menuSelect(func, menu, where);

	delete[] menu;

	return choice;
}

int chooseFromList(MenuData* func, const char* title, const char** choices, const char* cancel)
{
	return chooseFromList(func, title, choices, cancel, machine.mouse->getPosition());
}

MenuAlert::MenuAlert(MenuData* function, MenuChoice* choices, const Point& p) :
	MenuData(function)
{
	Font* font = fontSet(Font_Text);
	
	/*
	 * Find width of longest item and number of items
	 */

	MenuChoice* choice = choices;
	UWORD length = 0;
	int count = 0;

	while(choice->text)
	{
		UWORD l = font->getWidth(choice->text);
		if(l > length)
			length = l;

		count++;
		choice++;
	}

	SDimension totalWidth = length + 10;
	SDimension height = count * font->getHeight();
	SDimension totalHeight = height + 10;


	if(totalWidth > getW())
		totalWidth = getW();
	if(totalHeight > getH())
		totalHeight = getH();

	SDimension midX = p.x;
	SDimension midY = p.y;

	if(midX < 0)
		midX = getW() / 2;
	if(midY < 0)
		midY = getH() / 2;

	midX -= totalWidth/2;
	midY -= totalHeight/2;

	if( (midX + totalWidth) > getW())
		midX = getW() - totalWidth;
	if( (midY + totalHeight) > getH())
		midY = getH() - totalHeight;
	if(midX < 0)
		midX = 0;
	if(midY < 0)
		midY = 0;

	Rect outerRect = Rect(midX,
								 midY,
							    totalWidth, totalHeight);

	icons = new IconList[2];
	// IconSet* outer = new OuterBox(this, outerRect, Red, Black, LRed);
	IconSet* outer = new OuterBox(this, outerRect, Fill_Alert, Black, LRed);
	icons[0] = outer;
	icons[1] = 0;

	IconList* innerIcons = new IconList[2];
	innerIcons[0] = new MenuWindow(outer, Rect(3,3,length+4,height+4), choices);
	innerIcons[1] = 0;
	outer->setIcons(innerIcons);

	where = outerRect;
	under.resize(totalWidth, totalHeight);
	machine.unBlit(&under, where);
}

MenuAlert::~MenuAlert()
{
	machine.blit(&under, where);
	machine.screen->update();
}

MenuWindow::MenuWindow(IconSet* parent, const Rect& r, MenuChoice* choices) :
	Icon(parent, r)
{
	items = choices;
	currentItem = -1;		// No item selected
	lineHeight = fontSet(Font_Text)->getHeight();
	fillPattern = machine.systemLibrary->read(Fill_Paper);
}

MenuWindow::~MenuWindow()
{
	fillPattern->release();
}

void MenuWindow::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	// bm.fill(White);
	bm.fill(fillPattern);

	/*
	 * Draw text
	 */

	MenuChoice* choice = items;
	SDimension y = 2;
	int count = 0;

	TextWindow wind(&bm, Font_Text);

	while(choice->text)
	{
		if(choice->text[0] == '-')		// Special case for dividing line
			bm.HLine(0,y+lineHeight/2, getW(), Black);
		else
		{
			if(count == currentItem)
			{
				bm.box(0,y, getW(), lineHeight, Red);
				wind.setColours(White);
			}
			else
			{
				if(!choice->value)
					bm.box(0,y, getW(), lineHeight, White);
					// bm.box(0,y, getW(), lineHeight, Yellow);
				wind.setColours(Black);
			}

			wind.setPosition(Point(2, y));
			wind.draw(choice->text);
		}

		y += lineHeight;
		choice++;
		count++;
	}

	machine.screen->setUpdate(bm);
}

void MenuWindow::execute(Event* event, MenuData* data)
{
	/*
	 * Find closest Item to mouse
	 */

	int newItem = -1;

	if(event->overIcon)
	{
		newItem = event->where.getY() / lineHeight;

		if(items[newItem].value == 0)
			newItem = -1;
	}

	if(currentItem != newItem)
	{
		currentItem = newItem;
		setRedraw();
	}

	if(event->buttons && (currentItem != -1))
		data->setFinish(items[currentItem].value);
}

/*
 * Enhanced Popup dialogues
 */

class DialWork : public MenuData {
	Dialogue* dial;
	DialItem* items;

	Image under;
	Point where;
public:
	DialWork(Dialogue* d, DialItem* i);
	~DialWork();

	void proc(DialItem* item, Event* event);
};

/*
 * Button type icon used in popup
 */

class DialButton : public Icon {
	DialItem* item;
	DialWork* work;
public:
	DialButton(IconSet* parent, DialWork* w, const Rect& r, DialItem* i);

	void execute(Event* event, MenuData* data);
	void drawIcon();
};

DialButton::DialButton(IconSet* parent, DialWork* w, const Rect& r, DialItem* i) :
	Icon(parent, r)
{
	item = i;
	work = w;
}

void DialButton::execute(Event* event, MenuData* data)
{
	if(item->enabled)
		work->proc(item, event);
}

#define ButtonFillColour 0x36
#define ButtonHighlightColour 0x46
#define ButtonBorderColour1 0x30
#define ButtonBorderColour2 0x36
#define OuterFillColour 0x34

void DialButton::drawIcon()
{
#if 0	
	if(item->enabled)
	{
#endif
		Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));

		if(item->highlighted)
			bm.fill(ButtonHighlightColour);
		else
		{
			SpriteBlock* fillPattern = machine.systemLibrary->read(Fill_FileSel);
			bm.fill(fillPattern);
			fillPattern->release();
		}
		if(!item->enabled)
			bm.greyOut(Rect(0,0,getW(),getH()), Grey8);


		bm.frame(0,0,getW(),getH(), ButtonBorderColour1, ButtonBorderColour2);

		TextWindow window(&bm, Font_Button);
		window.setFormat(True, True);
		window.setShadow(Black);
		// window.setColours(item->highlighted ? White : Black);
		window.setColours(item->highlighted ? White : White);
		window.draw(item->text);


		machine.screen->setUpdate(bm);
#if 0
	}
#endif
}


class DialString : public Icon {
	DialItem* item;
public:
	DialString(IconSet* parent, const Rect& r, DialItem* i);

	void execute(Event* event, MenuData* data) { }
	void drawIcon();
};

DialString::DialString(IconSet* parent, const Rect& r, DialItem* i) :
	Icon(parent, r)
{
	item = i;
}

void DialString::drawIcon()
{

	if(item->enabled)
	{
		Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));

		if(item->highlighted)
			bm.fill(ButtonHighlightColour);

// #ifdef DEBUG
		bm.frame(0,0,getW(),getH(), ButtonBorderColour1, ButtonBorderColour2);
// #endif

		TextWindow window(&bm, Font_Button);
		window.setFormat(True, True);
		window.setColours(item->highlighted ? White : Black);
		window.draw(item->text);
		machine.screen->setUpdate(bm);
	}
}

class DialInput : public InputIcon {
	DialItem* item;
	DialWork* work;
public:
	DialInput(IconSet* set, DialWork* w, const Rect& r, DialItem* i, FontID f) :
		InputIcon(set, r, i->text, i->bufferSize, f)
	{
		item = i;
		work = w;
		// number = num;
	}

	void execute(Event* event, MenuData* d);

	void drawIcon();
};

class DialNumberInput : public NumberInputIcon {
	DialItem* item;
	DialWork* work;
public:
	DialNumberInput(IconSet* set, DialWork* w, const Rect& r, DialItem* i, FontID f) :
		NumberInputIcon(set, r, i->text, i->bufferSize, f)
	{
		item = i;
		work = w;
		// number = num;
	}

	void execute(Event* event, MenuData* d);

	void drawIcon();
};


void DialInput::execute(Event* event, MenuData* d)
{
	if(item->enabled)
	{
		if(event->buttons || active)
			work->proc(item, event);
		InputIcon::execute(event, d);
		if(active)
			work->proc(item, event);
	}
}

void DialInput::drawIcon()
{
	if(item->enabled)
	{
		InputIcon::drawIcon();
	}
}

void DialNumberInput::execute(Event* event, MenuData* d)
{
	if(item->enabled)
	{
		if(event->buttons || active)
			work->proc(item, event);
		NumberInputIcon::execute(event, d);
		if(active)
			work->proc(item, event);
	}
}

void DialNumberInput::drawIcon()
{
	if(item->enabled)
	{
		NumberInputIcon::drawIcon();
	}
}



DialWork::DialWork(Dialogue* d, DialItem* i)
{
	dial = d;
	items = i;

	/*
	 * Find out size of rectangle required, and centre it up.
	 */

	SDimension maxX = 0;
	SDimension maxY = 0;
	int iconCount = 0;

	DialItem* item = items;
	while(item->type != Dial_End)
	{
		SDimension x = item->r.x + item->r.getW();
		SDimension y = item->r.y + item->r.getH();

		if(x > maxX)
			maxX = x;

		if(y > maxY)
			maxY = y;

		item++;
		iconCount++;
	}

	/*
	 * Adjust for a 2 pixel border all round
	 */

	maxX += 4;
	maxY += 4;

	/*
	 * Convert to a screen rectangle
	 */

	Rect outerRect = Rect( (getW() - maxX) / 2, (getH() - maxY) / 2, maxX, maxY);

	/*
	 * Copy data from underneath
	 */

	where = outerRect;
	under.resize(maxX, maxY);
	machine.unBlit(&under, where);

	/*
	 * Create an outer box
	 */

	icons = new IconList[2];

	// IconSet* outer = new OuterBox(this, outerRect, OuterFillColour, ButtonBorderColour1, ButtonBorderColour2);
	IconSet* outer = new OuterBox(this, outerRect, Fill_Paper, ButtonBorderColour1, ButtonBorderColour2);

	icons[0] = outer;
	icons[1] = 0;

	/*
	 * Create apropriate icons and attach to outer box
	 */

	IconList* innerIcons = new IconList[iconCount + 1];
	Icon** iconPtr = innerIcons;

	item = items;
	while(item->type != Dial_End)
	{
		switch(item->type)
		{
			case Dial_FreeString:
				*iconPtr++ = new DialString(outer, item->r + Point(2,2), item);
				break;
			case Dial_Button:
				*iconPtr++ = new DialButton(outer, this, item->r + Point(2,2), item);
				break;
			case Dial_TextInput:
				*iconPtr++ = new DialInput(outer, this, item->r + Point(2,2), item, Font_DialInput);
				break;
			case Dial_NumberInput:
				*iconPtr++ = new DialNumberInput(outer, this, item->r + Point(2,2), item, Font_DialInput);
				break;
			case Dial_Box:
				break;
		}


		item++;
	}

	*iconPtr = 0;

	outer->setIcons(innerIcons);

}

DialWork::~DialWork()
{
	machine.blit(&under, where);
	machine.screen->update();
}


void DialWork::proc(DialItem* item, Event* event)
{
	dial->doIcon(item, event, this);
}


void Dialogue::setup(DialItem* items)
{
	work = new DialWork(this, items);
}

int Dialogue::process()
{
	while(!work->isFinished())
		work->process();

	return work->getMode();
}

Dialogue::~Dialogue()
{
	delete work;
}
