/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Icons on the battlefield
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "baticon.h"
#include "system.h"
#include "screen.h"
#include "sprlib.h"
#include "batldata.h"


/*
 * Constructor
 */

BattleIcon::BattleIcon(IconSet* parent, BattleSprite n, Rect r, Key k1, Key k2) :
		Icon(parent, r, k1, k2),
		iconNumber(n)
{
	// Do nothing
}

void BattleIcon::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	battle->spriteLib->drawCentredSprite(&bm, iconNumber);
	machine.screen->setUpdate(bm);
}
