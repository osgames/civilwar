/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Trigonometry Tester
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1993/11/15  17:20:06  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


#include <iostream.h>
#include "trig.h"


static void testCos(Wangle a)
{
	cout << "cos(" << int(Degree(a)) << ") = " << mcos(10000, a) << "\n";
	cout.flush();
}

static void testSin(Wangle a)
{
	cout << "sin(" << int(Degree(a)) << ") = " << msin(10000, a) << "\n";
	cout.flush();
}

static void testArctan(Wangle a)
{
	/*
	 * Make 2 points
	 */

	int x = mcos(10000, a);
	int y = msin(10000, a);

	Wangle a1(x, y);

	cout << "arctan(" << x << "," << y << ") = " << int(Degree(a1))
			<< " original was: " << int(Degree(a)) << "\n";
	cout.flush();
}

void main()
{
	cout << "Trigonometry Test\n";
	cout.flush();

	int i = 0;
	while(i < 360)
 	{
		testCos(Wangle(Degree(i)));
		testSin(Wangle(Degree(i)));
		testArctan(Wangle(Degree(i)));

		i += 30;
	}
}



