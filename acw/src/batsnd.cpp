/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battle Sound Effects Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "batsnd.h"
#include "batldata.h"


/*
 * List of sound effects used in battle
 */

const char* battleSoundEffectNames[] = {
	"cannon",		// Use for cannon
	"shotgun",	 	// Use for infantry fire!
	"melee",			// Used for men in Melee
	0
};

void BattleSound::playSound(BattleSoundID id, const Location& l, SoundPriority priority)
{
	/*
	 * Convert location into distance from view centre and left/right pan
	 */

	ViewPoint* view = &battle->data3D.view;
	Cord3D viewWidth = view->howBig() / 2;

	Cord3D x = DistToBattle(l.x) - view->xCentre;
	Cord3D y = DistToBattle(l.y) - view->zCentre;
	rotate(x, y, -view->yRotate);

	Cord3D d = distance(x, y);

	/*
	 * d is distance from centre of viewpoint
	 * x is position left/right
	 */

	unsigned int ax = abs(x);
	int pan = (ax * MaxPan) / viewWidth;
	if(pan > MaxPan)
		pan = MaxPan;
	if(x < 0)
		pan = -pan;

	int volume;

	if(d < viewWidth)
		volume = MaxVolume;
	else
	{
		// d -= viewWidth;

		// This really ought to be inverse square
		// v = K / (d^2)

		// volume = MaxVolume - (d * MaxVolume) / BattleMile(4);

		if(d <= 0)
			d = 1;

		volume = (MaxVolume * viewWidth) / d;
	}

	/*
	 * Adjust volume for zoom level
	 */

	static int zoomAdjust[ZOOM_LEVELS] = {
		100,		// Half
		90,		// 1 Mile
		80,		// 2 Mile
		70,		// 4 Mile
		60			// 8 Mile
	};

	volume = (volume * zoomAdjust[view->zoomLevel]) / 100;

#ifdef SOUND_DEBUG
	soundLog.printf("x=%ld, y=%ld, d=%ld", x, y, d);
	soundLog.printf("Playing %d, volume %d, pan %d", (int) id, volume, pan);
#endif

	if(volume > 0)
		SoundLibrary::playSound(id, volume, pan, priority);
}
