/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Test Program for LZHuf compression and decompression
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/07/19  19:53:03  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>

#include "lzhuf.h"

/*
 * Entry point for file to file versions
 */

void decode_lzhuf_f(const char* in, const char* out)
{
	long inLength;
	long outLength;
	UBYTE* inBuffer;
	UBYTE* outBuffer;
	FILE* inFile;
	FILE* outFile;

	/*
	 * Read the input file first of all
	 */

	inFile = fopen(in, "rb");
	if(!inFile)
		return;

	/*
	 * Find out how long the file is
	 */

	fseek(inFile, 0L, SEEK_END);
	inLength = ftell(inFile);
	rewind(inFile);
	if(inLength == 0)
		return;

	/*
	 * Read the length from the 1st 4 bytes
	 */

	if(fread(&outLength, sizeof(outLength), 1, inFile) != 1)
		return;
	inLength -= sizeof(outLength);
	if(outLength == 0)
		return;

	/*
	 * Allocate some buffers
	 *
	 * This can be updated later to handle long files by reading
	 * in a bit at a time.
	 */

	inBuffer = malloc(inLength);
	if(!inBuffer)
		return;

	if(fread(inBuffer, inLength, 1, inFile) != 1)
		return;
	fclose(inFile);

	/*
	 * Decompress it
	 */

	outBuffer = malloc(outLength);
	
	decode_lzhuf(inBuffer, outBuffer, inLength, outLength);

	/*
	 * Write the output file
	 */

	outFile = fopen(out, "wb");
	if(outFile)
	{
		fwrite(outBuffer, outLength, 1, outFile);
		fclose(outFile);
	}

	free(inBuffer);
	free(outBuffer);
}

/*
 * Stuff to make a test program
 */

void Error (const char* message)
{
        printf("\n%s\n", message);
        exit(EXIT_FAILURE);
}

int main (int argc, char *argv[])
{
        char  *s;
        int i;

		  const char* inName = 0;
		  const char* outName = 0;

		  FILE* inFile;
		  FILE* outFile;
		  long textSize;

        if (argc != 4) {
                printf ("\
usage: lzhuf e in_file out_file (packing)\n\
       lzhuf d in_file out_file (unpacking)\n");
                return EXIT_FAILURE;
        }
        if ((s = argv[1], ((*s != 'e') && (*s != 'd')) || s[1] != '\0') ||
            (inName = s = argv[2], (inFile  = fopen(s, "rb")) == NULL) ||
            (outName = s = argv[3], (outFile = fopen(s, "wb")) == NULL)) {
                printf("??? %s\n", s);
                return EXIT_FAILURE;
        }
        if (argv[1][0] == 'e')
		  {
				long codeSize;

                /* Get original text size and output it */
                fseek(inFile, 0L, 2);
                textSize = ftell(inFile);
                rewind (inFile);
                if (fwrite(&textSize, sizeof(textSize), 1, outFile) < 1)
                        Error("cannot write");

					codeSize = encode_lzhuf_ff(inFile, outFile, textSize);

               printf("input : %ld bytes\n", textSize);
               printf("output: %ld bytes\n", codeSize);
	        fclose(inFile);
   	     fclose(outFile);
        }
		  else	// Assume 'D'ecode
		  {
			  	fclose(inFile);
				fclose(outFile);
		  		decode_lzhuf_f(inName, outName);
        }
        return EXIT_SUCCESS;
}
