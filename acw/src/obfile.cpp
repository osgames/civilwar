/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Read and Write Order of Battle to Binary Chunk Files
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <string.h>
#include "obfile.h"
#include "dfile.h"
#include "filesup.h"
#include "ob.h"
#if !defined(TESTBATTLE) && !defined(BATEDIT) && !defined(TESTCAMP) && !defined(CAMPEDIT)
#include "campaign.h"
#include "batldata.h"
#endif
#if !defined(CAMPEDIT) && !defined(TESTCAMP)
#include "unit3d.h"
#endif

#if !defined(TESTBATTLE) && !defined(BATEDIT)
#include "campwld.h"
#endif
#if !defined(TESTBATTLE) && !defined(BATEDIT) && !defined(CAMPEDIT)
#include "ai_camp.h"
#include "ai_unit.h"
#endif

#ifdef DEBUG
#include "memptr.h"
#include "patchlog.h"
#endif

/*
 * Some constants
 */

static const UWORD OBVersion = 0x02;		// Version 0.2
static const ChunkID obID = 'OBAT';

struct D_OB_H {
	IWORD version;
	IWORD nUnits;
};

struct D_OB_LINK {
	IWORD sister;			// Index of sister
	IWORD child;			// Index of child
	ILONG filePos;			// File position for this unit
	IWORD size;				// Size of this unit's data
};

struct OB_Link {
	UWORD sister;			// Index of sister
	UWORD child;			// Index of child
	ChunkPos filePos;			// File position for this unit
	UWORD size;				// Size of this unit's data
};


struct D_Order {
	IBYTE mode;
	IBYTE how;
	D_Location destination;
	IWORD onFacility;
	IWORD offFacility;
};

struct D_BattleOrder {
	IBYTE mode;
	D_Location destination;
};

struct D_UNIT_CAMPAIGN {
	D_Order realOrders;
	D_Order givenOrders;
	D_Location location;
	D_Location nextDestination;
	IWORD facing;
	IBYTE moveMode;
	IBYTE seperated;
	IBYTE hasSeperated;
	IBYTE reallySeperated;
	IBYTE hasReallySeperated;
	IBYTE joining;
	IBYTE givenNewOrder;
	IBYTE newOrder;
	IBYTE inBattle;
	IBYTE siegeAction;
	IWORD occupying;
	IBYTE extensions;		// Indicates if AI_Info, battleInfo or General are present
};

struct D_UNIT_BATTLEINFO {
	D_Location where;
	D_BattleOrder order;
	IWORD formationFacing;
	IWORD menFacing;
	IBYTE bSeperated;
	IBYTE bHasSeperated;
	IBYTE bReallySeperated;
	IBYTE bHasReallySeperated;
	IBYTE bJoining;
	IBYTE isDead;
	IBYTE AIFlag;
	IBYTE longRoute;
	IBYTE AI_action;
	IBYTE followingOrder;
	IBYTE formation;
	IWORD inCombat;

	// Also need battleOrderList
	// Also need specific info such as limbered/mounted
};

#if !defined(CAMPEDIT)
struct D_AI_INFO {
	IBYTE respondingToEnemy;
	IBYTE toBeOrdered;
	IBYTE toBeAttacked;
	IBYTE atFacility;
	IBYTE garrison;
	IBYTE checkFlag;
	IWORD facility;
};
#endif

enum UnitExtensions {
	HasGeneral = 0x01,
	HasCampaignAI = 0x02,
	HasBattleInfo = 0x04,
	HasName = 0x08,
};

struct D_President {
	IBYTE side;
	IBYTE nGenerals;
	IBYTE nRegiments;
};

struct D_Regiment {
	D_UNIT_CAMPAIGN unit;

	IBYTE basicType;
	IBYTE subType;
	ILONG strength;
	ILONG stragglers;
	ILONG casualties;
	IBYTE fatigue;
	IBYTE morale;
	IBYTE supply;
	IBYTE experience;
	// regiment speed		// Unused... type determines this
	
	//IBYTE name[MaxRegimentName];		// Stored afterwards
};

struct D_Brigade {
	D_UNIT_CAMPAIGN unit;
	IBYTE movedResting;
	IBYTE basicType;
	IBYTE subType;
};

struct D_Army {
	D_UNIT_CAMPAIGN unit;
	// IBYTE name[MaxArmyName];	// Stored afterwards
};

struct D_General {
	IBYTE rank;
	IBYTE sideInactive;
	IBYTE efficiency;
	IBYTE ability;
	IBYTE aggression;
	IBYTE experience;
};



/*=======================================================
 * Index section
 */

class ObWriteData {
	ChunkPos linkPos;
	Boolean headerWritten;
	ChunkFileWrite* file;
	CampaignWorld* world;
public:
	D_OB_LINK* links;
	int nLinks;
	int next;

public:
	ObWriteData(int howMany, ChunkFileWrite& f, CampaignWorld* wld);
	~ObWriteData();
	void writeUnit(Unit* u);
private:
	void write();
	void fillUnitData(Unit* unit, D_UNIT_CAMPAIGN& data);
	void writeUnitData(Unit* unit);
	void writeGeneral(General* g);
	void writeRegiment(Regiment* r);
#if !defined(CAMPEDIT) && !defined(TESTCAMP)
	void writeBattleInfo(Unit* unit);
#endif
#if !defined(TESTBATTLE) && !defined(BATEDIT) && !defined(CAMPEDIT)
	void writeAIInfo(Unit* unit);
#endif
};

ObWriteData::ObWriteData(int howMany, ChunkFileWrite& f, CampaignWorld* w)
{
	nLinks = howMany;
	links = new D_OB_LINK[howMany];
	memset(links, 0, sizeof(D_OB_LINK) * howMany);
	next = 0;

	linkPos = 0;
	headerWritten = False;

	file = &f;
	world = w;

	write();
}

ObWriteData::~ObWriteData()
{
	write();
	delete[] links;
}

void ObWriteData::write()
{
	ChunkPos endPos = file->getPos();

	if(headerWritten)
		file->setPos(linkPos);
	else
		linkPos = endPos;

	file->write(links, sizeof(D_OB_LINK) * nLinks);

	if(headerWritten)
		file->setPos(endPos);
	else
		headerWritten = True;
}


void putOrder(D_Order* dest, const Order& order)
{
	putByte(&dest->mode, order.mode);
	putByte(&dest->how, order.how);
	putLocation(&dest->destination, order.destination);
	putWord(&dest->onFacility, order.onFacility);
	putWord(&dest->offFacility, order.offFacility);
}

void getOrder(D_Order* src, Order& order)
{
	order.mode = OrderMode(getByte(&src->mode));
	order.how = OrderHow(getByte(&src->how));
	getLocation(&src->destination, order.destination);
	order.onFacility = getWord(&src->onFacility);
	order.offFacility = getWord(&src->offFacility);
}

void putBattleOrder(D_BattleOrder* dest, const BattleOrder& order)
{
	putByte(&dest->mode, order.mode);
	putLocation(&dest->destination, order.destination);
}

void getBattleOrder(D_BattleOrder* src, BattleOrder& order)
{
	order.mode = OrderMode(getByte(&src->mode));
	getLocation(&src->destination, order.destination);
}



/*
 * Write General to disk
 */

void ObWriteData::writeGeneral(General* g)
{
	D_General data;

	UBYTE sideInactive = g->side;
	if(g->inactive)
		sideInactive |= 4;

	putByte(&data.rank, g->rank);
	putByte(&data.sideInactive, sideInactive);
	putByte(&data.efficiency, g->efficiency);
	putByte(&data.ability, g->ability);
	putByte(&data.aggression, g->aggression);
	putByte(&data.experience, g->experience);

	file->write(&data, sizeof(data));
	// file->writeString(g->name);
	file->writeString(g->getRealName());
}

/*
 * Fill in a units data section
 */

void ObWriteData::fillUnitData(Unit* unit, D_UNIT_CAMPAIGN& data)
{
	putOrder(&data.realOrders, unit->realOrders);
	putOrder(&data.givenOrders, unit->givenOrders);
	putLocation(&data.location, unit->location);
	putLocation(&data.nextDestination, unit->nextDestination);
	putWord(&data.facing, unit->facing);
	putByte(&data.moveMode, unit->moveMode);
	putByte(&data.seperated, unit->seperated);
	putByte(&data.hasSeperated, unit->hasSeperated);
	putByte(&data.reallySeperated, unit->reallySeperated);
	putByte(&data.hasReallySeperated, unit->hasReallySeperated);
	putByte(&data.joining, unit->joining);
	putByte(&data.givenNewOrder, unit->givenNewOrder);
	putByte(&data.newOrder, unit->newOrder);
	putByte(&data.inBattle, unit->inBattle);
	putByte(&data.siegeAction, unit->siegeAction);

#if defined(TESTBATTLE) || defined(BATEDIT)
	putWord(&data.occupying, NoFacility);
#else
	if(!world || !unit->occupying)
		putWord(&data.occupying, NoFacility);
	else if(unit->convertedForBattle)
		putWord(&data.occupying, unit->occupyingID);
	else
		putWord(&data.occupying, world->facilities.getID(unit->occupying));
#endif

	UBYTE ext = 0;

	if(unit->general)
		ext |= HasGeneral;
#if !defined(TESTBATTLE) && !defined(BATEDIT) && !defined(CAMPEDIT)
	if(unit->aiInfo)
		ext |= HasCampaignAI;
#endif

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
	if(unit->battleInfo)
		ext |= HasBattleInfo;
#endif		

	putByte(&data.extensions, ext);
}

void ObWriteData::writeRegiment(Regiment* r)
{ 
	D_Regiment data;
	memset(&data, 0, sizeof(data));
	
	fillUnitData(r, data.unit);

	putByte(&data.basicType, r->type.basicType);
	putByte(&data.subType, r->type.subType);
	putLong(&data.strength, r->strength);
	putLong(&data.stragglers, r->stragglers);
	putLong(&data.casualties, r->casualties);
	putByte(&data.fatigue, r->fatigue);
	putByte(&data.morale, r->morale);
	putByte(&data.supply, r->supply);
	putByte(&data.experience, r->experience);

	putByte(&data.unit.extensions, getByte(&data.unit.extensions) | HasName);

	file->write(&data, sizeof(data));
	// file->writeString(r->name);
	file->writeString(r->getRealName());
}

#if !defined(TESTBATTLE) && !defined(BATEDIT) && !defined(CAMPEDIT)

void ObWriteData::writeAIInfo(Unit* u)
{
	D_AI_INFO data;
	UnitAI* ai = u->aiInfo;

#ifdef CHRIS_AI
	putByte(&data.respondingToEnemy, ai->RespondingToEnemy);
	putByte(&data.toBeOrdered, 		ai->ToBeOrdered);
	putByte(&data.toBeAttacked, 		ai->ToBeAttacked);
	putByte(&data.atFacility, 			ai->AtFacility);
	putByte(&data.garrison, 			ai->Garrison);
	putByte(&data.checkFlag, 			ai->CheckFlag);

	if(u->convertedForBattle)
		putWord(&data.facility, ai->facID);
	else if(ai->fac)
		putWord(&data.facility, world->facilities.getID(ai->fac));
	else
		putWord(&data.facility, NoFacility);
#endif

	file->write(&data, sizeof(data));
}

#endif

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
/*
 * Write battle Info data
 */

void ObWriteData::writeBattleInfo(Unit* u)
{
	D_UNIT_BATTLEINFO data;
	UnitBattle* info = u->battleInfo;

	putLocation(&data.where, info->where);
	putBattleOrder(&data.order, info->battleOrder);
	putWord(&data.formationFacing, info->formationFacing);
	putWord(&data.menFacing, info->menFacing);
	putByte(&data.bSeperated, info->bSeperated);
	putByte(&data.bHasSeperated, info->bHasSeperated);
	putByte(&data.bReallySeperated, info->bReallySeperated);
	putByte(&data.bHasReallySeperated, info->bHasReallySeperated);
	putByte(&data.bJoining, info->bJoining);
	putByte(&data.isDead, info->isDead);
	putByte(&data.AIFlag, info->AIFlag);
	putByte(&data.longRoute, info->longRoute);
	putByte(&data.AI_action, info->AI_action);
	putByte(&data.followingOrder, info->followingOrder);
	putByte(&data.formation, info->formation);
	putWord(&data.inCombat, info->inCombat);

	file->write(&data, sizeof(data));
}
#endif

/*
 * Write a unit's data to disk at current file positions
 */

void ObWriteData::writeUnitData(Unit* unit)
{
	/*
	 * Write the unit's information
	 */

	switch(unit->rank)
	{
	case Rank_President:
		{
			D_President data;
			memset(&data, 0, sizeof(data));

			President* p = (President*) unit;

			putByte(&data.side, p->side);
			putByte(&data.nGenerals, p->freeGenerals->entries());

#ifdef CAMPEDIT
			putByte(&data.nRegiments, 0);
#else
			Unit* r = p->freeRegiments;
			int rCount = 0;
			while(r)
			{
				rCount++;
				r = r->sister;
			}
			putByte(&data.nRegiments, rCount);
#endif

			file->write(&data, sizeof(data));

#ifndef CAMPEDIT
			/*
			 * Put the free regiment list
			 */

			r = p->freeRegiments;
			while(r)
			{
				writeRegiment((Regiment*) r);
				r = r->sister;
			}
#endif

			/*
			 * Put the free general list
			 */

			GeneralIter gen = *p->freeGenerals;
			while(++gen)
				writeGeneral(gen.current());
		}
		break;

	case Rank_Army:
		{
			Army* army = (Army*)unit;

#if 0
			file->showInfo("\r");
			file->showInfo(army->getRealName());
#endif

			D_Army data;
			memset(&data, 0, sizeof(data));


			fillUnitData(unit, data.unit);

			putByte(&data.unit.extensions, getByte(&data.unit.extensions) | HasName);

			file->write(&data, sizeof(data));
			// file->writeString(army->name);	// getName(False));
			file->writeString(army->getRealName());

			if(unit->general)
				writeGeneral(unit->general);
		}
		break;

	case Rank_Corps:
	case Rank_Division:
		{
			D_UNIT_CAMPAIGN data;
			memset(&data, 0, sizeof(data));

			fillUnitData(unit, data);

			file->write(&data, sizeof(data));
			if(unit->general)
				writeGeneral(unit->general);
		}
		break;
	case Rank_Brigade:
		{
			Brigade* b = (Brigade*) unit;

			D_Brigade data;
			memset(&data, 0, sizeof(data));

			fillUnitData(unit, data.unit);
			putByte(&data.movedResting, b->moved | (b->resting << 1));
			putByte(&data.basicType, b->AIbasicType);
			putByte(&data.subType, b->AIsubType);

			file->write(&data, sizeof(data));
			if(unit->general)
				writeGeneral(unit->general);
		}
		break;
	case Rank_Regiment:
		writeRegiment((Regiment*) unit);
		break;

	}

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
	if(unit->battleInfo)
		writeBattleInfo(unit);
#endif

#if !defined(TESTBATTLE) && !defined(BATEDIT) && !defined(CAMPEDIT)
	if(unit->aiInfo)
		writeAIInfo(unit);
#endif
}

/*
 * write a unit and all its children and sisters
 */

void ObWriteData::writeUnit(Unit* u)
{
	while(u)
	{
		D_OB_LINK* lnk = &links[next++];

		ChunkPos here = file->getPos();
		putLong(&lnk->filePos, here);
		writeUnitData(u);
		putWord(&lnk->size, file->getPos() - here);

		/*
	 	 * Write children
	 	 */

		if(u->child)
		{
			putWord(&lnk->child, next);
			writeUnit(u->child);
		}
		else
			putWord(&lnk->child, 0);

		/*
		 * Move to sister
		 */

		u = u->sister;

		if(u)
			putWord(&lnk->sister, next);
		else
			putWord(&lnk->sister, 0);
	}
}


int countUnits(Unit* unit)
{
	int howMany = 0;

	while(unit)
	{
		howMany++;
		if(unit->child)
			howMany += countUnits(unit->child);
		unit = unit->sister;
	}
	return howMany;
}

void writeOB(ChunkFileWrite& file, OrderBattle* ob, CampaignWorld* world)
{
	file.startWriteChunk(obID);

	/*
	 * Count number of units
	 */

	int howMany = countUnits(ob->getSides());

	D_OB_H header;
	putWord(&header.version, OBVersion);
	putWord(&header.nUnits, howMany);
	file.write(&header, sizeof(header));

	/*
	 * Create link data
	 * This also handles writing the index to the file and the
	 * destructor will rewrite it.
	 */

	{
		ObWriteData wData(howMany, file, world);

		wData.writeUnit(ob->getSides());
	}

	file.endWriteChunk();
}

/*===========================================================
 * Read Order of Battle
 */

class ObReadData {
	ChunkFileRead* file;
	CampaignWorld* world;
	OrderBattle* ob;
	OB_Link* links;
	UWORD nUnits;
	UWORD version;
public:
	ObReadData(ChunkFileRead& f, OrderBattle* orderBattle, CampaignWorld* wld);
	~ObReadData();
	void readUnit(Rank r, UWORD entry, Unit* parent);
private:
	void getUnitData(Unit* unit, D_UNIT_CAMPAIGN& data);
	General* readGeneral();
	void readRegiment(Regiment* r);
#if !defined(CAMPEDIT) && !defined(TESTCAMP)
	void readBattleInfo(Unit* u);
#endif
#if !defined(TESTBATTLE) && !defined(BATEDIT) && !defined(CAMPEDIT)
	void readAIInfo(Unit* u);
#endif
};

ObReadData::ObReadData(ChunkFileRead& f, OrderBattle* orderBattle, CampaignWorld* wld)
{
	file = &f;
	ob = orderBattle;
	world = wld;

	D_OB_H header;
	file->read(&header, sizeof(header));

	nUnits = getWord(&header.nUnits);
	version = getWord(&header.version);

	links = new OB_Link[nUnits];
	file->read(links, sizeof(OB_Link) * nUnits);
}

ObReadData::~ObReadData()
{
	delete[] links;
}

/*
 * Copy Data into unit
 */

void ObReadData::getUnitData(Unit* unit, D_UNIT_CAMPAIGN& data)
{
#if !defined(TESTBATTLE) && !defined(BATEDIT) && !defined(TESTCAMP) && !defined(CAMPEDIT)
	if(campaign && battle)
		unit->convertedForBattle = True;
#endif

	getOrder(&data.realOrders, unit->realOrders);
	getOrder(&data.givenOrders, unit->givenOrders);
	getLocation(&data.location, unit->location);
	getLocation(&data.nextDestination, unit->nextDestination);
	unit->needLocalDestination = True;

	unit->facing =getWord(&data.facing);
	unit->moveMode = MoveMode(getByte(&data.moveMode));
	unit->seperated = getByte(&data.seperated);
	// unit->hasSeperated = getByte(&data.hasSeperated);
	unit->reallySeperated = getByte(&data.reallySeperated);
	// unit->hasReallySeperated = getByte(&data.hasReallySeperated);
	unit->joining = getByte(&data.joining);
	unit->givenNewOrder = getByte(&data.givenNewOrder);
	unit->newOrder = getByte(&data.newOrder);
	unit->inBattle = getByte(&data.inBattle);
	unit->siegeAction = SiegeMode(getByte(&data.siegeAction));

#if !defined(TESTBATTLE) && !defined(BATEDIT)
	FacilityID fid = getWord(&data.occupying);
	if(world && (fid != NoFacility))
	{
		if(fid >= world->facilities.entries())
		{
			unit->siegeAction = SIEGE_None;
#ifdef PATCHLOG
			patchLog.printf("Unit occupying illegal facility %d",
				(int) fid);
#endif
		}
		else
		{
			Facility* f = world->facilities[fid];

			if(unit->convertedForBattle)
				unit->occupyingID = fid;
			else
				unit->occupying = f;

			if(!f->occupier || (f->occupier->rank > unit->rank))
			{
#ifdef TESTING_NONE
				if(unit->getRank() == Rank_Regiment)
					throw GeneralError("Regiment %s occupies %s",
						unit->getName(True),
						f->getNameNotNull());
#endif

				if(unit->getRank() < Rank_Regiment)
				{
					if(unit->siegeAction == SIEGE_Occupying)
						f->occupier = unit;
					else if(unit->siegeAction == SIEGE_Besieging)
						f->sieger = unit;
				}
			}
		}
	}
	else
		unit->siegeAction = SIEGE_None;
#endif
}

void ObReadData::readRegiment(Regiment* r)
{
	D_Regiment data;
	file->read(&data, sizeof(data));

	getUnitData(r, data.unit);
	UBYTE extensions = getByte(&data.unit.extensions);

	r->type.basicType = getByte(&data.basicType);
	r->type.subType = getByte(&data.subType);
	r->strength = getLong(&data.strength);
	r->stragglers = getLong(&data.stragglers);
	r->casualties = getLong(&data.casualties);
	r->fatigue = getByte(&data.fatigue);
	r->morale = getByte(&data.morale);
	r->supply = getByte(&data.supply);
	r->experience = getByte(&data.experience);

	if(extensions & HasName)
		r->setName(file->readString());
		// r->name = file->readString();

	/*
	 * Bodge to remove huge regiments...
	 */

	if( (r->strength + r->stragglers) > MaxMenPerRegiment)
	{
#ifdef PATCHLOG
		patchLog.printf("Regiment %s had huge number of men (%ld + %ld)",
			(const char*) r->getName(True), 
			(long) r->strength, 
			(long) r->stragglers);
#endif
		r->strength = MaxMenPerRegiment;
		r->stragglers = 0;
	}



#if defined(BATEDIT)
	/*
	 * Battle Editor... bodge experiences to match morale
	 */

	if(r->experience == 0)
		r->experience = r->morale;

#endif


#if !defined(CAMPEDIT) && !defined(TESTCAMP)
	if(extensions & HasBattleInfo)
		readBattleInfo(r);
#ifdef BATEDIT
	else
		r->makeBattleInfo();
		// throw GeneralError("Regiment doesn't have battleinfo");
#endif
#endif

#if !defined(TESTBATTLE) && !defined(BATEDIT) && !defined(CAMPEDIT)
	if(extensions & HasCampaignAI)
		readAIInfo(r);
#endif
}

General* ObReadData::readGeneral()
{
	General* g = new General;

	D_General data;
	file->read(&data, sizeof(data));

	g->rank			= getByte(&data.rank);
	g->side			= Side(getByte(&data.sideInactive) & 3);
	g->inactive		= (getByte(&data.sideInactive) & 4) ? True : False;
	g->efficiency	= getByte(&data.efficiency);
	g->ability		= getByte(&data.ability);
	g->aggression	= getByte(&data.aggression);
	g->experience	= getByte(&data.experience);

	// g->name = file->readString();
	g->setName(file->readString());

	return g;
}

#if !defined(TESTBATTLE) && !defined(BATEDIT) && !defined(CAMPEDIT)
void ObReadData::readAIInfo(Unit* u)
{
	if(version >= 0x02)		// AIInfo didn't exist in 0.1 and below
	{
		D_AI_INFO data;
		file->read(&data, sizeof(data));
		UnitAI* ai = u->aiInfo;
		if(!ai)
			ai = u->aiInfo = new UnitAI;
#ifdef CHRIS_AI
		ai->RespondingToEnemy	= getByte(&data.respondingToEnemy);
		ai->ToBeOrdered 			= getByte(&data.toBeOrdered);
		ai->ToBeAttacked			= getByte(&data.toBeAttacked);
		ai->AtFacility				= getByte(&data.atFacility);
		ai->Garrison				= getByte(&data.garrison);
		ai->CheckFlag				= AIPhase(getByte(&data.checkFlag));

		FacilityID fid = getWord(&data.facility);

		if(u->convertedForBattle)
			ai->facID = fid;
		else if(fid != NoFacility)
			ai->fac = world->facilities[fid];
		else
			ai->fac = 0;
#endif
	}
}
#endif

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
void ObReadData::readBattleInfo(Unit* u)
{
	if(version >= 0x01)		// BattleInfo didn't exist before 0.0
	{
		D_UNIT_BATTLEINFO data;

		file->read(&data, sizeof(data));

		UnitBattle* info = u->battleInfo;
		if(!info)
			info = u->makeBattleInfo();

		if(info)
		{	
			getLocation(&data.where, info->where);
			getBattleOrder(&data.order, info->battleOrder);
			info->formationFacing = getWord(&data.formationFacing);
			info->menFacing = getWord(&data.menFacing);
			info->bSeperated = getByte(&data.bSeperated);
			info->bHasSeperated = getByte(&data.bHasSeperated);
			info->bReallySeperated = getByte(&data.bReallySeperated);
			info->bHasReallySeperated = getByte(&data.bHasReallySeperated);
			info->bJoining = getByte(&data.bJoining);
			info->isDead = getByte(&data.isDead);
			info->AIFlag = getByte(&data.AIFlag);
			info->longRoute = getByte(&data.longRoute);
			info->AI_action = getByte(&data.AI_action);
			info->followingOrder = getByte(&data.followingOrder);
			info->formation = (Formation) getByte(&data.formation);
			info->inCombat = getWord(&data.inCombat);

			/*
			 * Bodge up...
			 */

			if(info->bJoining && !info->bReallySeperated)
			{
#ifdef PATCHLOG
				patchLog.printf("%s had bJoining set, but bReallySeperated clear",
					u->getName(True));
#endif
				info->bReallySeperated = True;
				u->setBattleAttachMode(Joining);
			}
		}
	}
}
#endif

void ObReadData::readUnit(Rank r, UWORD entry, Unit* parent)
{
	ASSERT(r <= Rank_Regiment);
	ASSERT(r >= Rank_President);

	do
	{
		OB_Link* link = &links[entry];

		Unit* unit = 0;
		General* general = 0;

		// Read this unit in

		file->setPos(link->filePos);

		switch(r)
		{
		case Rank_President:
			{
				President* p = new President;
				unit = p;

				D_President data;
				file->read(&data, sizeof(data));

				p->side = Side(getByte(&data.side));
				UBYTE nGenerals = getByte(&data.nGenerals);
				UBYTE nRegiments = getByte(&data.nRegiments);

				/*
				 * Read free regiments
				 */

				while(nRegiments--)
				{
					Regiment* r = new Regiment;
					readRegiment(r);
#ifndef CAMPEDIT
					p->addFreeRegiment(r);
#endif
				}

				/*
				 * Read free generals
				 */

				while(nGenerals--)
				{
					General* g = readGeneral();
					p->addFreeGeneral(g);
				}
			}
			break;
		case Rank_Army:
			{
				Army* army = new Army;
				unit = army;

				D_Army data;
				file->read(&data, sizeof(data));
				getUnitData(unit, data.unit);
				UBYTE extension = getByte(&data.unit.extensions);

				if(extension & HasName)
					army->setName(file->readString());
					// army->name = file->readString();

				if(extension & HasGeneral)
					general = readGeneral();

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
				if(extension & HasBattleInfo)
					readBattleInfo(unit);

#ifdef BATEDIT
				else
					unit->makeBattleInfo();
					// throw GeneralError("Army doesn't have battleinfo");
#endif
#endif

#if !defined(TESTBATTLE) && !defined(BATEDIT) && !defined(CAMPEDIT)
				if(extension & HasCampaignAI)
					readAIInfo(unit);
#endif
#if 0
				file->showInfo("\r");
				file->showInfo(army->getName(False));
#endif
			}
			break;
		case Rank_Corps:
			{
				unit = new Corps;

				D_UNIT_CAMPAIGN data;
				file->read(&data, sizeof(data));
				getUnitData(unit, data);
				UBYTE extension = getByte(&data.extensions);

				if(extension & HasGeneral)
					general = readGeneral();

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
				if(extension & HasBattleInfo)
					readBattleInfo(unit);
#ifdef BATEDIT
				else
					unit->makeBattleInfo();
					// throw GeneralError("Corps doesn't have battleinfo");
#endif
#endif
#if !defined(TESTBATTLE) && !defined(BATEDIT) && !defined(CAMPEDIT)
				if(extension & HasCampaignAI)
					readAIInfo(unit);
#endif

			}
			break;
		case Rank_Division:
			{
				unit = new Division;

				D_UNIT_CAMPAIGN data;
				file->read(&data, sizeof(data));
				getUnitData(unit, data);
				UBYTE extension = getByte(&data.extensions);

				if(extension & HasGeneral)
					general = readGeneral();

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
				if(extension & HasBattleInfo)
					readBattleInfo(unit);
#ifdef BATEDIT
				else
					unit->makeBattleInfo();
					// throw GeneralError("Division doesn't have battleinfo");
#endif
#endif
#if !defined(TESTBATTLE) && !defined(BATEDIT) && !defined(CAMPEDIT)
				if(extension & HasCampaignAI)
					readAIInfo(unit);
#endif

			}
			break;
		case Rank_Brigade:
			{
				Brigade* b = new Brigade;
				unit = b;

				D_Brigade data;
				file->read(&data, sizeof(data));
				getUnitData(unit, data.unit);
				UBYTE extension = getByte(&data.unit.extensions);

				b->moved = getByte(&data.movedResting) & 1;
				b->resting = (getByte(&data.movedResting) >> 1) & 1;
				b->AIbasicType = getByte(&data.basicType);
				b->AIsubType = getByte(&data.subType);

				if(extension & HasGeneral)
					general = readGeneral();
#if !defined(CAMPEDIT) && !defined(TESTCAMP)
				if(extension & HasBattleInfo)
					readBattleInfo(unit);
#ifdef BATEDIT
				else
					unit->makeBattleInfo();
					// throw GeneralError("Brigade doesn't have battleinfo");
#endif
#endif
#if !defined(TESTBATTLE) && !defined(BATEDIT) && !defined(CAMPEDIT)
				if(extension & HasCampaignAI)
					readAIInfo(unit);
#endif

			}
			break;
		case Rank_Regiment:
			{
				Regiment* r = new Regiment;
				unit = r;
				readRegiment(r);
			}
			break;
#ifdef DEBUG
		default:
			throw GeneralError("Missing Rank case (%d) in obfile::readUnit", r);
#endif
		}

		ASSERT(unit != 0);


		if((r == Rank_Regiment) || (link->child))
		{
			if(parent)
				parent->addChild(unit);
			else
				ob->addSide((President*)unit);

			if(general)
				ob->assignGeneral(general, unit);
		}
		else
		{
#if defined(PATCHLOG)
		 	patchLog.printf("Unit deleted because it does not have any children");
			if(general)
			 	patchLog.printf("Leader = %s", general->getName());
		 	patchLog.printf("Parent = %s", parent->getName(True));
#endif
			if(general)
				delete general;
			delete unit;
		}


#ifdef DEBUG1
		MemPtr<char> buffer(200);
		sprintf(buffer, "\r%s", unit->getName(True));
		file->putInfo(buffer);
#endif

		/*
		 * Process Children
		 */

		if(link->child)
		{
			/*
			 * Patch for Master's version
			 * Somehow some saved games have regiments attached to regiments!
			 * Make these children into sisters instead!
			 */

			if(r == Rank_Regiment)
			{
#if defined(PATCHLOG)
				patchLog.printf("Regiment attached to Regiment");
				patchLog.printf("%s", unit->getName(True));
#endif
				readUnit(r, link->child, parent);
			}
			else
				readUnit(demoteRank(r), link->child, unit);


#if defined(PATCHLOG)
			if(unit->childCount == 0)
			{
				patchLog.printf("%s's children were not valid", unit->getName(True));
			}
#endif
		}

		/*
		 * Loop round to sister
		 */

		entry = link->sister;
	} while(entry);
}

void readOB(ChunkFileRead& file, OrderBattle* ob, CampaignWorld* world)
{
	if(file.startReadChunk(obID) == CE_OK)
	{
		ObReadData rData(file, ob, world);

		rData.readUnit(Rank_President, 0, 0);
#if !defined(TESTBATTLE) && !defined(BATEDIT)
		ob->tidy();
#endif
	}
}

