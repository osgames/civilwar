/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	3D Shape Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.14  1994/08/09  15:42:15  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.12  1994/04/11  21:28:35  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/04/11  13:36:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/04/05  12:28:09  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/03/21  21:03:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/03/18  15:07:16  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1993/12/23  09:26:01  Steven_Green
 * Mods for 3D Sprites.
 *
 * Revision 1.6  1993/12/10  16:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1993/11/30  02:57:11  Steven_Green
 * Some attempted optimisation
 *
 * Revision 1.4  1993/11/25  04:40:06  Steven_Green
 * map grid object added
 *
 * Revision 1.3  1993/11/24  09:32:52  Steven_Green
 * Use of Shape base object.
 *
 * Revision 1.2  1993/11/19  18:59:26  Steven_Green
 * Simplified drawing in preparation for the clipping
 *
 * Revision 1.1  1993/11/16  22:42:06  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "shape3d.h"
#include "shplist.h"
#include "view3d.h"
#include "image.h"
#include "polygon.h"
#include "polyclip.h"
#include "terrain.h"
#ifdef DEBUG1
#include "testtime.h"
#endif

Point3D Shape3D::draw(ObjectDrawData* drawData, const Position3D& position)
{
	return Point3D(0,0,0);
}


/*
 * Wrapper function for drawShape3D()
 * Called from WCPtrSlist<WorldObject>.findAll()
 */

void drawObject3D(WorldObject* ob, void* data)
{
	ob->shape->draw((ObjectDrawData*)data, *ob);
}


/*
 * Draw an Object List
 */


void WorldObjectList::draw(Region* bm, ViewPoint* view, LightSource* l, Terrain* terrain)
{
	ObjectDrawData data(view, l, terrain);			// Also initialises ShapeList

 	terrain->resetColours();
	forAll(drawObject3D, &data);

#ifdef DEBUG1
	testTimer.mark("Draw");
#endif

	data.draw(bm, &data);

#ifdef DEBUG1
	testTimer.mark("Render");
#endif
}

WorldObjectList::~WorldObjectList()
{
	clearAndDestroy();
}
