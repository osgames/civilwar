/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	City/Facility Info Display
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include "cityinfo.h"
#include "campaign.h"
#include "colours.h"
#include "campwld.h"
#include "text.h"
#include "system.h"
#include "screen.h"
#include "barchart.h"
#include "tables.h"
#include "darken.h"
#include "game.h"
#include "sprlib.h"
#include "options.h"


#define Font_CityInfo			Font_JAME15	// Font_EMFL15
#define Font_RailHeadCount		Font_JAME_5


/*
 * Table of values to use to make BuildInfo
 */

#if 0
BuildSetup buildSetup[BuildCount] = {
	BuildSetup(Rect( 2,  85, 57, 27), Mob_Fortification,	False, CI_Fort1,			Facility::F_None),					// Fort
	BuildSetup(Rect(63,  85, 57, 27), Mob_RailHead,			False, CI_RailHead,		Facility::F_None),					// Rail
	BuildSetup(Rect( 2, 115, 57, 27), Mob_Recruitment,		True,  CI_Recruitment,	Facility::F_RecruitmentCentre),	// Recruit
	BuildSetup(Rect(63, 115, 57, 27), Mob_Training,			True,  CI_Training,		Facility::F_TrainingCamp),			// Training
	BuildSetup(Rect( 2, 145, 57, 27), Mob_Hospital,			True,  CI_Hospital,		Facility::F_Hospital),				// Hospital
	BuildSetup(Rect(63, 145, 57, 27), Mob_POW,				True,  CI_POW,				Facility::F_POW),						// POW
	BuildSetup(Rect( 2, 175, 57, 27), Mob_SupplyDepot,		False, CI_Depot,			Facility::F_SupplyDepot),			// Depot
	BuildSetup(Rect(63, 175, 57, 27), Mob_SupplyWagon,		False, CI_BuildWagon,	Facility::F_None),		 			// Wagon
	BuildSetup(Rect( 2, 255, 57, 27), Mob_Nothing,			True,  CI_Infantry,		Facility::F_None),		  			// Infantry
	BuildSetup(Rect(63, 255, 57, 27), Mob_Nothing,			True,  CI_Cavalry,		Facility::F_None),		  			// Cavalry
	BuildSetup(Rect( 2, 285, 57, 27), Mob_Nothing,			True,  CI_Artillery,		Facility::F_None),		  			// Artillery
	BuildSetup(Rect(63, 285, 57, 27), Mob_Nothing,			True,  CI_Naval,			Facility::F_None) 		  			// Naval
};
#else		// Shift up to make room for supply bar chart
BuildSetup buildSetup[BuildCount] = {
	BuildSetup(Rect( 2,  82, 57, 27), Mob_Fortification,	False, CI_Fort1,			Facility::F_None),					// Fort
	BuildSetup(Rect(63,  82, 57, 27), Mob_RailHead,			False, CI_RailHead,		Facility::F_None),					// Rail
	BuildSetup(Rect( 2, 110, 57, 27), Mob_Recruitment,		True,  CI_Recruitment,	Facility::F_RecruitmentCentre),	// Recruit
	BuildSetup(Rect(63, 110, 57, 27), Mob_Training,			True,  CI_Training,		Facility::F_TrainingCamp),			// Training
	BuildSetup(Rect( 2, 138, 57, 27), Mob_Hospital,			True,  CI_Hospital,		Facility::F_Hospital),				// Hospital
	BuildSetup(Rect(63, 138, 57, 27), Mob_POW,				True,  CI_POW,				Facility::F_POW),						// POW
	BuildSetup(Rect( 2, 166, 57, 27), Mob_SupplyDepot,		False, CI_Depot,			Facility::F_SupplyDepot),			// Depot
	BuildSetup(Rect(63, 166, 57, 27), Mob_SupplyWagon,		False, CI_BuildWagon,	Facility::F_None),		 			// Wagon
	BuildSetup(Rect( 2, 255, 57, 27), Mob_Nothing,			True,  CI_Infantry,		Facility::F_None),		  			// Infantry
	BuildSetup(Rect(63, 255, 57, 27), Mob_Nothing,			True,  CI_Cavalry,		Facility::F_None),		  			// Cavalry
	BuildSetup(Rect( 2, 285, 57, 27), Mob_Nothing,			True,  CI_Artillery,		Facility::F_None),		  			// Artillery
	BuildSetup(Rect(63, 285, 57, 27), Mob_Nothing,			True,  CI_Naval,			Facility::F_None) 		  			// Naval
};
#endif



void Facility::showCampaignInfo(Region* window)
{
	FacilityInfo info;
	info.makeInfo(this);
	info.drawInfo(window, False);
}

static MobiliseType infantryBuildTypes[5] = {
	Mob_InfantryRegular,
	Mob_InfantryMilitia,
	Mob_InfantrySharpShooter,
	Mob_Engineer,
	Mob_RailEngineer
};

static MobiliseType cavalryBuildTypes[5] = {
	Mob_cavalryRegular,
	Mob_cavalryMilitia,
	Mob_cavalryRegularMounted,
	Mob_cavalryMilitiaMounted,
	Mob_Nothing
};

static MobiliseType artilleryBuildTypes[5] = {
	Mob_ArtillerySmoothbore,
	Mob_ArtilleryLight,
	Mob_ArtilleryRifled,
	Mob_ArtillerySiege,
	Mob_Nothing
};

static MobiliseType navalBuildTypes[5] = {
	Mob_Naval,
	Mob_NavalIronClad,
	Mob_Riverine,
	Mob_RiverineIronClad,
	Mob_Nothing
};


void BuildInfo::setupGroup(MobiliseType* table, Facility* where)
{
	/*
	 * Set values to default
	 */

	enabled = True;
	upgradeable = False;
	buildDays = 0;	// Need to find longest naval thing being built
	isBuilt = True;

	int count = 5;
	MobiliseList& mobList = campaign->world->mobList;

	while(count--)
	{
		MobiliseType type = *table++;

		if(type != Mob_Nothing)
		{
			/*
			 * If on other side and high realism then don't show building status
			 */

			if(!seeAll &&
				 !game->isPlayer(where->side) &&
			    (game->getDifficulty(Diff_Resource) >= Complex2))
			{
				upgradeable = False;
				buildDays = 0;
			}
			else
			{
				if(mobList.canIbuild(where, type))
				{
					upgradeable = True;
					enabled = True;
				}

				UBYTE days = mobList.whenMobilising(where, type);
				if(days && (!buildDays || (days < buildDays)))
				{
					buildDays = days;
					totalDays = mobList.totalMobilising(where, type);
				}
			}
		}
	}

}



/*
 * Create Info information
 */

void FacilityInfo::makeInfo(Facility* f)
{
	facility = f;

	MobiliseList& mobList = campaign->world->mobList;

	localResources = f->getLocalResources();
	importedResources = f->getGlobalResources();

	int count = 0;
	BuildSetup* sPtr = buildSetup;
	BuildInfo*  bPtr = info;

	while(count < BuildCount)
	{
		if(!seeAll &&
			 !game->isPlayer(facility->side) &&
		    (game->getDifficulty(Diff_Resource) >= Complex3))
		{
			bPtr->enabled = False;
		}
		else
		if( ((sPtr->what == Mob_Nothing) || mobList.canItemExist(sPtr->what)) &&
			((f->facilities & Facility::F_City) || !sPtr->onlyInCity) )
		{
			bPtr->enabled = True;
			bPtr->graphic = sPtr->graphic;	// Copy this because it may get changed

			if(sPtr->what != Mob_Nothing)
			{
				bPtr->upgradeable = mobList.canIbuild(f, sPtr->what);
				bPtr->buildDays = mobList.whenMobilising(f, sPtr->what);
				bPtr->totalDays = mobList.totalMobilising(f, sPtr->what);
			}
			else
			{
				bPtr->upgradeable = False;
				bPtr->buildDays = 0;
			}

			if(sPtr->type != Facility::F_None)
			{
				if(f->facilities & sPtr->type)
					bPtr->isBuilt = True;
				else
					bPtr->isBuilt = False;
			}

			/*
			 * Do special cases
			 */

			switch(count)
			{
			case BuildRail:
				if(!f->railCount)
					bPtr->enabled = False;
				else
					bPtr->isBuilt = True;
				break;

			case BuildFort:
				if(f->fortification)
				{	
					ASSERT(f->fortification <= MaxFortification);
					bPtr->graphic = GameSprites(CI_Fort1 + f->fortification - 1);
					bPtr->isBuilt = True;
				}
				else
					bPtr->isBuilt = False;
				break;

			case BuildWagon:
				if(f->facilities & Facility::F_SupplyDepot)
					bPtr->isBuilt = True;
				else
				{
					bPtr->isBuilt = False;
					bPtr->upgradeable = False;
				}
				break;

			case BuildInfantry:
				bPtr->setupGroup(infantryBuildTypes, f);
				break;

			case BuildCavalry:
				bPtr->setupGroup(cavalryBuildTypes, f);
				break;

			case BuildArtillery:
				bPtr->setupGroup(artilleryBuildTypes, f);
				break;

			case BuildNaval:
				if(f->facilities & Facility::F_Port)
				{
					bPtr->setupGroup(navalBuildTypes, f);
					break;
				}
				else
					bPtr->enabled = False;
				break;
			}

			/*
			 * Disable certain aspects if high realism
			 */

			if(!seeAll && !game->isPlayer(facility->side))
			{
			 	bPtr->upgradeable = False;

		    	if(game->getDifficulty(Diff_Resource) >= Complex2)
				{
					bPtr->isBuilt = False;
				}
		    	if(game->getDifficulty(Diff_Resource) >= Complex1)
				{
					bPtr->buildDays = 0;
					bPtr->totalDays = 0;
				}
			}
		}
		else
			bPtr->enabled = False;

		count++;
		sPtr++;
		bPtr++;
	}
}

/*
 * Draw the info area
 */

/*
 * Display Info about facilities
 */


void showResource(Region* window, ResourceType type, const ResourceSet* local, const ResourceSet* imported)
{
	SDimension y = SDimension(206 + type * 11);
	const SDimension barFullWidth = 100;
	const SDimension barWidth = 80;
	const SDimension barHalfWidth = barWidth / 2;

	ResourceVal maxValue = ResourceSet::getMaxVal(type);

	if(maxValue)
	{
		ResourceVal localVal;
		
		if(local)
			localVal = local->getValue(type);
		else
			localVal = 0;

		ResourceVal importedVal;
		
		if(imported)
			importedVal = imported->getValue(type);
		else
			importedVal = 0;

#ifdef DEBUG
		if((localVal > maxValue) || (importedVal > maxValue))
		{
			throw GeneralError("Too many resources");
		}
#endif

		game->sprites->drawSprite(window, Point(4, y), GameSprites(type + CI_Resource1));

		SDimension w1 = SDimension((localVal * barHalfWidth) / maxValue);
		SDimension w2 = SDimension(((importedVal + localVal) * barHalfWidth) / maxValue) - w1;

		window->box(18, y, barFullWidth, 10, Blue);

		window->HLine(18, y+8, w1+w2, Black);
		window->VLine(18 + w1 + w2 + 1, y+1, 8, Black);

		if(w1)
			window->box(18, y+2, w1, 6, Green);
		if(w2)
			window->box(18 + w1, y+2, w2, 6, Yellow);

		int percent = ((localVal + importedVal) * 1000) / (maxValue * 2);

		Region r1 = *window;
		// r1.setClip(window->getX() + 20, window->getY() + y, barWidth, 10);
		r1.setClip(window->getX() + 20, window->getY() + y, barFullWidth, 10);
		TextWindow textWind(&r1, Font_JAME_5);
		// textWind.setHCentre(True);
		textWind.setVCentre(True);
		textWind.setPosition(Point(w1 + w2 + 2, y));
		// textWind.setColours(Black);
		textWind.setColours(White);
		textWind.setShadow(Black);
		textWind.wprintf("%d.%d%%", percent / 10, percent % 10);
	}
#if 0
	else
		window->box(0,y, 120,10, Green);
#endif
}


void showMobiliseBar(Region* bm, int val, int maxVal, Rect r, const char* text)
{
	/*
	 * Fill area in grey
	 */

	bm->frame(r.getX(), r.getY(), r.getW(), r.getH(), Black);
	bm->box(r + Rect(1,1,-2,-2), DGrey);

	/*
	 * Add graduations
	 */

	bm->VLine(r.getX() + r.getW() / 4, 			r.getY(), r.getH(), LGrey);
	bm->VLine(r.getX() + r.getW() / 2, 			r.getY(), r.getH(), LGrey);
	bm->VLine(r.getX() + (r.getW() * 3) / 4, 	r.getY(), r.getH(), LGrey);

	/*
	 * Put the filled in area on
	 */

	SDimension w = (val * (r.getW() - 1)) / maxVal;
	if(w)
		bm->box(r.getX(), r.getY() + 1, w, r.getH() - 2, Green);

		// bm->greyOut(Rect(r.getX(), r.getY() + 1, w, r.getH() - 2), Green);

	/*
	 * Overlay Text
	 */

	if(text)
	{
		Region bm1 = *bm;
		// TextWindow textWind(&bm1, Font_JAME_5);
		TextWindow textWind(&bm1, Font_JAME08);
		// bm1.setClip(bm->getX() + r.getX(), bm->getY() + r.getY() - 6, r.getW(), 6);
		bm1.setClip(bm->getX() + r.getX() + r.getW()/2 - 30/2, bm->getY() + r.getY() - 10 - 1, 30, 10);
		textWind.setVCentre(True);		// Centre vertically
		textWind.setHCentre(True);		// CentreHorizontally

		bm1.fill(Black);
		// textWind.setPosition(Point(3, 1));	// Leave a couple of pixels to the left
		// textWind.setColours(Black);

		// textWind.setPosition(Point(2, 0));	// Leave a couple of pixels to the left
		textWind.setColours(White);
		textWind.draw(text);
	}
}




void FacilityInfo::drawInfo(Region* region, Boolean iconize)
{
	/*
	 * Fill the background in first
	 */

	if(iconize)
	{
		region->fill(Gold2);
	}
	else
	{
		SpriteIndex sNum;

		if(facility->side == SIDE_USA)
			sNum = FillUSA_Grey;
		else if(facility->side == SIDE_CSA)
			sNum = FillCSA_Grey;
		else
			sNum = FillPattern_Flag;

		SpriteBlock* pattern = game->sprites->read(sNum);
		region->fill(pattern);
		pattern->release();
	}

	/*
	 * Set up a textwindow
	 */

	 
	Region r = Region(region->getImage(), Rect(region->getX() + 1,region->getY(),region->getW() - 2,26));
	TextWindow textWind(&r, Font_CityInfo);

	/*
	 * Draw a frame in owner's colour
	 */

	Colour col;

	if(facility->side == SIDE_CSA)
		col = CSAColour;
	else if(facility->side == SIDE_USA)
		col = USAColour;
	else
		col = Neutral_Colour;

	r.frame(0, 0, r.getW(), r.getH(), col);
	r.frame(1, 1, r.getW()-2, r.getH()-2, col);

	/*
	 * Display the facility's name
	 */

	textWind.setColours(Black);
	textWind.setFormat(True, True);		// Horizontal & Vertical Centre

	const char* s = facility->getName();
	if(s)
	{
		FontID titleFont;

		if(fontSet(Font_EMFL15)->getWidth(s) < r.getW())
			titleFont = Font_EMFL15;
		else if(fontSet(Font_JAME15)->getWidth(s) < r.getW())
			titleFont = Font_JAME15;
		else if(fontSet(Font_EMFL10)->getWidth(s) < r.getW())
			titleFont = Font_EMFL10;
		else if(fontSet(Font_EMFL_8)->getWidth(s) < r.getW())
			titleFont = Font_EMFL_8;
		else
			titleFont = Font_JAME08;

		textWind.setFont(titleFont);
		textWind.setWrap(True);
		textWind.setPosition(Point(0, 4));
		textWind.draw(facility->getName());
	}


#ifdef DEBUG

	/*
	 * Display whether it is occupied and sieged by drawing some
	 * coloured rectangles
	 */

	Colour ourCol;
	Colour enemyCol;
	
	if(facility->side == SIDE_CSA)
	{
		ourCol = CSAColour;
		enemyCol = USAColour;
	}
	else if(facility->side == SIDE_USA)
	{
		ourCol = USAColour;
		enemyCol = CSAColour;
	}
	else
	{
		ourCol = Neutral_Colour;
		enemyCol = Neutral_Colour;
	}

	region->box(1,1,3,3, ourCol);
	if(facility->occupier)
		region->box(5,1,3,3, enemyCol);
	if(facility->sieger)
		region->box(9,1,3,3, enemyCol);


#endif

	/*
	 * If it is not a city what it is
	 */

	GameSprites icon;

	if(facility->facilities & Facility::F_City)
	{
		City* city = (City*) facility;

		static GameSprites cityIcon[] = {
			CI_Industrial1,
			CI_Agricultural1,
			CI_Mixed1,
			CI_Mixed1
		};

		if(facility->facilities & Facility::F_KeyCity)
			icon = CI_KeyCity;
		else
			icon = GameSprites(cityIcon[city->type] + city->size / (64/4));
	}
	else
	{
		if(facility->fortification)
			icon = CI_FortFacility;
		else if(facility->railCount)
			icon = CI_RailFacility;
		else if(facility->facilities & Facility::F_Port)
			icon = CI_Port;
		else if(facility->facilities & Facility::F_LandingStage)
			icon = CI_BoatFacility;
		else		// This shouldn't happen!
			icon = CI_Mixed1;
	}
	game->sprites->drawSprite(region, Point(2,27), icon);

	/*
	 * If it is a port or landingstage
	 */

	if(facility->facilities & Facility::F_Port)
		game->sprites->drawSprite(region, Point(63, 27), CI_Port);
	else if(facility->facilities & Facility::F_LandingStage)
		game->sprites->drawSprite(region, Point(63, 27), CI_BoatFacility);
	else
	{
	 	region->box(Rect(63, 27, 57, 54), LGrey);
	 	region->frame(Rect(63, 27, 57, 54), Black);
	}

	/*
	 * Display Facility bits
	 */

	BuildSetup* sPtr = buildSetup;
	BuildInfo*  bPtr = info;


	for(int count = 0; count < BuildCount; count++, sPtr++, bPtr++)
	{
		if(bPtr->enabled)
		{
			game->sprites->drawSprite(region, sPtr->r, bPtr->graphic);

			// if(iconize && bPtr->upgradeable)		// Make gold if clickable
			if(bPtr->upgradeable)		// Make gold if clickable
			{
				region->frame(sPtr->r, Yellow, Yellow);
				region->frame(sPtr->r + Rect(1,1,-2,-2), White);
				region->frame(sPtr->r + Rect(2,2,-4,-4), Yellow);
			}

			if(!bPtr->isBuilt)
				greyRegion(region, sPtr->r + Rect(2,2,-4,-4));

			/*
			 * Special Cases
			 */

			switch(count)
			{
				case BuildRail:
				{
					textWind.setClip(Rect(38 + sPtr->r.x + region->getX(),4+sPtr->r.y+region->getY(),17,11));
					textWind.setFont(Font_RailHeadCount);
					textWind.setFormat(True, True);
					textWind.setColours(Black);
					textWind.setPosition(sPtr->r + Point(38,4));
					textWind.wprintf("%d", facility->railCapacity);
				}
				break;
			}

			if(bPtr->buildDays)
			{
				int spentDays = bPtr->totalDays - bPtr->buildDays;
				if(spentDays < 0)
					spentDays = 0;
				else if(spentDays > bPtr->totalDays)
					spentDays = bPtr->totalDays;

				char days[10];
				sprintf(days, "%d%%", (int) (spentDays * 100) / bPtr->totalDays);

				showMobiliseBar(region, spentDays, bPtr->totalDays,
					Rect(sPtr->r.getX() + 2,
						  sPtr->r.getY() + sPtr->r.getH() - 5,
						  sPtr->r.getW() - 4,
						  5),
					days);
			}
		}
		else
		{
			region->box(sPtr->r, LGrey);
			region->frame(sPtr->r, Black);
		}
	}

	/*
	 * Resources
	 */

	if(seeAll ||
		 game->isPlayer(facility->side) ||
	    (game->getDifficulty(Diff_Resource) <= Complex2))
	{
		showResource(region, R_Human,		 localResources, importedResources);
		showResource(region, R_Horses,	 localResources, importedResources);
		showResource(region, R_Food,		 localResources, importedResources);
		showResource(region, R_Materials, localResources, importedResources);
	}

	/*
	 * Show supplies
	 */

	game->sprites->drawSprite(region, Point(4, 194), GameSprites(CI_Wagon));
	showBar(region, facility->supplies, MaxSupply, Rect(18,194,100,10), supplyStr(facility->supplies));


#ifdef DEBUG
	/*
	 * Display AI Info
	 */

	if(showAI)
	{
		textWind.setClip(Rect(2+region->getX(), 27+region->getY(), 57,54));
		textWind.setFont(Font_JAME08);
		textWind.setFormat(False, True);
		textWind.setColours(White);
		textWind.setPosition(Point(0, 20));
		textWind.wprintf("%d %d", (int) facility->aiDefend, (int) facility->aiAttack);
	}

#endif


	machine.screen->setUpdate(*region);
}
