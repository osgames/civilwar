/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Machine independant file format
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1993/12/16  22:19:35  Steven_Green
 * Initial revision
 *
 * Revision 1.1  1993/12/16  16:42:54  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "filedata.h"

void putWord(IWORD* dest, UWORD src)
{
	dest->b[0] = src;
	dest->b[1] = src >> 8;
}

void putLong(ILONG* dest, ULONG src)
{
	dest->b[0] = src;
	dest->b[1] = src >> 8;
	dest->b[2] = src >> 16;
	dest->b[3] = src >> 24;
}


UWORD getWord(IWORD* src)
{
	return src->b[0] + (src->b[1] << 8);
}

ULONG getLong(ILONG* src)
{
	return src->b[0] + (src->b[1] << 8) + (src->b[2] << 16) + (src->b[3] << 24);
}
