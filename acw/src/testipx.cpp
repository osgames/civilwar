/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Test IPX network functions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1994/05/19  17:44:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/05/04  22:09:38  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <fstream.h>
#include <conio.h>
#include <time.h>
#include <stdio.h>
   
#include "ipx.h"
#include "timer.h"


/*
 * Test Functions
 */

typedef RealMemory<ECB_Header> ECB;

class MyMessage {
	char buffer[200];
public:
	operator char*() { return buffer; }
};

#if 0
/*
 * Set up a send and receive buffer
 * Send a message every few seconds.
 * Display any incoming messages
 */

void rawTest()
{
	cout << "\nIPX Install returned " << IPX_install() << endl;
	Socket socket = IPX_openSocket(defaultSocket);
	cout << "Open Socket returned " << hex << setw(4) << setfill('0') << socket << dec << endl;

	if(socket)
	{
		RealMemory<IPX_NetAddress> ourAddress_R;
		IPX_NetAddress& ourAddress = ourAddress_R;
		ourAddress.socket = socket;

		IPX_GetInternetworkAddress(&ourAddress);

		cout << "\nNetwork address is: " << ourAddress.network <<
				  "\nNode address is:    " << hex << setfill('0') <<
				  		setw(2) << int(ourAddress.node[0]) <<
				  		setw(2) << int(ourAddress.node[1]) <<
				  		setw(2) << int(ourAddress.node[2]) <<
				  		setw(2) << int(ourAddress.node[3]) <<
				  		setw(2) << int(ourAddress.node[4]) <<
				  		setw(2) << int(ourAddress.node[5]) <<
				  "\nSocket is:          " <<
				  		setw(4) << ourAddress.socket <<
				  		dec << setfill(' ') << endl;

		/*
		 * Set up an Event Control Block and IPX Header for sending
		 */

		ECB testECB;
		ECB_Header& ecb = testECB;

		RealMemory<IPX_Header> ipxHeaderR;
		IPX_Header& ipxHeader = ipxHeaderR;

		RealMemory<MyMessage> bufferR;
		MyMessage& buffer = bufferR;

		memset(&ecb, 0, sizeof(ecb));
		memset(&ipxHeader, 0, sizeof(ipxHeader));

		ecb.socket = socket;
		ecb.fragment_count = 2;
		ecb.hdr.ptr = &ipxHeader;
		ecb.hdr.size = sizeof(ipxHeader);
		ecb.buffer.ptr = &buffer;
		ecb.buffer.size = sizeof(buffer);

		ipxHeader.type = IPX_TYPE;

		/*
		 * Set up a receive ECB, IPX header and buffer
		 */

		ECB listenECB_R;
		ECB_Header& listenECB = listenECB_R;

		RealMemory<IPX_Header> listenIPXHeaderR;
		IPX_Header& listenIPX = listenIPXHeaderR;

		RealMemory<MyMessage> listenBufferR;
		MyMessage& listenBuffer = listenBufferR;

		memset(&listenECB, 0, sizeof(listenECB));
		memset(&listenIPX, 0, sizeof(listenIPX));

		listenECB.socket = socket;
		listenECB.fragment_count = 2;
		listenECB.hdr.ptr = &listenIPX;
		listenECB.hdr.size = sizeof(listenIPX);
		listenECB.buffer.ptr = &listenBuffer;
		listenECB.buffer.size = sizeof(listenBuffer);

		listenIPX.type = IPX_TYPE;

		/*
		 * Loop sending and receiving!
		 */


		UBYTE oldComplete = ecb.in_use;

		IPX_listenForPacket(&listenECB);

		clock_t ticks = clock();

		while(!kbhit())
		{
			if((ecb.in_use == 0) && (clock() > ticks))
			{
				ticks = clock() + 3 * CLOCKS_PER_SEC;		// Wait 3 seconds

				cout << "Sending packet" << endl;

				memset(ipxHeader.dest.node, 0xff, sizeof(ipxHeader.dest.node));
				memset(ecb.dest_address, 0xff, sizeof(ecb.dest_address));
				ipxHeader.dest.network = ourAddress.network;
				ipxHeader.dest.socket = socket;

				time_t theTime;
				time(&theTime);
				sprintf(buffer, "Hello from %02x%02x%02x%02x%02x%02x using TestIPX at %s",
					ourAddress.node[0],
					ourAddress.node[1],
					ourAddress.node[2],
					ourAddress.node[3],
					ourAddress.node[4],
					ourAddress.node[5],
					ctime(&theTime));

				IPX_sendPacket(&ecb);
			}

			UBYTE complete = ecb.in_use;
			if(complete != oldComplete)
			{
				oldComplete = complete;
				cout << "Completion bit: " << ecb.in_use << endl;
			}

			if(listenECB.in_use == 0)
			{
				if(memcmp(listenIPX.src.node, ourAddress.node, sizeof(ourAddress.node)) == 0)
					cout << "Packet from us" << endl;
				else
				{
					cout << "Packet received from: " << hex << setfill('0') <<
						setw(2) << (int)listenIPX.src.node[0] <<
						setw(2) << (int)listenIPX.src.node[1] <<
						setw(2) << (int)listenIPX.src.node[2] <<
						setw(2) << (int)listenIPX.src.node[3] <<
						setw(2) << (int)listenIPX.src.node[4] <<
						setw(2) << (int)listenIPX.src.node[5] <<
						" socket: " << setw(4) << listenIPX.src.socket <<
						dec << setfill(' ') <<
						" Network: " << listenIPX.src.network << endl;

					MyMessage* packet = (MyMessage*)((void*)listenECB.buffer.ptr);

					cout << (char*)packet << endl;

				}

				IPX_listenForPacket(&listenECB);
			}
		}

		IPX_closeSocket(socket);
	}
}
#endif

/*
 * Test the higher level Connection class
 */

void connectTest()
{
	IPXConnection connection;

	if(connection.connect() == 0)
	{
		clock_t ticks = clock();
		while(!kbhit())
		{
			Packet* incoming;
			while( (incoming = connection.receiveData()) != 0)
			{
				cout << "Type: " << incoming->type << ", " <<
						  "Length: " << incoming->length << ", " <<
						  "Data: " << incoming->data << endl;


				connection.releaseData(incoming);
			}

			if(connection.canSend() && (clock() > ticks))
			{
				ticks = clock() + 3 * CLOCKS_PER_SEC;

				connection.sync();

				cout << "Sending packet" << endl;

				time_t theTime;
				time(&theTime);
				char buffer[100];
				sprintf(buffer, "%02x%02x%02x%02x%02x%02x says the time is %s",
					connection.localAddress->node[0],
					connection.localAddress->node[1],
					connection.localAddress->node[2],
					connection.localAddress->node[3],
					connection.localAddress->node[4],
					connection.localAddress->node[5],
					ctime(&theTime));

				connection.sendData(Connect_TEST, (UBYTE*)buffer, strlen(buffer) + 1);
			}
		}

		connection.disconnect();
	}
}

#if 0
/*
 * Main function
 */

void usage()
{
	cout << "\nTestIPX n\n"
			  "n = 1 : rawTest\n"
			  "n = 2 : connectTest[default]\n" << endl;
}
#endif

#if 0 
void main(int argc, char* argv[])
#else
void main()
#endif
{
	Timer timer;

#if 0
	char function = '2';

	if(argc >= 2)
		function = argv[1][0];

	switch(function)
	{
	case '1':
		rawTest();
		break;
	case '2':
		connectTest();
		break;
	default:
		usage();
	}
#else
	connectTest();
#endif
}
