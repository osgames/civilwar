/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign AI: Miscellaneous Support functions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "aic_misc.h"
#include "ai_camp.h"
#include "unititer.h"
#include "campaign.h"
#include "campwld.h"
#include "combval.h"
#include "camptab.h"

/*-----------------------------------------------------------------
 * Support Functions
 */

/*
 * Calculate CV of all units on given side near a location
 */

CombatValue calc_nearCV(const Location& l, Distance d, Side side)
{
	CombatValue value = 0;

	UnitIter iter(campaign->world->ob.getPresident(side), False, True, Rank_Army, Rank_Brigade);
	while(iter.ok())
	{
		Unit* unit = iter.get();

		Distance d1 = distanceLocation(l, unit->location);
		if(d1 <= d)
		{
			CombatValue cv = calc_UnitCV(unit, True);
			value += cv;

#ifdef DEBUG1
			aiLog.printf("+%ld for %s", cv, unit->getName(True));
#endif
		}
	}

#ifdef DEBUG1
	aiLog.printf("calc_nearCV => %ld", (long) value);
#endif

	return value;
}

/*
 * Calculate CV of all units on given side ordered to move to a location
 * The order may be within a few miles.
 */

CombatValue calc_orderedCV(const Location& l, Side side)
{
	CombatValue value = 0;

	UnitIter iter(campaign->world->ob.getPresident(side), True, False, Rank_Army, Rank_Brigade);
	while(iter.ok())
	{
		Unit* unit = iter.get();

		Distance d1 = distanceLocation(l, unit->givenOrders.destination);
		if(d1 <= involvedBattleDistance)
		{
			CombatValue cv = calc_UnitCV(unit, False);
			value += cv;
#ifdef DEBUG
			aiLog.printf("+%ld for %s", cv, unit->getName(True));
#endif
		}
	}

#ifdef DEBUG
	aiLog.printf("calc_orderedCV => %ld", (long) value);
#endif

	return value;
}

/*
 * Find nearest facility on given side to a facility (not including itself)
 */

Facility* findCloseFacility(const Facility* home, Side side)
{
	FacilityList& fList = campaign->world->facilities;

	Facility* best = 0;

	Distance bestD = 0;

	for(int i = 0; i < fList.entries(); i++)
	{
		Facility* f = fList[i];

		if( (f->side == side) && (f != home) && f->isRealTown())
		{
			Distance d = distanceLocation(home->location, f->location);

			if(!best || (d < bestD))
			{
				bestD = d;
				best = f;
			}
		}
	}

	return best;
}
