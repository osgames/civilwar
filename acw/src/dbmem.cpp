/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Code that used to be in memory.h
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <string.h>
#include "dbmem.h"

/*
 * Global memory
 */

Memory memory;          		
Keyinfo keyinfo[28];			// Up to 28 keywords on screen at once

/*
 * 	Constructor
 */


retrace::retrace()
{
	const int max = 10;
	maxcount = max;

	count = 0;
	
	undo = new store [ maxcount ];
}

retrace::~retrace()
{
	delete [] undo;
}

/*
 * 	Store position in textfile;
 */


void retrace::push( char form, unsigned char NewsNum, int textoffset, unsigned short int chap, long int pages[] )
{
	int temp;
	
	if ( count == maxcount )
		{
			for ( char a = 1; a < maxcount; a++ )
				{
					undo[ a - 1 ].format = undo[ a ].format;
					undo[ a - 1 ].NewsNum = undo[ a ].NewsNum;
					undo[ a - 1 ].offset = undo[ a ].offset;
					undo[ a - 1 ].chapter = undo[ a ].chapter;
					
					for ( temp = 0; temp < maxTextpages; temp++ )
						{
							Newspaper[ a - 1 ][ temp ] = Newspaper[ a ][ temp ];
						}
				}
			count--;
		}
		
	undo[ count ].format = form;
	undo[ count ].NewsNum = NewsNum;
	undo[ count ].offset = textoffset;
	undo[ count ].chapter = chap;
	for ( temp = 0; temp < maxTextpages; temp++ )
		{
			Newspaper[ count ][ temp ] = pages[ temp ];
		}
	count++;
}

/*
 * 	Restore position in textfile;
 */


int retrace::pop ( char *form, unsigned char *NewsNum, unsigned short int *chap, long int pages[] )
{
	if ( count > 0 ) count--;
	*form = undo[ count ].format;
	*NewsNum = undo[ count ].NewsNum;
	*chap = undo[ count ].chapter;
	for ( char temp = 0; temp < maxTextpages; temp++ )
		{
			pages[ temp ] = Newspaper[ count ][ temp ];
		}

	return undo[ count ].offset;
}

long findkeyword( char *original, char key, long tsize )
{
	char loop;
	char length = strlen( keyinfo[ key].name );

	for ( long a = 0; a < tsize; a++ )
		{
		 if ( ( *(original + a ) == keyinfo[ key].name[0] ) || ( ( *(original + a ) + 32 ) == keyinfo[ key].name[0] ) )
			{
			 for ( loop = 1; loop < length; loop++ )
			 	{
				 if ( ( *( original + a + loop ) != keyinfo[ key ].name[ loop ] ) && ( ( *( original + a + loop ) + 32 ) != keyinfo[ key ].name[ loop ] ) && ( ( *( original + a + loop ) - 32 ) != keyinfo[ key ].name[ loop ] ) ) break;
				}
			 if ( ( loop == length ) && ( *(original + a + loop ) == '~' ) ) break;
			 	else loop = 1;
			}
		 }
	if ( loop == length ) return ( a + loop + 1 );

	return -1;

}
















