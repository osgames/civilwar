/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	LZHuf compression (File to File)
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/07/19  19:53:03  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "lzhuf.h"
#include "lzmisc.h"


/*
 * Class for compression
 */

class LZ_Encode : public LzHuf {
	/*
	 * LZSS
	 */

	UWORD   match_position;
	UWORD	match_length;

	WORD	lson[N+1];
	WORD	rson[N+1+N];
	WORD	dad[N+1];
	UBYTE	same[N+1];

	/*
	 * Huffman
	 */

	UWORD putbuf;
	UBYTE putlen;

public:
	FILE* infile;			// Where data is coming from
	FILE* outfile;			// Where data is going to
	UBYTE* inBuf;
	long inCount;
	UBYTE* outBuf;

	long codesize;			// Compressed size
	// long textsize;			// Uncompressed size
public:
	LZ_Encode();
	~LZ_Encode();

	void Encode ();
private:
	/*
	 * Huffman
	 */

	void Putcode (WORD l, UWORD c);
	void EncodeChar (UWORD c);
	void EncodePosition (UWORD c);
	void EncodeEnd ();

	/*
	 * LZ
	 */

	void InitTree ();
	void LZ_Encode::InsertNode (WORD r);
	void link (WORD n, WORD p, WORD q);
	void linknode (WORD p, WORD q, WORD r);
	void DeleteNode (WORD p);

	/*
	 * I/O
	 */

	void writeByte(UBYTE c);
	int readByte();

};

inline void LZ_Encode::writeByte(UBYTE c)
{
	if(outfile)
		putc(c, outfile);
	else if(outBuf)
		*outBuf++ = c;
	codesize++;
}

inline int LZ_Encode::readByte()
{
	if(inCount)
	{
		inCount--;

		if(infile)
			return getc(infile);
		else if(inBuf)
			return *inBuf++;
	}
	return EOF;
}

/*----------------------------------------------------------------------*/
/*                                                                      */
/*              LZSS ENCODING                                           */
/*                                                                      */
/*----------------------------------------------------------------------*/


/* Initialize Tree */
void LZ_Encode::InitTree () 
{
        register WORD *p, *e;

        for (p = rson + N + 1, e = rson + N + N; p <= e; )
                *p++ = NIL;
        for (p = dad, e = dad + N; p < e; )
                *p++ = NIL;
}


/* Insert to node */
void LZ_Encode::InsertNode (WORD r)
{
        register WORD            p;
        WORD                     cmp;
        register UBYTE  *key;
        register UWORD   c;
        register UWORD   i, j;

        cmp = 1;
        key = &text_buf[r];
        i = key[1] ^ key[2];
        i ^= i >> 4;
        p = N + 1 + key[0] + ((i & 0x0f) << 8);
        rson[r] = lson[r] = NIL;
        match_length = 0;
        i = j = 1;
        for ( ; ; ) {
                if (cmp >= 0) {
                        if (rson[p] != NIL) {
                                p = rson[p];
                                j = same[p];
                        } else {
                                rson[p] = r;
                                dad[r] = p;
                                same[r] = i;
                                return;
                        }
                } else {
                        if (lson[p] != NIL) {
                                p = lson[p];
                                j = same[p];
                        } else {
                                lson[p] = r;
                                dad[r] = p;
                                same[r] = i;
                                return;
                        }
                }

                if (i > j) {
                        i = j;
                        cmp = key[i] - text_buf[p + i];
                } else
                if (i == j) {
                        for (; i < F; i++)
                                if ((cmp = key[i] - text_buf[p + i]) != 0)
                                        break;
                }

                if (i > THRESHOLD) {
                        if (i > match_length) {
                                match_position = ((r - p) & (N - 1)) - 1;
                                if ((match_length = i) >= F)
                                        break;
                        } else
                        if (i == match_length) {
                                if ((c = ((r - p) & (N - 1)) - 1) < match_position) {
                                        match_position = c;
                                }
                        }
                }
        }
        same[r] = same[p];
        dad[r] = dad[p];
        lson[r] = lson[p];
        rson[r] = rson[p];
        dad[lson[p]] = r;
        dad[rson[p]] = r;
        if (rson[dad[p]] == p)
                rson[dad[p]] = r;
        else
                lson[dad[p]] = r;
        dad[p] = NIL;  /* remove p */
}

void LZ_Encode::link (WORD n, WORD p, WORD q)
{
        register UBYTE *s1, *s2, *s3;
        if (p >= NIL) {
                same[q] = 1;
                return;
        }
        s1 = text_buf + p + n;
        s2 = text_buf + q + n;
        s3 = text_buf + p + F;
        while (s1 < s3) {
                if (*s1++ != *s2++) {
                        same[q] = s1 - 1 - text_buf - p;
                        return;
                }
        }
        same[q] = F;
}


void LZ_Encode::linknode (WORD p, WORD q, WORD r)
{
        WORD cmp;

        if ((cmp = same[q] - same[r]) == 0) {
                link(same[q], p, r);
        } else if (cmp < 0) {
                same[r] = same[q];
        }
}

void LZ_Encode::DeleteNode (WORD p)
{
        register WORD  q;

        if (dad[p] == NIL)
                return;                 /* has no linked */
        if (rson[p] == NIL) {
                if ((q = lson[p]) != NIL)
                        linknode(dad[p], p, q);
        } else
        if (lson[p] == NIL) {
                q = rson[p];
                linknode(dad[p], p, q);
        } else {
                q = lson[p];
                if (rson[q] != NIL) {
                        do {
                                q = rson[q];
                        } while (rson[q] != NIL);
                        if (lson[q] != NIL)
                                linknode(dad[q], q, lson[q]);
                        link(1, q, lson[p]);
                        rson[dad[q]] = lson[q];
                        dad[lson[q]] = dad[q];
                        lson[q] = lson[p];
                        dad[lson[p]] = q;
                }
                link(1, dad[p], q);
                link(1, q, rson[p]);
                rson[q] = rson[p];
                dad[rson[p]] = q;
        }
        dad[q] = dad[p];
        if (rson[dad[p]] == p)
                rson[dad[p]] = q;
        else
                lson[dad[p]] = q;
        dad[p] = NIL;
}



/*----------------------------------------------------------------------*/
/*                                                                      */
/*              HUFFMAN ENCODING                                        */
/*                                                                      */
/*----------------------------------------------------------------------*/


/* TABLE OF ENCODE/DECODE for upper 6bits position information */

/* for encode */
static UBYTE p_len[64] = {
        0x03, 0x04, 0x04, 0x04, 0x05, 0x05, 0x05, 0x05,
        0x05, 0x05, 0x05, 0x05, 0x06, 0x06, 0x06, 0x06,
        0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06,
        0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
        0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
        0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
        0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08,
        0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08
};

static UBYTE p_code[64] = {
        0x00, 0x20, 0x30, 0x40, 0x50, 0x58, 0x60, 0x68,
        0x70, 0x78, 0x80, 0x88, 0x90, 0x94, 0x98, 0x9C,
        0xA0, 0xA4, 0xA8, 0xAC, 0xB0, 0xB4, 0xB8, 0xBC,
        0xC0, 0xC2, 0xC4, 0xC6, 0xC8, 0xCA, 0xCC, 0xCE,
        0xD0, 0xD2, 0xD4, 0xD6, 0xD8, 0xDA, 0xDC, 0xDE,
        0xE0, 0xE2, 0xE4, 0xE6, 0xE8, 0xEA, 0xEC, 0xEE,
        0xF0, 0xF1, 0xF2, 0xF3, 0xF4, 0xF5, 0xF6, 0xF7,
        0xF8, 0xF9, 0xFA, 0xFB, 0xFC, 0xFD, 0xFE, 0xFF
};

/* output C bits */

void LZ_Encode::Putcode (WORD l, UWORD c)
{
        register len = putlen;
        register UWORD b = putbuf;
        b |= c >> len;
        if ((len += l) >= 8) {
                writeByte(b >> 8);
                if ((len -= 8) >= 8) {
                        writeByte(b);
                        len -= 8;
                        b = c << (l - len);
                } else {
                        b <<= 8;
                }
        }
        putbuf = b;
        putlen = len;
}


LZ_Encode::LZ_Encode()
{
	putbuf = 0;
	putlen = 0;

	infile = 0;
	outfile = 0;

	inBuf = 0;
	outBuf = 0;
	inCount = 0;

	codesize = 0;
	// textSize = 0;

}

LZ_Encode::~LZ_Encode()
{
}

void LZ_Encode::EncodeChar (UWORD c)
{
        register WORD *p;
        register ULONG i;
        register WORD j, k;

        i = 0;
        j = 0;
        p = prnt;
        k = p[c + T];

        /* trace links from leaf node to root */
        do {
                i >>= 1;

                /* if node index is odd, trace larger of sons */
                if (k & 1) i += 0x80000000;

                j++;
        } while ((k = p[k]) != R) ;
        if (j > 16) {
                Putcode(16, (UWORD)(i >> 16));
                Putcode(j - 16, (UWORD)i);
        } else {
                Putcode(j, (UWORD)(i >> 16));
        }
        update(c);
}

void LZ_Encode::EncodePosition (UWORD c)
{
        UWORD i;

        /* output upper 6bit from table */
        i = c >> 6;
        Putcode((WORD)(p_len[i]), (UWORD)(p_code[i]) << 8);

        /* output lower 6 bit */
        Putcode(6, (UWORD)(c & 0x3f) << 10);
}

void LZ_Encode::EncodeEnd ()
{
        if (putlen) {
                writeByte(putbuf >> 8);
        }
}

void LZ_Encode::Encode ()
{
        register WORD  i, c, r, s, last_match_length;
		  int len;

        if(inCount == 0)
                return;

        // textsize = 0;
        StartHuff();
        InitTree();
        s = 0;
        r = N - F;
        for (i = s; i < r; i++)
                text_buf[i] = ' ';
        for (len = 0; len < F && (c = readByte()) != EOF; len++)
                text_buf[r + len] = c;
        // textsize = len;
        for (i = 1; i <= F; i++)
                InsertNode(r - i);
        InsertNode(r);

			do {
                if (match_length > len)
                        match_length = len;
                if (match_length <= THRESHOLD) {
                        match_length = 1;
                        EncodeChar(text_buf[r]);
                } else {
                        EncodeChar(255 - THRESHOLD + match_length);
                        EncodePosition(match_position);
                }
                last_match_length = match_length;
                for (i = 0; i < last_match_length &&
                                (c = readByte()) != EOF; i++) {
                        DeleteNode(s);
                        text_buf[s] = c;
                        if (s < F - 1)
                                text_buf[s + N] = c;
                        s = (s + 1) & (N - 1);
                        r = (r + 1) & (N - 1);
                        InsertNode(r);
                }

                // textsize += i;
                while (i++ < last_match_length) {
                        DeleteNode(s);
                        s = (s + 1) & (N - 1);
                        r = (r + 1) & (N - 1);
                        if (--len) InsertNode(r);
                }
        } while (len > 0);
        EncodeEnd();
}


/*
 * Entry Point
 */


long encode_lzhuf_ff (FILE* infp, FILE* outfp, long size)
{
	LZ_Encode* encodeInfo = new LZ_Encode;

	encodeInfo->infile = infp;
   encodeInfo->outfile = outfp;
   encodeInfo->inCount = size;
   encodeInfo->codesize = 0;
   encodeInfo->Encode ();
   long retval = encodeInfo->codesize;

	delete encodeInfo;

	return retval;
}

/*
 * Compress from memory to memory
 * Caller must provide a buffer to store the result!
 */

long encode_lzhuf_mm(UBYTE* src, UBYTE* dest, long size)
{
	LZ_Encode* encodeInfo = new LZ_Encode;

	encodeInfo->inBuf = src;
   encodeInfo->outBuf = dest;
   encodeInfo->inCount = size;
   // encodeInfo->textsize = size;
   encodeInfo->codesize = 0;
   encodeInfo->Encode ();
   long retval = encodeInfo->codesize;

	delete encodeInfo;

	return retval;
}

