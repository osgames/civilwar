/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Test Shapes
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1993/12/14  23:29:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/30  02:57:11  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "testshp.h"
#include "poly3d.h"

/*
 * A Plane covering the floor
 */

Point3D floorPoints[] = {
	Point3D(500, 0, 500),
	Point3D(500, 0, -500),
	Point3D(-500, 0, -500),
	Point3D(-500, 0, 500)
};

Edge floorEdges[] = {
	Edge(0, 1),
	Edge(1, 2),
	Edge(2, 3),
	Edge(3, 0)
};

EdgeFace floorFace1[] = { EdgeFace(3, R), EdgeFace(2, R), EdgeFace(1, R), EdgeFace(0, R) };	// Bottom
EdgeFace floorFace2[] = { EdgeFace(0, F), EdgeFace(1, F), EdgeFace(2, F), EdgeFace(3, F) };	// Top

Face floorFaces[] = {
	Face(4, floorFace1, Colour(0x50)),
	Face(4, floorFace2, Colour(0x51))
};

Poly3D floor(4, floorPoints, 4, floorEdges, 2, floorFaces);

/*
 * Define a 3D Shape (a simple 3 sided Pyramid)
 */

Point3D pyramidPoints[] = {
	Point3D(-50,  0,  50),		// Base Left Front
	Point3D(  0,  0, -50),		// Base Rear
	Point3D( 50,  0,  50),		// Base Right Front
	Point3D(  0,150,   0)		// Top
};

Edge pyramidEdges[] = {
	Edge(2, 0),
	Edge(0, 1),
	Edge(1, 2),
	Edge(0, 3),
	Edge(1, 3),
	Edge(2, 3)
};

EdgeFace face1[] = { EdgeFace(2, R), EdgeFace(1, R), EdgeFace(0, R) };
EdgeFace face2[] = { EdgeFace(4, F), EdgeFace(3, R), EdgeFace(1, F) };
EdgeFace face3[] = { EdgeFace(3, F), EdgeFace(5, R), EdgeFace(0, F) };
EdgeFace face4[] = { EdgeFace(5, F), EdgeFace(4, R), EdgeFace(2, F) };

Face pyramidFaces[] = {
	Face(3, face1, Colour(0xe3)),
	Face(3, face2, Colour(0xe0)),
	Face(3, face3, Colour(0xe1)),
	Face(3, face4, Colour(0xe2))
};

Poly3D pyramid(4, pyramidPoints, 6, pyramidEdges, 4, pyramidFaces);

WorldObject ob1(&pyramid, Position3D(Point3D(   0,  256,    0)));
WorldObject ob2(&pyramid, Position3D(Point3D( 200,  256,    0)));
WorldObject ob3(&pyramid, Position3D(Point3D(-500,  256,  200)));
WorldObject ob4(&pyramid, Position3D(Point3D(   0,  256, -200)));
WorldObject obFloor(&floor, Position3D(Point3D(0,0,0)));

