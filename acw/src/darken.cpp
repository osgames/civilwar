/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Create palette remap tables to darken, greyify, etc
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "darken.h"
#include "palette.h"
#include "system.h"
#include "screen.h"

class ColourTable {
	Colour* table;
public:
	ColourTable()
	{
		table = 0;
	}

	~ColourTable()
	{
		if(table)
			delete[] table;
	}

	virtual void makeTable(Colour* dest, const Palette* p) = 0;

	operator Colour* ()
	{
		if(!table)
		{
			table = new Colour[256];
			makeTable(table, machine.screen->getDeferredPalette());
		}
		return table;
	}
};


int cdiff(UBYTE v1, UBYTE v2)
{
	if(v1 > v2)
		return v1 - v2;
	else
		return v2 - v1;
}

int colourDiff(const TrueColour& c1, const TrueColour& c2)
{
	int rDiff = cdiff(c1.red, c2.red);
	int bDiff = cdiff(c1.blue, c2.blue);
	int gDiff = cdiff(c1.green, c2.green);

	return rDiff * rDiff + bDiff * bDiff + gDiff * gDiff;
}

UBYTE colourIntensity(const TrueColour& tc)
{
	UBYTE i = tc.red;
	if(tc.blue > i)
		i = tc.blue;
	if(tc.green > i)
		i = tc.green;
	return i;
}

class GreyTable : public ColourTable {
public:
	void makeTable(Colour* dest, const Palette* p);
};

void GreyTable::makeTable(Colour* dest, const Palette* src)
{
	const TrueColour* tc = src->colors;
	int count = 256;
	while(count--)
	{
		/*
		 * Get intensity wanted as the maximum of the primary colours
		 */

		UBYTE intensity = tc->red;
		if(tc->blue > intensity)
			intensity = tc->blue;
		if(tc->green > intensity)
			intensity = tc->green;

		/*
		 * Scan colours for closest match
		 */

		Colour bestColour = 0;
		int bestDiff = -1;

		for(int i = 0; i < 256; i++)
		{
			int newDiff = colourDiff(TrueColour(intensity, intensity, intensity), src->colors[i]);
			if( (bestDiff < 0) || (newDiff < bestDiff))
			{
				bestColour = i;
				bestDiff = newDiff;
			}
		}

		*dest++ = bestColour;
		tc++;
	}
}

class BrightTable : public ColourTable {
public:
	void makeTable(Colour* dest, const Palette* p);
};

void BrightTable::makeTable(Colour* dest, const Palette* src)
{
	const int brightness = 30;

	const TrueColour* tc = src->colors;
	for(int count = 0; count < 256; count++)
	{
		/*
		 * Get intensity wanted as the maximum of the primary colours
		 */

		UBYTE intensity = colourIntensity(*tc);
		
		/*
		 * Scan colours for closest match
		 */

		TrueColour wantC;

		int n = tc->red + brightness;
		if(n < 0xff)
			wantC.red = n;
		else
			wantC.red = 0xff;

		n = tc->green + brightness;
		if(n < 0xff)
			wantC.green = n;
		else
			wantC.green = 0xff;
		
		n = tc->blue + brightness;
		if(n < 0xff)
			wantC.blue = n;
		else
			wantC.blue = 0xff;


		Colour bestColour = count;
		int bestDiff = -1;

		for(int i = 0; i < 256; i++)
		{
		  	const TrueColour& c = src->colors[i];
		  
			if(colourIntensity(c) > intensity)
			{
				int newDiff = colourDiff(wantC, c);

				if( (bestDiff < 0) || (newDiff < bestDiff))
				{
					bestColour = i;
					bestDiff = newDiff;
				}
			}
		}

		*dest++ = bestColour;
		tc++;
	}
}


class Colorize : public ColourTable {
	Colour baseColour;
public:
	Colorize(Colour c) : ColourTable() { baseColour = c; }

	void makeTable(Colour* dest, const Palette* p);
};

void Colorize::makeTable(Colour* dest, const Palette* src)
{
	const TrueColour& bc = src->colors[baseColour];
	UBYTE baseI = colourIntensity(bc);

	const TrueColour* tc = src->colors;
	for(int count = 0; count < 256; count++)
	{
		/*
		 * Get intensity wanted as the maximum of the primary colours
		 */

		UBYTE intensity = colourIntensity(*tc);

		TrueColour newC;

#if 0
		newC.red = ((int) bc.red * intensity) / baseI;
		newC.green = ((int) bc.green * intensity) / baseI;
		newC.blue = ((int) bc.blue * intensity) / baseI;
#else
		newC.red = ((int) bc.red * intensity) / 255;
		newC.green = ((int) bc.green * intensity) / 255;
		newC.blue = ((int) bc.blue * intensity) / 255;
#endif
		
		/*
		 * Scan colours for closest match
		 */

		Colour bestColour = count;
		int bestDiff = -1;

		for(int i = 0; i < 256; i++)
		{
			const TrueColour& c = src->colors[i];

			int newDiff = colourDiff(newC, c);
			if( (bestDiff < 0) || (newDiff < bestDiff))
			{
				bestColour = i;
				bestDiff = newDiff;
			}
		}

		*dest++ = bestColour;
		tc++;
	}
}




void greyRegion(Region* region, const Rect& r)
{
	static GreyTable greyTable;

	region->remap(r, greyTable);
}


void brightenRegion(Region* region, const Rect& r)
{
	BrightTable brightTable;

	region->remap(r, brightTable);
}

void colourizeRegion(Region* region, const Rect& r, Colour c)
{
	Colorize table(c);
	
	region->remap(r, table);
}

