/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Units/Generals things that only concern the campaign game
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:54  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include "unitcamp.h"
#include "city.h"
#include "campaign.h"
#include "campwld.h"
#include "system.h"
#include "mapwind.h"
#include "colours.h"
#include "realism.h"
#include "game.h"
#include "sprlib.h"
#include "micelib.h"
#include "gamelib.h"
#include "options.h"

#if !defined(CAMPEDIT)
#include "route.h"
#endif

void makeNewRegiment(City* where, BasicRegimentType type, RegimentSubType subType)
{
	Side side = where->side;
	President* p = campaign->world->ob.getPresident(side);

	RegimentType rtype(type, subType);

	int regCount = where->allocateRegiment();

	/*
	 * Updated, so that names come from state instead of town
	 */

	const char* stateName;

	if(where->state == NoState)
		stateName = where->getNameNotNull();
	else
	{
		const State& state = campaign->world->states[where->state];
		stateName = state.fullName;
	}

	char name[200];
	sprintf(name, "%d%s %s",
		regCount,
		makeNths(regCount),
		stateName);

	Regiment* r = new Regiment(where->location, name, rtype);

	/*
	 * Set up random values for attributes
	 * (These ought to depend on the city size and type of unit).
	 */

	UnitInfo info(False, False);

	int maxMen;
	int minMen;

	switch(type)
	{
	case Infantry:
	default:
		maxMen = 1000;
		minMen = 500;
		break;

	case Cavalry:
		maxMen = 1000;
		minMen = 500;
		break;

	case Artillery:
		maxMen = 200;	// 1000;
		minMen = 100;	// 500;
		break;
	}

	info.strength 		= game->gameRand(minMen, maxMen);
	info.stragglers	= 0;
	info.casualties	= 0;
	info.fatigue		= 0;					// Fresh
	info.morale			= MaxAttribute;	// High

	info.speed			= YardsPerHour(game->gameRand(3520, 10560));	// 2..6 MPH

	info.supply			= MaxAttribute;

	info.experience	= 0;					// Untried

	r->setInfo(info);

	p->addFreeRegiment(r);
}

MapPriority Army::isVisible(MapWindow* map) const
{
	if((map->displayType == MapWindow::Disp_Moveable) && (seeAll || canBeSeen))
	{
		if(highlight)
			return Map_Highlight;
		else
			return Map_Army;
	}

	return Map_NotVisible;
}

void Army::mapDraw(MapWindow* map, Region* bm, Point where)
{
	if(map->displayType == MapWindow::Disp_Moveable)
		drawCampaignUnit(map, bm, where, SPR_ArmyFlag1);
}

MapPriority Corps::isVisible(MapWindow* map) const
{
	if(map->displayType == MapWindow::Disp_Moveable && (seeAll || canBeSeen) )
	{
		if(highlight)
			return Map_Highlight;
		else
			return Map_Corps;
	}
	return Map_NotVisible;
}

void Corps::mapDraw(MapWindow* map, Region* bm, Point where)
{
	if(map->displayType == MapWindow::Disp_Moveable)
		drawCampaignUnit(map, bm, where, SPR_CorpsFlag1);
}

MapPriority Division::isVisible(MapWindow* map) const
{
	if( (map->displayType == MapWindow::Disp_Moveable) &&
	    (seeAll || canBeSeen) &&
	    ( (map->mapStyle == MapWindow::Zoomed) || (getCampaignAttachMode() != Attached) || highlight) )
	{
		if(highlight)
			return Map_Highlight;
		else
			return Map_Division;
	}
	else
		return Map_NotVisible;
}

void Division::mapDraw(MapWindow* map, Region* bm, Point where)
{
	if( (map->displayType == MapWindow::Disp_Moveable) &&
		 (map->mapStyle == MapWindow::Zoomed) )
	{
		drawCampaignUnit(map, bm, where, SPR_DivisionFlag1);
	}
}

MapPriority Brigade::isVisible(MapWindow* map) const
{
	if((map->displayType == MapWindow::Disp_Moveable) && 
	   (canBeSeen || seeAll) &&
		( (map->mapStyle == MapWindow::Zoomed) || (getCampaignAttachMode() != Attached) || highlight) )
	{
		if(highlight)
			return Map_Highlight;
		else
			return Map_Brigade;
	}
	else
		return Map_NotVisible;
}


void Brigade::mapDraw(MapWindow* map, Region* bm, Point where)
{
	/*
	 * Draw as squares on map
	 */

	Colour col;

	Side side = getSide();

	if(side == SIDE_CSA)
		col = CSAColour;
	else if(side == SIDE_USA)
		col = USAColour;

#if 0
	if(map->mapStyle == MapWindow::Full)
		bm->plot(where, col);
		// bm->box(where.x, where.y, 1,1, col);
	else
		// bm->box(where.x-1, where.y-1, 3,3, col);
		bm->plot(where, col);
#else
	if(map->mapStyle == MapWindow::Zoomed)
		bm->plot(where, col);
#endif

	if( (getCampaignAttachMode() == Attached) && !highlight)
		return;

	/*
	 * Old method... just draw flag
	 */

	if( (map->displayType == MapWindow::Disp_Moveable) &&
		 (map->mapStyle == MapWindow::Zoomed) )
	{
		drawCampaignUnit(map, bm, where, SPR_BrigadeFlag1);
	}
}

MapPriority Regiment::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Regiment::mapDraw(MapWindow* map, Region* bm, Point where)
{
}


MapPriority Unit::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Unit::mapDraw(MapWindow* map, Region* bm, Point where)
{
#ifdef DEBUG
	throw GeneralError("virtual Unit::draw()");
#endif
}


void Unit::drawCampaignUnit(MapWindow* map, Region* bm, Point& where, UWORD icon)
{
	/*
	 * Draw destination if it is highlighted and has move orders
	 */

#if !defined(CAMPEDIT)

#ifdef CAMPEDIT
	if(highlight)
#else
	if(highlight && (seeAll || game->isPlayer(getSide())))
#endif
	{

		/*
		 * If it has orders to move
		 */

		// if(isCampaignReallyIndependant())	// && (moveMode == MoveMode_Marching))
		// if(isCampaignIndependant() || isJoining())	// && (moveMode == MoveMode_Marching))
		if(isCampaignDetached() || (getCampaignAttachMode() != Attached))
		{
			/*
			 * If moving by land then show line to destination
			 * If water or sea, then must highlight route
			 *
			 * If has been given new orders today
			 *  Show route of given orders
			 * Else if has orders in queue
			 *  Show route of queued order
			 * Else
			 *  If mode == Marching
			 *    Show route to 1st station
			 *  Else
			 *    Show route from current station
			 */


			Location finalDest;
			
			// if(isJoining())
			// if(!isCampaignIndependant())
			if(!isCampaignDetached())
				finalDest = superior->location;
			else
				finalDest = givenOrders.destination;

			if(givenOrders.how == ORDER_Land)
			{
				Point dest = map->locationToPixel(finalDest);

				/*
			 	 * Only draw if more than 8 pixels away
			 	 */

				if(distance(where.x - dest.x, where.y - dest.y) > 8)
				{
					/*
					 * Draw Route
					 */

					RoutePlan route;

					route.planRoute(location, finalDest, realOrders.how, getSide());
					route.mapDraw(map, bm, location, finalDest);

					/*
				 	 * Draw a cross at Destination
				 	 */

					bm->line(dest - Point(0,4), dest + Point(0,4), Colours(LRed));
					bm->line(dest - Point(4,0), dest + Point(4,0), Colours(LRed));

					/*
				 	 * Draw a line from Unit
				 	 */

					// bm->line(where, dest, Colours(Red));

				}
			}
			else
			{
				if((givenOrders.onFacility != NoFacility) && (givenOrders.offFacility != NoFacility))
				{
					Facility* f1 = campaign->world->facilities[givenOrders.onFacility];
					Facility* f2 = campaign->world->facilities[givenOrders.offFacility];

					Point p1 = map->locationToPixel(f1->location);
					Point p2 = map->locationToPixel(f2->location);

					/*
				 	 * Line from current location to onFacility
				 	 */

					if((moveMode == MoveMode_Marching) || (moveMode == MoveMode_Standing))
					{
						if(distance(where.x - p1.x, where.y - p1.y) > 8)
							bm->line(where, p1, Colours(Red));
					}

					/*
				 	 * Line from offFacility to destination
				 	 */

					Point dest = map->locationToPixel(finalDest);

					if(distance(p2.x - dest.x, p2.y - dest.y) > 8)
					{
						bm->line(dest - Point(0,4), dest + Point(0,4), Colours(LRed));
						bm->line(dest - Point(4,0), dest + Point(4,0), Colours(LRed));
						bm->line(dest, p2, LRed);
					}

					if(givenOrders.how == ORDER_Rail)		// Draw route!
					{
						RailRouteInfo* info = new RailRouteInfo;

						if( (campaign->world->findRailRoute(givenOrders.onFacility, givenOrders.offFacility, SIDE_None, info) != NoFacility) ||
							 (campaign->world->findRailRoute(givenOrders.onFacility, givenOrders.offFacility, getSide(), info) != NoFacility))
							info->drawRoute(map, bm, campaign->world->facilities, givenOrders.onFacility, givenOrders.offFacility);
					 
						delete info;
					}
					else if(givenOrders.how == ORDER_Water)		// Draw route!
					{
						WaterZone* z1 = &campaign->world->waterNet.zones[f1->waterZone];
						WaterZone* z2 = &campaign->world->waterNet.zones[f2->waterZone];
						
						if((moveMode == MoveMode_Marching) || (moveMode == MoveMode_Standing))
						{	// Line from onFacility to water zone
							Point dest = map->locationToPixel(z1->location);

							bm->line(dest, p1, LRed);
						}

						// Line from offFacility to water zone

						Point dest = map->locationToPixel(z2->location);
						bm->line(dest, p2, LRed);

				 		WaterRoute* info = new WaterRoute;

				 		if(campaign->world->waterNet.findRoute(f1->waterZone, f2->waterZone, True, 0, info) != NoWaterZone)
				 			info->drawRoute(map, bm, f1->waterZone, f2->waterZone);

				 		delete info;
					}

				}
			}
		}
	}
#endif	// CAMPEDIT

	if(getSide() == SIDE_CSA)
		icon += 2;
	// icon += getSide() * 2;

	if(highlight)
		icon++;

	game->sprites->drawAnchoredSprite(bm, where, GameSprites(icon));

	if((moveMode == MoveMode_Transport) && (realOrders.how == ORDER_Rail))
		machine.mouseLibrary->drawAnchoredSprite(bm, where, M_MoveRail);
	if((moveMode == MoveMode_Transport) && (realOrders.how == ORDER_Water))
		machine.mouseLibrary->drawAnchoredSprite(bm, where, M_MoveWater);
}

