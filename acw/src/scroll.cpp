/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Scroll Bar Icons
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.10  1994/09/23  13:28:28  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/06/29  21:38:52  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.7  1994/05/25  23:31:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/05/19  17:44:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/04/26  13:39:07  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/04/22  22:14:22  Steven_Green
 * Icon Handling tidied up.
 *
 * Revision 1.3  1994/04/11  13:36:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/03/21  21:03:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/03/17  14:29:31  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "scroll.h"
#include "system.h"
#include "screen.h"
#include "colours.h"
#include "hintstat.h"
#include "language.h"
#include "menudata.h"

#ifdef VERSION_116
#define Scroll_FillColour 0xf5
#define Scroll_BordColour 0xfe
#else
#define Scroll_FillColour 0x1d
#define Scroll_BordColour 0x5e
#endif

void ScrollBar::drawSlider(Region& bm, const Rect& r)
{
	bm.box(SDimension(r.x + 1), SDimension(r.y + 1), SDimension(r.getW() - 2), SDimension(r.getH() - 2), Scroll_FillColour);
	bm.frame(r.x, r.y, r.getW(), r.getH(), Scroll_BordColour);
}

ScrollBar::~ScrollBar()
{
}

/*
 * Generic Scroll Bar Icon
 */

class ScrollBarIcon : public SystemIcon {
	SystemSprite normal;
	SystemSprite pressed;

protected:
	ScrollBar* bar;

public:
	ScrollBarIcon(ScrollBar* set, Rect r, SystemSprite i1, SystemSprite i2, Key k = 0) :
		SystemIcon(set, i1, r, k)
	{
		bar = set;
		normal = i1;
		pressed = i2;
	}

	void setPressed() { setIcon(pressed); setRedraw(); }
	void setNormal() { setIcon(normal); setRedraw(); }

	void execute(Event* event, MenuData* d) { event=event; d = d; }
};

/*
 * Scroll Bar Region
 */

class ScrollMiddle : public Icon {
protected:
	ScrollBar* bar;

	SDimension top;			// Where the slider currently is
	SDimension bottom;

public:
	ScrollMiddle(ScrollBar* set, Rect r);
};

ScrollMiddle::ScrollMiddle(ScrollBar* set, Rect r) :
	Icon(set, r)
{
	bar = set;
	top = 0;
	bottom = 0;
}


class ScrollVBar : public ScrollMiddle {
public:
	ScrollVBar(ScrollBar* set, Rect r) : ScrollMiddle(set, r) { }

	void drawIcon();
	void execute(Event* event, MenuData* d);
};

class ScrollHBar : public ScrollMiddle {
public:
	ScrollHBar(ScrollBar* set, Rect r) : ScrollMiddle(set, r) { }

	void drawIcon();
	void execute(Event* event, MenuData* d);
};

void ScrollVBar::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	bm.fill(Scroll_FillColour);

	static Colour colours[] = {
		0x50, 0x1f, 0x1d, 0x18, 0x1a, 0x1a, 0x1a, 0x1a,
		0x1a, 0x1a, 0x1a, 0x1a, 0x1a, 0x1a, 0x1f, 0x1d, 0xf3, 0x1f
	};

	SDimension x = 0;
	while(x < bm.getW())
	{
		bm.VLine(x, 0, bm.getH(), colours[x]);
		x++;
	}

	bar->getSliderSize(top, bottom, getH());

	if(top != bottom)
	{
		SDimension vsize = SDimension(bottom - top);
		SDimension vposition = top;

#ifdef VERSION_116
		bar->drawSlider(bm, Rect(1, vposition, 13, vsize));
#else
		bar->drawSlider(bm, Rect(1, vposition, 14, vsize));
#endif
	}

	machine.screen->setUpdate(bm);
}

void ScrollHBar::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	bm.fill(Scroll_FillColour);

	static Colour colours[] = {
		0x50, 0x1f, 0x1d, 0x50,	0x1a, 0x1a,	0x1a,	0x1a,
		0x1a,	0x1a,	0x1a,	0x1a,	0x1a,	0x1a,	0x1a,	0x1a,
		0x1a, 0x1f, 0x1d, 0x50
	};										 

	SDimension y = 0;
	while(y < bm.getH())
	{
		bm.HLine(0, y, bm.getW(), colours[y]);
		y++;
	}

	bar->getSliderSize(top, bottom, getW());

	if(bottom != top)
	{
		SDimension hsize = SDimension(bottom - top);
		SDimension hposition = top;

#ifdef VERSION_116
		bar->drawSlider(bm, Rect(hposition, 2, hsize, 15));
#else
		bar->drawSlider(bm, Rect(hposition, 2, hsize, 16));
#endif
	}

	machine.screen->setUpdate(bm);
}

void ScrollVBar::execute(Event* event, MenuData* d)
{
#if 0
	if(event->overIcon)
	{
		SDimension y = event->where.getY();

		if(y < top)
			d->showHint( language( lge_shiftUp ) );
		else if(y < bottom)
		{
			// Drag slider
		}
		else
			d->showHint( language( lge_shiftDown ) );
	}
#endif

	if(event->buttons)
	{
		SDimension y = event->where.getY();

		if(y < top)
			bar->clickAdjust(+2);
		else if(y < bottom)
		{
			// Drag slider
		}
		else
			bar->clickAdjust(-2);
		setRedraw();
	}
}

void ScrollHBar::execute(Event* event, MenuData* d)
{
#if 0
	if(event->overIcon)
	{
		SDimension x = event->where.getX();

		if(x < top)	// really left
			d->showHint( language( lge_shiftLeft ) );
		else if(x < bottom)	// really right
		{
			// Drag slider
		}
		else
			d->showHint( language( lge_shiftRight ) );
	}
#endif

	if(event->buttons)
	{
		SDimension x = event->where.getX();

		if(x < top)	// really left
			bar->clickAdjust(-2);
		else if(x < bottom)	// really right
		{
			// Drag slider
		}
		else
			bar->clickAdjust(+2);
		setRedraw();
	}
}

/*
 * Individual little icons
 */

class ScrollEndIcon : public ScrollBarIcon {
	int adjustValue;
	LGE hintText;
public:
	ScrollEndIcon(ScrollBar* set, Rect r, SystemSprite icon, SystemSprite icon1, int adjust, Key key,  LGE hint);
	void execute(Event* event, MenuData* d);
};

ScrollEndIcon::ScrollEndIcon(ScrollBar* set, Rect r, SystemSprite icon, SystemSprite icon1, int adjust, Key key,  LGE hint) :
	ScrollBarIcon(set, r, icon, icon1, key)
{
	adjustValue = adjust;
	hintText = hint;
}

void ScrollEndIcon::execute(Event* event, MenuData* d)
{
	if(event->overIcon)
		d->showHint(language(hintText));

	if(event->buttons)
	{
		bar->clickAdjust(adjustValue);
		bar->setRedraw();
	}
}


#if 0
class ScrollUpIcon : public ScrollBarIcon {
public:
	ScrollUpIcon(ScrollBar* set, Point p, SystemSprite icon = SCR_UScroll) :
		ScrollBarIcon(set, Rect(p, Point(16,19)), icon, SCR_UScrollP, KEY_Up)
	{
	}

	void execute(Event* event, MenuData* d);
};

void ScrollUpIcon::execute(Event* event, MenuData* d)
{
#if 0
	if(event->overIcon)
		d->showHint( language( lge_scrollUp ) );
#endif

	if(event->buttons)
	{
		bar->clickAdjust(+1);
		bar->setRedraw();
	}
}



class ScrollDownIcon : public ScrollBarIcon {
public:
	ScrollDownIcon(ScrollBar* set, Point p, SystemSprite icon = SCR_DScroll) :
		ScrollBarIcon(set, Rect(p, Point(16,19)), icon, SCR_DScrollP, KEY_Down)
	{
	}

	void execute(Event* event, MenuData* d);
};

void ScrollDownIcon::execute(Event* event, MenuData* d)
{
#if 0
	if(event->overIcon)
		d->showHint( language( lge_scrollDown ) );
#endif

	if(event->buttons)
	{
		bar->clickAdjust(-1);
		bar->setRedraw();
	}
}


class ScrollLeftIcon : public ScrollBarIcon {
public:
	ScrollLeftIcon(ScrollBar* set, Point p, SystemSprite icon = SCR_LScroll) :
		ScrollBarIcon(set, Rect(p, Point(19,19)), icon, SCR_LScrollP, KEY_Left)
	{
	}

	void execute(Event* event, MenuData* d);
};

void ScrollLeftIcon::execute(Event* event, MenuData* d)
{
#if 0
	if(event->overIcon)
		d->showHint( language( lge_scrollLeft ) );
#endif

	if(event->buttons)
	{
		bar->clickAdjust(-1);
		bar->setRedraw();
	}
}

class ScrollRightIcon : public ScrollBarIcon {
public:
	ScrollRightIcon(ScrollBar* set, Point p, SystemSprite icon = SCR_RScroll) :
		ScrollBarIcon(set, Rect(p, Point(19,19)), icon, SCR_RScrollP, KEY_Right)
	{
	}

	void execute(Event* event, MenuData* d);
};

void ScrollRightIcon::execute(Event* event, MenuData* d)
{
#if 0
	if(event->overIcon)
		d->showHint( language( lge_scrollRight ) );
#endif

	if(event->buttons)
	{
		bar->clickAdjust(+1);
		bar->setRedraw();
	}
}


#endif

VerticalScrollBar::VerticalScrollBar(IconSet* parent, Rect r, SystemSprite upIcon, SystemSprite downIcon) :
	ScrollBar(parent, r)
{
	icons = new IconList[4];

	icons[0] = new ScrollEndIcon(this, Rect(0,0,16,19), SCR_UScroll, SCR_UScrollP, +1, KEY_Up, lge_scrollUp);
	icons[1] = new ScrollEndIcon(this, Rect(0,r.getH()-19,16,19), SCR_DScroll, SCR_DScrollP, -1, KEY_Down, lge_scrollDown);
	icons[2] = new ScrollVBar(this, Rect(0,19,16,SDimension(r.getH()-19-19)));	// slider area
	icons[3] = 0;
}



HorizontalScrollBar::HorizontalScrollBar(IconSet* parent, Rect r, SystemSprite leftIcon, SystemSprite rightIcon) :
	ScrollBar(parent, r)
{
	icons = new IconList[4];

	icons[0] = new ScrollEndIcon(this, Rect(0,0,19,19), leftIcon, SCR_LScrollP, -1, KEY_Left, lge_scrollLeft);
	icons[1] = new ScrollEndIcon(this, Rect(r.getW()-19,0,19,19), rightIcon, SCR_RScrollP, +1, KEY_Right, lge_scrollRight);
	icons[2] = new ScrollHBar(this, Rect(19,0,SDimension(r.getW()-19-19),19));	// slider area
	icons[3] = 0;
}


void MapBorder::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));

	bm.frame(0,0,getW(),getH(), 0x1f, 0x50);
	bm.box(1,1,getW()-2,getH()-2, 0x1d);

	machine.screen->setUpdate(bm);
}


