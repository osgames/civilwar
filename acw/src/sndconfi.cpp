/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	SoundCard Configuration File Strings
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "sndconfi.h"

char sc_fileName[]	= "sndsetup.dat";

char sc_title[]		= "[SoundCard]";

char sc_ID[]			= "ID";
char sc_version[]		= "VERSION";
char sc_name[] 		= "NAME";
char sc_ioPort[]		= "I/O";
char sc_dma[]			= "DMA";
char sc_irq[]			= "IRQ";
char sc_minRate[]		= "MINRATE";
char sc_maxRate[]		= "MAXRATE";
char sc_stereo[]		= "STEREO";
char sc_mixer[]		= "MIXER";
char sc_sampSize[]	= "SAMPLESIZE";
char sc_extra[]		= "EXTRA";

char sc_rate[]			= "RATE";
char sc_quality[]		= "QUALITY";
