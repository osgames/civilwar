/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Local Tree Generation.
 * These are trees that are generated temporarily over wooded terrain
 * areas on the map.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "batldata.h"
#include "staticob.h"
#include "map3d.h"
#include "random.h"

#if 0
ULONG swapLong(ULONG l)
{
#if 0
	return (l >> 16) + (l << 16);
#else
	union {
		ULONG l;
		struct {
			UWORD w1;
			UWORD w2;
		} w;
	} v;

	v.l = l;
	
	UWORD w;

	w = v.w.w1;
	v.w.w1 = v.w.w2;
	v.w.w2 = w;

	return v.l;
#endif
}
#else

ULONG swapLong(ULONG l);
#pragma aux swapLong =	\
	"mov dx,ax"				\
	"shr eax,10h"			\
	"shl edx,10h"			\
	"or eax,edx"			\
	parm [eax]				\
	value [eax]				\
	modify [eax edx]


#endif

/*
 * For now let us add a single tree in the middle of the display area!
 */

void BattleData::makeLocalTrees()
{
	// Grid* g = getGrid(data3D.view.zoomLevel);		// Work with current zoom level
	Grid* g = grid->getGrid(Zoom_Min);					// Work with current zoom level

	int gx = data3D.view.bigGridX;
	int gz = data3D.view.bigGridZ;

	Cord3D z = data3D.view.minZ;

#if !defined(BATEDIT)
	TerrainType* tp = g->getTSquare(gx, gz);
#endif

	int zCount = data3D.view.bigGridH;

	RandomNumber random;

	while(zCount--)
	{
#if defined(BATEDIT)
		MapGridCord gx1 = gx;
#else
		TerrainType* t1p = tp;
#endif
		Cord3D x = data3D.view.minX;
		int xCount = data3D.view.bigGridW;

		while(xCount--)
		{
#if defined(BATEDIT)
			TerrainType tType = g->getTerrain(gx1++, gz);
#else
			TerrainType tType = *t1p++;
#endif

			if(tType == CampSiteTerrain)
			{
				UBYTE buildChance[ZOOM_LEVELS] = {
					40,
					28,
					20,
					8,
					0,
#ifdef BATEDIT
					0,0,0
#endif
				};

				random.seed(x ^ swapLong(z));

				if( (random.getB() & 63) < buildChance[data3D.view.zoomLevel])
				{
					BattleLocation l(x, z);

					l.x += random.getL((ULONG) g->gridSize);
					l.z += random.getL((ULONG) g->gridSize);

					TreeObject* tob = new TreeObject(TentObject, l, 0);
					localTrees->add(tob);
				}
			}
			else
			{
				UBYTE wooded = data3D.terrain.isWooded(tType);

				if(wooded)
				{
					// See if we want a tree or not n/64 chance of building

					static UBYTE buildChance[ZOOM_LEVELS][4] = {
						{ 0, 20, 30, 40 },		// Half
						{ 0, 14, 21, 28 },		// 1
						{ 0, 10, 15, 20 },		// 2
						{ 0,  5,  8, 10 },		// 4
						{ 0,  3,  4,  5 },		// 8
#ifdef BATEDIT
						{ 0,  0,  0,  0 },		// 16
						{ 0,  0,  0,  0 },		// 32
						{ 0,  0,  0,  0 }			// 64
#endif
					};

					random.seed(x ^ swapLong(z));

					/*
				 	 * Reduce number of trees on zoomed out levels
				 	 */

					if( (random.getB() & 63) < buildChance[data3D.view.zoomLevel][wooded])
					{

						BattleLocation l(x, z);

						l.x += random.getL((ULONG) g->gridSize);
						l.z += random.getL((ULONG) g->gridSize);

						/*
					 	 * n/16 chance of tree being leafless in each month
					 	 */

						static UBYTE seasonAdjust[12] = {
							16, 15, 14, 10, 6, 0, 0, 2, 6, 8, 10, 15
						};

						int r = random.getB() & 0xf;

						StaticSpriteType spr;
						Boolean dead;

						if( (random.getB() & 15) < seasonAdjust[timeInfo.month])
							dead = True;
						
						switch(tType)
						{
						case HeavyOrchardTerrain:
							spr = dead ? Tree2Object : Tree5Object;
							break;
						case LightOrchardTerrain:
							spr = dead ? Tree3Object : Tree6Object;
							break;
						case HeavyWoodTerrain:
							spr = dead ? Tree1Object : Tree4Object;
							break;
						case LightWoodTerrain:
						default:
							spr = dead ? Tree2Object : Tree5Object;
							break;
						}

						TreeObject* tob = new TreeObject(spr, l, 0);
						localTrees->add(tob);
					}
				}
			}

			x += g->gridSize;
		}

		z += g->gridSize;
#if defined(BATEDIT)
		gz++;
#else
		tp += g->gridCount;
#endif
	}
}
