/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Class to Generate Random Numbers
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

// #define DEBUG_RANDOM

#include <stdlib.h>
#include <string.h>
#include "random.h"

#ifdef DEBUG_RANDOM
#include "clog.h"

LogFile randLog("random.log");
#endif

RandomNumber::RandomNumber(ULONG start)
{
	seed(start);
}

RandomNumber::RandomNumber()
{
	seed(rand() + (rand() << 8) + (rand() << 16) + (rand() << 24));
}

void RandomNumber::seed(ULONG start)
{
#ifdef DEBUG_RANDOM
	randLog.printf("Seed: %08lx", start);
#endif

	static UBYTE startValue[6] = {
		'D', 'A', 'G', 'G', 'E', 'R'
	};

	memcpy(value.bytes, startValue, sizeof(value.bytes));

	ULONG rev = 0;
	ULONG v = start;
	int i = 32;
	while(i--)
	{
		rev <<= 1;

		rev |= (v & 1);

		v >>= 1;
	}

	value.lw.lHi ^= start ^ rev;
	value.wl.lLo ^= start ^ rev;

	shuffle();
	shuffle();
}

inline void RandomNumber::shuffle()
{
	// value.lw.lHi += value.lw.lHi + value.wl.lLo ^ value.lw.lHi + 0x671de713;
	
	value.lw.lHi = value.wl.lLo - value.lw.lHi;
	value.lw.wLo ^= 0xe713;

	// + value.wl.lLo ^ value.lw.lHi + 0x671de713;

#ifdef DEBUG_RANDOM
	randLog.printf("Shuffle: %02x%02x%02x%02x%02x%02x",
		(int)value.bytes[0],	(int)value.bytes[1],
		(int)value.bytes[2],	(int)value.bytes[3],
		(int)value.bytes[4],	(int)value.bytes[5]);
#endif
}

ULONG RandomNumber::getL()
{
	shuffle();
	return value.lw.lHi;
}

UWORD RandomNumber::getW()
{
	shuffle();
	return value.words[0];
}

UBYTE RandomNumber::getB()
{
	shuffle();
	return value.bytes[0];
}

