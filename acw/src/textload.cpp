/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Load Text File
 *
 * Originally by Chris, but functions left in header file.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:54  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#ifndef strlen
#include<string.h>
#endif

#ifndef EOF
#include<stdio.h>
#endif

#include "textload.h"

#include "dialogue.h"
#include "cd_open.h"
#include "memptr.h"

void TextLoad::Textdistroy()
{
	if ( txt ) txt.close();
	delete [] textfile;
	delete [] texdata;
}

TextLoad::TextLoad( const char *filename )
{
	textfile = new char [ strlen ( filename ) + 1 ];
	
	for ( char loop = 0; loop <= strlen( filename ); loop++ )
	{
		textfile[loop] = filename[loop];
	}
		
	// txt.open( textfile, ios::in | ios::binary );
	
	fileOpen( txt, textfile, ios::in | ios::binary );

	error = 0;
	
	if ( !txt )
	{
		MemPtr<char> buffer(500);

		sprintf(buffer, "File\r\r%s\r\ris missing", filename);

		error = 1;
		dialAlert(0, buffer, "OK" );
		texdata = new char [ 1 ];
	}
	else
	{
		txt.seekg( 0, ios::end);
		tsize = txt.tellg ();
		txt.seekg( 0, ios::beg );    // restore read ptr position;
	
		texdata = new char [ tsize + 1 ];
	}
}



void TextLoad::getdata()
{

	if ( tsize > 200000 )
	{
		 cout << "Error: data size=" << tsize << " 2";
	}
	else
	{
		txt.read( texdata, tsize );
 
		error = txt.fail();
		
		texdata[ tsize ] = '\0';
 	}
}



