/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Terrain 
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/06/07  18:29:54  Steven_Green
 * Added a get grid function to get terrain about a given area
 *
 * Revision 1.2  1994/05/19  17:44:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/05/04  22:09:38  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <fstream.h>

#include "campter.h"
#include "measure.h"
#include "error.h"
#include "cd_open.h"
#include "myassert.h"

CampaignTerrain::CampaignTerrain()
{
	data = 0;
	linePtr = 0;
}

void CampaignTerrain::init(const char* fileName)
{
	ifstream in;
	
	fileOpen(in, fileName, ios::in | ios::binary | ios::nocreate);

	if(in)
	{
		TerrainHeaderF header;

		in.read((char*)&header, sizeof(header));

		width = getWord(&header.width);
		height = getWord(&header.height);
		size_t dataSize = getLong(&header.dataSize);

		data = new UBYTE[dataSize];
		linePtr = new UBYTE*[height];

		/*
		 * Read Line Pointers
		 */

		int lineCount = height;
		UBYTE** line = linePtr;
		ILONG iLong;
		while(lineCount--)
		{
			in.read((UBYTE*)&iLong, sizeof(iLong));
			*line++ = data + getLong(&iLong);
		}

		/*
		 * read data
		 */

		in.read(data, dataSize);



	}
	else
		throw GeneralError("Couldn't load Campaign Terrain file");
}

CampaignTerrain::~CampaignTerrain()
{
	delete data;
	delete linePtr;
}

/*
 * Extract a particular byte from an RLE buffer
 */

UBYTE getRLEbyte(UBYTE* buffer, int x)
{

	for(;;)
	{
		UBYTE b = *buffer++;
		if(b > 127)		// Byte run
		{
			int count = 257 - b;
			b = *buffer++;

			if(x < count)
				return b;
			else
				x -= count;
		}
		else				// Byte Copy
		{
			int count = b + 1;

			if(x < count)
				return buffer[x];
			else
			{
				buffer += count;
				x -= count;
			}
		}
	}
}


CTerrainVal CampaignTerrain::get(const Location& l)
{
	int line = l.y / (65536 * 5);
	int column = l.x / (65536 * 5);

	if((line >= 0) && (line < height) && (column >= 0) && (column < width))
	{
		UBYTE val = getRLEbyte(linePtr[line], column/2);

		if(column & 1)
			return CTerrainVal(val & 15);
		else
			return CTerrainVal((val >> 4) & 15);
	}
	else
		return CT_Impassable;

}

/*
 * This should be able to be speeded up quite considerably by
 * having a way of getting sequential nybbles from the RLE data
 */

CTerrainVal CampaignTerrain::getVal(int column, int line)
{
	if((line >= 0) && (line < height) && (column >= 0) && (column < width))
	{
		UBYTE val = getRLEbyte(linePtr[line], column/2);

		if(column & 1)
			return CTerrainVal(val & 15);
		else
			return CTerrainVal((val >> 4) & 15);
	}
	else
		return CT_Impassable;
}

/*
 * Fill an array sizeXsize with grid values
 */

void CampaignTerrain::makeGrid(CTerrainVal* grid, const Location& l, int size)
{
	int line = l.y / (65536 * 5) - size/2;
	int column = l.x / (65536 * 5) - size/2;

	int y = size;
	while(y--)
	{
		int col = column;
		int x = size;
		while(x--)
		{
			*grid++ = getVal(col, line);
			col++;
		}
		line++;
	}
}


void CampaignTerrain::locationToCampTCord(CampaignTerrainCoord& g, const Location& l)
{
	ASSERT(l.x >= 0);
	ASSERT(l.y >= 0);
	ASSERT(l.x < (width * 65536 * 5));
	ASSERT(l.y < (height * 65536 * 5));

	g.x = l.x / (65536 * 5);
	g.y = l.y / (65536 * 5);

	ASSERT(g.y >= 0);
	ASSERT(g.y < height);
	ASSERT(g.x >= 0);
	ASSERT(g.x < width);
}

/*
 * Convert Campaign coordinates into world coordinates
 *
 * adjusted to center of square
 */

void CampaignTerrain::campTCordToLocation(Location& l, const CampaignTerrainCoord& g)
{
	ASSERT(g.y >= 0);
	ASSERT(g.y < height);
	ASSERT(g.x >= 0);
	ASSERT(g.x < width);

	l.x = g.x * (65536 * 5) + (65536 * 5) / 2;
	l.y = g.y * (65536 * 5) + (65536 * 5) / 2;
}


