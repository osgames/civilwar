/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	ZBuffer based Polygon Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:54  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.20  1994/08/31  15:23:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.19  1994/08/24  15:05:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.18  1994/08/19  17:29:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.17  1994/08/09  21:29:58  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.16  1994/06/24  14:43:30  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.14  1994/04/05  12:28:09  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1993/12/16  22:19:35  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1993/12/15  20:56:36  Steven_Green
 * Texturing works
 *
 * Revision 1.11  1993/12/14  23:29:01  Steven_Green
 * Modifications to prepare for textured polygons
 *
 * Revision 1.10  1993/12/13  17:11:43  Steven_Green
 * Fast HLine used if colours same (not just intensities).
 *
 * Revision 1.9  1993/12/13  14:09:20  Steven_Green
 * Gourad Polygon Rendering debugged
 *
 * Revision 1.8  1993/12/10  16:06:39  Steven_Green
 * Gourad shading added
 *
 * Revision 1.7  1993/12/04  16:22:51  Steven_Green
 * Exceptions added for running out of memory.
 *
 * Revision 1.6  1993/12/01  15:11:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1993/11/30  02:57:11  Steven_Green
 * Some attempted optimisation
 *
 * Revision 1.4  1993/11/24  09:32:52  Steven_Green
 * Combination of 2 and 3D polygons.
 * Removed clipping to seperate file.
 *
 * Revision 1.3  1993/11/19  18:59:26  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/11/16  22:42:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/05  16:50:53  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "zpoly.h"
#include "image.h"
#include "terrain.h"
#include "data3d.h"

#ifdef DEBUG
#include "options.h"
#include "mouse.h"
#endif


/*
 * Scan line class for storing min/max values
 */

struct ScanPoint {
	SDimension x;
	Cord3D z;
	Intensity i;
};

class Scan {
	SDimension yTop;				// Actual Y coordinate after clipping
	SDimension lineCount;		// Actual number of lines after clipping

	ScanPoint* left;	 
	ScanPoint* right;

public:
			Scan(SDimension y1, SDimension y2);
		  ~Scan();
	void			addPoint(const Point3DI& p);
	void			addLine(const Point3DI& p1, const Point3DI& p2);
	void 			draw(System3D& drawData, TerrainType material) const;

};


Scan::Scan(SDimension y1, SDimension y2)
{
	yTop = y1;
	lineCount = y2 - y1 + 1;


	left = new ScanPoint[lineCount * 2];
	right = left + lineCount;

	ScanPoint* l = left;
	ScanPoint* r = right;

	int i = lineCount;
	while(i--)
	{
		l++->x = maxSDimension;
		r++->x = 0;
	}

}

Scan::~Scan()
{
	delete[] left;
}

/*
 * Update a scan line
 */

void Scan::addPoint(const Point3DI& p)
{
	if(p.y >= yTop)
	{
		int line = p.y - yTop;

		if(line < lineCount)
		{
			ScanPoint& l = left[line];
			ScanPoint& r = right[line];

			if(p.x < l.x)
			{
				l.x = p.x;
				l.z = p.z;
				l.i = p.intensity;
			}

			if(p.x > r.x)
			{
				r.x = p.x;
				r.z = p.z;
				r.i = p.intensity;
			}
		}
	}
}

/*
 * Draw a line into the scanline buffer
 */

void Scan::addLine(const Point3DI& p1, const Point3DI& p2)
{
	/*
	 * Force "from" to be left of "to" to ensure the same pixels are
	 * drawn regardless of which direction it is drawn from
	 */

	Point3DI from;
	Point3DI to;

	if(p1.x < p2.x)
	{
		from = p1;
		to = p2;
	}
	else
	{
		from = p2;
		to = p1;
	}

	Point3DI diff = to - from;
	Point3DI absDiff = diff.abs();
	Point3DI dir = diff.sign();

	// ScanLine* line = get(from.y);
	// addPoint(line, from);
	addPoint(from);

	/*
	 * Which is major axis?
	 */

	if(absDiff.x > absDiff.y)
	{
		// X is main axis

		if(from.x != to.x)
		{
			int value = 0x8000;
			int add = (absDiff.y * 0x10000) / absDiff.x;

			int iSlope = diff.intensity / (absDiff.x + 1);		// Always add this
			int iRem = absDiff.intensity % (absDiff.x + 1);		// use for bresenham
			int iVal = iRem/2;	// Setting it to half automatically rounds up numbers

			// Update Z Cordinate code!

			int zSlope = diff.z / (absDiff.x + 1);
			int zRem = absDiff.z % (absDiff.x + 1);
			int zVal = zRem/2;

			while(from.x != to.x)
			{
				/*
				 * Alter Y
				 */

				value += add;
				if(value >= 0x10000)
				{
					from.y += dir.y;
					// line += dir.y;
					value -= 0x10000;
				}

				/*
				 * Alter Intensity
				 */

				from.intensity += iSlope;

				iVal += iRem;
				if(iVal >= absDiff.x)
				{
					from.intensity += dir.intensity;
					iVal -= absDiff.x;
				}

				/*
				 * Alter Z
				 */

				from.z += zSlope;
				zVal += zRem;
				if(zVal >= absDiff.x)
				{
					from.z += dir.z;
					zVal -= absDiff.x;
				}

				from.x += dir.x;

				// addPoint(line, from);
				addPoint(from);
			}
		}
	}
	else
	{
		// Y is main axis

		if(from.y != to.y)
		{
			int value = 0x8000;
			int add = (absDiff.x * 0x10000) / absDiff.y;

			int iSlope = diff.intensity / (absDiff.y + 1);
			int iRem = absDiff.intensity % (absDiff.y + 1);
			int iVal = iRem/2;

			int zSlope = diff.z / (absDiff.y + 1);
			int zRem = absDiff.z % (absDiff.y + 1);
			int zVal = zRem/2;

			while(from.y != to.y)
			{
				value += add;
				if(value >= 0x10000)
				{
					from.x += dir.x;
					value -= 0x10000;
				}

				from.y += dir.y;
				// line += dir.y;

				/*
				 * Alter Intensity
				 */

				from.intensity += iSlope;
				iVal += iRem;
				if(iVal >= absDiff.y)
				{
					from.intensity += dir.intensity;
					iVal -= absDiff.y;
				}

				/*
				 * Alter Z
				 */

				from.z += zSlope;
				zVal += zRem;
				if(zVal >= absDiff.y)
				{
					from.z += dir.z;
					zVal -= absDiff.y;
				}


				// addPoint(line, from);
				addPoint(from);
			}
		}
	}
}

/*
 * add scan lines to the global scan buffer
 */

void Scan::draw(System3D& drawData, TerrainType material) const
{
	const ScanPoint* l = left;
	const ScanPoint* r = right;

	int count = lineCount;
	Cord3D y = yTop;

	while(count--)
	{
		if(l->x <= r->x)
		{
#ifdef BATEDIT
			Boolean firstLine = ((count == 0) || (count == (lineCount - 1)));
#endif
			/*
			 * Simple clipping
			 */

			SDimension x1 = l->x;
			SDimension x2 = r->x;

			if(x1 < 0)
				x1 = 0;
			if(x2 >= drawData.bitmap->getW())
				x2 = drawData.bitmap->getW() - 1;

			if( (x1 < drawData.bitmap->getW()) && (x2 >= 0))
			{
				ZSIZE* zBuf = &drawData.zBuffer.get(x1, y);
				UBYTE* drawBuf = drawData.bitmap->getAddress(x1, y);

				const Image* texture = drawData.terrain.getTexture(material);
				TerrainCol* col = drawData.terrain.getColour(material);

				SDimension length = x2 - x1 + 1;
				ZSIZE z = zToScanZ(l->z);

				/*
				 * Initialise texture variables
				 */

				SDimension tw = texture->getWidth();
				UBYTE tMask = tw - 1;		// Assume texture is ^2 wide

				SDimension tx = x1 & tMask;
				SDimension ty = SDimension(y % texture->getHeight());
				UBYTE* tline = texture->getAddress(0, ty);

				/*
	 			 * Normalise intensities to given range
	 			 */

				Intensity i1 = l->i - col->minVol;
				Intensity i2 = r->i - col->minVol;

				int iRange = col->maxVol - col->minVol;
				int cRange = col->colourRange - 3;		// Allow 3 colours for texture

				Colour c1;
				Colour c2;
 
				if(iRange == 0)
				{
					c1 = 0;
					c2 = 0;
				}
				else if(cRange == 13)		// 13 is most common configuration!
				{
					c1 = Colour((i1 * 13) / iRange);	// Let compiler optimise this to shifts
					c2 = Colour((i2 * 13) / iRange);
				}
				else
				{
					c1 = Colour((i1 * cRange) / iRange);
					c2 = Colour((i2 * cRange) / iRange);
				}

				Colour c = Colour(c1 + col->baseColour);

#ifdef DEBUG
				if(optTexture())
				{
#endif
					if(c1 == c2)
					{
						while(length--)
						{
#ifdef BATEDIT
							if(firstLine || (length == 0))
								z--;
#endif
							if(z < *zBuf)
							{
#ifdef BATEDIT
								if(firstLine|| (length == 0))
								{
									*drawBuf = 0xf0;	// Black line at edge of polygon
								}
								else
#endif
								*drawBuf = c + tline[tx];
								*zBuf = z;
							}
							zBuf++;
							drawBuf++;

							tx = (tx + 1) & tMask;

							// Adjust Z...
						}
					}
					else
					{
	
						int ir = i1 - (c1 * iRange) / cRange;		// intensity remainder

						Colour dCol;
						Intensity dI;
						int change;

						if(c2 > c1)		// Increase in colour
						{
							dCol = Colour(c2 - c1);
							dI = Intensity(i2 - i1);
							change = 1;
						}
						else
						{
							dCol = Colour(c1 - c2);
							dI = Intensity(i1 - i2);
							change = -1;
						}


						/*
						 * NOTE: This can be speeded up by stepping along the
						 * axis, and using div, remainder on the colour
						 */

						if(dCol > length)		// Colour change is major axis
						{
							int top = dI * cRange;
							int bottom = iRange * length;

#if 0
							int val = top / 2;

							while(length)
							{
								val += bottom;

								if(val >= top)
								{
									val -= top;
									length--;

#ifdef BATEDIT
									if(firstLine || (length == 0))
										z--;
#endif
									if(z < *zBuf)
									{
#ifdef BATEDIT
										if(firstLine || (length == 0))
										{
											*drawBuf = 0xf0;	// Black line at edge of polygon
										}
										else
#endif
										*drawBuf = c + tline[tx];
										*zBuf = z;
									}
									zBuf++;
									drawBuf++;

									tx = (tx + 1) & tMask;

									// Adjust Z...
								}

								c += change;
							}
#else			
							/*
							 * Speeded up version
							 */

							int iDiv = top / bottom;
							int iRem = top % bottom;
							int iVal = bottom / 2;

							if(change < 0)
								iDiv = -iDiv;

							while(length--)
							{
#ifdef BATEDIT
								if(firstLine || (length == 0))
									z--;
#endif
								if(z < *zBuf)
								{
#ifdef BATEDIT
									if(firstLine || (length == 0))
									{
										*drawBuf = 0xf0;	// Black line at edge of polygon
									}
									else
#endif
									*drawBuf = c + tline[tx];
									*zBuf = z;
								}
								zBuf++;
								drawBuf++;

								tx = (tx + 1) & tMask;

								/*
								 * Adjust i
								 */

								c += iDiv;
								iVal += iRem;
								if(iVal >= bottom)
								{
									iVal -= bottom;
									c += change;
								}

							}


#endif
						}
						else						// Length is major axis
						{
							int top = dI * cRange;
							int bottom = iRange * length;

							int val;
		
							if(c2 > c1)
								val = ir * length * cRange;
							else
								val = bottom - ir * length * cRange;

							while(length--)
							{
								val += top;

								if(val >= bottom)
								{
									val -= bottom;
									c += change;
								}

#ifdef BATEDIT
								if(firstLine || (length == 0))
									z--;
#endif
								if(z < *zBuf)
								{
#ifdef BATEDIT
									if(firstLine || (length == 0))
									{
										*drawBuf = 0xf0;	// Black line at edge of polygon
									}
									else
#endif
									*drawBuf = c + tline[tx];
									*zBuf = z;
								}
								zBuf++;
								drawBuf++;

								tx = (tx + 1) & tMask;

								// Adjust Z...
							}
						}
					}
#ifdef DEBUG
				}
			 	else // Not Textured
			 	{
					if(c1 == c2)
					{
						while(length--)
						{
#ifdef BATEDIT
							if(firstLine || (length == 0))
								z--;
#endif
							if(z < *zBuf)
							{
#ifdef BATEDIT
								if(firstLine || (length == 0))
								{
									*drawBuf = 0xf0;	// Black line at edge of polygon
								}
								else
#endif
								*drawBuf = c;
								*zBuf = z;
							}
							zBuf++;
							drawBuf++;

							// Adjust Z...
						}
					}
					else
					{
	
						int ir = i1 - (c1 * iRange) / cRange;		// intensity remainder

						Colour dCol;
						Intensity dI;
						int change;

						if(c2 > c1)		// Increase in colour
						{
							dCol = Colour(c2 - c1);
							dI = Intensity(i2 - i1);
							change = 1;
						}
						else
						{
							dCol = Colour(c1 - c2);
							dI = Intensity(i1 - i2);
							change = -1;
						}


						/*
						 * NOTE: This can be speeded up by stepping along the
						 * axis, and using div, remainder on the colour
						 */

						if(dCol > length)		// Colour change is major axis
						{
							int top = dI * cRange;
							int bottom = iRange * length;

							int val = top / 2;

							while(length)
							{
								val += bottom;

								if(val >= top)
								{
									val -= top;
									length--;

#ifdef BATEDIT
									if(firstLine || (length == 0))
										z--;
#endif
									if(z < *zBuf)
									{
#ifdef BATEDIT
										if(firstLine || (length == 0))
										{
											*drawBuf = 0xf0;	// Black line at edge of polygon
										}
										else
#endif
										*drawBuf = c;
										*zBuf = z;
									}
									zBuf++;
									drawBuf++;


									// Adjust Z...
								}

								c += change;
							}
						}
						else						// Length is major axis
						{
							int top = dI * cRange;
							int bottom = iRange * length;

							int val;
		
							if(c2 > c1)
								val = ir * length * cRange;
							else
								val = bottom - ir * length * cRange;

							while(length--)
							{
								val += top;

								if(val >= bottom)
								{
									val -= bottom;
									c += change;
								}

#ifdef BATEDIT
								if(firstLine || (length == 0))
									z--;
#endif
								if(z < *zBuf)
								{
#ifdef BATEDIT
									if(firstLine || (length == 0))
									{
										*drawBuf = 0xf0;	// Black line at edge of polygon
									}
									else
#endif
									*drawBuf = c;
									*zBuf = z;
								}
								zBuf++;
								drawBuf++;

								// Adjust Z...
							}
						}
					}
				}
#endif	// DEBUG using texture options
			}
		}

		l++;
		r++;
		y++;
	}
}


/*
 * Take a triangle and make it into scan lines
 *
 * Do a quick back side test and throw it out
 * If the triangle goes over the edge of the viewing area then clip
 * it into several triangles.
 */

void System3D::renderTriangle(const Point3DI& p1, const Point3DI& p2, const Point3DI& p3, TerrainType ter)
{
	/*
	 * Do a cross product and remove if back face
	 */

	Point3D n;

	cross(n, p3 - p1, p2 - p1);
	if(n.z < 0)
		return;

	// clipTriangle(p1, p2, p3, ter);

	triangles.add(p1, p2, p3, ter);

	TerrainCol* t = terrain.getColour(ter);

	t->addIntensity(p1.intensity);
	t->addIntensity(p2.intensity);
	t->addIntensity(p3.intensity);
}

void System3D::drawTriangle(const Point3DI& p1, const Point3DI& p2, const Point3DI& p3, TerrainType ter)
{
	/*
	 * Get the min/max Y coordinates
	 */

	SDimension minY = p1.y;
	SDimension maxY = p1.y;

	if(p2.y < minY)
		minY = p2.y;
	if(p2.y > maxY)
		maxY = p2.y;
	if(p3.y < minY)
		minY = p3.y;
	if(p3.y > maxY)
		maxY = p3.y;

	/*
	 * Ignore cases where triangle is all off screen!
	 */

	if(maxY < 0)
		return;
	if(minY >= bitmap->getH())
		return;

	/*
	 * Clip to screen
	 */

	if(maxY >= bitmap->getH())
		maxY = bitmap->getH() - 1;
	if(minY < 0)
		minY = 0;

	Scan scanLines(minY, maxY);

	scanLines.addLine(p1, p2);
	scanLines.addLine(p2, p3);
	scanLines.addLine(p3, p1);

	scanLines.draw(*this, ter);
}

void System3D::renderSquare(const Point3DI& p1, const Point3DI& p2, const Point3DI& p3, const Point3DI& p4, TerrainType terrain)
{
	renderTriangle(p3, p2, p1, terrain);
	renderTriangle(p2, p3, p4, terrain);
}



void TriangleList::add(const Point3DI& p1, const Point3DI& p2, const Point3DI& p3, TerrainType ter)
{
	TriangleElement* el = new TriangleElement;

	el->points[0] = p1;
	el->points[1] = p2;
	el->points[2] = p3;
	el->terrain = ter;

	el->next = entry;
	entry = el;
}


void System3D::render()
{
	terrain.makeColours();
	terrain.setPalette();
	zBuffer.clear();

	TriangleElement* el;

	while( (el = triangles.get()) != 0)
	{
		drawTriangle(el->points[0], el->points[1], el->points[2], el->terrain);
		delete el;
	}
}

