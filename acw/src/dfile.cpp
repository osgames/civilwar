/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Binary Data File for Rally Around the Flag
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <string.h>
#include <stdarg.h>
#include "dfile.h"
#include "dialogue.h"
#include "strutil.h"
#include "cd_open.h"
#include "language.h"
#include "system.h"
#include "mouselib.h"

static const UWORD ChunkHeaderVersion = 0x0001;

/*=========================================================
 * Base Class Functions
 */

ChunkFileBase::ChunkFileBase()
{
	fp = 0;
	lastError = CE_OK;
	oldPointer = machine.mouse->setPointer(M_Busy);
}

ChunkFileBase::~ChunkFileBase()
{
	if(fp)
		fclose(fp);
	machine.mouse->setPointer(oldPointer);
}

ChunkError ChunkFileBase::getError() const
{
	return lastError;
}

Boolean ChunkFileBase::isGood() const
{
	return lastError == CE_OK;
}

void ChunkFileBase::setError(ChunkError err)
{
	lastError = err;

	if(fp)
	{
		fclose(fp);
		fp = 0;
	}

	throw ChunkFileError();
}

/*
 * Set the file position
 */

ChunkError ChunkFileBase::setPos(ChunkPos pos)
{
	if(isGood())
	{
		if(fsetpos(fp, &pos) == 0)
			return CE_OK;
		setError(CE_GeneralError);
	}

	return getError();
}

ChunkPos ChunkFileBase::getPos()
{
	if(isGood())
	{
		fpos_t pos;

		if(fgetpos(fp, &pos) == 0)
			return pos;
	}
	
	setError(CE_GeneralError);

	return -1;
}

ChunkFileBase::ChunkFileError::ChunkFileError(const char* fmt, ...)
{
	char buffer[500];

	va_list vaList;
	va_start(vaList, fmt);
	_vbprintf(buffer, 500, fmt, vaList);
	va_end(vaList);

	setMessage(buffer);
}

/*=========================================================
 * FileRead Functions
 */

ChunkFileRead::ChunkFileRead(const char* fileName, FileType wantType)
{
	fp = fopen( getPathAndCheck( fileName ), "rb");

	if(!fp)
	{
		setError(CE_GeneralError);
		return;
	}

	/*
	 * Read in the directory....
	 */

	D_FileHeader header;

	read(&header, sizeof(header));
	if(getLong(&header.id) != RRFC_ID)
		throw ChunkFileError("%s is not an RRFC file", fileName);

	/*
	 * See if its an old version file (description goes where version is)
	 */

	if(getWord(&header.headerVersion) > 0x2000)
	{
		rewind(fp);

		D_FileHeader_00 head_00;

		read(&head_00, sizeof(head_00));
		if(getLong(&head_00.id) != RRFC_ID)
			throw ChunkFileError("%s is not an RRFC file", fileName);

		nChunks = getWord(&head_00.nChunks);
		fileVersion = getWord(&head_00.version);
		id = copyString(head_00.name);
		fileType = wantType;
	}
	else
	{
		nChunks = getWord(&header.nChunks);
		fileVersion = getWord(&header.version);
		id = copyString(header.name);
		fileType = FileType(getWord(&header.fileType));

		if( (wantType != FT_DontCare) && (fileType != wantType))
			throw ChunkFileError("%s is wrong type (%d instead of %d)",
				fileName,
				fileType,
				wantType);
	}

	if(!nChunks)
		throw ChunkFileError("%s has no chunks", fileName);

	dir = new ChunkDir[nChunks];

	for(int i = 0; i < nChunks; i++)
	{
		D_ChunkDir data;

		read(&data, sizeof(data));

		dir[i].id = getLong(&data.id);
		dir[i].pos = getLong(&data.pos);
		dir[i].length = getLong(&data.length);
	}
}

/*
 * Destructor
 */

ChunkFileRead::~ChunkFileRead()
{
	delete[] dir;
	delete[] id;
}

/*
 * Move to start of desired chunk
 */

ChunkError ChunkFileRead::startReadChunk(ChunkID id)
{
	if(isGood())
	{
		/*
		 * Scan dir for correct chunk
		 */

		for(int i = 0; i < nChunks; i++)
		{
			if(dir[i].id == id)
			{
				setPos(dir[i].pos);
				return getError();
			}
		}

		throw ChunkFileError("There is no chunk of type '%c%c%c%c'",
			(char) id >> 24,
			(char) id >> 16,
			(char) id >> 8,
			(char) id);

	}

	return getError();
}

/*
 * Read some data
 */

ChunkError ChunkFileRead::read(void* ad, size_t size)
{
	if(isGood())
	{
		if(fread(ad, size, 1, fp) != 1)
			setError(CE_GeneralError);
	}

	return getError();
}

char* ChunkFileRead::readString()
{
	char* s = 0;

	if(isGood())
	{
		int length = fgetc(fp);

		if(length > 0)
		{
			s = new char[length + 1];
			s[length] = 0;
			read(s, length);
		}
		else if(length < 0)
			setError(CE_GeneralError);
	}

	return s;
}

#ifdef CAMPEDIT

char* ChunkFileRead::readString(char* s)
{
	s[0] = 0;

	if(isGood())
	{
		int length = fgetc(fp);

		if(length > 0)
		{
			s[length] = 0;
			read(s, length);
		}
		else if(length < 0)
			setError(CE_GeneralError);
	}

	return s;
}
#endif



/*=========================================================
 * FileWrite Functions
 */

/*
 * Create file
 */

ChunkFileWrite::ChunkFileWrite(const char* fileName, FileType fType, const char* fileID, UWORD version, int nChunks)
{
	fp = fopen(fileName, "wb");

	/*
	 * Write File Header
	 */

	D_FileHeader header;
	memset(&header, 0, sizeof(header));

	putLong(&header.id, RRFC_ID);
	putWord(&header.headerVersion, ChunkHeaderVersion);
	putWord(&header.fileType, fType);
	putWord(&header.version, version);
	putWord(&header.nChunks, nChunks);
	strncpy(header.name, fileID, sizeof(header.name));

	if(fwrite(&header, sizeof(header), 1, fp) != 1)
	{
		setError(CE_GeneralError);
		return;
	}

	/*
	 * Create directory
	 */

	dir = new D_ChunkDir[nChunks];
	memset(dir, 0, sizeof(D_ChunkDir) * nChunks);

	chunkCount = nChunks;
	nextChunk = 0;

	if(fwrite(dir, sizeof(D_ChunkDir), nChunks, fp) != nChunks)
	{
		setError(CE_GeneralError);
		return;
	}

}

/*
 * Destructor
 *
 * Rewrite directory
 */

ChunkFileWrite::~ChunkFileWrite()
{
	setPos(sizeof(D_FileHeader));

	if(fwrite(dir, sizeof(D_ChunkDir), chunkCount, fp) != chunkCount)
	{
		setError(CE_GeneralError);
	}

	delete[] dir;
}

/*
 * Start a new chunk
 */

ChunkError ChunkFileWrite::startWriteChunk(ChunkID id)
{
	if(isGood())
	{
		if(nextChunk < chunkCount)
		{
			putLong(&dir[nextChunk].id, id);
			putLong(&dir[nextChunk].pos, getPos());
		}
		else
			setError(CE_GeneralError);
	}

	return getError();
}

/*
 * Finish a chunk (rewrite header and update directory)
 */

ChunkError ChunkFileWrite::endWriteChunk()
{
	if(isGood())
	{
		if(nextChunk < chunkCount)
		{
			putLong(&dir[nextChunk].length, getPos() - getLong(&dir[nextChunk].pos));
			nextChunk++;
		}
		else
			setError(CE_GeneralError);
	}

	return getError();
}

/*
 * Write some data
 */

ChunkError ChunkFileWrite::write(const void* ad, size_t size)
{
	if(isGood())
	{
		if(fwrite(ad, size, 1, fp) == 1)
			return CE_OK;

		setError(CE_GeneralError);
	}

	return getError();
}

void putName(IBYTE* dest, const char* src, size_t length)
{
	if(src && src[0])
		strncpy((char*)dest, src, length);
}

ChunkError ChunkFileWrite::writeString(const char* s)
{
	if(isGood())
	{
		UBYTE length;
	
		if(s)
			length = strlen(s);
		else
			length = 0;

		if(fputc(length, fp) != length)
			setError(CE_GeneralError);

		if(length)
			write(s, length);
	}

	return getError();
}

/*
 * Reading and writing with popups
 */


ReadWithPopup::ReadWithPopup(const char* fileName, FileType wantType) :
	ChunkFileRead(fileName, wantType)
{
#ifdef DEBUG
	popupBuffer = new char[400];
	sprintf(popupBuffer, "Reading %s", fileName);
	popup = new PopupText(popupBuffer);
	sPtr = popupBuffer + strlen(popupBuffer);
#else
	popup = new PopupText( language( lge_LoadingData ) );
#endif

}

ReadWithPopup::~ReadWithPopup()
{
	delete popup;
#ifdef DEBUG
	delete[] popupBuffer;
#endif
}

#ifdef DEBUG
void ReadWithPopup::showInfo(const char* fmt)
{
	strcpy(sPtr, fmt);
	popup->showText(popupBuffer);
	sPtr = popupBuffer + strlen(popupBuffer);
}

void ReadWithPopup::putInfo(const char* fmt)
{
	strcpy(sPtr, fmt);
	popup->showText(popupBuffer);
}


#endif


WriteWithPopup::WriteWithPopup(const char* fileName, FileType fType, const char* fileID, UWORD version, int nChunks) :
	ChunkFileWrite(fileName, fType, fileID, version, nChunks)
{
#ifdef DEBUG
	popupBuffer = new char[400];
	sprintf(popupBuffer, "Writing %s", fileName);
	popup = new PopupText(popupBuffer);
#else
	popup = new PopupText( language( lge_SavingData ) );
#endif

}

WriteWithPopup::~WriteWithPopup()
{
	delete popup;
#ifdef DEBUG
	delete[] popupBuffer;
#endif
}

#ifdef DEBUG
void WriteWithPopup::showInfo(const char* fmt)
{
	strcat(popupBuffer, fmt);
	popup->showText(popupBuffer);
}
#endif


