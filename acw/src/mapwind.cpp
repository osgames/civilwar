/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Map Window
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.23  1994/06/09  23:32:59  Steven_Green
 * Changed Fonts
 *
 * Revision 1.22  1994/06/07  18:29:54  Steven_Green
 * overmap only applies to actual display area not the entire thing!
 *
 * Revision 1.20  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.6  1994/02/03  15:04:21  Steven_Green
 * Split of User Interface into mapscrol.cpp
 *
 * Revision 1.2  1994/01/24  21:19:13  Steven_Green
 * Zoom to location
 * Pixel to Location function
 *
 * Revision 1.1  1994/01/20  20:04:00  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>

#include "mapwind.h"
#include "mapicon.h"

#include "scroll.h"
#include "campmode.h"
#include "layout.h"

#include "system.h"
#include "screen.h"

#include "campaign.h"
#include "city.h"

#include "ilbm.h"
// #include "bitmap.h"

#include "menudata.h"
#include "colours.h"
#include "text.h"

#include "language.h"
#include "mouselib.h"

#include "files.h"

#ifdef DEBUG
#include "campwld.h"
#include "campter.h"
#include "options.h"
#endif

#ifdef DEBUG
#define Font_Grid Font_EMMA14
#endif


MapWindow::MapWindow(IconSet* parent, CampaignMode* mode) :
	IconSet(parent, Rect(MapX,MapY,MapW,MapH)),
	mapCentre(MapMaxX/2, MapMaxY/2),
	region(machine.screen->getImage(), Rect(FullX,FullY,FullW,FullH)),
	staticMap()
{
	mapStyle = Full;
	mapMode = mode;
	setStyle(Full, mapCentre);
}

MapWindow::MapWindow(IconSet* parent, CampaignMode* mode, MapStyle style, const Location& l) :
	IconSet(parent, Rect(MapX,MapY,MapW,MapH)),
	mapCentre(l),
	region(machine.screen->getImage(), Rect(FullX,FullY,FullW,FullH)),
	staticMap()
{
	zoomMap.init(zoomMapName);
	mapStyle = Full;
	mapMode = mode;
	setStyle(style, l);
}

MapWindow::~MapWindow()
{
}

/*
 * Ask for map area to be redrawn
 */

void MapWindow::mapRedraw()
{
	if(icons)
		icons[0]->setRedraw();
}

void MapWindow::draw()
{

	/*
	 * Copy staticMap
	 */

	region.blit(staticMap.getImage(), Point(0,0));

	/*
	 * Display Units and things
	 */

	drawObjects(Disp_Moveable);
	
	/*
	 * Copy to the physical screen
	 */

	machine.screen->setUpdate(*this);
}

/*
 * Use Uncompressed bitmap format
 */

void MapWindow::makeZoomedMap()
{
	/*
	 * Convert coordinates into pixels
	 */

	Point p = Point(SDimension(mapTop.x / 65536), SDimension(mapTop.y / 65536));

	// readBMsection(staticMap.getImage(), "art\\maps\\mapzoom.bm", p);

	zoomMap.updateBM(staticMap.getImage(), p);

#ifdef DEBUG
	if(optTerrain())
		showTerrain(&staticMap);
#endif
}



#ifdef DEBUG
/*
 * Show terrain on zoomed map!
 */

static Colour terrainColours[16] = {
	0x80,	// Unused
	0x3a,	// DBlue
	0x3c,	// MBlue
	0x3e,	// PBlue
	0x4e,	// LBlue
	0x62,	// DGreen
	0x6c,	// LGreen
	0x5f,	// Yellow
	0x17,	// Red
	0x27,	// Brown
	0xf0,	// Black
	0x80,	// Unused
	0x80,	// Unused
	0x80,	// Unused
	0x80,	// Unused
	0x80	// Unused
};

void MapWindow::showTerrain(Region* bm)
{
	if(mapStyle == Zoomed)
	{
		Distance y = (mapTop.y / (65536 * 5)) * (65536 * 5);
		SDimension y1 = SDimension((y - mapTop.y) / 65536);

		while(y1 < bm->getH())
		{
			Distance x = (mapTop.x / (65536 * 5)) * (65536 * 5);
			SDimension x1 = SDimension((x - mapTop.x) / 65536);

			while(x1 < bm->getW())
			{
				Location l = Location(x, y);

				CTerrainVal t = campaign->world->terrain.get(l);

				// bm->frame(x1, y1, 5, 5, terrainColours[t]);

#ifdef CAMPEDIT
				bm->box(x1+1, y1+1, 3, 3, terrainColours[t]);
#else
				bm->HLine(x1, y1, 4, terrainColours[t]);
				bm->VLine(x1, y1, 4, terrainColours[t]);
#endif

				x1 += 5;
				x += 65536 * 5;
			}

			y1 += 5;
			y += 65536 * 5;
		}
	}
}

#ifdef CAMPEDIT
void MapWindow::show1Terrain(const Location& l, UBYTE t)
{
	/*
	 * Round location to start of terrain grid
	 */

	Location l1;
	l1.x = (l.x / (65536 * 5)) * (65536 * 5);
	l1.y = (l.y / (65536 * 5)) * (65536 * 5);

	Point p = locationToPixel(l1);

	staticMap.box(p.x, p.y, 5, 5, terrainColours[t]);
}

#endif

#endif


void MapWindow::makeStaticMap()
{
	PointerIndex oldPointer = machine.mouse->setPointer(M_Busy);

	staticMap.resize(region.getSize());

	switch(mapStyle)
	{
	case Full:
		readILBM("art\\maps\\bigmap", staticMap.getImage());
		break;

	case Zoomed:
		makeZoomedMap();
		break;

#ifdef DEBUG
	default:
		throw GeneralError("Campaign::mapStyle is invalid in MapWindow::makeStaticMap()");
#endif
	}

	/*
	 * Place permanent objects
	 */

	drawObjects(Disp_Static);

	machine.mouse->setPointer(oldPointer);
}


void MapWindow::setStyle(MapStyle s, const Location& l)
{
	PointerIndex oldPointer = machine.mouse->setPointer(M_Busy);

	mapStyle = s;

	if(mapStyle == Full)
	{
		displaySize = Location(MapMaxX, MapMaxY);
		region.setClip(MapX+FullX, MapY+FullY, FullW, FullH);
		mapTop = Location(0,0);
		mapCentre = displaySize / 2;
		setFull();
	}
	else	// Assume Zoomed
	{
		zoomMap.reset();
		displaySize = Location(ZoomW * 65536, ZoomH * 65536);
		region.setClip(MapX+ZoomX, MapY+ZoomY, ZoomW, ZoomH);
		setLocation(l);
		setZoom();
	}
	makeStaticMap();

	machine.mouse->setPointer(oldPointer);
}


void MapWindow::toggleStyle(MenuData* d)
{
	if(mapStyle == Full)
	{
		if(mapMode)
			mapMode->startZoom();
		else
			setStyle(Zoomed, mapCentre);
	}
	else
		setStyle(Full, mapCentre);
}

void MapWindow::toggleZoom(const Location& l)
{
	if(mapStyle == Full)
		setStyle(Zoomed, l);
	else
		setStyle(Full, mapCentre);
}

/*
 * Set the map's location and clip it using current map size
 */

void MapWindow::setLocation(const Location& l)
{
	Location top = l - (displaySize / 2);

	if(top.x < 0L)
		top.x = 0;
	if(top.y < 0L)
		top.y = 0;

	if(top.x >= (MapMaxX - long(displaySize.x)))
		top.x = MapMaxX - long(displaySize.x);
	if(top.y >= (MapMaxY - long(displaySize.y)))
		top.y = MapMaxY - long(displaySize.y);

	mapTop = top;
	mapCentre = top + (displaySize / 2);
}

void MapWindow::scroll(Distance dx, Distance dy)
{
	setLocation( Location(mapCentre.x + dx, mapCentre.y + dy));
	makeStaticMap();
	setRedraw();
}

/*
 * Convert location to pixel coordinates
 */

Point MapWindow::locationToPixel(const Location& l)
{
	/*
	 * Adjust for map position
	 */

	Location loc = l - mapTop;

	Point v;		// Where the result will go

	switch(mapStyle)
	{
	case Full:
		v.x = SDimension(loc.x / (65536 * 5));
		v.y = SDimension(loc.y / (65536 * 5));
		break;

	case Zoomed:
		v.x = SDimension(loc.x / 65536);
		v.y = SDimension(loc.y / 65536);
		break;
	}

	return v;
}

/*
 * Convert pixels to location
 */

Location MapWindow::pixelToLocation(const Point& p)
{
	Location l;		// Where the result will go

	/*
	 * Scale value
	 */

	switch(mapStyle)
	{
	case Full:
		l.x = p.x * 65536 * 5;
		l.y = p.y * 65536 * 5;
		break;
	case Zoomed:
		l.x = p.x * 65536;
		l.y = p.y * 65536;
		break;
	}

	/*
	 * Adjust for map position
	 */

	l += mapTop;

	return l;
}

/*
 * Draw Objects on map
 *
 * Return True if object was eligible to be drawn.
 */

void MapWindow::drawObject(MapObject* ob)
{
	MapPriority priority;

	priority = ob->isVisible(this);

	if(priority != Map_NotVisible)
	{
		Point p = locationToPixel(ob->location);

		if(ob->alwaysDraw ||
		   ( (p.x >= 0) && (p.x < region.getW()) &&
	   	 (p.y >= 0) && (p.y < region.getH()) ) )
		{
			if(displayType == Disp_Static)
				onScreenStatic.add(ob, p, priority);
			else
				onScreenMoveable.add(ob, p, priority);
		}
	}
}

void MapWindow::drawObjects(DisplayType type)
{
	/*
	 * Clear existing display list
	 */

	if(type == Disp_Static)
	{
		drawBM = &staticMap;
		onScreenStatic.clear();
	}
	else	// Assume Disp_Moveable
	{
		drawBM = &region;
		onScreenMoveable.clear();
	}

	// drawOnList->clear();
	displayType = type;

	campaign->drawObjects(this);

	if(type == Disp_Static)
		onScreenStatic.display(this);
	else	// Assume Disp_Moveable
	{
		onScreenMoveable.display(this);

#ifdef DEBUG
		Region r = *drawBM;
		r.setClip(drawBM->getX() + 300, drawBM->getY() + 2, 140, 10);
		TextWindow wind(&r, Font_JAME_5);
		char buffer[60];
		sprintf(buffer, "%9ld, %9ld", mouseLocation.x, mouseLocation.y);
		wind.setColours(Black);
		wind.setFormat(True, True);
		r.fill(White);
		wind.draw(buffer);
#endif
	}
}



/*
 * Mouse is over the map window, so do something
 */

void MapWindow::overMap(Event* event, MenuData* d)
{
	if(event->overIcon)
	{

		/*
		 * Convert the mouse coordinate to a real-life location
		 */

		Location l = pixelToLocation(event->where);

#ifdef DEBUG
		/*
		 * Display coordinate on screen
		 */

		mouseLocation = l;

#endif



#ifdef DEBUG
		if(event->buttons)
			putTestPixel(event->where);
#endif	
		if(mapMode)
			mapMode->overMap(this, event, d, &l);

	}
}


#ifdef DEBUG
void MapWindow::putTestPixel(Point p)
{
	region.plot(p, Colour(0xfe)); 
}
#endif



/*
 * Map Zoom Toggle Icon
 */


void MapViewIcon::execute(Event* event, MenuData* d)
{
	if(event->buttons & Mouse::LeftButton)
		map->toggleStyle(d);
	else
	if( ((event->key == '+') && (map->mapStyle == MapWindow::Full))
	 || ((event->key == '-') && (map->mapStyle == MapWindow::Zoomed)) )
	{
		event->key = 0;
		map->toggleStyle(d);
	}

	if(event->overIcon)
	{
		if ( map->mapStyle == map->Full)
			d->showHint( language( lge_FullZoom ) );
		else
			d->showHint( language( lge_Zoomed ) );
	}
}


/*
 * Zoomed map area
 */

class ZoomedMapArea : public MapWindowIcon, public Icon {
public:
	ZoomedMapArea(MapWindow* m);
	void execute(Event* mouse, MenuData* d);
	void drawIcon() { map->draw(); }
};

ZoomedMapArea::ZoomedMapArea(MapWindow* m) :
	MapWindowIcon(m),
	Icon(m, Rect(ZoomX,ZoomY,ZoomW,ZoomH))
{
}


void ZoomedMapArea::execute(Event* event, MenuData* data)
{
	if(event->overIcon)
		map->overMap(event, data);
}

/*
 * Zoomed map scroll icons
 */

class MapVScrollBar : public MapWindowIcon, public VerticalScrollBar {
public:
	MapVScrollBar(MapWindow* parent, Rect r) :
		VerticalScrollBar(parent, r),
		MapWindowIcon(parent)
	{
	}

	void clickAdjust(int v);
	void getSliderSize(SDimension& from, SDimension& to, SDimension size);
};

class MapHScrollBar : public MapWindowIcon, public HorizontalScrollBar {
public:
	MapHScrollBar(MapWindow* parent, Rect r) :
		HorizontalScrollBar(parent, r),
		MapWindowIcon(parent)
	{
	}

	void clickAdjust(int v);
	void getSliderSize(SDimension& from, SDimension& to, SDimension size);
};

void MapVScrollBar::clickAdjust(int v)
{
	map->scroll(0, v * (-MapSectionWidth / 4));
}

void MapHScrollBar::clickAdjust(int v)
{
	map->scroll(v * (MapSectionWidth / 4), 0);
}

void MapVScrollBar::getSliderSize(SDimension& from, SDimension& to, SDimension size)
{
	SDimension vsize = SDimension(long(size * map->getH()) /  MapMaxPY);
	SDimension vposition = SDimension((size * long(map->getMapTop().y / 65536)) / (MapMaxY / 65536));

	from = vposition;
	to = SDimension(from + vsize);
}

void MapHScrollBar::getSliderSize(SDimension& from, SDimension& to, SDimension size)
{
	SDimension hsize = SDimension(long(size * map->getW()) /  MapMaxPX);
	SDimension hposition = SDimension((size * long(map->getMapTop().x / 65536)) / (MapMaxX / 65536));
	
	from = hposition;
	to = SDimension(from + hsize);
}

/*
 * Set up the zoomed map
 */

void MapWindow::setZoom()
{
	if(icons)
		killIcons();

	icons = new IconList[7];

	icons[0] = new ZoomedMapArea	(this);
	// icons[1] = new MapVScrollBar	(this,  Rect(VScrollX1, VBorderY, VScrollW, HScrollY-VBorderY));
	icons[1] = new MapBorder		(this,  Rect(VBorderX1, VBorderY, VBorderW, VBorderH));
	icons[2] = new MapHScrollBar	(this,  Rect(HScrollX,  HScrollY, HScrollW, HScrollH));
	icons[3] = new MapVScrollBar	(this,  Rect(VScrollX2, VBorderY, VScrollW, HScrollY-VBorderY));
	// icons[4] = new BlankIcon		(this, Point(VBorderX1,  VBorderY));
	// icons[5] = new BlankIcon		(this, Point(VBorderX2,  VBorderY));
	icons[4] = new BlankIcon		(this, Point(VScrollX1, HScrollY));
	icons[5] = new BlankIcon		(this, Point(VScrollX2, HScrollY));
	icons[6] = 0;

	setRedraw();
}

/*
 * Full Map area is a single icon using the entire area
 */


class FullMapArea : public MapWindowIcon, public Icon {
public:
	FullMapArea(MapWindow* m);
	void execute(Event* mouse, MenuData* d);
	void drawIcon() { map->draw(); }
};


FullMapArea::FullMapArea(MapWindow* m) :
	MapWindowIcon(m),
	Icon(m, Rect(FullX, FullY, FullW, FullH))
{
}

void FullMapArea::execute(Event* event, MenuData* data)
{
	if(event->overIcon)
		map->overMap(event, data);
}

void MapWindow::setFull()
{
	if(icons)
		killIcons();

	icons = new IconList[4];

	icons[0] = new FullMapArea(this);
	icons[1] = new MapBorder(this, Rect(VBorderX1,VBorderY,VBorderW,VBorderH));
	icons[2] = new MapBorder(this, Rect(VBorderX2,VBorderY,VBorderW,VBorderH));
	icons[3] = 0;

	setRedraw();
}



/*
 * Keyboard Scrolling
 */

/*
 * Scroll Buttons
 */

void CampaignScrollLeftIcon::execute(Event* event, MenuData* data)
{
	data = data;
	if(event->buttons)
		campaign->mapArea->scroll(-MapSectionWidth/4, 0);
}

void CampaignScrollRightIcon::execute(Event* event, MenuData* data)
{
	data = data;
	if(event->buttons)
		campaign->mapArea->scroll(+MapSectionWidth/4, 0);
}

void CampaignScrollUpIcon::execute(Event* event, MenuData* data)
{
	data = data;
	if(event->buttons)
		campaign->mapArea->scroll(0,-MapSectionWidth/4);
}

void CampaignScrollDownIcon::execute(Event* event, MenuData* data)
{
	data = data;
	if(event->buttons)
		campaign->mapArea->scroll(0, +MapSectionWidth/4);
}


/*=========================================================
 * Display List support functions
 */

/*
 * Add object to the list
 */

MapDisplayList::MapDisplayList()
{
	entry = 0;
	last = 0;
}

MapDisplayList::~MapDisplayList()
{
	clear();
}

void MapDisplayList::clear()
{
	while(entry)
	{
		MapPriorityObject* ob = entry;
		entry = ob->next;
		delete ob;
	}
	last = 0;
}


MapPriorityObject::~MapPriorityObject()
{
	MapDisplayObject* dob = objects;

	while(dob)
	{
		MapDisplayObject* dob1 = dob;
		dob = dob->next;
		delete dob1;
	}
}

/*
 * Add an object to the display list
 */


void MapDisplayList::add(MapObject* ob, const Point& where, MapPriority priority)
{
	/*
	 * Find the correct priority list
	 */

	MapPriorityObject* plist = entry;
	MapPriorityObject* prev = 0;

	while(plist && (plist->priority < priority))
	{
		prev = plist;
		plist = plist->next;
	}

	/*
	 * If a priority object of the correct type is not found then create one
	 */

	if(!plist || (plist->priority != priority))
	{
		plist = new MapPriorityObject;
		plist->priority = priority;

		MapPriorityObject* next = prev ? prev->next : entry;

		plist->next = next;
		plist->prev = prev;
		plist->objects = 0;
		
		if(prev)
			prev->next = plist;
		else
			entry = plist;

		if(next)
			next->prev = plist;
		else
			last = plist;
	}

	/*
	 * Add the object to the current list
	 */

	MapDisplayObject* dob = new MapDisplayObject;
	dob->ob = ob;
	dob->screenPos = where;

	dob->next = plist->objects;
	plist->objects = dob;
}


/*
 * Go through the list and put onto screen
 */

void MapDisplayList::display(MapWindow* map) const
{
	/*
	 * Step through priority lists
	 */

	MapPriorityObject* plist = last;
	while(plist)
	{
		/*
		 * Step through list's entries
		 */

		MapDisplayObject* dob = plist->objects;
		while(dob)
		{
			dob->ob->mapDraw(map, map->drawBM, dob->screenPos);

			dob = dob->next;
		}

		plist = plist->prev;
	}

}


MapDisplayIter::MapDisplayIter(MapDisplayList& list)
{
	plist = list.entry;
	dob = 0;
}

int MapDisplayIter::operator ++()
{
	if(plist)
	{
		do
		{
			if(dob)
				dob = dob->next;
			else
				dob = plist->objects;

			if(dob)
				return 1;
			else
			{
				plist = plist->next;
				dob = 0;
			}
		} while(plist);
	}

	return 0;
}

#if 0
void MapWindow::cutdown()
{
	staticMap.resize(Point(0,0));
	onScreenStatic.clear();
	onScreenMoveable.clear();
}

void MapWindow::restore()
{
	makeStaticMap();
}
#endif



