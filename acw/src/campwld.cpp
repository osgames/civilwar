/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign World Environment
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.7  1994/06/09  23:32:59  Steven_Green
 * Added States
 *
 * Revision 1.6  1994/06/07  18:29:54  Steven_Green
 * River/Sea started to be implemented
 *
 * Revision 1.4  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.1  1994/05/19  17:44:37  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


#include <stdlib.h>
#include "campwld.h"
#include "generals.h"
#include "camptab.h"
#include "game.h"
#include "campaign.h"

#ifdef DEBUG
#include "memlog.h"
#endif

#ifndef CAMPEDIT
#ifdef DEBUG
LogFile cLog("campaign.log");
#endif
#endif


char UnitTerrainEffects[][ 14 ] = {
		
// unused, coast,	stream,  wood,	mountain, impass,  ?
	//	  sea,	river, swamp,	 clear,	rough,  	?,    ?
																		
	{ 0, 90, 90, 50, 30,	60, 20,	0,	60, 30, 90, 0, 0, 0 },
	{ 0, 90, 90, 40, 10,	80, 20,	0,	90, 40, 90, 0, 0, 0 },
	{ 0, 90, 90, 50, 30,	80, 20,	0,	90, 60, 90, 0, 0, 0 }

};

CampaignWorld::CampaignWorld()
{
}


CampaignWorld::~CampaignWorld()
{
	facilities.clearAndDestroy();
}


#ifdef CAMPEDIT

void CampaignWorld::init()
{
	terrain.init();
}

#else	// Normal version 

void CampaignWorld::init()
{
	terrain.init("art\\terrmap.ter");
}

void CampaignWorld::smallSetup()
{
	terrain.init("art\\terrmap.ter");
}

#endif	// CAMPEDIT

/*
 * Setup things after loading
 */

void CampaignWorld::setUp()
{
	for(WaterZoneID cid = 0; cid < waterNet.zones.entries(); cid++)
	{
		WaterZone& zone = waterNet.zones[cid];

#ifdef CAMPEDIT
		for(int i = 0; i < zone.facilityList.entries(); i++)
		{
			Facility* f = facilities[zone.facilityList[i]];
			f->waterZone = cid;

			if(f->waterCapacity)
			{
				f->facilities |= Facility::F_LandingStage;

				if(f->facilities & Facility::F_City)
				{
					City* c = (City*) f;

					if( (c->type == Industrial) || (c->type == Mixed) )
						f->facilities |= Facility::F_Port;
				}
			}
		}
#else
		int i = zone.facilityCount;
		FacilityListID fid = zone.facilityList;

		while(i--)
		{
			Facility* f = facilities[waterNet.facilityList[fid++]];

			f->waterZone = cid;

			if(f->waterCapacity)
			{
				f->facilities |= Facility::F_LandingStage;

				if(f->facilities & Facility::F_City)
				{
					City* c = (City*) f;

					if( (c->type == Industrial) || (c->type == Mixed) )
						f->facilities |= Facility::F_Port;
				}
			}
		}
#endif	// CAMPEDIT
	}

	for(FacilityID fid = 0; fid < facilities.entries(); fid++)
	{
		Facility* f = facilities[fid];

		if(!(f->facilities & Facility::F_LandingStage))
		{
			f->waterCapacity = 0;
			f->freeWaterCapacity = 0;
		}
#ifdef CAMPEDIT
		if(f->connections.entries() == 0)
			f->railCapacity = 0;
		f->freeRailCapacity = f->railCapacity;
		f->freeWaterCapacity = f->waterCapacity;
#else
		if(f->railCount == 0)
		{
			f->railCapacity = 0;
			f->freeRailCapacity = 0;
		}
#endif

	}

#ifdef PUT_UNITS_TOGETHER
	/*
	 * Force all units under army to be in same location
	 */


	for(Unit* p = ob.getSides(); p; p = p->sister)
		for(Unit* a = p->child; a; a = a->sister)
		{
			for(Unit* c = a->child; c; c = c->sister)
			{
				if(!c->isCampaignReallyIndependant())
					c->location = a->location;

				for(Unit* d = c->child; d; d = d->sister)
				{
					if(!d->isCampaignReallyIndependant())
						d->location = c->location;

					for(Unit* b = d->child; b; b = b->sister)
					{
						if(!b->isCampaignReallyIndependant())
							b->location = d->location;

						for(Unit* r = b->child; r; r = r->sister)
							r->location = b->location;
					}
				}
			}
		}

#endif
}


#ifndef CAMPEDIT

/*
 *  At end of day go through each regiment and adjust stats
 */

void CampaignWorld::UpdateStats()
{
	President* pres;
	Unit* army;
	Unit*	corps;
	Unit* division;
	Brigade* brigade;
	Regiment* regiment;

	Side si = SIDE_USA;

	while ( si != SIDE_None )
	{
		 pres = ob.getPresident( si );

		 army = pres->child;

		 while( army )
		 {
			corps = army->child;

			while( corps )
			{
				division = corps->child;

				while( division )
				{
					brigade = (Brigade*) division->child;

					while( brigade )
					{
						ASSERT(brigade->getRank() == Rank_Brigade);

						EODSupplyEffect( brigade );

					 	regiment = ( Regiment*)brigade->child;

						while ( regiment )
						{
							EODMoraleEffect( regiment );
							EODFatigueEffect( regiment );
							EODStragglersEffect( regiment );

						 	regiment = ( Regiment* )regiment->sister;
						}

						brigade->movedTime = 0;

						brigade = (Brigade*) brigade->sister;
					}

					division = division->sister;
				}

		  		corps = corps->sister;
			}

		 	army = army->sister;
		 }
		
		 if ( si == SIDE_USA ) si = SIDE_CSA;
		 else si = SIDE_None;
	}
}

/*
 * 	Set bit if enemy unit can be seen;
 */

void CampaignWorld::CanBeSeen( Unit* u )
{
 	// if( game->playersSide == u->getSide() || game->getDifficulty(Diff_TerrainEffects) < Complex1 )
 	// if( game->getLocalSide() == u->getSide() || game->getDifficulty(Diff_TerrainEffects) < Complex1 )
 	if(game->isPlayer(u->getSide()) || (game->getDifficulty(Diff_TerrainEffects) < Complex1))
	 	u->canBeSeen = True;
	else
	 	u->canBeSeen = False;
}

/*
 *  	Terrain effects Fatigue and display of units on campaign map
 */

void CampaignWorld::EODStragglersEffect( Regiment* regiment )
{
	// static int orderEffect[] = { -10, -3, -15, 15, 10, 5, -10, -15, -20 };
	static int orderEffect[] = { -15, -5, -22, 22, 15, 7, -15, -22, -30 };

	int change = game->gameRand(5);

	ASSERT(regiment->superior != 0);

	Location& l = regiment->superior->location;
	Side side = regiment->getSide();

	if ( regiment->superior->realOrders.how == ORDER_Land )
	{
		Brigade* b = (Brigade*) (regiment->superior);

		ASSERT(b != 0);

		if(b->realOrders.basicOrder() == ORDERMODE_Hold)
		{
			change += orderEffect[b->realOrders.mode];

			/*
	 		 * Find distance to nearest hospital and adjust accordingly
			 *
			 * If within:
			 *		CityWidth: Double Recovery Rate
			 *    CityWidth.. 30 Miles : 150% recovery
	 		 */

			Facility* bestF = 0;
			Distance bestD = 0;

			for(FacilityID fid = 0; fid < facilities.entries(); fid++)
			{
				Facility* f = facilities[fid];

				ASSERT(f != 0);

				if((f->side == side) && (f->facilities && Facility::F_Hospital))
				{
					Distance d = distanceLocation(f->location, l);

					if( (d < Mile(30)) && (!bestF || (d < bestD)))
					{
						bestF = f;
						bestD = d;
					}
				}
			}

			if(bestF)
			{
				if(bestD <= cityWidth)
					change += change;
				else
				{
					int m = unitToMile(bestD - cityWidth);
					change += change - (change * m) / 30;
				}
#ifdef DEBUG
				cLog.printf("Hospital Straggler recovery increased to %d for %s",
					change, regiment->getName(True));
#endif
			}
		}
		else
		{
			int movedRatio = (b->movedTime * 100) / timeToTicks(24,0,0);

			// Reduce when actually moving

			change += (movedRatio * orderEffect[b->realOrders.mode]) / 100;

			// Update for while they were not moving

			change += ((100 - movedRatio) * 5) / 100;
		}

		Realism re = game->getDifficulty(Diff_Fatigue);

		// if ( re >= Complex1 && change ) 
		if(re >= Complex1) 
		{
			int perchange = ( (MaxAttribute - regiment->fatigue) / 50 ) - 2;

			change += perchange;
		}
	}


	if ((change == 0) || (regiment->strength == 0))
		return;

	if(change < 0)		// Get more stragglers!
	{
		change = -change;
		int newStrag = (regiment->strength * change) / 100;
		if(newStrag > regiment->strength)
		{
			regiment->stragglers += regiment->strength;
			regiment->strength = 0;
		}
		else
		{
			regiment->stragglers += newStrag;
			regiment->strength -= newStrag;
		}
	}
	else		// Recover some stragglers
	{
		int newStrag = (regiment->stragglers * change + 99) / 100;
	  
		if(newStrag > regiment->stragglers)
			newStrag = regiment->stragglers;

		regiment->strength += newStrag;
		regiment->stragglers -= newStrag;
	}
}

/*
 * 	Orders effect fatigue and number of straggulers created / rejoining
 */

void CampaignWorld::EODFatigueEffect( Regiment* regiment )
{
	Realism re = game->getDifficulty(Diff_Fatigue);

	if ( re < Average ) return;

	static BYTE orderEffect[] = { +20, +10, +40, -30, -20, -25, +20, +40, +60 };

	/*
	 * 	Add 10 for end of day recovery
	 */

	if(regiment->fatigue > 10)
		regiment->fatigue -= 10;
	else
		regiment->fatigue = 0;

	ASSERT(regiment->superior != 0);

	if ( regiment->superior->realOrders.how == ORDER_Land )
	{
		BYTE change;
		
		Brigade* b = (Brigade*) (regiment->superior);

		if(b->realOrders.basicOrder() == ORDERMODE_Hold)
			change = orderEffect[b->realOrders.mode];
		else
		{
			int movedRatio = (b->movedTime * 100) / timeToTicks(24,0,0);

			// Reduce when actually moving

			change = (movedRatio * orderEffect[b->realOrders.mode]) / 100;

			// Update for while they were not moving

			change += ((100 - movedRatio) * 5) / 100;
		}

		/*
		 * Lack of Supply can increase fatigue
		 */

		change += 20 - (20 * regiment->supply) / MaxSupply;

		// if ( change == 0 || regiment->fatigue == 0 ) return;
		if(change == 0)
			return;


		/*
		 * 	Terrain effects Fatigue if realism >= complex1;
		 */

		re = game->getDifficulty(Diff_TerrainEffects);

		int perchange = 0;

		if ( re >= Complex1 )
		{
			perchange = UnitTerrainEffects[ regiment->type.basicType ][ terrain.get( regiment->location ) ];
		}

		if ( re >= Complex3 )
		{
			perchange += (regiment->morale / 50) - 2;
		}

		if ( perchange )
			change += change * perchange / 100;

		change += regiment->fatigue;
		if(change < 0)
			regiment->fatigue = 0;
		else if(change > MaxAttribute)
			regiment->fatigue = MaxAttribute;
		else
			regiment->fatigue = change;


	}
}


void CampaignWorld::EODMoraleEffect( Regiment* regiment )
{
  	Realism re = game->getDifficulty(Diff_Morale);

	if ( re == Simple ) return;

#if 0
	signed char moraleEffects[] = { 10, 5, 15, -15, -10, -5, 20, 30, 40 };
#else
	signed char moraleEffects[] = { -10, -5, -15, +15, +10, +5, -20, -30, -40 };
#endif

	ASSERT(regiment->superior != 0);

	signed char change = moraleEffects[ regiment->superior->realOrders.mode ];

#if 0
	if ( regiment->superior->realOrders.how == ORDER_Water ) change = -5;
	else if ( regiment->superior->realOrders.how == ORDER_Rail ) change = -10;
#else
	if ( regiment->superior->realOrders.how == ORDER_Water ) change = +5;
	else if ( regiment->superior->realOrders.how == ORDER_Rail ) change = +10;
#endif
	
	re = game->getDifficulty(Diff_TerrainEffects);

	/*
	 * 	Terrain effects Fatigue if realism >= complex1;
	 */

	int perchange = 0;

	if ( re >= Complex1 ) 
	{
		perchange += UnitTerrainEffects[ regiment->type.basicType ][ terrain.get( regiment->location ) ];
	}

	if ( re >= Complex2 ) 
	{
		General* temp = regiment->superior->general;

		if ( temp )
		{
			perchange += ( temp->ability / 50 - 2 );
		}
	}

	if ( re >= Complex3 && game->getDifficulty(Diff_Supply) != Simple ) 
	{
		perchange += ( ( regiment->supply / 50 ) - 2 );
	}

	if ( perchange ) change += (change * perchange) / 100;

	if ( regiment->morale + ( regiment->morale * change ) / 100 < MaxAttribute )
	{
		if ( regiment->morale + ( regiment->morale * change ) / 100 > 0 )
		{
			regiment->morale += ( regiment->morale * change ) / 100;
		}
		else regiment->morale = 0;
	}
	else regiment->morale = MaxAttribute;
}

/*
 * 	Is regiment in supply range:
 *
 *		If brigade is in range assume all children are.
 */


void CampaignWorld::EODSupplyEffect(Brigade* brigade)
{
	Realism re = game->getDifficulty(Diff_Supply);

	if ( re == Simple )
	{
		Regiment* regiment = ( Regiment* )brigade->child;
		while(regiment)
		{
			regiment->supply = MaxSupply;
			regiment = (Regiment*) regiment->sister;
		}
		
		return;
	}

	Regiment* regiment = ( Regiment* )brigade->child;

	Side side = brigade->getSide();

	/*
	 * Find closest facility
	 * should be friendly!!!!!!!
	 */

	if(facilities.entries())
	{
		FacilityIter list = facilities;

		Facility* closest = 0;
		Distance bestDist;

		while(++list)
		{
			Facility* f = list.current();

			if(!onOtherSide(f->side, side))
			{
				Distance d = distanceLocation(f->location, regiment->location);

				if( ( (d < citySupplyRange) &&
						(!closest || (d < bestDist))) &&
						( ( f->facilities & Facility::F_SupplyDepot ) || 
						  ( (re < Complex2) && f->railCount ) ) )
				{
					closest = f;
					bestDist = d;
				}
			}
		}

		int supplyAmount = 0;

		if(closest)
 		{
		 	if((re < Complex1) || (bestDist < Mile(1)))
				supplyAmount = closest->supplies;
			else
				supplyAmount = closest->supplies - (closest->supplies * (bestDist - Mile(1))) / citySupplyRange;
		}

		/*
		 * Summer/Autumn ensure supplies don't drop below 50
		 */

		Boolean selfSufficient = False;

		if ( (supplyAmount < (MaxAttribute - 8)) && (campaign->timeInfo.month >= 6) && (campaign->timeInfo.month <= 8))
			selfSufficient = True;

		int totalUsed = 0;
		Strength totalMen = 0;

		while ( regiment )
		{
			/*
			 * Use up some supplies
			 */

			int newAmount = regiment->supply - 10;		// Use up 10 per day
			if(newAmount < 0)
				newAmount = 0;

			if(selfSufficient && (newAmount < (MaxSupply / 2)))
				newAmount = MaxSupply / 2;

			/*
			 * Take some from City to top up newAmount
			 */

			newAmount += supplyAmount;

			int amountUsed = supplyAmount;
			if(newAmount > MaxSupply)
			{
				amountUsed -= newAmount - MaxSupply;
				newAmount = MaxSupply;
			}

			regiment->supply = newAmount;
			totalUsed += amountUsed;
			totalMen += regiment->strength;

#ifdef DEBUG
			cLog.printf("%s's supplies changed to %d", newAmount);
#endif

			regiment = ( Regiment* )regiment->sister;
		}


		/*
		 * Subtract from city's supplies!
		 */

		/*
		 * Subtract 1 unit per 5000 men from city
		 */

		if(closest && totalUsed)
		{
			totalUsed = totalUsed / (1 + totalMen / 5000);

			if(closest->supplies > totalUsed)
				closest->supplies -= totalUsed;
			else
				closest->supplies = 0;
#ifdef DEBUG
			cLog.printf("%s's supplies reduced to %d by %s",
				closest->getNameNotNull(), (int) closest->supplies, brigade->getName(True));
#endif
		}
	}
}




#if 0
			if ( battleunit->unit->general )
			{
			 	if ( battleunit->unit->general->experience < 50 ) battleunit->unit->general->experience = 50;
				else if ( (battleunit->unit->general->experience + 5) < MaxAttribute ) battleunit->unit->general->experience += 5;
			}
			
			if ( battleunit->unit->rank < Rank_Brigade )
			{
				Unit* c = battleunit->unit->child;

				while ( c )
				{
					if ( c->general )
					{
				 		if ( c->general->experience < 50 ) c->general->experience = 50;
						else if ( ( c->general->experience + 5 ) <= MaxAttribute ) c->general->experience += 5;
					}
			
					if ( c->rank < Rank_Brigade )
					{
						Unit* d = c->child;

						while ( d )
						{
						 	if ( d->general )
							{
				 				if ( d->general->experience < 50 ) d->general->experience = 50;
								else if ( ( d->general->experience + 5 ) <= MaxAttribute ) d->general->experience += 5;
							}

							if ( d->rank < Rank_Brigade )
							{
								Unit* b = d->child;

								while ( b )
								{
								 	if ( b->general )
									{
				 						if ( b->general->experience < 50 ) b->general->experience = 50;
										else if ( ( b->general->experience + 5 ) <= MaxAttribute ) b->general->experience += 5;
									}

									b = b->sister;
				 				}
							}

							d = d->sister;
						}
					}
					c = c->sister;
				}
			}
			battleunit = battleunit->next;
		}
	}
}
#endif

/*
 *  	Add terrain effects to speed
 */

Speed CampaignWorld::getOrderEffect( Speed s, OrderMode how )
{
	switch( how )
	{
		case ORDER_Advance:
		case ORDER_Retreat:
			return s;
		
		case ORDER_CautiousAdvance:
		case ORDER_Withdraw:
			return s * 80 / 100;

		case ORDER_Attack:
		case ORDER_HastyRetreat:
			return s * 120 / 100;
	}

	return s * 50 / 100;
}



Speed CampaignWorld::getTerrainEffect( Speed s, const Location& l )
{
	if(game->getDifficulty(Diff_TerrainEffects) == Simple) return s;

 	CTerrainVal CTval = terrain.get( l );

	UBYTE percent = terrainSpeedEffect[CTval];

	if(percent == 0)
		return 0;
	else if(percent == 100)
		return s;
	else
		return (s * percent) / 100;
}

Boolean CampaignWorld::CheckLocalDest( const Location& l )
{
 	CTerrainVal CTval = terrain.get( l );

	return terrainSpeedEffect[CTval] == 0;
}

Boolean CampaignWorld::waterRouteOK( FacilityID on, FacilityID off )
{
	const Facility* facilityfrom = facilities.get(on);
	WaterZoneID fromZone = facilityfrom->waterZone;

	const Facility* facilityto = facilities.get(off);
	WaterZoneID toZone = facilityto->waterZone;

	WaterZoneID wzid = waterNet.findRoute( fromZone, toZone, False, 0, 0 );

	if ( wzid != NoWaterZone ) return True;

	return False;
}


#ifdef CREATE_WORLD

Boolean validLocation(Location& l)
{
	return ((long)l.x < MapMaxX) && ((long)l.y < MapMaxY);
}

/*==========================================================================
 * Test Data
 */

void OrderBattle::makeNewSide(CampaignWorld* world, President* president, const Location& l, GeneralGenerator& gen, RegimentGenerator& reg)
{
	int armyCount = (game->gameRand.getB() & 1) + 4;	// 4..5 Armies
	Side side = president->getSide();

	/*
	 * Make a few unallocated generals and regiments on each side
	 */

	int i = 5;
	while(i--)
	{
		president->addFreeGeneral(gen.make(side, this));
		president->addFreeRegiment(reg.make());
	}

	while(armyCount--)
	{
		Location al;
		
		do
			al = l + Location(game->gameRand.getL() % Mile(256), game->gameRand.getL() % Mile(256));
		while(!validLocation(al) || (world->terrain.get(al) <= CT_River));

		ostrstream aName;

		aName << armyCount+1 << " Army" << '\0';

		Army* army = new Army(al, aName.str());
		delete aName.str();
		// addUnit(army);

		assignGeneral(gen.make(side, this), army);
		assignUnit(army, president);

		int corpsCount = (game->gameRand.getL() % 4) + 2;	// 2..5 Corps

		while(corpsCount--)
		{
			Location ac;
			do
				ac = al + Location(game->gameRand.getL() % Mile(128), game->gameRand.getL() % Mile(128));
			while(!validLocation(ac) || (world->terrain.get(ac) <= CT_River));

			Corps* corps = new Corps(ac);
			// addUnit(corps);
			assignGeneral(gen.make(side, this), corps);
			assignUnit(corps, army);

			int divCount = (game->gameRand.getL() % 5) + 1;	// 1..5 Divisions

			while(divCount--)
			{
				Location ad;
				
				do
					ad = ac + Location(game->gameRand.getL() % Mile(64), game->gameRand.getL() % Mile(64));
				while(!validLocation(ad) || (world->terrain.get(ad) <= CT_River));

				Division* div = new Division(ad);
				// addUnit(div);
				assignGeneral(gen.make(side, this), div);
				assignUnit(div, corps);

				int brigCount = (game->gameRand.getL() % 5) + 1;	// 1..5 Brigades
				while(brigCount--)
				{
					Location ab;
					do
						ab = ad + Location(game->gameRand.getL() % Mile(32), game->gameRand.getL() % Mile(32));
					while(!validLocation(ab) || (world->terrain.get(ab) <= CT_River));

					Brigade* brig = new Brigade(ab);
					// addUnit(brig);
					assignGeneral(gen.make(side, this), brig);
					assignUnit(brig, div);

					int regCount = (game->gameRand.getL() % 6) + 1;	// 1..6 Regiments
					while(regCount--)
					{
						Location ar;
						do
							ar= ab + Location(game->gameRand.getL() % Mile(16), game->gameRand.getL() % Mile(16));
						while(!validLocation(ar) || (world->terrain.get(ar) <= CT_River));
				
						ostrstream aName;

						aName << regCount+1 << '/' << brigCount+1 << '/' << divCount+1 << '/' << corpsCount+1 << '/' << armyCount+1 << " Regiment" << '\0';

						Regiment* regiment = reg.make(ar, aName.str());
						assignUnit(regiment, brig);
					}
				}
			}
		}
	}
}

void OrderBattle::makeTestArmies(CampaignWorld* world)
{
	GeneralGenerator gen;
	RegimentGenerator reg;

	PopupText popup("Making CSA army");

	// int oldRand = rand();
	// srand(12345);
		President* p = new President(SIDE_CSA);
		// addUnit(p);
		// sides->append(p);
		addSide(p);

		makeNewSide(world, p, Location(Mile(256), Mile(600)), gen, reg);

		popup.showText("Making USA army");

		p = new President(SIDE_USA);
		// addUnit(p);
		// sides->append(p);
		addSide(p);

		makeNewSide(world, p, Location(Mile(900), Mile(256)), gen, reg);
	// srand(oldRand);

#ifdef DEBUG

	popup.showText("Writing OB to log");

	logArmies(sides);
#endif
}

const FacilityID maxCities = 500;

class CityGenerator {
	int howMany;
public:
	CityGenerator() { howMany = 0; }

	Facility* make(CampaignWorld* world);
};

Facility* CityGenerator::make(CampaignWorld* world)
{
	/*
	 * Pick a random location not on water
	 */

	Location l;
	
	do {
		l = Location(game->gameRand.getL(MapMaxX), game->gameRand.getL(MapMaxY));
	} while(world->terrain.get(l) <= CT_River); 

	/*
	 * Find out if it is on a river/sea
	 */

	CTerrainVal grid[49];
	world->terrain.makeGrid(grid, l, 7);

	Boolean onCoast = False;
	Boolean onRiver = False;
	for(int x = 0; x < 48; x++)
	{
		if(grid[x] == CT_Coastal)
			onCoast = True;
		else if(grid[x] == CT_River)
			onRiver = True;
	}


	Side side = (l.y < Distance(MapMaxY / 2)) ? SIDE_USA : SIDE_CSA;
	UWORD facilityFlag;

	// do {
		facilityFlag = UWORD(game->gameRand() &
			(Facility::F_SupplyDepot |
			 Facility::F_RecruitmentCentre |
			 Facility::F_TrainingCamp |
			 Facility::F_Hospital |
			 Facility::F_POW));

		if(howMany < 50)
		{
			facilityFlag |= Facility::F_City;

			if(howMany < 6)
				facilityFlag |= Facility::F_KeyCity;
		}
		else
			facilityFlag &= (Facility::F_SupplyDepot);


	// } while(facilityFlag == 0);

	if(onCoast)
		facilityFlag |= Facility::F_Port | Facility::F_City;
	else if(onRiver)
		facilityFlag |= Facility::F_LandingStage;


	Facility* f;

	if(facilityFlag & Facility::F_City)
	{
		char s[16];
		sprintf(s, "Test City %d", howMany++);
		f = new City(s, l, side, facilityFlag | Facility::F_SupplyDepot);

		if(game->gameRand(100) < 10)
			f->fortification = UBYTE(game->gameRand(4));
	}
	else
	{
		char s[16];
		sprintf(s, "Facility %d", howMany++);
		f = new Facility(s, l, side, facilityFlag);

		if(game->gameRand(100) < 10)
			f->fortification = UBYTE(game->gameRand(4));
	}

#if 0
	if(facilityFlag && Facility::F_SupplyDepot)
	 	f->wagons = UBYTE(game->gameRand(9));
#endif

	if( (facilityFlag & Facility::F_City) || f->fortification)
		f->victory = game->gameRand();

	return f;

}

void CampaignWorld::makeCities()
{
	/*
	 * Make random cities and railways
	 */

	facilities.init(maxCities);

	CityGenerator cityGenerator;
	FacilityID cityCount = 0;
	while(cityCount < maxCities)
		// facilities.set(cityCount++, cityGenerator.make(this));
		facilities[cityCount++] = cityGenerator.make(this);

	/*
	 * Set key cities
	 */

	for(cityCount = 0; cityCount < maxCities; cityCount++)
	{
		// Facility* f = facilities.get(cityCount);
		Facility* f = facilities[cityCount];

		// if(f->facilities & Facility::F_Named)
		{
			Facility* nf = f;

			if(nf->facilities & Facility::F_KeyCity)
				nf->setOwner(NoFacility);
			else
			{	// Locate nearest key city
				FacilityID closest = NoFacility;
				Distance dist = 0;

				for(FacilityID cityIndex = 0; cityIndex < maxCities; cityIndex++)
				{
					// Facility* f1 = facilities.get(cityIndex);
					Facility* f1 = facilities[cityIndex];

					if(f1->facilities & Facility::F_KeyCity)
					{
						Distance d = distance(f->location.x - f1->location.x,
													 f->location.y - f1->location.y);

						if( (closest == NoFacility) || (d < dist))
						{
							closest = cityIndex;
							dist = d;
						}
					}
				}

				nf->setOwner(closest);
			}
		}
	}
		
}

void CampaignWorld::setCityStates()
{
	FacilityIter list = facilities;

	while(++list)
	{
		Facility* f = list.current();

		Distance dist = 0;
		StateID best = NoState;

		StateListIter slist = states;
		StateID id = 0;
		while(++slist)
		{
			State* st = &slist.current();

			Distance d = distance(f->location.x - st->location.x,
										 f->location.y - st->location.y);

			if((best == NoState) || (d < dist))
			{
				best = id;
				dist = d;
			}

			id++;
		}

		f->state = best;
	}
}


class RailMakeQueue {
	FacilityID* items;
	unsigned int howMany;
	unsigned int head;
	unsigned int tail;
public:
	RailMakeQueue(int n)
	{
		items = new FacilityID[n];
		howMany = n;
		head = 0;
		tail = 0;
	}

	~RailMakeQueue()
	{
		delete[] items;
	}

	void put(FacilityID id)
	{
		int nextHead = head + 1;
		if(nextHead >= howMany)
			nextHead = 0;
		if(nextHead == tail)
			throw GeneralError("Rail Make queue is full");

		items[head] = id;
		head = nextHead;
	}

	Boolean get(FacilityID& id)
	{
		if(head == tail)
			return False;
		else
		{
			id = items[tail];
			if(++tail >= howMany)
				tail = 0;
			return True;
		}
	}

	Boolean isEmpty()
	{
		return head == tail;
	}

	int entries()
	{
		if(head > tail)
			return head - tail;
		else
			return head + howMany - tail;
	}
};

void CampaignWorld::makeRailways(PopupText& popup)
{
	/*
	 * Generate railways (uses intermediate data structure)
	 */

	const int MaxConnections = 8;		// Maximum number of connections from a facility

	struct RailCreate {
		UBYTE howMany;
		FacilityID connections[MaxConnections];
	};
	
	RailCreate* cons = new RailCreate[maxCities];

	int totalJunctions = 0;			// Running total
	int totalCities = 0;

	/*
	 * Queue structure
	 */

	RailMakeQueue queue(100);

	/*
	 * Initialise data
	 */

	FacilityID cityCount = maxCities;
	RailCreate* conPtr = cons;
	while(cityCount--)
		conPtr++->howMany = 0;

	/*
	 * Create rail sections
	 */

	queue.put(0);

	while(queue.get(cityCount))
	{
#ifdef DEBUG1
		if(logFile)
			*logFile << "Considering Facility " << cityCount << endl;
#endif

		if((totalCities % 10) == 0)
		{
			char popupBuf[120];

			sprintf(popupBuf,
				"Generating Railways\rCity %d\rConnections %d\r%d in queue",
					totalCities, totalJunctions, queue.entries());
			popup.showText(popupBuf);
		}
		totalCities++;

		conPtr = &cons[cityCount];

		static UBYTE junctionTable[32] = {
#if 0
			0, 1, 1, 2, 2, 2, 2, 2,
			2, 2, 2, 2, 2, 2, 3, 3,
			3, 3, 2, 2, 3, 3, 3, 3,
			3, 3, 3, 3, 3, 3, 3, 4
#else
			2, 2, 2, 2, 2, 2, 2, 2,
			2, 2, 2, 2, 2, 2, 2, 2,
			2, 2, 2, 2, 2, 2, 2, 2,
			2, 2, 3, 3, 3, 3, 3, 3
#endif
		};

		UBYTE junctions = junctionTable[game->gameRand() & 0x1f];
		// Facility* f = facilities.get(cityCount);
		Facility* f = facilities[cityCount];

#if 0
		if(!junctions && !(f->facilities & Facility::F_Named))
			junctions = 1;
#endif

		if(junctions > conPtr->howMany)
		{
			junctions -= conPtr->howMany;

			/*
			 * Pick closest 20 cities, but don't include further ones that
			 * are within 22.5 degrees of a closer one
			 */

			const FacilityID MaxClose = 25;

			struct {
				FacilityID city;
				Distance distance;
				Wangle direction;
				UBYTE railCount;
			} closeCities[MaxClose];

			for(int i = 0; i < MaxClose; i++)
				closeCities[i].city = NoFacility;

			int nextFreeClose = 0;

			for(FacilityID cityIndex = 0; cityIndex < maxCities; cityIndex++)
			{
				// Don't connect with ourself

				if(cityIndex == cityCount)
					continue;
			
#if 0
				// Don't let connections array overflow

				if(cons[cityIndex].howMany >= MaxConnections)
					continue;
#endif

				// Facility* nf = facilities.get(cityIndex);
				Facility* nf = facilities[cityIndex];

				Distance d = distance(nf->location.x - f->location.x,
											nf->location.y - f->location.y);

#if 0
				Wangle direction = Wangle(nf->location.x - f->location.x,
												  nf->location.y - f->location.y);
#else
				Wangle dir = direction(nf->location.x - f->location.x,
												     nf->location.y - f->location.y);
#endif

				/*
				 * See if any have similar direction
				 */

				Boolean discard = False;
				for(int i = 0; i < nextFreeClose; i++)
				{
					Wangle dw = dir - closeCities[i].direction;

					if( (dw < 0x2000) || (dw > 0xe000) )
					{
						if(d > closeCities[i].distance)
						{
							discard = True;
							break;
						}
						else
						{	// Remove the existing one!
							for(int j = i; j < nextFreeClose; j++)
								closeCities[j] = closeCities[j+1];
							nextFreeClose--;
							i--;		// Backup so the newly shuffled one is processed
						}
					}
				}

				if(discard)
					continue;

#if 0
				// Check if already connected to us
				// If it is then it is entered into the table, but with an
				// invalid city ID, so it won't get picked.

				discard = False;
				for(int count = 0; count < conPtr->howMany; count++)
				{
					if(conPtr->connections[count] == cityIndex)
					{
						discard = True;
						break;
					}
				}
#endif

				int slot;

				if(nextFreeClose < MaxClose)
					slot = nextFreeClose++;
				else		// Replace largest distance
				{
					int best = 0;
					for(int i = 1; i < MaxClose; i++)
					{
						if(closeCities[i].distance > closeCities[best].distance)
							best = i;
					}

					slot = best;
				}

#if 0
				if(discard)
					closeCities[slot].city     = NoFacility;
				else
#endif

					closeCities[slot].city     = cityIndex;
				closeCities[slot].distance = d;
				closeCities[slot].direction = dir;
				closeCities[slot].railCount = cons[cityIndex].howMany;
			}

#if 0
			/*
			 * Fill up junctions with random cities from the top 20.
			 */


			nextFreeClose = 0;
			for(int count = 0; count < MaxClose; count++)
				if(closeCities[count].city != NoFacility)
					nextFreeClose++;

			if(nextFreeClose < junctions)
				junctions = UBYTE(nextFreeClose);

			while(junctions)
			{
				int picked;

				/*
				 * Pick up to 10 random places.
				 * If it already has a railway, then take it
				 * otherwise use whatever's left.
				 */

				do
				{
					picked = game->gameRand(MaxClose);
				} while(closeCities[picked].city == NoFacility);

				FacilityID connect = closeCities[picked].city;
				closeCities[picked].city = NoFacility;

				cons[connect].connections[cons[connect].howMany++] = cityCount;
				conPtr->connections[conPtr->howMany++] = connect;
				totalJunctions += 2;
				junctions--;
			}
#else

#ifdef DEBUG1
			if(logFile)
				*logFile << nextFreeClose << " locations found" << endl;
#endif

			/*
		 	 * Pick closest destinations that don't already have railways
		 	 */

			// Filter through removing any with railways

			int lastUsed = 0;
			for(int count = 0; count < nextFreeClose; count++)
			{
				if(closeCities[count].railCount == 0)
					closeCities[lastUsed++] = closeCities[count];
			}
			nextFreeClose = lastUsed;

#ifdef DEBUG1
			if(logFile)
				*logFile << nextFreeClose << " don't already have railways!" << endl;
#endif

			if(junctions > nextFreeClose)
				junctions = UBYTE(nextFreeClose);

			while(junctions)
			{
				Distance d = 0;
				int picked = 0;
				for(int count = 0; count < nextFreeClose; count++)
				{
					if((closeCities[count].city != NoFacility) &&
				   	((picked == 0) || (closeCities[count].distance < d)))
					{
						picked = count;
						d = closeCities[count].distance;
					}
				}

				FacilityID connect = closeCities[picked].city;
				closeCities[picked].city = NoFacility;

				cons[connect].connections[cons[connect].howMany++] = cityCount;
				conPtr->connections[conPtr->howMany++] = connect;
				totalJunctions += 2;
				junctions--;
				queue.put(connect);

#ifdef DEBUG1
				if(logFile)
					*logFile << "Picked " << connect << endl;
#endif

			}

#endif
		}

	}

	/*
	 * Copy RailCreate into final railways data
	 */

	popup.showText("Copying Railway data");

	railways.init(totalJunctions);
	RailSectionID railCount = 0;
	for(cityCount = 0; cityCount < maxCities; cityCount++)
	{
		// Facility* f = facilities.get(cityCount);
		Facility* f = facilities[cityCount];
		RailCreate* conPtr = &cons[cityCount];

		f->railCount = conPtr->howMany;
		if(conPtr->howMany)
		{
			f->railSection = railCount;

			for(int count = 0; count < conPtr->howMany; count++)
				// railways.set(railCount++, conPtr->connections[count]);
				railways[railCount++] = conPtr->connections[count];

			f->railCapacity = UBYTE(game->gameRand(railCount * 10));
		}
		else
		{
			f->railSection = NoRailSection;
			f->railCapacity = 0;
		}
	}

	delete cons;

	popup.showText("Checking railways");

	checkRails();
}

#endif	// CAMPEDIT

#endif	// CREATE_WORLD

