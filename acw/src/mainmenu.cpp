/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Main Menu Screen Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.15  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.10  1994/04/22  22:14:22  Steven_Green
 * Icon Handling tidied up.
 *
 * Revision 1.3  1994/01/24  21:19:13  Steven_Green
 * execute function only activated if button pressed
 *
 * Revision 1.1  1994/01/11  22:29:03  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include <stdlib.h>
#include <stdio.h>

#include "mainmenu.h"
#include "menudata.h"
#include "hintstat.h"
#include "layout.h"
#include "system.h"
#include "mouselib.h"
#include "gameicon.h"
#include "game.h"
#include "text.h"
#include "connect.h"
#include "dialogue.h"
#include "timer.h"
#include "version.h"
#include "savegame.h"

#include "language.h"

#include "screen.h"
#include "colours.h"

#include "options.h"

/*====================================================================
 * Main Menu
 */

class MainMenu : public MenuData, public MenuConnection, public HintStatus {
	Colour hintColour;
public:
	MainMenu();
	~MainMenu() { processMessages(True); }

	void processPacket(Packet* pkt);
	void proc();
	void drawIcon();						// virtual Icon

	void updatePlayers();

	void showHint(const char* s) { HintStatus::showHint(s); shownHint = True; }
	void showStatus(const char* s) { HintStatus::showStatus(s); }

	void showHintStatus(const char* s);
#ifdef COMMS_TODO
	void checkFinished();
#endif
};

void MainMenu::showHintStatus(const char* s)
{
	Region bm(machine.screen->getImage(), Rect(10,1,497,8));
	TextWindow wind(&bm, Font_JAME_5);
	wind.setFormat(TW_CV | TW_CH);	// Vertically centre, Horizontal Centre
	wind.setColours(Black);

	bm.fill(hintColour);

	if(s)
		wind.draw(s);

	machine.screen->setUpdate(bm);
}


void MainMenu::drawIcon()
{
	showMainMenuScreen();

	/*
	 * Get the hint colour from the screen
	 */

	hintColour = *machine.screen->getImage()->getAddress(10,1);

	/*
	 * Put on version Number
	 */

	Region bm(machine.screen->getImage(), Rect(10,363,497,8));
	TextWindow wind(&bm, Font_JAME_5);
	wind.setFormat(TW_CV | TW_Left);	// Vertically centre, Left align
	wind.setColours(Black);
	wind.wprintf("%s %s", language(lge_Version), getVersionStr());

	IconSet::drawIcon();
}

/*
 * Connection packets used by main menu
 */

struct StartGamePKT {
	int how;
	GameType gameType;
};

struct SidePKT {					// MM_Side packet
	Side side;
};

struct SetupPKT {					// MM_Setup packet
	char id[8];
	ULONG version;						// Program version
	Side side;							// What side to play
	UWORD seed;							// Random number seed
	Realism realism[DifficultyCount];
	Boolean realTimeCampaign;
	RouteMode routeMode;
};

static char setupID[8] = "ACW_SET";

/*
 * One/Two Player Icon
 */

class PlayersIcon : public MenuIcon {
public:
	PlayersIcon(IconSet* set, Point p, Key k1 = 0, Key k2 = 0) :
		MenuIcon(set, MM_SinglePlayer, Rect(p, Point(SIDE_ICON_W,SIDE_ICON_H)), k1, k2) { }

	void execute(Event* event, MenuData* d);
	void drawIcon();
};

void PlayersIcon::execute(Event* event, MenuData* d)
{
	if(event->overIcon)
		d->showHint( language( lge_multi ) );

	if(event->buttons)
	{
#if defined(COMPUSA_DEMO)
		dialAlert(0, "2 Player mode\ris not available\rin this demo version", "OK");
#else
		/*
	 	 * If already multi-player, then switch to single player
	 	 */

		// if(game->playerCount > 1)
		if(game->isRemoteActive())
		{
			game->disconnect();
			setRedraw();
		}

		/*
	 	 * Otherwise get connection type and initialise
	 	 */

		else
		{
			Connection* newConnect = 0;

			switch(dialAlert(0,
				language(lge_connectionType),
				language(lge_SerialModemIpxCancel)))
			{
			case 0:		// Serial
				newConnect = connectSerial();
				if(newConnect)
					d->showStatus(language(lge_SerialEstablished));
				else
					dialAlert(0, language(lge_SerialNoConnect), language( lge_OK) );		// Clear message
				break;
			case 1:		// Modem
				newConnect = connectModem();
				if(newConnect)
					d->showStatus(language(lge_ModemEstablished));
				else
					dialAlert(0, language(lge_ModemNoConnect), language( lge_OK) );		// Clear message
				break;
			case 2:		// IPX
				newConnect = connectIPX();
				if(newConnect)
					d->showStatus(language(lge_IPXEstablished));
				else
					dialAlert(0, language(lge_IPXNoConnect), language( lge_OK) );		// Clear message
				break;
			case 3:		// Cancel
				break;
			}
			if(newConnect)
			{
				delete game->connection;
				game->connection = newConnect;
				// game->playerCount = 2;
				game->setRemote();
				setRedraw();

				/*
				 * if we're the boss, make sure the other side is in the
				 * same situation as us.
				 */

				if(game->connection->inControl)
				{
					SetupPKT* packet = new SetupPKT;
					strncpy(packet->id, setupID, sizeof(packet->id));
					packet->version = getVersionLong();
					packet->side = game->getRemoteSide();
					packet->seed = timer->getFastCount();
					packet->realTimeCampaign = realTimeCampaign;
					packet->routeMode = routeMode;

					for(int i = 0; i < DifficultyCount; i++)
						packet->realism[i] = game->getDifficulty(DifficultyType(i));

					game->connection->sendMessage(Connect_MM_Setup, (UBYTE*)packet, sizeof(SetupPKT));
#ifdef DEBUG
					netLog.printf("Setup: Side=%s, seed=%d",
						(packet->side == SIDE_USA) ? "USA" : "CSA",
						packet->seed);
#endif
				}
			}
		}
#endif
	}
}

void PlayersIcon::drawIcon()
{
	// setIcon((game->playerCount == 1) ? MM_SinglePlayer : MM_MultiPlayer);
	setIcon(game->isRemoteActive() ? MM_MultiPlayer : MM_SinglePlayer);
	MenuIcon::drawIcon();
}

/*
 * Load Game Icon
 */


class LoadGameIcon : public MenuIcon {
public:
	LoadGameIcon(IconSet* set, Point p, Key k1 = 0, Key k2 = 0) : MenuIcon(set, MM_Load, Rect(p, Point(MAIN_ICON_W,MAIN_ICON_H)), k1, k2) { }
	void execute(Event* event, MenuData* d);
};

void LoadGameIcon::execute(Event* event, MenuData* d)
{
	if(event->overIcon)
		d->showHint( language( lge_load ) );

	if(event->buttons)
	{
#ifdef OLD_COMMS
		// d->setFinish(MMR_LoadGame);
#else
		StartGamePKT *packet = new StartGamePKT;
		packet->how = MMR_LoadGame;
		packet->gameType = game->gameType;

	  	game->connection->sendMessage(Connect_Finish, (UBYTE*)packet, sizeof(StartGamePKT));
#endif
	}
}



/*
 * Select Side
 */

class SideIcon : public MenuIcon {
public:
	SideIcon(IconSet* set, Point p, Key k1 = 0, Key k2 = 0) :
		MenuIcon(set, MM_SideUSA, Rect(p, Point(SIDE_ICON_W,SIDE_ICON_H)), k1, k2) { }

	void execute(Event* event, MenuData* d);
	void drawIcon();
};

void SideIcon::execute(Event* event, MenuData* d)
{
	if(event->overIcon)
		d->showHint( language( lge_chooseSide ) );

	if(event->buttons)
	{
#if 0
		game->playersSide = otherSide(game->playersSide);

		if(game->connection)
		{
			SidePKT* packet = new SidePKT;
			packet->side = otherSide(game->playersSide);
			game->connection->sendData(Connect_MM_Side, (UBYTE*)packet, sizeof(SidePKT));
		}

		setRedraw();
#else
		SidePKT* packet = new SidePKT;
		packet->side = game->getLocalSide();
		game->connection->sendMessage(Connect_MM_Side, (UBYTE*)packet, sizeof(SidePKT));
#endif
	}
}

void SideIcon::drawIcon()
{
	// setIcon( (game->playersSide == SIDE_USA) ? MM_SideUSA : MM_SideCSA);
	setIcon( (game->getLocalSide() == SIDE_USA) ? MM_SideUSA : MM_SideCSA);
	MenuIcon::drawIcon();
}


class CampaignIcon : public MenuIcon {
public:
	CampaignIcon(IconSet* set, Point p) :
		MenuIcon(set, MM_Campaign, Rect(p, Point(SIDE_ICON_W,SIDE_ICON_H)))
	{
	}

	void execute(Event* event, MenuData* d);
};

class BattleIcon : public MenuIcon {
public:
	BattleIcon(IconSet* set, Point p) :
		MenuIcon(set, MM_Battle, Rect(p, Point(SIDE_ICON_W,SIDE_ICON_H)))
	{
	}

	void execute(Event* event, MenuData* d);
};

void CampaignIcon::execute(Event* event, MenuData* d)
{
	if(event->overIcon)
		d->showHint( language( lge_startCampaign ) );

	if(event->buttons)
	{
		/*
		 * Are you happy with selections?
		 */

		if(dialAlert(d, machine.mouse->getPosition(), language( lge_campaignReady ), language( lge_yesno ) ) == 0)
		{
#ifdef OLD_COMMS
			game->setGameType(CampaignGame);
			d->setFinish(MenuData::MenuOK);
#else
			StartGamePKT *packet = new StartGamePKT;
			packet->how = MenuData::MenuOK;
			packet->gameType = CampaignGame;

		  	game->connection->sendMessage(Connect_Finish, (UBYTE*)packet, sizeof(StartGamePKT));
#endif
		}
	}
}

void BattleIcon::execute(Event* event, MenuData* d)
{
	if(event->overIcon)
		d->showHint( language( lge_historical ) );

	if(event->buttons)
	{
		/*
		 * Are you happy with selections?
		 */

		if(dialAlert(d, machine.mouse->getPosition(), language( lge_historicalReady ), language( lge_yesno ) ) == 0)
		{
#ifdef OLD_COMMS
			game->setGameType(BattleGame);
			d->setFinish(MenuData::MenuOK);
#else
			StartGamePKT *packet = new StartGamePKT;
			packet->how = MenuData::MenuOK;
			packet->gameType = BattleGame;

		  	game->connection->sendMessage(Connect_Finish, (UBYTE*)packet, sizeof(StartGamePKT));
#endif
		}
	}
}


class AboutIcon : public MenuIcon {
public:
	AboutIcon(IconSet* parent, Point p, Key k1 = 0, Key k2 = 0) :
		MenuIcon(parent, MM_About, Rect(p, Point(120,48)), k1, k2) { }
	void execute(Event* event, MenuData* d);
};

void AboutIcon::execute(Event* event, MenuData* d)
{
	if(event->overIcon)
		d->showHint( language( lge_ShowCredits ) );

	if(event->buttons)
	{
		for(LGE i = lge_Credits1; i <= lge_Credits5; INCREMENT(i))
		{
			LGE prompt;

			if(i == lge_Credits5)
				prompt = lge_OK;
			else
				prompt = lge_MoreCancel;

			if(dialAlert(0, language(i), language(prompt)) != 0)
				break;
		}
	}
}


enum MainMenuIcons {
	MMI_Dos,
	MMI_Database,
	MMI_Sound,
	MMI_Load,
	MMI_About,
	// MMI_GameType,
	MMI_Players,
	MMI_Side,
	// MMI_OK,
	MMI_Campaign,
	MMI_Battle,
	MMI_Quit,

	MMI_HowMany

};


MainMenu::MainMenu() :
	MenuData(),
	MenuConnection("MainMenu")
{
#ifdef DEBUG
	netLog.setRealTime();
#endif

	icons = new IconList[MMI_HowMany + 1];

	Icon** iconPtr = icons;

	*iconPtr++ = new DosIcon(this,		Point(MAIN_ICON_X1,MAIN_ICON_Y));
	*iconPtr++ = new DatabaseIcon(this, Point(MAIN_ICON_X2,MAIN_ICON_Y));
	*iconPtr++ = new SoundIcon(this, 	Point(MAIN_ICON_X3,MAIN_ICON_Y));
	*iconPtr++ = new LoadGameIcon(this, Point(MAIN_ICON_X4,MAIN_ICON_Y));

	*iconPtr++ = new AboutIcon(this, Point(SIDE_ICON_X,SIDE_ICON_Y1));

	// *iconPtr++ = new CampaignIcon(this, Point(SIDE_ICON_X,51));
	*iconPtr++ = new PlayersIcon(this, 	Point(SIDE_ICON_X,51));
	*iconPtr++ = new SideIcon(this, 		Point(SIDE_ICON_X,158));

	// *iconPtr++ = new MMOkIcon(this, 		Point(MAIN_ICON_X5,MAIN_ICON_Y));

	*iconPtr++ = new CampaignIcon(this, Point(SIDE_ICON_X, 265));
	*iconPtr++ = new BattleIcon(this, Point(SIDE_ICON_X, MAIN_ICON_Y));

	*iconPtr++ = new QuitIcon(this);
	*iconPtr++ = 0;

	hintColour = White;
	processMessages(True);
}

void MainMenu::proc()
{

	processMessages(False);

}

void MainMenu::updatePlayers()
{
	icons[MMI_Players]->setRedraw();
	icons[MMI_Database]->setRedraw();
}

/*
 * Process an incoming packet
 */

void MainMenu::processPacket(Packet* packet)
{
	switch(packet->type)
	{
	case Connect_MM_Side:
		{
			showStatus( language( lge_remoteSidechanged ) );
			SidePKT* pkt = (SidePKT*)packet->data;

			if(packet->sender == RID_Local)
				game->setupPlayerSide(otherSide(pkt->side));
			else
				game->setupPlayerSide(pkt->side);
			icons[MMI_Side]->setRedraw();
		}
		break;

	case Connect_Finish:
		if(!isFinished())
		{
			if(packet->sender == RID_Local)
			{
				if(waitRemoteReply())
				{
					StartGamePKT* pkt = (StartGamePKT*)packet->data;

					game->setGameType(pkt->gameType);
					setFinish(pkt->how);
				}
				else
					dialAlert(this, language( lge_remoteNoleave ), language( lge_OK ) );
			}
			else
			{
				StartGamePKT* pkt = (StartGamePKT*)packet->data;

				const char* text;

				if(pkt->how == MMR_LoadGame)
					text = language(lge_remoteload);
				else if(pkt->gameType == BattleGame)
					text = language(lge_remotebattle);
				else
					text = language(lge_remotecampaign);

				if(askRemoteReply(text, language(lge_startcancel)))
				{
					game->setGameType(pkt->gameType);
					setFinish(pkt->how);
				}
			}
		}
		break;
	case Connect_MM_Setup:
		{
			SetupPKT* pkt = (SetupPKT*)packet->data;

			if( (strncmp(pkt->id, setupID, sizeof(pkt->id)) != 0) ||
			    (pkt->version != getVersionLong()) )
			{
#ifdef DEBUG
				netLog.printf("Program Version Mis-Match");
				netLog.printf("Local: %s %lx", setupID, getVersionLong());
				netLog.printf("Remote: %s %lx", pkt->id, pkt->version);
#endif
				dialAlert(0, language(lge_RemoteDifferentVersion), language(lge_OK));
				game->disconnect();
			}
			else
			{
				if(packet->sender == RID_Local)
					game->setupPlayerSide(otherSide(pkt->side));
				else
					game->setupPlayerSide(pkt->side);

				for(int i = 0; i < DifficultyCount; i++)
					game->setDifficulty(DifficultyType(i), pkt->realism[i]);
				icons[MMI_Side]->setRedraw();
				icons[MMI_Database]->setRedraw();
				srand(pkt->seed);
				game->gameRand.seed(pkt->seed);
				game->miscRand.seed(game->gameRand.getL());

				realTimeCampaign = pkt->realTimeCampaign;
				routeMode = pkt->routeMode;
#ifdef DEBUG
				netLog.printf("Setup: side=%s, seed=%d",
					(pkt->side == SIDE_USA) ? "USA" : "CSA",
					pkt->seed);
#endif
			}
		}
		break;

	default:
#ifdef DEBUG
		netLog.printf("Unknown packet received\rtype: %d, length:%d", (int) packet->type, (int)packet->length);
#endif
		break;
	}
}


int mainMenu()
{

	MainMenu menuData;

	while(!menuData.isFinished())
	{
		/*
		 * Get icon/function
		 */

		Boolean hasRemote = game->isRemoteActive();
		menuData.process();
		if(game->isRemoteActive() != hasRemote)
		{
			menuData.updatePlayers();
			hasRemote = game->isRemoteActive();
		}

	}

	return menuData.getMode();
}


