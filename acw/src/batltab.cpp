/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Tables and constants Used by battle game
 * These should be kept in a data file for easy modification by designers
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "batltab.h"
#include "terrain.h"

/*
 * Table of effective ranges for unit types
 * Those that are 0 shouldn't really be on the battlefield!
 */

Distance effectiveRanges[3][4] = {
	{ Yard( 400), Yard( 200), Yard( 600), Yard(   0) },	// Infantry
	{ Yard( 200), Yard( 100), Yard( 350), Yard( 150) },	// Cavalry
	{ Yard(2000), Yard(1500), Yard(2000), Yard(2000) },	// Artillery
};

/*
 * Decisive Range against experience
 */

Distance decisiveRange[] = {
	Yard(50),		// Untried
	Yard(100),		// Tried
	Yard(75),		// Seasoned
	Yard(50),		// Veteran
	Yard(100)		// Battle weary
};


/*
 * Command ranges of different commander types
 */

Distance battleCommandRange[] = {
	Mile(8),				// President has range over all america
	Mile(4),				// Army
	Mile(2),				// Corps
	Mile(1),				// Division has 5 mile command radius
	Mile(1)/2, 			// Brigade
};


/*
 * Loading times for each type of unit in rounds per 10 minutes
 * The 0 fields are for units that shouldn't be on the battlefield!
 */

UBYTE loadingTimes[3][4] = {
	{ 30, 20,  80,  0 },	// Infantry (Regular, Militia, Sharpshooter)
	{ 60, 20, 100, 40 },	// Cavalry (Regular, Milita, RegularMounted, MilitiaMounted)
	{ 20, 30,  30,  0 }	// Artillery (12lb, 6lb, Rifled)
};

/*
 * Ranges in yards corresponding to each entry in the infantry firing
 * modifier table
 * e.g.
 *    0 :  0 -  50 yards
 *    1 : 50 - 100 yards
 *		etc
 */

int infantryRanges[] = {
	50, 100, 150, 200, 300, 400, 0
};

int artilleryRanges[] = {
	100, 200, 300, 500, 800, 1200, 1500, 0
};

/*
 * Modifiers for each type of unit at different ranges
 */

UBYTE firingModifiers[3][4][8] = {
	{	// Infantry
		{ 6, 5, 4, 3, 2, 1, 0, 0 },	// Regular
		{ 5, 4, 3, 2, 1, 0, 0, 0 },	// Militia
		{ 6, 5, 5, 4, 3, 2, 1, 0 },	// Sharpshooter
		{ 0, 0, 0, 0, 0, 0, 0, 0 }		// Engineer
	},
	{	// Cavalry
		{ 4, 3, 2, 1, 0, 0, 0, 0 },	// Regular
		{ 4, 3, 1, 0, 0, 0, 0, 0 },	// Militia
		{ 6, 5, 4, 3, 2, 1, 0, 0 },	// Regular Mounted
		{ 4, 3, 2, 1, 0, 0, 0, 0 }		// Militia Mounted
	},
	{	// Artillery (I have used the greatest value from Adrian's table
		{ 16, 8, 4, 3, 3, 2, 2, 1 },	// 12lb
		{ 10, 6, 3, 3, 3, 2, 2, 0 },	// 6lb
		{ 10, 6, 3, 4, 4, 3, 3, 2 },	// Rifled
		{  0, 0, 0, 0, 0, 0, 0, 0 }	// Siege
	}
};

BYTE fireExperience[] = {
	-1,		// Untried
	0,			// Tried
	1,			// Seasoned
	2,			// Veteran
	0			// Battle weary
};

BYTE fireSupply[] = {
	-3,		// Unsupplied
	-2,		// Poorly
	-1,		// Partially
	 0			// Unsupplied
};

BYTE fireFatigue[] = {
	1,			// Fresh
	0,			// Weary
	-1,		// Tired
	-2			// Exhausted
};

BYTE fireMorale[] = {
	0,			// Routing
	-2,		// Demoralised
	-1,		// Dispirited
	0,			// Normal
	1,			// Good
	2			// High
};

/*=============================================
 * Regimental Response Check Tables
 */

// Modification based on Experience

BYTE rrcExperience[] = {
	+10,		// Untried
	+5,		// Tried
	+0,		// Seasoned
	-5,		// Veteran
	-10		// Battle Weary
};

BYTE rrcFatigue[] = {
	+10,		// Fresh
	+0,		// Weary
	-5,		// Tired
	-10		// Exhausted
};

BYTE rrcSupply[] = {
	-20,		// Unsupplied
	-10,		// Poorly supplied
	+0,		// Partially supplied
	+5			// Supplied
};

// Based on personality???? No such thing!!!
// Should we use average of all attributes or a single one
// Let us use ability for the time being

BYTE rrcBrigCommander[] = {	
	-20,			// Useless
	-10,			// Ability 1
	0				// Ability 2
	+5,			// Ability 3
	+10			// Very able
};

BYTE rrcTopCommander[] = {	
	-10,			// Useless
	-5,			// Ability 1
	0				// Ability 2
	+5,			// Ability 3
	+10			// Very able
};

const UWORD MeleeFightTime = 5;		// 5 Minutes


/*==========================================================
 * Melee Results
 */

// If mounted or limbered

UBYTE meleeMountedType[3][4] = {
	{ 5, 4, 6, 0 },		// Infantry: Regular, Miltia, Sharpshooter, (Engineer)
	// { 8, 6, 2, 2 },		// Cavalry: Regular, Militia, Regular Mtd, Militia Mtd
	{ 8, 6, 8, 6 },		// Cavalry: Regular, Militia, Regular Mtd, Militia Mtd
	{ 2, 2, 2, 2 }			// Artillery: 12lb, 6lb, rifled, (siege)
};

// If unmounted / unlimbered

UBYTE meleeUnmountedType[3][4] = {
	{ 5, 4, 6, 0 },		// Infantry: Regular, Miltia, Sharpshooter, (Engineer)
	{ 3, 3, 4, 4 },		// Cavalry: Regular, Militia, Regular Mtd, Militia Mtd
	{ 2, 2, 2, 2 }			// Artillery: 12lb, 6lb, rifled, (siege)
};

BYTE meleeExperience[] = {
	+1,		// Untried
	+0,		// Tried
	+1,		// Seasoned
	+2,		// Veteran
	+0			// Battle Weary
};

BYTE meleeFatigue[] = {
	+1,		// Fresh
	+0,		// Weary
	-1,		// Tired
	-2			// Exhausted
};

BYTE meleeMorale[] = {
	-10,		// Routing (should not happen)
	-2,		// Demoralised
	-1,		// Dispirited
	+0,		// Normal
	+1,		// Good
	+2			// High
};


/*-----------------------------------------------------------
 * Terrain Table: These begin at NORMAL_TERRAIN_START
 *
 * Format is:
 *		colour (0..7)		(Foreground colour)
 *		texture (0..3)
 *		Wooded flag (0..3) 0 = no trees, 3 = heavy trees
 *    Height (0..3) in 7 yard increments (0=flat, 1=7 yards, etc)
 *		Infantry Speed (0..255) { 255= full speed, 0 = impassable }
 *		Cavalry Speed
 *		Artillery Speed
 */

TerrainVal Terrain::terrains[] = {

	/*
	 * 1st are 16 River Terrains
	 */

	{ 7, 0, 0, 0, 64, 128, 32, 0 },
	{ 7, 0, 0, 0, 64, 128, 32, 0 },
	{ 7, 0, 0, 0, 64, 128, 32, 0 },
	{ 7, 0, 0, 0, 64, 128, 32, 0 },
	{ 7, 0, 0, 0, 64, 128, 32, 0 },
	{ 7, 0, 0, 0, 64, 128, 32, 0 },
	{ 7, 0, 0, 0, 64, 128, 32, 0 },
	{ 7, 0, 0, 0, 64, 128, 32, 0 },
	{ 7, 0, 0, 0, 64, 128, 32, 0 },
	{ 7, 0, 0, 0, 64, 128, 32, 0 },
	{ 7, 0, 0, 0, 64, 128, 32, 0 },
	{ 7, 0, 0, 0, 64, 128, 32, 0 },
	{ 7, 0, 0, 0, 64, 128, 32, 0 },
	{ 7, 0, 0, 0, 64, 128, 32, 0 },
	{ 7, 0, 0, 0, 64, 128, 32, 0 },
	{ 7, 0, 0, 0, 64, 128, 32, 0 },

	/*
	 * 16 Surface Road
	 */

	{ 5, 0, 0, 0, 255, 255, 255, 0 },
	{ 5, 0, 0, 0, 255, 255, 255, 0 },
	{ 5, 0, 0, 0, 255, 255, 255, 0 },
	{ 5, 0, 0, 0, 255, 255, 255, 0 },
	{ 5, 0, 0, 0, 255, 255, 255, 0 },
	{ 5, 0, 0, 0, 255, 255, 255, 0 },
	{ 5, 0, 0, 0, 255, 255, 255, 0 },
	{ 5, 0, 0, 0, 255, 255, 255, 0 },
	{ 5, 0, 0, 0, 255, 255, 255, 0 },
	{ 5, 0, 0, 0, 255, 255, 255, 0 },
	{ 5, 0, 0, 0, 255, 255, 255, 0 },
	{ 5, 0, 0, 0, 255, 255, 255, 0 },
	{ 5, 0, 0, 0, 255, 255, 255, 0 },
	{ 5, 0, 0, 0, 255, 255, 255, 0 },
	{ 5, 0, 0, 0, 255, 255, 255, 0 },
	{ 5, 0, 0, 0, 255, 255, 255, 0 },

	/*
	 * 16 Sunken Road
	 */

	{ 4, 3, 0, 0, 255, 255, 255, -2 },
	{ 4, 3, 0, 0, 255, 255, 255, -2 },
	{ 4, 3, 0, 0, 255, 255, 255, -2 },
	{ 4, 3, 0, 0, 255, 255, 255, -2 },
	{ 4, 3, 0, 0, 255, 255, 255, -2 },
	{ 4, 3, 0, 0, 255, 255, 255, -2 },
	{ 4, 3, 0, 0, 255, 255, 255, -2 },
	{ 4, 3, 0, 0, 255, 255, 255, -2 },
	{ 4, 3, 0, 0, 255, 255, 255, -2 },
	{ 4, 3, 0, 0, 255, 255, 255, -2 },
	{ 4, 3, 0, 0, 255, 255, 255, -2 },
	{ 4, 3, 0, 0, 255, 255, 255, -2 },
	{ 4, 3, 0, 0, 255, 255, 255, -2 },
	{ 4, 3, 0, 0, 255, 255, 255, -2 },
	{ 4, 3, 0, 0, 255, 255, 255, -2 },
	{ 4, 3, 0, 0, 255, 255, 255, -2 },

	/*
	 * 16 Railway
	 */

	{ 5, 3, 0, 0, 255, 255, 255, 0 },
	{ 5, 3, 0, 0, 255, 255, 255, 0 },
	{ 5, 3, 0, 0, 255, 255, 255, 0 },
	{ 5, 3, 0, 0, 255, 255, 255, 0 },
	{ 5, 3, 0, 0, 255, 255, 255, 0 },
	{ 5, 3, 0, 0, 255, 255, 255, 0 },
	{ 5, 3, 0, 0, 255, 255, 255, 0 },
	{ 5, 3, 0, 0, 255, 255, 255, 0 },
	{ 5, 3, 0, 0, 255, 255, 255, 0 },
	{ 5, 3, 0, 0, 255, 255, 255, 0 },
	{ 5, 3, 0, 0, 255, 255, 255, 0 },
	{ 5, 3, 0, 0, 255, 255, 255, 0 },
	{ 5, 3, 0, 0, 255, 255, 255, 0 },
	{ 5, 3, 0, 0, 255, 255, 255, 0 },
	{ 5, 3, 0, 0, 255, 255, 255, 0 },
	{ 5, 3, 0, 0, 255, 255, 255, 0 },

	/*
	 * User Defined Terrain Types
	 */

	{ 5, 0, 0, 2, 255, 255, 255, -4 },		// Contains Facility
	{ 1, 2, 3, 1, 255, 255, 255, -4 },		// Heavy Wooded
	{ 1, 3, 2, 1, 255, 255, 255, -2 },		// Light Wooded
	{ 3, 2, 3, 1, 255, 255, 255, -3 },		// Heavy Orchard
	{ 3, 3, 2, 1, 255, 255, 255, -1 },		// Light Orchard
	{ 1, 0, 0, 0, 255, 255, 255,  0 },		// Pasture
	{ 4, 1, 0, 0, 255, 255, 255,  0 },		// Ploughed Field
	{ 6, 2, 0, 0, 255, 255, 255, -1 },		// Corn Field
	{ 2, 3, 0, 0, 255, 255, 255, -1 },		// Wheat Field
	{ 1, 1, 0, 0, 255, 255, 255,  0 },		// Meadow
	{ 1, 0, 0, 0, 255, 255, 255,  0 },		// Campsite
	{ 7, 0, 0, 0, 255, 255, 255,  0 },		// Water
	{ 0, 0, 0, 0, 255, 255, 255,  0 },		// Marsh (Also RiverSide)
	{ 1, 3, 0, 0, 255, 255, 255,  0 },		// Rough
	{ 6, 2, 0, 0, 255, 255, 255, -1 },		// Rocky Desert
	{ 6, 3, 0, 0, 255, 255, 255,  0 },		// Desert

#ifdef BATEDIT
	{ 7, 3, 0, 0, 64, 128, 32 },			// Illegal
#endif

	// If new ones added, please update NormalTerrainCount in terrain.h

};

/*
 * Terrain Colour Table
 *  { Hue, Saturation }
 *
 * Hue (0..255) (0=Red, 85=Green, 171=Blue)
 * Saturation (0..255), 0=Grey, 255=Colour
 */

HueSaturation Terrain::colours[MAX_TERRAIN_COL] = {
#if 0	// Pre-Masters
		{  58, 255 },		// 0: Marsh Colour
		{  79, 255 },		// 1: Fresh Green (Pasture)
		{  65,  97 }, 		// 2: Field: Another green
		{   0,  30 },		// 3: Orchard
		{  40, 100 },		// 4: Ploughed Field / Brown
		{  30,  76 },		// 5: Road and builtup: Brown
		{  40, 200 },		// 6: Yellow
		{ 138, 215 } 		// 7: River: Deep Blue
#else	// Reduce Saturation for Masters
		{  58, 200 },		// 0: Marsh Colour
		{  79, 200 },		// 1: Fresh Green (Pasture)
		{  65,  97 }, 		// 2: Field: Another green
		{   0,  30 },		// 3: Orchard
		{  40, 100 },		// 4: Ploughed Field / Brown
		{  30,  76 },		// 5: Road and builtup: Brown
		{  40, 200 },		// 6: Yellow
		{ 138, 215 } 		// 7: River: Deep Blue
#endif
};

//------------ Old attempts
// {  89,  97 }, 		// 2: Field2: Another green
// {  58, 128 },		// 3: Orchard
// {  25, 163 },		// 4: Wood


/*
 * Number of grid squares used by objects
 */

UBYTE staticObjectSizes[] = {
	2,	2,	1,	1,	2,	2,		// Church, house, tavern
	2,	2,	1,	1,	2,	2,		// Church1, house1, tavern1
	0,	0,	0,	0,	0,	0,		// Trees
	0,	0,	0,	0				// Dead things
};


