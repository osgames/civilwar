/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	The AI battlefield routines to respond to enemy or my actions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * 23rd Jan 1995 : Some debugging by SWG.
 *   1. INCREMENT(rank) and DECREMENT(rank) replaced with
 *      decrementRank and promoteRank so as to avoid modifying
 *      unit's actual ranks!
 *   2. findClosestReserve() debugged.  Local variables were not
 *      initialised (assumed to start at 0).
 *   3. Results of findClosestReserve() checked in case none were found.
 *
 *----------------------------------------------------------------------
 */

#include "ai_bat.h"
#include "trig.cpp"
#include "unit3d.h"
#include "campaign.h"
#include "batldata.h"
#include "map3d.h"
#include "terrain.h"
#include "gametime.h"
#include "measure.h"
#include "game.h"
#include "combval.h"

#ifdef DEBUG
#include "tables.h"
#endif

/*
 *		Call when the player has selected a new destination for one of his
 *		units. Pass it the unit the player is moving.	( Check 1 )
 */

void AI_Battle::NewPlayerClickPoint( Unit* enemy, Unit* loyal )
{

#ifdef DEBUG_CHRIS
	PutInLog( "NewPlayerClickPoint near " );
	if ( loyal->general ) PutInLog( loyal->general->name );
	PutInLog( "\r\n" );
#endif

#ifdef DEBUG
	aiBLog.printf("NewPlayerClickPoint(%s, %s)",
		enemy ? enemy->getName(True) : "Null",
		loyal ? loyal->getName(True) : "Null");
#endif

	/*
	 * Quick check for valid parameters
	 */

	if(!enemy || !enemy->battleInfo || !loyal || !loyal->battleInfo)
		return;


	OrderMode action = ORDER_Max;

	CombatValue loyalStr = calc_UnitCV( loyal, True );
	CombatValue enemyStr = calc_UnitCV( enemy, True );

	Wangle wang;

	BattleOrder ebo = enemy->battleInfo->battleOrder;
	BattleOrder lbo = loyal->battleInfo->battleOrder;

	General* gen;

	if(loyal->isCampaignDetached())
		gen = loyal->general;
	else
		gen = loyal->superior->general;

	/*
	 * 	Check for enemy units already ordered to attack my unit
	 */

	enemyStr += CheckEnemyOrders( loyal, enemy );

	retValue rv = Efficiency_Test( gen, loyalStr, enemyStr, game->gameRand.getB() );

	// TimeBase tempTime;

	for ( int a = 0; a < MAXCorpsAL; a++ )
	{
		if ( time[ a ] && time[ a ] <= battle->gameTime )
		{
			AIReachedClickPoint( Armylist[ a ], a );
			time[ a ].set(0,0);	// = timeToTicks( 0, 0, 0 );
		}
	}

	if(tact >= Withdraw) 
	{
		withDraw(loyal, enemyStr);
		return;
	}

	if ( ebo.mode <= ORDER_Attack || ( ( ebo.mode <= ORDER_Defend ) && ( rv == PASS ) ) )
	{
		if ( rv == PASS )
		{
			ReinforcementsRequested( loyal, enemy, -1 );
		}
		else
		{
			int which = 0;

			if ( loyalStr && (enemyStr / loyalStr >= 4 ) ) 
			{
				withDraw( loyal, enemyStr );
				which = 1;
			}
			else
			{
				int agg = Aggressvalue( loyal );

				if ( (agg < 5) && (agg >= 3) && ( lbo.mode <= ORDER_Attack ) )
				{
					action = ORDER_Hold;
				
					Location batl = findBestDefPos( loyal, enemy );

					sendAIOrder( action, batl, loyal, enemy );
				}
				else if ((agg < 3) && ( lbo.mode <= ORDER_Attack )	) 
				{
					action = ORDER_Stand;

					sendAIOrder( action, loyal->battleInfo->where, loyal, enemy );
				}

				ReinforcementsRequested( loyal, enemy, which );
			}
		}

	}
	else if ( ebo.mode >= ORDER_Withdraw )
	{
		wang = direction( ebo.destination.x - loyal->battleInfo->where.x, ebo.destination.y - loyal->battleInfo->where.y );

		wang -= loyal->battleInfo->formationFacing;

		if ( rv == PASS )
		{
			if ( ( wang > 0x2000 && wang < 0x6000 ) || ( wang < 0xe000 && wang < 0xa000 ) )
			{
				Unit* loyal1 = loyal;

				clearEnemyPointer( enemy );

				return;
			}
			else
			{
			   retValue abil = Ability_Test( loyal->general );

			  	if ( abil == PASS )
				{
					int agg = Aggressvalue( loyal );

					if(agg >= 5)
						action = ORDER_Attack;
					else if(agg >= 3)
						action = ORDER_Advance;
					else
						action = ORDER_CautiousAdvance;

					Unit* toSend = moveSimilarUnit( loyal, enemy, action );

					FillInGap( toSend );
				}
				else if ( abil == FAIL )
				{
					int agg = Aggressvalue( loyal );

					if ( agg >= 5 )
						action = ORDER_Advance;
					else if(agg >= 3)
						action = ORDER_CautiousAdvance;
					// else rotate to face enemy click point and return;

					Unit* toSend = moveSimilarUnit( loyal, enemy, action );

					FillInGap ( toSend );
				}
			}

		}
		else
		{
  			if ( ( wang > 0x2000 && wang < 0x6000 ) || ( wang > 0xa000 && wang < 0xe000 ) )
			{
				int agg = Aggressvalue( loyal );

				if ((agg < 5) && (agg >= 3) && ( lbo.mode <= ORDER_Attack ) )
				{
					action = ORDER_CautiousAdvance;

					sendAIOrder( action, ebo.destination, loyal, enemy );

				}
				else if(agg < 3)
				{
					action = ORDER_Attack;

					sendAIOrder( action, ebo.destination, loyal, enemy );
				}

				ReinforcementsRequested( loyal, enemy, 1 );
			 }
		}
	}
}


/*
 * 	Call when one of my units have reached there destination.
 *		Pass it my unit.											( Check 2 )
 *
 * Bodged up for Master's Edition...
 *		Checks for all AIFlags being clear was making AI grind to a halt.
 *		As long as any AIFlag was set, then this function returned!
 */

void AI_Battle::AIReachedClickPoint( Unit* loyal, int tempT, Boolean skipFlag )
{
#ifdef DEBUG
	aiBLog.printf("AIReachedClickPoint(%s, %d, %s)",
		loyal ? loyal->getName(True) : "Null",
		tempT,
		getBoolStr(skipFlag));
#endif


	if(!loyal || !loyal->battleInfo)
		return;

	Unit* enemy = 0;

	char flank = 0;

	Location loca( 0, 0 );

	Wangle angl;
	Location dest;
	if ( loyal->battleInfo->OrderPresent() ) 
	{
#ifdef DEBUG
	 	aiBLog.printf(" -already has order!!" );
#endif
		return;
	}

	if(loyal->battleInfo->AIFlag)
	{
#ifdef DEBUG
	 	aiBLog.printf(" - AIFlag == True");
#endif

	 	loyal->battleInfo->AIFlag = False;

		Unit* loyal1;

		// The line below is a patch !!!!

		loyal->battleInfo->bSeperated = False;

		// loyal->battleInfo->bJoining = True;

		int corpLoop = MAXCorpsAL;

#if defined(PRE_MASTERS_VERSION)
		while(corpLoop--)
	 	{
			loyal1 = Armylist[ corpLoop ];

			if ( loyal1->battleInfo->AIFlag == True )
			{
#ifdef DEBUG
			 	aiBLog.printf(" - AIFlag still True for %s", loyal1->getName(True));
#endif
				return;
		 	}
		}

		corpLoop = MAXCorpsAL;

		while ( corpLoop-- )
	 	{
			loyal1 = Armylist[ corpLoop ];

			clearSeperated( loyal1 );
		}
#else	// Master's edition
		while ( corpLoop-- )
	 	{
			loyal1 = Armylist[ corpLoop ];

			if(!loyal1->battleInfo->AIFlag)
				clearSeperated(loyal1);
		}
#endif

		sendInitialOrders();
	}

	// if ( loyal->battleInfo->enemy && !loyal->battleInfo->longRoute ) loyal->battleInfo->enemy = 0;

	// if ( !loyal->battleInfo->enemy )
	enemy = FindClosestEnemy( loyal );

	Wangle temp;

	if ( !skipFlag )
	{
		int corpLoop = MAXCorpsAL;

		int num = 0xff;

		while ( corpLoop-- )
	 	{
			Unit* loyal1 = Armylist[ corpLoop ];

#if defined(PRE_MASTERS_VERSION)
			if ( loyal1->battleInfo->AIFlag == True ) return;
#endif
			if ( loyal == loyal1 ) num = corpLoop;
		}

		for ( int a = 0; a < MAXCorpsAL; a++ )
		{
			if ( num != 0xff ) time[ num ].set(0,0);
			
			if ( time[ a ] && time[ a ] <= battle->gameTime )
			{
				if ( Armylist[ a ] != loyal ) AIReachedClickPoint( Armylist[ a ], a, 1 );
				time[ a ].set(0,0);	// = timeToTicks( 0, 0, 0 );
			}
		}
	}

	switch( tact )
	{
		case Frontal_Assault:

#ifdef DEBUG
	 		aiBLog.printf(" - Frontal_Assault");
#endif

			if ( OnBattlefield )
			{
				if ( loyal->battleInfo->inCombat && !tempT )
				{
					if ( !enemy ) sendAIOrder( ORDER_Stand, loyal->battleInfo->where, loyal, loyal->battleInfo->enemy );
					else sendAIOrder( ORDER_Stand, loyal->battleInfo->where, loyal, enemy );

					time[ tempT ] = battle->gameTime + timeToTicks( 0, 40 + game->gameRand(20), 0 );
				}
				else
				{
					if(enemy)		// Added SWG to prevent crash when enemy NULL
						sendAIOrder( ORDER_CautiousAdvance, enemy->battleInfo->where, loyal, enemy );
					else
						sendAIOrder( ORDER_Stand, loyal->battleInfo->where, loyal, loyal->battleInfo->enemy );
				}
			}
			else
			{
				sendAIOrder( ORDER_CautiousAdvance, senior->givenOrders.destination, loyal, loyal->battleInfo->enemy );
			}
			break;

		case Left_Flanking:
#ifdef DEBUG
	 		aiBLog.printf(" - Left_Flanking");
#endif
			flank = 2;
			goto doFlank;
		case Right_Flanking:
#ifdef DEBUG
	 		aiBLog.printf(" - Right_Flanking");
#endif
			flank = 3;
		doFlank:
			if ( OnBattlefield )
			{
			 	if ( LOSandHER( loyal ) ) 
				{
					sendAIOrder( ORDER_Hold, loyal->battleInfo->where, loyal, loyal->battleInfo->enemy );

					time[ tempT ] = battle->gameTime + timeToTicks( 0, 40 + game->gameRand(20), 0 );
				}
				else
				{
					enemy = FindClosestEnemy( 0, flank );

					if(enemy)		// Added SWG to prevent crash when enemy NULL
					{
						loca = enemy->battleInfo->where;

						if ( loyal == Armylist[ 0 ] ) loca = getDestOffset( loyal, loca );

						loca.x -= mcos( Yard( 1000 ), 0x4000 - ang );
						loca.y -= msin( Yard( 1000 ), 0x4000 - ang );

						// sendAIOrder( ORDER_CautiousAdvance, loyal->battleInfo->enemy->battleInfo->where, loyal, loyal->battleInfo->enemy );
						
						sendAIOrder( ORDER_CautiousAdvance, enemy->battleInfo->where, loyal, enemy );
					}
				}
			}
			else
			{
				if(enemy)
					sendAIOrder( ORDER_Advance, senior->givenOrders.destination, loyal, enemy );
			}
			break;

		case Pincer:
#ifdef DEBUG
	 		aiBLog.printf(" - Pincer");
#endif
			if ( OnBattlefield )
			{
				if ( enemy ) loyal->battleInfo->enemy = enemy;

				/*
				 * This was very odd code???
				 * I'm not convinced it does what it should.
				 */

				int counter = 0;

				while(counter < MAXCorpsAL)
				{
					if(loyal == Armylist[counter])
						break;
					counter++;
				}

				if(counter < MAXCorpsAL)
				{
	 				enemy = FindClosestEnemy( 0, counter + 1, loyal );

					if(enemy)
					{
						Location loca = getDestOffset( loyal, enemy->battleInfo->where );

						sendAIOrder( ORDER_CautiousAdvance, loca, loyal, loyal->battleInfo->enemy );

					}
				}
				else
				{
					// if ( AttackFlag )
					{
						Unit* loca2 = FindClosestEnemy( loyal );

						if(loca2)
							sendAIOrder( ORDER_CautiousAdvance, loca2->battleInfo->where, loyal, loca2 );
					}
			   }
			}
			else
			{
				sendAIOrder( ORDER_Advance, senior->givenOrders.destination, loyal, loyal->battleInfo->enemy );
			}
			break;

		case Feint_Left_Right:
#ifdef DEBUG
	 		aiBLog.printf(" - Feint_Left_Right");
#endif
			flank = 2;
			goto doFeint;
		case Feint_Right_Left:
#ifdef DEBUG
	 		aiBLog.printf(" - Feint_Right_Left");
#endif
			flank = 3;
		doFeint:
			if ( OnBattlefield )
			{
				if ( loyal == Armylist[ 0 ] ) 
				{
					Location dest = enemy->battleInfo->where;

					Wangle newWang = direction( dest.x - loyal->battleInfo->where.x, dest.y - loyal->battleInfo->where.y );

					dest.x -= mcos( Yard( 2200 ), newWang );
					dest.y -= msin( Yard( 2200 ), newWang );

					if ( distance ( dest.x - loyal->battleInfo->where.x, dest.y - loyal->battleInfo->where.y ) < 2000 ) sendAIOrder( ORDER_Stand, loyal->battleInfo->where, loyal, loyal->battleInfo->enemy );
					else sendAIOrder( ORDER_CautiousAdvance, dest, loyal, loyal->battleInfo->enemy );
				}
				else 
				{
					// if ( !enemy ) enemy = FindClosestEnemy( loyal );
					
					enemy = FindClosestEnemy( loyal );

					if(enemy)
						sendAIOrder( ORDER_CautiousAdvance, enemy->battleInfo->where, loyal, loyal->battleInfo->enemy );
				}
			}
			else
			{
			  /*	if ( loyal == Armylist[ 0 ] ) 
				{
					sendAIOrder( ORDER_Stand, loyal->battleInfo->where, loyal, loyal->battleInfo->enemy );
				}

				else
				*/
				sendAIOrder( ORDER_CautiousAdvance, senior->givenOrders.destination, loyal, loyal->battleInfo->enemy );
			}
			break;
					
		case Line_Defence:
#ifdef DEBUG
	 		aiBLog.printf(" - Line_Defence");
#endif
			sendAIOrder( ORDER_Hold, loyal->battleInfo->where, loyal, loyal->battleInfo->enemy );
			break;
					
		case Defence_In_Depth:
#ifdef DEBUG
	 		aiBLog.printf(" - Defence_In_Depth");
#endif
			sendAIOrder( ORDER_Hold, loyal->battleInfo->where, loyal, loyal->battleInfo->enemy );
			break;

		case Feint_Defence:
#ifdef DEBUG
	 		aiBLog.printf(" - Feint_Defence");
#endif
			sendAIOrder( ORDER_Hold, loyal->battleInfo->where, loyal, loyal->battleInfo->enemy );
			break;

		case Holding_Action:
#ifdef DEBUG
	 		aiBLog.printf(" - Holding_Action");
#endif
			sendAIOrder( ORDER_Hold, loyal->battleInfo->where, loyal, loyal->battleInfo->enemy );
			break;

		case Covering_Action:
#ifdef DEBUG
	 		aiBLog.printf(" - Covering_Action");
#endif
			temp = ang - loyal->battleInfo->menFacing;
			if ( temp > 0x8000 ) temp += 0x8000;

			if ( OnBattlefield )
			{
				sendAIOrder( ORDER_Hold, loyal->battleInfo->where, loyal, loyal->battleInfo->enemy );
			}
			else
			{
				if ( temp < 0x4000 )
				{
					Location loca( loyal->battleInfo->where.x, loyal->battleInfo->where.y );
					loca.x -= msin( 5000, ang );
					loca.y -= mcos( 5000, ang );

					sendAIOrder( ORDER_Withdraw, loca, loyal, loyal->battleInfo->enemy );
				}
				else
				{
					sendAIOrder( ORDER_Withdraw, senior->givenOrders.destination, loyal, loyal->battleInfo->enemy );
				}
			}
			break;

		case Withdraw:
#ifdef DEBUG
	 		aiBLog.printf(" - Withdraw");
#endif
			if (!enemy || loyal->getTopCommand()->battleInfo->followingOrder || IsArmyOffMainBF( loyal ) )
				return;

			angl = direction( loyal->battleInfo->where.x - enemy->battleInfo->where.x, loyal->battleInfo->where.y - enemy->battleInfo->where.y );

			dest = loyal->battleInfo->where;

			dest.x -= msin( Mile(4), angl );
			dest.y -= mcos( Mile(4), angl );

			angl = direction( loyal->battleInfo->where.x - senior->givenOrders.destination.x, loyal->battleInfo->where.y - senior->givenOrders.destination.y );

			if ( !OnBattlefield && IsEnemyInWay( loyal, angl ) )
			{
				sendAIOrder( ORDER_Withdraw, senior->givenOrders.destination, loyal, loyal->battleInfo->enemy );
			}
			else
			{
				sendAIOrder( ORDER_Withdraw, senior->givenOrders.destination, loyal, loyal->battleInfo->enemy );
			}
			break;

		case Retreat:
#ifdef DEBUG
	 		aiBLog.printf(" - Retreat");
#endif
			if (!enemy || loyal->getTopCommand()->battleInfo->followingOrder || IsArmyOffMainBF( loyal ) )
				return;
		
			angl = direction( loyal->battleInfo->where.x - enemy->battleInfo->where.x, loyal->battleInfo->where.y - enemy->battleInfo->where.y );

			dest = loyal->battleInfo->where;

			dest.x -= msin( Mile(4), angl );
			dest.y -= mcos( Mile(4), angl );

			sendAIOrder( ORDER_Retreat, dest, loyal, loyal->battleInfo->enemy );

			break;

		default:
#ifdef DEBUG
			throw GeneralError( "Battle tactic is in ERROR!!");
#endif
			break;
	}
}

/*
 *  To be called by battlefield machanics when an AI unit begins to 
 *	 withdraw. Pass it the withdrawing unit.				( Check 3 )
 */

void AI_Battle::UnitWithdrawing( Unit* reg )
{
#ifdef DEBUG
	aiBLog.printf("unitWithdrawing(%s)",
		reg ? reg->getName(True) : "Null");
#endif

 // Check to see how many units are within firing distance and find
 // the enemy unit.

 	for ( int a = 0; a < MAXCorpsAL; a++ )
	{
		if ( time[ a ] && time[ a ] <= battle->gameTime )
		{
			AIReachedClickPoint( Armylist[ a ], a );
			time[ a ].set(0,0);	// = timeToTicks( 0, 0, 0 );
		}
	}

#ifdef DEBUG_CHRIS
	if ( reg->general ) PutInLog( reg->general->name );
	PutInLog( " is withdrawing\r\n" );
#endif

	if ( reg->battleInfo->followingOrder || IsArmyOffMainBF( reg ) ) return;

	if ( tact >= Withdraw )
	{
	   Unit* enemy = FindClosestEnemy( reg );

		if(enemy)
		{
			Wangle angl = direction( reg->battleInfo->where.x - enemy->battleInfo->where.x, reg->battleInfo->where.y - enemy->battleInfo->where.y );

			Location dest = reg->battleInfo->where;

			Distance d = distanceLocation( reg->battleInfo->where, reg->battleInfo->battleOrder.destination );

			if ( d > Mile(1) ) return;

			dest.x -= msin( Mile(5), angl );
			dest.y -= mcos( Mile(5), angl );

			Wangle wangl = direction( reg->battleInfo->where.x - senior->givenOrders.destination.x, reg->battleInfo->where.y - senior->givenOrders.destination.y );

			if ( ( ( (angl - wangl) < 0xc000 ) && ( ( angl - wangl ) > 0x4000 ) ) && !OnBattlefield && !IsEnemyInWay( reg, wangl ) )
			{
				sendAIOrder( ORDER_Withdraw, senior->givenOrders.destination, reg, enemy );
			}
			else sendAIOrder( ORDER_Withdraw, dest, reg, enemy );
		}

		return;
	}


	if ( !reg->battleInfo->enemy ) 
	{
	#ifdef DEBUG_CHRIS
		PutInLog( "No enemy pointer\r\n" );
	#endif
		return;
	}

	int agg = Aggressvalue( reg );

	if ( agg >= 5 ) return;

	Unit* loyal1;
	Unit* loyal2;
 	Unit* loyal3;
 	Unit* closest;

 	int corpLoop = MAXCorpsAL;

	Distance dx, dy, d;

	CombatValue enemyStr = calc_UnitCV( reg->battleInfo->enemy, True );
	CombatValue loyalStr = 0;

	// Location enemyLoc = reg->battleInfo->enemy->battleInfo->where;

 	while ( corpLoop-- )
 	{
		loyal1 = Armylist[ corpLoop ];

		if ( loyal1->rank != Rank_Brigade && loyal1->battleInfo->inCombat )
 		{
			loyal2 = loyal1->child;

			while ( loyal2 )
			{
				if ( loyal2->rank != Rank_Brigade && loyal2->battleInfo->inCombat )
				{
					loyal3 = loyal2->child;

					while( loyal3 )
					{
						if ( loyal3->battleInfo->enemy == reg->battleInfo->enemy )
						{
							loyalStr += calc_UnitCV( loyal3, True );
						}
						loyal3 = loyal3->sister;
					}
				}
				else if ( loyal2->battleInfo->enemy == reg->battleInfo->enemy )
				{
					loyalStr += calc_UnitCV( loyal2, True );
				}
				loyal2 = loyal2->sister;
			}
		}
		else if ( loyal1->battleInfo->enemy == reg->battleInfo->enemy )
		{
			loyalStr += calc_UnitCV( loyal1, True );
		}
	}

	retValue rv = Efficiency_Test( loyal1->general, loyalStr, enemyStr, game->gameRand.getB() );
 
	if ( (rv == PASS) && (agg < 3) ) ReinforcementsRequested( reg, reg->battleInfo->enemy, 0 );
}


/*
 * 	Get reinforcements from reserves						( Check 4 )
 */

void AI_Battle::ReinforcementsRequested( Unit* loyal, Unit* enemy, signed int which )
{
#ifdef DEBUG
	aiBLog.printf("ReinforcementsRequested(%s, %s, %d)",
		loyal ? loyal->getName(True) : "Null",
		enemy ? enemy->getName(True) : "Null",
		which);
#endif

	OrderMode action;

	Unit* closest;

	if ( which == -1 )
	{
		retValue abil = Ability_Test( loyal->general );

		if ( abil == PASS )
		{
			int agg = Aggressvalue( loyal );

			if(agg >= 5)
				action = ORDER_Attack;
			else if(agg >= 3 )
				action = ORDER_Advance;
			else
				action = ORDER_CautiousAdvance;

		}
		else if ( abil == FAIL )
		{
			int agg = Aggressvalue( loyal );

			if ( agg >= 5 )
				action = ORDER_Advance;
			else if ( agg >= 3 )
				action = ORDER_CautiousAdvance;
			// else rotate to face enemy click point and return;

		}
		else
			return;

		if ( !CheckCommander( loyal, enemy, action ) )
		{
			getReinforcements( enemy, action, -1 );
		}
	}
	else if ( which == 1 || which == 0 )
	{
		retValue abil = Ability_Test( loyal->general );

		if ( abil == PASS )
		{
			int agg = Aggressvalue( loyal );

			if(agg >= 5)
				action = ORDER_Attack;
			else if(agg >= 3)
				action = ORDER_Advance;
			else
				action = ORDER_CautiousAdvance;

		}
		else if ( abil == FAIL )
		{
			int agg = Aggressvalue( loyal );

			if ( agg >= 5 )
				action = ORDER_Advance;
			else if ( agg >= 3 )
				action = ORDER_CautiousAdvance;
			// else rotate to face enemy click point and return;

		}
		else return;

		if ( !CheckCommander( loyal, enemy, action ) )
		{
			getReinforcements( enemy, action, which );
		}

	}
}

/*
 *  Currently this function only returns the highest position between
 *  the players and AI's unit.
 */

Location AI_Battle::findBestDefPos( Unit* loyal, Unit* enemy )
{
#ifdef DEBUG
	aiBLog.printf("findBestDefPos(%s, %s)",
		loyal ? loyal->getName(True) : "Null",
		enemy ? enemy->getName(True) : "Null");
#endif

	Wangle wang;

	Grid* gr = battle->grid->getGrid( Zoom_1 );

	Location el = enemy->battleInfo->where;
	Location ll = loyal->battleInfo->where;

	BattleOrder ebo = enemy->battleInfo->battleOrder;
	BattleOrder lbo = loyal->battleInfo->battleOrder;

	wang = direction( ebo.destination.x - ll.x, ebo.destination.y - ll.y );

	int egx, egy, lgx, lgy;

	egx = DistToBattle( el.x ) + battle->grid->getTotalWidth()  / 2;
	egy = DistToBattle( el.y ) + battle->grid->getTotalHeight() / 2;

	lgx = DistToBattle( ll.x ) + battle->grid->getTotalWidth()  / 2;
	lgy = DistToBattle( ll.y ) + battle->grid->getTotalHeight() / 2;

	egx /= gr->gridSize;
	egy /= gr->gridSize;

	lgx /= gr->gridSize;
	lgy /= gr->gridSize;

	Height8* hibest;
	Height8* hi;

	int bestx, besty;

	if ( ( wang > 0x2000 && wang < 0x6000 ) || ( wang > 0xa000 && wang < 0xe000 ) )
 	{
		signed int incre = 1;

		if ( lgx - egx < 0 ) incre = -1;

		for ( int dx = 0; dx < ( lgx - egx) / 2; dx += incre )
		{
			for ( int dy = -4; dy < 4; dy++ )
 			{
			 	hi = gr->getHSquare( lgx + dx, lgy + dy );

				if ( !hibest || hi->height > hibest->height )
				{
					hibest = hi;

					bestx = lgx + dx;
					besty = lgy + dy;
				}
			}
		}
	}
	else
	{
		signed int incre = 1;

		if ( lgy - egy < 0 ) incre = -1;

		for ( int dy = 0; dy < (lgy - egy) /2 ; dy += incre )
		{
			for ( int dx = -4; dx < 4; dx++ )
 			{
			 	hi = gr->getHSquare( lgx + dx, lgy + dy );

				if ( !hibest || hi->height > hibest->height )
				{
					hibest = hi;

					bestx = lgx + dx;
					besty = lgy + dy;
				}
			}
		}

	}

	Location loca( BattleToDist( bestx * gr->gridSize - battle->grid->getTotalWidth() / 2 ), BattleToDist( besty * gr->gridSize - battle->grid->getTotalHeight() / 2 ) );

	return loca;
}


/*
 * 	Move similar number of troops as the enemy has to this attack enemy.
 */

Unit* AI_Battle::moveSimilarUnit( Unit* loyal, Unit* enemy, OrderMode action )
{
#ifdef DEBUG
	aiBLog.printf("moveSimilarUnit(%s, %s, %s)",
		loyal ? loyal->getName(True) : "Null",
		enemy ? enemy->getName(True) : "Null",
		getOrderStr(action));
#endif

	Unit* toSend = loyal;

	if ( toSend->rank <= enemy->rank )
	{
		sendAIOrder( action, enemy->battleInfo->where, loyal, enemy );

		return loyal;
	}

	while( toSend->rank != enemy->rank && toSend->rank != Rank_Brigade )
	{
		toSend = toSend->child;
	}

	sendAIOrder( action, enemy->battleInfo->where, toSend, enemy );

	return toSend;
}


/*
 * 	Move reserve troops of a number equal in size 
 * 	to the moving loyal troops, and to the loyal troops
 *		current position.
 */
	
void AI_Battle::FillInGap ( Unit* loyal )
{
#ifdef DEBUG
	aiBLog.printf("FillInGap(%s)",
		loyal ? loyal->getName(True) : "Null");
#endif

	Unit* reserve = findClosestReserve( loyal, loyal->rank );

	if(reserve)
		sendAIOrder( ORDER_CautiousAdvance, loyal->battleInfo->where, reserve );

}

/*
 * 	Try to send commanders troops that are currently not within 
 *		firing range.
 */

Boolean AI_Battle::CheckCommander( Unit* loyal, Unit* enemy, OrderMode action )
{
#ifdef DEBUG
	aiBLog.printf("CheckCommander(%s, %s, %s)",
		loyal ? loyal->getName(True) : "Null",
		enemy ? enemy->getName(True) : "Null",
		getOrderStr(action));
#endif

	if ( loyal->rank == Rank_Brigade ) return False;

	CombatValue eStrength = calc_UnitCV( enemy, True );
	CombatValue lStrength = calc_UnitCV( loyal, True );

	Distance fireRange = getfireRange( enemy );

	Unit* loyal1 = loyal->child;

	CombatValue strSent = 0;

	/*
	 * Modification by SWG:
	 *  Pick a point midway between units location and destination
	 */

	Location loc = (enemy->battleInfo->battleOrder.destination + enemy->battleInfo->where) / 2;


	while ( loyal1 )
	{
		if ( loyal1->rank != Rank_Brigade && loyal1->battleInfo->inCombat )
 		{
			Unit* loyal2 = loyal1->child;

			while ( loyal2 )
			{
				if ( loyal2->rank != Rank_Brigade && loyal2->battleInfo->inCombat )
				{
					Unit* loyal3 = loyal2->child;

					while( loyal3 )
					{
						if ( ( loyal3->battleInfo->battleOrder.mode >= ORDER_Stand && loyal3->battleInfo->battleOrder.mode <= ORDER_Withdraw ) )
						{
							sendAIOrder( action, loc, loyal3, enemy );

							strSent+= calc_UnitCV( loyal3, True );

							if ( strSent > eStrength ) return True;
						}

						loyal3 = loyal3->sister;
					}
				}
				else if ( !loyal2->battleInfo->inCombat && ( ( loyal2->battleInfo->battleOrder.mode >= ORDER_Stand && loyal2->battleInfo->battleOrder.mode <= ORDER_Withdraw ) ) )
				{
					sendAIOrder( action, loc, loyal2, enemy );

					strSent += calc_UnitCV( loyal2, True );

					if ( strSent > eStrength ) return True;
				}
				loyal2 = loyal2->sister;
			}
		}																																 // || check to see if enemy is the one I'm attacking
		else if ( !loyal1->battleInfo->inCombat && ( ( loyal1->battleInfo->battleOrder.mode >= ORDER_Stand && loyal1->battleInfo->battleOrder.mode <= ORDER_Withdraw ) ) )
		{
			sendAIOrder( action, loc, loyal1, enemy );

			strSent += calc_UnitCV( loyal1, True );

			if ( strSent > eStrength ) return True;
		}
		loyal1 = loyal1->sister;
	}
	
	if ( strSent > eStrength ) return True;

	return False;
}

void AI_Battle::getReinforcements( Unit* enemy, OrderMode action, signed int which )
{
#ifdef DEBUG
	aiBLog.printf("getReinforcements(%s, %s, %d)",
		enemy ? enemy->getName(True) : "Null",
		getOrderStr(action),
		which);
#endif

	Unit* reserve;   // = findClosestReserve( enemy );

	if ( which == -1 || which == 0 )
	{
		if ( enemy->rank >= Rank_Brigade ) reserve = findClosestReserve( enemy, Rank_Brigade );
		reserve = findClosestReserve( enemy, promoteRank( enemy->rank ) );
		/*
		 * NOTE: The above line was changed by SWG to fix a bug
		 * It previously used INCREMENT(enemy->rank), which actually
		 * modified the enemy's rank!
		 *
		 * Looking at the logic I assume promote is correct rather than
		 * demote so that for example a corps is sent against a division,
		 * even though promoteRank is actual rank-1
		 */

	}
	else
	{
		reserve = findClosestReserve( enemy, enemy->rank );
	}

	if(reserve)	// Added by SWG
		sendAIOrder( action, enemy->battleInfo->where, reserve, enemy );
	
}

/*
 * This routine debugged by SWG, 23rd Jan 95
 *
 * closest and closestDist were not initialised, but the inner
 * tests were checking for !closestDist.
 *
 * I have initialised closest to 0, and checked for !closest instead.
 */

Unit* AI_Battle::findClosestReserve( Unit* enemy, Rank of )
{
#ifdef DEBUG
	aiBLog.printf("findClosestReserve(%s, %d)",
		enemy ? enemy->getName(True) : "Null",
		(int) of);
#endif

	Distance dx, dy, d, closestDist;

	Unit* loyal1;
	Unit* loyal2;
	Unit* loyal3;
	Unit* closest = 0;

	int corpLoop = MAXCorpsAL;

	while ( corpLoop-- )
	{
		loyal1 = Armylist[ corpLoop ];

		if ( loyal1->rank < of ) 
 		{
			loyal2 = loyal1->child;

			while ( loyal2 )
			{
				if ( loyal2->rank < of )
				{
					loyal3 = loyal2->child;

					while( loyal3 )
					{
					  	dx = enemy->battleInfo->where.x - loyal3->battleInfo->where.x;
						dy = enemy->battleInfo->where.y - loyal3->battleInfo->where.y;
		
						d = distance( dx, dy );

						if ( ( !closest || ( d < closestDist ) ) && !loyal3->battleInfo->inCombat && !loyal3->battleInfo->enemy )
						{
						 	closestDist = d;

							closest = loyal3;
						}
						loyal3 = loyal3->sister;
					}
				}
				else
				{
				  	dx = enemy->battleInfo->where.x - loyal2->battleInfo->where.x;
					dy = enemy->battleInfo->where.y - loyal2->battleInfo->where.y;
		
					d = distance( dx, dy );

					if ( ( !closest || ( d < closestDist ) ) && !loyal2->battleInfo->inCombat && !loyal2->battleInfo->enemy  )
					{
					 	closestDist = d;

						closest = loyal2;
					}
				}
				loyal2 = loyal2->sister;
			}
		}
		else
		{
		  	dx = enemy->battleInfo->where.x - loyal1->battleInfo->where.x;
		 	dy = enemy->battleInfo->where.y - loyal1->battleInfo->where.y;
		
		 	d = distance( dx, dy );

		 	if ( ( !closest || ( d < closestDist ) ) && !loyal1->battleInfo->inCombat && !loyal1->battleInfo->enemy )
		 	{
		 	 	closestDist = d;

		 		closest = loyal1;
		 	}
		}
	}

	return closest;
}

void AI_Battle::sendAIOrder( OrderMode action, const Location& dest, Unit* loyal, Unit* enemy )
{
#ifdef DEBUG
	aiBLog.printf("sendAIOrder(%s, %s, %s)",
		getOrderStr(action),
		loyal ? loyal->getName(True) : "Null",
		enemy ? enemy->getName(True) : "Null");
#endif


	if ( ( dest.x == -1 ) || ( dest.y == -1 ) )
	{

	#ifdef DEBUG_CHRIS
		PutInLog( "Trying to send order to -1" );
	#endif

		return;
	}

	Location dloc( dest.x, dest.y );

	// if ( enemy && distanceLocation( dest, enemy->battleInfo->where ) < 1000 && distanceLocation( enemy->battleInfo->battleOrder.destination, enemy->battleInfo->where ) )

	if ( enemy && action < ORDER_Stand && distanceLocation( enemy->battleInfo->battleOrder.destination, enemy->battleInfo->where ) )
	{
	 	if ( enemy->battleInfo->battleOrder.mode < ORDER_Stand || enemy->battleInfo->battleOrder.mode > ORDER_Defend )
		{
			Wangle ewang = direction( enemy->battleInfo->battleOrder.destination.x - enemy->battleInfo->where.x, enemy->battleInfo->battleOrder.destination.y - enemy->battleInfo->where.y );
		  
			Wangle lwang = direction( enemy->battleInfo->where.x - loyal->battleInfo->where.x, enemy->battleInfo->where.y - loyal->battleInfo->where.y );

			if ( ( abs( ewang - lwang ) <= 0x4000 ) || ( abs( ewang - lwang ) >= 0xc000 ) ) 
			{
			 	dloc = enemy->battleInfo->battleOrder.destination;
			}
			else
			{
				//Distance d = mcos( distanceLocation( dest, loyal->battleInfo->where ), 0x4000 + ewang );

				Distance d = distanceLocation( dest, loyal->battleInfo->where );

				if ( d < distanceLocation( enemy->battleInfo->battleOrder.destination, enemy->battleInfo->where ) )
				{
			 		dloc = enemy->battleInfo->battleOrder.destination;
				}
				else
				{
		 			int xdif = mcos( d, ang );
		 			int ydif = msin( d, ang );

					dloc.x = dest.x + xdif;
					dloc.y = dest.y + ydif;
				}
			}
		}
	}

#ifdef DEBUG_CHRIS
	if ( loyal->general ) PutInLog( loyal->general->name );
	PutInLog( " sent to ");
	PutOrderMode( action );
	if ( enemy ) 
	{
		PutInLog( " wrt " );
		if ( enemy->general ) PutInLog( enemy->general->name );
	}
	PutInLog ( " from " );

	Grid* gr = battle->grid->getGrid( Zoom_1 );

	Location el = loyal->battleInfo->where;

	int egx, egy;

	egx = DistToBattle( el.x ) + battle->grid->getTotalWidth()  / 2;
	egy = DistToBattle( el.y ) + battle->grid->getTotalHeight() / 2;

	egx /= gr->gridSize;
	egy /= gr->gridSize;

	PutInLog( egx );
	PutInLog( "," );
	PutInLog( egy );

	PutInLog ( " to " );

	el = dloc;

	egx = DistToBattle( el.x ) + battle->grid->getTotalWidth()  / 2;
	egy = DistToBattle( el.y ) + battle->grid->getTotalHeight() / 2;

	egx /= gr->gridSize;
	egy /= gr->gridSize;

	if ( egx < 0 || egx > gr->gridCount || egy < 0 || egy > gr->gridCount )
	{
		PutInLog( "leave the battlefield to " );
	}

	PutInLog( egx );
	PutInLog( "," );
	PutInLog( egy );

	PutInLog( "\r\n" );
#endif

	if ( ( action < ORDER_Stand ) && distance( loyal->battleInfo->where.x - dloc.x, loyal->battleInfo->where.y - dloc.y ) < 2000 ) action = ORDER_Hold;

	SentBattleOrder* order;

	order = new SentBattleOrder( action, dloc, battle->gameTime );

	loyal->battleInfo->sendBattleOrder( order );

	loyal->battleInfo->enemy = enemy;
}

/*
 * 	Call this whenever a unit reaches its destination. If the enemy 
 *		pointer is non-zero set longRoute to True;
 */

Location AI_Battle::findClosestLow( Unit* loyal, Unit* enemy, Boolean Highest )
{
#ifdef DEBUG
	aiBLog.printf("findClosestLow(%s, %s, %s)",
		loyal ? loyal->getName(True) : "Null",
		enemy ? enemy->getName(True) : "Null",
		getBoolStr(Highest));
#endif

	signed int loop;

	Grid* gr = battle->grid->getGrid( Zoom_1 );

	Location el = enemy->battleInfo->where;
	
#ifdef DEBUG_CHRIS
	if ( !enemy || !enemy->battleInfo ) PutInLog( "\r\nEnemy is wrong!!\r\n" );
#endif

	Location ll = loyal->battleInfo->where;

	int lgx, lgy;

	lgx = DistToBattle( ll.x ) + battle->grid->getTotalWidth()  / 2;
	lgy = DistToBattle( ll.y ) + battle->grid->getTotalHeight() / 2;

	lgx /= gr->gridSize;
	lgy /= gr->gridSize;

	if ( ( lgx > gr->gridCount ) || ( lgx < 0 ) || ( lgy > gr->gridCount ) || ( lgy < 0 ) ) return enemy->battleInfo->where;

	Wangle alpha = direction( el.x - ll.x, el.y - ll.y );

	Distance d = distance( el.x - ll.x, el.y - ll.y );

	int radius = ( ( DistToBattle( d ) + battle->grid->getTotalWidth() / 2 ) / gr->gridSize ) / 2;

	int incradius = 1;

	if ( radius < 0 ) incradius = -1;
	else if ( abs( radius ) <= 3 )
	{
		AttackFlag = True;
		return el;
	}

	Wangle aa = Degree( 13 );

	int sin26 = sin( aa );

	int cos26 = cos( aa );

	// const int ta = cos( aa ) / sin( aa );

	// int gh = ta * radius;

	// int gamma = 0x2000 - alpha ;

	int grx, gry, tgrx, tgry;

	int cosa = cos ( alpha );

	int sina = sin ( alpha );

	// int tana = 1;
	
	// if ( !cosa ) tana = sina / cosa;

	// if ( !tana ) tana = 1;

	Height8* hi;

	Height8* hibest = 0;

	int bestx, besty;

	Height8* lowbest = 0;

	int lowbestx, lowbesty;

	if ( ( alpha > 0x2000 && alpha < 0x6000 ) || ( ( alpha > 0xA000 ) && ( alpha < 0xe000 ) ) )
	{
		for ( int crow = 2; crow < radius; crow += incradius )
		{
			int sing = msin( crow, alpha );

			int dy = ( ( crow * sin26 / cos26 ) * sina ) / 0x4000;

			if ( !dy ) dy = 1;

			// grx = ( cosa * crow ) / 0x4000 + lgx;
			// gry = ( sina * crow ) / 0x4000 + lgy;

			if ( (sing + dy) < (sing - dy) ) dy = -dy;

			for ( int xloop = sing - dy; xloop < sing + dy; xloop++ ) 
			{
				tgry = lgy + ( crow * sina - xloop * cosa ) / 0x4000;		// crow * sina / 0x4000 + xloop / tana + lgy;

				tgrx = lgx + ( crow * cosa - xloop * sina ) / 0x4000;

				if ( ( tgrx < gr->gridCount ) && ( tgrx > 0 ) && ( tgry < gr->gridCount ) && ( tgry > 0 ) )
				{
					hi = gr->getHSquare( tgrx, tgry );

					if ( !hibest || hi->height >= hibest->height )
					{
						if ( hi->height == hibest->height && !battle->data3D.terrain.isWooded(*gr->getTSquare( tgrx, tgry )) ) continue;

						hibest = hi;

						bestx = tgrx;
						besty = tgry;
					}
					else if ( !lowbest || hi->height <= lowbest->height )
					{
						if ( hi->height == lowbest->height && !battle->data3D.terrain.isWooded(*gr->getTSquare( tgrx, tgry ) )) continue;

						lowbest = hi;

						lowbestx = tgrx;
						lowbesty = tgry;
					}
				}
			}
		}
	}
	else
	{
		for ( int crow = 2; crow < radius; crow += incradius )
		{
			int sing = mcos( crow, alpha );

			int dy = ( ( crow * sin26 / cos26 ) * cosa ) / 0x4000;

			if ( !dy ) dy = 1;

			// grx = ( cosa * crow ) / 0x4000 + lgx;
			// gry = ( sina * crow ) / 0x4000 + lgy;

			if ( (sing + dy) < (sing - dy) ) dy = -dy;

			for ( int yloop = sing - dy; yloop < sing + dy; yloop++ ) 
			{
				tgry = lgy + ( crow * sina - yloop * cosa ) / 0x4000;		// crow * sina / 0x4000 + xloop / tana + lgy;

				tgrx = lgx + ( crow * cosa - yloop * sina ) / 0x4000;

				if ( ( tgrx < gr->gridCount ) && ( tgrx > 0 ) && ( tgry < gr->gridCount ) && ( tgry > 0 ) )
				{
					hi = gr->getHSquare( tgrx, tgry );

					if ( !hibest || hi->height >= hibest->height )
					{
						if ( hi->height == hibest->height && !battle->data3D.terrain.isWooded(*gr->getTSquare( tgrx, tgry )) ) continue;

						hibest = hi;

						bestx = tgrx;
						besty = tgry;
					}
					else if ( !lowbest || hi->height <= lowbest->height )
					{
						if ( hi->height == hibest->height && !battle->data3D.terrain.isWooded(*gr->getTSquare( tgrx, tgry ) ) ) continue;

						lowbest = hi;

						lowbestx = tgrx;
						lowbesty = tgry;
					}
				}
			}
		}
	}

	// Need a function to convert between grid and battlecoordinates;

	if ( ( !hibest ) && ( !lowbest ) )
	{
#ifdef DEBUG_CHRIS
		PutInLog( "\r\nERROR: No terrain height found!!\r\n" );
#endif
		return ( Location )enemy->battleInfo->where;
	}

	Location loca( lowbestx * gr->gridSize - battle->grid->getTotalWidth() / 2, lowbesty * gr->gridSize - battle->grid->getTotalHeight() / 2 );

	if ( abs( d ) < Mile( 1 ) )
	{
		loca = enemy->battleInfo->where;
	}
	else if ( abs( d ) < Mile( 2 ) || !lowbest ) 
	{
		loca.x = BattleToDist( bestx * gr->gridSize - battle->grid->getTotalWidth()  / 2 );
		loca.y = BattleToDist( besty * gr->gridSize - battle->grid->getTotalHeight() / 2 );
	}

	return loca;
}

void AI_Battle::sendInitialOrders( Boolean initial )
{
#ifdef DEBUG
	aiBLog.printf("sendInitialOrders(%s)",
		getBoolStr(initial));
#endif

	if(!MAXCorpsAL)
		return;

	Location loca( 0, 0 );

	Unit* loyal1 = Armylist[ 0 ];

	int corpLoop = MAXCorpsAL;
	int lgx, lgy;
	Grid* gr = battle->grid->getGrid( Zoom_1 );

	Unit* enemy;
	
	Wangle angl;
	Wangle wangl;
	Location dest;

	Boolean flank = 0;

  // 	enemy = FindClosestEnemy( loyal1 );

	GameTicks tempTime;

	switch( tact )
	{
		case Frontal_Assault:
#ifdef DEBUG
	 		aiBLog.printf(" - Frontal_Assault");
#endif
		  enemy = FindClosestEnemy( loyal1 );

		  if(enemy)
		  {
		  	if ( initial || LOSandHER() )
		  	{
					tempTime = timeToTicks( 0, 60 + game->gameRand(60), 0 );
				
					while ( corpLoop-- )
					{
						loyal1 = Armylist[ corpLoop ];

						if ( loyal1->battleInfo->OrderPresent() ) 
						{
							if ( initial )	loyal1->battleInfo->AIFlag = False;
							continue;
						}

						if ( initial && distance( loyal1->battleInfo->where.x - loyal1->battleInfo->battleOrder.destination.x, loyal1->battleInfo->where.y - loyal1->battleInfo->battleOrder.destination.y ) < 2000 )
						{
							if ( initial ) loyal1->battleInfo->AIFlag = False;

							sendAIOrder( ORDER_Hold, loyal1->battleInfo->where, loyal1, enemy );

							time[ corpLoop ] = battle->gameTime + tempTime;
						}
						else
						{
							// sendAIOrder( ORDER_Stand, loyal1->battleInfo->where, loyal1, enemy );
							sendAIOrder( ORDER_CautiousAdvance, loyal1->battleInfo->battleOrder.destination, loyal1, enemy );

							time[ corpLoop ].set(0, 0);
							loyal1->battleInfo->AIFlag = True;
						}
					}
				}
				else
				{
					while ( corpLoop-- )
					{
						loyal1 = Armylist[ corpLoop ];
					
						if ( loyal1->battleInfo->OrderPresent() ) 
						{
							if ( initial )	loyal1->battleInfo->AIFlag = False;
							continue;
						}

						if ( OnBattlefield )
						{
							//if ( corpLoop <= 3 ) sendAIOrder( ORDER_CautiousAdvance, enemy->battleInfo->where, loyal1, enemy );
							//else 
							//{
								loca = enemy->battleInfo->where;

								loca = KeepFormation( corpLoop, loca, 1 );

								sendAIOrder( ORDER_CautiousAdvance, loca, loyal1, enemy );
							//}

							loyal1->battleInfo->AI_action = 2;
						}
						else
						{
							sendAIOrder( ORDER_CautiousAdvance, senior->givenOrders.destination, loyal1, 0 );
							loyal1->battleInfo->AI_action = 2;
						}
					}
				}
			}
			break;
		case Left_Flanking:
#ifdef DEBUG
	 		aiBLog.printf(" - Left_Flanking");
#endif
			flank = 2;
			goto doFlank;
		case Right_Flanking:
#ifdef DEBUG
	 		aiBLog.printf(" - Right_Flanking");
#endif
		 	flank = 3;
		doFlank:

		   if ( initial || LOSandHER() )
		   {
		  		enemy = FindClosestEnemy( loyal1 );
				if(enemy)
				{
					tempTime = timeToTicks( 0, 60 + game->gameRand(20), 0 );
				
					while ( corpLoop--  )
					{
						loyal1 = Armylist[ corpLoop ];

						if ( initial && distance( loyal1->battleInfo->where.x - loyal1->battleInfo->battleOrder.destination.x, loyal1->battleInfo->where.y - loyal1->battleInfo->battleOrder.destination.y ) < 1800 )
						{
							if ( initial ) loyal1->battleInfo->AIFlag = False;
							sendAIOrder( ORDER_Hold, loyal1->battleInfo->where, loyal1, enemy );
						}
						else if ( LOSandHER( loyal1 ) )
						{
							time[ corpLoop ] = battle->gameTime + tempTime;
							sendAIOrder( ORDER_Hold, loyal1->battleInfo->where, loyal1, enemy );
						}
						else
						{
							if ( !loyal1->battleInfo->OrderPresent() )
							{
								sendAIOrder( ORDER_CautiousAdvance, loyal1->battleInfo->battleOrder.destination, loyal1, enemy );
								if ( distance( loyal1->battleInfo->battleOrder.destination.x - loyal1->battleInfo->where.x, loyal1->battleInfo->battleOrder.destination.y - loyal1->battleInfo->where.y ) > 1000 )
								{
									loyal1->battleInfo->AIFlag = True;
								}
							}
						}
					}
				}
			}
			else
			{
				enemy = FindClosestEnemy( 0, flank );
				if(enemy)
				{

					while ( corpLoop-- )
					{
						loyal1 = Armylist[ corpLoop ];

						// Location dest = findClosestLow( loyal1, enemy );

						if ( OnBattlefield )
						{
							if ( corpLoop ) 
							{
								loca = enemy->battleInfo->where;
							
								// loca = KeepFormation( corpLoop, loca, 1 );

								loca = getDestOffset( loyal1, loca );

								loca.x -= mcos( Yard( 1000 ), 0x4000 - ang );
								loca.y -= msin( Yard( 1000 ), 0x4000 - ang );

								sendAIOrder( ORDER_CautiousAdvance, loca, loyal1, enemy );
							}
							else
							{

								loca = enemy->battleInfo->where;

								loca = getDestOffset( loyal1, loca );

								// loca = KeepFormation( corpLoop, loca, 1 );

								sendAIOrder( ORDER_CautiousAdvance, loca, loyal1, enemy );
								loyal1->battleInfo->AI_action = 2;
							}
				  		}
						else 
						{
							if ( ( ang < 0x1200 ) || ( ang > 0xe800 ) )
							{
								Location dest = findClosestLow( loyal1, enemy );

								sendAIOrder( ORDER_CautiousAdvance, dest, loyal1, enemy );
							}
							else
							{
								sendAIOrder( ORDER_CautiousAdvance, senior->givenOrders.destination, loyal1, enemy );
							}
						}

						loyal1->battleInfo->AI_action = 2;
					}
				}
			}
			break;

		case Pincer:
#ifdef DEBUG
	 		aiBLog.printf(" - Pincer");
#endif
			tempTime = timeToTicks( 0, 60 + game->gameRand(20), 0 );
		  		
			enemy = FindClosestEnemy( loyal1 );
			if(enemy)
			{

				if ( initial || LOSandHER() )
		   	{
					while ( corpLoop-- )
					{
						loyal1 = Armylist[ corpLoop ];

						if ( loyal1->battleInfo->OrderPresent() ) 
						{
							if ( initial )	loyal1->battleInfo->AIFlag = True;

							#ifdef DEBUG_CHRIS
								PutInLog( "\r\nAlready has an order!!\r\n" );
							#endif

							continue;
						}

						if ( corpLoop < 2 && !LOSandHER( loyal1 ) )
						{
		 					enemy = FindClosestEnemy( 0, corpLoop + 2, loyal1 );

							Location dest = enemy->battleInfo->where;
						
							// findClosestLow( loyal1, enemy, corpLoop + 1 );

							dest = getDestOffset( loyal1, dest );

							sendAIOrder( ORDER_CautiousAdvance, dest, loyal1, enemy );

							time[ corpLoop ].set( 0, 0 );

							loyal1->battleInfo->AIFlag = False;

						}
						else
						{
		  					// enemy = FindClosestEnemy( loyal1 );

							time[ corpLoop ] = battle->gameTime + tempTime;

							sendAIOrder( ORDER_Stand, loyal1->battleInfo->where, loyal1, enemy );
							loyal1->battleInfo->AIFlag = False;

						}
					}
				}
				else
				{
					while ( corpLoop-- )
					{
						loyal1 = Armylist[ corpLoop ];

						if ( loyal1->battleInfo->OrderPresent() ) 
						{
							if ( initial )	loyal1->battleInfo->AIFlag = False;
								
							#ifdef DEBUG_CHRIS
								PutInLog( "\r\nAlready has an order!!\r\n" );
							#endif

							continue;
						}

						// if ( OnBattlefield )
						// {
							if ( ( !corpLoop || corpLoop == 1 ) && !LOSandHER( loyal1 ) )
							{
		  						enemy = FindClosestEnemy( loyal1, corpLoop + 2, loyal1 );

								if(enemy)
								{
									// Location dest = findClosestLow( loyal1, enemy, corpLoop + 1 );

									Location dest = getDestOffset( loyal1, enemy->battleInfo->where );

									sendAIOrder( ORDER_CautiousAdvance, dest, loyal1, enemy );
								}

								loyal1->battleInfo->AIFlag = False;
							}
							else 
							{
								sendAIOrder( ORDER_Stand, loyal1->battleInfo->where, loyal1, enemy );
							
								loyal1->battleInfo->AI_action = 2;
								loyal1->battleInfo->AIFlag = False;

								time[ corpLoop ] = battle->gameTime + tempTime;
							}
						// }
						// else
						// {
						// 	sendAIOrder( ORDER_Advance, senior->givenOrders.destination, loyal1, 0 );
						// }
					}
				}
			}
			break;

		case Feint_Left_Right:
#ifdef DEBUG
	 		aiBLog.printf(" - Feint_Left_Right");
#endif
		  flank = 2;
		  goto doFeint;
		case Feint_Right_Left:
#ifdef DEBUG
	 		aiBLog.printf(" - Feint_Right_Left");
#endif
		 	flank = 3;
		doFeint:

		  enemy = FindClosestEnemy( 0, flank ); // loyal1 );
		  if(enemy)
		  {

		   	if ( initial || LOSandHER() )
		   	{
					tempTime = timeToTicks( 0, 60 + game->gameRand(20), 0 );
				
					while ( corpLoop-- )
					{
						loyal1 = Armylist[ corpLoop ];

						loyal1->battleInfo->AIFlag = False;

						if ( loyal1->battleInfo->OrderPresent() ) 
						{
							if ( initial )	
							{
								loyal1->battleInfo->AIFlag = True;
								corpsTotal++;
							}
						
							#ifdef DEBUG_CHRIS
								if ( loyal1->general ) PutInLog( loyal1->general->name );
								PutInLog( " already has an order!!\r\n" );
							#endif

							continue;
						}

						if ( initial && ( loyal1->battleInfo->battleOrder.mode < ORDER_Stand ) &&  distance( loyal1->battleInfo->battleOrder.destination.x - loyal1->battleInfo->where.x, loyal1->battleInfo->battleOrder.destination.y - loyal1->battleInfo->where.y ) > 1000 )
						{
							corpsTotal++;
							loyal1->battleInfo->AIFlag = True;
			
							#ifdef DEBUG_CHRIS
								if ( loyal1->general ) PutInLog( loyal1->general->name );
								PutInLog( "\r\nAlready has an order!!(2)\r\n" );
							#endif

							continue;	
						}


						if ( initial && distance( loyal1->battleInfo->where.x - loyal1->battleInfo->battleOrder.destination.x, loyal1->battleInfo->where.y - loyal1->battleInfo->battleOrder.destination.y ) < 1800 )
						{
							if ( initial ) loyal1->battleInfo->AIFlag = False;
							sendAIOrder( ORDER_Hold, loyal1->battleInfo->where, loyal1, enemy );
						}
						else if ( LOSandHER( loyal1 ) )
						{
							time[ corpLoop ] = battle->gameTime + tempTime;
							sendAIOrder( ORDER_Hold, loyal1->battleInfo->where, loyal1, enemy );
						}
						else 
						{
						 	if ( !initial && !loyal1->battleInfo->OrderPresent() )
						 	{

								if ( corpLoop )
 								{
									Location dest = enemy->battleInfo->where;

									if ( deployTable[tact][corpLoop] > 2 && deployTable[tact][corpLoop] != 4 ) dest = KeepFormation( corpLoop, dest, 1 );
	  								else dest = getDestOffset( loyal1, dest );

									loca.x -= mcos( Yard( 1000 ), 0x4000 - ang );
									loca.y -= msin( Yard( 1000 ), 0x4000 - ang );

									loyal1->battleInfo->battleOrder.destination = loca;
								}

								sendAIOrder( ORDER_CautiousAdvance, loyal1->battleInfo->battleOrder.destination, loyal1, enemy );
					 	 	}
	
						 	if ( initial && ( loyal1->battleInfo->battleOrder.mode < ORDER_Stand ) && distance( loyal1->battleInfo->battleOrder.destination.x - loyal1->battleInfo->where.x, loyal1->battleInfo->battleOrder.destination.y - loyal1->battleInfo->where.y ) > 1000 )
								{
									corpsTotal++;
									loyal1->battleInfo->AIFlag = True;
			
									#ifdef DEBUG_CHRIS
										if ( loyal1->general ) PutInLog( loyal1->general->name );
										PutInLog( "\r\nAlready has an order!!2\r\n" );
									#endif
								}

					  	/*	
							else 
							{
								if ( initial )	loyal1->battleInfo->AIFlag = True;

								#ifdef DEBUG_CHRIS
									if ( loyal1->general ) PutInLog( loyal1->general->name );
									PutInLog( "\r\nAlready has an order!!(3)\r\n" );
								#endif
							}
						*/
						}
					}
				}
				else
				{
					while ( corpLoop-- )
					{
						loyal1 = Armylist[ corpLoop ];

						if ( loyal1->battleInfo->OrderPresent() ) 
						{
							if ( initial )	loyal1->battleInfo->AIFlag = False;
												
							#ifdef DEBUG_CHRIS
								PutInLog( "\r\nAlready has an order!!\r\n" );
							#endif

							continue;
						}

						Location dest = findClosestLow( loyal1, enemy );

						if ( OnBattlefield )
						{
							if ( corpLoop ) 
							{
								enemy = FindClosestEnemy( 0, flank );
		
								loca = enemy->battleInfo->where;
							
								if ( deployTable[tact][corpLoop] > 2 && deployTable[tact][corpLoop] != 4 ) loca = KeepFormation( corpLoop, loca, 1 );
	  							else loca = getDestOffset( loyal1, loca );

								sendAIOrder( ORDER_CautiousAdvance, loca, loyal1, enemy );
							}
							else
							{
		 
								enemy = FindClosestEnemy( 0, flank^1 );
							
								loca = enemy->battleInfo->where;

								// Allow for dist. commanding officer is away from 
								// his front children

								loca = getDestOffset( loyal1, loca );

								// Cautious advance to a 1000 yards from enemy

								loca.x -= mcos( Yard( 1000 ), 0x4000 - ang );
								loca.y -= msin( Yard( 1000 ), 0x4000 - ang );

								sendAIOrder( ORDER_CautiousAdvance, loca, loyal1, enemy );
								loyal1->battleInfo->AI_action = 2;
							}
				  		}
						else 
						{
							if ( ( ang < 0x1200 ) || ( ang > 0xe800 ) )
							{
								sendAIOrder( ORDER_CautiousAdvance, dest, loyal1, enemy );
							}
							else
							{
								sendAIOrder( ORDER_CautiousAdvance, senior->givenOrders.destination, loyal1, enemy );
							}
						}

						loyal1->battleInfo->AI_action = 2;
					}
				}
			}
			break;
					
		case Line_Defence:
#ifdef DEBUG
	 		aiBLog.printf(" - Line_Defence");
#endif
 		   enemy = FindClosestEnemy( loyal1 );
			if(enemy)
				{

		   	if ( LOSandHER() )
		   	{
					tempTime = timeToTicks( 0, 20 + game->gameRand(20), 0 );
				
					while ( corpLoop-- )
					{
						loyal1 = Armylist[ corpLoop ];

						time[ corpLoop ] = battle->gameTime + tempTime;

						sendAIOrder( ORDER_Stand, loyal1->battleInfo->where, loyal1, enemy );

						loyal1->battleInfo->AI_action = 1;
					}
				}
				else
				{
					while ( corpLoop-- )
					{
						loyal1 = Armylist[ corpLoop ];

						if ( OnBattlefield )
						{
							sendAIOrder( ORDER_Hold, enemy->battleInfo->where, loyal1, enemy );
							loyal1->battleInfo->AI_action = 1;
						}
						else
						{
							sendAIOrder( ORDER_Advance, senior->givenOrders.destination, loyal1, 0 );
							loyal1->battleInfo->AI_action = 2;
						}
					}
				}
			}
			break;
					
		case Defence_In_Depth:
#ifdef DEBUG
	 		aiBLog.printf(" - Defence_In_Depth");
#endif
		   enemy = FindClosestEnemy( loyal1 );
			if(enemy)
			{
		   
				if ( LOSandHER() )
		   	{
					tempTime = timeToTicks( 0, 20 + game->gameRand(20), 0 );
				
					while ( corpLoop-- )
					{
						loyal1 = Armylist[ corpLoop ];

						time[ corpLoop ] = battle->gameTime + tempTime;

						sendAIOrder( ORDER_Stand, loyal1->battleInfo->where, loyal1, enemy );

						loyal1->battleInfo->AI_action = 1;
					}
				}
				else
				{
					while ( corpLoop-- )
					{
						loyal1 = Armylist[ corpLoop ];

						if ( OnBattlefield )
						{
							sendAIOrder( ORDER_Hold, enemy->battleInfo->where, loyal1, enemy );
							loyal1->battleInfo->AI_action = 1;
						}
						else
						{
							sendAIOrder( ORDER_CautiousAdvance, senior->givenOrders.destination, loyal1, 0 );
							loyal1->battleInfo->AI_action = 2;
						}
					}
				}
			}
			break;

		case Feint_Defence:
#ifdef DEBUG
	 		aiBLog.printf(" - Feint_Defence");
#endif
		   enemy = FindClosestEnemy( loyal1 );
			if(enemy)
			{
		   
				if ( LOSandHER() )
		   	{
					tempTime = timeToTicks( 0, 20 + game->gameRand(20), 0 );
				
					while ( corpLoop-- )
					{
						loyal1 = Armylist[ corpLoop ];

						time[ corpLoop ] = battle->gameTime + tempTime;

						sendAIOrder( ORDER_Stand, loyal1->battleInfo->where, loyal1, enemy );

						loyal1->battleInfo->AI_action = 3;
					}
				}
				else if ( !initial )
				{
					while ( corpLoop-- )
					{
						loyal1 = Armylist[ corpLoop ];

						if ( OnBattlefield )
						{
							sendAIOrder( ORDER_Hold, enemy->battleInfo->where, loyal1, enemy );
							loyal1->battleInfo->AI_action = 1;
						}
						else
						{
							sendAIOrder( ORDER_Advance, senior->givenOrders.destination, loyal1, 0 );
							loyal1->battleInfo->AI_action = 2;
						}
					}
				}
			}
			break;

		case Holding_Action:
#ifdef DEBUG
	 		aiBLog.printf(" - Holding_Action");
#endif
		  enemy = FindClosestEnemy( loyal1 );
		  if(enemy)
		  {
		   
				if ( LOSandHER() )
		   	{
					tempTime = timeToTicks( 0, 20 + game->gameRand(20), 0 );
				
					while ( corpLoop-- )
					{
						loyal1 = Armylist[ corpLoop ];

						time[ corpLoop ] = battle->gameTime + tempTime;

						sendAIOrder( ORDER_Stand, loyal1->battleInfo->where, loyal1, enemy );

						loyal1->battleInfo->AI_action = 1;
					}
				}
				else if ( !initial )
				{
					while ( corpLoop-- )
					{
						loyal1 = Armylist[ corpLoop ];

						sendAIOrder( ORDER_Hold, enemy->battleInfo->where, loyal1, enemy );

						loyal1->battleInfo->AI_action = 1;
					}
				}
			}
			break;

		case Covering_Action:
#ifdef DEBUG
	 		aiBLog.printf(" - Covering_Action");
#endif
		   enemy = FindClosestEnemy( loyal1 );
			if(enemy)
			{
			
		   	if ( LOSandHER() )
		   	{
					tempTime = timeToTicks( 0, 20 + game->gameRand(20), 0 );
				
					while ( corpLoop-- )
					{
						loyal1 = Armylist[ corpLoop ];

						time[ corpLoop ] = battle->gameTime + tempTime;

						sendAIOrder( ORDER_Stand, loyal1->battleInfo->where, loyal1, enemy );

						loyal1->battleInfo->AI_action = 3;
					}
				}
				else
				{
					while ( corpLoop-- )
					{							   
						loyal1 = Armylist[ corpLoop ];

						if ( OnBattlefield )
						{
							sendAIOrder( ORDER_Hold, enemy->battleInfo->where, loyal1, enemy );
							loyal1->battleInfo->AI_action = 1;
						}
						else if ( !initial )
						{
							sendAIOrder( ORDER_Withdraw, senior->givenOrders.destination, loyal1, 0 );
							loyal1->battleInfo->AI_action = 2;
						}
					}
				}
			}
			break;

		case Withdraw:
#ifdef DEBUG
	 		aiBLog.printf(" - Withdraw");
#endif
		   enemy = FindClosestEnemy( loyal1 );
			if(enemy)
			{
			
				angl = direction( loyal1->battleInfo->where.x - enemy->battleInfo->where.x, loyal1->battleInfo->where.y - enemy->battleInfo->where.y );

				dest = loyal1->battleInfo->where;

				dest.x -= msin( Mile(5), angl );
				dest.y -= mcos( Mile(5), angl );

				lgx = DistToBattle( dest.x ) + battle->grid->getTotalWidth() / 2;
				lgy = DistToBattle( dest.y ) + battle->grid->getTotalHeight() / 2;
			
				while ( ( lgx > 0 && lgx < gr->gridCount ) && ( lgy > 0 && lgy < gr->gridCount ) )
				{
					dest.x -= msin( Mile(2), angl );
					dest.y -= mcos( Mile(2), angl );

					lgx = DistToBattle( dest.x ) + battle->grid->getTotalWidth() / 2;
					lgy = DistToBattle( dest.y ) + battle->grid->getTotalHeight() / 2;
				}

				wangl = direction( loyal1->battleInfo->where.x - senior->givenOrders.destination.x, loyal1->battleInfo->where.y - senior->givenOrders.destination.y );

				while ( corpLoop-- )
				{
					loyal1 = Armylist[ corpLoop ];

					if ( ( (angl - wangl) < 0xc000 ) && ( (angl - wangl) > 0x4000 ) && !OnBattlefield && !IsEnemyInWay( loyal1, wangl ) )
					{
						sendAIOrder( ORDER_Withdraw, senior->givenOrders.destination, loyal1, enemy );
					}
					else sendAIOrder( ORDER_Withdraw, dest, loyal1, enemy );

					if ( loyal1->hasBattleDetached() || loyal1->hasCampaignReallyDetached() ) orderChildren( loyal1, ORDER_Withdraw, dest ); 

					loyal1->battleInfo->AI_action = 3;
				}

				/*
				if ( abs( ang - wangl ) > 0x4000 && !OnBattlefield)
				{
					sendAIOrder( ORDER_Withdraw, senior->givenOders.destination, senior, enemy );
				}
				else sendAIOrder( ORDER_Withdraw, dest, senior, enemy );
				*/
			}
			break;

		case Retreat:
#ifdef DEBUG
	 		aiBLog.printf(" - Retreat");
#endif
			while ( corpLoop-- )
			{
				loyal1 = Armylist[ corpLoop ];

		   	enemy = FindClosestEnemy( loyal1 );
				if(enemy)
				{

		   		angl = direction( loyal1->battleInfo->where.x - enemy->battleInfo->where.x, loyal1->battleInfo->where.y - enemy->battleInfo->where.y );

					dest = loyal1->battleInfo->where;

					dest.x -= msin( Mile(5), angl );
					dest.y -= mcos( Mile(5), angl );

					lgx = DistToBattle( dest.x ) + battle->grid->getTotalWidth() / 2;
					lgy = DistToBattle( dest.y ) + battle->grid->getTotalHeight() / 2;

					while ( ( lgx > 0 && lgx < gr->gridCount ) && ( lgy > 0 && lgy < gr->gridCount ) )
					{
						dest.x -= msin( Mile(2), angl );
						dest.y -= mcos( Mile(2), angl );

						lgx = DistToBattle( dest.x ) + battle->grid->getTotalWidth() / 2;
						lgy = DistToBattle( dest.y ) + battle->grid->getTotalHeight() / 2;
					}
			
					sendAIOrder( ORDER_Retreat, dest, loyal1, enemy );

					loyal1->battleInfo->AI_action = 3;

					if ( loyal1->hasBattleDetached() || loyal1->hasCampaignReallyDetached() ) orderChildren( loyal1, ORDER_Retreat, dest ); 
				}
			}

			/*
			sendAIOrder( ORDER_Withdraw, dest, senior, enemy );
			*/

			break;
		default:
#ifdef DEBUG
			throw GeneralError("Error in sendInitialOrders");
#endif
			break;
	}


}


Unit* AI_Battle::getSideUnit( Boolean Left )
{
#ifdef DEBUG
	aiBLog.printf("getSideUnit(%s)",
		getBoolStr(Left));
#endif

	President* p = ob->getPresident( otherSide( side ) );

	Unit* enemyArmy = p->child;
	Unit* enemyCorps;
	Unit* enemyDivision;
	Unit* enemyBrigade;

	Wangle alpha, bestAlpha = 0;

	Unit* best;

	Location eloc;

 	if ( Left )		
	{
		while ( enemyArmy )
		{
			if ( enemyArmy->battleInfo || enemyArmy->hasCampaignReallyDetached() )
			{
				enemyCorps = enemyArmy->child;
		
				while( enemyCorps )
				{
					if ( enemyCorps->battleInfo )
					{
			 			eloc = enemyCorps->battleInfo->where;
		
						alpha = direction( eloc.x - deployCentre.x, eloc.y- deployCentre.y ) - senior->battleInfo->formationFacing;

						if ( alpha < 0x4000 && alpha > bestAlpha )
						{
							bestAlpha = alpha;
							best = enemyCorps;
						}
					}
					else if ( enemyCorps->battleInfo || enemyCorps->hasCampaignReallyDetached() )
					{
						enemyDivision = enemyCorps->child;

						while( enemyDivision )
						{
							if ( enemyDivision->battleInfo )
							{
				 				eloc = enemyDivision->battleInfo->where;
																 
								alpha = direction( eloc.x - deployCentre.x, eloc.y - deployCentre.y ) - senior->battleInfo->formationFacing;

								if ( alpha < 0x4000 && alpha > bestAlpha )
								{
									bestAlpha = alpha;
									best = enemyDivision;
								}
							}
							else
							{
								enemyBrigade = enemyDivision->child;

								while( enemyBrigade )
								{
									if ( enemyBrigade->battleInfo )
									{
					 					eloc = enemyBrigade->battleInfo->where;
			
										alpha = direction( eloc.x - deployCentre.x, eloc.y - deployCentre.y ) - senior->battleInfo->formationFacing;

										if ( alpha < 0x4000 && alpha > bestAlpha )
										{
											bestAlpha = alpha;
											best = enemyDivision;
										}		
									}
									enemyBrigade = enemyBrigade->sister;
								}
							}
							enemyDivision = enemyDivision->sister;
						}
					}
					enemyCorps = enemyCorps->sister;
				}
			}
			enemyArmy = enemyArmy->sister;
		}
	}
	else
	{
		while ( enemyArmy )
		{
			if ( enemyArmy->battleInfo || enemyArmy->hasCampaignReallyDetached() )
			{
				enemyCorps = enemyArmy->child;
		
				while( enemyCorps )
				{
					if ( enemyCorps->battleInfo )
					{
			 			eloc = enemyCorps->battleInfo->where;
		
						alpha = direction( eloc.x - deployCentre.x, eloc.y - deployCentre.y ) - senior->battleInfo->formationFacing;

						if ( alpha > 0xc000 && alpha < bestAlpha )
						{
							bestAlpha = alpha;
							best = enemyCorps;
						}
					}
					else if ( enemyCorps->battleInfo || enemyCorps->hasCampaignReallyDetached() ) 
					{
						enemyDivision = enemyCorps->child;

						while( enemyDivision )
						{
							if ( enemyDivision->battleInfo )
							{
				 				eloc = enemyDivision->battleInfo->where;
			
								alpha = direction( eloc.x - deployCentre.x, eloc.y - deployCentre.y ) - senior->battleInfo->formationFacing;

								if ( alpha > 0xc000 && alpha < bestAlpha )
								{
									bestAlpha = alpha;
									best = enemyDivision;
								}
							}
							else
							{
								enemyBrigade = enemyDivision->child;

								while( enemyBrigade )
								{
									if ( enemyBrigade->battleInfo )
									{
					 					eloc = enemyBrigade->battleInfo->where;
			
										alpha = direction( eloc.x - deployCentre.x, eloc.y - deployCentre.y ) - senior->battleInfo->formationFacing;

										if ( alpha > 0xc000 && alpha < bestAlpha )
										{
											bestAlpha = alpha;
											best = enemyDivision;
										}		
									}
									enemyBrigade = enemyBrigade->sister;
								}
							}
							enemyDivision = enemyDivision->sister;
						}
					}
					enemyCorps = enemyCorps->sister;
				}
			}
			enemyArmy = enemyArmy->sister;
		}
	}

	return best;
}

/*
 *  If the int flag below is set to one the routine searches to the right
 *  of the loyal, and if it contains 2 it searches to the left. If it is
 *  clear it searches to both left and right.
 */

Location AI_Battle::findClosestLow( Unit* loyal, Location el, int flag )
{
#ifdef DEBUG
	aiBLog.printf("findClosestLow(%s, %ld,%ld, %d)",
		loyal ? loyal->getName(True) : "Null",
		el.x, el.y, flag);
#endif
	signed int loop;

	Grid* gr = battle->grid->getGrid( Zoom_1 );

	Location ll = loyal->battleInfo->where;

	int lgx, lgy;

	lgx = DistToBattle( ll.x ) + battle->grid->getTotalWidth() / 2;
	lgy = DistToBattle( ll.y ) + battle->grid->getTotalHeight() / 2;

	lgx /= gr->gridSize;
	lgy /= gr->gridSize;

	if ( ( lgx > gr->gridCount ) || ( lgx < 0 ) || ( lgy > gr->gridCount ) || ( lgy < 0 ) ) return el;

	Wangle alpha = direction( el.x - ll.x, el.y - ll.y );

	if ( flag == 1 ) alpha -= 0x1150;			 // search area to right;
	else if ( flag == 2 ) alpha += 0x1150;		 // search area to left;

	Distance d = distance( el.x - ll.x, el.y - ll.y );

	int radius = ( ( DistToBattle( d ) + battle->grid->getTotalWidth() / 2 ) / gr->gridSize ) / 2;

	int incradius = 1;

	if ( radius < 0 ) incradius = -1;
	else if ( abs( radius ) <= 2 )
	{
		return el;
	}

	Wangle aa = Degree( 13 );

	int sin26 = sin( aa );

	int cos26 = cos( aa );

	// const int ta = cos( aa ) / sin( aa );

	// int gh = ta * radius;

	// int gamma = 0x2000 - alpha ;

	int grx, gry, tgrx, tgry;

	int cosa = cos ( alpha );

	int sina = sin ( alpha );

	// int tana = 1;
	
	// if ( !cosa ) tana = sina / cosa;

	// if ( !tana ) tana = 1;

	Height8* hi;

	Height8* hibest = 0;

	int bestx, besty;

	Height8* lowbest = 0;

	int lowbestx, lowbesty;

	if ( ( alpha > 0x2000 && alpha < 0x6000 ) || ( ( alpha > 0xA000 ) && ( alpha < 0xe000 ) ) )
	{
		for ( int crow = 2; crow < radius; crow += incradius )
		{
			int sing = msin( crow, alpha );

			int dy = ( ( crow * sin26 / cos26 ) * sina ) / 0x4000;

			if ( !dy ) dy = 1;

			// grx = ( cosa * crow ) / 0x4000 + lgx;
			// gry = ( sina * crow ) / 0x4000 + lgy;

			if ( (sing + dy) < (sing - dy) ) dy = -dy;

			for ( int xloop =  sing - dy; xloop < sing + dy; xloop++ ) 
			{
				tgry = lgy + ( crow * sina - xloop * cosa ) / 0x4000;		// crow * sina / 0x4000 + xloop / tana + lgy;

				tgrx = lgx + ( crow * cosa - xloop * sina ) / 0x4000;

				if ( ( tgrx < gr->gridCount ) && ( tgrx > 0 ) && ( tgry < gr->gridCount ) && ( tgry > 0 ) )
				{
					hi = gr->getHSquare( tgrx, tgry );

					if ( !hibest || hi->height >= hibest->height )
					{
						if ( hi->height == hibest->height && !battle->data3D.terrain.isWooded(*gr->getTSquare( tgrx, tgry )) ) continue;

						hibest = hi;

						bestx = tgrx;
						besty = tgry;
					}
					else if ( !lowbest || hi->height <= lowbest->height )
					{
						if ( hi->height == lowbest->height && !battle->data3D.terrain.isWooded(*gr->getTSquare( tgrx, tgry ) ) ) continue;

						lowbest = hi;

						lowbestx = tgrx;
						lowbesty = tgry;
					}
				}
			}
		}
	}
	else
	{
		for ( int crow = 2; crow < radius; crow += incradius )
		{
			int sing = mcos( crow, alpha );

			int dy = ( ( crow * sin26 / cos26 ) * cosa ) / 0x4000;

			if ( !dy ) dy = 1;

			// grx = ( cosa * crow ) / 0x4000 + lgx;
			// gry = ( sina * crow ) / 0x4000 + lgy;

			if ( (sing + dy) < (sing - dy) ) dy = -dy;

			for ( int yloop = sing - dy; yloop < sing + dy; yloop++ ) 
			{
				tgry = lgy + ( crow * sina - yloop * cosa ) / 0x4000;		// crow * sina / 0x4000 + xloop / tana + lgy;

				tgrx = lgx + ( crow * cosa - yloop * sina ) / 0x4000;

				if ( ( tgrx < gr->gridCount ) && ( tgrx > 0 ) && ( tgry < gr->gridCount ) && ( tgry > 0 ) )
				{
					hi = gr->getHSquare( tgrx, tgry );

					if ( !hibest || hi->height >= hibest->height )
					{
						if ( hi->height == hibest->height && !battle->data3D.terrain.isWooded(*gr->getTSquare( tgrx, tgry )) ) continue;

						hibest = hi;

						bestx = tgrx;
						besty = tgry;
					}
					else if ( !lowbest || hi->height <= lowbest->height )
					{
						if ( hi->height == lowbest->height && !battle->data3D.terrain.isWooded(*gr->getTSquare( tgrx, tgry )) ) continue;

						lowbest = hi;

						lowbestx = tgrx;
						lowbesty = tgry;
					}
				}
			}
		}

	}

#ifdef DEBUG_CHRIS
	if ( ( !hibest ) && ( !lowbest ) )
	{
		PutInLog( "\r\nERROR: No terrain height found(2)!!\r\n" );
	}
#endif

	Location loca( BattleToDist( lowbestx * gr->gridSize - battle->grid->getTotalWidth() / 2 ), BattleToDist( lowbesty * gr->gridSize - battle->grid->getTotalHeight() / 2 ) );

	if ( abs( d ) < Mile( 1 ) || !lowbest ) 
	{
		loca.x = BattleToDist( bestx * gr->gridSize - battle->grid->getTotalWidth()  / 2 );
		loca.y = BattleToDist( besty * gr->gridSize - battle->grid->getTotalHeight() / 2 );
	}
	return loca;
}

void AI_Battle::clearEnemyPointer( Unit* enemy )
{
	Unit* loyal1;
	Unit* loyal2;
	Unit* loyal3;
	// Unit* loyal4;

	int corpLoop = MAXCorpsAL;

	while ( corpLoop-- )
	{
		loyal1 = Armylist[ corpLoop ];

		if ( loyal1->battleInfo->enemy == enemy ) loyal1->battleInfo->enemy = 0; 
 		
		loyal2 = loyal1->child;

		while ( loyal2 )
		{
			if ( loyal2->battleInfo->enemy == enemy ) loyal2->battleInfo->enemy = 0;
			
			loyal3 = loyal2->child;

			while( loyal3 )
			{
				if ( loyal3->battleInfo->enemy == enemy ) loyal3->battleInfo->enemy = 0;

				loyal3 = loyal3->sister;
			}
		  	loyal2 = loyal2->sister;
		}
	}

	return;
}


Unit* AI_Battle::FindClosestEnemy( Unit* loyal, int flank, Unit* loyal2 )
{
#ifdef DEBUG
	aiBLog.printf("FindClosestEnemy(%s, %d, %s)",
		loyal ? loyal->getName(True) : "Null",
		flank,
		loyal2 ? loyal2->getName(True) : "Null");
#endif

	President *p = ob->getPresident( otherSide( side ) );

	Unit *enemyArmy = p->child;
	Unit *enemyCorps;
	Unit *enemyDivision;
	Unit *enemyBrigade;

	Unit* closest = 0;
	Unit* closest2 = 0;

	Distance best = 0;
	Distance d;
	Distance opp;

	int lx, ly;

	Grid* gr = battle->grid->getGrid( Zoom_1 );

	int lgx, lgy;

	if ( loyal ) 
	{
		lx = loyal->battleInfo->where.x;
		ly = loyal->battleInfo->where.y;
	}
	else
	{
		lx = deployCentre.x;
		ly = deployCentre.y;
	}

	if ( !flank )
	{
		while ( enemyArmy )
		{
			if ( enemyArmy->battleInfo || enemyArmy->hasCampaignReallyDetached() )
			{
				enemyCorps = enemyArmy->child;

				while( enemyCorps )
				{
					if ( enemyCorps->battleInfo || enemyCorps->hasCampaignReallyDetached() )
					{
						enemyDivision = enemyCorps->child;

						while( enemyDivision )
						{
							enemyBrigade = enemyDivision->child;
				
							while ( enemyBrigade )
							{
								if ( enemyBrigade->battleInfo )
								{
									d = distance( enemyBrigade->battleInfo->where.x - lx, enemyBrigade->battleInfo->where.y - ly );

									if ( !best || abs( d ) < best )
									{
										best = abs( d );
										closest = enemyBrigade;
									}
  								}
								enemyBrigade = enemyBrigade->sister;
							}
							enemyDivision = enemyDivision->sister;
						}
					}
					enemyCorps = enemyCorps->sister;
				}
			}
	 	enemyArmy = enemyArmy->sister;
		}
	}
	else
	{
		Wangle bestang = 0;
		Wangle bestang2 = 0;

		Wangle angl;

		Location tempbl( 0, 0 );

		tempbl = averageEnemyLoc( otherSide( side ) );

		Wangle centreAng = direction( tempbl.x - lx, tempbl.y - ly );

		int offset;

		if ( centreAng < 0x4000 ) offset = 0x4000;
		else if ( centreAng > 0xc000 ) offset = -0x4000;

		while ( enemyArmy )
		{
 			if ( enemyArmy->battleInfo || enemyArmy->hasCampaignReallyDetached() )
			{
				enemyCorps = enemyArmy->child;

				while( enemyCorps )
				{
					if ( enemyCorps->battleInfo || enemyCorps->hasCampaignReallyDetached() )
					{
						enemyDivision = enemyCorps->child;

						while( enemyDivision )
						{
							enemyBrigade = enemyDivision->child;
				
							while ( enemyBrigade )
							{
								if ( enemyBrigade->battleInfo )
								{
									lgx = DistToBattle( enemyBrigade->battleInfo->where.x ) + battle->grid->getTotalWidth()  / 2;
									lgy = DistToBattle( enemyBrigade->battleInfo->where.y ) + battle->grid->getTotalHeight() / 2;

									lgx /= gr->gridSize;
									lgy /= gr->gridSize;

									if ( ( lgx < gr->gridCount ) && ( lgx > 0 ) && ( lgy < gr->gridCount ) && ( lgy > 0 ) )
									{
										if ( flank != 1 )
						  				{
						  					Wangle wang = offset + direction( enemyBrigade->battleInfo->where.x - lx, enemyBrigade->battleInfo->where.y - ly );

						  					// Is enemy to the left of centre location

						  					if ( !bestang || wang >= bestang )
 						  					{
						  						bestang = wang;
						  						closest = enemyBrigade;
						  					}
						  		
						  					// Is enemy to the right of the enemy centre location

						  					if ( !bestang2 || wang <= bestang2 )
						  					{
							  					bestang2 = wang;
							  					closest2 = enemyBrigade;
						  					}
						  				}
						  				else
						  				{
						  					d = distance( enemyBrigade->battleInfo->where.x - lx, enemyBrigade->battleInfo->where.y - ly );

						  					opp = distance( tempbl.x - enemyBrigade->battleInfo->where.x, tempbl.y - enemyBrigade->battleInfo->where.y);

						  					angl = direction( d, opp );
						  		
						  					bestang = angl;
						  					closest = enemyBrigade;
						  				}
									}
  								}
								enemyBrigade = enemyBrigade->sister;
							}
							enemyDivision = enemyDivision->sister;
						}
					}
					enemyCorps = enemyCorps->sister;
				}
			}
	 	enemyArmy = enemyArmy->sister;
		}
	}

#ifdef DEBUG
	aiBLog.printf("Closest=%s", closest ? closest->getName(True) : "Null");
	aiBLog.printf("Closest2=%s", closest2 ? closest2->getName(True) : "Null");
#endif

#ifdef DEBUG_CHRIS
	if ( !closest )
	{
	 	PutInLog( "\r\nNo closest enemy!!\r\n");
	}
#endif

	if ( (flank > 1) && closest2 ) 
	{
		if ( loyal2 )
		{
			d = distance( closest->battleInfo->where.x - loyal2->battleInfo->where.x, closest->battleInfo->where.y - loyal2->battleInfo->where.y );

			opp = distance( closest2->battleInfo->where.x - loyal2->battleInfo->where.x, closest2->battleInfo->where.y - loyal2->battleInfo->where.y );
		}
		else
		{
			d = distance( closest->battleInfo->where.x - lx, closest->battleInfo->where.y - ly );

			opp = distance( closest2->battleInfo->where.x - lx, closest2->battleInfo->where.y - ly );
		}

#ifdef DEBUG
		aiBLog.printf("d = %ld, opp = %ld", (long) d, (long) opp);
#endif

		if ( d > opp ) 
			return closest2;
	}

	return closest;
}


Boolean AI_Battle::LOSandHER( Unit* loyal, int CheckEnemy )
{
#ifdef DEBUG
	aiBLog.printf("LOSandHER(%s, %d)",
		loyal ? loyal->getName(True) : "Null",
		CheckEnemy);
#endif

	Unit* loyal1;

	if ( CheckEnemy )
	{
		// Check each enemy and see if it is within firing range
		// also check inCombat;

		Distance d;

		if ( !loyal )
		{
			int corpLoop = MAXCorpsAL;

			while ( corpLoop-- )
			{
				loyal1 = Armylist[ corpLoop ];

				if ( loyal1->battleInfo->inCombat )
				{
					return True;
				}
				else
				{
					Unit* enemy = FindClosestEnemy( loyal );

					d = distance( enemy->battleInfo->where.x - loyal1->battleInfo->where.x, enemy->battleInfo->where.y - loyal1->battleInfo->where.y );

					if ( abs( unitToYard( d ) ) < 1000 )
 					{
					 	return True;
					}
				}
			}
		}
		else
		{
				Unit* enemy = FindClosestEnemy( loyal );

				d = distance( enemy->battleInfo->where.x - loyal->battleInfo->where.x, enemy->battleInfo->where.y - loyal->battleInfo->where.y );

				if ( abs( unitToYard( d ) ) < 1000 )
				{
				 	return True;
				}
		}
	}
	else
	{
		if ( !loyal )
		{
			int corpLoop = MAXCorpsAL;

			while ( corpLoop-- )
			{
				loyal1 = Armylist[ corpLoop ];

				if ( loyal1->battleInfo->inCombat )
				{
					return True;
				}
			}
		}
		else
		{
			if ( loyal->battleInfo->inCombat )
			{
				return True;
			}
		}
	}

#ifdef DEBUG
	aiBLog.printf("LOSandHer --> False");
#endif

	return False;
}

void AI_Battle::getKeyTerrain( Unit* unit, int deep, Location* sofar )
{
	if ( unit->getSide() != side ) return;

	Grid* gr = battle->grid->getGrid( Zoom_1 );

	int lgx, lgy;

	lgx = DistToBattle( sofar->x ) + battle->grid->getTotalWidth() / 2;
	lgy = DistToBattle( sofar->y ) + battle->grid->getTotalHeight() / 2;

	lgx /= gr->gridSize;
	lgy /= gr->gridSize;

	int count = deep;

	Height8* hi;

	Height8* hibest = gr->getHSquare( lgx, lgy );

	int bestx = lgx;
	int besty = lgy;

	while( count )
 	{
		for ( int x = lgx - count; x < lgx + count; x++	)
		{
			if ( x > 0 && x < gr->gridSize && (lgy + count) > 0 && (lgy + count) < gr->gridSize )
			{
				hi = gr->getHSquare( x, lgy + count );

				if ( !hibest || hi->height >= hibest->height )
				{
					hibest = hi;

					bestx = x;
					besty = lgy + count;
				}
			}

			if ( x > 0 && x < gr->gridSize && (lgy - count) > 0 && (lgy - count) < gr->gridSize )
			{
				hi = gr->getHSquare( x, lgy - count );

				if ( hi->height >= hibest->height )
				{
					hibest = hi;

					bestx = x;
					besty = lgy - count;
				}
			}
		}

		for ( int y = lgy - ( count - 1 ); y < lgy + ( count - 1 ); y++ )
		{
			if ( y > 0 && y < gr->gridSize && (lgx + count) > 0 && (lgx + count) < gr->gridSize )
 			{
				hi = gr->getHSquare( lgx + count, y );

				if ( !hibest || hi->height >= hibest->height )
				{
					hibest = hi;

					bestx = lgx + count;
					besty = y;
				}
			}

			if ( y > 0 && y < gr->gridSize && (lgx - count) > 0 && (lgx - count) < gr->gridSize )
			{
				hi = gr->getHSquare( lgx - count, y );

				if ( hi->height >= hibest->height )
				{
					hibest = hi;

					bestx = lgx + count;
					besty = y;
				}
			}
		}
	 	count--;
	}

	sofar->x = BattleToDist( bestx * gr->gridSize - battle->grid->getTotalWidth()  / 2 );
	sofar->y = BattleToDist( besty * gr->gridSize - battle->grid->getTotalHeight() / 2 );

}

void AI_Battle::withDraw(Unit* loyal, int enemyStr)
{
#ifdef DEBUG
	aiBLog.printf("withDraw(%s, %d)",
		loyal ? loyal->getName(True) : "Null",
		enemyStr);
#endif

	/*
	 * If already withdrawing and destination is a long way away, then continue
	 */

	if ( loyal->battleInfo->battleOrder.mode >= ORDER_Withdraw )	// Used to be ==
	{
		Distance d = distanceLocation(loyal->battleInfo->battleOrder.destination, loyal->battleInfo->where );
 
		if ( d > Yard( 1300 ) ) return;
	}

	/*
	 * See if anyone can come to help
	 */

	Unit* reserve;

	if ( loyal->rank == senior->rank )
		reserve = findClosestReserve( loyal, loyal->rank );
	else
		reserve = findClosestReserve( loyal, promoteRank( loyal->rank ) );

	/*
	 * NOTE: The above line was changed by SWG to fix a bug
	 * It previously used DECREMENT(enemy->rank), which actually
	 * modified the enemy's rank!
	 */

	UnitInfo ui(False, False);

	loyal->getInfo(ui);

	int loyalStr = ui.strength;		// This used to be += on an unitialised variable

	retValue rv = FAIL_BADLY;

	if(reserve)		// Added by SWG
	{
		reserve->getInfo(ui);
		loyalStr += ui.strength;

		rv = Efficiency_Test(loyal->general, loyalStr, enemyStr, game->gameRand.getB());

		if(rv == PASS)
		{
			sendAIOrder(ORDER_CautiousAdvance, loyal->battleInfo->battleOrder.destination, reserve);
			sendAIOrder(ORDER_Stand, loyal->battleInfo->where, loyal );
			return;
		}
	}

	/*
	 * There were no eserves coming to help, so really withdraw
	 */


	int agg = Aggressvalue(loyal);
	
	OrderMode action;

	if(agg >= 5)
		action = ORDER_Withdraw;
	else if( agg >= 3)
		action = ORDER_Retreat;
	else
		action = ORDER_HastyRetreat;

	Location dest = loyal->battleInfo->where;

	/*
	 * 	Find best angle to withdraw at
	 */

	Wangle wang = withdrawAngle(loyal);

	dest.x -= msin( Yard(1300), wang);
	dest.y -= mcos( Yard(1300), wang);

	sendAIOrder(action, dest, loyal);
}

Location AI_Battle::findClosestHigh( Location ll, Location el, Boolean direc4 )
{
	signed int loop;

	int base = 3;

	Location loca( 0, 0 );

	Grid* gr = battle->grid->getGrid( Zoom_1 );

	int lgx, lgy;

	lgx = DistToBattle( ll.x ) + battle->grid->getTotalWidth()  / 2;
	lgy = DistToBattle( ll.y ) + battle->grid->getTotalHeight() / 2;

	lgx /= gr->gridSize;
	lgy /= gr->gridSize;

	int egx, egy;

	egx = DistToBattle( el.x ) + battle->grid->getTotalWidth()  / 2;
	egy = DistToBattle( el.y ) + battle->grid->getTotalHeight() / 2;

	egx /= gr->gridSize;
	egy /= gr->gridSize;

	Wangle alpha = direction( el.x - ll.x, el.y - ll.y );

	Distance d = distance( abs( el.x - ll.x ), abs( el.y - ll.y ) );

	// loop = 2;

	if ( Lflag ) loop = 3;	// 3
	else loop = 3;	 // 4

	if ( ( d / loop ) < Mile( 1 ) ) 
	{

	#ifdef DEBUG_CHRIS
		PutInLog( "Error: d=" );
		PutInLog( DistToBattle( d ) / loop );
		PutInLog( " mile(1)= " );
		PutInLog( Mile(1 ) );
		PutInLog( " Enemy Loc=" );
		PutInLog( egx );
		PutInLog( "," );
		PutInLog( egy );
		PutInLog( " Loyal loc= " );
		PutInLog( lgx );
		PutInLog( "," );
		PutInLog( lgy );
	#endif

		alpha += 0x8000;

		//	if ( d == 0 ) 

		return ll;
	}

	// d -= Mile( 2 );

	int radius = distance( abs( egx - lgx ), abs( egy - lgy ) );

	radius /= loop;

		// ( ( DistToBattle( d ) + battle->grid->getTotalWidth() / 2 ) / gr->gridSize ) / loop;

	radius -= 2;

	int incradius = 1;

	if ( radius > 8 ) 
	{
		base = 5;
		radius -= 5;
	}

	if ( radius < 0 ) 
	{
		incradius = -1;
		radius -= 1;
	}
	else 
	{
		if ( !Lflag ) radius += 3;
		else radius += 5;
	}

	if ( abs( radius ) <= 5 ) 
	{	
		return ll;
	}

	Wangle aa = Degree( 30 );

	int sin26 = sin( aa );

	int cos26 = cos( aa );

	// const int ta = cos( aa ) / sin( aa );

	// int gh = ta * radius;

	// int gamma = 0x2000 - alpha ;

	int grx, gry, tgrx, tgry;

	int cosa = cos ( alpha );

	int sina = sin ( alpha );

	// int tana = 1;
	
	// if ( cosa ) tana = sina / cosa;

	// if ( !tana ) tana = 1;

	Height8* hi;

	Height8* hibest = 0;

	int bestx = 0, besty = 0;

	// Height8* lowbest = 0;

	for ( char lcount = 0; lcount < 4; lcount++ )
	{
		if ( direc4 ) alpha += 0x4000;
		else lcount = 4;

		if ( ( alpha > 0x2000 && alpha < 0x6000 ) || ( ( alpha > 0xA000 ) && ( alpha < 0xe000 ) ) )
		{
			for ( int crow = base; crow < radius; crow += incradius )
			{
				int sing = msin( crow, alpha );

				int dy = ( ( crow * cos26 / cos26 ) * sina ) / 0x4000;

				if ( !dy ) dy = 1;

				// grx = ( cosa * crow ) / 0x4000 + lgx;
				// gry = ( sina * crow ) / 0x4000 + lgy;

				if ( (sing + dy) < (sing - dy) ) 
				{
					dy = -dy;
				}

				for ( int xloop =  sing - dy; xloop < sing + dy; xloop++ ) 
				{
					tgry = lgy + ( crow * sina - xloop * cosa ) / 0x4000;		// crow * sina / 0x4000 + xloop / tana + lgy;

					tgrx = lgx + ( crow * cosa - xloop * sina ) / 0x4000;

					if ( ( tgrx < gr->gridCount - 8 ) && ( tgrx > 8 ) && ( tgry < gr->gridCount - 8 ) && ( tgry > 8 ) )
					{
						hi = gr->getHSquare( tgrx, tgry );

						if ( !hibest || hi->height >= hibest->height && NotToClose( tgrx * gr->gridSize - battle->grid->getTotalWidth()  / 2, tgry  * gr->gridSize - battle->grid->getTotalHeight() / 2)	)
						{
							if ( hi->height == hibest->height && !battle->data3D.terrain.isWooded(*gr->getTSquare( tgrx, tgry )) ) continue;

							hibest = hi;

							bestx = tgrx;
							besty = tgry;
						}
					}
				}
			}
		}
		else
		{
			for ( int crow = base; crow < radius; crow += incradius )
			{
				int sing = mcos( crow, alpha );

				int dy = ( ( crow * sin26 / cos26 ) * cosa ) / 0x4000;

				if ( !dy ) dy = 1;

				// grx = ( cosa * crow ) / 0x4000 + lgx;
				// gry = ( sina * crow ) / 0x4000 + lgy;

				if ( (sing + dy) < (sing - dy) ) 
				{
					dy = -dy;
				}

				for ( int yloop = sing - dy; yloop < sing + dy; yloop++ ) 
				{
					tgry = lgy + ( crow * sina - yloop * cosa ) / 0x4000;		// crow * sina / 0x4000 + xloop / tana + lgy;

					tgrx = lgx + ( crow * cosa - yloop * sina ) / 0x4000;

					if ( ( tgrx < gr->gridCount - 8) && ( tgrx > 8 ) && ( tgry < gr->gridCount - 8 ) && ( tgry > 8 ) )
					{
						hi = gr->getHSquare( tgrx, tgry );

						if ( !hibest || hi->height >= hibest->height )
						{
							if ( hi->height == hibest->height && !battle->data3D.terrain.isWooded(*gr->getTSquare( tgrx, tgry) ) ) continue;

							hibest = hi;

							bestx = tgrx;
							besty = tgry;
						}
					}
				}
			}
		}
	}

#ifdef DEBUG_CHRIS
	PutInLog( "\r\nBest height at " );
	PutInLog( bestx );
	PutInLog( "," );
	PutInLog( besty );
	PutInLog( " is " );
	PutInLog( hibest->height );
	PutInLog( " :radius=" );
	PutInLog( radius );
	PutInLog( "\r\n" );
#endif

	loca.x = BattleToDist( bestx * gr->gridSize - battle->grid->getTotalWidth()  / 2 );
	loca.y = BattleToDist( besty * gr->gridSize - battle->grid->getTotalHeight() / 2 );
	
	return loca;
}


// Patch Function !!!!

/*
 * What does this do????
 * Ends up setting joining flags
 */


void AI_Battle::clearSeperated( Unit* loyal, Boolean forget )
{
#ifdef CHRIS_AI
	if ( forget ) return;

 	Unit* ch = loyal->child;

	if ( !loyal->superior || !loyal->superior->battleInfo ) 
 	{
		ch->battleInfo->bJoining = True;

		return;
	}

	while ( ch )
	{
		if ( ch->child ) clearSeperated( ch, forget );

		if ( ch->battleInfo && ch->battleInfo->bSeperated == True || forget )
			ch->battleInfo->bJoining = True;

		ch = ch->sister;
	}
#endif
}

Location AI_Battle::KeepFormation( int which, Location enemy, Boolean Before )
{
	int times = ( which / 3 ) + 1;

	int tempwhich = which % 3;

	Distance width, height;

	Unit* loyal1 = Armylist[ which ];

	loyal1->child->battleInfo->getArea( width, height );	

	if ( times == 1 ) height = Yard( 1000 );

	Location temp( enemy.x, enemy.y );

#ifdef DEBUG_CHRIS
	PutInLog( "\r\nIn keep Formation (");
	PutInLog( which );
	PutInLog( ")\r\n" );
#endif

/*	if ( Before ) 
	{
 		temp.x -= mcos( height, ang );
 		temp.y -= msin( height, ang );
	}
*/
	switch ( tempwhich )
	{
		case 0:			// centre rear;
			
			if ( tact < Defence_In_Depth ) 
			{
				temp.x -= ( mcos( height, ang ) * times );
				temp.y -= ( msin( height, ang ) * times );
			}
			else
			{
				temp.x += ( mcos( width, ang ) * times );
				temp.y -= ( msin( width, ang ) * times );
			}
			break;

		case 1:			// right rear
			if ( tact < Defence_In_Depth )
			{
				temp.x += mcos( width, ang ) * times;
				temp.y -= msin( width, ang ) * times;
				temp.x -= mcos( height, ang ) * times;
				temp.y -= msin( height, ang ) * times;
			}
			else
			{
				temp.x -= ( mcos( width, ang ) * times );
				temp.y += ( msin( width, ang ) * times );
			}
			break;

		case 2: 			// left rear
			if ( tact < Defence_In_Depth )
			{
				temp.x -= mcos( width, ang ) * times;
				temp.y += msin( width, ang ) * times;
				temp.x -= mcos( height, ang ) * times;
				temp.y -= msin( height, ang ) * times;
			}
			else
			{
				temp.x += mcos( width, ang ) * times;
				temp.y -= msin( width, ang ) * times;
				temp.x -= mcos( height, ang ) * times;
				temp.y -= msin( height, ang ) * times;
			}
			break;
	}

	return temp;
}

Boolean AI_Battle::NotToClose( int xp, int yp )
{
	President *p = ob->getPresident( otherSide( side ) );

	Unit *enemyArmy = p->child;
	Unit *enemyCorps;
	Unit *enemyDivision;
	Unit *enemyBrigade;

	Unit* closest = 0;

	Distance best = 0;
	Distance d;

	int lx = xp;
	int ly = yp;

	while ( enemyArmy )
	{
		if ( enemyArmy->battleInfo || enemyArmy->hasCampaignReallyDetached() )
		{
			enemyCorps = enemyArmy->child;

			while( enemyCorps )
			{
				if ( enemyCorps->battleInfo || enemyCorps->hasCampaignReallyDetached() )
				{
					enemyDivision = enemyCorps->child;

					while( enemyDivision )
					{
						enemyBrigade = enemyDivision->child;
			
						while ( enemyBrigade )
						{
							if ( enemyBrigade->battleInfo )
							{
								d = distance( enemyBrigade->battleInfo->where.x - lx, enemyBrigade->battleInfo->where.y - ly );

								if ( ( !best || abs( d ) < best ) && abs( d ) < Mile( 2 ) )
								{
									best = abs( d );
									closest = enemyBrigade;
								}
  							}
							enemyBrigade = enemyBrigade->sister;
						}
						enemyDivision = enemyDivision->sister;
					}
				}
			enemyCorps = enemyCorps->sister;
			}
		}
		enemyArmy = enemyArmy->sister;
	}

	if ( !closest )
	{
		return 1;
	}

	return 0;
}

void AI_Battle::orderChildren( Unit* loyal, OrderMode action, const Location& destin )
{
	Unit* c1 = loyal->child;

	Grid* gr = battle->grid->getGrid( Zoom_1 );

	int lgx, lgy;

 	while ( c1 )
	{
	 	if( c1->battleInfo &&
			(c1->isBattleDetached() || c1->isCampaignReallyDetached() ) )
		{
		   Unit* enemy = FindClosestEnemy( c1 );
			
			Wangle angl = direction( c1->battleInfo->where.x - enemy->battleInfo->where.x, c1->battleInfo->where.y - enemy->battleInfo->where.y );

			Location dest = c1->battleInfo->where;

			// Distance d = distanceLocation( c1->battleInfo->where, loyal->battleInfo->battleOrder.destination );

			dest.x -= msin( Mile(5), angl );
			dest.y -= mcos( Mile(5), angl );

			lgx = DistToBattle( dest.x ) + battle->grid->getTotalWidth() / 2;
			lgy = DistToBattle( dest.y ) + battle->grid->getTotalHeight() / 2;

			while ( ( lgx > 0 && lgx < gr->gridCount ) && ( lgy > 0 && lgy < gr->gridCount ) )
			{
				dest.x -= msin( Mile(2), angl );
				dest.y -= mcos( Mile(2), angl );

				lgx = DistToBattle( dest.x ) + battle->grid->getTotalWidth() / 2;
				lgy = DistToBattle( dest.y ) + battle->grid->getTotalHeight() / 2;
			}

			Wangle wangl = direction( c1->battleInfo->where.x - destin.x, c1->battleInfo->where.y - destin.y );

			if ( ( ( (angl - wangl) < 0xc000 ) && ( ( angl - wangl ) > 0x4000 ) ) && !OnBattlefield )
			{
				sendAIOrder( action, destin, c1, enemy );
			}
			else sendAIOrder( action, dest, c1, enemy );

			// sendAIOrder( action, destin, c1, 0 );
		}

		if ( c1->hasBattleDetached() || c1->hasCampaignReallyDetached() )
			orderChildren( c1, action, destin );

		c1 = c1->sister;
	}
}

/*
 * 	Used to calculate in NewPlayerClickPoint.
 * 	Function returns the total strength within a set range of a unit,
 *		excluding the unit that has just been ordered
 */

int AI_Battle::CheckEnemyOrders( Unit* loyal, Unit* enemy )
{
#ifdef DEBUG
	aiBLog.printf("CheckEnemyOrders(%s, %s)",
		loyal ? loyal->getName(True) : "Null",
		enemy ? enemy->getName(True) : "Null");
#endif

  	President *p = ob->getPresident( otherSide( side ) );

	Unit *enemyArmy = p->child;
	Unit *enemyCorps;
	Unit *enemyDivision;
	Unit *enemyBrigade;

	Location loc = loyal->battleInfo->where;

	Distance d;

	Wangle myang = direction( loc.x - loyal->battleInfo->battleOrder.destination.x, loc.y - loyal->battleInfo->battleOrder.destination.y );

	CombatValue total = 0;

	while ( enemyArmy )
	{
		if ( ( enemyArmy->battleInfo || enemyArmy->hasCampaignReallyDetached() ) && enemyArmy != enemy )
		{
			enemyCorps = enemyArmy->child;

			while( enemyCorps )
			{
				if ( ( enemyCorps->battleInfo || enemyCorps->hasCampaignReallyDetached() ) && enemyCorps != enemy )
				{
					enemyDivision = enemyCorps->child;

					while( enemyDivision )
					{
					 if ( ( enemyDivision->battleInfo || enemyDivision->hasCampaignReallyDetached() ) && enemyDivision != enemy )
					 {	
					 	enemyBrigade = enemyDivision->child;
			
						while ( enemyBrigade )
						{
							if ( enemyBrigade->battleInfo && enemyBrigade != enemy )
							{
								d = distanceLocation( loc, enemyBrigade->battleInfo->battleOrder.destination );

								Wangle enemyAng = direction( loc.x - enemyBrigade->battleInfo->battleOrder.destination.x, loc.y - enemyBrigade->battleInfo->battleOrder.destination.y );

								if ( abs( d ) <= Yard( 1300 ) || ( myang - enemyAng ) < 0x1000 || ( myang - enemyAng ) > 0xf000 )
								{
									UnitInfo ui(False, False);

									enemyBrigade->getInfo(ui);

									total += ui.strength;
								}
  							}
							enemyBrigade = enemyBrigade->sister;
						}
					}
					enemyDivision = enemyDivision->sister;
				  }
				}
			enemyCorps = enemyCorps->sister;
			}
		}
		enemyArmy = enemyArmy->sister;
	}

  return total;
}

Boolean AI_Battle::IsEnemyInWay( Unit* loyal, Wangle direc )
{
  	President *p = ob->getPresident( otherSide( side ) );

	Unit *enemyArmy = p->child;
	Unit *enemyCorps;
	Unit *enemyDivision;
	Unit *enemyBrigade;

	while ( enemyArmy )
	{
		if ( enemyArmy->battleInfo || enemyArmy->hasCampaignReallyDetached() )
		{
			enemyCorps = enemyArmy->child;

			while( enemyCorps )
			{
				if ( enemyCorps->battleInfo || enemyCorps->hasCampaignReallyDetached() )
				{
					enemyDivision = enemyCorps->child;

					while( enemyDivision )
					{
					 if ( enemyDivision->battleInfo || enemyDivision->hasCampaignReallyDetached() )
					 {	
					 	enemyBrigade = enemyDivision->child;
			
						while ( enemyBrigade )
						{
							if ( enemyBrigade->battleInfo )
							{
								Wangle d = direction( loyal->battleInfo->where.x - enemyBrigade->battleInfo->battleOrder.destination.x, loyal->battleInfo->where.y - enemyBrigade->battleInfo->battleOrder.destination.y );

								if ( (d - direc) < 0x4000 || (d - direc) > 0xc000 )
								{
								  return True;	
								}
  							}
							enemyBrigade = enemyBrigade->sister;
						}
					}
					enemyDivision = enemyDivision->sister;
				  }
				}
			enemyCorps = enemyCorps->sister;
			}
		}
		enemyArmy = enemyArmy->sister;
	}

  return False;
}

Wangle AI_Battle::withdrawAngle( Unit* loyal )
{
  	President *p = ob->getPresident( otherSide( side ) );

	Unit *enemyArmy = p->child;
	Unit *enemyCorps;
	Unit *enemyDivision;
	Unit *enemyBrigade;

	char DirectionFlag = 0;

	while ( enemyArmy )
	{
		if ( enemyArmy->battleInfo || enemyArmy->hasCampaignReallyDetached() )
		{
			enemyCorps = enemyArmy->child;

			while( enemyCorps )
			{
				if ( enemyCorps->battleInfo || enemyCorps->hasCampaignReallyDetached() )
				{
					enemyDivision = enemyCorps->child;

					while( enemyDivision )
					{
					 if ( enemyDivision->battleInfo || enemyDivision->hasCampaignReallyDetached() )
					 {	
					 	enemyBrigade = enemyDivision->child;
			
						while ( enemyBrigade )
						{
							if ( enemyBrigade->battleInfo )
							{
								Wangle d = direction( loyal->battleInfo->where.x - enemyBrigade->battleInfo->battleOrder.destination.x, loyal->battleInfo->where.y - enemyBrigade->battleInfo->battleOrder.destination.y );

								if ( d < 0x1000 ) DirectionFlag |= 1;
								else if ( d < 0x3000 ) DirectionFlag |= 2;
								else if ( d < 0x5000 ) DirectionFlag |= 4;
								else if ( d < 0x7000 ) DirectionFlag |= 8;
								else if ( d < 0x9000 ) DirectionFlag |= 16;
								else if ( d < 0xb000 ) DirectionFlag |= 32;
								else if ( d < 0xd000 ) DirectionFlag |= 64;
								else if ( d < 0xf000 ) DirectionFlag |= 128;
								else DirectionFlag |= 1;
  							}
							enemyBrigade = enemyBrigade->sister;
						}
					}
					enemyDivision = enemyDivision->sister;
				  }
				}
			enemyCorps = enemyCorps->sister;
			}
		}
		enemyArmy = enemyArmy->sister;
	}

   Wangle wang;

	for ( char a = 0; a < 8; a++ )
	{
		if ( !( DirectionFlag | ( 1 << a ) ) ) wang = a * 0x2000;
	}

	return wang;
}

