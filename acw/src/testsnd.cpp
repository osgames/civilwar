/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Sound Effect Testing program
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <conio.h>
#include <stdlib.h>
#include <ctype.h>

#include "daglib.h"
#include "sndlib.h"
#include "ilbm.h"
#include "colours.h"

static const char* soundNames[] = {
	"explose4",		// Use for cannon
	"belch",			// Use for infantry fire!
	"melee",
	"testsnd"		// Use for testing?
};

const int SoundCount = 3;

/*
 * Main Function
 */

void main(int argc, char *argv[])
{
	/*
	 * Use try/catch to make use of exceptions
	 */

	try
	{
		/*
		 * Process command line
		 */

#ifdef DEBUG
		MemoryLog::Mode memMode = MemoryLog::Summary;
		int debugLevel = 0;
#endif

		int i = 0;
		while(++i < argc)
		{
			const char* arg = argv[i];

			if((arg[0] == '-') || (arg[0] == '/'))
			{
				switch(toupper(arg[1]))
				{
#ifdef DEBUG
				case 'M':										// -M for Memory Logging
					if(toupper(arg[2]) == 'S')				// -MS for memory summary
						memMode = MemoryLog::Summary;
					else
						memMode = MemoryLog::Full;
					break;

				case 'D':										// -D for debug log
					if(isdigit(arg[2]))
						debugLevel = atoi(&arg[2]);
					else
						debugLevel = 32767;
					break;
#endif
				}
			}
		}


		/*
		 * Set up the system.
		 *
		 * This sets up the video mode, mouse, timer, fonts
		 *
		 * Things will automatically be restored when the
		 * program finishes and machine's destructor is called.
		 */


		machine.init();
#ifdef DEBUG
		memLog->setMode("memory.log", memMode);
		if(debugLevel)
			logFile = new LogStream("debug.log", debugLevel);
#endif

		machine.screen->getBitmap()->fill(Green);
		readILBM("art\\screens\\playscrn.lbm", 0, machine.screen->getDeferredPalette());
		machine.screen->updatePalette(20);		// quarter second fade up
		machine.screen->setUpdate();

		Region r(machine.screen->getImage(), Rect(220, 50, 200, 70));
		r.fill(White);
		r.frame(0,0,200,60, Yellow, LGreen);
		TextWindow window(&r, Font_EMFL32);
		window.setFormat(True, True);
		window.setColours(Black);
		window.draw("Sound Effect\rTester");
		machine.screen->setUpdate(r);
		machine.screen->update();

		sound.init();

		machine.screen->getBitmap()->fill(Green);
		machine.screen->setUpdate();
		r.fill(White);
		r.frame(0,0,200,60, Yellow, LGreen);
		window.setColours(Black);
		window.draw("Sound Effect Tester");

		/*
		 * Area on screen to display information
		 */

		window.setFont(Font_EMFL15);
		r.setClip(20, 140, 600, 20);
		r.fill(LGreen);
		r.frame(0,0,600,20, Yellow);
		window.setFormat(True, True);

		window.wprintf("Opening Sound Library");
		machine.screen->setUpdate(r);
		machine.screen->update();

		SoundLibrary library("testfx.snd", soundNames);

		r.setClip(20, 170, 600, 20);
		r.fill(LGreen);
		r.frame(0,0,600,20, Yellow);
		window.wprintf("Playing sound");
		machine.screen->setUpdate(r);
		machine.screen->update();

		library.playSound(0, MaxVolume, 0);
		int nextSound = 1;
		int pan = 0;

		char buffer[80];
		int y = 4;

		while(!machine.mouse->getEvent())
		{
			if(kbhit())
			{
				int ch = getch();
				if(ch == 0)
					ch = getch();

				library.playSound(nextSound, MaxVolume, pan);
				nextSound++;
				if(nextSound >= SoundCount)
					nextSound = 0;
				pan = (++pan) & 63;
			}

			int y1 = 4;

			r.setClip(20, 260, 600, 20);
			r.fill(LGreen);
			r.frame(0,0, 600, 20, Yellow);

#if 0
			sprintf(buffer, "Status: %s",
				(ampGetModuleStatus() == MD_PLAYING) ? "Playing" : "Paused");
			window.setPosition(Point(4, y1));
			window.draw(buffer);
			y1 += 16;

			sprintf(buffer, "Pattern: %d, Row: %d, Sync: %d",
				ampGetPattern(),
				ampGetRow(),
				ampGetSync());
			window.setPosition(Point(4, y1));
			window.draw(buffer);
			y1 += 16;
#endif

			window.setPosition(Point(4, 4));
			window.wprintf("Playing");

			machine.screen->setUpdate(r);
			machine.screen->update();

			soundManager.process();
		}

		// Let SoundLibrary Destructor tidy up.

	}
 	catch(GeneralError e)
 	{
		cout << "Untrapped Error: ";
		if(e.get())
			cout << e.get();
		else
			cout << "No description";
		cout << endl;
 	}
 	catch(...)
 	{
 		cout << "Caught some kind of error" << endl;
 	}
}


