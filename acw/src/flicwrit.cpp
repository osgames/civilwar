/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	FLIC File writer
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "flicwrit.h"

FlicWrite::FlicWrite(const char* fileName)
{
	/*
	 * Initialise header
	 */

	memset(&header, 0, sizeof(header));
	header.type = FLC_TYPE;
	header.depth = 8;
	header.flags = FLI_FINISHED | FLI_LOOPED;
	header.speed = 20;
	header.aspect_dx = 1;
	header.aspect_dy = 1;


	error = file.fileOpen(fileName);
	if(!error)
	{
		/*
		 * Put in a dummy header
		 */

		error = file.fileWrite(&header, sizeof(header));

		if(error)
			file.fileClose();
	}
}

FlicWrite::~FlicWrite()
{
	/*
	 * Rewrite header
	 */

	header.size = file.getPos();
	file.fileSeek(0);
	file.fileWrite(&header, sizeof(header));
	file.fileClose();
}


void FlicWrite::addLastFrame(Image& img, Palette& p)
{
	addFrame(img, p);
	header.frames--;
}

void FlicWrite::addFrame(Image& img, Palette& p)
{

	/*
	 * If 1st frame then set up image and also header's width/height and oframe1
	 */

	if(header.frames == 0)
	{
		header.width = img.getWidth();
		header.height = img.getHeight();
		header.oframe1 = file.getPos();

		image.resize(img.getWidth(), img.getHeight());
	}

	/*
	 * If 2nd frame then set up header's oframe2
	 */

	if(header.frames == 1)
	{
		header.oframe2 = file.getPos();
	}

	/*
	 * Write a dummy FrameHead
	 */

	FrameHead frameHead;
	memset(&frameHead, 0, sizeof(frameHead));
	frameHead.type = FRAME_TYPE;
	long framePos = file.getPos();

	file.fileWrite(&frameHead, sizeof(frameHead));

	/*
	 * Write chunks
	 */

	if(header.frames == 0)	// Write the palette if it's the 1st frame
	{
		writeColor256(p);
		frameHead.chunks++;

		writeByteRun(img);
		frameHead.chunks++;
	}
	else
	{
		writeDeltaFLC(img);
		frameHead.chunks++;
	}

	/*
	 * rewrite FrameHead
	 */

	long newPos = file.getPos();
	frameHead.size = newPos - framePos;
	file.fileSeek(framePos);
	file.fileWrite(&frameHead, sizeof(frameHead));
	file.fileSeek(newPos);

	header.frames++;

}

class ChunkWrite {
	ChunkHead head;
	FileHandleW* file;
	long headPos;
public:
	ChunkWrite(ChunkTypes type, FileHandleW* f);
	~ChunkWrite();
};

ChunkWrite::ChunkWrite(ChunkTypes type, FileHandleW* f)
{
	file = f;
	head.size = 0;
	head.type = type;
	headPos = file->getPos();
	file->fileWrite(&head, sizeof(head));
}

ChunkWrite::~ChunkWrite()
{
	long newPos = file->getPos();
	head.size = newPos - headPos;
	file->fileSeek(headPos);
	file->fileWrite(&head, sizeof(head));
	file->fileSeek(newPos);
}

void FlicWrite::writeDeltaFLC(Image& img)
{
	ChunkWrite chunk(DELTA_FLC, &file);

	UWORD lpCount = 0;

	long lpPos = file.getPos();
	file.writeWord(lpCount);

	UBYTE* src = img.getAddress();
	UBYTE* dest = image.getAddress();
	int lineCount = img.getHeight();

	int skipLines = 0;

	while(lineCount--)
	{
		/*
		 * Check for line exactly the same
		 */

		if(memcmp(src, dest, img.getWidth()) == 0)
		{
			skipLines++;
		}
		else
		{
			if(skipLines)
			{
				file.writeWord(65536 - skipLines);
				skipLines = 0;
			}

			lpCount++;

			UWORD opCount = 0;
			long opPos = file.getPos();
			file.writeWord(opCount);

			/*
			 * Count across to changed word
			 */

			UWORD* sw = (UWORD*)src;
			UWORD* dw = (UWORD*)dest;

			int lastX = 0;
			int x = 0;

			while(x < img.getWidth())
			{
				while( (x < img.getWidth()) && (*sw == *dw))
				{
					x += 2;
					sw++;
					dw++;
				}

				if(x < img.getWidth())
				{
					do
					{
						int diff = x - lastX;
						if(diff > 0xff)
						{
							file.writeByte(0xff);
							file.writeByte(0);		// Dummy copy
							opCount++;
							lastX += 255;
						}
						else
						{
							file.writeByte(x - lastX);
							lastX = x;
						}
					} while(x != lastX);
				
					/*
					 * Now we should look for repeated words
					 */

					/*
					 * First find how much we have that is different
					 */

					UWORD* sw1 = sw;
					UWORD* dw1 = dw;

					int psize = 0;

					while( (x < img.getWidth()) && (*sw != *dw) )
					{
						x += 2;
						psize++;
						sw++;
						dw++;
					}

					/*
					 * Within the given range, we should RLE encode it.
					 * But I can't be bothered.. so just do a copy segment
					 */

					while(psize)
					{
						opCount++;
						if(psize > 0x7f)
						{
							file.writeByte(0x7f);
							file.fileWrite(sw1, 0x7f * 2);
							psize -= 0x7f;
							sw1 += 0x7f * 2;
							lastX += 0x7f * 2;

							file.writeByte(0);		// x position = 0 fornext section
						}
						else
						{
							file.writeByte(psize);
							file.fileWrite(sw1, psize * 2);
							sw1 += psize * 2;
							lastX += psize * 2;
							psize = 0;
						}
					}
				}
			}

			long newPos = file.getPos();
			file.fileSeek(opPos);
			file.writeWord(opCount);
			file.fileSeek(newPos);
		}


		memcpy(dest, src, img.getWidth());

		src += img.getWidth();
		dest += img.getWidth();

	}

	long newPos = file.getPos();
	file.fileSeek(lpPos);
	file.writeWord(lpCount);
	file.fileSeek(newPos);
}

void FlicWrite::writeColor256(Palette& p)
{
	ChunkWrite chunk(COLOR_256, &file);

	file.writeWord(1);		// Only 1 chunk
	file.writeByte(0);		// Start at colour 0
	file.writeByte(0);		// Store all 256 colours
	file.fileWrite(p.getColour(0), 256 * 3);

	memcpy(palette.getColour(0), p.getColour(0), 256 * 3);
}

void FlicWrite::writeByteRun(Image& img)
{
	ChunkWrite chunk(BYTE_RUN, &file);

	UBYTE* src = img.getAddress();
	int lCount = img.getHeight();

	/*
	 * Copy into flic's image for comparison
	 */

	memcpy(image.getAddress(), src, img.getWidth() * img.getHeight());

	while(lCount--)
	{
		UBYTE opcount = 0;
		long opPos = file.getPos();
		file.writeByte(opcount);

		UBYTE* lineAd = src;

		int width = img.getWidth();

		while(width)
		{
			if( (width > 1) && (src[0] == src[1]))
			{	// Repeated byte
				UBYTE v = *src++;
				int psize = 1;
				width--;

				while(width && (src[0] == v))
				{
					src++;
					psize++;
					width--;
				}

				while(psize)
				{
					if(psize > 127)
					{
						opcount++;
						file.writeByte(127);
						file.writeByte(v);
						psize -= 127;
					}
					else
					{
						opcount++;
						file.writeByte(psize);
						file.writeByte(v);
						psize = 0;
					}
				}
			}
			else	// Byte copy
			{
				UBYTE* start = src;

				int psize = 1;
				UBYTE v = *src++;
				width--;

				while(width && ((src[0] != v) || (src[1] != v)))
				{
					v = *src++;
					psize++;
					width--;
				}

				if(width)
				{
					psize--;
					src--;
					width++;
				}

				while(psize)
				{
					if(psize > 128)
					{
						opcount++;
						file.writeByte(256 - 128);
						file.fileWrite(start, 128);
						start += 128;
						psize -= 128;
					}
					else
					{
						opcount++;
						file.writeByte(256 - psize);
						file.fileWrite(start, psize);
						psize = 0;
					}
				}
			}
		}

		long newPos = file.getPos();
		file.fileSeek(opPos);
		file.writeByte(opcount);
		file.fileSeek(newPos);

		src = lineAd + img.getWidth();
	}
}

