/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Miscellaneous objects such as trees and buildings
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1994/06/21  18:45:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/04/11  13:36:38  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "miscob.h"
#include "shplist.h"
#include "view3d.h"
#include "sprite3d.h"
#include "batldata.h"
#include "map3d.h"

MiscObject::MiscObject(const Location& l, BattleSprite spriteIndex) :
	MapObject(l, OT_Other)
{
	sprite = spriteIndex;
}


void MiscObject::draw3D(ObjectDrawData* drawData)
{
	if(drawData->view->isOnDisplay(location))
	{
		Sprite3D spr(*battle->spriteLib, sprite);
		Position3D pos = LocationToPoint3D(location, battle->grid);
		spr.draw(drawData, pos);
	}
}
