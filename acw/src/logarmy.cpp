/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Print army information to log file
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.12  1994/07/19  19:53:03  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/07/11  14:26:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/06/21  18:49:27  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.8  1994/05/19  17:44:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/04/06  12:40:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/03/15  15:15:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/03/10  14:27:18  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/02/18  22:51:35  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/02/17  14:57:44  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/02/15  23:22:28  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/02/09  14:59:26  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "logarmy.h"

#if !defined(BATEDIT) && !defined(TESTBATTLE)
#include "campaign.h"
#include "campwld.h"
#endif

#include "generals.h"
#include "unititer.h"
#include "tables.h"

#if defined(LOG_ARMIES)

#include "log.h"

class IndentString {
	char introString[16];
	int nest;
public:
	IndentString() { nest = 0; introString[0] = '\0'; }
	void indent(Boolean isLast);
	void undent();

	const char* str() const { return introString; }
};

struct LogCount {
	long presidentCount;
	long armyCount;
	long corpsCount;
	long divisionCount;
	long brigadeCount;
	long regimentCount;

	clear()
	{
		presidentCount = 0;
		armyCount = 0;
		corpsCount = 0;
		divisionCount = 0;
		brigadeCount = 0;
		regimentCount = 0;
	}
};

void IndentString::indent(Boolean isLast)
{
	char* s = &introString[nest];

	*s++ = isLast ? ' ' : '\xb3';
	*s++ = ' ';
	*s++ = ' ';
	*s = '\0';
	nest += 3;
}

void IndentString::undent()
{
	nest -= 3;
	introString[nest] = '\0';
}

static IndentString indent;
static LogCount counters;

void Unit::logInfo(Boolean isLast)
{
	isLast = isLast;
	*logFile << "loginfo() Undefined type " << endl;
}

void Regiment::logInfo(Boolean isLast)
{
	counters.regimentCount++;

	*logFile << indent.str();

	if(isLast)
		*logFile << "\xc0";
	else
		*logFile << "\xc3";

	*logFile << "\xc4 Regiment: \"" << getName(True) << "\"" << endl;
}

void Brigade::logInfo(Boolean isLast)
{
	counters.brigadeCount++;

	*logFile << indent.str();

	if(isLast)
		*logFile << "\xc0";
	else
		*logFile << "\xc3";

	*logFile << "\xc4 Brigade: \"" << getName(True) << "\" has ";
	*logFile << (int)childCount << " Regiments" << endl;

	if(childCount)
	{
		Unit* u = child;

		indent.indent(isLast);

		while(u)
		{
			u->logInfo(u->sister == 0);
			u = u->sister;
		}

		indent.undent();
	}
}

void Division::logInfo(Boolean isLast)
{
	counters.divisionCount++;

	*logFile << indent.str();

	if(isLast)
		*logFile << "\xc0";
	else
		*logFile << "\xc3";

	*logFile << "\xc4 Division: \"" << getName(True) << "\" has ";
	*logFile << (int)childCount << " Brigades" << endl;

	if(childCount)
	{
		Unit* u = child;
		indent.indent(isLast);

		while(u)
		{
			u->logInfo(u->sister == 0);
			u = u->sister;
		}

		indent.undent();
	}
}

void Corps::logInfo(Boolean isLast)
{
	counters.corpsCount++;

	*logFile << indent.str();

	if(isLast)
		*logFile << "\xc0";
	else
		*logFile << "\xc3";

	*logFile << "\xc4 Corps: \"" << getName(True) << "\" has ";
	*logFile << (int)childCount << " Divisions" << endl;

	if(childCount)
	{
		Unit* u = child;
		indent.indent(isLast);

		while(u)
		{
			u->logInfo(u->sister == 0);
			u = u->sister;
		}

		indent.undent();
	}
}

void Army::logInfo(Boolean isLast)
{
	counters.armyCount++;

	*logFile << indent.str();

	if(isLast)
		*logFile << "\xc0";
	else
		*logFile << "\xc3";

	*logFile << "\xc4 Army: \"" << getName(True) << "\" has ";

	if(childCount)
	{
		*logFile << (int)childCount << " Corps" << endl;

		Unit* u = child;
		indent.indent(isLast);

		while(u)
		{
			u->logInfo(u->sister == 0);
			u = u->sister;
		}

		indent.undent();
	}
	else
		*logFile << "no Corps" << endl;
}

void President::logInfo(Boolean isLast)
{
	counters.presidentCount++;

	if(isLast)
		*logFile << "\xc0";
	else
		*logFile << "\xc3";

	*logFile << "\xc4 Side: has ";

	if(childCount)
	{
		*logFile << (int)childCount << " armies" << endl;

		Unit* army = child;
		indent.indent(isLast);
		while(army)
		{
			army->logInfo(army->sister == 0);
			army = army->sister;
		}


		indent.undent();
	}
	else
		*logFile << "no Armies" << endl; 
}

void logArmies(Unit* top)
{
	if(logFile && logFile->isShown(30))
	{
		int howMany;
	 
		counters.clear();

		if(top)
			howMany = top->childCount;
		else
			howMany = 0;

		if(howMany)
		{
			*logFile << "\n\n\nThere are " << howMany << " Sides in memory" << endl;

			while(top)
			{
				top->logInfo(top->sister == 0);
				top = top->sister;
			}
		}
		else
			*logFile << "There are no Sides in memory!" << endl;

		*logFile << "\nTotals:" << endl;
		long total = counters.presidentCount;
		*logFile << "    Sides: " << counters.presidentCount << endl;
		total += counters.armyCount;
		*logFile << "   Armies: " << counters.armyCount      << "(" << total << ")" << endl;
		total += counters.corpsCount;
		*logFile << "    Corps: " << counters.corpsCount     << "(" << total << ")" << endl;
		total += counters.divisionCount;
		*logFile << "Divisions: " << counters.divisionCount  << "(" << total << ")" << endl;
		total += counters.brigadeCount;
		*logFile << " Brigades: " << counters.brigadeCount   << "(" << total << ")" << endl;
		total += counters.regimentCount;
		*logFile << "Regiments: " << counters.regimentCount  << "(" << total << ")" << endl;

		*logFile << "\n\n\n" << endl;
	}
}

#endif	// LOG_ARMIES

#ifdef LOG_GENERALS

#include "clog.h"

class GeneralLog : public LogFile {
public:
	GeneralLog(const char* name) : LogFile(name) { }
	void logGeneral(const General* g);
};


void GeneralLog::logGeneral(const General* g)
{
	ASSERT(g != 0);

	const char* sSide = sideStr[g->getSide()];
	const char* sUnit;
	Unit* unit = g->getAllocation();
	if(unit == 0)
		sUnit = "Unallocated";
	else
		sUnit = unit->getName(False);

	printf("%3s %-15s Eff:%3d, Abl:%3d, Agr:%3d, Exp:%3d, Rank:%-8s, %-15s",
		sSide, 
		g->getName(), 
		(int) g->efficiency,
		(int) g->ability,
		(int) g->aggression,
		(int) g->experience,
		rankStr(g->getRank()),
		sUnit);
}

void logGenerals()
{
	if(campaign == 0)
		return;
	if(campaign->world == 0)
		return;

	static char fileName[] = "generals.txt";

	GeneralLog gLog(fileName);

	gLog.setTime(0);
	gLog.printf("Civil War Generals");
	gLog.printf("");

	President* p = campaign->world->ob.getSides();
	ASSERT(p != 0);

	while(p)
	{
		UnitIter iter(p, False, False, Rank_Army, Rank_Brigade);

		while(iter.ok())
		{
			const Unit* unit = iter.get();
			ASSERT(unit != 0);

			if(unit->general)
				gLog.logGeneral(unit->general);
		}

		GeneralList* gl = p->getFreeGenerals();
		if(gl->entries() != 0)
		{
			GeneralIter gi = *gl;
			while(++gi)
			{
				const General* g = gi.current();
				ASSERT(g != 0);
				gLog.logGeneral(g);
			}
		}


		p = (President*) p->sister;
	}

}

#endif
