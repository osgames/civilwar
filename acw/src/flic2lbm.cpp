/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Utility to pull out frames from FLI/FLC files and place them
 * on a 640 by 480 LBM screen with lines around them.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <conio.h>
#include <stdlib.h>
#include <ctype.h>

#include "daglib.h"
#include "flicread.h"
#include "makename.h"
#include "ilbm.h"

/*
 * Find which part of an FLI is used and return the background colour
 */
																		 
UBYTE getUsedRectangle(Image& image, Rect& r)
{
	int y = 0;


	UBYTE* ad = image.getAddress();

	UBYTE background = *ad;

	SDimension minX = image.getWidth();
	SDimension maxX = 0;
	SDimension minY = image.getHeight();
	SDimension maxY = 0;

	while(y < image.getHeight())
	{
		UBYTE* linead = ad;

		int x = 0;
		while(x < image.getWidth())
		{
			if(*ad++ != background)
			{
				if(x < minX)
					minX = x;
				if(x > maxX)
					maxX = x;
				if(y < minY)
					minY = y;
				if(y > maxY)
					maxY = y;
			}
			x++;
		}

		y++;
		ad = linead + image.getWidth();
	}

	r = Rect(minX, minY, maxX - minX + 1, maxY - minY + 1);

	return background;
}

void usage()
{
	cout << "Usage:\n" <<
			  "\tflic2lbm [options] flicfile.flc\n\n" <<
			  "\tOptions are:\n" <<
			  "\t\t-b n : Set background colour to colour n\n"
			  "\t\t-o filename.lbm : set output filename\n" <<
			  endl;
}

void main(int argc, char* argv[])
{
	UBYTE background = 0;
	Boolean gotBackground = False;
	char* fliName = 0;
	char* lbmName = 0;

	/*
	 * Read arguments
	 */

	int i = 0;
	while(++i < argc)
	{
		char* arg = argv[i];

		char c = *arg++;

		if((c == '-') || (c == '/'))
		{
			c = *arg++;

			switch(toupper(c))
			{
			case 'B':	// Set background colour
				if(++i < argc)
				{
					arg = argv[i];
					if(isdigit(*arg))
					{
						background = atoi(arg);
						gotBackground = True;
#if 0
						cout << "Background Colour " << (int)background << endl;
#endif
						break;
					}
				}

				cout << "-B must be followed by a number" << endl;
				usage();
				return;

			case 'O':	// Set output filename
				if(++i < argc)
				{
					if(!lbmName)
					{
						lbmName = argv[i];
#if 0
						cout << "Output file: " << lbmName << endl;
#endif
						break;
					}
					else
					{
						cout << "There can only be one output filename" << endl;
						usage();
						return;
					}
				}
				cout << "-O must be followed by a filename" << endl;
				usage();
				return;
			default:
				cout << "unknown option " << argv[i] << endl;
				usage();
				return;
			}
		}
		else	// Assume input file
		{
			if(!fliName)
			{
				fliName = argv[i];
#if 0
				cout << "Input file: " << fliName << endl;
#endif
			}
			else
			{
				cout << "There can only be one input file" << endl;
				usage();
				return;
			}
		}
	}

	/*
	 * See what we've got
	 */

	if(!fliName)
	{
		cout << "No FLI/FLC file specified" << endl;
		usage();
		return;
	}

	char inName[120];
	char outName[120];

	makeFilename(inName, fliName, ".FLC", False);

	if(!lbmName)
		makeFilename(outName, fliName, ".LBM", True);
	else
		makeFilename(outName, lbmName, ".LBM", False);

	/*
	 * Summarise parameters
	 */

	cout << "Converting " << inName << " to " << outName << endl;
	if(gotBackground)
		cout << " Background/frame colour is " << (int) background << endl;
	else
		cout << "Each frame's 1st pixel will be used as a background colour" << endl;

	try
	{

		FlicRead fli(inName);	// This opens it

		if(fli.getError())
		{
			cout << "Error in FLIC file " << argv[1] << endl;
			return;
		}

		const FlicHead* head = fli.getHeader();

		cout << "Header for " << argv[1] << endl;
		cout << "Size: " << head->size << endl;
		cout << "Type: " << hex << head->type << dec << endl;
		cout << "frames: " << head->frames << endl;
		cout << "width: " << head->width << endl;
		cout << "height: " << head->height << endl;
		cout << "depth: " << head->depth << endl;
		cout << "flags: " << head->flags << endl;
		cout << "speed: " << head->speed << endl;

		/*
		 * For each frame
		 * Cut out white space
		 * copy to lbm screen
		 */

		Bitmap lbmImage(Point(640, 480));

		lbmImage.fill(background);
		SDimension topX = 1;		// Position for next sprite
		SDimension topY = 1;
		SDimension maxY = 1;		// Lowest level of previous sprite
		int fileCount = 0;
		Boolean firstFrame = True;

		while(!fli.finished)
		{
			cout << "Reading frame " << fli.frame;

			fli.nextFrame();

			/*
			 * Find square inside fli's image containing non-black
			 */

			Rect r;
			UBYTE back = getUsedRectangle(fli.image, r);
			if(!gotBackground)
			{
				background = back;
				if(firstFrame)
					lbmImage.fill(background);
			}
			firstFrame = False;

			cout << ", Inner rectangle is " <<
					r.x << "," << r.y << "," << r.getW() << "," << r.getH();

			/*
			 * See if rectangle will fit on current screen
			 * If it won't then write screen and start a new one
			 * blit image onto new screen
			 */

			if((topX + r.getW() + 2) >= (lbmImage.getW() - 1))
			{
				topX = 1;
				topY = maxY;

				if((topY + r.getH() + 2) >= (lbmImage.getH() - 1))
				{
					cout << endl;
					writeILBM(outName, *lbmImage.getImage(), fli.palette, background);
					char* s1 = strrchr(outName, '\\');
					if(!s1)
						s1 = strrchr(outName, ':');
					if(!s1)
						s1 = outName;
					else
						s1++;

					char newName[5];
					char* s = strrchr(outName, '.');
					if(s)
						strcpy(newName, s);
					else
						strcpy(newName, ".LBM");

					int count = 6;
					while(*s1 && (*s1 != '.') && count)
					{
						s1++;
						count--;
					}
					while(count--)
						*s1++ = '_';
					
					sprintf(s1, "%02d%s", fileCount++, newName);

					cout << "Starting new file " << outName;

					/*
				 	 * Set up new top values
				 	 */

					lbmImage.fill(background);
					topX = 1;		// Position for next sprite
					topY = 1;
					maxY = 1;		// Lowest level of previous sprite
				}
			}

			cout << ", Placing at " << topX << "," << topY << endl;

			lbmImage.frame(topX, topY, r.getW()+2, r.getH()+2, 0, 0);

			lbmImage.blit(&fli.image, Point(topX+1, topY+1), r);

			// Replace colour 0 with background colour

			UBYTE* ad = lbmImage.getAddress(Point(topX+1, topY+1));
			int h = r.getH();
			while(h--)
			{
				UBYTE* ad1 = ad;
				int w = r.getW();

				while(w--)
				{
					if( (*ad == 0) || (*ad == back) )
						*ad = background;
					ad++;
				}

				ad = ad1 + lbmImage.getW();
			}


			topX += r.getW() + 3;
			SDimension y1 = topY + r.getH() + 3;
			if(y1 > maxY)
				maxY = y1;
		}

		writeILBM(outName, *lbmImage.getImage(), fli.palette, background);
	}
	catch( GeneralError e )
	{
		cout << "Error:";
		if ( e.get() )
		 	cout << e.get();
		else
			cout << " ??";
		cout << endl;
		getch();
	}
	catch( ...)
	{
		cout << "Unknown error!\n";
		getch();
	}
}
