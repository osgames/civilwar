/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Buffered Physical Screen
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.1  1994/06/02  15:27:46  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "screen.h"
#include "mouse.h"
#include "vesa.h"
#include "palette.h"
#include "timer.h"
#include "pal_s.h"

#ifdef DEBUG
#include "error.h"
#endif

#ifdef TESTING
#include "text.h"
#include "memalloc.h"
#include "colours.h"
#endif

/*
 * Define NO_PALETTE_IRQ to disable interrupt
 */

#define NO_PALETTE_IRQ

/*
 * Screen class implementation
 */

static Screen* theScreen = 0;

Screen::Screen()
{
	physical = new Vesa(0x101);
	size = physical->getSize();

	buffer = new Bitmap(size);
	buffer->fill(Colour(0));

	mouse = 0;
	deferredPalette = new Palette;
	deferredPalette->setBlack();

	palette = new Palette;
	palette->setBlack();
	paletteChanged = True;
	fadeRate = 0;

	realPalette = new Palette;
	realPalette->setBlack();
	paletteWrong = True;
	realFadeRate = 0;

	/*
	 * Set up the rectangles in the free list
	 */

	LinkedRect* r = rectangles;
	int count = MaxUpdateRects;
	while(count--)
		freeRects.add(r++);

	/*
	 * Add the whole screen to be updated
	 */

	setUpdate();

	theScreen = this;
#if !defined(NO_PALETTE_IRQ)
	palTimerTag = timer->addRoutine(paletteInterrupt, 80);		// Call palette switcher at 80Hz
#endif
#if defined(NO_PALETTE_IRQ)
	while(paletteWrong)
		::updatePalette();
#endif
}

Screen::~Screen()
{
#if !defined(NO_PALETTE_IRQ)
	timer->removeRoutine(palTimerTag);
#endif

	delete deferredPalette;
	delete realPalette;
	delete palette;
	delete buffer;
	// delete image;
	delete physical;
}

void Screen::physBlit(Image* image, const Point& dest, const Rect& src)
{
	physical->blit(image, dest, src);
}

void Screen::physMaskBlit(Sprite* sprite, const Point& dest)
{
	physical->maskBlit(sprite, dest);
}


void Screen::blit(Image* image, const Point& dest, const Rect& src)
{
	Rect rect;
	if(src.getW())
		rect = Rect(dest, src.getSize());
	else
		rect = Rect(dest, image->getSize());

	setUpdate(rect);

 	buffer->getImage()->blit(image, dest, src);
}

void Screen::physUnBlit(Image* image, const Point& where)
{
	physical->unBlit(image, where);
}

void Screen::unBlit(Image* image, const Point& where)
{
	image->blit(buffer->getImage(), Point(0,0), Rect(where, image->getSize()));
}

void Screen::maskBlit(Sprite* sprite, const Point& dest)
{
	// image->maskBlit(sprite, dest);
	buffer->maskBlit(sprite, dest);
}


/*
 * Update the screen
 */

void Screen::update()
{
#ifdef DEBUG
	checkRectangles();
#endif

#if defined(TESTING) && !defined(SYSTEM_MALLOC)
	/*
	 * Display free memory
	 */

	Region region = Region(getImage(), Rect(520,470, 100,8));
	region.fill(White);
	TextWindow wind(&region, Font_JAME_5);
	wind.setVCentre(True);
	wind.setHCentre(True);
	wind.wprintf("%ld, %ld", currentAllocated, maximumAllocated);
	setUpdate(region);

#endif

	LinkedRect* r;
	while((r = usedRects.get()) != 0)
	{
		/*
		 * Blit the rectangle from buffer to physical screen
		 */

		if(mouse)
			mouse->blitWithMouse(buffer->getImage(), r->rect, r->rect);
		else
			physical->blit((Image*)buffer, r->rect, r->rect);

		/*
		 * Remove from used list
		 */

		freeRects.add(r);
	}

#ifdef DEBUG
	checkRectangles();
#endif

	if(paletteChanged)
	{
		paletteChanged = False;
		setPaletteNow(deferredPalette, fadeRate);
	}
}

/*
 * Add a rectangle
 */

void Screen::setUpdate()
{
	setUpdate(*physical);
}

void Screen::setUpdate(const Rect& rect)
{
#ifdef DEBUG
	checkRectangles();
#endif


	/*
	 * Ignore zero size
	 */

	if(rect.getW() == 0)
		return;
	if(rect.getH() == 0)
		return;

	/*
	 * Copy rectangle because we may be chopping bits off if it overlaps
	 */

	Rect area = rect;
	Point p1 = Point(area + area.getSize());

	/*
	 * Check if it is covered already?
	 */

	LinkedRect* r = usedRects.entry;
	while(r)
	{
		LinkedRect* nextR = r->next;


		enum IntersectType {
			None,
			Complete,
			Intersects,
			Covered
		};

		IntersectType xIntersect;
		IntersectType yIntersect;

		Point p2 = r->rect + r->rect.getSize();

		/*
		 * Does it overlap at all?
		 */

		if(area.x < r->rect.x)
		{
			if(p1.x < r->rect.x)
				xIntersect = None;
			else if(p1.x < p2.x)
				xIntersect = Intersects;
			else
				xIntersect = Complete;
		}
		else if(area.x < p2.x)
		{
			if(p1.x <= p2.x)
				xIntersect = Covered;
			else
				xIntersect = Intersects;
		}
		else
		{
			xIntersect = None;
		}

		if(xIntersect != None)
		{
			if(area.y < r->rect.y)
			{
				if(p1.y < r->rect.y)
					yIntersect = None;
				else if(p1.y < p2.y)
					yIntersect = Intersects;
				else
					yIntersect = Complete;
			}
			else if(area.y < p2.y)
			{
				if(p1.y <= p2.y)
					yIntersect = Covered;
				else
					yIntersect = Intersects;
			}
			else
			{
				yIntersect = None;
			}
		}

		if( (xIntersect != None) && (yIntersect != None) )
		{
			/*
			 * We are completely covered so give up now.
			 */

			if( (xIntersect == Covered) && (yIntersect == Covered) )
				return;

			/*
			 * We cover this one completely, so replace it
			 */

			if( (xIntersect == Complete) && (yIntersect == Complete) )
			{
				usedRects.remove(r);
				freeRects.add(r);
			}

			/*
			 * Else we must cut ourselves up and apply again!
			 * but being friendly looking for ones with same X/w or Y/h
			 */


		}

		r = nextR;
	}

	/*
	 * Find a free rectangle
	 */

	r = freeRects.get();
	if(r)
	{
	 	/*
		 * Copy rect into it
		 */

		r->rect = rect;

		/*
		 * Add to used list
		 */

		usedRects.add(r);
	}

#ifdef DEBUG
	checkRectangles();
#endif


}

#ifdef DEBUG

void Screen::checkRectangles()
{
	LinkedRect* r = usedRects.entry;
	LinkedRect* p = 0;
	int count = 0;

	while(r)
	{
		count++;

		if(r->prev != p)
			throw GeneralError("Linked Rectangle's prev is wrong");

		p = r;
		r = r->next;
	}

	r = freeRects.entry;
	p = 0;

	while(r)
	{
		count++;

		if(r->prev != p)
			throw GeneralError("Linked Rectangle's prev is wrong");

		p = r;
		r = r->next;
	}

	if(count != MaxUpdateRects)
		throw GeneralError("Some screen update rectangles have got lost!");

}

#endif

/*
 * Rectangle List maintenance
 */

LinkedRect* LinkedRectList::get()
{
	LinkedRect* r = entry;

	if(r)
	{
		entry = r->next;
		if(entry)
			entry->prev = 0;
 	}

	return r;
}

void LinkedRectList::add(LinkedRect* r)
{
	r->next = entry;
	r->prev = 0;
	if(r->next)
		r->next->prev = r;
	entry = r;
}

void LinkedRectList::remove(LinkedRect* r)
{
	if(r->prev)
		r->prev->next = r->next;
	else
		entry = r->next;

	if(r->next)
		r->next->prev = r->prev;
}







/*================================================================
 * Screen palette
 */

/*
 * Set Default Game palette at next screen update
 */

void Screen::setPalette(Palette* p, int rate)
{
	if(p)
		*deferredPalette = *p;
	paletteChanged = True;

	fadeRate = rate;
}

/*
 * Set part of the palette at the next screen update
 */

void Screen::updateColours(UBYTE start, UBYTE count, TrueColour* colours)
{
	deferredPalette->setColours(start, colours, count);
	fadeRate = 0;
	paletteChanged = True;
}

void Screen::updatePalette(int rate)
{
	paletteChanged = True;
	fadeRate = rate;
}

/*
 * Set default game palette NOW!
 * Allows interrupt routine to start fading 
 * or does instant set if faderate is 0.
 */

void Screen::setPaletteNow(Palette* p, int rate)
{
	if(p)
		*palette = *p;

	realFadeRate = rate;

	if(rate == 0)
	{
		paletteWrong = False;
		*realPalette = *palette;
		realPalette->set();
	}
	else
	{
		paletteWrong = True;
#if defined(NO_PALETTE_IRQ)
	while(paletteWrong)
		::updatePalette();
#endif
	}
}


/*
 * Instant fade
 */


void Screen::fadePalette(int rate)
{
	palette->setBlack();
	// memset(palette->address(), 0, palette->size());

	realFadeRate = rate;
	paletteWrong = True;

#if defined(NO_PALETTE_IRQ)
	while(paletteWrong)
		::updatePalette();
#endif
}

/*
 * Interrupt routine to fiddle with palette
 */


void updatePalette()
{
	if(theScreen)
	{
		if(theScreen->paletteWrong)
		{
			if(theScreen->realFadeRate <= 1)
			{
				*theScreen->realPalette = *theScreen->palette;
				theScreen->realPalette->set();
				theScreen->paletteWrong = False;
			}
			else
			{
				theScreen->paletteWrong = theScreen->realPalette->fadeTo(theScreen->palette, theScreen->realFadeRate--);
			}
		}
	}
}

#if !defined(NO_PALETTE_IRQ)

extern "C" {
void paletteIRQ()
{
	static volatile UBYTE lock = 0;	// Prevent reentrancy

	if(++lock == 1)
	{
		updatePalette();
	}
	--lock;
}
}

#endif
