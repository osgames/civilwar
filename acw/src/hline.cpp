/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Horizontal Line drawing.
 *
 * This used to be part of image.cpp, but was seperated to tidy
 * things up.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.7  1994/08/31  15:23:00  Steven_Green
 * Attempted a bit of optimisation on the Gourad textured Hline
 *
 * Revision 1.6  1994/08/19  17:29:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.4  1994/04/05  12:28:09  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/01/07  23:43:18  Steven_Green
 * Use Image::getWidth/Height instead of accessing fields directly.
 *
 * Revision 1.2  1993/12/16  22:19:35  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/12/15  20:56:36  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "image.h"
#include "terrain.h"

void Region::HLine(SDimension x, SDimension y, SDimension length, Colour col)
{
	if(length < 0)
	{
		x += length;
		length = SDimension(-length);
	}

	/*
	 * Clip it
	 *
	 * TBA: Use a clipping rectangle
	 */

	if( (y < 0) || (y >= getH()) )
		return;

	if(x < 0)
	{
		length += x;
		x = 0;
	}

	if( (x + length) >= getW())
	{
		length = (SDimension) (getW() - x);
	}

	if(length <= 0)
		return;

	memset(getAddress(Point(x, y)), col, length);
}

/*
 * Textured HLine
 *
 * Don't bother with clipping
 */

void Region::HLineFast(SDimension x, SDimension y, SDimension length, Colour col, const Image* texture)
{
	SDimension tx = SDimension(x % texture->getWidth());
	SDimension ty = SDimension(y % texture->getHeight());
	UBYTE* tline = texture->getAddress(0, ty);
	UBYTE* tad = tline + tx;
	UBYTE* sad = getAddress(Point(x, y));

	tx = SDimension(texture->getWidth() - tx);

	while(length--)
	{
		*sad++ = Colour(*tad++ + col);

		if(!--tx)
		{
			tad = tline;
			tx = texture->getWidth();
		}
	}
}

/*
 * Draw from min to max changing colour smoothly
 */

void Region::HLineFast(SDimension x, SDimension y, SDimension length, TerrainCol* col, Intensity i1, Intensity i2)
{

	if(length < 0)
		throw GeneralError("Divide by zero in HLine");

	/*
	 * Normalise intensities to given range
	 */

	i1 -= col->minVol;
	i2 -= col->minVol;

	UBYTE* ad = getAddress(Point(x, y));

	int iRange = col->maxVol - col->minVol;

	Colour c1;
	Colour c2;

	if(iRange == 0)
	{
		c1 = 0;
		c2 = 0;
	}
	else
	{
		c1 = Colour((i1 * col->colourRange) / iRange);
		c2 = Colour((i2 * col->colourRange) / iRange);
	}

	Colour c = Colour(c1 + col->baseColour);

	if(c1 == c2)
	{
		memset(ad, c, length);
		return;
	}
	

	int ir = i1 - (c1 * iRange) / col->colourRange;		// intensity remainder

	Colour dCol;
	Intensity dI;
	int change;

	if(c2 > c1)		// Increase in colour
	{
		dCol = Colour(c2 - c1);
		dI = Intensity(i2 - i1);
		change = 1;
	}
	else
	{
		dCol = Colour(c1 - c2);
		dI = Intensity(i1 - i2);
		change = -1;
	}


	if(dCol > length)		// Colour change is major axis
	{
		int top = dI * col->colourRange;
		int bottom = iRange * length;

		int val = top / 2;

		while(length)
		{
			val += bottom;

			if(val >= top)
			{
				val -= top;

				*ad++ = c;
				length--;
			}

			c += change;
		}
	}
	else						// Length is major axis
	{
		int top = dI * col->colourRange;
		int bottom = iRange * length;

		int val;
		
		if(c2 > c1)
			val = ir * length * col->colourRange;
		else
			val = bottom - ir * length * col->colourRange;

		while(length--)
		{
			val += top;

			if(val >= bottom)
			{
				val -= bottom;
				c += change;
			}

			*ad++ = c;
		}
	}
}

/*
 * Draw from min to max changing colour smoothly with texture
 */

void Region::HLineFast(SDimension x, SDimension y, SDimension length, TerrainCol* col, Intensity i1, Intensity i2, const Image* texture)
{

	if(length < 0)
		throw GeneralError("Divide by zero in HLine");

	/*
	 * Initialise texture variables
	 */

	SDimension tw = texture->getWidth();
	UBYTE tMask = tw - 1;		// Assume texture is ^2 wide

	SDimension tx = x & tMask;
	SDimension ty = SDimension(y % texture->getHeight());
	UBYTE* tline = texture->getAddress(0, ty);
	// UBYTE* tad = tline + tx;
	// tx = SDimension(texture->getWidth() - tx);

	/*
	 * Normalise intensities to given range
	 */

	i1 -= col->minVol;
	i2 -= col->minVol;

	int iRange = col->maxVol - col->minVol;
	int cRange = col->colourRange - 3;		// Allow 3 colours for texture

	UBYTE* ad = getAddress(Point(x, y));

	Colour c1;
	Colour c2;
 
	if(iRange == 0)
	{
		c1 = 0;
		c2 = 0;
	}
	else if(cRange == 13)		// 13 is most common configuration!
	{
		c1 = Colour((i1 * 13) / iRange);	// Let compiler optimise this to shifts
		c2 = Colour((i2 * 13) / iRange);
	}
	else
	{
		c1 = Colour((i1 * cRange) / iRange);
		c2 = Colour((i2 * cRange) / iRange);
	}

	Colour c = Colour(c1 + col->baseColour);

	if(c1 == c2)
	{
		while(length--)
		{
			*ad++ = c + tline[tx];
			tx = (tx + 1) & tMask;
		}
		return;
	}
	
	int ir = i1 - (c1 * iRange) / cRange;		// intensity remainder

	Colour dCol;
	Intensity dI;
	int change;

	if(c2 > c1)		// Increase in colour
	{
		dCol = Colour(c2 - c1);
		dI = Intensity(i2 - i1);
		change = 1;
	}
	else
	{
		dCol = Colour(c1 - c2);
		dI = Intensity(i1 - i2);
		change = -1;
	}


	if(dCol > length)		// Colour change is major axis
	{
		int top = dI * cRange;
		int bottom = iRange * length;

		int val = top / 2;

		while(length)
		{
			val += bottom;

			if(val >= top)
			{
				val -= top;

				*ad++ = c + tline[tx];
				tx = (tx + 1) & tMask;
				length--;
			}

			c += change;
		}
	}
	else						// Length is major axis
	{
		int top = dI * cRange;
		int bottom = iRange * length;

		int val;
		
		if(c2 > c1)
			val = ir * length * cRange;
		else
			val = bottom - ir * length * cRange;

		while(length--)
		{
			val += top;

			if(val >= bottom)
			{
				val -= bottom;
				c += change;
			}

			*ad++ = c + tline[tx];
			tx = (tx + 1) & tMask;

		}
	}
}
