/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	File Selector Routines
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdlib.h>
#include <stdio.h>
#include <dos.h>
#include <ctype.h>

#include "filesel.h"
#include "dialogue.h"
#include "texticon.h"
#include "scroll.h"
#include "menudata.h"
#include "system.h"
#include "screen.h"
#include "mouselib.h"
#include "colours.h"
#include "text.h"
#include "cd_open.h"
#include "language.h"
#include "memptr.h"
#include "strutil.h"

#define OuterCol 		BlueGrey4
#define OuterB1		BlueGrey0
#define OuterB2		BlueGrey6
#define FileAreaCol	BlueGrey7

#define FONT_Button		Font_EMMA14
#define FONT_Title		Font_EMMA14
#define FONT_Input		Font_EMMA14
#define Font_FileName	Font_EMMA14

/*
 * Class for internal working of file selector
 */

class FSelDialogue : public MenuData {
	Image under;
	Point where;
public:
	FSelDialogue(MenuData* function, const Point& p, char* path, const char* title, Boolean exist);
	~FSelDialogue();

	char fullpath	[_MAX_PATH];
	char drive		[_MAX_DRIVE];
	char dir			[_MAX_DIR + _MAX_DRIVE];
	char fname		[_MAX_FNAME];
	char ext			[_MAX_EXT];

	int top;
	int nFiles;
	int lines;

	char** names;		// Array of nFiles

private:
	void makeFileNames(const char* path);
};

class FileScroll : public VerticalScrollBar {
	FSelDialogue* control;
public:
	FileScroll(IconSet* parent, FSelDialogue* ctrl, const Rect& r) :
		VerticalScrollBar(parent, r)
	{
		control = ctrl;
	}

	void clickAdjust(int v);
	void getSliderSize(SDimension& from, SDimension& to, SDimension size);
};

class FileArea : public Icon {
	FSelDialogue* control;
public:
	FileArea(IconSet* parent, FSelDialogue* ctrl, const Rect& r) :
		Icon(parent, r)
	{
		control = ctrl;
	}

	void execute(Event* event, MenuData* data);
	void drawIcon();
};

/*
 * Icon to input a file name
 *
 * processKey overides virtual function to check for valid keys
 */

class FileNameIcon : public InputIcon {
public:
	FileNameIcon(IconSet* set, const Rect& r, char* buf, size_t size, FontID f) :
		InputIcon(set, r, buf, size, f)
	{
	}

	Key processKey(Key k);
};

Key FileNameIcon::processKey(Key k)
{
	if(isalnum(k) || (k == '_'))
		return toupper(k);
	else
		return 0;
}



/*
 * Constructor for file selector dialogue
 */

FSelDialogue::FSelDialogue(MenuData* function, const Point& p, char* path, const char* title, Boolean exist) :
	MenuData(function)
{
	const SDimension w = 272 + 24;
	const SDimension h = 186;

	SDimension x = p.x - w/2;
	SDimension y = p.y - h/2;

	if(x < 0)
		x = 0;
	if( (x + w) > getW())
		x = getW() - w;

	if(y < 0)
		y = 0;
	if( (y + h) > getH())
		y = getH() - h;

	Rect outerRect = Rect(x, y, w, h);

	where = Point(x,y);
	under.resize(w, h);
	machine.unBlit(&under, where);

	/*
	 * Sort out the filename
	 */

	char buffer[_MAX_PATH];

	// _fullpath(buffer, path, _MAX_PATH);
	// _splitpath(buffer, drive, dir, fname, ext);
	_splitpath(path, drive, dir, fname, ext);


	fname[0] = 0;

	/*
	 * Set up filenames
	 */

	lines = 112 / fontSet(Font_FileName)->getHeight();
	top = 0;
	nFiles = 0;
	names = 0;
	makeFileNames(path);


	/*
	 * Create outer box
	 */

	icons = new IconList[2];

	// IconSet* outer = new OuterBox(this, outerRect, OuterCol, OuterB1, OuterB2);
	IconSet* outer = new OuterBox(this, outerRect, Fill_FileSel, OuterB1, OuterB2);
	icons[0] = outer;
	icons[1] = 0;

	/*
	 * Create inner icons
	 */

	IconList* innerIcons = new IconList[7];
	Icon** iconPtr = innerIcons;

	/*
	 * OK
	 */

	*iconPtr++ = new ButtonIcon(outer, Rect(200, 44, 88, 20), FONT_Button, 0, language(lge_OK), KEY_Enter, False);
#if defined(COMPUSA_DEMO)
	*iconPtr++ = new ButtonIcon(outer, Rect(200, 72, 88, 20), FONT_Button, -1, language(lge_cancel), KEY_Space, False);
#else
	*iconPtr++ = new ButtonIcon(outer, Rect(200, 72, 88, 20), FONT_Button, -1, language(lge_cancel), KEY_Escape, False);
#endif
	// *iconPtr++ = new TextBox(outer, Rect(8,8,256,32), title, FONT_Title, Black, OuterB2, OuterB1, White);
	*iconPtr++ = new TextBox(outer, Rect(8,8,256,32), title, FONT_Title, Fill_Paper, OuterB2, OuterB1, Black);

	if(exist)
		// *iconPtr++ = new TextBox(outer, Rect(8,44,168,16), fname, FONT_Input, OuterCol, OuterCol, OuterCol, Black);
		*iconPtr++ = new TextBox(outer, Rect(8,44,168,16), fname, FONT_Input, OuterCol, OuterB2, OuterB1, Black);
	else
		*iconPtr++ = new FileNameIcon(outer, Rect(8, 44, 168, 16), fname, sizeof(fname), FONT_Input);

	*iconPtr++ = new FileScroll(outer, this, Rect(176, 64, 16, 114));

	*iconPtr++ = new FileArea(outer, this, Rect(8, 64, 168, 114));

	*iconPtr = 0;

	outer->setIcons(innerIcons);
}

FSelDialogue::~FSelDialogue()
{
	machine.blit(&under, where);
	machine.screen->update();

	if(names)
	{
		int count = nFiles;
		char** ptr = names;

		while(count--)
			free(*ptr++);

		delete[] names;
	}
}


void FileScroll::clickAdjust(int v)
{
	int newTop = control->top;

	if(v < 0)
	{
		if(v == -1)
			newTop++;
		else
			newTop += control->lines;

		if(newTop > (control->nFiles - control->lines))
			newTop = control->nFiles - control->lines;
		if(newTop < 0)
			newTop = 0;

#if 0
		// if(control->top < (control->nFiles - control->lines))
		if(newTop !- control->top)
		{
			// control->top++;
			control->top = newTop;
			control->setRedraw();
		}
#endif
	}
	else if(v > 0)
	{
		if(v == 1)
			newTop--;
		else
			newTop -= control->lines;
		if(newTop < 0)
			newTop = 0;

#if 0
		if(control->top > 0)
		{
			control->top--;
			control->setRedraw();
		}
#endif
	}

	if(newTop != control->top)
	{
		// control->top++;
		control->top = newTop;
		control->setRedraw();
	}
}

void FileScroll::getSliderSize(SDimension& from, SDimension& to, SDimension size)
{
	if(control->nFiles <= control->lines)
	{
		from = 0;
		to = from + size;
	}
	else
	{
		int newSize = (control->lines * size) / control->nFiles;

		from = (control->top * size) / control->nFiles;
		to = from + newSize;
	}
}


/*
 * File Area
 */

void FileArea::execute(Event* event, MenuData* data)
{
	if(event->buttons)
	{
		int height = fontSet(Font_FileName)->getHeight();

		int item = (event->where.getY() - 2) / height + control->top;

		if(item < control->nFiles)
		{
			strcpy(control->fname, control->names[item]);
			control->setRedraw();
		}
	}
}

void FileArea::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	bm.fill(FileAreaCol);
	TextWindow window(&bm, Font_FileName);

	bm.frame(Point(0,0), getSize(), OuterB1, OuterB2);

	if(control->nFiles)
	{
		int y = 2;

		int count = control->nFiles - control->top;
		if(count > control->lines)
			count = control->lines;

		int fileNo = control->top;

		while(count--)
		{
			window.setPosition(Point(2, y));

			window.draw(control->names[fileNo++]);

			y += fontSet(Font_FileName)->getHeight();
		}
	}
	else
	{
		window.setFormat(True, True);
		window.draw(language(lge_NoFiles));
	}

	machine.screen->setUpdate(bm);
}

int fileCompare(const void* s1, const void* s2)
{
	const char* str1 = *(char**) s1;
	const char* str2 = *(char**) s2;

	return strcmp(str1, str2);
}

void FSelDialogue::makeFileNames(const char* path)
{
	struct find_t fileInfo;

	struct FileLink {
		FileLink* next;
		char* name;
	}* links = 0;
		

	/*
	 * Pass 1 counts the files
	 * and makes a list of names
	 */

	int count = 0;

	if(!_dos_findfirst(path, _A_NORMAL, &fileInfo))
	{
		do
		{
			char* s = strchr(fileInfo.name, '.');
			if(s)
				*s = 0;

			FileLink* lnk = new FileLink;
			lnk->next = links;
			lnk->name = strdup(fileInfo.name);
			links = lnk;

			count++;
		} while(!_dos_findnext(&fileInfo));
	}

	if(!_dos_findfirst( addPath( path ), _A_NORMAL, &fileInfo))
	{
		do
		{
			char* s = strchr(fileInfo.name, '.');
			if(s)
				*s = 0;

			FileLink* lnk = new FileLink;
			lnk->next = links;
			lnk->name = strdup(fileInfo.name);
			links = lnk;

			count++;
		} while(!_dos_findnext(&fileInfo));
	}

	/*
	 * Phase 2 makes an array of the names
	 * and sorts them
	 */

	if(count)
	{
		names = new char*[count];
		nFiles = count;

		char** ptr = names;
		FileLink* l = links;
		while(l)
		{
			FileLink* next = l->next;
			*ptr++ = l->name;
			delete l;
			l = next;
		}

		qsort(names, nFiles, sizeof(char*), fileCompare);
	}

	/*
	 * Phase 3 removes duplicate items
	 */

	if(count > 1)
	{
		char** srcPtr = names + 1;
		char** destPtr = names;

		nFiles = 1;
		int i = count - 1;
		while(i--)
		{
			if(strcmp(*srcPtr, *destPtr))
			{
				*++destPtr = *srcPtr;
				nFiles++;
			}
			srcPtr++;
		}
	}

}

/*
 * Entry point to selector
 *
 * srcPath should be the full path name including wildcards of suitable
 * files to select
 *
 * If existing is True, then the user may only select an existing file
 * otherwise they may type in a name.
 *
 * Prompt is a string displayed to the player
 *
 * Returns 0 if user clicked OK, or -1 if cancelled.
 * The srcPath will contain a complete filename if return is 0.  If the
 * user cancelled then the contents of srcPath will be unaltered.
 *
 * e.g.
 *
 * char pathName[FILENAME_MAX];
 * strcpy(pathName, "c:\data\*.dat");
 * if(fileSelect(pathName, "Load a data file", True) == 0)
 *		loadFile(pathName);
 *
 */

int fileSelect(char* srcPath, const char* prompt, Boolean existing)
{
	char* newPrompt = copyString(prompt);	// Bodge because prompt can get overwritten!
	FSelDialogue dial(0, machine.mouse->getPosition(), srcPath, newPrompt, existing);

	MemPtr<char> fileName(FILENAME_MAX);

	strcpy ( fileName, srcPath );

	while(!dial.isFinished())
		dial.process();

	delete[] newPrompt;

	int result = dial.getMode();

	if(result == 0)
	{
		if(dial.fname[0])
		{
#if 0
			sprintf(srcPath, "%s%s%s%s", dial.drive, dial.dir, dial.fname, dial.ext);

			if ( !DoesFileExist( srcPath ) )
			{
				sprintf(srcPath, "%s%s%s", addPath(dial.dir), dial.fname, dial.ext);
				if(!DoesFileExist(srcPath))
					sprintf(srcPath, "%s%s%s%s", dial.drive, dial.dir, dial.fname, dial.ext);
	
			}
#else	// Only return base filename.
			sprintf(srcPath, "%s%s%s", dial.dir, dial.fname, dial.ext);
#endif
		}
		else
			result = -1;
	}

	return result;
}
