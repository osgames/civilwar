/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Mobilisation and facility building
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.4  1994/07/28  18:57:04  Steven_Green
 * Creation of new items after mobilisation period implemented
 *
 * Revision 1.2  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.1  1994/05/25  23:31:25  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include "mobilise.h"
#include "resource.h"
#include "campaign.h"
#include "unitcamp.h"
#include "campwld.h"
#include "camptab.h"
#include "city.h"
#include "error.h"
#include "game.h"
#include "language.h"
#include "memptr.h"
#include "dialogue.h"
#ifdef DEBUG
#include "options.h"
#include "patchlog.h"
#endif

Mobilising::Mobilising()
{
	next = 0;
}

#ifndef CAMPEDIT

Mobilising::Mobilising(MobiliseType type, UBYTE days, UBYTE count, Facility* city)
{
	what = type;
	when = days;
	howMany = count;
	where = city;
}

/*
 * Process Mobilising things each day
 */

void MobiliseList::process()
{
	Mobilising* m = entry;
	Mobilising* last = 0;

	MemPtr<char> buffer(900);

	buffer[0] = 0;
	int count = 0;
	Facility* lastCity = 0;

	while(m)
	{
#ifdef DEBUG
		if(instantBuild || (--m->when == 0))
#else
		if(--m->when == 0)
#endif
		{
			/*
			 * Precalculate some useful values
			 */

			Side side = m->where->side;

#ifdef DEBUG
			cLog.printf("%d %s completed at %s",
				(int) m->howMany,
				language(buildCosts[m->what].description),
				m->where->getNameNotNull());
#endif

			/*
			 * Process it and delete it from list
			 */

#if defined(DEBUG)
			if(!noBuildMessages)
#else
			if(game->isPlayer(side))
#endif
			{
				/*
				 * If it's a different city, then display previous info
				 */

				
				if( (lastCity && (m->where != lastCity)) || (count > 8))
				{
					if(campaign)
						campaign->doAlert(buffer, language(lge_OK), 0, lastCity, 0, 0);
					else
						dialAlert(0, buffer, language(lge_OK));
					buffer[0] = 0;
					count = 0;
				}

				sprintf(buffer + strlen(buffer),
					language(lge_builtIn),
					language(buildCosts[m->what].description),
					m->where->getNameNotNull());

				lastCity = m->where;

				count++;
			}

			/*
			 * Do the actual building!!!
			 */

			int buildCount = m->howMany;

			switch(m->what)
			{
			case Mob_InfantryRegular:
			case Mob_InfantryMilitia:
			case Mob_InfantrySharpShooter:
			case Mob_Engineer:
				while(buildCount--)
					makeNewRegiment((City*)m->where, Infantry, m->what - Mob_InfantryRegular);
				break;

			case Mob_RailEngineer:
				while(buildCount--)
					m->where->facilities |= Facility::F_RailEngineer;
				break;

			case Mob_cavalryRegular:
			case Mob_cavalryMilitia:
			case Mob_cavalryRegularMounted:
			case Mob_cavalryMilitiaMounted:
				while(buildCount--)
					makeNewRegiment((City*)m->where, Cavalry, m->what - Mob_cavalryRegular);
				break;

			case Mob_ArtillerySmoothbore:
			case Mob_ArtilleryLight:
			case Mob_ArtilleryRifled:
			case Mob_ArtillerySiege:
				while(buildCount--)
					makeNewRegiment((City*)m->where, Artillery, m->what - Mob_ArtillerySmoothbore);
				break;

			/*
	 		 * Naval
	 		 */

			case Mob_Naval:
			case Mob_NavalIronClad:
			case Mob_Riverine:
			case Mob_RiverineIronClad:
				{
					WaterZone* zone = &campaign->world->waterNet.zones[m->where->waterZone];
					if(zone)
					{
						Flotilla boats;	// constructor clears it

						boats.count[m->what - Mob_Naval] += buildCount;
						Fleet* newFleet = new Fleet(boats, m->where->side);
						campaign->world->waterNet.sendFleet(newFleet, m->where->waterZone, m->where->waterZone, campaign->gameTime);
						// zone->boats.count[m->what - Mob_Naval] += buildCount;
					}
#ifdef DEBUG
					else
						throw GeneralError("Attempt to build naval unit in facility with no waterzone!");
#endif

				}
				break;

			case Mob_Blockade:
				if( (m->where->blockadeRunners + buildCount) < MaxBlockade)
					m->where->blockadeRunners += buildCount;
				else
				{
					m->where->blockadeRunners = MaxBlockade;
#ifdef DEBUG
					patchLog.printf("Patch 2.0.3, prevented blockade being over %d", MaxBlockade);
#endif
				}
				break;

			/*
	 		 * Supplies
	 		 */

			case Mob_SupplyWagon:
				{
					Supply& sPtr = m->where->supplies;
					int newSupply = sPtr + 100;
					if(newSupply > MaxSupply)
						newSupply = MaxSupply;
					sPtr = newSupply;
				}
				break;

			/*
	 		 * Facilities
	 		 */

			case Mob_Fortification:
				if(m->where->fortification < MaxFortification)
					m->where->fortification++;
				else
				{
					m->where->fortification = MaxFortification;
#ifdef PATCHLOG
					patchLog.printf("Patch 2.0.3, prevented fortifications being over level %d", (int) MaxFortification);
#endif
				}

				ASSERT(m->where->fortification <= MaxFortification);
				break;

			case Mob_SupplyDepot:
				m->where->facilities |= Facility::F_SupplyDepot;
				break;

			case Mob_Recruitment:
				m->where->facilities |= Facility::F_RecruitmentCentre;
				break;

			case Mob_Training:
				m->where->facilities |= Facility::F_TrainingCamp;
				break;

			case Mob_RailHead:
				if(m->where->railCapacity < MaxRailHead)
				{
					m->where->railCapacity++;
					m->where->freeRailCapacity++;
				}
				else
				{
					m->where->freeRailCapacity = MaxRailHead - m->where->freeRailCapacity;
					m->where->railCapacity = MaxRailHead;
#ifdef DEBUG
					patchLog.printf("Patch 2.0.3, prevented railHead capacity being over %d", (int) MaxRailHead);
#endif
				}
				break;

			case Mob_Hospital:
				m->where->facilities |= Facility::F_Hospital;
				break;

			case Mob_POW:
				m->where->facilities |= Facility::F_POW;
				break;

			default:
				{
#ifdef DEBUG
					throw GeneralError("Don't know how to build item %d at %s\n",
						(int)m->what,
						m->where->getNameNotNull());
#endif
					continue;
				}
			}

			Mobilising* m1 = m;
			m = m->next;

			if(last)
				last->next = m;
			else
				entry = m;

			delete m1;
		}
		else
		{
#ifdef DEBUG
			cLog.printf("%d %s being built at %s, %d days to go",
				(int) m->howMany,
				language(buildCosts[m->what].description),
				m->where->getNameNotNull(),
				(int)m->when);
#endif

			last = m;
			m = m->next;
		}
	}

	/*
	 * Is there anything in the buffer to print?
	 */

	if(count) 
	{
		if(campaign)
			campaign->doAlert(buffer, language(lge_OK), 0, lastCity, 0, 0);
		else
			dialAlert(0, buffer, language(lge_OK));
	}
}


/*
 * List functions
 */

void MobiliseList::clear()
{
	while(entry)
	{
		Mobilising* m = entry;
		entry = entry->next;
		delete m;
	}
}

#endif	// CAMPEDIT

MobiliseList::~MobiliseList()
{
#ifndef CAMPEDIT
	clear();
#endif
}

#ifndef CAMPEDIT

/*
 * Let user mobilise something
 *
 * Display a dialogue detailing:
 *		What they are building
 *		How much it costs
 *		How long it will take to build
 * Allow them to cancel or Build.
 *
 * If the quiet flag is set then no dialogue boxes are given
 *
 * return Number of items built (0 if cancelled)
 */

int MobiliseList::askHowMany(MobiliseType type, Facility* city, MenuData* control)
{
	/*
	 * Check that city can build it!
	 */

	ResourceSet* imported = city->getGlobalResources();
	ResourceSet* local = city->getLocalResources();
	MobiliseCost* cost = &buildCosts[type];
	UBYTE howLong = cost->getBuildTime(city);

	int howMany = cost->cost.canAfford(local, imported);

	if(howMany)
	{
		if(!cost->canBuildMulti)
			howMany = 1;

		MemPtr<char> buffer(200);
		MemPtr<char> buttons(100);

		int buttonCount = 0;
		int buildCount = howMany;

		buttons[0] = 0;

		static buildMultiples[] = {
			1, 2, 5, 10, 20, 50, 0
		};

		while(buildMultiples[buttonCount] && (buildMultiples[buttonCount] <= buildCount))
		{
			sprintf(buttons + strlen(buttons), language( lge_build ), buildMultiples[buttonCount]);
			buttonCount++;
		}

		strcat(buttons, language( lge_cancel ) );

		sprintf(buffer, 
			language(lge_Build_TimeToBuild),
			language(cost->description),
			howLong);

		int result = dialAlert(control, buffer, buttons);
		
		if((result >= 0) && (result < buttonCount))		// Cancel
			howMany = buildMultiples[result];
		else
			return 0;

		return howMany;
	}
	else
	{
		if(control)
			dialAlert(control, language( lge_cantAfford ), language(lge_OK));
		return 0;
	}
}

int MobiliseList::startBuild(MobiliseType type, Facility* city, int howMany)
{
	/*
	 * Check that city can build it!
	 */

	ResourceSet* imported = city->getGlobalResources();
	ResourceSet* local = city->getLocalResources();
	MobiliseCost* cost = &buildCosts[type];
	UBYTE howLong = cost->getBuildTime(city);

	cost->cost.takeCost(local, imported, howMany);

	Mobilising* m = new Mobilising(type, howLong, howMany, city);

	if(m)
	{
		m->next = entry;
		entry = m;
	}
#ifdef DEBUG
	else
		throw GeneralError("Couldn't allocate space for Mobilising thing");
#endif

	return howMany;
}


#if 0
/*
 * Iterator functions
 */

/*
#if 0
 * Return percentage built
 *		0 = not building
 *	 	1 = just started to build
 *  255 = fully built
#else
 *
 * Return number of days to build!
 *
#endif
 */

UBYTE MobiliseIter::whenMobilising(const Facility* where, MobiliseType what)
{
	Mobilising* r;

	do {
		r = nextItem;

		if(r)
			nextItem = r->next;
		else
			return 0;

	} while((r->where != where) || (r->what != what));

	return r->when;

	// UBYTE t = buildCosts[what].getBuildTime(where);

#if 0
	if(r->when > t)
		return 0;

	return UBYTE(255 - ((r->when * 254) / t));
#else
	// return t;
#endif
}

#endif

/*
 * Return least number of days left to built item at facility.
 */

UBYTE MobiliseList::whenMobilising(const Facility* where, MobiliseType what) const
{
	Mobilising* r;

	UBYTE shortest = 0;
	Boolean noneFound = True;

	r = entry;
	while(r)
	{
		if((r->where == where) && (r->what == what))
		{
			if(noneFound || (r->when < shortest))
			{
				noneFound = False;
				shortest = r->when;
			}
		}

		r = r->next;
	}

	return shortest;
}

UBYTE MobiliseList::totalMobilising(const Facility* where, MobiliseType what) const
{
	return buildCosts[what].getBuildTime(where);
}

/*
 * Return how many items of a particular type are being built by a side
 */

int MobiliseList::howManyMobilising(Side side, MobiliseType what) const
{
	int count = 0;

	Mobilising* r = entry;
	while(r)
	{
		if((r->where->side == side) && (r->what == what))
			count++;

		r = r->next;
	}

	return count;
}

int MobiliseList::MobilisingPerFacility( const Facility *f, MobiliseType what) const
{
	int count = 0;

	Mobilising* r = entry;
	while(r)
	{
		if((r->where == f) && (r->what == what))
			count++;

		r = r->next;
	}

	return count;
}

/*
 * Return The Number of items that can be afforded at city
 * 0 if none.
 */

int canAffordToMobilise(MobiliseType type, Facility* city)
{
	MobiliseCost* cost = &buildCosts[type];
 
	const ResourceSet* imported = city->getGlobalResources();

	return cost->cost.canAfford(city->getLocalResources(), imported);
}

Boolean MobiliseList::canItemExist(MobiliseType what) const
{
	return ((game->getDifficulty(Diff_RegimentTypes) >= buildCosts[what].regRealism) &&
			  (game->getDifficulty(Diff_Facilities)    >= buildCosts[what].facRealism) &&
			  (game->getDifficulty(Diff_Supply)        >= buildCosts[what].supRealism) );
}

Boolean MobiliseList::canEverBuild(const Facility* f, MobiliseType what) const
{
 	if(!canItemExist(what))
		return False;

	if(buildCosts[what].inCity && !(f->facilities & Facility::F_City))
		return False;

	UBYTE buildDays = whenMobilising(f, what);

	if(buildCosts[what].onlyOne && buildDays)
		return False;

	switch(what)
	{
		case Mob_InfantryRegular:
		case Mob_cavalryRegular:
		case Mob_ArtillerySmoothbore:
		case Mob_InfantryMilitia:
		case Mob_cavalryMilitia:
		case Mob_ArtilleryLight:
		case Mob_InfantrySharpShooter:
		case Mob_cavalryRegularMounted:
		case Mob_ArtilleryRifled:
		case Mob_Engineer:
		case Mob_cavalryMilitiaMounted:
		case Mob_ArtillerySiege:
		 	return True;

		/*
 		 * Naval
 		 */

		case Mob_Naval:
	 		return f->facilities & Facility::F_Port;

		case Mob_Riverine:
			return f->facilities & (Facility::F_Port | Facility::F_LandingStage);

		case Mob_RiverineIronClad:
			return f->facilities & (Facility::F_Port | Facility::F_LandingStage);

		case Mob_NavalIronClad:
			return f->facilities & Facility::F_Port;

		case Mob_Blockade:
			return ( (f->side == SIDE_CSA) &&
						(f->facilities & (Facility::F_Port | Facility::F_LandingStage)));
				
		/*
 		 * Supplies
 		 */

		case Mob_SupplyWagon:
			return (f->facilities & Facility::F_SupplyDepot);

		case Mob_SupplyDepot:
			return !(f->facilities & Facility::F_SupplyDepot);

		/*
 		 * Facilities
 		 */

		case Mob_RailEngineer:
			return ( (f->side == SIDE_USA) &&
						f->railCount && 
						!(f->facilities & Facility::F_RailEngineer));

		case Mob_Fortification:
			return (f->fortification < MaxFortification);

		case Mob_Recruitment:
			return !(f->facilities & Facility::F_RecruitmentCentre);

		case Mob_RailHead:
			return f->railCount && (f->railCapacity < MaxRailHead);

		case Mob_Training:
			return !(f->facilities & Facility::F_TrainingCamp);

		case Mob_Hospital:
			return !(f->facilities & Facility::F_Hospital );

		case Mob_POW:
			return !(f->facilities & Facility::F_POW );
#ifdef DEBUG
		default:
			throw GeneralError("Unknown MobiliseType %d in " __FILE__ ", line %d", (int) what, __LINE__);
#endif

	}
	return False;
}

/*
 * Find out if something can be built
 *
 * This depends on being able to afford it
 * Whether the city already has certain facilties, etc
 * 
 *	It also takes into account the Facility realism level
 */

Boolean MobiliseList::canIbuild(Facility* f, MobiliseType what) const
{
	if(!canEverBuild(f, what))
		return False;

	return canAffordToMobilise(what, f);
}

/*
 * Remove all mobilising things at a city
 *
 * Usually called when a city has been taken over by enemy
 */

void MobiliseList::removeAll(Facility* where)
{
#ifdef DEBUG
	cLog.printf("Checking %s for half built items",
			where->getNameNotNull());
#endif

	Mobilising* r = entry;
	Mobilising* last = 0;
	while(r)
	{
		Mobilising* next = r->next;

		if(r->where == where)
		{
#ifdef DEBUG
			cLog.printf("Removing half built %s from %s",
					language(buildCosts[r->what].description),
					r->where->getNameNotNull());
#endif
			if(last)
				last->next = next;
			else
				entry = next;
			delete r;
		}
		else
			last = r;

		r = next;
	}
}


UBYTE MobiliseCost::getBuildTime(const Facility* f) const
{
	UBYTE t = days;

	if(reduceIfTraining)
	{
		UBYTE percent = 100;

		if(f->facilities & Facility::F_TrainingCamp)
			percent -= 30;
		if(f->facilities & Facility::F_RecruitmentCentre)
			percent -= 20;

		t = (t * percent) / 100;

	}

	return t;
}

#endif	// CAMPEDIT
