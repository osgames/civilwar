/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Serial Communications and Modem Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#if !defined(COMPUSA_DEMO)

#ifdef DEBUG
#define FLASH_BORDER	// Flash border to see if interrupts working
#endif

#include <i86.h>
#include <conio.h>
#include <stdio.h>
#include <stdarg.h>

#include "serial.h"
#include "ser_cfg.h"
#include "memptr.h"
#include "dialogue.h"
#include "timer.h"
#include "system.h"
#include "mouselib.h"
#include "language.h"
#ifdef DEBUG
#include "game.h"
#endif


/* ******************************************************************** *
 *
 *  UART Register Definitions
 *
 * ******************************************************************** */

                                            // UART regs (base address +)
#define RBR         0                       // receive buffer register
#define THR         0                       // transmit holding register
#define DLL         0                       // divisor latch LSB
#define DLM         1                       // divisor latch MSB
#define IER         1                       // interrupt enable register
#define IIR         2                       // interrupt id register
#define FCR         2                       // FIFO control register
#define AFR         2                       // alternate function register
#define LCR         3                       // line control register
#define MCR         4                       // modem control register
#define LSR         5                       // line status register
#define MSR         6                       // modem status register
#define SCR         7                       // scratch register

                                            // interrupt enable register
#define IER_RBF     0x01                    //  receive buffer full
#define IER_TBE     0x02                    //  transmit buffer empty
#define IER_LSI     0x04                    //  line status interrupt
#define IER_MSI     0x08                    //  modem status interrupt
#define IER_ALL     0x0f                    //  enable all interrupts

                                            // interrupt id register
#define IIR_PEND    0x01                    //  interrupt pending = 0
#define IIR_II      0x06                    //  interrupt id bits
                                            //   000 = modem status change
                                            //   001 = trans holding empty
                                            //   010 = receive buffer full
                                            //   110 = receive fifo full
                                            //   011 = line status change
#define IIR_MSI     0x00                    //  modem status interrupt
#define IIR_TBE     0x02                    //  transmit buffer empty
#define IIR_RBF     0x04                    //  receive buffer full
#define IIR_LSI     0x06                    //  line status interrupt
#define IIR_RFF     0x0c                    //  receive fifo threshold

                                            // fifo control register
#define FCR_FIFO    0x01                    //  fifo enable
#define FCR_RCVR    0x02                    //  receiver fifo reset
#define FCR_XMIT    0x04                    //  transmit fifo reset
#define FCR_DMA     0x08                    //  DMA mode select
#define FCR_TRIGGER 0xc0                    //  receiver trigger select
                                            //   00 = 1 byte
                                            //   01 = 4 bytes
                                            //   10 = 8 bytes
                                            //   11 = 14 bytes
#define FCR_16550   0xc7                    //  16550 fifo enable/reset

                                            // line control register
#define LCR_WLEN    0x03                    //  word length
                                            //   10 = 7 bits
                                            //   11 = 8 bits
#define LCR_STOP    0x04                    //  stop bits
                                            //   0 = 1 stop bit
                                            //   1 = 2 stop bits
#define LCR_PARITY  0x08                    //  parity enable
                                            //   0 = no parity
                                            //   1 = send/check parity
#define LCR_EVEN    0x10                    //  even/odd parity
                                            //   0 = odd parity
                                            //   1 = even parity
#define LCR_BREAK   0x40                    //  break, set to xmit break
#define LCR_DLAB    0x80                    //  divisor latch access bit

                                            // modem control register
#define MCR_DTR     0x01                    //  DTR control
#define MCR_RTS     0x02                    //  RTS control
#define MCR_OUT1	  0x04
#define MCR_OUT2    0x08                    //  OUT2 control
#define MCR_LOOP	  0x10
#define MCR_DO      0x0b                    //  dtr, rts & out2 enabled

                                            // line status register
#define LSR_DR      0x01                    //  data ready
#define LSR_ORUN    0x02                    //  overrun error
#define LSR_PRTY    0x04                    //  parity error
#define LSR_FRM     0x08                    //  framing error
#define LSR_BRK     0x10                    //  break interrupt
#define LSR_THRE    0x20                    //  transmit holding reg empty
#define LSR_TSRE    0x40                    //  transmit shift reg empty
#define LSR_ERROR   0x1e                    //  error conditions

                                            // modem status register
#define MSR_DCTS    0x01                    //  delta clear to send
#define MSR_DDSR    0x02                    //  delta data set ready
#define MSR_TERI    0x04                    //  trailing edge ring indicator
#define MSR_DCD     0x08                    //  delta carrier detect
#define MSR_CTS     0x10                    //  clear to send
#define MSR_DSR     0x20                    //  data set ready (modem ready)
#define MSR_RI      0x40                    //  ring indicated
#define MSR_CD      0x80                    //  carrier detected



/* ******************************************************************** *
 *
 *    8259 Programmable Interrupt Controller Definitions
 *
 * ******************************************************************** */

#define I8259       0x20                    // control register address
#define EOI         0x20                    // end of interrupt command
#define I8259M      0x21                    // mask register



/*
 * Private Data 
 */

CommPort ports[] = {
	{ 0x3f8, 4, UART_None, "COM1:" },
	{ 0x2f8, 3, UART_None, "COM2:" },
	{ 0x3e8, 4, UART_None, "COM3:" },
	{ 0x2e8, 3, UART_None, "COM4:" },
	{     0, 0, UART_None, 0       },		// End of table marker
};

static UWORD baudDivisor[] = {
	0x60,	// 1200
	0x30,	// 2400
	0x18,	// 4800
	0x0c,	// 9600
	0x06,	// 19200
	0x03,	// 38400
	0x02,	// 57600
	0x01,	// 115200
};



const size_t RX_BufferSize = 8192;
const size_t TX_BufferSize = 2048;

/*==========================================================
 * Private Serial functions
 */

/*
 * Interrupt Control class disabled interrupts
 * and automatically re-enables them when the
 * function returns without needing to have to
 * put enable() before every return!
 */

class InterruptControl {
public:
	InterruptControl() { _disable(); }
	~InterruptControl() { _enable(); }
};

/*
 * Check a particular port for a UART
 * Based on routine on page 121 of
 *		C++ Communications Utilities
 *		Michael Holmes & Bob Flanders
 */

static PortType checkPort(CommPort* p)
{
	UBYTE mcr;
	UBYTE msr;

	UWORD port = p->port;

	InterruptControl intControl;		// Disable Interrupts until destructed

	/*
	 * Is there a UART there at all?
	 */

	mcr = inp(port + MCR);
	outp(port + MCR, 0x18);	// Into loopback mode
	msr = inp(port + MSR);
	outp(port + MCR, mcr);

	if( (msr & 0xf0) != 0x80)
		return UART_None;

#if 1		// Can't get this to work!
	/*
	 * Try and find out what IRQ it uses...
	 */
	
	// Interrupts already disabled

	UBYTE oldLCR = inp(port + LCR);
	UBYTE oldIER = inp(port + IER);
	UBYTE oldMCR = inp(port + MCR);
	UBYTE oldPIC = inp(I8259M);
	outp(port + LCR, oldLCR & ~LCR_DLAB);
	outp(port + IER, 0);
	outp(port + MCR, (oldMCR | MCR_OUT2) & ~MCR_LOOP);
	outp(I8259, 0x0a);

	// This is where it goes wrong... irr is always 0
	// Setting the TBE flag in IER seems to have no effect on 8259.
	// I;ve tried lots of things including even putting a character
	// in the Transmit buffer and waiting for iir to be non zero, but
	// nothing happened.
	// At the end IIR is 1, which means no Interrupt was pending
	// I tried setting all the bits in IER and then moving the mouse
	// while it was checking COM1, but still nothing.

	outp(port + IER, IER_TBE);					// Enable Transmit Buffer Empty Interrupt
	UBYTE irr = inp(I8259) & 0xfc;
	outp(port + IER, 0);
	irr = ~inp(I8259) & irr;
	outp(I8259M, oldPIC & ~irr);
	outp(I8259, 0x0c);
	irr = inp(I8259);
	inp(port + LSR);
	inp(port + IIR);
	inp(port + MSR);
	outp(port + IER, oldIER);
	outp(port + LCR, oldLCR);
	outp(port + MCR, oldMCR);

	outp(I8259, 0x20);
	outp(I8259M, oldPIC);

	if(irr & 0x80)
	{
		UBYTE irq = irr & 7;
		if((irq >= 2) && (irq <= 7))
			p->irq = irq;
	}
#if 0		// USe Default IRQ anyway!
	else
		return UART_None;
#endif

	// don't re-enable interrupts
#endif

	/*
	 * Find out what type of UART it is
	 */

	outp(port + SCR, 0x55);

	if(inp(port + SCR) != 0x55)
		return UART_8250;

	outp(port + FCR, 0xcf);

	if((inp(port + IIR) & 0xc0) != 0xc0)
		return UART_16450;

	outp(port + FCR, 0);
	return UART_16550;
} 

/*
 * Set up the ports[] array to any existing ports
 */
 
int SerialConnection::checkPorts()
{
	for(CommPort* p = ports; p->name; p++)
	{
		p->available = checkPort(p);
		if(p->available != UART_None)
			portCount++;
	}

	return portCount;
}

/*==========================================================
 * Implementation of a volatile Queue to be used for
 * Tx/Rx buffers
 */

class QueueBuffer {
	size_t size;
	volatile UBYTE* data;
	volatile int head;
	volatile int tail;
public:
	QueueBuffer(size_t length);
	~QueueBuffer();

	Boolean put(UBYTE b);
	Boolean put(UBYTE* data, size_t length); 
	UBYTE get();

	Boolean canGet() const
	{
		return head != tail;
	}

	Boolean canPut() const
	{
		return (advance(head) != tail);
	}

	void clear()
	{
		_disable();
		head = tail = 0;
		_enable();
	}

private:
	int advance(int marker) const
	{
		return (marker + 1) % size;
	}
};

/*
 * Construct a QueueBuffer
 */

QueueBuffer::QueueBuffer(size_t length)
{
	data = new UBYTE[length];
	size = length;
	head = 0;
	tail = 0;
}

/*
 * Destruct QueueBuffer
 */

QueueBuffer::~QueueBuffer()
{
	delete[] data;
}

/*
 * Add a single byte to a QueueBuffer
 * Return True if succesful, False if queue was full
 */

Boolean QueueBuffer::put(UBYTE b)
{
	if(canPut())
	{
		data[head] = b;
		head = advance(head);
		return True;
	}
	else
		return False;
}

/*
 * Add a whole load of data to a QueueBuffer
 * Return True if successful
 *
 * This could be optimised to copy into buffer without having
 *	to call canSend() each byte by copying sections of the data
 * into the buffer splitting as necessary at the end of the
 * queue.
 *    
 */

Boolean QueueBuffer::put(UBYTE* data, size_t length)
{
	while(length--)
		if(!put(*data++))
			return False;
	return True;
}

UBYTE QueueBuffer::get()
{
	if(canGet())
	{
		UBYTE item = data[tail];
		tail = advance(tail);
		return item;
	}
	else
		return 0;
}

/*==========================================================
 * Physical Level Functions
 */

/*
 * Interrupt Driven Stuff
 */

class Comm {
	QueueBuffer rxBuffer;
	QueueBuffer txBuffer;
	Boolean useFifo;

	UWORD pmSel;		// Old Serial Interrupt Address
	ULONG pmOff;

	UWORD ioPort;
	UBYTE irq;

#ifdef DEBUG
public:
#endif

	volatile UBYTE modemSR;		// Modem Status Register
	volatile UBYTE lineSR;
	volatile Boolean needPrime;

	volatile Boolean newModemSR;
	volatile Boolean newLineSR;

#ifdef DEBUG
	volatile UBYTE lastIIR;
	volatile Boolean newIIR;
#endif

public:
	Comm(BaudRate baud, UWORD ioPort, UBYTE irq, PortType portType, size_t rxSize, size_t txSize);
	~Comm();

	Boolean put(UBYTE* data, size_t size);
	Boolean put(UBYTE data);
	UBYTE get() { return rxBuffer.get(); }

	void flushOutput();

	Boolean canGet() { return rxBuffer.canGet(); }
	void clearInput() { rxBuffer.clear(); }
	void flushInput();

	void cycleDTR();

	friend void interrupt far serialInterrupt();
	// void doInterrupt();
private:
	UBYTE in(UBYTE offset) { return inp(ioPort + offset); }
	void out(UBYTE offset, UBYTE data) { outp(ioPort + offset, data); }
};


/*
 * Global comm for interrupt routine to access
 */

Comm* comm = 0;

// extern "C" {
void interrupt far serialInterrupt()
{
#ifdef FLASH_BORDER
	static ULONG color = 0;
	color += 0x10;
	outp(0x3c8, 0);
	outp(0x3c9, color & 0x3f);
	outp(0x3c9, (color >> 6) & 0x3f);
	outp(0x3c9, (color >> 12) & 0x3f);
#endif
	// comm->doInterrupt();

	UBYTE iir;

	while( ((iir = comm->in(IIR)) & IIR_PEND) == 0)
	{
#ifdef DEBUG
		comm->lastIIR = iir;
		comm->newIIR = True;
#endif

		switch(iir & IIR_II)
		{
		case IIR_MSI:			// Modem Status Interrupt
			comm->newModemSR = True;
			comm->modemSR = comm->in(MSR);
			if(comm->modemSR & MSR_CTS)
			{
				if(comm->in(LSR) & LSR_THRE)		// Transmitter is empty?
				{
					if(comm->txBuffer.canGet())
					{
						comm->out(THR, comm->txBuffer.get());
						comm->needPrime = False;
					}
					else
						comm->needPrime = True;
				}
			}
			break;

		case IIR_LSI:							// Line Status Interrupt
			comm->newLineSR = True;
			comm->lineSR = comm->in(LSR);
			if(comm->lineSR & LSR_THRE)		// New code added...
				comm->needPrime = True;
			break;

		case IIR_TBE:							// Transmit Buffer Empty
			{
#if 0
				UBYTE count = comm->useFifo ? 15 : 1;
				while(count--)
#else
				while(comm->in(LSR) & LSR_THRE)
#endif
				{
					if(comm->in(MSR) & MSR_CTS)		// Is CTS set?
					{
						if(comm->txBuffer.canGet())
						{
							comm->out(THR, comm->txBuffer.get());
							comm->needPrime = False;
						}
						else
						{
							comm->needPrime = True;
							break;
						}
					}
					else
					{
						comm->needPrime = True;
						break;
					}
				}
			}
			break;

		case IIR_RBF:							// Incomming Character
			while(comm->in(LSR) & LSR_DR)		// While data available
				comm->rxBuffer.put(comm->in(RBR));
			break;
		}
	}

	outp(I8259, EOI);
}
// }

/*
 * Initialise Port
 */

Comm::Comm(BaudRate baud, UWORD ioPort, UBYTE irq, PortType portType, size_t rxSize, size_t txSize) :
	rxBuffer(rxSize),
	txBuffer(txSize)
{
	/*
	 * If already setup, then close it down
	 */

	if(comm)
	{
		delete comm;
		comm = 0;
	}

	/*
	 * Setup Hardware
	 */

	Comm::ioPort = ioPort;
	Comm::irq = irq;
	useFifo = (portType == UART_16550);

	out(IER, 0);					// Clear Interrupt Enable

	/*
	 * Install Interrupt
	 */

	union REGS r;
	struct SREGS sr;

	memset(&r, 0, sizeof(r));
	memset(&sr, 0, sizeof(sr));

	r.h.ah = 0x35;			// Get Vector Interrupt
	r.h.al = irq + 8;
	sr.ds = sr.es = 0;
	int386x(0x21, &r, &r, &sr);
	pmSel = sr.es;
	pmOff = r.x.ebx;

	memset(&r, 0, sizeof(r));
	memset(&sr, 0, sizeof(sr));

	r.h.ah = 0x25;		// Set Vector
	r.h.al = irq + 8;
	r.x.edx = FP_OFF(serialInterrupt);
	sr.ds = FP_SEG(serialInterrupt);
	sr.es = 0;
	int386x(0x21, &r, &r, &sr);

	if(useFifo)
		out(FCR, FCR_16550);		// Enable Fifo

	UWORD divisor = baudDivisor[baud];

	_disable();
	out(LCR, in(LCR) | LCR_DLAB);
	out(DLM, divisor >> 8);
	out(DLL, divisor);
	out(LCR, in(LCR) & ~LCR_DLAB);
	out(MCR, MCR_DO);
	out(LCR, LCR_WLEN);								// 8N1
	in(LSR);							// Reading these registers resets them
	in(MSR);
	in(IIR);
	in(RBR);
	_enable();

	needPrime = True;
#ifdef DEBUG
	newIIR = False;
#endif

	modemSR = in(MSR);
	lineSR = in(LSR);

	newModemSR = True;
	newLineSR = True;

	comm = this;

	_disable();
	outp(I8259M, inp(I8259M) & ~(1 << irq));
	_enable();

	out(IER, IER_ALL);	// IER_RBF | IER_TBE | IER_MSI);		// Enable UART Interrupts
}

/*
 * Remove Interrupt
 */

Comm::~Comm()
{
	out(MCR, 0);
	out(IER, 0);
	out(FCR, 0);

	union REGS r;
	struct SREGS sr;

	memset(&r, 0, sizeof(r));
	memset(&sr, 0, sizeof(sr));

	r.h.ah = 0x25;		// Set Vector
	r.h.al = irq + 8;
	r.x.edx = pmOff;
	sr.ds = pmSel;
	sr.es = 0;
	int386x(0x21, &r, &r, &sr);

	UBYTE mask = inp(I8259M);
	outp(I8259M, mask | (1 << irq));

	comm = 0;
}


Boolean Comm::put(UBYTE* data, size_t size)
{
#ifdef DEBUG1
	netLog.printf("Comm::put()");
	netLog.printf("MSR = %02x", (int) modemSR);
	netLog.printf("LSR = %02x", (int) lineSR);
	netLog.printf("needPrime = %d", needPrime);
#endif

#ifdef DEBUG
	if(needPrime)
		netLog.printf("Output needs priming, modeSR=%d, lineSR=%d",
			(int) modemSR,
			(int) lineSR);
#endif

	// Boolean primed = txBuffer.canGet();		// Is txBuffer empty?

	_disable();
	Boolean flag = txBuffer.put(data, size);

#ifdef DEBUG
	if(needPrime)
		netLog.printf("Output needs priming, modeSR=%d, lineSR=%d",
			(int) modemSR,
			(int) lineSR);
#endif

	if(flag)
	{
		while( (needPrime || (in(LSR) & LSR_THRE)) && (in(MSR) & MSR_CTS))
		{
#ifdef DEBUG
			netLog.printf("Sending direct to Output buffer");
#endif
			out(THR, txBuffer.get());
			needPrime = False;
		}
	}

#ifdef OLD
	if(needPrime && flag && (in(MSR) & MSR_CTS))
	{
#ifdef DEBUG
		netLog.printf("Priming Output buffer");
#endif
		out(THR, txBuffer.get());
		needPrime = False;
	}
#endif

	_enable();

	return flag;
}

Boolean Comm::put(UBYTE c)
{
	_disable();

	Boolean flag = txBuffer.put(c);
	if(needPrime && flag && (in(MSR) & MSR_CTS))
	{
#ifdef DEBUG
		netLog.printf("Priming Output buffer");
#endif
		out(THR, txBuffer.get());
		needPrime = False;
	}
	_enable();

	return flag;
}

/*
 * Lower DTR
 * Wait for 1.5 seconds
 * Raise DTR
 */

void Comm::cycleDTR()
{
	out(MCR, MCR_DO & ~MCR_DTR);
	timer->wait((timer->ticksPerSecond * 3) / 2);
	out(MCR, MCR_DO);
}

/*
 * Clear the input buffer
 *
 * Wait for half a second and clear the buffer.
 */


void Comm::flushInput()
{
	timer->wait(timer->ticksPerSecond / 2);
	clearInput();
}

void Comm::flushOutput()
{
	ULONG timeOut = timer->getCount() + 2 * timer->ticksPerSecond;
	while(txBuffer.canGet() && (timer->getCount() < timeOut))
		;
}

/*==========================================================
 * Packet level functions
 */

static const char PacketID[6] = { 'D', 'W', 'G', '_', '0', '0' };

#if 0
/*
 * Very simple checksum
 *
 * It doesn't matter how it works as long as results are same for same data
 */

UWORD makeCrc(UBYTE* data, size_t length)
{
	UWORD result = 0;

	for(int i = 0; i < length; i++)
	{
		result = result + (*data++ ^ i);
	}

	return result;
}

#else

/* ******************************************************************** *
 *
 *  CRC() -- calculate CRC for message
 *
 * ******************************************************************** */

UWORD makeCrc(UBYTE* data, size_t length)
{
	UWORD acc = 0;                            // crc accumulator

	while (length--)                               // loop thru entire message
	{
		acc = acc ^ (*data++ << 8);                // xor in new byte from msg

		for (int i = 0; i++ < 8;)                   // loop thru each of 8 bits
		{
			if (acc & 0x8000)                   // q. this bit on?
   			acc = (acc << 1) ^ 0x1021;      // a. yes .. shift and xor
			else
				acc <<= 1;                      // else .. just shift
		}
	}

#if 0
	_asm    mov     ax, acc                     // ax = crc value
	_asm    xchg    ah, al                      // swap high and low bytes
	return (_AX);                               // ..and return the crc
#else
	return acc;
#endif
}

#endif

int SerialConnection::sendRawData(PacketType type, UBYTE* data, size_t length, Boolean receipt, ULONG serial)
{
	SerialPacket packet;

	memcpy(packet.id, PacketID, sizeof(packet.id));
	packet.serial = serial;
	packet.needReceipt = receipt;
	packet.crc = makeCrc(data, length);
	packet.pktHead.type = type;
	packet.pktHead.length = length;

#ifdef DEBUG
	packet.random = game->gameRand.getSeed();
#endif

#ifdef DEBUG
	netLog.printf("Serial::sendRawData(type:%s, serial:%ld, receipt:%d, crc:%d, length:%d, rand=%ld)",
		packetStr(type),
		serial,
		receipt,
		(int) packet.crc,
		(int) length,
		packet.random);
#endif


	if(comm->put((UBYTE*)&packet, sizeof(packet)))
		if(!data || comm->put(data, length))
			return 0;
	return -1;
}


/*==========================================================
 * Public Functions overiding Connection's virtual functions
 */

/*
 * Constructor
 */

SerialConnection::SerialConnection()
{
	// serial = 0;
	// rxSerial = 0;

	connected = False;
	portCount = 0;
	for(CommPort* p = ports; p->name; p++)
		p->available = UART_None;
	mode = WaitHeader;
	counter = 0;
}

/*
 * Destructor
 */

SerialConnection::~SerialConnection()
{
	if(connected)
	{
		disconnect();
		connected = False;
	}
}

/*
 * Make connection with other side
 */

int SerialConnection::connect()
{
	/*
	 * Find what ports are available
	 */

	checkPorts();

	if(portCount == 0)
	{
		dialAlert(0, language(lge_NoPorts), language(lge_cancel));
		return -1;
	}

	/*
	 * Ask user to select parameters:
	 *		Port
	 *		IRQ
	 *		Baud Rate
	 */

#if 0
	BaudRate baud;
	UWORD ioPort;
	UBYTE irq;
	PortType portType;


	{
		CommSetup edit;

		switch(edit.process())
		{
			case CI_OK:
				baud = edit.baudRate;
				ioPort  	= ports[edit.portNumber].port;
				irq 		= ports[edit.portNumber].irq;
				portType = ports[edit.portNumber].available;
				break;
			default:
			case CI_Cancel:
				return -1;
		}
	}

	/*
	 * Set up physical parameters
	 */

#ifdef USE_BIOS
	new Comm(baud, i);
#else
	new Comm(baud, ioPort, irq, portType, RX_BufferSize, TX_BufferSize);
#endif

#else
	SerialConfig setup;
	if(setup.getSerialConfig() != CI_OK)
		return -1;

	new Comm(setup.baud, setup.ioPort, setup.irq, setup.portType, RX_BufferSize, TX_BufferSize);
#endif

	if(comm == 0)
		return -1;

	/*
	 * Empty input buffer
	 * Wait for no characters to arrive for at least 3rd second
	 */

	comm->flushInput();

	int val = sendHello();

	if(!val)
		connected = True;

	return val;
}

int SerialConnection::sendHello()
{
#ifdef DEBUG
	netLog.printf("--------------------------");
	netLog.printf("Starting Serial connection");
	netLog.printf("--------------------------");
#endif

	/*
	 * Send hello packets, etc, to get software connection going
	 */

	unsigned int time = timer->getCount();
	int tries = 30;

	MemPtr<char> popBuffer(500);
	strcpy(popBuffer, language(lge_TrySerial));
	char* countPtr = popBuffer + strlen(popBuffer);

	PopupText popup(popBuffer);

	while(tries)
	{
		/*
		 * Mouse Press aborts
		 */

		MouseEvent* event = machine.mouse->getEvent();
		if(event || kbhit())
		{
			ButtonStatus b = event->buttons;

			if(kbhit())
			{
				Key k = getch();
				if(k == 0)
					k = getch();
				b = 1;
			}

			if(b)
			{
#ifdef DEBUG
				dialAlert(0, "User Abort", "OK");
#endif
				return -1;
			}
		}

		/*
		 * Check for incomming packets
		 */

		Packet* pkt;

		while( (pkt = receiveData()) != 0)
		{
#ifdef DEBUG
			netLog.printf("Packet Received");
#endif

			if( (pkt->type == Connect_HELLO) ||
				 (pkt->type == Connect_HelloAck) )
			{
#ifdef DEBUG
				netLog.printf("It was a HELLO or HelloAck");

				sprintf(popBuffer, "Received %s packet\rWaiting for HelloAck\r", packetStr(pkt->type));
				popup.showText(popBuffer);
#endif

				struct HelloPacket {
					ULONG value;
				};

				ULONG sentValue = timer->getFastCount();
				ULONG rxValue = 0;

				HelloPacket* hPkt = new HelloPacket;
				hPkt->value = sentValue;

				sendData(Connect_HelloAck, (UBYTE*) hPkt, sizeof(HelloPacket));

				if(pkt->type == Connect_HelloAck)
				{
					HelloPacket* p = (HelloPacket*) pkt->data;
					rxValue = p->value;
				}
				else
				{
					Boolean ackRx = False;
					while(!ackRx)
					{
						/*
					 	 * Mouse press to abort
					 	 */

						MouseEvent* event = machine.mouse->getEvent();
						if(event || kbhit())
						{
							ButtonStatus b = event->buttons;
	
							if(kbhit())
							{
								Key k = getch();
								if(k == 0)
									k = getch();
								b = 1;
							}

							if(b)
							{
#ifdef DEBUG	
								dialAlert(0, "User Abort", "OK");
#endif
								delete pkt;
								return -1;
							 }
						}

						if(flush())
						{
							delete pkt;
							return -1;
						}

						Packet* pkt1; 

						while(!ackRx && (pkt1 = receiveData()) != 0)
						{
							if(pkt1->type == Connect_HelloAck)
							{
#ifdef DEBUG
								netLog.printf("Received HelloAck packet");
#endif
								HelloPacket* p = (HelloPacket*) pkt1->data;
								rxValue = p->value;

								ackRx = True;
							}
#ifdef DEBUG
							else
								netLog.printf("Received packet, but it wasn't an HelloAck");
#endif
							delete pkt1;
						}
					}
				}

#ifdef DEBUG
				netLog.printf("sentValue=%ld, rxValue=%ld", sentValue, rxValue);

				sprintf(popBuffer, "Finished connection\rWaiting for acknowledge", packetStr(pkt->type));
				popup.showText(popBuffer);
#endif

				if(sentValue > rxValue)
					inControl = True;
				else if(sentValue == rxValue)
				{
#ifdef DEBUG	
					netLog.printf("rx and sent values were the same");
				 	dialAlert(0, "Hello Values identical!", "Try\rAgain");
#endif
					delete pkt;
					return -1;
				}

				/*
				 * We think we've finished...
				 * Wait for other side to acknowledge
				 */

				delete pkt;
				return flush();
			}

			delete pkt;
		}

		/*
		 * If timer run out (or 1st pass through)
		 * send a Hello Message
		 */

		if(timer->getCount() >= time)
		{
#ifdef DEBUG
			netLog.printf("Sending Hello Message (timeout = %d)", tries);
#endif
			sprintf(countPtr, "%d", tries);
			popup.showText(popBuffer);

			time = timer->getCount() + 3 * timer->ticksPerSecond;
			tries--;

			char buffer[] = "Hello Civil War Player!\n\r";
			sendRawData(Connect_HELLO, (UBYTE*)buffer, strlen(buffer) + 1, False, serial++);
		}
	}
#ifdef DEBUG
	dialAlert(0, "Time Out", "Ok");
#endif

	return -1;
}

/*
 * Break connection
 */

void SerialConnection::disconnect()
{
	comm->flushOutput();

	delete comm;
}


#ifdef COMMS_OLD
/*
 * Receive any waiting packets
 */

Packet* SerialConnection::receiveData()
{
	processIncomming();
	return packetList.get();
}
#endif

/*
 * Check Input Buffer for valid Packets
 */

void SerialConnection::processIncomming()
{
#ifdef DEBUG1
	if(comm->newModemSR)
	{
		netLog.printf("new MSR = %02x", (int) comm->modemSR);
		comm->newModemSR = False;
	}

	if(comm->newLineSR)
	{
		netLog.printf("new LSR = %02x", (int) comm->lineSR);
		comm->newLineSR = False;
	}

	if(comm->newIIR)
	{
		netLog.printf("new IIR = %02x", (int) comm->lastIIR);
		comm->newIIR = False;
	}

#endif

	while(comm->canGet())
	{
		UBYTE c = comm->get();

#ifdef DEBUG1
		netLog.printf("Rx: %02x", (int) c);
#endif

		if(mode == WaitHeader)
		{
			if(c != PacketID[counter])
			{
				counter = 0;
#ifdef DEBUG
				netLog.printf("Corrupt Packet Header byte %d = %d",
					counter, (int) c);
#endif
			}
			else
			{
				incommingHeader.id[counter++] = c;
				if(counter >= sizeof(incommingHeader.id))
				{
#ifdef DEBUG1
					netLog.printf("Got Header ID");
#endif
					mode = ReadingHeader;
				}
			}
		}
		else if(mode == ReadingHeader)
		{
			UBYTE* ptr = (UBYTE*) &incommingHeader;

			ptr[counter++] = c;

			if(counter >= sizeof(SerialPacket))
			{
				if(incommingHeader.pktHead.length > 64)
				{
#ifdef DEBUG
					netLog.printf("Packet too big... waitHeader");
#endif
					mode = WaitHeader;
					counter = 0;
				}
				else
				{
#ifdef DEBUG1
					netLog.printf("Serial gotPacketHeader: Serial:%ld, receipt:%d, crc:%d, type:%s, length:%d, rand=%ld",
						incommingHeader.serial,
						(int) incommingHeader.needReceipt,
						incommingHeader.crc,
						packetStr(incommingHeader.pktHead.type),
						(int) incommingHeader.pktHead.length,
						incommingHeader.random);
					ULONG ourRandom = game->gameRand.getSeed();
					if(incommingHeader.random != ourRandom)
						netLog.printf("RANDOM NUMBERS OUT OF SYNC! %ld and %ld",
							incommingHeader.random, ourRandom);
							
#endif
					mode = ReadingData;
					counter = 0;
				}
			}
		}
		else if(mode == ReadingData)
			incommingData[counter++] = c;


		if( (mode == ReadingData) && (counter >= incommingHeader.pktHead.length))
		{
#ifdef DEBUG1
			netLog.printf("Finished Reading Data... waitHeader");
#endif
			if(incommingHeader.crc == makeCrc(incommingData, incommingHeader.pktHead.length))
			{
#ifdef DEBUG1
				netLog.printf("CRC Correct");
#endif

				if(incommingHeader.pktHead.type == Connect_Acknowledge)
					receiptList.remove(incommingHeader.serial);
				else
				{
#ifdef DEBUG
					netLog.printf("Serial Received: type:%s, Serial:%ld, receipt:%d, crc:%d, length:%d, random:%ld",
						packetStr(incommingHeader.pktHead.type),
						incommingHeader.serial,
						(int) incommingHeader.needReceipt,
						incommingHeader.crc,
						(int) incommingHeader.pktHead.length,
						incommingHeader.random);
					ULONG ourRandom = game->gameRand.getSeed();
					if(incommingHeader.random != ourRandom)
						netLog.printf("RANDOM NUMBERS OUT OF SYNC! %ld and %ld",
							incommingHeader.random, ourRandom);
#endif

					if(incommingHeader.needReceipt)
					{
#ifdef DEBUG
						netLog.printf("Sending receipt for %ld", incommingHeader.serial);
#endif
						sendRawData(Connect_Acknowledge, 0, 0, False, incommingHeader.serial);
					}


					/*
					 * Ony process it, if serial is bigger than ones we have
					 * already processed!
					 */

					if(incommingHeader.serial >= rxSerial)
					{
						rxSerial = incommingHeader.serial + 1;

#ifdef DEBUG1
						netLog.printf("Copying Packet to packetList");
#endif

						Packet* packet = new Packet;
						packet->type = incommingHeader.pktHead.type;
						packet->length = incommingHeader.pktHead.length;
						packet->sender = RID_Remote;
						if(packet->length)
							memcpy(packet->data, incommingData, packet->length);
						packetList.append(packet);
					}
#ifdef DEBUG
					else
						netLog.printf("Packet's serial number %ld is smaller than rxSerial %ld",
							incommingHeader.serial, rxSerial);
#endif
				}
			}
#ifdef DEBUG
			else
				netLog.printf("CRC Error %d instead of %d",
						(int) makeCrc(incommingData, incommingHeader.pktHead.length),
						(int) incommingHeader.crc);
#endif

		 	mode = WaitHeader;
		 	counter = 0;
		}
	}
}

/*===================================================
 * Modem Class
 */

/*
 * Some static Strings
 */

static char mdmOKMessage[] = "OK";
static char mdmConnectMessage[] = "CONNECT";
// static char mdmDialMessage[] = "DIALLING";
// static char mdmRingMessage[] = "RING";

static char *mdmErrorMessages[] = {
	"BUSY",
	"NO CARRIER",
	"ERROR",
	"NO DIAL TONE",
	"NO ANSWER",
	"VOICE",
	"FAX",
	0
};

/*
 * Global storage for modem strings
 */

/*-------------------------------------------------------------------
 * Modem Connection Class Implementation
 */


ModemConnection::ModemConnection()
{
	popup = 0;
	popupBuffer = 0;
	popupPtr = 0;

	initString = 0;
	dialString = 0;
	answerString = 0;
	phoneString = 0;
}

ModemConnection::~ModemConnection()
{
	killPopup();

	delete[] initString;
	delete[] dialString;
	delete[] answerString;
	delete[] phoneString;
}

int ModemConnection::connect()
{
	Boolean finished = False;

	/*
	 * Find what ports are available
	 */

	checkPorts();

	if(portCount == 0)
	{
		dialAlert(0, language(lge_NoPorts), language(lge_cancel));
		return -1;
	}

#if 0
	BaudRate baud;
	UWORD ioPort;
	UBYTE irq;
	PortType portType;
#endif

	while(!finished)
	{
		/*
		 * Do the Comm Port setup first
		 */
#if 0
		{
			CommSetup edit;

			switch(edit.process())
			{
				case CI_OK:
					baud		= edit.baudRate;
					ioPort  	= ports[edit.portNumber].port;
					irq 		= ports[edit.portNumber].irq;
					portType = ports[edit.portNumber].available;
					break;
				default:
				case CI_Cancel:
					return -1;
			}
		}

		new Comm(baud, ioPort, irq, portType, RX_BufferSize, TX_BufferSize);
#else
		{
			SerialConfig setup;
			if(setup.getSerialConfig() != CI_OK)
				return -1;
			new Comm(setup.baud, setup.ioPort, setup.irq, setup.portType, RX_BufferSize, TX_BufferSize);
		}
#endif

		if(comm == 0)
			return -1;

		/*
	 	 * Now do the modem setup
	 	 */

		Boolean inModemSetup = True;

		while(inModemSetup)
		{
#if 0

			int result;

			{
				ModemSetup edit;

				result = edit.process();

				switch(result)
				{
					case MDI_Dial:
					case MDI_Answer:
						if(modemStringsChanged)
						{
							delete[] initString;
							delete[] dialString;
							delete[] answerString;
							delete[] phoneString;
						}

						initString = copyString(edit.initString);
						dialString = copyString(edit.dialString);
						answerString = copyString(edit.answerString);
						phoneString = copyString(edit.phoneString);
						modemStringsChanged = True;

						break;

					default:
					case MDI_Cancel:
						inModemSetup = False;
						break;
				}
			}		// edit is destructed here
#else
			ModemDialID result = getModemConfig(&initString, &dialString, &answerString, &phoneString);
#endif

			switch(result)
			{
				case MDI_Dial:
					if(dialModem(initString, dialString, phoneString) == 0)
					{
						finished = True;
						inModemSetup = False;
					}
					else
						dialAlert(0, language(lge_ModemNoConnect), language(lge_OK));
					break;

				case MDI_Answer:
					if(answerModem(initString, answerString) == 0)
					{
						finished = True;
						inModemSetup = False;
					}
					else
						dialAlert(0, language(lge_ModemNoConnect), language(lge_OK));
					break;

				default:
					inModemSetup = False;
					break;
			}


			killPopup();
		}
	}

	if(sendHello())
	{
		hangup();
		return -1;
	}
	else
		return 0;

	// return SerialConnection::connect();
}

/*
 * Initialise Modem
 * Lower DTR
 * Pause 1 second
 * Raise DTR
 * Send init String
 */

int ModemConnection::initModem(const char* initString)
{
	showInfo(True, language(lge_initModem));

	/*
	 * Lower DTR
	 * Wait for a bit
	 * Raise DTR
	 */

	if(comm)
	{
		comm->cycleDTR();

		sendModem(initString);
		sendModem("\r");

		/*
	 	 * Wait for OK message
	 	 */

		MemPtr<char> buffer(80);

		for(;;)
		{
			if(waitModem(buffer, 80))
				return -1;
			if(strstr(buffer, mdmOKMessage))
			{
				comm->flushInput();
				return 0;
			}
		}
	}

	return -1;
}

/*
 * Send a modem command to the modem
 *
 * ^M means CR (^c means send c & 0x1f)
 * ~ means pause for 1/2 second
 *
 * Input buffer is cleared before a CR is sent
 */


int ModemConnection::sendModem(const char* message)
{
#ifdef DEBUG1
	static Boolean gotCR = True;

	if(gotCR)
	{
		showInfo(True, "Send: ");
		gotCR = False;
	}
#endif

	char c;
	while( (c = *message++) != 0)
	{
		switch(c)
		{
			case '~':		// Twiddle means pause for 1/2 second
				timer->wait(timer->ticksPerSecond / 2);
				break;
			case '^':		// Carat means control character follows
				c = *message++;
				if(c != '^')
					c &= 0x1f;
				// Drop through...
			default:
				if(c == '\r')
				{
#ifdef DEBUG1
					gotCR = True;
#endif
					timer->wait( (timer->ticksPerSecond + 59) / 60);	// Allow time for echod characters
					// comm->clearInput();
				}
#ifdef DEBUG1
				showInfo(True, "%c", c);
#endif
				comm->put(c);
				timer->wait( (timer->ticksPerSecond + 59) / 60);		// Time for 2 characters at 1200 baud
				break;
		}
	}


	return 0;
}

/*
 * Wait for a message from the modem
 * A message is anything followed by a CR
 * Ignore CR's on their own.
 *
 * Also the user can cancel by clicking the mouse
 * Or it can time out after.
 */

int ModemConnection::waitModem(char* buffer, size_t maxLength)
{
	char* ptr = buffer;

	buffer[0] = 0;

	Boolean finished = False;

	unsigned int time = timer->getCount() + timer->ticksPerSecond * 60;

	while(!finished)
	{
		/*
		 * Mouse Press aborts
		 */

		MouseEvent* event = machine.mouse->getEvent();
		if(event)
		{
			ButtonStatus b = event->buttons;
			if(b)
			{
#ifdef DEBUG
				dialAlert(0, "User Abort", "OK");
#endif
				return -1;
			}
		}

		/*
		 * See if Timer has timed out
		 */

		if(timer->getCount() >= time)
		{
#ifdef DEBUG
			dialAlert(0, "Time Out", "OK");
#endif
			return -1;
		}

		/*
		 * See if there are any characters waiting
		 */

		if(comm->canGet())
		{
			UBYTE c = comm->get();

			if(c == '\n')
			{
				;	// Ignore
			}
			else
			if(c == '\r')
			{
				/*
				 * Return unless it is an echo of an AT command
				 * or the buffer is empty
				 */

				if(buffer[0] && !( (buffer[0] == 'A') && (buffer[1] == 'T')))
					finished = True;
				else
				{
					ptr = buffer;
					buffer[0] = 0;
				}
			}
			else if( (c != ' ') || buffer[0])	// Ignore leading spaces
				*ptr++ = c;
		}
	}

	*ptr = 0;

	showInfo(True, "\001> %s\r", buffer);

	/*
	 * The following mean an error
	 */

	strupr(buffer);
	char** msgs = mdmErrorMessages;
	while(*msgs)
	{
		if(strstr(buffer, *msgs++))
			return -1;
	}

	return 0;
}

int ModemConnection::dialModem(const char* initString, const char* dialString, const char* phoneString)
{

	if(initModem(initString))
		return -1;

	sendModem(dialString);
	sendModem(phoneString);
	sendModem("\r");

	showInfo(True, "%s: %s\r", language(lge_Dial), phoneString);

	return waitConnect();
}

int ModemConnection::answerModem(const char* initString, const char* answerString)
{
	if(initModem(initString))
		return -1;

	sendModem(answerString);
	sendModem("\r");

	/*
	 * Wait for a message
	 */

	showInfo(True, language(lge_waitCall));

	return waitConnect();
}

int ModemConnection::waitConnect()
{

	MemPtr<char> modemMessage(80);

	for(;;)
	{
		strupr(modemMessage);

		if(waitModem(modemMessage, 80))		// A serious error, or user cancelled
			return -1;

		if(strstr(modemMessage, mdmConnectMessage))	// Success
			return 0;
	}

	return -1;
}

void ModemConnection::hangup()
{
	initModem(initString);
}

void ModemConnection::disconnect()
{
	hangup();

	SerialConnection::disconnect();
}


void ModemConnection::showInfo(Boolean permanent, const char* fmt, ...)
{
	if(!popupBuffer)
	{
		popupBuffer = new char[1024];
		popupPtr = popupBuffer;
	}

	va_list vaList;
	va_start(vaList, fmt);
	vsprintf(popupPtr, fmt, vaList);
	va_end(vaList);

	if(popup)
		popup->showText(popupBuffer);
	else
		popup = new PopupText(popupBuffer);

#ifdef DEBUG
	netLog.printf("%s", popupPtr);
#endif

	if(permanent)
		popupPtr = popupBuffer + strlen(popupBuffer);
}

void ModemConnection::killPopup()
{
	delete popup;
	popup = 0;
	delete[] popupBuffer;
	popupBuffer = 0;
}

#endif	// DEMO
