/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Troop movement User Interface
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.12  1994/07/28  18:57:04  Steven_Green
 * Naval Movement
 *
 * Revision 1.3  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.1  1994/05/21  13:16:19  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


#include "memptr.h"

#include "campunit.h"
#include "game.h"
#include "layout.h"
#include "system.h"
#include "screen.h"
#include "mouselib.h"
#include "generals.h"
#include "campaign.h"
#include "colours.h"
#include "mapwind.h"
#include "moblist.h"
#include "trig.h"
#include "menuicon.h"
#include "campwld.h"
#include "menudata.h"
#include "cal.h"
#include "timer.h"
#include "railway.h"
#include "city.h"
#include "connect.h"
#include "language.h"
#include "strutil.h"
#include "darken.h"

#ifdef DEBUG
#include "tables.h"
#include "options.h"
#include "msgwind.h"
#endif

#include "dialogue.h"

/*
 * Orders Sub Menu
 */

class CampaignOrderIcon : public Icon {
	GameSprites iconNumber;
	OrderMode mode;
	TrackUnit* control;
public:
	CampaignOrderIcon(TrackUnit* c, IconSet* parent, GameSprites n, Rect r, OrderMode ord) :
		Icon(parent, r),
		iconNumber(n),
		mode(ord),
		control(c)
	{
	}

	void drawIcon();
	void execute(Event* event, MenuData* data);
};


void CampaignOrderIcon::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));

	SpriteBlock* spriteImage = game->sprites->read(iconNumber);
	bm.blit(spriteImage, Point(0,0));
	spriteImage->release();

	/*
	 * Highlight current selected order
	 */

	if(mode == control->currentMode)
	{
		// bm.frame(Point(1,1), getSize() - Point(2,2), Colours(LYellow));

		// brightenRegion(&bm, Rect(Point(1, 1), getSize() - Point(2,2)));
			
		colourizeRegion(&bm, Rect(Point(0, 0), getSize()), Red7);
	}
	else
	if(control->currentObject && (control->currentObject->obType == OT_Unit))
	{
		Unit* currentUnit = (Unit*) control->currentObject;

		/*
		 * Highlight Units current Order
		 */

		if(mode == currentUnit->realOrders.mode)
		{
			// bm.frame(Point(3,3), getSize() - Point(6,6), Colours(LRed));

			colourizeRegion(&bm, Rect(Point(0, 0), getSize()), Gold3);
		}

		/*
		 * Highlight units last sent order
		 */

		else
		if(mode == currentUnit->givenOrders.mode)
		{
			// bm.frame(Point(2,2), getSize() - Point(4,4), Colours(Blue));
			colourizeRegion(&bm, Rect(Point(0, 0), getSize()), Gold7);
		}
	}

	machine.screen->setUpdate(bm);
}

void CampaignOrderIcon::execute(Event* event, MenuData* data)
{
	data=data;

	if( event->overIcon )
	{
	  	char a = 0;

		MemPtr<char> buttons(200);
		strcpy(buttons, language(LGE(lge_OrderAd + mode)));
		if(control->currentMode == mode)
			strcat(buttons, language(lge_latestO));

		data->showHint( buttons );
	}

	if(event->buttons & Mouse::LeftButton)
	{
		control->currentMode = mode;

		parent->setRedraw();
		control->setPointer(M_WhereTo);
	}

}

class C_AdvanceIcon : public CampaignOrderIcon {
public:
	C_AdvanceIcon(TrackUnit* c, IconSet* parent) :
		CampaignOrderIcon(c, parent, C2_Advance, Rect(4, 0, 113, 52), ORDER_Advance) { }
};

class C_CautiousIcon : public CampaignOrderIcon {
public:
	C_CautiousIcon(TrackUnit* c, IconSet* parent) :
		CampaignOrderIcon(c, parent, C2_Cautious, Rect(4, 52, 57, 53), ORDER_CautiousAdvance) { }
};


class C_AttackIcon : public CampaignOrderIcon {
public:
	C_AttackIcon(TrackUnit* c, IconSet* parent) :
		CampaignOrderIcon(c, parent, C2_Attack, Rect(61, 52, 57, 53), ORDER_Attack) { }
};


class C_StandIcon : public CampaignOrderIcon {
public:
	C_StandIcon(TrackUnit* c, IconSet* parent) :
		CampaignOrderIcon(c, parent, C2_Stand, Rect(4, 105, 113, 52), ORDER_Stand) { }
};


class C_HoldIcon : public CampaignOrderIcon {
public:
	C_HoldIcon(TrackUnit* c, IconSet* parent) :
		CampaignOrderIcon(c, parent, C2_Hold, Rect(4, 157, 57, 53), ORDER_Hold) { }
};


class C_DefendIcon : public CampaignOrderIcon {
public:
	C_DefendIcon(TrackUnit* c, IconSet* parent) :
		CampaignOrderIcon(c, parent, C2_Defend, Rect(61, 157, 57, 53), ORDER_Defend) { }
};


class C_WithdrawIcon : public CampaignOrderIcon {
public:
	C_WithdrawIcon(TrackUnit* c, IconSet* parent) :
		CampaignOrderIcon(c, parent, C2_Withdraw, Rect(4, 210, 113, 52), ORDER_Withdraw) { }
};


class C_RetreatIcon : public CampaignOrderIcon {
public:
	C_RetreatIcon(TrackUnit* c, IconSet* parent) :
		CampaignOrderIcon(c, parent, C2_Retreat, Rect(4, 262, 57, 53), ORDER_Retreat) { }
};


class C_HastyRetreatIcon : public CampaignOrderIcon {
public:
	C_HastyRetreatIcon(TrackUnit* c, IconSet* parent) :
		CampaignOrderIcon(c, parent, C2_HastyRetreat, Rect(61, 262, 57, 53), ORDER_HastyRetreat) { }
};


class CampaignMoveIcon : public Icon {
	GameSprites iconNumber;
	OrderHow how;
	TrackUnit* control;
public:
	CampaignMoveIcon(TrackUnit* c, IconSet* parent, GameSprites n, Rect r, OrderHow h) :
		Icon(parent, r),
		iconNumber(n),
		how(h),
		control(c)
	{
	}

	void drawIcon();
	void execute(Event* event, MenuData* data);
};


void CampaignMoveIcon::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));

	SpriteBlock* spriteImage = game->sprites->read(iconNumber);
	bm.blit(spriteImage, Point(0,0));
	spriteImage->release();

#if 0
	/*
	 * Highlight current selected order
	 */

	if(how == control->currentHow)
	{
		bm.frame(Point(1,1), getSize() - Point(2,2), Colours(LYellow));
	}


	if(control->currentObject && (control->currentObject->obType == OT_Unit))
	{
		Unit* currentUnit = (Unit*) control->currentObject;
	
		/*
		 * Highlight Units current Order
		 */

		if(how == currentUnit->realOrders.how)
		{
			bm.frame(Point(3,3), getSize() - Point(6,6), Colours(LRed));
		}

		/*
		 * Highlight units last sent order
		 */
	}
#else

	ASSERT(control->currentObject != 0);
	ASSERT(control->currentObject->obType == OT_Unit);

	/*
	 * Colour in as:
	 *		Current order : Bright Red
	 *    real order	  : Gold3
	 *    given order	  : Gold7
	 *    none          : Normal Full Colour
	 */


	Unit* currentUnit = (Unit*) control->currentObject;

	if(how == control->currentHow)
		colourizeRegion(&bm, Rect(Point(0, 0), getSize()), Red7);
	else if(currentUnit && (how == currentUnit->realOrders.how))
		colourizeRegion(&bm, Rect(Point(0, 0), getSize()), Gold3);
	else if(currentUnit && (how == currentUnit->givenOrders.how))
		colourizeRegion(&bm, Rect(Point(0, 0), getSize()), Gold7);
	else
		;
#endif

	machine.screen->setUpdate(bm);
}

void CampaignMoveIcon::execute(Event* event, MenuData* data)
{
	data=data;

	if(event->buttons & Mouse::LeftButton)
	{
		if(control->currentHow != how)
		{
			control->currentHow = how;
			control->setMoveMode();
			parent->setRedraw();
		}
	}

	if( event->overIcon )
	{
		static LGE moveHint[] = {
			lge_OrderByLand,
			lge_OrderByRail,
			lge_OrderByWater,
			lge_OrderByAny,
		};

		data->showHint(language(moveHint[how]));
	}
}

class C_AnyIcon : public CampaignMoveIcon {
public:
	C_AnyIcon(TrackUnit* c, IconSet* parent) :
		CampaignMoveIcon(c, parent, C2_AnyMove, Rect(4, 315, 57, 26), ORDER_Any) { }
};

class C_LandIcon : public CampaignMoveIcon {
public:
	C_LandIcon(TrackUnit* c, IconSet* parent) :
		CampaignMoveIcon(c, parent, C2_Land, Rect(4, 341, 57, 26), ORDER_Land) { }
};

class C_RailIcon : public CampaignMoveIcon {
public:
	C_RailIcon(TrackUnit* c, IconSet* parent) :
		CampaignMoveIcon(c, parent, C2_Rail, Rect(61, 315, 57, 26), ORDER_Rail) { }
};

class C_WaterIcon : public CampaignMoveIcon {
public:
	C_WaterIcon(TrackUnit* c, IconSet* parent) :
		CampaignMoveIcon(c, parent, C2_Water, Rect(61, 341, 57, 26), ORDER_Water) { }
};

#if 0
class C_RailIcon : public CampaignMoveIcon {
public:
	C_RailIcon(TrackUnit* c, IconSet* parent) :
		CampaignMoveIcon(c, parent, C2_Rail, Rect(4, 315, 57, 53), ORDER_Rail) { }
};

class C_WaterIcon : public CampaignMoveIcon {
public:
	C_WaterIcon(TrackUnit* c, IconSet* parent) :
		CampaignMoveIcon(c, parent, C2_Water, Rect(61, 315, 57, 53), ORDER_Water) { }
};
#endif

class C_MiniOK : public MenuIcon {
	TrackUnit* control;
public:
	C_MiniOK(TrackUnit* c, IconSet* parent) :
		MenuIcon(parent, C2_MiniOK, Rect(MENU_ICON_X, MENU_ICON_Y8, MENU_ICON_W, MENU_ICON_H))
	{
		control = c;
	}
		
	void execute(Event* event, MenuData* data);
};

void C_MiniOK::execute(Event* event, MenuData* data)
{
	data = data;

	if(event->overIcon)
		data->showHint( language( lge_cancel ) );
	
	if(event->buttons)
		control->clickOK();
}

void TrackUnit::clickOK()
{
	if(currentObject)
	{
		if(currentObject->obType == OT_Unit)
		{
			Unit* currentUnit = (Unit*) currentObject;

			if ( campaign->world->CheckLocalDest( currentUnit->realOrders.destination ) ) 
			{
	 			dialAlert( 0, language( lge_InvalidDest  ), language( lge_OK ) );
 				return;
			}

			/*
			 * If the current order has changed then send an order
			 * otherwise just cancel it.
			 */


			if( (currentMode != currentUnit->realOrders.mode) ||
			    (currentHow != currentUnit->realOrders.how) )
			{
				sendOrder(currentUnit->realOrders.destination);
			}
		}
	}

	closeDown();
}

/*
 * Temporary Objects
 */

class TemporaryDisplay : public MobIL {
protected:
	TrackUnit* control;
public:
	TemporaryDisplay(TrackUnit* parent, const Location& l);
	~TemporaryDisplay();

	virtual void mapDraw(MapWindow* map, Region* bm, Point where) = 0;
};

TemporaryDisplay::TemporaryDisplay(TrackUnit* parent, const Location& l) :
	MobIL(l)
{
	control = parent;
	highlight = Map_Highlight;
	campaign->tempObjects.add(this);
	alwaysDraw = True;
}

TemporaryDisplay::~TemporaryDisplay()
{
	campaign->tempObjects.remove(this);
}

/*=======================================================================
 * Naval Unit Icons
 */

class NavalMoveIcon : public Icon {
	TrackUnit* control;
	WaterZone* zone;
	NavalType boatType;
public:
	NavalMoveIcon(TrackUnit* c, IconSet* parent, NavalType type, SDimension y) :
		Icon(parent, Rect(0, y, 122, 78))
	{
		control = c;
		boatType =type;
		zone = (WaterZone*) c->currentObject;
	}

	void drawIcon();
	void execute(Event* event, MenuData* d);
};


void NavalMoveIcon::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));

	zone->placeInfo(&bm, boatType, &control->fleet);
}

void NavalMoveIcon::execute(Event* event, MenuData* d)
{
	if( event->overIcon )
	{
#if 0
	  	char a = 0;

		LGE message = lge_OrderNavalUnit;

		while ( a++ < boatType ) 
			INCREMENT( message );


		MemPtr<char> buttons(100);

		strcpy( buttons, language( message ) );
		
		a = strlen( buttons );

		a += strlen( language( lge_NotAvailable ) );

		if ( !zone->boats.count[boatType] && a < 100 ) strcat ( buttons, language( lge_NotAvailable ) );
#else
		MemPtr<char> buttons(200);
		strcpy(buttons, language(LGE(lge_OrderNavalUnit + boatType)));
		if(!zone->boats.count[boatType])
			strcat(buttons, language( lge_NotAvailable));
#endif
		d->showHint( buttons );
	}

	if(event->buttons)
	{
		UBYTE& homeBoats = zone->boats.count[boatType];
		UBYTE& awayBoats = control->fleet.count[boatType];

		if(event->buttons & Mouse::LeftButton)
		{
			if(homeBoats > awayBoats)
			{
				awayBoats++;
				setRedraw();
			}
		}
		else	// assume right
		{
			if(awayBoats)
			{
				awayBoats--;
				setRedraw();
			}
		}
	}

}

WaterZone* findCloseWaterZone(MapWindow* map, const Point& where, Flotilla& fleet)
{
	WaterZone* best = 0;
	SDimension dist = 0;

	if(map->onScreenMoveable.entries())
	{
		MapDisplayIter list = map->onScreenMoveable;

		while(++list)
		{
			MapDisplayObject* dob = list.current();

			if(dob->ob->obType == OT_WaterZone)
			{
				SDimension d = distance(dob->screenPos.x - where.x, dob->screenPos.y - where.y);

				if( (!best || (d < dist)) && (d < 64))
				{
					dist = d;
					best = (WaterZone*)dob->ob;
				}
			}
		}
	}

	/*
	 * Check for apropriate type for fleet
	 */

	if(best)
	{
		// WaterZoneType type = best->getType();
		WaterZoneType type = best->type;

		if(type == WZ_Sea)
		{
			if(fleet.count[Nav_Riverine] || fleet.count[Nav_Monitor])
				best = 0;
		}
		else if(type == WZ_River)
		{
			if(fleet.count[Nav_NavalUnit] || fleet.count[Nav_NavalIronClad])
				best = 0;
		}
	}

	return best;
}

class WaterRouteShow : public TemporaryDisplay {
public:
	WaterRouteShow(TrackUnit* parent, const Location& l) : TemporaryDisplay(parent, l) { }

	void mapDraw(MapWindow* map, Region* bm, Point where);
};

void WaterRouteShow::mapDraw(MapWindow* map, Region* bm, Point where)
{
	if(control->onMap)
	{
		/*
	 	 * Display route
	 	 */

		if((control->fromWater != NoWaterZone) && 
		   (control->toWater != NoWaterZone) &&
			(control->fromWater != control->toWater) &&
			control->destWaterZone &&
			(control->routeHow != TrackUnit::RL_None))
		{
			control->waterRoute.drawRoute(map, bm, control->fromWater, control->toWater);
		}
	}
}


/*
 * Close it down
 */

void TrackUnit::closeDown()
{
	if(campaign->subMenu)
		campaign->subMenu->kill();
	waitMode = WaitUnit;
	setPointer(oldPointer);
	forceUpdate = True;
	campaign->showEmptyWaterZones = False;

	if(startObject)
	{
		delete startObject;
		startObject = 0;
	}

	if(endObject)
	{
		delete endObject;
		endObject = 0;
	}

	if(destWaterZone)
	{
		destWaterZone->highlight = 0;
		destWaterZone = 0;
	}

	campaign->updateMap();		// Force a redraw
}

/*==================================================================
 * Set up for movement mode
 */

void TrackUnit::setMoveMode()
{
	/*
 	 * Remove any existing highlight objects
 	 */

	if(startObject)
	{
		delete startObject;
		startObject = 0;
	}

	if(endObject)
	{
		delete endObject;
		endObject = 0;
	}

	startLocation = NoFacility;
	endLocation = NoFacility;

	/*
	 * Set up for appropriate movement mode
	 */

	switch(currentHow)
	{
	case ORDER_Land:
	case ORDER_Any:
		startLandMode();
		break;
	case ORDER_Rail:
		startRailMode();
		break;
	case ORDER_Water:
		startWaterMode();
		break;
	}
}

/*
 * Initiate rail movement temporary objects
 */

class TransportStart : public TemporaryDisplay {
public:
	TransportStart(TrackUnit* parent, const Location& l) : TemporaryDisplay(parent, l) { }

	void mapDraw(MapWindow* map, Region* bm, Point where);
};

class TransportEnd : public TemporaryDisplay {
public:
	TransportEnd(TrackUnit* parent, const Location& l) : TemporaryDisplay(parent, l) { }

	void mapDraw(MapWindow* map, Region* bm, Point where);

};

void TransportStart::mapDraw(MapWindow* map, Region* bm, Point where)
{
	if(control->onMap)
	{
		/*
		 * where is the location of the railhead or landing stage
		 */

		bm->frame(where.x - 2, where.y - 2, 5, 5, White);

		/*
	 	 * Highlight the railhead/landingstage and draw a line to the unit
	 	 */

		if(control->currentObject)
		{
			Point dest = map->locationToPixel(control->currentObject->location);

			bm->line(dest, where, Colours(LRed));


			if(control->currentHow == ORDER_Water)
			{
				if(control->fromWater != NoWaterZone)
				{
		 			WaterZone* zone = &campaign->world->waterNet.zones[control->fromWater];
		 			Point zcord = map->locationToPixel(zone->location);
		 			bm->line(where, zcord, LRed);
				}
			}

		}
	}
}

void TransportEnd::mapDraw(MapWindow* map, Region* bm, Point where)
{
	if(control->onMap)
	{
		Colour col;

		if(control->routeHow == TrackUnit::RL_None)
			col = Black;
		else if(control->routeHow == TrackUnit::RL_Friend)
			col = White;
		else
			col = LRed;

		/*
	 	 * where is the location of the mouse
	 	 */

		bm->line(where - Point(0,4), where + Point(0,4), Colours(LRed));
		bm->line(where - Point(4,0), where + Point(4,0), Colours(LRed));

		/*
	 	 * Highlight the railhead/landing stage and draw a line to the unit
	 	 */

		if(control->endLocation != NoFacility)
		{
			Facility* f = campaign->world->facilities.get(control->endLocation);

			Point dest = map->locationToPixel(f->location);

			bm->frame(dest.x - 2, dest.y - 2, 5, 5, col);
			bm->line(dest, where, Colours(col));

			/*
		 	 * Draw the route
		 	 */

			if(control->currentHow == ORDER_Rail)
			{
				if(control->routeHow != TrackUnit::RL_None)
					control->railInfo.drawRoute(map, bm, campaign->world->facilities, control->startLocation, control->endLocation);
			}
			else if(control->currentHow == ORDER_Water)
			{
				if(control->routeHow != TrackUnit::RL_None)
				{
					if(control->toWater != NoWaterZone)
					{
						WaterZone* zone = &campaign->world->waterNet.zones[control->toWater];
						Point zcord = map->locationToPixel(zone->location);
						bm->line(dest, zcord, col);
					}

					control->waterRoute.drawRoute(map, bm, control->fromWater, control->toWater);
				}
			}
		}
	}
}

/*
 * Find a close railhead
 *
 * If there isn't a friendly one within 100 miles, then
 * any railhead is returned.
 */

FacilityID findCloseRailhead(const Location& l, Side side)
{
	FacilityID best = NoFacility;
	Distance dist = 0;

	int pass = 0;

	do
	{
		FacilityID id = 0;
		FacilityIter iter = campaign->world->facilities;
		while(++iter)
		{
			Facility* f = iter.current();

			if(f->railCapacity && (pass || !onOtherSide(side, f->side)))
			{
				Distance d = distance(l.x - f->location.x, l.y - f->location.y);

				if((d < Mile(100)) && ((best == NoFacility) || (d < dist)))
				{
					best = id;
					dist = d;
				}
			}
			id++;
		}
	} while(!pass++ && (best == NoFacility));

	return best;
}

FacilityID findCloseRailJunction(const Location& l)
{
	FacilityID best = NoFacility;
	Distance dist = 0;

	FacilityID id = 0;
	FacilityIter iter = campaign->world->facilities;
	while(++iter)
	{
		Facility* f = iter.current();

		if(f->railCount && f->isRealTown())
		{
			Distance d = distance(l.x - f->location.x, l.y - f->location.y);

			if((best == NoFacility) || (d < dist))
			{
				best = id;
				dist = d;
			}
		}
		id++;
	}

	return best;
}

void TrackUnit::startRailMode()
{
	if(currentObject && (currentObject->obType == OT_Unit))
	{
		Unit* currentUnit = (Unit*) currentObject;

		startLocation = findCloseRailhead(currentUnit->location, currentUnit->getSide());

		if(startLocation != NoFacility)
		{
			Facility* f = campaign->world->facilities.get(startLocation);
			startObject = new TransportStart(this, f->location);
		}

		endObject = new TransportEnd(this, currentUnit->location);

		campaign->updateMap();		// Force a redraw
	}
}

void TrackUnit::startLandMode()
{
	if(currentObject && (currentObject->obType == OT_Unit))
	{
		Unit* currentUnit = (Unit*) currentObject;

		startObject = new TransportStart(this, currentUnit->givenOrders.destination);

		campaign->updateMap();		// Force a redraw
	}
}

/*==================================================================
 * Ferry by water mode
 */


FacilityID findCloseLandingStage(const Location& l, Side side)
{
	FacilityID best = NoFacility;
	Distance dist = 0;

	FacilityID id = 0;
	FacilityIter iter = campaign->world->facilities;
	while(++iter)
	{
		Facility* f = iter.current();

		if(f->waterCapacity && !onOtherSide(side, f->side))
		{
			Distance d = distance(l.x - f->location.x, l.y - f->location.y);

			if((best == NoFacility) || (d < dist))
			{
				best = id;
				dist = d;
			}
		}
		id++;
	}

	return best;
}

FacilityID findCloseWaterFacility(const Location& l)
{
	FacilityID best = NoFacility;
	Distance dist = 0;

	FacilityID id = 0;
	FacilityIter iter = campaign->world->facilities;
	while(++iter)
	{
		Facility* f = iter.current();

		if(f->waterZone != NoWaterZone)
		{
			Distance d = distance(l.x - f->location.x, l.y - f->location.y);

			if((best == NoFacility) || (d < dist))
			{
				best = id;
				dist = d;
			}
		}
		id++;
	}

	return best;
}

void TrackUnit::startWaterMode()
{
	if(currentObject && (currentObject->obType == OT_Unit))
	{
		Unit* currentUnit = (Unit*) currentObject;

		startLocation = findCloseLandingStage(currentUnit->location, currentUnit->getSide());
		
		if(startLocation == NoFacility)
		{
			fromWater = NoWaterZone;
		}
		else
		{

			Facility* f = campaign->world->facilities.get(startLocation);
		
			fromWater = f->waterZone;

			if(startLocation != NoFacility)
			{
				Facility* f = campaign->world->facilities.get(startLocation);
				startObject = new TransportStart(this, f->location);
			}

			endObject = new TransportEnd(this, currentUnit->location);
		}
	 	campaign->updateMap();		// Force a redraw
	}
}

/*==================================================================
 * Track Unit Setup
 */

TrackUnit::TrackUnit(CampaignMode* p) : CampaignFunction(p)
{
	currentObject = 0;
	waitMode = WaitUnit;
	forceUpdate = True;
	highlightTimer = 0;

	startObject = 0;
	endObject = 0;
	startLocation = NoFacility;
	endLocation = NoFacility;

	destWaterZone = 0;
	fromWater = NoWaterZone;
	toWater = NoWaterZone;

	onMap = False;
	mouseMoved = True;					// Force initial tracking
	previousMouse = Point(-1,-1);		// Illegal value to force update
}

/*
 * Remove Track Unit
 */

TrackUnit::~TrackUnit()
{
	if(waitMode == WaitOrder)
		closeDown();

	if(currentObject)
	{
		currentObject->highlight = 0;
	 	campaign->updateMap();		// Force a redraw
	}
}


void TrackUnit::setupOrders()
{
	if(currentObject)
		if(currentObject->obType == OT_Unit)
		{
			Unit* currentUnit = (Unit*) currentObject;

			currentMode = currentUnit->givenOrders.mode;

			if(currentUnit->givenOrders.basicOrder() == ORDERMODE_Hold)
				currentHow = ORDER_Any;
			else
				currentHow = currentUnit->givenOrders.how;
			setMoveMode();

			waitMode = WaitOrder;

			IconList* icons = new IconList[15];

			icons[0]  = new C_AdvanceIcon			(this, campaign->subMenu);
			icons[1]  = new C_CautiousIcon		(this, campaign->subMenu);
			icons[2]  = new C_AttackIcon			(this, campaign->subMenu);
			icons[3]  = new C_StandIcon			(this, campaign->subMenu);
			icons[4]  = new C_HoldIcon				(this, campaign->subMenu);
			icons[5]  = new C_DefendIcon			(this, campaign->subMenu);
			icons[6]  = new C_WithdrawIcon		(this, campaign->subMenu);
			icons[7]  = new C_RetreatIcon			(this, campaign->subMenu);
			icons[8]  = new C_HastyRetreatIcon	(this, campaign->subMenu);
			icons[9]	 = new C_AnyIcon				(this, campaign->subMenu);
			icons[10] = new C_LandIcon				(this, campaign->subMenu);
			icons[11] = new C_RailIcon				(this, campaign->subMenu);
			icons[12] = new C_WaterIcon			(this, campaign->subMenu);
			icons[13] = new C_MiniOK				(this, campaign->subMenu);
			icons[14] = 0;

			campaign->subMenu->add(icons);

			oldPointer = getPointer();
			setPointer(M_WhereTo);

			campaign->updateMap();		// Force a redraw
		}
		else if(currentObject->obType == OT_WaterZone)
		{
			WaterZone* zone = (WaterZone*)currentObject;

			fleet.clear();
			destWaterZone = 0;
			fromWater = zone->getID(&campaign->world->waterNet);
			// fromWater = campaign->world->waterNet.zones.getID(zone);
			toWater = NoWaterZone;
			campaign->showEmptyWaterZones = True;

			waitMode = WaitOrder;

			IconList* icons = new IconList[6];
			icons[0] = new NavalMoveIcon(this, campaign->subMenu, Nav_NavalUnit, 0);
			icons[1] = new NavalMoveIcon(this, campaign->subMenu, Nav_NavalIronClad, 78);
			icons[2] = new NavalMoveIcon(this, campaign->subMenu, Nav_Riverine, 78*2);
			icons[3] = new NavalMoveIcon(this, campaign->subMenu, Nav_RiverineIronClad, 78*3);
			icons[4] = new C_MiniOK(this, campaign->subMenu);
			icons[5] = 0;

			campaign->subMenu->add(icons);

			endObject = new WaterRouteShow(this, currentObject->location);

			oldPointer = getPointer();
			setPointer(M_Arrow);

			campaign->updateMap();		// Force a redraw
		}
}

/*
 * Network packets
 */

struct CampaignOrderPacket {
	UnitID unit;						// Who has been ordered?
	Order order;
};

struct CampaignNavalPacket {
	Flotilla flotilla;
	Side side;
	WaterZoneID fromWater;
	WaterZoneID toWater;
	TimeBase when;
};

void Campaign::rxOrder(UBYTE* packet)
{
	CampaignOrderPacket* pkt = (CampaignOrderPacket*) packet;

	Unit* u = world->ob.getUnitPtr(pkt->unit);

#ifdef DEBUG
	netLog.printf("Order for %s (%s)", u->getName(True), getOrderStr(pkt->order));
#endif

	u->sendOrder(pkt->order);
}


void Campaign::rxFleet(UBYTE* packet)
{
	CampaignNavalPacket* pkt = (CampaignNavalPacket*) packet;

	Fleet *newFleet = new Fleet(pkt->flotilla, pkt->side);

#ifdef DEBUG
	netLog.printf("Order for fleet received [%d,%d,%d,%d] %d->%d",
		(int) pkt->flotilla.count[0],
		(int) pkt->flotilla.count[1],
		(int) pkt->flotilla.count[2],
		(int) pkt->flotilla.count[3],
		(int) pkt->fromWater,
		(int) pkt->toWater);
#endif

	WaterZone* zone = &world->waterNet.zones[pkt->fromWater];
	zone->boats -= pkt->flotilla;

	world->waterNet.sendFleet(newFleet, pkt->fromWater, pkt->toWater, pkt->when);
}

void TrackUnit::sendOrder(const Location& l)
{
	if(currentObject)
	{
		if(currentObject->obType == OT_Unit)
		{
			if ( campaign->world->CheckLocalDest( l ) ) 
			{
				dialAlert( 0, language( lge_InvalidDest  ), language( lge_OK ) );
				return;
			}

#ifdef DEBUG
			if(optQuickMove())
			{
				Unit* u = (Unit*) currentObject;
				u->setLocation(l);

				return;
			}
#endif

			Unit* currentUnit = (Unit*) currentObject;
	
			if(currentHow == ORDER_Water)
			{
				if(toWater == NoWaterZone)
					return;
			}


			// SentOrder* order = new SentOrder(currentMode, currentHow, l, campaign->gameTime);
			Order order(currentMode, currentHow, l);

			if( (currentHow == ORDER_Rail) || (currentHow == ORDER_Water) )
			{
				if((startLocation == NoFacility) || (endLocation == NoFacility))
					return;

				order.onFacility = startLocation;
				order.offFacility = endLocation;
			}
			else if(currentHow == ORDER_Any)
				campaign->world->getBestRoute(currentUnit->location, order.destination, order, currentUnit->getSide());
		 
			CampaignOrderPacket* pkt = new CampaignOrderPacket;
			pkt->unit = campaign->world->ob.getID(currentUnit);
			pkt->order = order;
			game->connection->sendMessage(Connect_CS_Order, (UBYTE*)pkt, sizeof(CampaignOrderPacket));

#ifdef DEBUG
			netLog.printf("Sent Order for %s (%s)", currentUnit->getName(True), getOrderStr(order));
#endif

#ifdef COMMS_OLD
			currentUnit->sendOrder(order);
		 	campaign->updateMap();		// Force a redraw
#endif
		}
		else if(currentObject->obType == OT_WaterZone)
		{
			if(fleet.totalBoats() && destWaterZone)
			{
			 	WaterZone* zone = (WaterZone*)currentObject;

#ifdef COMMS_OLD
				/*
				 * Remove boats from zone
				 */

				zone->boats -= fleet;

				/*
				 * Add fleet to zone
				 */

				Fleet* newFleet = new Fleet(fleet, zone->side);

				campaign->world->waterNet.sendFleet(newFleet, fromWater, toWater, campaign->gameTime);
#endif

				CampaignNavalPacket* pkt = new CampaignNavalPacket;

				pkt->flotilla = fleet;
				pkt->side = zone->side;
				pkt->fromWater = fromWater;
				pkt->toWater = toWater;
				pkt->when = campaign->gameTime;

				game->connection->sendMessage(Connect_CS_SendFleet, (UBYTE*)pkt, sizeof(CampaignNavalPacket));

#ifdef DEBUG
				netLog.printf("Sent Fleet [%d,%d,%d,%d] %d->%d",
					(int) fleet.count[0],
					(int) fleet.count[1],
					(int) fleet.count[2],
					(int) fleet.count[3],
					(int) fromWater,
					(int) toWater);
#endif

#ifdef COMMS_OLD
#ifdef DEBUG
				TimeInfo tm = newFleet->arrival;

				campaign->msgArea->printf("Fleet [%d,%d,%d,%d] moving from zone %d to %d\rArrival at %d at %d:%02d:%02d",
					(int)fleet.count[0],
					(int)fleet.count[1],
					(int)fleet.count[2],
					(int)fleet.count[3],
					(int)fromWater,
					(int)toWater,
					(int)viaWater,
					tm.hour, tm.minute, tm.second);
#endif
#endif	// COMMS_OLD
	 			campaign->updateMap();		// Force a redraw
			}
		}
	}
}

/*
 * Choose which object to use if there are several stacked up.
 */

void TrackUnit::chooseStackedObject()
{
	if(objectList.entries() > 1)
	{
#if 0
		int count = objectList.entries();
		if(count > 10)
			count = 10;
#else
		int count = 0;
#endif

		char** choices = new char*[objectList.entries() + 1];
		// MemPtr<const char*> choices(count + 1);
		char** choice = choices;

		PtrDListIter<MapObject> iter = objectList;

		// while(count-- && ++iter)
		while((count < 10) && ++iter)
		{
			MapObject* ob = iter.current();

			if(ob->obType == OT_Unit)
			{
				Unit* u = (Unit*) ob;

				if((u->getCampaignAttachMode() != Attached) || u->highlight)
				{
					const char* name = u->getName(True);
					char* buf = new char[strlen(name) + 10];

					sprintf(buf, "%s (%ld)", name, u->getDependantStrength());

					*choice++ = buf;
					count++;
				}
			}
			else if(ob->obType == OT_WaterZone)
			{
				WaterZone* z = (WaterZone*) ob;

				if(z->boats.totalBoats())
				{
					*choice++ = copyString(language(lge_OrderNavalUnit));
					count++;
				}
			}
		}

		*choice++ = 0;
		
		int result;

		if(count > 1)
			result = chooseFromList(getMenuData(), 0, (const char**) choices, 0);
		else
			result = 1;

		choice = choices;
		while(*choice)
		{
			delete[] *choice;
			*choice++ = 0;
		}

		delete[] choices;

		if(result > 0)
		{
			if(currentObject)
				currentObject->highlight = 0;
			currentObject = objectList.find(result - 1);
			currentObject->highlight = 1;
			forceUpdate = True;
			campaign->updateMap();		// Force a redraw
		}
	}
}

/*
 * Get Info about map object needed for prioritizing
 */

struct MapObPriorityInfo {
	Unit* u;
	WaterZone* z;
	Side sd;

	void set(MapObject* ob);
};

void MapObPriorityInfo::set(MapObject* ob)
{
	u = 0;
	z = 0;
	
	if(ob->obType == OT_Unit)
	{
		u = (Unit*) ob;
		sd = u->getSide();
	}
	else if(ob->obType == OT_WaterZone)
	{
		z = (WaterZone*) ob;
		sd = z->side;
	}
	else
		sd = SIDE_None;
}

/*
 * Function called when mouse is over map
 */

void TrackUnit::overMap(MapWindow* map, Event* event, MenuData* d, const Location* l)
{
	onMap = True;

	if(event->where != previousMouse)
	{
		mouseMoved = True;
		previousMouse = event->where;
	}

	if(campaign->newDay)
		forceUpdate = True;

	if(waitMode == WaitUnit)
	{
		d->setTempPointer(M_TrackUnit);

		/*
		 * Build list of close objects
		 * keep list sorted according to side, rank and type
		 */

		/*
		 * If a current Unit has been selected (e.g. from CAL)
		 * then ensure that keep it as the current object
		 */

		if(highlightTimer)
		{
			if(timer->getCount() >= highlightTimer)
			{
				highlightTimer = 0;
				mouseMoved = True;
			}
			objectList.clear();
		}

		/*
		 * If the mouse has not moved then we can use the list from
		 * last time.
		 */

		else if(mouseMoved)
		{
			objectList.clear();		// Prepare to start a new list

			if(map->onScreenMoveable.entries())
			{
				MapDisplayIter list = map->onScreenMoveable;
				SDimension dist = 0;
				MapDisplayObject* best = 0;

				while(++list)
				{
					MapDisplayObject* dob = list.current();
					MapPriorityObject* pob = list.currentPlist();

					if( (dob->ob->obType == OT_Unit) ||
						 (dob->ob->obType == OT_WaterZone) )
					{
						SDimension d = distance(dob->screenPos.x - event->where.x,
														dob->screenPos.y - event->where.y);

						if(d < 32)		// Must be fairly close!
						{
							/*
							 * Now things get a bit complex...
							 * If this is close to best, then add it to the list
							 * otherwise if it closer than best, restart the list
							 * else throw it away
							 */

							if(!best)
							{
								best = dob;
								dist = d;
								objectList.append(dob->ob);
							}
							else
							{
								/*
								 * Is it close to the current best?
								 */

								Distance d1 = distance(dob->screenPos.x - best->screenPos.x,
															  dob->screenPos.y - best->screenPos.y);

								if(d1 < 8)
								{
									if(d < dist)
									{
										dist = d;
										best = dob;
									}

									// Insert into list in a sorted way!

									if(objectList.entries() == 0)
										objectList.append(dob->ob);
									else
									{
										MapObPriorityInfo mop;
										mop.set(dob->ob);

										MapObPriorityInfo mop1;
										mop1.set(objectList.find());

										if(mop.sd != mop1.sd)
										{
											// if(mop.sd == game->playersSide)
											if(game->isPlayer(mop.sd))
											{
												objectList.clear();
												objectList.append(dob->ob);
											}
											// Else ignore...
										}
										else
										{	// Insert into list based on rank
											Boolean inserted = False;
											PtrDListIter<MapObject> iter = objectList;
											while(++iter)
											{
												MapObject* ob = iter.current();
												mop1.set(ob);

												if(mop.u && !mop1.u)
												{	// Units before boats
													iter.insert(dob->ob);
													inserted = True;
													break;
												}
												else if(mop.u && mop1.u &&
														  (mop.u->getRank() < mop1.u->getRank()) )
												{
													iter.insert(dob->ob);
													inserted = True;
													break;
												}
											}

											if(!inserted)
												objectList.append(dob->ob);
										}
									}
								}
								else if(d < dist)
								{
									dist = d;
									best = dob;
									objectList.clear();
									objectList.append(dob->ob);
								}
							}
						}
					}
				}

				/*
				 * At this point we should have:
				 *   best as the closest object
				 *   objectList being a list of objects close to closest object
				 *   sorted by rank!
				 */

				if(best)
				{
					if(currentObject != objectList.find())	// 1st item
					{
						if(currentObject)
							currentObject->highlight = 0;
						currentObject = objectList.find();
						currentObject->highlight = 1;
						forceUpdate = True;
						campaign->updateMap();
					}
				}
				else if(currentObject || forceUpdate)
				{
					forceUpdate = False;
					if(currentObject)
					{
						currentObject->highlight = 0;
						currentObject = 0;
						campaign->updateMap();
					}
					campaign->clearInfo();
				}
			}
		}
		
		if(forceUpdate)
		{
			forceUpdate = False;

			if(currentObject)
				currentObject->showCampaignInfo(campaign->infoArea);
		}

	}
	else	// In WaitOrder mode and railway
	{
		if(currentObject && (currentObject->obType == OT_Unit))
		{
			Unit* currentUnit = (Unit*)currentObject;
			Side obSide = currentUnit->getSide();

			if(currentHow == ORDER_Rail)
			{
				/*
				 * Track for destination railhead
				 */

				d->setTempPointer(M_MoveRail);		// could be rail icon

				/*
				 * Find closest railhead to mouse and highlight it
				 */

				FacilityID close = findCloseRailJunction(*l);

				if(close != endLocation)
				{
					endLocation = close;

					if( (close == NoFacility) || (startLocation == endLocation))
						routeHow = RL_None;
					else
					{
						// FacilityID routeDir = campaign->world->findRailRoute(startLocation, endLocation, game->playersSide, &railInfo);
						FacilityID routeDir = campaign->world->findRailRoute(startLocation, endLocation, obSide, &railInfo);
						if(routeDir == NoFacility)
						{
							routeDir = campaign->world->findRailRoute(startLocation, endLocation, SIDE_None, &railInfo);
							if(routeDir == NoFacility)
								routeHow = RL_None;
							else
								routeHow = RL_Enemy;
						}
						else
							routeHow = RL_Friend;
					}
				}
				endObject->location = *l;
				campaign->updateMap();		// Force a redraw
			}
			else if(currentHow == ORDER_Water)
			{
				if(fromWater != NoWaterZone)
				{
					/*
				 	 * Track waterways
				 	 */

					d->setTempPointer(M_MoveWater);		// could be rail icon
		 
					/*
				 	 * Find closest landingstage to mouse and highlight it
				 	 */

					FacilityID close = findCloseWaterFacility(*l);

					if(close != endLocation)
					{
						endLocation = close;

						if( (close == NoFacility) || (startLocation == endLocation))
						{
							routeHow = RL_None;
							toWater = NoWaterZone;
						}
						else
						{
							Facility* f = campaign->world->facilities[endLocation];
							toWater = f->waterZone;

							if(toWater == NoWaterZone)
								routeHow = RL_None;
							else
							{
								FacilityID routeDir = campaign->world->waterNet.findRoute(fromWater, toWater, False, 0, &waterRoute);
								if(routeDir == NoFacility)
								{
									FacilityID routeDir = campaign->world->waterNet.findRoute(fromWater, toWater, True, 0, &waterRoute);
									if(routeDir == NoFacility)
										routeHow = RL_None;
									else
										routeHow = RL_Enemy;
								}
								else
									routeHow = RL_Friend;
							}
						}
					}
					endObject->location = *l;
					campaign->updateMap();		// Force a redraw
				}
			}
			else	// Land or Any
			{
				if(startObject->location != *l)
				{
					startObject->location = *l;
					campaign->updateMap();		// Force a redraw
				}
			}
		}
		else if(currentObject && (currentObject->obType == OT_WaterZone))
		{
			/*
			 * If we've selected any boats to move
			 */

			if(fleet.totalBoats())
			{
				/*
				 * Track for any waterzone
				 */

				WaterZone* zone = findCloseWaterZone(map, event->where, fleet);

				if( (zone != destWaterZone) && (zone != currentObject) )
				{
					
					if(zone)
					{
						WaterZoneID destID;

						destID = zone->getID(&campaign->world->waterNet);
						WaterZoneID routeDir = campaign->world->waterNet.findRoute(fromWater, destID, False, &fleet, &waterRoute);
						if(routeDir == NoWaterZone)
						{
							routeDir = campaign->world->waterNet.findRoute(fromWater, destID, True, &fleet, &waterRoute);
							if(routeDir == NoFacility)
								routeHow = RL_None;
							else
								routeHow = RL_Enemy;
						}
						else
							routeHow = RL_Friend;

						if(routeDir == NoWaterZone)
						{
							zone = 0;
							toWater = NoWaterZone;
						}
						else
						{
							toWater = destID;
							viaWater = routeDir;
						}
					}
					else
						toWater = NoWaterZone;


					if(destWaterZone)
						destWaterZone->highlight = 0;
					if(zone)
						zone->highlight = 1;
					destWaterZone = zone;
					campaign->updateMap();		// Force a redraw
				}

				d->setTempPointer(M_MoveWater);		// could be rail icon
			}
		}
	}

	mouseMoved = False;

	/*
	 * Do things if buttons pressed
	 */

	/*
	 * Right buttons:
	 *		Cancel the order if in order mode
	 *    Goes to CAL if in track mode
	 */

	if(event->buttons & Mouse::RightButton)
	{
		if(waitMode == WaitOrder)
			closeDown();
		else if(waitMode == WaitUnit)
		{
			if(objectList.entries() > 1)
				chooseStackedObject();			// This sets currentObject

			if(!currentObject || (currentObject->obType != OT_Unit))
				return;

			Unit* currentUnit = 0;
			if(currentObject && (currentObject->obType == OT_Unit))
			{
				currentUnit = (Unit*)currentObject;
				campaign->updateMap();		// Force a redraw
			}

			// Prevent player from viewing other side's CAL

#ifdef DEBUG
			// if(seeAll || (currentUnit->getSide() == campaign->world->getPlayersSide()))
			if(seeAll || game->isPlayer(currentUnit->getSide()))
#else
			// if(currentUnit->getSide() == campaign->world->getPlayersSide())
			if(game->isPlayer(currentUnit->getSide()))
#endif
			{
		
				/*
				 * Call CAL and centre on the returned unit
				 *
				 * CAL can return in 2 modes:
				 *		Find (centre on unit, but leave in tracking mode)
				 *		Order (Centre on unit and go into order mode)
				 */

				int giveOrders;
				Unit* newUnit = doCampaignCAL(&campaign->world->ob, currentUnit, &giveOrders, d);
				d->setUpdate();

				if(newUnit)
					setCurrentUnit(newUnit, giveOrders);
			}
		}
	}
	else 

	/*
	 * Left button pulls up mobilise icons
	 */

	if(event->buttons & Mouse::LeftButton)
	{
		if((waitMode == WaitUnit) && (objectList.entries() > 1))
			chooseStackedObject();			// This sets currentObject

		/*
		 * Initial click brings up submenu
		 */

		Boolean canControl = False;

		if(currentObject && (currentObject->obType == OT_Unit))
		{
			Unit* currentUnit = (Unit*)currentObject;

#ifdef DEBUG
			// canControl = seeAll || (currentUnit->getSide() == campaign->world->getPlayersSide());
			canControl = seeAll || game->isPlayer(currentUnit->getSide());
#else
			// canControl = (currentUnit->getSide() == campaign->world->getPlayersSide());
			canControl = game->isPlayer(currentUnit->getSide());
#endif

			if( (currentUnit->moveMode == MoveMode_Transport) &&
				 (currentUnit->getCampaignAttachMode() == Attached) )
			{
				canControl = False;
			}
		}
		else if ( currentObject && (currentObject->obType == OT_WaterZone) )
		{
			WaterZone* zone = (WaterZone*)currentObject;

#ifdef DEBUG
			// canControl = seeAll || (zone->side == campaign->world->getPlayersSide());
			canControl = seeAll || game->isPlayer(zone->side);
#else
			// canControl = (zone->side == campaign->world->getPlayersSide());
			canControl = game->isPlayer(zone->side);
#endif
		}

		if(canControl)
		{
			if(waitMode == WaitUnit)
			{
				if(currentObject)
					setupOrders();
			}

			/*
			 * 2nd click sends orders
			 */

			else if(waitMode == WaitOrder)
			{
				if ( currentObject && currentObject->obType == OT_Unit && campaign->world->CheckLocalDest( *l ) ) 
				{
	 				dialAlert( 0, language( lge_InvalidDest  ), language( lge_OK ) );
 					return;
				}

				/*
				 * Force Advance order if click on map with hold order
				 */

				if(getBasicOrder(currentMode) == ORDERMODE_Hold)
					currentMode = ORDER_Advance;

				sendOrder(*l);
				closeDown();
			}
		}
	}
}

void TrackUnit::offMap()
{
	if( (waitMode == WaitUnit) && (forceUpdate || currentObject))
	{
		forceUpdate = False;
		if(currentObject)
		{
			currentObject->highlight = 0;
			currentObject = 0;

			campaign->updateMap();		// Force a redraw
		}
		campaign->clearInfo();
	}

	if(onMap)
	{
		onMap = False;
	 	campaign->updateMap();		// Force a redraw
	}
}

#if 0	// Unused
FacilityID FindBestTrainRoute( FacilityID on, Location dest, int loop, FacilityID comeFrom )
{
  	Facility* f = campaign->world->facilities.get( on );

	if ( loop++ > 5 ) return NoFacility;

	Distance d;

	Facility* bestf = 0;


	FacilityID bid;

	int count = f->railCount;

	if( count )
	{
		RailSectionID rCount = f->railSection;

		while ( count-- )
		{
			FacilityID connection = campaign->world->railways.get(rCount++);
			
			if ( connection == comeFrom || connection == NoFacility ) continue;

			Facility* cf = campaign->world->facilities.get(connection);

			if ( cf->railCount < 2 && f->railCount != 1 ) continue;

			if ( !bestf || (d = distanceLocation( cf->location, dest ) ) < distanceLocation( bestf->location, dest ) )
			{
				bestf = cf;

				bid = connection;

				if ( d < Mile( 3 ) ) return bid;
			}
		}
	}

 	if ( bestf ) 
	{
		FacilityID newbid = FindBestTrainRoute( bid, dest, loop, on );
	
		if ( newbid != NoFacility )
		{
			Facility* newcf = campaign->world->facilities.get(newbid);

			if ( newcf || distanceLocation( newcf->location, dest ) < distanceLocation( bestf->location, dest ) )
			{
				bestf = newcf;

				bid = newbid;
			}
		}

		return bid;
	}

	return NoFacility;
}
#endif


void TrackUnit::setCurrentUnit(Unit* unit, Boolean giveOrders)
{
	if(unit && (unit != currentObject))
	{
		if(currentObject)
			currentObject->highlight = 0;

		currentObject = unit;
		currentObject->highlight = 1;
		forceUpdate = True;

		campaign->mapArea->setStyle(MapWindow::Zoomed, unit->location);
		campaign->updateMap();		// Force a redraw
	}
	 
	highlightTimer = timer->getCount() + Timer::ticksPerSecond * 4;

	if(giveOrders)
		setupOrders();
}

