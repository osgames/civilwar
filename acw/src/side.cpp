/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Things to do with what side things are on
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "side.h"

#if 0
Side indexToSideTable[SideCount] = { 1 << 0, 1 << 1 };
SideIndex sideToIndexTable[1 << SideCount] = { 0, 0, 1, 0 };
#elif 0
SideEnum indexToSideTable[SideCount] = { 1 << 0, 1 << 1 };
ISideEnum sideToIndexTable[1 << SideCount] = { 0, 0, 1, 0 };
#else
Side indexToSideTable[SideCount] = { SIDE_USA, SIDE_CSA };
SideIndex sideToIndexTable[1 << SideCount] = { SideI_USA, SideI_USA, SideI_CSA, SideI_USA };
#endif
