/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Test ILBM reader
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.7  1994/06/29  21:38:52  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/06/22  20:07:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/06/21  18:49:27  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1993/12/21  00:31:04  Steven_Green
 * Error traps added.
 *
 * Chunk size rounded up to multiple of 2.
 *
 * Revision 1.3  1993/12/04  16:22:51  Steven_Green
 * Exception catching added.
 *
 * Revision 1.2  1993/12/04  01:06:10  Steven_Green
 * Loads in LBM screen.
 * Uses new palette class.
 *
 * Revision 1.1  1993/12/03  16:45:51  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <iostream.h>
#include <conio.h>

#include "ilbm.h"
#include "image.h"
#include "palette.h"
#include "vesa.h"

void main(int argc, char *argv[])
{
	if(argc != 2)
	{
		cout << "Usage: testilbm filename" << endl;
		return;
	}

	try
	{
		Palette myPalette;
		Image myImage;

		if(readILBM(argv[1], &myImage, &myPalette))
			cout << "readILBM returned an error" << endl;
		else
		{
			Vesa screen(0x100, 0x101);

			myPalette.set();

			screen.blit(&myImage);

			while(!kbhit());
		}
	}
	catch(Vesa::VesaError e)
	{
		cout << "Vesa Error: " << e.get() << endl;
	}
	catch(...)
	{
		cout << "An unexpected error happened" << endl;
	}

}
