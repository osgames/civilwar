/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Put Logion in a Window
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "logo.h"
#include "game.h"
#include "sprlib.h"
#include "gamelib.h"
#include "system.h"
#include "screen.h"

/*
 * Fill a bitmap with a pattern
 */

void putPlayerPattern(Region* region)
{
	SpriteIndex sNum;

	// if(game->playersSide == SIDE_USA)
	if(game->getLocalSide() == SIDE_USA)
		sNum = FillUSA_Grey;
	else
		sNum = FillCSA_Grey;

	SpriteBlock* fillPattern = game->sprites->read(sNum);
	region->fill(fillPattern);
	fillPattern->release();

	machine.screen->setUpdate(*region);
}




/*
 * Fill a bitmap with a pattern and logo
 */

void putLogo(Region* region)
{
	SpriteIndex sNum;

	// if(game->playersSide == SIDE_USA)
	if(game->getLocalSide() == SIDE_USA)
		sNum = FillUSA_Grey;
	else
		sNum = FillCSA_Grey;

	SpriteBlock* fillPattern = game->sprites->read(sNum);
	region->fill(fillPattern);
	fillPattern->release();

	// if(!icons)
		// game->sprites->drawCentredSprite(&r, LogoSprite);

	SpriteBlock* image = game->sprites->read(LogoSprite);
	region->maskBlit(image, Point(image->xOffset + (region->getW() - image->fullWidth) / 2, 64));
	image->release();

	machine.screen->setUpdate(*region);
}



