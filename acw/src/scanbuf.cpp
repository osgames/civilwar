/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Scan Line Z Buffering
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1994/08/31  15:23:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/08/19  17:29:00  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "scanbuf.h"
#include "data3d.h"

template<class T>
inline T intersect(SDimension x1, SDimension x2, T v1, T v2, SDimension xi)
{
	if(x1 == x2)			// Single point
		return v1;
	else if(xi == x1)		// Left edge
		return v1;
	else if(xi == x2)		// Right edge
		return v2;
	else						// General case
		return v1 + ((xi - x1) * (v2 - v1)) / (x2 - x1);
}


ScanBuffer::ScanBuffer()
{
	segments = 0;
	sections = 0;
	freeSegment = NoScanSegment;
	height = 0;
	paletteSet = False;
}

ScanBuffer::~ScanBuffer()
{
	delete[] sections;
	delete[] segments;
}

void ScanBuffer::init(SDimension h)
{
	height = h;

	if(!segments)
		segments = new ScanSegmentList[MaxScanSegments];

	if(sections)
		delete[] sections;
	sections = new UWORD[h];

	clear();
}

void ScanBuffer::clear()
{
	/*
	 * Add all the segments to the free Segment buffer
	 */

	for(UWORD i = 0; i < (MaxScanSegments - 1); i++)
	{
		segments[i].next = i + 1;
	}
	segments[i].next = NoScanSegment;
	freeSegment = 0;

	/*
	 * Set all scan lines to none
	 */

	for(i = 0; i < height; i++)
		sections[i] = NoScanSegment;

	paletteSet = False;
}

void ScanBuffer::add(SDimension y, ScanSegment* seg)
{
	/*
	 * For now just create a new segment...
	 * It ought to check for overlaps, Z ordering, etc
	 */

	if(y < height)
		putLineSeg(&sections[y], seg);
}

void ScanBuffer::putLineSeg(UWORD* info, ScanSegment* seg)
{
	/*
	 * Check any existing segments for overlap
	 */

	Boolean split;

	ScanSegmentList* oldSeg = 0;

	do
	{
		split = False;

		if(*info != NoScanSegment)
		{
			UWORD segID = *info;
			oldSeg = 0;

			while(segID != NoScanSegment)
			{
				ScanSegmentList* seg1 = &segments[segID];

				/*
				 * List is sorted, so if
				 * new segments right edge is less than left edge then we've finished
				 */

				if(seg->to.x < seg1->seg.from.x)
					break;

				if( (seg->from.x < seg1->seg.to.x) &&
				 	(seg->to.x > seg1->seg.from.x) )
				{
					/*
				 	 * They overlap...
				 	 */

					/*
				 	 * New segment inside old segment
					 * IF new segment is further away
					 *   then discard it
					 * otherwise split old segment into 2 surrounding it.
				 	 */

					if( (seg->from.x >= seg1->seg.from.x) && 
					 	 (seg->to.x <= seg1->seg.to.x) )
					{
						WORD z1 = intersect(
							seg1->seg.from.x, seg1->seg.to.x,
							seg1->seg.from.z, seg1->seg.to.z,
							seg->from.x);

						// WORD z1 = seg1->seg.from.z + ( (seg1->seg.to.z - seg1->seg.from.z) * (seg->from.x - seg1->seg.from.x) ) / (seg1->seg.to.x - seg1->seg.from.x);

						if(seg->from.z >= z1)
							return;

						/*
						 * If exactly overlaps then replace it
						 */

						if( (seg->from.x == seg1->seg.from.x) &&
							 (seg->to.x == seg1->seg.to.x) )
						{
							seg1->seg = *seg;
							return;
						}

						/*
						 * If left aligned
						 * Replace old segment with intersection
						 * and continue search
						 */

						if(seg->from.x == seg1->seg.from.x)
						{
							SDimension newX = seg->to.x + 1;

							seg1->seg.from.z = intersect(
								seg1->seg.from.x, seg1->seg.to.x,
								seg1->seg.from.z, seg1->seg.to.z,
								newX);
							seg1->seg.from.i = intersect(
								seg1->seg.from.x, seg1->seg.to.x,
								seg1->seg.from.i, seg1->seg.to.i,
								newX);
							seg1->seg.from.x = newX;
						}

						/*
						 * Right aligned?
						 */

						else
						if(seg->to.x == seg1->seg.to.x)
						{
							SDimension newX = seg->from.x - 1;

							seg1->seg.to.z = intersect(
								seg1->seg.from.x, seg1->seg.to.x,
								seg1->seg.from.z, seg1->seg.to.z,
								newX);
							seg1->seg.to.i = intersect(
								seg1->seg.from.x, seg1->seg.to.x,
								seg1->seg.from.i, seg1->seg.to.i,
								newX);
							seg1->seg.to.x = newX;
						}
						else
						{
							/*
							 * Old segment must be split into 2!
							 */

							// Right hand segment

							SDimension newX = seg->to.x + 1;

							ScanSegment newSeg;
							newSeg.material = seg1->seg.material;
							newSeg.from.x = newX;
							newSeg.from.z = intersect(
									seg1->seg.from.x, seg1->seg.to.x,
									seg1->seg.from.z, seg1->seg.to.z,
									newX);
							newSeg.from.i = intersect(
									seg1->seg.from.x, seg1->seg.to.x,
									seg1->seg.from.i, seg1->seg.to.i,
									newX);
							newSeg.to = seg1->seg.to;

							// Left segment

							newX = seg->from.x - 1;

							seg1->seg.to.x = newX;
							seg1->seg.to.z = intersect(
									seg1->seg.from.x, seg1->seg.to.x,
									seg1->seg.from.z, seg1->seg.to.z,
									newX);
							seg1->seg.to.i = intersect(
									seg1->seg.from.x, seg1->seg.to.x,
									seg1->seg.from.i, seg1->seg.to.i,
									newX);

							putLineSegQuick(info, &newSeg, oldSeg);
						}
					}

					/*
				 	 * Old segment inside new segment
					 * IF new segment is nearer
				 	 *   then remove old segment
					 * ELSE
					 *   split new segment into 2
				 	 */

					else
					if( (seg->from.x <= seg1->seg.from.x) &&
				    	(seg->to.x >= seg1->seg.to.x) )
					{
						WORD z1 = intersect(
							seg->from.x, seg->to.x,
							seg->from.z, seg->to.z,
							seg1->seg.from.x);

						if(z1 < seg1->seg.from.z)
						{
							/*
							 * Remove from scan list
							 */

							if(oldSeg)
								oldSeg->next = seg1->next;
							else
								*info = seg1->next;

							/*
							 * Add to free list
							 */

							UWORD next = seg1->next;
							seg1->next = freeSegment;
							freeSegment = segID;
							segID = next;

							continue;	// Go around loop again with next segment
						}
						else
						{
							/*
							 * Left Aligned?
							 *
							 * Truncate new segment
							 */

							if(seg->from.x == seg1->seg.from.x)
							{
								SDimension newX = seg1->seg.to.x + 1;

								seg->from.z = intersect(
									seg->from.x, seg->to.x,
									seg->from.z, seg->to.z,
									newX);
								seg->from.i = intersect(
									seg->from.x, seg->to.x,
									seg->from.i, seg->to.i,
									newX);
								seg->from.x = newX;
							}
							else if(seg->to.x == seg1->seg.to.x)
							{	// Right aligned
								SDimension newX = seg1->seg.from.x - 1;

								seg->to.z = intersect(
									seg->from.x, seg->to.x,
									seg->from.z, seg->to.z,
									newX);
								seg->to.i = intersect(
									seg->from.x, seg->to.x,
									seg->from.i, seg->to.i,
									newX);
								seg->to.x = newX;
							}
							else
							{
								/*
								 * New segment must be split into 2
								 */

								SDimension newX = seg1->seg.to.x + 1;

								ScanSegment newSeg;
								newSeg.material = seg->material;
								newSeg.from.x = newX;
								newSeg.from.z = intersect(
										seg->from.x, seg->to.x,
										seg->from.z, seg->to.z,
										newX);
							
								newSeg.from.i = intersect(
										seg->from.x, seg->to.x,
										seg->from.i, seg->to.i,
										newX);

								newSeg.to = seg->to;

								newX = seg1->seg.from.x - 1;

								seg->to.z = intersect(
									seg->from.x, seg->to.x,
									seg->from.z, seg->to.z,
									newX);
								seg->to.i = intersect(
									seg->from.x, seg->to.x,
									seg->from.i, seg->to.i,
									newX);
								seg->to.x = newX;

								putLineSeg(info, &newSeg);

								/*
								 * Need to make a clean break of it because
								 * putLineSeg could change the scan links
								 */

								split = True;
								break;
							}
						}
					}

					/*
				 	 * One side overlaps, so split into 2 and recurse!
				 	 */

					else
					if(seg->from.x < seg1->seg.from.x)
					{
						/*
						 * New segment overhangs right edge
						 */


						WORD z1 = intersect(
							seg->from.x, seg->to.x,
							seg->from.z, seg->to.z,
							seg1->seg.from.x);

						// In front or behind?

						if(seg1->seg.from.z < z1)
						{
							/*
							 * New line is behind old line
							 * So.. truncate new line and continue
							 */

							SDimension newX = seg1->seg.from.x - 1;

							seg->to.z = intersect(
								seg->from.x, seg->to.x,
								seg->from.z, seg->to.z,
								newX);
							seg->to.i = intersect(
								seg->from.x, seg->to.x,
								seg->from.i, seg->to.i,
								newX);
							seg->to.x = newX;
						}
						else
						{
							/*
							 * New line is in front of old line
							 */

							SDimension newX = seg->to.x + 1;

							seg1->seg.from.z = intersect(
								seg1->seg.from.x, seg1->seg.to.x,
								seg1->seg.from.z, seg1->seg.to.z,
								newX);
							seg1->seg.from.i = intersect(
								seg1->seg.from.x, seg1->seg.to.x,
								seg1->seg.from.i, seg1->seg.to.i,
								newX);
							seg1->seg.from.x = newX;
						}
					}
					else if(seg->to.x > seg1->seg.to.x)
					{
						/*
						 * New segment overhangs left edge
						 */

						WORD z1 = intersect(
							seg->from.x, seg->to.x,
							seg->from.z, seg->to.z,
							seg1->seg.to.x);

						// In front or behind?

						if(seg1->seg.from.z < z1)
						{
							/*
							 * New line is behind old line
							 * So.. truncate new line and continue
							 */

							SDimension newX = seg1->seg.to.x + 1;

							seg->from.z = intersect(
								seg->from.x, seg->to.x,
								seg->from.z, seg->to.z,
								newX);
							seg->from.i = intersect(
								seg->from.x, seg->to.x,
								seg->from.i, seg->to.i,
								newX);
							seg->from.x = newX;
						}
						else
						{
							/*
							 * New line is in front of old line
							 */

							SDimension newX = seg->from.x - 1;
							seg1->seg.to.z = intersect(
								seg1->seg.from.x, seg1->seg.to.x,
								seg1->seg.from.z, seg1->seg.to.z,
								newX);
							seg1->seg.to.i = intersect(
								seg1->seg.from.x, seg1->seg.to.x,
								seg1->seg.from.i, seg1->seg.to.i,
								newX);
							seg1->seg.to.x = newX;
						}
					}

				}
				oldSeg = seg1;
				segID = seg1->next;
			}
		}
	} while(split);

	/*
	 * If anything left, then really add it
	 */

	if(freeSegment != NoScanSegment)
	{

		/*
		 * Get a new segment
		 */

		
		UWORD newSegID = freeSegment;
		ScanSegmentList* newSeg = &segments[newSegID];
		freeSegment = newSeg->next;

		/*
		 * Copy information
		 */

		newSeg->seg = *seg;

		/*
		 * Add to scan line
		 */

		if(oldSeg)
		{
			newSeg->next = oldSeg->next;
			oldSeg->next = newSegID;
		}
		else
		{
			newSeg->next = *info;
			*info = newSegID;
		}
	}
#ifdef DEBUG
	else
		throw GeneralError("Ran out of scan buffer segments");
#endif
}

/*
 * Add a new segment that we know won't overlap!
 */

void ScanBuffer::putLineSegQuick(UWORD* info, ScanSegment* seg, ScanSegmentList* prev)
{
	if(freeSegment != NoScanSegment)
	{
		/*
		 * Get a new segment
		 */
		
		UWORD newSegID = freeSegment;
		ScanSegmentList* newSeg = &segments[newSegID];
		freeSegment = newSeg->next;

		/*
		 * Copy information
		 */

		newSeg->seg = *seg;

		/*
		 * Add to scan line
		 */

		if(prev)
		{
			newSeg->next = prev->next;
			prev->next = newSegID;
		}
		else
		{
			newSeg->next = *info;
			*info = newSegID;
		}
	}
#ifdef DEBUG
	else
		throw GeneralError("Ran out of scan buffer segments");
#endif
}

void ScanBuffer::draw(System3D& drawData)
{
	if(!paletteSet)
	{
		paletteSet = True;

		drawData.terrain.makeColours();
		drawData.terrain.setPalette();
	}


	SDimension y = 0;
	UWORD* info = sections;

	while(y < height)
	{
		UWORD segID = *info++;

		while(segID != NoScanSegment)
		{
			ScanSegmentList* seg = &segments[segID];
			segID = seg->next;

			/*
			 * Draw the scan line
			 */

			SDimension x1 = seg->seg.from.x;
			SDimension x2 = seg->seg.to.x;

			if(x1 < 0)
				x1 = 0;
			if(x2 >= drawData.bitmap->getW())
				x2 = drawData.bitmap->getW() - 1;

			if( (x1 < drawData.bitmap->getW()) && (x2 >= 0))
			{
				const Image* texture = drawData.terrain.getTexture(seg->seg.material);
				TerrainCol* col = drawData.terrain.getColour(seg->seg.material);
				SDimension length = x2 - x1 + 1;

				if(texture)
					drawData.bitmap->HLineFast(x1, y, length, col, seg->seg.from.i, seg->seg.to.i, texture);
				else
					drawData.bitmap->HLineFast(x1, y, length, col, seg->seg.from.i, seg->seg.to.i);

#ifdef DEBUG1
				*drawData.bitmap->getAddress(x1, y) = 0;
				*drawData.bitmap->getAddress(x2, y) = 0;
#endif
			}
		}
		y++;
	}
}


void ScanBuffer::blitSprite(Region* region, const Sprite* sprite, const Point3D& pt) const
{
	region->maskBlit(sprite, pt);
}


/*
 * Return the x,y,z of the given screen coordinate
 */

Boolean ScanBuffer::findPoint(const Point& p, Point3D& result)
{
	if( (p.y >= 0) && (p.y < height) )
	{
		UWORD segID = sections[p.y];

		while(segID != NoScanSegment)
		{
			ScanSegmentList* seg = &segments[segID];
			segID = seg->next;
#if 0
			if(seg->seg.from.x > p.x)
				return False;

			if(p.x <= seg->seg.to.x)
#else	// Overcome bug where segments not sorted properly
			if( (p.x >= seg->seg.from.x) && (p.x <= seg->seg.to.x) )
#endif
			{
				result.x = p.x;
				result.y = p.y;
				result.z = seg->seg.from.z;	// Really ought to be intersection

				return True;
			}
		}

	}

	return False;
}

