/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	IPX Network and Connection Routines
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.5  1994/08/22  16:02:09  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/07/19  19:53:03  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/06/21  18:49:27  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/05/25  23:31:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/05/19  17:44:37  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#if !defined(COMPUSA_DEMO)

#include <string.h>
#include <stdio.h>
#ifdef DEBUG1
#include <fstream.h>
#include <iomanip.h>
#endif

#include "ipx.h"
#include "ipx_asm.h"
#include "system.h"
#include "timer.h"
#include "game.h"
#include "mouselib.h"
#ifdef DEBUG
#include "log.h"
#endif
#include "memptr.h"
#include "dialogue.h"
#include "language.h"

/*
 * IPX Network Connection Class
 */

const char PacketID[] = "DWG_00";
const UWORD defaultSocket = 0x4447; 



/*
 * IPX Network Connection class Implementation
 */

/*
 * Constructor
 */

IPXConnection::IPXConnection()
{
	void* ad = realMemory.alloc(
		(sizeof(IPX_Header) + sizeof(ECB_Header) + sizeof(IPX_Message)) * (IPXrxBufferCount + 1) +
		sizeof(IPX_NetAddress) * 2);

	ad = initIPXbuffer(&txBuffer, ad);
	int i = IPXrxBufferCount;
	IPX_Buffer* buf = rxBuffer;
	while(i--)
		ad = initIPXbuffer(buf++, ad);
	localAddress = (IPX_NetAddress*)ad;
	memset(localAddress, 0, sizeof(IPX_NetAddress));
	ad = localAddress + 1;

	remoteAddress = (IPX_NetAddress*)ad;
	memset(remoteAddress, 0, sizeof(IPX_NetAddress));
	ad = remoteAddress + 1;

	// serial = 0;		// Start at packet number 0
	// rxSerial = 0;
}

IPXConnection::~IPXConnection()
{
	disconnect();
}

/*
 * Private function to set up pointers to real mode IPX buffer memory.
 */

void* IPXConnection::initIPXbuffer(IPX_Buffer* buffer, void* ad)
{
	buffer->header = (ECB_Header*)ad;
	memset(buffer->header, 0, sizeof(ECB_Header));
	ad = buffer->header + 1;

	buffer->ipxHeader = (IPX_Header*)ad;
	memset(buffer->ipxHeader, 0, sizeof(IPX_Header));
	ad = buffer->ipxHeader + 1;

	buffer->message = (IPX_Message*)ad;
	memset(buffer->message, 0, sizeof(IPX_Message));
	ad = buffer->message + 1;

	/*
	 * Set up header fields
	 */

	buffer->header->fragment_count = 2;
	buffer->header->hdr.ptr = buffer->ipxHeader;
	buffer->header->hdr.size = sizeof(IPX_Header);
	buffer->header->buffer.ptr = buffer->message;
	buffer->header->buffer.size = sizeof(Packet);
	buffer->ipxHeader->type = IPX_TYPE;



	return ad;
}


int IPXConnection::connect()
{
	MemPtr<char> buffer(500);

	strcpy(buffer, language(lge_makeIPX));
	char* countPtr = buffer + strlen(buffer);

	PopupText popup(buffer);

#ifdef DEBUG
	netLog.printf("-----------------------");
	netLog.printf("Starting IPX connection");
	netLog.printf("-----------------------");
#endif

	if(IPX_install())
	{
		Socket socket = IPX_openSocket(defaultSocket);

		if(socket)
		{
			localAddress->socket = socket;
			IPX_GetInternetworkAddress(localAddress);

#ifdef DEBUG
			netLog.printf("Network address is: %08lx", localAddress->network);
			netLog.printf("Node address is: %02x%02x%02x%02x%02x%02x",
				  			int(localAddress->node[0]),
				  			int(localAddress->node[1]),
				  			int(localAddress->node[2]),
				  			int(localAddress->node[3]),
				  			int(localAddress->node[4]),
				  			int(localAddress->node[5]));
			netLog.printf("Socket is: %d", (int)localAddress->socket);
#endif

			/*
			 * Set up the listening channels
			 */

			int i = IPXrxBufferCount;
			IPX_Buffer* buf = rxBuffer;
			while(i--)
			{
				buf->header->buffer.size = sizeof(Packet);
				buf->header->socket = localAddress->socket;

				IPX_listenForPacket(buf->header);

				buf++;
			}

			/*
			 * Set up a broadcast message
			 */

			unsigned int time = timer->getCount();
			int tries = 30;

			while(tries)
			{
				/*
				 * Mouse press to abort
				 */

				MouseEvent* event = machine.mouse->getEvent();
				if(event)
				{
					ButtonStatus b = event->buttons;
					// delete event;
					if(b)
					{
#ifdef DEBUG
						dialAlert(0, "User Abort", "OK");
#endif
						return -1;
					}
				}

				if(timer->getCount() >= time)
				{
					time = timer->getCount() + 3 * timer->ticksPerSecond;
					tries--;

					if(canSend())
					{
						sprintf(countPtr, "%d", tries);
						popup.showText(buffer);

						memset(remoteAddress->node, 0xff, sizeof(remoteAddress->node));	
						remoteAddress->network = localAddress->network;
						remoteAddress->socket = socket;

						char buffer[] = "Hello Civil War Player!\n\r";
						sendRawData(Connect_HELLO, (UBYTE*)buffer, strlen(buffer) + 1, False, serial++);
					}
				}

				/*
				 * Check for any incoming packets
				 */

				int i = IPXrxBufferCount;
				IPX_Buffer* buf = rxBuffer;
				while(i--)
				{
					if(buf->header->in_use == 0)
					{
						if(memcmp(buf->ipxHeader->src.node, localAddress->node, sizeof(localAddress->node)) == 0)
						{
							;	// ...
						}
						else
						{
							IPX_Message* packet = buf->message;
#ifdef DEBUG1
							log(20, "Packet received from: " << hex << setfill('0') <<
								setw(2) << (int)buf->ipxHeader->src.node[0] <<
								setw(2) << (int)buf->ipxHeader->src.node[1] <<
								setw(2) << (int)buf->ipxHeader->src.node[2] <<
								setw(2) << (int)buf->ipxHeader->src.node[3] <<
								setw(2) << (int)buf->ipxHeader->src.node[4] <<
								setw(2) << (int)buf->ipxHeader->src.node[5] <<
								", socket: " << setw(4) << buf->ipxHeader->src.socket <<
								dec << setfill(' ') <<
								", Network: " << buf->ipxHeader->src.network);

							log (20,
								(char)packet->id[0] <<
								(char)packet->id[1] <<
								(char)packet->id[2] <<
								(char)packet->id[3] <<
								(char)packet->id[4] <<
								(char)packet->id[5] <<
								", Serial: " << packet->serial <<
								", Type: " << packet->packet.type <<
								", Length: " << packet->packet.length <<
								", Message: " << packet->packet.data);

							log(20, "Connection established\n");
#endif
							if( (packet->packet.type == Connect_HELLO) ||
								 (packet->packet.type == Connect_HelloAck) )
							{
								*remoteAddress = buf->ipxHeader->src;

								if(packet->packet.type == Connect_HELLO)
									IPX_listenForPacket(buf->header);

								sendData(Connect_HelloAck, 0, 0);

								if(packet->packet.type != Connect_HelloAck)
								{
									Boolean ackRx = False;
									while(!ackRx)
									{
										/*
				 					 	* Mouse press to abort
				 					 	*/

										MouseEvent* event = machine.mouse->getEvent();
										if(event)
										{
											ButtonStatus b = event->buttons;
											// delete event;
											if(b)
											{
#ifdef DEBUG
												dialAlert(0, "User Abort", "OK");
#endif
												return -1;
											}
										}

										if(flush())
											return -1;

										Packet* pkt = receiveData();
										if(pkt)
										{
											if(pkt->type == Connect_HelloAck)
												ackRx = True;
											releaseData(pkt);
										}
										IPX_relinquishControl();
									}
								}

								/*
								 * Set the one with the biggest node as controller
								 */

								if(memcmp(localAddress->node, remoteAddress->node, NODE_ADDRESS_SIZE) > 0)
									inControl = True;

#ifdef DEBUG
								netLog.printf("inControl = %d", (int) inControl);
#endif
								connected = True;
								return 0;
							}
						}
						IPX_listenForPacket(buf->header);
					}

					buf++;
				}

				IPX_relinquishControl();
			}
#ifdef DEBUG
			dialAlert(0, "Time out", "OK");
#endif
			return -1;
		}
	}

#ifdef DEBUG
	dialAlert(0, "IPX Not installed\ror\rNo Network Card installed", "OK");
#endif

	return -1;			// Unimplemented
}

void IPXConnection::disconnect()
{
	if(localAddress->socket)
	{
		IPX_closeSocket(localAddress->socket);
		localAddress->socket = 0;
		connected = False;
	}
}

Boolean IPXConnection::canSend()
{
	return (txBuffer.header->in_use == 0);
}

int IPXConnection::sendRawData(PacketType type, UBYTE* data, size_t length, Boolean receipt, ULONG ser)
{
#ifdef DEBUG
	netLog.printf("IPX::sendRawData(type:%s, serial:%ld, receipt:%d, length:%d)",
		packetStr(type),
		ser,
		receipt,
		(int) length);
#endif

	/*
	 * Wait for txBuffer to be free
	 */

	unsigned int timeOut = timer->getCount() + timer->ticksPerSecond * 5;

	do
	{
		if(txBuffer.header->in_use == 0)
		{
			/*
		 	 * Copy data to real mode memory
		 	 */

			IPX_Message* msg = txBuffer.message;

			memcpy(msg->id, PacketID, sizeof(msg->id));
			msg->packet.type = type;
			msg->packet.length = UWORD(length);
			if(data)
				memcpy(msg->packet.data, data, length);
			msg->serial = ser;

			msg->needReceipt = UBYTE(receipt);

			/*
		 	 * Set up IPX Header fields
		 	 */

			txBuffer.header->socket = localAddress->socket;
			txBuffer.header->buffer.size = UWORD(length + sizeof(IPX_Message) - sizeof(msg->packet.data));

			txBuffer.ipxHeader->dest = *remoteAddress;
			memcpy(txBuffer.header->dest_address, remoteAddress->node, sizeof(remoteAddress->node));

			return IPX_sendPacket(txBuffer.header);
		}
#ifdef DEBUG1
		log(20, "TX Buffer in use");	
#endif

		IPX_relinquishControl();

	} while(timer->getCount() < timeOut);

	return -1;
}

#ifdef COMMS_OLD
Packet* IPXConnection::receiveData()
{
	processIncomming();			// Clear any acknowledgements

	/*
	 * Return 1st entry in packet list
	 */

	return packetList.get();
}
#endif

void IPXConnection::processIncomming()
{
#ifdef DEBUG1
	if(logFile)
	{
		log(20, "processIncomming()");
		*logFile << startLogLine << "RxBuffers:";
		{
			int i = IPXrxBufferCount;
			IPX_Buffer* buf = rxBuffer;
			while(i--)
			{
				*logFile << (int)buf->header->in_use << " ";
				buf++;
			}
			*logFile << endl;
		}
	}

	if(receiptList.entry)
	{
		log(20, "Packets waiting for a receipt at " << timer->getCount());

		Receipt* receipt = receiptList.entry;
		while(receipt)
		{
			log(20, "Serial: " << receipt->serial <<
				 ", Type: " << receipt->type <<
				 ", length: " << receipt->length <<
				 ", Time: " << receipt->time);

			receipt = receipt->next;
		}
	}
	else
		log(20, "Not waiting for receipts");
#endif

	int i = IPXrxBufferCount;
	IPX_Buffer* buf = rxBuffer;
	while(i--)
	{
		if(buf->header->in_use == 0)
		{
			if(memcmp(buf->ipxHeader->src.node, localAddress->node, sizeof(localAddress->node)) != 0)
			{
				IPX_Message* packet = buf->message;

				if(memcmp(packet->id, PacketID, sizeof(packet->id)) == 0)
				{
#ifdef DEBUG
					netLog.printf("IPX:CheckAck(): type=%s, serial=%ld, receipt:%d",
						packetStr(packet->packet.type),
						packet->serial,
						(int) packet->needReceipt);
#endif
					/*
					 * Got acknowledgement?
					 */

					if(packet->packet.type == Connect_Acknowledge)
					{
						receiptList.remove(packet->serial);
#ifdef DEBUG1
						log(20, "ack received for " << packet->serial);
#endif
					}
					else
					{
						/*
						 * Send receipts
						 */

						if(packet->needReceipt)
						{
#ifdef DEBUG1
							log(20, "Sending receipt for " << packet->serial);
#endif
							packet->needReceipt = False;
							sendRawData(Connect_Acknowledge, 0, 0, False, packet->serial);
						}

						/*
						 * Copy packet into received packet buffer
						 */

						if(packet->serial >= rxSerial)
						{
							rxSerial = packet->serial + 1;

#ifdef DEBUG1
							log(20, "Copying packet to packetlist");
#endif
							Packet* pkt = new Packet;
							pkt->type = packet->packet.type;
							pkt->length = packet->packet.length;
							pkt->sender = RID_Remote;
							if(pkt->length)
								memcpy(pkt->data, packet->packet.data, packet->packet.length);
							packetList.append(pkt);
						}
					}
				}
#ifdef DEBUG1
				else	// Bad packet... throw it away!
				{
					log(20, "processIncomming::Ignoring bad packet");
				}
#endif
			}
#ifdef DEBUG1
			else		// Echo'd message from us! throw it away
			{
				log(20, "processIncomming::Ignoring echoed packet");
			}
#endif
			IPX_listenForPacket(buf->header);
		}

		buf++;
	}
}

void IPXConnection::releaseData(Packet* data)
{
	delete data;
}

#endif	// DEMO
