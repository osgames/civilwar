/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Simple Message Window
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.8  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.4  1994/04/20  22:21:40  Steven_Green
 * Use new TextWindow class.
 *
 * Revision 1.1  1994/01/11  22:29:03  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdarg.h>
#include <stdio.h>
#include "msgwind.h"
#include "system.h"
#include "screen.h"
#include "memptr.h"


MessageWindow::MessageWindow(Image* img, const Rect& r) :
	region(img, r),
	textWind(&region, Font_EMFL10)

{
}

MessageWindow::~MessageWindow()
{
}

void MessageWindow::draw(const char* s)
{
	textWind.setFormat(TW_Centre);		// Centre Horizonally and vertically
	textWind.setColours(Black);

	region.fill(MsgFillColour);
	region.frame(0,0,region.getW()-1,region.getH()-1, MsgBorderColour1, MsgBorderColour2);

	if(s)
		textWind.draw(s);

	machine.screen->setUpdate(region);
}

void MessageWindow::printf(const char* fmt, ...)
{
	MemPtr<char> buffer(1024);

	va_list vaList;
	va_start(vaList, fmt);
	vsprintf(buffer, fmt, vaList);
	va_end(vaList);

	draw(buffer);
}


