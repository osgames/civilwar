/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Icon Interface Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.22  1994/07/11  14:26:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.21  1994/06/24  14:43:30  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.20  1994/06/06  13:17:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.19  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.18  1994/05/25  23:31:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.17  1994/05/19  17:44:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.16  1994/05/04  22:09:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/04/26  13:39:07  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.14  1994/04/22  22:14:22  Steven_Green
 * Icon Handling tidied up.
 *
 * Revision 1.13  1994/03/17  14:29:31  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1994/03/15  15:15:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/03/11  23:12:15  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/03/10  14:27:18  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/02/09  14:59:26  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/02/03  15:04:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/01/28  22:22:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/01/26  23:08:55  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/01/17  20:14:26  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/01/11  22:29:03  Steven_Green
 * MenuData stuff moved into its own file
 *
 * Revision 1.3  1994/01/11  21:13:30  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/01/07  23:43:18  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/01/06  22:38:30  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG
#include <iostream.h>
#endif

#include <ctype.h>

#include "icon.h"
#include "system.h"

#ifdef DEBUG
#include "screen.h"
#endif

/*
 * See if keypress would activate icon
 */

ButtonStatus Icon::matchKey(Key key)
{
	if(key)
	{
		key = Key(toupper(key));

		if(key == toupper(leftKey))
			return Mouse::LeftButton;
		if(key == toupper(rightKey))
			return Mouse::RightButton;
	}

	return 0;
}

#ifdef DEBUG
/*
 * Default execute/draw
 */

void Icon::execute(Event* event, MenuData* data)
{
	data = data;

	if(event->buttons)
		cout << "Icon pressed" << endl;
}

void Icon::drawIcon()
{
	Point p = getPosition();

	Rect r = Rect(p, getSize());
	machine.screen->getBitmap()->frame(r, 0xe0, 0xef);
	machine.screen->setUpdate(r);
}
#endif

Point Icon::getPosition() const
{
	if(parent)
		return parent->getPosition() + *this;
	else
		return *this;
}

/*
 * Icon Set
 */

IconSet::IconSet(IconSet* parent, Rect r) :
	Icon(parent, r)
{
	icons = 0;
}

void IconSet::killIcons()
{
	if(icons)
	{
		Icon**i = icons;

		while(*i)
			delete *i++;

		delete[] icons;

		icons = 0;
	}
}


/*
 * Draw all the icons in the icon set
 */


void IconSet::drawIcon()
{
#ifdef DEBUG1
	Icon::draw(screen);		// Debug draw Rectangle around area
#endif

	if(icons)
	{
		Icon** i = icons;

		while(*i)
		{
			i[0]->redrawIcon();
			i++;
		}
	}
}

void Icon::drawUpdate()
{
	if(reDraw)
		redrawIcon();
}

void IconSet::drawUpdate()
{
	Icon::drawUpdate();

	if(icons)
	{
		Icon** i = icons;

		while(*i)
		{
			Icon* icon = *i++;

			icon->drawUpdate();
		}
	}
}

void IconSet::execute(Event* event, MenuData *d)
{
	if(icons)
	{
		Icon** i = icons;

		while(*i)
		{

			Icon* icon = *i++;
			Event e1;

			e1.key = event->key;

			const Point& mpos = event->where;

			if(
				(mpos.x >= icon->getX()) &&
	   		(mpos.x < (icon->getX() + icon->getW())) &&
	   		(mpos.y >= icon->getY()) &&
	 			(mpos.y < (icon->getY() + icon->getH()) ) )
			{
				e1.overIcon = True;
				e1.buttons = event->buttons;
			}
			else
			{
				e1.overIcon = False;
				e1.buttons = 0;
			}

			if(event->key)
			{
				ButtonStatus b = icon->matchKey(e1.key);
				if(b)
				{
					e1.buttons |= b;
					event->key = 0;
				}
			}

			e1.where = event->where - *icon;		// Make coordinates relative

			icon->execute(&e1, d);
			event->key = e1.key;

			if(e1.buttons)
				break;
		}
	}
}

/*
 * Nest into a new sub menu
 *
 * Redraw it, unless this is the first entry
 */

void SubMenu::add(IconList* i)
{
	IconList* old = icons;

	last.insert(icons);
	icons = i;

	if(old)
		setRedraw();
}

/*
 * Backup to previous submenu usage and redraw unless it was the top.
 */

void SubMenu::remove()
{
	icons = last.get();

	if(icons)
		setRedraw();
}

/*
 * Delete a sub menu and backup
 */

void SubMenu::kill()
{
	IconList* list = icons;
	remove();

	if(list)
	{
		IconList* i = list;
		while(*i)
			delete *i++;
		delete[] list;
	}
}

void SubMenu::destroy()
{
	while(icons)
		kill();
}

SubMenu::~SubMenu()
{
	destroy();
}
