/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 **********************************************************************
 * $Id$
 **********************************************************************
 *
 *	Implementation of Vesa Interface
 *
 **********************************************************************
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:54  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.16  1994/07/11  14:26:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.14  1994/05/04  22:09:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/04/13  19:47:11  Steven_Green
 * Get the address of each bank!
 *
 * Revision 1.12  1994/04/11  21:28:35  Steven_Green
 * Functions for setting Read/Write Bank.  Use winAttributes to set it up.
 *
 * Revision 1.11  1994/01/07  23:43:18  Steven_Green
 * Use Image::getWidth/Height instead of accessing fields directly.
 *
 * Revision 1.10  1993/12/21  00:31:04  Steven_Green
 * Does not use the videoPtr field to determine whether modes are
 * available, as that did not work on all of our 486's with Cirrus
 * Logic Vesa BIOS in protected mode.  It now just tries to set the
 * video mode and uses the return value to find out if it was succesful!
 *
 * Revision 1.9  1993/12/04  16:22:51  Steven_Green
 * Exception handling added if can't initialise graphics mode.
 *
 * Revision 1.8  1993/12/04  01:02:56  Steven_Green
 * current bank is initialised to -1, instead of assuming it is set to 0.
 *
 * Revision 1.7  1993/11/19  18:59:26  Steven_Green
 * Allowed alternative graphic mode parameter if requested one isn't
 * available.
 *
 * Revision 1.6  1993/11/16  22:42:06  Steven_Green
 * Moved debug text to DEBUG1
 *
 * Revision 1.5  1993/11/15  17:20:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1993/11/05  16:50:53  Steven_Green
 * Support for sprites, mice and polygons added
 *
 * Revision 1.3  1993/10/27  21:08:17  Steven_Green
 * Clipping Rectangle Modification
 *
 * Revision 1.2  1993/10/26  21:32:42  Steven_Green
 * Clipping functions added
 * blit from portion of image to screen added
 *
 * Revision 1.1  1993/10/26  14:54:05  Steven_Green
 * Initial revision
 *
 *
 **********************************************************************
 */

#ifdef DEBUG1
#include <iostream.h>
#endif
#include <string.h>
#include <i86.h>

#include "vesa.h"
#include "vesa_asm.h"
#include "image.h"
#include "sprite.h"


/*
 * Initialise Vesa and set mode
 */

Vesa::Vesa(VesaMode mode, VesaMode mode2)
{
#ifdef DEBUG1
	cout << "Initialising Vesa to mode 0x" << hex << mode << dec << "\n";
#endif

	if(isVesa())
		throw NoVesaBios();


	/*
	 * Method that doesn't require videoModePtr to work correctly!
	 */

	oldMode = getVesaMode();

	if(mode == VESANoMode)
		mode = oldMode;

	Selector modeSelector;
	VesaModeInfo* modeInfoPtr = getVesaModeInfo(mode, modeSelector);
	if(!modeInfoPtr)
		modeInfoPtr = getVesaModeInfo(mode = mode2, modeSelector);


	if(modeInfoPtr)
	{
		/*
		 * Fill in Info
		 */

		currentMode = mode;

		currentABank = -1;			// Illegal value
		currentBBank = -1;

		if( (modeInfoPtr->winAAttributes & 3) == 3)
		{
			readWin = 0;
			windowPtrR = (UBYTE*)(modeInfoPtr->winASegment << 4);
		}
		else if( (modeInfoPtr->winBAttributes & 3) == 3)
		{
			readWin = 1;
			windowPtrR = (UBYTE*)(modeInfoPtr->winBSegment << 4);
		}
		else	// No readable window!
			throw VesaError("No readable VESA Window");

		if( (modeInfoPtr->winAAttributes & 5) == 5)
		{
			writeWin = 0;
			windowPtrW = (UBYTE*)(modeInfoPtr->winASegment << 4);
		}
		else if( (modeInfoPtr->winBAttributes & 5) == 5)
		{
			writeWin = 1;
			windowPtrW = (UBYTE*)(modeInfoPtr->winBSegment << 4);
		}
		else	// No readable window!
			throw VesaError("No writeable VESA Window");

		bankStep = (UWORD)(modeInfoPtr->winSize / modeInfoPtr->winGranularity);
		winSize = modeInfoPtr->winSize * 1024;
		bankSize = modeInfoPtr->winGranularity * 1024;

		if(modeInfoPtr->modeAttributes & VESA_Extended)
		{
			xRes = modeInfoPtr->xRes;
			yRes = modeInfoPtr->yRes;
		}
		else	// Bodge!!!
		{
			xRes = 640;
			yRes = 480;
		}

		bytesPerScanLine = modeInfoPtr->bytesPerScanLine;
		setClip();

		freeDosMemory(modeSelector);

		/*
		 * Set the new mode
		 */
			
		setVesaMode(mode);
	}
	else
		throw NoInfoBlock();
}

/*
 * Close Vesa (restore)
 */

Vesa::~Vesa()
{
#ifdef DEBUG1
	cout << "Destructing Vesa (from mode 0x" << hex << currentMode <<
			" back to 0x" << oldMode << dec << ")\n";
	cout.flush();
#endif

	setVesaMode(oldMode);
}

/*
 * Set Vesa bank address
 */

VesaState Vesa::setABank(VesaBank bank)
{
	VesaState retVal = VESA_OK;

	_disable();
	if( (bank != currentABank) && (bank != NoVesaBank) )
	{
		currentABank = bank;
		retVal = setVesaBank(bank, 0);				// Change to new bank
	}
	_enable();

	return retVal;
}

VesaState Vesa::setBBank(VesaBank bank)
{
	VesaState retVal = VESA_OK;

	_disable();
	if( (bank != currentBBank) && (bank != NoVesaBank) )
	{
		currentBBank = bank;
		retVal = setVesaBank(bank, 1);				// Change to new bank
	}
	_enable();

	return retVal;
}



/*
 * Blit a portion of an image to a VESA compatible screen
 */

void Vesa::blit(Image* image, Point dest, Rect src)
{
	if(src.getW() == 0)
	{
		src.setW(image->getWidth());
		src.setH(image->getHeight());
	}


	if(clip(dest, src))
		return;

	VesaBank oldBank = getWBank();

	/*
	 * Calculate starting bank and offset
	 */

	int screenPos = dest.getY() * bytesPerScanLine + dest.getX();
	VesaBank bank = VesaBank(screenPos / bankSize);
	int offset = screenPos % bankSize;

	setWBank(bank);

	int lineCount = src.getH();
	// UBYTE* linePtr = image.data + src.getY() * image.width + src.getX();
	UBYTE* linePtr = image->getAddress(src);
	UBYTE* winPtr = windowPtrW;
	while(lineCount--)
	{
		/* If line will be split in half */

		if( (offset + src.getW()) >= winSize)
		{
			memcpy(winPtr + offset, linePtr, winSize - offset);

			bank += bankStep;
			setWBank(bank);

			memcpy(winPtr, linePtr + winSize - offset, src.getW() - (winSize - offset));

			offset -= winSize;

		}
		else
		{
			memcpy(winPtr + offset, linePtr, src.getW());
		}

		/* If next line will go over a bank */

		offset += bytesPerScanLine;
		if(offset >= winSize)
		{
			bank += bankStep;
			setWBank(bank);
			offset -= winSize;
		}

		linePtr += image->getWidth();
	}
	setWBank(oldBank);
}



void Vesa::setClip(SDimension x1, SDimension y1, SDimension w1, SDimension h1)
{
	if(w1 == 0)
		w1 = xRes;
	if(h1 == 0)
		h1 = yRes;

	ClipRect::setClip(x1,y1,w1,h1);
}

/*
 * Copy a line of image
 */

inline void maskCopy(Colour mask, UBYTE* dest, UBYTE* src, size_t length)
{
	while(length--)
	{
		Colour pixel = *src++;
		if(pixel != mask)
			*dest = pixel;
		dest++;
	}
}



/*
 * Blit a masked Sprite onto a VESA compatible screen
 */

void Vesa::maskBlit(Sprite* sprite, Point dest)
{
	Rect src(0,0,sprite->getWidth(), sprite->getHeight());

	if(clip(dest, src))
		return;

	VesaBank oldBank = getWBank();

	/*
	 * Calculate starting bank and offset
	 */

	int screenPos = dest.getY() * bytesPerScanLine + dest.getX();
	VesaBank bank = VesaBank(screenPos / bankSize);
	int offset = screenPos % bankSize;

	setWBank(bank);

	int lineCount = src.getH();
	UBYTE* linePtr = sprite->getAddress(src.x, src.y);
	UBYTE* winPtr = windowPtrW;
	while(lineCount--)
	{
		/* If line will be split in half */

		if( (offset + src.getW()) >= winSize)
		{
			maskCopy(sprite->transparent, winPtr + offset, linePtr, winSize - offset);

			bank += bankStep;
			setWBank(bank);

			maskCopy(sprite->transparent, winPtr, linePtr + winSize - offset, src.getW() - (winSize - offset));

			offset -= winSize;

		}
		else
		{
			maskCopy(sprite->transparent, winPtr + offset, linePtr, src.getW());
		}

		/* If next line will go over a bank */

		offset += bytesPerScanLine;
		if(offset >= winSize)
		{
			bank += bankStep;
			setWBank(bank);
			offset -= winSize;
		}

		linePtr += sprite->getWidth();
	}

	setWBank(oldBank);
}

/*
 * Blit a portion of the screen to an image
 */

void Vesa::unBlit(Image* image, Point src)
{
	Rect dest(0, 0, image->getWidth(), image->getHeight());

	if(clip(src, dest))
		return;

	VesaBank oldBank = getRBank();

	/*
	 * Calculate starting bank and offset
	 */

	int screenPos = src.y * bytesPerScanLine + src.x;
	VesaBank bank = VesaBank(screenPos / bankSize);
	int offset = screenPos % bankSize;

	setRBank(bank);

	int lineCount = dest.getH();
	UBYTE* linePtr = image->getAddress(dest);
	UBYTE* winPtr = windowPtrR;
	while(lineCount--)
	{
		/* If line will be split in half */

		if( (offset + dest.getW()) >= winSize)
		{
			memcpy(linePtr, winPtr + offset, winSize - offset);

			bank += bankStep;
			setRBank(bank);

			memcpy(linePtr + winSize - offset, winPtr, dest.getW() - (winSize - offset));

			offset -= winSize;

		}
		else
		{
			memcpy(linePtr, winPtr + offset, dest.getW());
		}

		/* If next line will go over a bank */

		offset += bytesPerScanLine;
		if(offset >= winSize)
		{
			bank += bankStep;
			setRBank(bank);
			offset -= winSize;
		}

		linePtr += image->getWidth();
	}
	setRBank(oldBank);
}


