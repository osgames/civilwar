/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Ascii to Binary Convertor
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdarg.h>
#include "system.h"
#include "wldfile.h"
#include "campwld.h"
#include "campaign.h"
// #include "datafile.h"
// #include "dfile.h"
// #include "obfile.h"
// #include "cityfile.h"

#include "mapwind.h"
#include "game.h"
#include "ai_camp.h"
#include "msgwind.h"

// #include "dialogue.h"


void main()
{
	try
	{
		machine.init();

		CampaignWorld world;
  	
		readStaticWorld("gamedata\\world_s.rrf", &world);
		readWorldOB("gamedata\\troops.rrf", &world);
		readDynamicWorld("gamedata\\world_d.rrf", &world);

		writeStaticWorld("gamedata\\world_s.new", &world);
		writeWorldOB("gamedata\\troops.new", &world);
		writeDynamicWorld("gamedata\\world_d.new", &world);

	}
 	catch(GeneralError e)
 	{
		cout << "General Error: ";
		if(e.get())
			cout << e.get();
		else
			cout << "No description";
		cout << endl;

 	}
 	catch(...)
 	{
 		cout << "Caught some kind of error" << endl;
 	}

}


/*
 * Some functions to keep the linker Happy
 */

void Unit::logInfo(Boolean isLast)
{
}

void Regiment::logInfo(Boolean isLast)
{
}

void Brigade::logInfo(Boolean isLast)
{
}


void Division::logInfo(Boolean isLast)
{
}


void Corps::logInfo(Boolean isLast)
{
}

void Army::logInfo(Boolean isLast)
{
}


void President::logInfo(Boolean isLast)
{
}


void Unit::draw3D(System3D& drawData) { }
void Unit::setupBattle() { }
void Unit::clearupBattle() { }
void playBattle(OrderBattle* ob, MarkedBattle* batl)
{
}

MapPriority Army::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Army::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

MapPriority Corps::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Corps::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

MapPriority Division::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Division::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

MapPriority Brigade::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}


void Brigade::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

MapPriority Regiment::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Regiment::mapDraw(MapWindow* map, Region* bm, Point where)
{
}


MapPriority Unit::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Unit::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

void Unit::drawCampaignUnit(MapWindow* map, Region* bm, Point& where, UWORD icon)
{
}

void Unit::showCampaignInfo(Region* window)
{
}




CampaignTerrain::CampaignTerrain()
{
}

CampaignTerrain::~CampaignTerrain()
{
}

void CampaignTerrain::makeGrid(CTerrainVal* grid, const Location& l, int size)
{
}

CTerrainVal CampaignTerrain::get(const Location& l)
{
	return 0;
}

void CampaignTerrain::init(const char* fileName)
{
}


Point MapWindow::locationToPixel( Location const & l)
{
	return Point(0,0);
}

void MapWindow::drawObject( MapObject* ob )
{
}

#define Font_SimpleMessage Font_EMFL10

void GameVariables::simpleMessage(const char* fmt, ...)
{
	va_list vaList;
	va_start(vaList, fmt);
	vprintf(fmt, vaList);
	va_end(vaList);
}

GameVariables* game = 0;
Campaign* campaign = 0;

void Facility::writeData( DataFileOutStream& s)
{
}

void City::writeData( DataFileOutStream& s)
{
}

UnitAI::~UnitAI()
{
}

UnitAI::UnitAI()
{
}


void LogFile::printf( char const *fmt, ... )
{
}

LogFile::~LogFile()
{
}

LogFile::LogFile( char const * s)
{
}

TimeInfo *LogFile::theTime = 0;

void ReadWorld( char const *s, CampaignWorld *w )
{
}

void MessageWindow::showInfo( char const *s )
{
}

void makeNewRegiment( City *c, char unsigned v, char unsigned w)
{
}

extern "C" {
void logArmies(Unit* top)
{
}
}

void writeWorld(const char* fileName, CampaignWorld* world)
{
}

void  readWorld(const char* fileName, CampaignWorld* world)
{
}

