/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Order of Battle
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.1  1994/04/06  12:40:46  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <string.h>
#include <stdio.h>
#include "ob.h"
#include "generals.h"
#include "campbatl.h"
#include "game.h"
#include "unititer.h"

#if !defined(CAMPEDIT) && !defined(BATEDIT) && !defined(TESTBATTLE)
#include "city.h"
#include "campcomb.h"
#endif

#if !defined(TESTBATTLE) && !defined(BATEDIT)
#include "campaign.h"
#include "campwld.h"
#include "camptab.h"
#endif

#if !defined(CAMPEDIT) && !defined(TESTBATTLE) && !defined(BATEDIT)
#ifdef DEBUG
#include "clog.h"
static LogFile obLog("ob.log");
#endif
#endif
#include "error.h"

/*
 * 	The structure below contains a list of names to be used when
 *		creating a new general
 */

static char* genNames[] = {
 	"Johnson", 
	"Trueman", 
	"Morgan", 
	"Wall", 
	"Mather", 
	"Green", 
	"Earle",
	"Davis", 
	"Carnegie", 
	"Rockefeller", 
	"Field", 
	"Gould", 
	"Boothe", 
	"Scott",
	"O'Brian", 
	"Wilkes", 
	"Parish", 
	"Griffiths"
};

#define nRandomNames (sizeof(genNames) / sizeof(char*))


OrderBattle::OrderBattle()
{
	// generals = new GeneralList;
	// freeGenerals = new GeneralList;
	// freeRegiments = new RegimentList;
	sides = 0;
}

OrderBattle::~OrderBattle()
{
	// generals->clearAndDestroy();

	// freeGenerals->clear();
	// freeRegiments->clearAndDestroy();

	Unit* u = sides;
	while(u)
	{
		// kill(sides);
		Unit* u1 = u;
		u = u1->sister;

		deleteUnit(u1);
	}

	// delete generals;
	// delete freeGenerals;
	// delete freeRegiments;
}


void OrderBattle::deleteUnit(Unit* unit)
{
#if !defined(CAMPEDIT) && !defined(BATEDIT) && !defined(TESTBATTLE)
	/*
	 * If he is occupying a city then unoccupy it!
	 */

	if(unit->occupying)
	{
		if(unit->occupying->occupier == unit)
			actOnCity(unit->occupying, unit, SIEGE_None);
		unit->occupying = 0;
	}

	// Patch... 7th February 1996

	/*
	 * Remove from transport network so that we don't get floating pointers
	 * on trains and ferries
	 */

	if(unit->moveMode == MoveMode_Transport)
	{
#ifdef DEBUG
		obLog.printf("%s is being deleted and is on transport", unit->getName(True));
#endif

		if(unit->realOrders.how == ORDER_Rail)
		{
#ifdef DEBUG
			obLog.printf("Is on Rail");
#endif
			campaign->world->railNet.removeUnit(unit);
		}
		else
		if(unit->realOrders.how == ORDER_Water)
		{
#ifdef DEBUG
			obLog.printf("Is on Water");
#endif
			campaign->world->waterNet.removeUnit(unit);
		}
	}

	//.... end of patch


#endif
	
	General* g = unit->general;
	if(g)
	{
		loseGeneral(g);
		// assignGeneral(g, 0);
	}

	Unit* u = unit->child;
	while(u)
	{
		Unit* u1 = u;
		u = u->sister;

		deleteUnit(u1);
	}

	// assignUnit(unit, 0);
	if(unit->superior)
		unit->superior->removeChild(unit);
	else if(unit->getRank() == Rank_President);
	{	// Removing a president.

		Unit* last = 0;
		Unit* u = sides;

		while(u && (u != unit))
		{
			last = u;
			u = u->sister;
		}

		if(last)
			last->sister = unit->sister;
		else
			sides = (President*) unit->sister;
	}

	delete unit;

#ifdef DEBUG
	check(__FILE__, __LINE__);
#endif
}


President* OrderBattle::getPresident(Side side) const
{
	Unit* president = sides;
	while(president && (president->getSide() != side))
		president = president->sister;
	return (President*) president;
}

void OrderBattle::addFreeGeneral(General* g)
{
	President* p = getPresident(g->getSide());

	if(p)
		p->addFreeGeneral(g);

#if !defined(CAMPEDIT) && !defined(BATEDIT)
	g->inactive = True;
#endif
}

void OrderBattle::removeFreeGeneral(General* g)
{
	President* p = getPresident(g->getSide());

	if(p)
		p->removeFreeGeneral(g);

	g->inactive = False;
}


void OrderBattle::addFreeRegiment(Regiment* r)
{
	President* p = getPresident(r->getSide());

	assignUnit(r, 0);		// Remove from where he is

	if(p)
		p->addFreeRegiment(r);

#if !defined(CAMPEDIT) && !defined(BATEDIT)
	r->joining = True;
#endif
}


void OrderBattle::addSide(President* p)
{
	Unit* last = sides;

	if(last)
	{
		while(last->sister)
			last = last->sister;
		last->sister = p;
	}
	else
		sides = p;
}

/*============================================================
 * Bits that used to assignUnit()
 * Split into more manageable seperate sections
 */

/*
 * Remove a unit from its organisation leaving it in limbo
 */

void OrderBattle::unassignUnit(Unit* u)
{
	ASSERT(u != 0);

	ASSERT(u->getRank() != Rank_President);

	Unit* b = u->superior;

	ASSERT(b != 0);			// He is not assigned anyway!

	b->removeChild(u);		// Remove from tree

	/*
 	 * Remove superior if left childless
 	 */

	while( (b->childCount == 0) && (b->getRank() > Rank_President) )
	{
		Unit* b1 = b;
		b = b->superior;
		deleteUnit(b1);
	}
}

/*
 * Add unit to organisation
 * Create intermediate command positions and generals
 */

void OrderBattle::placeUnit(Unit* unit, Unit* to)
{
	ASSERT(unit != 0);
	ASSERT(to != 0);
	ASSERT(unit->superior == 0);

	ASSERT(unit->getRank() > to->getRank());

	/*
	 * Create missing commanders
	 */

	Unit* realTop = to;

	Rank pRank = promoteRank(unit->getRank());

	while(to && (to->getRank() < pRank))
	{
		to = makeNewUnder(to, unit->location);

#if !defined(TESTBATTLE) && !defined(BATEDIT)
	 	if(to->getRank() == Rank_Army)
		{
			Army* newArmy = (Army*)to;
			newArmy->makeArmyName();
		}
#endif
	}

	/*
	 * Add unit...
	 */

	to->addChild(unit);

#if !defined(TESTBATTLE) && !defined(BATEDIT)
	/*
	 * Fill in missing Generals
	 */

	while(to && !to->general && (to->getRank() > Rank_President))
	{
		makeGeneral(to);

		to = to->superior;
	}
#endif

	/*
	 * Adjust attached status
	 */

	if(realTop != to)
	{
		if(realTop->getRank() > Rank_Army)
		{
			realTop->setCampaignAttachMode(unit->getCampaignAttachMode());
			realTop->makeCampaignDetached(unit->isCampaignDetached());
		}

		unit->setCampaignAttachMode(Attached);
		unit->makeCampaignDetached(False);
	}
}

/*
 * Assign a Unit to another unit (removing it from whatever it
 * already belongs to)
 *
 * This function is too complicated!  There should be seperate
 * functions for transferring units to/from the free regiment pools.
 * In fact there should be a seperate function to remove a unit
 * and add him somewhere else.
 */

void OrderBattle::assignUnit(Unit* unit, Unit* to)
{
#ifdef DEBUG
	check(__FILE__, __LINE__);

	// if(unit && to)
	// 	ASSERT(unit->getRank() == demoteRank(to->getRank()));
#endif

#ifdef OLD_CHRIS
	Unit* originalTO = to ? to->child : 0;		// NB Chris will fix this?
#endif

	/*
	 * Naughty code to prevent President from being deleted!
	 */

	if(unit->getRank() == Rank_President)
	{
#ifdef DEBUG
		throw GeneralError("President is being assigned! File:" __FILE__ ", Line:%d", __LINE__);
#endif
		return;
	}


#if !defined(CAMPEDIT) && !defined(BATEDIT) && !defined(TESTBATTLE)
	/*
	 * Unoccupy anywhere he is occupying
	 */

	if(unit)
	{
		if(unit->occupying)
		{
			if(unit->occupying->occupier == unit)
				actOnCity(unit->occupying, unit, SIEGE_None);
			unit->occupying = 0;
		}
	}
#endif

	/*
	 * Get his existing superior
	 */

	Unit* b = unit->superior;

	/*
	 * Unassign the unit from where he already is
	 */

	if(b)
	{
		/*
		 * Quit if he is not moving
		 */

		if(b == to)
			return;

		unassignUnit(unit);
	}
	else
	{
		/*
		 * Remove from free Regiment list
		 */

		if(unit->getRank() == Rank_Regiment)
		{
			// Find what side we're on

			if(to)
			{
				President* p = getPresident(to->getSide());
				p->removeFreeRegiment((Regiment*)unit);
			}
			else
			{
				/*
				 * We don't know what side the regiment is on, so
				 * we'll try and remove it from both sides!
				 */

				President* p = getSides();
				while(p)
				{
					if(p->removeFreeRegiment((Regiment*)unit))
						break;

					p = (President*) p->sister;
				}

			}
		}
	}

	/*
	 * Now assign him to where he's going
	 */

	// unit->superior = to;

	if(to)
	{

		placeUnit(unit, to);

		/*
		 * Set up orders to be same as commander
		 */

		if(to->getRank() != Rank_President)
		{
#if !defined(TESTBATTLE) && !defined(BATEDIT)
			unit->rejoinCampaign();
#endif

			/*
			 * If assigning to places with no location
			 * set the superiors location
			 */

			Unit* u = to;
			while( (u->getRank() > Rank_President) && (u->location == Location(0,0)))
			{
				u->location = unit->location;
				u->givenOrders.destination = unit->location;
				u->realOrders.destination = unit->location;
				// u->destination = unit->location;
				u = u->superior;
			}
		}
	}
#ifdef DEBUG
	check(__FILE__, __LINE__);
#endif
}

/*
 * Assign a General to a Unit (removing him from wherever he already is)
 *
 */

void OrderBattle::unassignGeneral(General* g)
{
	ASSERT(g != 0);
	ASSERT(g->allocation != 0);

	g->allocation->general = 0;
	g->allocation = 0;
}

void OrderBattle::loseGeneral(General* g)
{
	unassignGeneral(g);
	addFreeGeneral(g);
}


void OrderBattle::assignGeneral(General* g, Unit* to)
{
	ASSERT(g != 0);

	if(!g)
	{
#ifdef TESTING
#ifdef DEBUG
		throw
#endif
		GeneralError("assignGeneral(Null, %s)",
			to ? to->getName(True) : "Null");
#endif
		return;
	}

	ASSERT(!to || (g->getSide() == to->getSide()));

#ifdef TESTING
	if(to && (g->getSide() != to->getSide()))
#ifdef DEBUG
		throw
#endif
		GeneralError("General %s assigned to unit %s on other side!",
			g->getName(), to->getName(True));
#endif


	/*
	 * Displace existing General into free general list
	 */

	if(to)
	{
		General* g1 = to->general;
		if(g1)
			loseGeneral(g1);
			// assignGeneral(g1, 0);
	}

	/*
	 * Remove General from his old position...
	 */

	if(g->allocation)
	{
#if 0
		g->allocation->general = 0;
		g->allocation = 0;
#else
		unassignGeneral(g);
#endif
	}
	else
		removeFreeGeneral(g);

	/*
	 * ... and assign to new place
	 */

  	g->allocation = to;

	/*
	 * Bodge to force correct general side
	 */

	if(to)
	{
		g->side = to->getSide();

		to->general = g;
		if(g->rank < to->getRank())
			g->rank = to->getRank();

		to->setCanBeSeenFlag();
	}
	else
		addFreeGeneral(g);
}


/*
 * Merge 2 regiments... ending up in dest, deleting src
 */

void OrderBattle::mergeRegiment(Regiment* src, Regiment* dest)
{
	ASSERT(src->type.basicType == dest->type.basicType);
	ASSERT( (src->strength + src->stragglers + dest->strength + dest->stragglers) <= MaxMenPerRegiment);

	/*
	 * Unattach src unit
	 */

	assignUnit(src, 0);

	/*
	 * Make up combined values
	 */

	UnitInfo i(False, False);
	UnitInfo i1(False, False);
	src->getInfo(i);
	dest->getInfo(i1);
	i1 += i;

	/*
	 * Set the resultant merge values
	 */

	dest->setInfo(i1);

	/*
	 * Delete the src unit
	 */

	deleteUnit(src);

#ifdef DEBUG
	check(__FILE__, __LINE__);
#endif
}

/*
 * Create a new unit underneath an existing one
 */

// Unit* OrderBattle::makeNewUnder(Unit* unit, const Location& l, General* g, const char* name)
Unit* OrderBattle::makeNewUnder(Unit* unit, const Location& l)
{
	// Unit* u = unit->makeNewUnder(l, name);
	Unit* u = unit->makeNewUnder(l);

#ifdef BATEDIT
	u->makeBattleInfo();
#endif

	assignUnit(u, unit);

#if 0
	if(g)
		assignGeneral(g, u);
#endif

#ifdef DEBUG
	check(__FILE__, __LINE__);
#endif

	return u;
}



/*
 * Create a new unit underneath another, making all slots down to brigade
 */

Unit* OrderBattle::makeNew(Unit* unit, General* g)
{
	Location l = unit->location;

	if(unit->getRank() == Rank_President)
		l = Location(0,0);

	Unit* p = makeNewUnder(unit, l);
	if(g)
		assignGeneral(g, p);

	while(p->getRank() < Rank_Brigade)
		p = makeNewUnder(p, l);

	return p;
}

#if !defined(CAMPEDIT) && !defined(TESTCAMP)

/*
 * Find the next unit to process
 */


Unit* OrderBattle::getNextBattleUnit(Unit* last, Rank maxRank, Rank minRank)
{
	do
	{
		if(last)
		{
			/*
		 	 * Proceed with unit's children
		 	 */

#if 0
			if( (last->getRank() < minRank) && 
				  last->child && 
				  (last->battleInfo || (last->hasCampaignReallyIndependant())))
#else
			if( (last->getRank() < minRank) && 
				  last->child && 
				  (last->battleInfo || (last->hasCampaignReallyDetached())))
#endif
				last = last->child;

			/*
		 	 * Continue with sisters if no children
		 	 */

			else if(last->sister)
				last = last->sister;

			/*
		 	 * otherwise Go back up tree to a superior with a sister
		 	 */

			else
			{
				do
				{
					last = last->superior;
				} while(last && !last->sister && (last->getRank() > maxRank));

				if(last && (last->getRank() >= maxRank))
					last = last->sister;
				else		// Run out so reset to start again
					last = 0;
			}
		}
		else
			last = 0;
	} while(last && !last->battleInfo);

	return last;
}

#endif	// CAMPEDIT

/*
 * Set up all the unit's side values
 */


void OrderBattle::setSides()
{
	President* p = sides;

	while(p)
	{
		Side side = p->getSide();

		/*
		 * Free Generals
		 */

		if(p->freeGenerals->entries())
		{
			GeneralIter glist = *p->freeGenerals;
			while(++glist)
				glist.current()->side = side;
		}

		p = (President*) p->sister;
	}
}

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
// && !defined(BATEDIT) 

/*
 * Prepare units for battle game
 */

void OrderBattle::setupBattle()
{
	Unit* u = sides;
	while(u)
	{
		u->setupBattle();
		u = u->sister;
	}
}

void OrderBattle::setupBattle(MarkedBattle* batl)
{
	BattleUnit* units = batl->units;
	while(units)
	{
		units->unit->setupBattle();
		units = units->next;
	}
}


void OrderBattle::clearupBattle()
{
	Unit* u = sides;
	while(u)
	{
		u->clearupBattle();
		u = u->sister;
	}
}

#if !defined(TESTBATTLE)
/*
 * Convert all pointers to facilities into IDs
 */


void OrderBattle::beforeBattle()
{
	Unit* u = sides;
	while(u)
	{
		u->beforeBattle();
		u = u->sister;
	}
}

/*
 * Convert IDs back to pointers
 */

void OrderBattle::afterBattle()
{
	Unit* u = sides;
	while(u)
	{
		u->afterBattle();
		u = u->sister;
	}
}
#endif	// TESTBATTLE
#endif

#if !defined(CAMPEDIT) && !defined(BATEDIT) && !defined(TESTBATTLE)

void OrderBattle::clearInBattle()
{
	Unit* u = sides;
	while(u)
	{
		u->clearInBattle();
		u = u->sister;
	}
}

void Unit::clearInBattle()
{
	inBattle = False;

	Unit* u = child;
	while(u)
	{
		u->clearInBattle();
		u = u->sister;
	}
}

void OrderBattle::setVisibility()
{
	if(game->invisibilityON())
	{
		President* pres = getSides();
		while(pres)
		{
		 	if(!game->isPlayer(pres->getSide()))
		 	{

			// President* pres = getPresident(otherSide(game->getLocalSide()));
			// if(pres)
			// {
				for(Unit* a = pres->child; a; a = a->sister)
				{
	 				a->canBeSeen = False;

					for(Unit* c = a->child; c; c = c->sister)
					{
		 				c->canBeSeen = False;

						for(Unit* d = c->child; d; d = d->sister)
						{
			 				d->canBeSeen = False;

							for(Brigade* b = (Brigade*)d->child; b; b = (Brigade*)b->sister)
							{
				 				b->canBeSeen = False;

								// President* p = getPresident(game->getLocalSide());
								President* p = getPresident(otherSide(pres->getSide()));
								if(p)
								{
									for(Unit* army = p->child; army; army = army->sister)
										for(Unit* corps = army->child; corps; corps = corps->sister)
											for(Unit* div = corps->child; div; div = div->sister)
												for(Brigade* brid = (Brigade*)div->child; brid; brid = (Brigade*)brid->sister)
												{
													Distance d = distanceLocation(b->location, brid->location);

													if(d < CampaignVisibleDistance)
													{
						 								(b->getTopCommand() )->canBeSeen = True;
													}
												}
								}

							}
						}
					}
				}
		 	}
			pres = (President*) pres->sister;
		}
	}
}

/*
 * Transfer players orders for the day into the message queue
 *
 * Note: To Be Written... increase time for orders to arrive on higher
 *       difficulty settings.
 */

void OrderBattle::updateDailyOrders()
{
	Unit* p = sides;

	while(p)
	{
		UnitIter iter(p, False, False, Rank_Army, Rank_Brigade);

		while(iter.ok())
		{
			Unit* unit = iter.get();
			ASSERT(unit != 0);

			if(unit->givenNewOrder)
			{
				unit->orderList.append(new SentOrder(unit->givenOrders, campaign->gameTime));
				unit->givenNewOrder = False;
			}
		}

		p = p->sister;
	}
}

/*
 * Daily Processing
 */

void OrderBattle::daily()
{
#ifdef DEBUG
	check(__FILE__, __LINE__);
#endif

	clearInBattle();
	clearupDead();
	setVisibility();
	updateDailyOrders();

#ifdef DEBUG
	check(__FILE__, __LINE__);
#endif

	President* p = sides;
	while(p)
	{
		p->dailyUpdate();

		p = (President*) p->sister;
	}
#ifdef DEBUG
	check(__FILE__, __LINE__);
#endif
}


#endif

/*
 * Conversion between ID and Pointer
 *
 * IDs are encoded based on a unit's position in the tree
 *   B31 : Set if in unallocated list, in which case lower word is entry in list
 *   B30
 *   B29
 *   B28
 *   B27
 *   B26
 *   B25	\
 *   B24	|
 *   B23	|- Regiment
 *   B22	|
 *   B21 /
 *   B20	\
 *   B19	|
 *   B18	|- Brigade
 *   B17 |
 *   B16	/
 *   B15	\
 *   B14	|
 *   B13	|- Division
 *   B12	|
 *   B11	/
 *   B10	\
 *   B9  |
 *   B8  |- Corps (1..31)
 *   B7  |
 *   B6  /
 *   B5  \
 *   B4  |
 *   B3  +- Army (1..31)
 *   B2  |
 *   B1  /
 *   B0  :- Side (0/1)
 *
 * A value of 0 in any field means it is unused
 * eg, 2nd Division of the 3rd Corps 1st Army on side 0
 *
 *  00000 00000 00000 00010 00011 00001 0
 *
 */

#define IDisUnallocated (1 << 31)
#define IDwhichSide (1 << 30)
#define IDillegal UnitID(-1)

UnitID OrderBattle::getID(Unit* u) const
{
	if(u)
	{
		if(!u->superior && (u->getRank() == Rank_Regiment))
		{
			President* p = getSides();

			int count = 0;

			while(p)
			{
				count = 0;

				Unit* r = p->freeRegiments;
				while(r && (r != u))
				{
					count++;
					r = r->sister;
				}

				if(r)
					break;

				p = (President*) p->sister;
			}

			if(p)
			{
				UnitID result = IDisUnallocated ;

				if(p->getSide() == SIDE_CSA)
					result |= IDwhichSide;

				result |= count;

				return result;
			}
			else
				return IDillegal;
		}
		else
		{
			UnitID result = 0;

			while(u->superior)
			{
				result <<= 5;
				result |= u->getOrgPosition() + 1;
				u = u->superior;
			}

			result <<= 1;

			if(u->getSide() == SIDE_CSA)
				result |= 1;

			return result;
		}
	}
	else
		return IDillegal;
}

Unit* OrderBattle::getUnitPtr(UnitID id) const
{
	if(id == IDillegal)
		return 0;

	if(id & IDisUnallocated)
	{
		President *p;
		if(id & IDwhichSide)
			p = getPresident(SIDE_CSA);
		else
			p = getPresident(SIDE_USA);

		Unit* u = p->freeRegiments;
		int count = id & 0xffff;
		while(count-- && u)
			u = u->sister;
		return u;
	}
	else
	{
		Unit* u;

		if(id & 1)
			u = getPresident(SIDE_CSA);
		else
			u = getPresident(SIDE_USA);

		id >>= 1;

		while(id && u)
		{
			int count = id & 0x1f;
			id >>= 5;

			u = u->child;

			while(--count && u)
			{
				u = u->sister;
			}
		}

		return u;
	}
}


UnitID OrderBattle::getID(General* g) const
{
	Unit* u = g->allocation;

	if(u)
		return getID(u);
	else
	{	// Return entry in Free General List
		UnitID result = IDisUnallocated;

		if(g->getSide() == SIDE_CSA)
			result |= IDwhichSide;

		President* p = getPresident(g->getSide());

		result |= p->freeGenerals->getID(g);

		return result;
	}
}

General* OrderBattle::getGeneralPtr(UnitID id) const
{
	if(id & IDisUnallocated)
	{
		President *p;
		if(id & IDwhichSide)
			p = getPresident(SIDE_CSA);
		else
			p = getPresident(SIDE_USA);

		return p->freeGenerals->find(id & 0xffff);
	}
	else
	{
		Unit* u = getUnitPtr(id);
		return u->general;
	}
}

#if !defined(TESTBATTLE) && !defined(BATEDIT)

/*
 * Promote Generals up through the hierarchy
 */

/*
 * Fill in missing General slots
 */

void OrderBattle::makeGeneral(Unit* target)
{
#if defined(DEBUG) && !defined(CAMPEDIT)
	static int recursing = 0;

	// if(!recursing++)
	recursing++;
	obLog.printf("--- makeGeneral (%d)", recursing);
#endif

	ASSERT(target != 0);
	ASSERT(target->rank <= Rank_Brigade);
	ASSERT(target->general == 0);

	Side side = target->getSide();
	President* p = target->getPresident();

	while(target)
	{
#if defined(DEBUG) && !defined(CAMPEDIT)
		obLog.printf("Making General for %s", target->getName(True));
#endif
		/*
		 * Find child with best general
		 */

		General* bestGeneral = 0;

		if(target->getRank() < Rank_Brigade)
		{
			Unit* u = target->child;

			Unit* bestChild = 0;

			while(u)
			{
				if(u->general && (!bestGeneral || generalBetter(u->general, bestGeneral)))
				{
					bestChild = u;
					bestGeneral = u->general;
				}
				u = u->sister;
			}

#if defined(DEBUG) && !defined(CAMPEDIT)
			if(bestGeneral)
				obLog.printf("Promoting %s from %s",
					bestGeneral->getName(), bestChild->getName(True));
#endif
		}

		if(!bestGeneral)
		{
			/*
			 * Get best one from free list
			 */

			GeneralList* glist = p->getFreeGenerals();
			int NumFreeGens = glist->entries();

			if(NumFreeGens)
			{
				GeneralIter iter = *glist;

				while(++iter)
				{
					General* g1 = iter.current();

					if(!g1->inactive && (!bestGeneral || (generalBetter(g1, bestGeneral))))
						bestGeneral = g1;
				}
			}

#if defined(DEBUG) && !defined(CAMPEDIT)
			if(bestGeneral)
				obLog.printf("Taken %s from free General List",
					bestGeneral->getName());
#endif
		}	

		/*
		 * Make a brand new general
		 */

		if(!bestGeneral)
		{
			char name[25];

			sprintf(name, "%c. %c. %s",
				(char) 'A' + game->gameRand.getB(23),
				(char) 'A' + game->gameRand.getB(23),
				genNames[game->gameRand(nRandomNames)]);

			bestGeneral = new General(name, side);

			bestGeneral->efficiency = game->gameRand(MaxAttribute);
			bestGeneral->ability    = game->gameRand(MaxAttribute);
			bestGeneral->aggression = game->gameRand(MaxAttribute);
			bestGeneral->experience = game->gameRand(MaxAttribute);

#if defined(DEBUG) && !defined(CAMPEDIT)
			obLog.printf("Created new General %s", bestGeneral->getName());
#endif
		}

		ASSERT(bestGeneral != 0);

		if(bestGeneral)
			assignGeneral(bestGeneral, target);

		/*
		 * Make sure children have generals
		 */

		if(target->getRank() < Rank_Brigade)
		{
			Unit* u = target->child;

			target = 0;			// Avoid recursion if only one child

			while(u)
			{
				if(!u->general)
				{
					if(target)
						makeGeneral(target);
					target = u;
				}
				u = u->sister;
			}
		}
		else
			target = 0;
	}

#if defined(DEBUG) && !defined(CAMPEDIT)
	if(!--recursing)
	{
		obLog.printf("");
		obLog.close();
	}
#endif

}

/*
 * Tidy Up the Order of Battle:
 *		1) Remove Childless Commanders
 *    2) Make sure at least one attached Unit
 *    3) Set up seperated flags correctly
 */

Boolean OrderBattle::tidyUnit(Unit* u)
{
	while(u)
	{
		Unit* u1 = u;
		u = u->sister;

		/*
		 * Force values for top/bottom ranks, which can never
		 * be attached/dettached
		 */

		if(u1->getRank() == Rank_Army)
		{
			u1->seperated = True;
			u1->joining = False;
			u1->reallySeperated = True;
		}
		else if(u1->getRank() == Rank_President)
		{
			u1->seperated = True;
			u1->joining = False;
			u1->reallySeperated = True;
		}
		else if(u1->getRank() == Rank_Regiment)
		{
			u1->seperated = False;
			u1->joining = False;
			u1->reallySeperated = False;
		}



		if( (u1->getRank() < Rank_Regiment) && (u1->getRank() > Rank_President) )
		{
			if(u1->childCount)
			{
				/*
				 * Make sure every unit has at least one attached unit
				 */

				int realCount = 0;
				Unit* c = u1->child;
				Unit* child = 0;
				while(c)
				{
					// if(!c->isReallyIndependant() || c->isJoining())
					if(c->getAttachMode() != Detached)
						realCount++;
					else
						if(!child)
							child = c;

					c = c->sister;
				}

				ASSERT(child != 0);

				if(realCount == 0)
				{
#if defined(DEBUG) && !defined(CAMPEDIT)
					obLog.printf("Rejoining %s because no dependant children",
						child->getName(True));
#endif
					u1->rejoin();
				}

				if(tidyUnit(u1->child))
					return True;
			}
			if(!u1->childCount)
			{
#if !defined(CAMPEDIT) && !defined(TESTBATTLE) && !defined(BATEDIT)
#ifdef DEBUG
				obLog.printf("Removing %s because has no children",
					u1->getName(True));
				obLog.close();
#endif
#endif

				// assignUnit(u1, 0);
				// delete u1;
				deleteUnit(u1);	// Fixed: 1st Feb 1997 to ensure cleaned up correctly
				return True;
			}
		}


		/*
		 * Update seperated flags (Children already processed)
		 */

		Boolean hasSeperated = False;
		Boolean hasRealSeperated = False;
		Unit* c = u1->child;
		while(c)
		{
			if( (c->getAttachMode() != Attached) || c->hasDetached())
				hasSeperated = True;
			if(c->isDetached() || c->hasReallyDetached())
				hasRealSeperated = True;
			c = c->sister;
		}
		u1->hasSeperated = hasSeperated;
		u1->hasReallySeperated = hasRealSeperated;

	}

	return False;
}

void OrderBattle::tidy()
{
	Boolean doing = True;

	do
	{
		doing = False;

		Unit* u = sides;
		while(u)
		{
			if(tidyUnit(u))
			{
				doing = True;
				break;
			}

			u = u->sister;
		}
	} while(doing);
#ifdef DEBUG
	check(__FILE__, __LINE__);
#endif
}

#endif	// TESTBATTLE

#if !defined(CAMPEDIT) && !defined(TESTBATTLE) && !defined(BATEDIT)

/*
 * Check for any regiments that are too small to continue and
 * merge them with somewhere else.
 *
 * This is made slightly complicated because merge regiment can result
 * in commanders further up the tree being removed, so the routine exits
 * and restarts every time a regiment is merged.
 */

Boolean OrderBattle::clearupDead(Unit* u)
{
	if(u->getRank() == Rank_Regiment)
	{
		Regiment* r = (Regiment*) u;

		Strength str = r->strength + r->stragglers;

		if(!r->strength || ((r->strength + r->stragglers) < 50))
		{
			Regiment* rMerge = r->findBestMergeable(r->getTopCommand(), r);

#ifdef DEBUG
			obLog.printf("Almost dead %s is merging with %s",
				r->getName(True), rMerge ? rMerge->getName(True) : "Nobody");
			obLog.close();
#endif

			if(rMerge)
				mergeRegiment(r, rMerge);	// This could result in u being removed
			else	// Kill completely?
				assignUnit(r, 0);

#ifdef DEBUG
			check(__FILE__, __LINE__);
#endif
			return True;
		}
	}
	else
	{
		u = u->child;
		while(u)
		{
			if(clearupDead(u))
				return True;
			u = u->sister;
		}
	}

#ifdef DEBUG
	check(__FILE__, __LINE__);
#endif

	return False;
}

void OrderBattle::clearupDead()
{
	Boolean keepGoing = True;

	do
	{
		keepGoing = False;

		Unit* u = sides;
		while(u)
		{
			if(clearupDead(u))
			{
				keepGoing = True;
				break;
			}

			u = u->sister;
		}
	} while(keepGoing);

#ifdef DEBUG
	check(__FILE__, __LINE__);
#endif

	tidy();

#ifdef DEBUG
	check(__FILE__, __LINE__);
#endif
}

#endif

#ifdef DEBUG

/*
 * Give the order of battle a check that correct number of children
 * are present.
 */

void OrderBattle::checkUnit(Unit* u, const char* file, int line)
{
	int i = 0;
	Unit* unit = u->child;

	while(unit && (i < 20))		// 20 should be enough for cyclic checking
	{
		i++;

		unit = unit->sister;
	}

	if(i >= 20)
		throw GeneralError("File: %s, Line: %d\n%s: Probably looping children",
			file, line,
			u->getName(True));

	if(i != u->childCount)
		throw GeneralError("File: %s, Line: %d\n%s: childCount=%d, actual children=%d\n",
			file, line,
			u->getName(True),
			(int) u->childCount,
			i);

	if( (u->getRank() == Rank_Brigade) && (i > MaxRegimentsPerBrigade) )
		throw GeneralError("File: %s, Line: %d\n%s: more than 6 regiments (%d) in brigade\n",
			file, line,
			u->getName(True),
			i);

	/*
	 * Check independant/joining flags
	 */

	int val = 0;
#if 0
	if(u->seperated)
		val |= 4;
	if(u->reallySeperated)
		val |= 2;
	if(u->joining)
		val |= 1;

	if( (val != 0) &&
		 (val != 6) &&
		 (val != 3) &&
		 (val != 4) )
		 throw GeneralError("File: %s, Line: %d\nInvalid seperated flags for %s (%d)",
		 	file, line,
		 	u->getName(True),
			val);
#else
	if(u->reallySeperated)
		val |= 2;
	if(u->joining)
		val |= 1;

	if(val == 1)
		 throw GeneralError("File: %s, Line: %d\nInvalid seperated flags for %s (%d)",
		 	file, line,
		 	u->getName(True),
			val);
#endif

	Boolean hasSeperated = False;
	Boolean hasReallySeperated = False;
	unit = u->child;
	while(unit)
	{
		if(unit->seperated || unit->hasSeperated)
			hasSeperated = True;
		if(unit->reallySeperated || unit->hasReallySeperated)
			hasReallySeperated = True;

		unit = unit->sister;
	}
	if(hasSeperated != u->hasSeperated)
		throw GeneralError("File: %s, Line: %d\n%s seperated flag wrong",
			file, line,
			u->getName(True));
	if(hasReallySeperated != u->hasReallySeperated)
		throw GeneralError("File: %s, Line: %d\n%s reallySeperated flag wrong",
			file, line,
			u->getName(True));


	/*
	 * Check the children as well.
	 */

	unit = u->child;

	while(unit)		// 20 should be enough for cyclic checking
	{
		checkUnit(unit, file, line);
		unit = unit->sister;
	}


}

void OrderBattle::check(const char* file, int line)
{
	Unit* u = sides;
	int count = 0;

	while(u && (count < 10))
	{
		count++;
		checkUnit(u, file, line);

		u = u->sister;
	}

	if(count > 2)
		throw GeneralError("File: %s, Line: %d\nThere are %d sides", file, line, count);
}

#endif


