/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Simplified Linked List
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.4  1994/07/19  19:53:03  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/03/10  14:27:18  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/02/17  14:57:44  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/02/15  23:22:28  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "ptrlist.h"

/*======================================================================
 * Doubly Linked List
 */

/*
 * Constructor for list
 */

DList::DList()
{
	first = 0;
	last = 0;
	count = 0;
}

void DList::clear()
{
	while(entries())
		get();
}

/*
 * Add Item to end of list
 */

void DList::append(void* data)
{
	DPtrLink* node = new DPtrLink;
	node->data = data;
	node->next = 0;
	node->prev = last;
	if(last)
		last->next = node;
	last = node;

	if(!first)
		first = node;

	count++;
}

/*
 * Add item to beginning of list
 */

void DList::insert(void* data)
{
	DPtrLink* node = new DPtrLink;
	node->data = data;
	node->next = first;
	node->prev = 0;
	if(first)
		first->prev = node;
	first = node;

	if(!last)
		last = node;

	count++;
}

void* DList::find(int n) const
{
	DPtrLink* link = first;

	while(link && n--)
	{
		link = link->next;
	}

	if(link)
		return link->data;
	else
		return 0;
}

int DList::getID(void* el) const
{
	int count = 0;

	DPtrLink* link = first;
	while(link && (link->data != el))
	{
		link = link->next;
		count++;
	}

	if(link)
		return count;
	else
		return -1;
}

void* DList::findLast() const
{
	if(last)
		return last->data;
	else
		return 0;
}

/*
 * Get and remove 1st item from list
 */

void* DList::get()
{
	if(first)
	{
		count--;
		DPtrLink* node = first;

		first = node->next;
		first->prev = 0;

		if(last == node)
			last = 0;

		void* data = node->data;
		delete node;

		return data;
	}
	else
		return 0;
}

void DList::forAll(ForAllFunction function, void* data)
{
	DPtrLink* node = first;

	while(node)
	{
		DPtrLink* next = node->next;

		function(node->data, data);

		node = next;
	}
}

void DList::removeNode(DPtrLink* link)
{
	if(link->prev)
		link->prev->next = link->next;
	else
		first = link->next;

	if(link->next)
		link->next->prev = link->prev;
	else
		last = link->prev;

	delete link;

	count--;
}

void DList::remove(void* data)
{
	DPtrLink* link = first;

	while(link && (link->data != data))
		link = link->next;

	if(link)
		removeNode(link);
}

/*==============================================================
 * Double Link Iterator
 */

/*
 * Constructor
 */

DListIter::DListIter(DList& list)
{
	container = &list;
	link = 0;
}

/*
 * Insert before current entry
 */

void DListIter::insert(void* data)
{
	if(link)
	{
		DPtrLink* node = new DPtrLink;
		node->data = data;

		node->next = link;
		node->prev = link->prev;

		if(link->prev)
			link->prev->next = node;
		else
			container->first = node;

		link->prev = node;
	}
	else
		container->insert(data);
	container->count++;
}

/*
 * Append after current entry
 */

void DListIter::append(void* data)
{
	if(link)
	{
		DPtrLink* node = new DPtrLink;
		node->data = data;

		node->prev = link;
		node->next = link->next;

		if(link->next)
			link->next->prev = node;
		else
			container->last = node;

		link->next = node;
	}
	else
		container->append(data);

	container->count++;
}

/*
 * Delete current item and set current to previous item
 */

void DListIter::remove()
{
	if(link)
	{
		DPtrLink* node = link;
		link = node->prev;

		container->removeNode(node);
	}
}

void* DListIter::current() const
{
	if(link)
		return link->data;
	else
		return 0;
}

int DListIter::find(void* data)
{
	link = container->first;

	while(link)
	{
		if(link->data == data)
			return 1;				// Success

		link = link->next;
	}

	return 0;						// Not found
}

/*
 * Advance to next item
 * return 0 if at end of list, or non-zero
 */

int DListIter::operator ++()
{
	if(link)
		link = link->next;
	else
		link = container->first;
	return (link != 0);
}

int DListIter::operator --()
{
	if(link)
	{
		link = link->prev;
		return (link != 0);
	}
	else
		return 0;
}

/*======================================================================
 * Singly Linked List
 */

/*
 * Constructor for list
 */

SList::SList()
{
	first = 0;
	last = 0;
	count = 0;
}

void SList::clear()
{
	while(entries())
		get();
}

/*
 * Add Item to end of list
 */

void SList::append(void* data)
{
	SPtrLink* node = new SPtrLink;
	node->data = data;
	node->next = 0;
	if(last)
		last->next = node;
	last = node;

	if(!first)
		first = node;

	count++;
}

/*
 * Add item to beginning of list
 */

void SList::insert(void* data)
{
	SPtrLink* node = new SPtrLink;
	node->data = data;
	node->next = first;
	first = node;

	if(!last)
		last = node;

	count++;
}

void* SList::find(int n) const
{
	SPtrLink* link = first;

	while(link && n--)
		link = link->next;

	if(link)
		return link->data;
	else
		return 0;
}

void* SList::findLast() const
{
	if(last)
		return last->data;
	else
		return 0;
}

/*
 * Get and remove 1st item from list
 */

void* SList::get()
{
	if(first)
	{
		count--;

		SPtrLink* node = first;

		first = node->next;

		if(last == node)
			last = 0;

		void* data = node->data;
		delete node;

		return data;
	}
	else
		return 0;
}

Boolean SList::get(void*& value)
{
	if(first)
	{
		count--;

		SPtrLink* node = first;

		first = node->next;

		if(last == node)
			last = 0;

		value = node->data;
		delete node;

		return True;
	}
	else
		return False;
}



void SList::forAll(ForAllFunction function, void* data)
{
	SPtrLink* node = first;

	while(node)
	{
		SPtrLink* next = node->next;

		function(node->data, data);

		node = next;
	}
}

/*==============================================================
 * Singly Link Iterator
 */

/*
 * Constructor
 */

SListIter::SListIter(SList& list)
{
	container = &list;
	link = 0;
}

/*
 * Append after current entry
 */

void SListIter::append(void* data)
{
	if(link)
	{
		SPtrLink* node = new SPtrLink;
		node->data = data;

		node->next = link->next;

		if(!link->next)
			container->last = node;
		link->next = node;

	}
	else
		container->append(data);

	container->count++;
}

void* SListIter::current() const
{
	if(link)
		return link->data;
	else
		return 0;
}

/*
 * Advance to next item
 * return 0 if at end of list, or non-zero
 */

int SListIter::operator ++()
{
	if(link)
		link = link->next;
	else
		link = container->first;
	return (link != 0);
}

