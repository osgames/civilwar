/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Remap a whole load of ILBM pictures to use a common palette
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.4  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.3  1994/05/21  13:16:19  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/05/19  17:44:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/04/05  12:28:09  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include <iostream.h>
#include <iomanip.h>
// #include <fstream.h>
#include <stdlib.h>
#include <stdio.h>
#include <dos.h>
#include <wclist.h>
#include <wclistit.h>


#include "ilbm.h"
#include "ilbmdata.h"
#include "image.h"
#include "palette.h"

class RGBColour {
public:
	UBYTE red;
	UBYTE green;
	UBYTE blue;

	RGBColour(UBYTE* ad)
	{
		red = ad[0];
		green = ad[1];
		blue = ad[2];
	}

	UBYTE closestColour(Palette& p);
	int colourDiff(const RGBColour& col);
};

int cdiff(UBYTE v1, UBYTE v2)
{
	if(v1 > v2)
		return v1 - v2;
	else
		return v2 - v1;
}

int RGBColour::colourDiff(const RGBColour& col)
{
	int rDiff = cdiff(red, col.red);
	int bDiff = cdiff(blue, col.blue);
	int gDiff = cdiff(green, col.green);

	return rDiff * rDiff + bDiff * bDiff + gDiff * gDiff;
}

/*
 * Find closest colour in a palette
 */

UBYTE RGBColour::closestColour(Palette& pal)
{
	UBYTE best = 0;
	int bestDiff = colourDiff(pal.getColour(0));

	for(int i = 1; i < 256; i++)
	{
		RGBColour col = pal.getColour(i);

		int diff = colourDiff(col);

		if(diff < bestDiff)
		{
			bestDiff = diff;
			best = UBYTE(i);
		}
	}

	if(bestDiff > 100)
		cout << "Warning: Colour " << int(red) << "," << int(green) << "," << int(blue) <<
			" matched badly with a difference of "  << bestDiff << endl;

	return best;
}


#if 0

void RGBColour::rgbToHSV(int& h, int& s, int& v)
{
	UBYTE max;
	UBYTE min;

	max = green;
	if(red > max)
		max = red;
	if(blue > max)
		max = blue;

	min = green;
	if(red <	min)
		min = red;
	if(blue < min)
		min = blue;

	v = max;				// Set V

	/*
	 * Calculate Saturation
	 */

	if(max)
		s = ((max - min) * 256) / max;
	else
		s = 0;

	/*
	 * Hue
	 */

	if(s)
	{
		int delta = max - min;

		if(red == max)
			h = ((green - blue) * 60) / delta;
		else if(green == max)
			h = ((blue - red) * 60) / delta + 120;
		else	// assume blue
			h = ((red - green) * 60) / delta + 240;

		if(h < 0)
			h += 360;
		if(h >= 360)
			h -= 360;

	}
	else
		h = 0;			// Should be undefined!


}

#endif


/*
 * Process a file
 */


void processPicture(const char* fileName, Palette& mainPalette)
{
	Image image;
	Palette palette;
	BMHD header;

	if(readILBM(fileName, &image, &palette, &header) == 0)
	{
#ifdef DEBUG
		cout << image.getWidth() << " by " << image.getHeight() << endl;
#endif
		/*
		 * Histogram
		 */

		int used[256];

		UBYTE* ad = image.getAddress();
		int pixelCount = image.getWidth() * image.getHeight();
		while(pixelCount--)
			used[*ad++]++;

		/*
		 * Make a remap table
		 */

		UBYTE remap[256];

		for(int i = 0; i < 256; i++)
		{
			if(used[i])
			{
				RGBColour col = palette.getColour(i);
				UBYTE n = col.closestColour(mainPalette);

				remap[i] = n;

#ifdef DEBUG
				if(i != n)
					cout << "Remap " << int(i) << " to " << int(n) << endl;
#endif
			}
		}

		/*
		 * Go through a pixel at a time and remap it
		 */

		ad = image.getAddress();		// Address of 1st pixel
		pixelCount = image.getWidth() * image.getHeight();
		while(pixelCount--)
		{
			UBYTE c = *ad;
			UBYTE n = remap[c];

			*ad++ = n;
		}

		/*
		 * Write it out to a file
		 */

		writeILBM(fileName, image, mainPalette, remap[header.transparentColor]);
	}

	/*
	 * Image gets freed automatically during destructor
	 */

}

/*
 * Usage:
 *		palcount palpic [filename]...
 *
 * filename may include DOS wildcards
 * palpic is an LBM containing the palette we require.
 */


void main(int argc, char* argv[])
{
	Boolean gotPalette = False;
	Palette palette;

	int i = 0;

	while(++i < argc)
	{
		const char* fileName = argv[i];

		if(!gotPalette)
		{
#ifdef DEBUG
			cout << "Reading palette file: " << fileName << endl;
#endif

			if(readILBM(fileName, 0, &palette) != 0)
			{
				cout << "Error reading palette from " << fileName << endl;
				return;
			}

			gotPalette = True;
		}
		else
		{
			char fullpath	[_MAX_PATH];
			char drive		[_MAX_DRIVE];
			char dir			[_MAX_DIR];
			char fname		[_MAX_FNAME];
			char ext			[_MAX_EXT];

			_fullpath(fullpath, fileName, _MAX_PATH);
			_splitpath(fullpath, drive, dir, fname, ext);

			struct find_t fileInfo;

			if(!_dos_findfirst(fileName, _A_NORMAL, &fileInfo))
			{
				do
				{
					sprintf(fullpath, "%s%s%s", drive, dir, fileInfo.name);
#ifdef DEBUG
					cout << "Reading: " << fullpath << ", " << fileInfo.size << " bytes" << endl;
#endif
					processPicture(fullpath, palette);

				} while(!_dos_findnext(&fileInfo));
			}
		}
	}
}

