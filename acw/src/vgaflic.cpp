/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	FLI Tester
 *
 * Actually plays FLI files to a SVGA screen
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:54  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <fstream.h>
#include <conio.h>
#include <ctype.h>
#include "vgascrn.h"
#include "palette.h"
#include "flicread.h"
#include "timer.h"
#include "sound.h"

class FlicSystem {
public:
	Vgascr* screen;
public:
	FlicSystem();
	~FlicSystem();
};

FlicSystem::FlicSystem()
{
	screen = new Vgascr( _MRES256COLOR );
	timer = new Timer;
}

FlicSystem::~FlicSystem()
{
	delete screen;
	delete timer;
	timer = 0;
}


void usage()
{
	cout << "\nUsage: vgaflic [-1] filename\n";
	cout << "  -1 : loop play until key press\n\n";
}

void main(int argc, char* argv[])
{
	char *fliName = 0;
	char* musName = 0;

	Boolean playOnce = True;

	int i = 0;
	while(++i < argc)
	{
		char* arg = argv[i];

		if((*arg == '-') || (*arg == '/'))
		{
			switch(toupper(arg[1]))
			{
			case '1':
				playOnce = False;
				break;
			default:										  
				usage();
				return;
			}
		}
		else
		{
			if(fliName)
			{
				if(musName)
				{
					usage();
					return;
				}
				else
					musName = arg;
			}
			else
				fliName = arg;
		}
	}
	if(!fliName)
	{
		usage();
		return;
	}


	try
	{
		// FlicRead fli(fliName);	// This opens it

		FlicRead fli(fliName, True);	// This opens it using 64 colour palette

		if(fli.getError())
		{
			cout << "Error in FLIC file " << fliName << endl;
			return;
		}

		const FlicHead* head = fli.getHeader();

		// cout << "Header for " << fliName << endl;
		// cout << "Size: " << head->size << endl;
		// cout << "Type: " << hex << head->type << dec << endl;
		// cout << "frames: " << head->frames << endl;
		// cout << "width: " << head->width << endl;
		// cout << "height: " << head->height << endl;
		// cout << "depth: " << head->depth << endl;
		// cout << "flags: " << head->flags << endl;
		// cout << "speed: " << head->speed << endl;

		unsigned int waitTime = (head->speed * timer->ticksPerSecond) / 1000;

		// if ( waitTime >= 2 ) waitTime >>= 1;

		FlicSystem system;
		Palette64* pal = (Palette64*)&fli.palette;

		system.screen->setFlicPosition( head->height, head->width, True );

		sound.init();
		
		MODULE* song = 0;
		if(musName)
			song = sound.loadMusic( "dbdata\\cryfree.amf" );

		if(song) sound.playMusic(song, True);

		#ifdef DEBUG
		int minimum = 0xffff;
		int maximum = 0;
		int average = 0;
		int frameCount = 0;
		#endif

		while(!kbhit() && (!playOnce || !fli.finished))
		{
		#ifdef DEBUG
			int tcount = timer->getCount();
		#endif
			
			fli.nextFrame();

		#ifdef DEBUG
			tcount = timer->getCount() - tcount;

			if ( tcount < minimum ) minimum = tcount;
			else if ( tcount > maximum ) maximum = tcount;

			average += tcount;
			frameCount++;
		#endif

			/*
			 * Blit it to screen
			 */
			
			timer->waitRel( waitTime );

			system.screen->waitForVerBlank();

			// setHardwarePalette(0, 256, fli.palette.colors);

			if(fli.paletteChanged)
				setHardwarePalette(0, 256, pal->colors);

			system.screen->blit(&fli.image);
		}
		if ( kbhit() ) getch();

		if(song)
		{
			sound.stopMusic( song );
			sound.unloadSong( song );
		}

#ifdef DEBUG
		system.screen->resetScreen();

		cout << "Minimum time is " << minimum;
		cout << "\nMaximum time is " << maximum;
		cout << "\nAverage time is " << average/frameCount;

		cout << "\n\nPress a key";
		getch();
#endif
	}
	catch( GeneralError e )
	{
		cout << "Error:";
		if ( e.get() )
		 	cout << e.get();
		else
			cout << " ??";
		cout << endl;
		getch();
	}
	catch( ...)
	{
		cout << "Unknown error!\n";
		getch();
	}
}
