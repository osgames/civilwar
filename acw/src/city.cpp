/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Cities and Facilities
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.21  1994/07/28  18:57:04  Steven_Green
 * Added regimentCount to City data, and function to update it.
 *
 * Revision 1.15  1994/06/09  23:32:59  Steven_Green
 * Changes for reading from File
 * Changed Fonts
 *
 * Revision 1.13  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.8  1994/04/20  22:21:40  Steven_Green
 * Use TextWindow class instead of old Font class
 *
 * Revision 1.1  1994/01/20  20:04:00  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>

#include "city.h"
#include "game.h"
#include "mapwind.h"
#include "text.h"
#include "colours.h"
#include "campaign.h"
#include "camptab.h"
#include "campwld.h"
#include "language.h"
#include "myassert.h"

#ifdef DEBUG
#include "options.h"
#include "memptr.h"
#endif

#define Font_CityName			Font_EMFL_8
#define Font_SmallCityName		Font_JAME_5
#define Font_BigStateName		Font_EMFL15	// Font_EMFL32
#define Font_SmallStateName  	Font_EMFL10	// Font_EMFL15
#define Font_FacilityName		Font_JAME_5

#define StateColour 0x18		// Dark Brown

/*
 * Initialise State
 */

State::State() : MapObject(OT_State)
{
#ifdef CAMPEDIT
	fullName[0] = 0;
	shortName[0] = 0;
#else
	shortName = 0;
	fullName = 0;
#endif
	side = SIDE_None;
	regimentCount = 1;
}

State::~State()
{
#ifndef CAMPEDIT
	if(fullName)
		delete[] fullName;
	if(shortName)
		delete[] shortName;
#endif
}

/*
 * Initialise a Facility
 */

Facility::Facility() :
	MapObject(OT_Facility)
{
	owner = NoFacility;
#ifdef CAMPEDIT
	name[0] = 0;
#else
	name = 0;
#endif

	facilities = 0;
	state = NoState;
	side = SIDE_None;
#ifndef CAMPEDIT
	railSection = NoRailSection;
	railCount = 0;
#endif
	railCapacity = 0;
	freeRailCapacity = 0;
	fortification = 0;
	// wagons = 0;

#if !defined(CAMPEDIT)
#ifdef CHRIS_AI
	attacking = False;
	reinforcing = False;
	AISkipFlag = False;
#endif
	aiDefend = 0;
#ifdef DEBUG
	aiAttack = 0;
#endif
#endif

	blockadeRunners = 0;
	waterCapacity = 0;
	freeWaterCapacity = 0;
	waterZone = NoWaterZone;

	occupier = 0;
	sieger = 0;
	victory = 0;

	supplies = MaxSupply;
}

#ifdef CREATE_CITIES

Facility::Facility(const char* s, const Location& l, Side sd, FacilityFlag f) :
	MapObject(l, OT_Facility)
{
	owner = NoFacility;
#ifndef CAMPEDIT
	name = new char[strlen(s) + 1];
#endif

	strcpy(name, s);
	facilities = f;
	state = NoState;
	side = sd;
#ifndef CAMPEDIT
	railSection = NoRailSection;
	railCount = 0;
#endif
	railCapacity = 0;
	freeRailCapacity = 0;
	fortification = 0;
	// wagons = 0;

#ifdef CHRIS_AI
	attacking = False;
	reinforcing = False;
	AISkipFlag = False;
#endif
	aiPriority = 0;
	aiCV = 0;
	aiNeedAction = False;

	blockadeRunners = 0;
	waterCapacity = 0;
	freeWaterCapacity = 0;
	waterZone = NoWaterZone;

	occupier = 0;
	sieger = 0;

	victory = 0;
	supplies = MaxSupply;
}

#endif	// CREATE_CITIES

Facility::~Facility()
{
#ifndef CAMPEDIT
	if(name)
		delete[] name;
#endif

	if(occupier)
	{
		occupier->occupying = 0;
		occupier->siegeAction = SIEGE_None;
	}
}

/*
 * get the supply level taking into account difficulty level
 */

Supply Facility::getSupplyLevel() const
{
#ifndef CAMPEDIT
	if(game->getDifficulty(Diff_Supply) == Simple)
		return MaxSupply;
	else
#endif
		return supplies;
}

City::City()
{
	type = 0;
	size = 0;

	resource.values[R_Human]     = 0;	// MaxResource;
	resource.values[R_Horses]    = 0;	// MaxResource;
	resource.values[R_Food] 	  = 0;	// MaxResource;
	resource.values[R_Materials] = 0;	// MaxResource;
}

#ifdef CREATE_CITIES
City::City(const char* s, const Location& l, Side owner, FacilityFlag f) :
		Facility(s, l, owner, f|F_City),
		resource()
{
	type = CityType(game->gameRand(4));
	size = CitySize(game->gameRand(64));

	facilities |= F_City;

#ifndef CAMPEDIT
	resource.values[R_Human]     = ResourceVal(game->gameRandResourceSet::getMaxVal(R_Human)));
	resource.values[R_Horses]    = ResourceVal(game->gameRandResourceSet::getMaxVal(R_Horses)));
	resource.values[R_Food] 	  = ResourceVal(game->gameRandResourceSet::getMaxVal(R_Food)));
	resource.values[R_Materials] = ResourceVal(game->gameRandResourceSet::getMaxVal(R_Materials)));
#endif	// CAMPEDIT
}
#endif	// CREATE_CITIES

/*
 * Draw a state
 */

MapPriority State::isVisible(MapWindow* map) const
{
#ifdef CAMPEDIT
	if(map->displayType == MapWindow::Disp_Moveable)
	{
		if(highlight)
			return Map_Highlight;
		else
			return Map_State;
	}
	else
		return Map_NotVisible;
#else
	return (map->displayType == MapWindow::Disp_Static) ? Map_State : Map_NotVisible;
#endif
}

void State::mapDraw(MapWindow* map, Region* bm, Point where)
{
#ifndef CAMPEDIT
	if(map->displayType == MapWindow::Disp_Static)
#endif
	{
		TextWindow window(bm, (map->mapStyle == MapWindow::Full) ? Font_SmallStateName : Font_BigStateName);
#ifdef CAMPEDIT
		if(highlight)
			window.setColours(White);
		else
#endif
		window.setColours(StateColour);

		char* name = fullName;

#ifdef CAMPEDIT
		if((map->mapStyle == MapWindow::Full) && shortName[0])
			name = shortName;
#else
		if((map->mapStyle == MapWindow::Full) && shortName)
			name = shortName;
#endif

		UWORD width = window.getFont()->getWidth(name);
		UWORD height = window.getFont()->getHeight();

		window.setPosition(where - Point(width/2, height/2));

		window.draw(name);
	}
}

/*
 * Draw a City
 *
 * This will need updating, when we have names display on/off etc
 */

MapPriority City::isVisible(MapWindow* map) const
{
#ifdef CAMPEDIT
	if(map->displayType == MapWindow::Disp_Moveable)
	{
		if(highlight)
			return Map_Highlight;
		else
			return Map_City;
	}
	else
		return Map_NotVisible;
#else
	if(map->displayType == MapWindow::Disp_Moveable)
	{
		if(highlight)
			return Map_Highlight;
		else
			return Map_City;
	}
	else
		return Map_City;
		// return Map_NotVisible;
#endif	// CAMPEDIT
}
 
void City::mapDraw(MapWindow* map, Region* bm, Point where)
{
	Boolean hl;

	if(map->displayType == MapWindow::Disp_Static)
		hl = False;
	else
		hl = highlight;

	Colour col;

	if(side == SIDE_CSA)
		col = CSAColour;
	else if(side == SIDE_USA)
		col = USAColour;
	else
		col = Neutral_Colour;

	if(map->mapStyle == MapWindow::Full)
	{
		bm->box(where.x - 1, where.y - 1, 3, 3, col);
		bm->frame(where.x - 2, where.y - 2, 5, 5, hl ? White : CityColour);
	}
	else
	{
		bm->box(where.x - 2, where.y - 2, 5, 5, col);
		bm->frame(where.x - 3, where.y - 3, 7, 7, hl ? White : CityColour);
	}

#ifndef CAMPEDIT
	ASSERT(name != 0);

	if(name && (hl || (map->displayType == MapWindow::Disp_Static)))
	{
#endif
		TextWindow window(bm, (map->mapStyle == MapWindow::Full) ? Font_SmallCityName : Font_CityName);

		if(hl)
			window.setColours(White);
		else
			window.setColours(Black);

		window.setPosition(where + Point(8, -6));
		window.draw(name);
#ifndef CAMPEDIT
	}
#endif


#ifdef DEBUG
#if !defined(CAMPEDIT)

	/*
	 * Display AIValue
	 */

	if(showAI)
	{
		TextWindow window(bm, Font_JAME_5);

		window.setPosition(where + Point(8, 2));
		window.setColours(White);
		window.wprintf("%d %d", (int) aiDefend, (int) aiAttack);
	}
#endif
#endif	// DEBUG

}


/*
 * Draw a facility
 *
 * Note cities are handled by their own class, so this must handle
 * everything else.
 *
 * Standalone facilities may be:
 *		railhead
 *		landing stage / port
 *		fortification
 *		minor town?
 */

MapPriority Facility::isVisible(MapWindow* map) const
{
#ifdef CAMPEDIT
	if( (map->displayType == MapWindow::Disp_Moveable) &&
		 (map->mapStyle == MapWindow::Zoomed) )
	{
		if(highlight)
			return Map_Highlight;
		else
			return Map_Facility;
	}
	else
		return Map_NotVisible;
#else
	if(!isRealTown())
		return Map_NotVisible;

	if(map->displayType == MapWindow::Disp_Moveable)
	{
		if(highlight)
			return Map_Highlight;
		else if(map->mapStyle == MapWindow::Zoomed)
			return Map_Facility;
		else
			return Map_NotVisible;
	}
	else
	{
		if(map->mapStyle == MapWindow::Zoomed)
			return Map_Facility;
		else
			return Map_NotVisible;
	}
#endif	// CAMP_EDIT
}

void Facility::mapDraw(MapWindow* map, Region* bm, Point where)
{
	if(map->mapStyle == MapWindow::Zoomed)
	{
		Boolean hl;
		
		if(map->displayType == MapWindow::Disp_Static)
			hl = False;
		else
			hl = highlight;

		Colour col;

		if(side == SIDE_CSA)
			col = CSAColour;
		else if(side == SIDE_USA)
			col = USAColour;
		else
			col = Neutral_Colour;

		if(facilities & F_City)
			throw GeneralError("Facility::draw(CITY) should be using City::draw()");
		else
		{
			bm->box(where.x - 1, where.y - 1, 3, 3, col);
			bm->frame(where.x - 2, where.y - 2, 5, 5, hl ? White : Black);
		}

#ifndef CAMPEDIT
		if(hl || (map->displayType == MapWindow::Disp_Static))
		{
#endif
			const char* name = getName();

			ASSERT(name != 0);

			if(name)
			{
				TextWindow window(bm, Font_FacilityName);

				if(hl)
					window.setColours(White);
				else
					window.setColours(Black);

				window.setPosition(where + Point(8, -6));
				window.draw(name);
			}
#ifndef CAMPEDIT
		}
#endif

#ifdef DEBUG

#if !defined(CAMPEDIT)
		/*
		 * Display AIValue
		 */

		if(showAI)
		{
			TextWindow window(bm, Font_JAME_5);

			window.setPosition(where + Point(8, 2));
			window.setColours(White);
			window.wprintf("%d %d", (int) aiDefend, (int) aiAttack);
		}
#endif
#endif	// DEBUG

	}
}

void FacilityList::clearAndDestroy()
{
	clear();
}


FacilityID FacilityList::getID(const Facility* f) const
{
	for(int i = 0; i < entries(); i++)
	{
		if(get(i) == f)
			return i;
	}
	return NoFacility;
}




ResourceSet* Facility::getGlobalResources()
{
	City* key = getKeyCity();
	if(key && (key->side == side) && (key->sieger == 0) && (sieger == 0))
		return key->getLocalResources();
	else
		return 0;
}

City* Facility::getKeyCity() const
{
	if(owner == NoFacility)
		return 0;

	Facility* f = campaign->world->facilities[owner];

#if !defined(CAMPEDIT)
	if(onOtherSide(f->side, side))
		return 0;
#endif

	return (City*) f;
}

#if !defined(CAMPEDIT)

/*===================================================================
 * Daily facility processing
 */

void FacilityList::dailyProcess()
{
	for(int i = 0; i < entries(); i++)
	{
		Facility* f = get(i);

		if(f)
			f->dailyProcess();
	}
}

void City::dailyProcess()
{
	/*
	 * Generate new resources based on city type and size
	 */

	if(size && (sieger == 0))
	{
		/*
		 * Change this so proportional to city size
		 *  size 0 produces resourceGeneration * 1/64
		 *  size 63 generates resourceGeneration * 64/64
		 *
		 * Change again so that:
		 *  size 0 produces rg * minR/64
		 *  size 63 produces rg * 64/64
		 */

		ResourceSet generated = resourceGeneration[type];

		// int value = size + 1;
		const int minR = 20;
		int value = (size * (64-minR)) / 64 + minR;

		if(waterZone != NoWaterZone)
			value += blockadeRunners * 4;
		if(value > 128)
			value = 128;

		generated.multiplyRatio(value, 64);

#ifdef DEBUG
		if(fastResources)
			generated.multiplyRatio(4, 1);

#endif


#ifdef DEBUG
		MemPtr<char> logBuffer(200);
		sprintf(logBuffer, "%-18s generated ", getNameNotNull());
		generated.print(logBuffer + strlen(logBuffer));
#endif


		/*
		 * Send proportion back to key city if connected
		 */

		City* keyCity = getKeyCity();

		if(keyCity && (keyCity->side == side) && (keyCity->sieger == 0))
		{
			keyCity->resource.addTax(generated, 10 + ( (blockadeRunners + 1) >> 1 ) );
			if ( blockadeRunners && game->gameRand.getB( 100 ) < 2 )
				blockadeRunners--;
		}

#ifdef DEBUG
		sprintf(logBuffer + strlen(logBuffer), " -tax = ");
		generated.print(logBuffer + strlen(logBuffer));
#endif

		/*
	 	 * Add what's left onto total
	 	 */

		resource += generated;

#ifdef DEBUG
		sprintf(logBuffer + strlen(logBuffer), " ==> ");
		resource.print(logBuffer + strlen(logBuffer));
		cLog.printf("%s", (char*) logBuffer);
#endif

		/*
		 * Update supplies as well
		 */

		int newSupply = supplies + (size * 16) / 64 + 1;
		if(newSupply > MaxSupply)
			newSupply = MaxSupply;

#ifdef DEBUG
		if(supplies != newSupply)
			cLog.printf("Supplies updated from %d to %d",
				(int) supplies,
				newSupply);
#endif

		supplies = newSupply;
	}

	Facility::dailyProcess();
}

/*
 * Daily process for a facility
 */

void Facility::dailyProcess()
{
	if(sieger != 0)
	{
#ifdef DEBUG
		cLog.printf("%s supplies not updated because of siege", getNameNotNull());
#endif
		if(supplies > 2)
			supplies -= 2;
	}
	else
	{

		/*
		 * Update supplies based on capital city
		 */

		int newSupply = supplies;

		if(facilities & F_Port)
			newSupply += 2;
		else if(facilities & F_LandingStage)
			newSupply++;

		City* city = getKeyCity();

		if(city && (city->side == side))
			newSupply += (city->supplies + 15) / 16;

		if(newSupply > MaxSupply)
			newSupply = MaxSupply;

#ifdef DEBUG
			cLog.printf("%s Supplies updated from %d to %d",
				getNameNotNull(),
				(int) supplies,
				newSupply);
#endif

		supplies = newSupply;
	}
}

#endif	// !CAMPEDIT


UBYTE City::allocateRegiment()
{
	ASSERT(state != NoState);

	if(state != NoState)
	{
		State& st = campaign->world->states[state];

		return st.regimentCount++;
	}
	else
	{
#ifdef DEBUG
		GeneralError("Facility %s doesn't have a state!", getNameNotNull());
#endif
		return 1;
	}
}




const char* Facility::getNameNotNull() const
{
	if(name)
		return name;
	else
		return language(lge_unNamed);
}

#if !defined(CAMPEDIT)

Facility* FacilityList::findClose(const Location& l, Distance maxDist)
{
	Facility* best = 0;
	Distance d;

	for(int i = 0; i < entries(); i++)
	{
		Facility* f = get(i);

		Distance d1 = distanceLocation(f->location, l);

		if((d1 < maxDist) && (!best || (d1 < d)))
		{
			best = f;
			d = d1;
		}
	}
	return best;
}

#endif
