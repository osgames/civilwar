/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Iterator for Order of Battle
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:54  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "unititer.h"
#include "generals.h"

#include "myassert.h"

/*
 * Initialise Unit Iterator
 *
 * IF onlyIndependant is set, then only units Ordered to be
 * seperate are returned.  Note that this is different than units
 * that are physically detached.
 *
 * IF real is set, then only Really detached units are returned.
 *
 * IF both are set then only detached and ordered units are returned
 */

void UnitIter::setup(Unit* u, Boolean onlyIndependant, Boolean real, Rank highest, Rank lowest)
{
	ASSERT(u != 0);
	ASSERT(u->rank <= lowest);
	ASSERT(u->rank <= highest);
	ASSERT(highest <= lowest);

	top = u;
	current = u;
	independant = onlyIndependant;
	realDetached = real;
	hiRank = highest;
	lowRank = lowest;

	//-------- Patch 23Jan96
	if((u->rank < hiRank) || (u->rank > lowRank))
		getNext();
	//-------- Patch 23Jan96
}

void UnitIter::reset()
{
	current = top;

	//-------- Patch 23Jan96
	if((current->rank < hiRank) || (current->rank > lowRank))
		getNext();
	//-------- Patch 23Jan96
}


// -------- Patch 23Jan96
/*
 * Set up next current unit
 */

void UnitIter::getNext()
{
	do
	{
		Boolean finished = False;
		Boolean winding = False;

		if(current)
		{
			while(!finished)
			{
				ASSERT(current != 0);

				if(!winding &&
					(current->rank < lowRank) &&
					current->childCount &&
					// (!independant || current->hasIndependant()) )
					(!independant || current->hasDetached()) &&
					(!realDetached || current->hasReallyDetached()))
				{
					current = current->child;
					// if(!independant || current->isIndependant())
					if( (!independant || current->isDetached()) &&
						 (!realDetached || (current->getAttachMode() != Attached)) )
							finished = True;
				}
				else if(current == top)
				{
					current = 0;
					finished = True;
				}
				else if(current->sister)
				{
					current = current->sister;
					// if(!independant || current->isIndependant())
					if( (!independant || current->isDetached()) &&
						 (!realDetached || (current->getAttachMode() != Attached)))
							finished = True;
					winding = False;
				}
				else
				{
					current = current->superior;
					winding = True;		// Prevent from going down children again
				}
			}
		}
	} while(current && (current->rank < hiRank));

#ifdef DEBUG
	if(current)
	{
		ASSERT(current->rank >= hiRank);
		ASSERT(current->rank <= lowRank);
		ASSERT(!independant || current->isDetached());
		ASSERT(!realDetached || (current->getAttachMode() != Attached));
	}
#endif
}

//-------- Patch 23Jan96

/*
 * Get current unit and advance to next
 */

Unit* UnitIter::get()
{
//-------- Patch 23Jan96
#if 0
	Unit* retVal;

	do
	{
		Boolean finished = False;
		Boolean winding = False;

		retVal = current;

		if(current)
		{
			while(!finished)
			{
				ASSERT(current != 0);

				if(!winding &&
					(current->rank < lowRank) &&
					current->childCount &&
					// (!independant || current->hasIndependant()) )
					(!independant || current->hasDetached()) &&
					(!realDetached || current->hasReallyDetached()))
				{
					current = current->child;
					// if(!independant || current->isIndependant())
					if( (!independant || current->isDetached()) &&
						 (!realDetached || (current->getAttachMode() != Attached)) )
							finished = True;
				}
				else if(current == top)
				{
					current = 0;
					finished = True;
				}
				else if(current->sister)
				{
					current = current->sister;
					// if(!independant || current->isIndependant())
					if( (!independant || current->isDetached()) &&
						 (!realDetached || (current->getAttachMode() != Attached)))
							finished = True;
					winding = False;
				}
				else
				{
					current = current->superior;
					winding = True;		// Prevent from going down children again
				}
			}
		}

		/*-----------------------------------------------
		 * Bug fix: 23rd January 1996
		 * Problem is that retVal can have illegal values
		 * (i.e. rank is higher than allowed)
		 * But if this happens during the last iteration when current
		 * becomes NULL, it returns the illegal value.
		 *
		 * Solution is to test on retVal being NULL instead of current
		 * Next time round the loop, retVal will become NULL anyway and exit.
		 */

	//--------- Patch... 23Jan96 old line -------------------------
	// } while(current && (retVal->rank < hiRank));
	//---------- Patch 23Jan96 replacement line -------------------
	} while(retVal && (retVal->rank < hiRank));
	//---------------------End of Patch 23Jan96 -------------------

#ifdef DEBUG
	if(retVal)
	{
		ASSERT(retVal->rank >= hiRank);
		ASSERT(retVal->rank <= lowRank);
		ASSERT(!independant || retVal->isDetached());
		ASSERT(!realDetached || (retVal->getAttachMode() != Attached));
	}
#endif

	return retVal;

#else

	/*
	 * Replacement version that used getNext
	 */

	Unit* retVal = current;
#ifdef DEBUG
	if(retVal)
	{
		ASSERT(retVal->rank >= hiRank);
		ASSERT(retVal->rank <= lowRank);
		ASSERT(!independant || retVal->isDetached());
		ASSERT(!realDetached || (retVal->getAttachMode() != Attached));
	}
#endif
	getNext();
	return retVal;

#endif
}
//-------- Patch 23Jan96

