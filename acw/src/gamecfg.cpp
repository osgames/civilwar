/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Game Configuration File settings
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "gamecfg.h"

const char configFileName[] 	= "acw.ini";
const char cfgSoundID[] 		= "Sound";
const char cfgMusicID[] 		= "Music";
const char cfgRealismID[] 		= "Realism";

const char cfgPhoneID[] 		= "Phone";
const char cfgModemInitID[] 	= "ModemInit";
const char cfgDialID[] 			= "Dial";
const char cfgAnswerID[] 		= "Answer";
const char cfgCommPortID[] 	= "CommPort";
const char cfgBaudID[] 			= "BaudRate";

const char cfgComm1IRQ[] 		= "Com1_IRQ";
const char cfgComm2IRQ[] 		= "Com2_IRQ";
const char cfgComm3IRQ[] 		= "Com3_IRQ";
const char cfgComm4IRQ[] 		= "Com4_IRQ";

const char cfgCampRealTime[]	= "CampaignRealTime";
const char cfgRouteFinder[]	= "RouteFinder";

const char* cfgRouteEnum[] = { "Quick", "Full", 0 };

const char cfgAIpasses[]		= "AIpasses";
