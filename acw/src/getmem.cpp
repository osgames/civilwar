/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Find out how much extended memory there is
 * It must be passed a real mode address on the command line in hex
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

// #define DEBUG

#include <stdio.h>
#include <i86.h>
#include <string.h>

struct MemInfo {
	// unsigned long freeDPMI;
	// unsigned long freeDOS;
	unsigned long totalMemory;
};

void main(int argc, char* argv[])
{
	MemInfo* info = 0;

	if(argc == 2)
	{
		unsigned long address;
		sscanf(argv[1], "%lx", &address);

#ifdef DEBUG
		printf("argv[1] = %s\n", argv[1]);
		printf("Address = %lx\n", address);
#endif
		info = (MemInfo*) address;	// Real Mode memory is at 0:address
	}


	/*
	 * Get DPMI Info
	 */

	struct {
		unsigned long largest;					// Largest free block of DPMI memory
		unsigned long maxPage;
		unsigned long largeBlock;
		unsigned long linSpace;					// Total Linear memory Space
		unsigned long freePages;				// Total Number of free pages
		unsigned long physPages;				// Total Number of free Physical Pages
		unsigned long totalPhysicalPages;	// Total Number of Physical Pages
		unsigned long freeLinSpace;			// Free Linear Space
		unsigned long sizeofPage;				// Size of paging partition
		unsigned long reserved[3];
	} DPMIInfo;

	
	union REGS regs;
	struct SREGS sregs;

	regs.x.eax = 0x500;
	memset(&sregs, 0, sizeof(sregs));
	sregs.es = FP_SEG(&DPMIInfo);
	regs.x.edi = FP_OFF(&DPMIInfo);
	int386x(0x31, &regs, &regs, &sregs);

	/*
	 * DOS Memory
	 */

	memset(&sregs, 0, sizeof(sregs));
	regs.x.eax = 0x0100;
	regs.x.ebx = 0xffff;		// Allocate more than is available

	int386(0x31, &regs, &regs);

	if(info)
	{
#if 0
		info->freeDPMI = DPMIInfo.largest;
		info->freeDOS = regs.w.bx << 4;
#else
		info->totalMemory = DPMIInfo.largest + (regs.w.bx << 4);
#endif
	}

#ifdef DEBUG
	printf("FreeDPMI = 0x%lx %ld %ldK\n", (long) DPMIInfo.largest, (long) DPMIInfo.largest, DPMIInfo.largest / 1024);
	printf("FreeDOS = 0x%lx %ld %ldK\n", (long) regs.w.bx << 4, (long) regs.w.bx << 4, ((long) regs.w.bx << 4) / 1024);
#endif

}
