/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *		VGA Screen functions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:54  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "vgascrn.h"
#include "error.h"
#include "image.h"
// #include "vertblnk.asm"
#include <dos.h>
#include "vertblnk.h"


#pragma aux setOverscanRegister = \
	"mov ax,1001h"		\
	"mov bh,0"			\
	"int 10h"			\
	modify [eax ebx ecx edx esi edi]

Vgascr::Vgascr( short newmode )
{
	setOldVideoMode();

	if ( !_setvideomode( newmode ) )
		throw GeneralError ( "Unable to set video mode" );

	setOverscanRegister();
	setVideoInfo();
}


Vgascr::~Vgascr()
{
 	if ( !_setvideomode( oldmode ) )
		_setvideomode( _DEFAULTMODE );
}


void Vgascr::setOldVideoMode()
{
	struct videoconfig vconfig;

	_getvideoconfig( &vconfig );

	oldmode = vconfig.mode;

}

void Vgascr::setVideoInfo()
{
	struct videoconfig vconfig;

	_getvideoconfig( &vconfig );

	xpix = vconfig.numxpixels;
	ypix = vconfig.numypixels;

	currentmode = vconfig.mode;
}

void Vgascr::setFlicPosition( short height, short width, Boolean centre )
{
	int offset = 0;

	// screenPtr = (unsigned char* ) MK_FP( 0xa000, 0 );

	screenPtr = (unsigned char* )(0xa000 << 4);

	if ( currentmode != _MRES256COLOR ) return;

	if ( ( height > ypix ) || ( width > xpix ) )
		throw GeneralError ( "Flic file is too big to display in this mode" );

	if ( centre )
	{
	 	offset = ( (ypix - height) / 2 ) * 320;  // For 320 mode

		screenPtr += offset;
	}
}

void Vgascr::blit(Image* image )
{
	memcpy( screenPtr, image->getAddress(), image->getWidth() * image->getHeight() );
}


void Vgascr::waitForVerBlank()
{
	awaitVerticalBlank();	
}

#ifdef DEBUG
void Vgascr::resetScreen()
{
	 _setvideomode( oldmode );
}
#endif
