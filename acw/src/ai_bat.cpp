/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	The AI battlefield routines associated mainly with deployment
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <conio.h>
#include <stdio.h>
#include "ai_bat.h"
#include "ai_unit.h"
#include "terrain.h"
#include "map3d.h"
#include "unit3d.h"
#include "view3d.h"
#include "batldata.h"
#include "campaign.h"
#include "measure.h"
#include "generals.h"
#include "orders.h"
#include "measure.h"
#include "game.h"
#include "combval.h"

#ifndef ERROR_H
#include "error.h"
#endif
#include "myassert.h"

#ifdef DEBUG
#include "clog.h"
#include "options.h"
#include "tables.h"

LogFile aiBLog("batai.log");

#endif

static char tactics[9][2][3][2] = {
	
	// [ start in table, offset to finish][ odds acceptable, not ]
						// [ aggression test ][ orderMode ]
														
	{ { { 3, 1 }, { 1, 2 }, { 4, 2 } }, { { 4, 2 }, { 6, 1 }, { 8, 1 } } },
	{ { { 1, 2 }, { 4, 2 }, { 6, 1 } }, { { 6, 1 }, { 8, 1 }, { 9, 1 } } },
	{ { { 0, 1 }, { 1, 3 }, { 4, 2 } }, { { 1, 2 }, { 4, 3 }, { 6, 1 } } },
	
	{ { {	4, 2 }, { 6, 1 }, { 8, 1 } }, { { 6, 1 }, { 8, 1 }, { 9, 1 } } },
	{ { { 6, 1 }, { 7, 1 }, { 8, 1 } }, { { 7, 1 }, { 8, 1 }, { 9, 1 } } },
	{ { { 6, 1 }, { 7, 1 }, { 8, 1 } }, { { 7, 1 }, { 8, 1 }, { 9, 1 } } },

	{ { { 7 ,1 }, { 8, 1 }, { 9, 1 } }, { { 8, 1 }, { 9, 1 }, {10, 1 } } },
	{ { { 8, 1 }, { 9, 1 }, {10, 1 } }, { { 9, 1 }, {10, 1 }, {11, 1 } } },
	{ { { 9, 1 }, {10, 1 }, {11, 1 } }, { {10, 1 }, {11, 1 }, {12, 1 } } }

};


static char priority[] = { 2, 1, 4, 5, 1, 4, 0, 0, 0, 0, 0, 0, 0 };

#ifdef DEBUG
const char* getTactStr(AI_Tactic tact)
{
	static char* tactStr[] = {
		"Frontal_Assault",
		"Left_Flanking",
		"Right_Flanking",
		"Pincer",
		"Feint_Left_Right",
		"Feint_Right_Left",
		"Line_Defence",
		"Defence_In_Depth",
		"Feint_Defence",
		"Holding_Action",
		"Covering_Action",
		"Withdraw",
		"Retreat"
	};

	if(tact < Max_Tactic)
		return tactStr[tact];
	else
		return "Invalid tactic";
}

#endif	// DEBUG

/*
 *  Class AI_Battle functions
 */


AI_Battle::AI_Battle() //  CampaignAI* AI )
{
#ifdef DEBUG
	aiBLog.printf("AI_Battle::AI_Battle()");
#endif

	/*
	 * Set everything up to default values

	 */

	OnBattlefield = False;
	TactAdvantage = False;
	AttackFlag = False;
	TimeArray = False;
	ob = 0;
	side = SIDE_None;
	Lflag = False;
	FitFlag = False;
	MAXCorpsAL = 0;
	pres = 0;
	senior = 0;
	tact = Frontal_Assault;
	corpsTotal = 0;
	deployCentre = Location( 0, 0 );
	ang = 0;
	// memset(strs, 0, sizeof(strs));
	time = 0;

	TimeArray = False;
	

}

AI_Battle::~AI_Battle()
{
#ifdef DEBUG
	aiBLog.printf("~AI_Battle()");
#endif

	ReassignUnits();

#ifdef DEBUG_CHRIS
	tout.close();
#endif
	// if ( ArmyAL ) delete [] Armylist;

	// delete deployCentre;

	if ( TimeArray ) delete [] time;
}

Boolean AI_Battle::AI_Bat_Initiate( Side sd, Boolean Historical )
{
#ifdef DEBUG
	aiBLog.printf("AI_Bat_Initiate(%d %s)",
		(int) sd,
		getBoolStr(Historical));
#endif

	ob = battle->ob;

	int choice = 0;

#ifdef NO_DEBUG
	if ( AI_ForceOption )
	{
		choice = AI_ForceOption;
	}
	else
	{
		PutInLog( "No AI_ForceOption specified\r\n" );
	}

	PutInLog ( choice	);
#endif

	side = sd;

	pres = ob->getPresident( side );
 	
	if ( !findSeniorCommander() ) return False;

	if ( !Historical ) IsCPOnBattlefield();
	else OnBattlefield = True;

#ifdef DEBUG
	if	( choice )
	{
		OnBattlefield = True;

		if ( choice >= 160 ) choice -= 80;

		switch ( choice - 80 )
		{
			case 0:
				tact = Frontal_Assault;
				break;
			case 1:
				tact = Left_Flanking;
				break;
			case 2:
				tact = Right_Flanking;
				break;
			case 3:
				tact = Pincer;
				break;
			case 4:
				tact = Feint_Left_Right;
				break;
			case 5:
				tact = Feint_Right_Left;
				break;
			case 6:
				tact = Line_Defence;
				break;
			case 7:
				tact = Defence_In_Depth;
				break;
			case 8:
				tact = Feint_Defence;
				break;
			case 9:
				tact = Holding_Action;
				break;
			case 10:
				tact = Covering_Action;
				break;
			case 11:
				tact = Withdraw;
				break;
			case 12:
				tact = Retreat;
				break;
	 	}
	}
	
	SelectTactic();
	
#else
	SelectTactic();
#endif

	tempAssignUnits();

	TimeArray = True;

	Lflag = False;

	if ( senior->givenOrders.mode >= ORDER_Stand ) 
	{
		Lflag = True;
		DeployTroops(sd, Historical);
	}

	if ( !Historical ) 
	{
		Lflag = !Lflag;
		DeployTroops(otherSide(sd), Historical);
	}

	if ( senior->givenOrders.mode < ORDER_Stand )
	{
		Lflag = !Lflag;
		DeployTroops(sd, Historical);
	}

	/*
	 * 	Ensure armies are facing each other
	 */

	faceArmies( SIDE_USA );

	faceArmies( SIDE_CSA );

	corpsTotal = 0;

	sendInitialOrders( 1 );

	if ( !corpsTotal )
		sendInitialOrders();

	return True;
}

Boolean AI_Battle::findSeniorCommander()
{
#ifdef DEBUG
	aiBLog.printf("findSeniorCommander()");
#endif

	Unit* loyalArmy = pres->child;
	Unit* loyalCorps;
	Unit* loyalDivision;
	Unit* loyalBrigade;

	int No_corps = 0;

	senior = 0;

	while ( loyalArmy )
	{
		if ( loyalArmy->battleInfo || loyalArmy->hasCampaignReallyDetached() )
		{
			if ( loyalArmy->battleInfo )
			{
				if ( !senior || senior->rank > Rank_Army ) 
				{
					senior = loyalArmy;
				}
			}

			loyalCorps = loyalArmy->child;
		
			while( loyalCorps )
			{
				if ( loyalCorps->battleInfo || loyalCorps->hasCampaignReallyDetached() )
				{
					if ( loyalCorps->battleInfo )
					{
						if ( !senior || senior->rank > Rank_Corps ) 
						{
							senior = loyalCorps;
						}

						No_corps++;

						if ( loyalCorps->isCampaignDetached() && loyalArmy->battleInfo )
						{
							loyalCorps->rejoinBattle();
							//loyalCorps->makeCampaignDetached(False);
						}
					}

					loyalDivision = loyalCorps->child;

					while( loyalDivision )
					{
						if ( loyalDivision->battleInfo || loyalDivision->hasCampaignReallyDetached())
						{

							if ( loyalDivision->battleInfo)
							{
								if ( !senior || senior->rank > Rank_Division ) 
								{
									senior = loyalDivision;
								}
								// else if ( senior->rank > Rank_Division )
								// {
								// 	senior = loyalDivision;
								// }
							}

							loyalBrigade = loyalDivision->child;

							while( loyalBrigade )
							{
								if ( loyalBrigade->battleInfo )
								{
									if ( !senior ) 
									{
										senior = loyalBrigade;
									}
								}

								loyalBrigade =	loyalBrigade->sister;
							}
						}

						loyalDivision = loyalDivision->sister;
					}
				}
				loyalCorps = loyalCorps->sister;
			}
		}
		loyalArmy = loyalArmy->sister;
	}

	corpsTotal = No_corps;

	if ( senior ) return True;

#ifdef DEBUG_CHRIS
	PutInLog( "Error: can't find senior commander!!" );
#endif

	// throw GeneralError( "Can't find a senior commander" );

	return False;
}

#ifdef DEBUG

Unit* AI_Battle::findEnemySenior()
{
#ifdef DEBUG
	aiBLog.printf("findEnemySenior()");
#endif

	President* p = ob->getPresident( otherSide ( side ) );
	Unit* loyalArmy = p->child;
	Unit* loyalCorps;
	Unit* loyalDivision;
	Unit* loyalBrigade;

	Unit* topMan = 0;

	while ( loyalArmy )
	{
		if ( loyalArmy->battleInfo || loyalArmy->hasCampaignReallyDetached() )
		{
			if ( loyalArmy->battleInfo )
			{
				if ( !topMan ) 
				{
					topMan = loyalArmy;
				}
			}

			loyalCorps = loyalArmy->child;
		
			while( loyalCorps )
			{
				if ( loyalCorps->battleInfo || loyalCorps->hasCampaignReallyDetached() )
				{
					if ( loyalCorps->battleInfo )
					{
						if ( !topMan ) 
						{
							topMan = loyalCorps;
						}
					}

					loyalDivision = loyalCorps->child;

					while( loyalDivision )
					{
						if ( loyalDivision->battleInfo )
						{
							if ( !topMan ) 
							{
								topMan = loyalDivision;
							}
						}

						loyalBrigade = loyalDivision->child;

						while( loyalBrigade )
						{
							if ( loyalBrigade->battleInfo )
							{
								if ( !topMan ) 
								{
									topMan = loyalBrigade;
								}
							}

							loyalBrigade =	loyalBrigade->sister;
						}
				
						loyalDivision = loyalDivision->sister;
					}
				}
				loyalCorps = loyalCorps->sister;
			}
		}
		loyalArmy = loyalArmy->sister;
	}

	if ( topMan ) return topMan;

#ifdef DEBUG_CHRIS
	PutInLog( "Error: can't find enemy senior commander!!" );
#endif

	// throw GeneralError( "Can't find enemy senior commander" );

	return NULL;

}

#endif

void AI_Battle::IsCPOnBattlefield()
{
#ifdef DEBUG
	aiBLog.printf("IsCPOnBattlefield()");
#endif

   CombatValue loyalStr = CalcStrength( side );
   CombatValue enemyStr = CalcStrength( otherSide( side ) );

	retValue eff = Efficiency_Test( senior->general, loyalStr, enemyStr, game->gameRand.getB() );
  
   if ( eff == FAIL_BADLY )
	{
		OnBattlefield = False;
		senior->givenOrders.mode = ORDER_Withdraw;
		return;
	}
	else if ( eff == PASS )
	{
		OnBattlefield = True;
		return;
	}

 	Distance dx = senior->battleInfo->where.x - senior->givenOrders.destination.x;
	Distance dy = senior->battleInfo->where.y - senior->givenOrders.destination.y;

	Distance d = distance( dx, dy );

	if ( abs( unitToMile( d ) ) < 8 ) 
	{
		OnBattlefield = True;
		return;
	}
	else OnBattlefield = False;

	President* p = ob->getPresident( otherSide ( side ) );
	Unit* loyalArmy = p->child;
	Unit* loyalCorps;
	Unit* loyalDivision;
	Unit* loyalBrigade;

	while ( loyalArmy )
	{
		if ( loyalArmy->battleInfo || loyalArmy->hasCampaignReallyDetached() )
		{
#ifdef DEBUG
			if(loyalArmy->battleInfo)
				ASSERT(loyalArmy->aiInfo != 0);
#endif

			if ( ( loyalArmy->battleInfo ) && ( loyalArmy->aiInfo->ToBeAttacked ) )
			{
				OnBattlefield = True;
				return;
			}

			loyalCorps = loyalArmy->child;
		
			while( loyalCorps )
			{
				if ( loyalArmy->battleInfo || loyalArmy->hasCampaignReallyDetached() )
				{
#ifdef DEBUG
					if(loyalCorps->battleInfo)
						ASSERT(loyalCorps->aiInfo != 0);
#endif
					if ( ( loyalCorps->battleInfo ) && ( loyalCorps->aiInfo->ToBeAttacked ) )
					{
						OnBattlefield = True;
						return;
					}

					loyalDivision = loyalCorps->child;

					while( loyalDivision )
					{
#ifdef DEBUG
						if(loyalDivision->battleInfo)
							ASSERT(loyalDivision->aiInfo != 0);
#endif
						if ( ( loyalDivision->battleInfo ) && ( loyalDivision->aiInfo->ToBeAttacked ) )
						{
							OnBattlefield = True;
							return;
						}

						loyalBrigade = loyalDivision->child;

						while( loyalBrigade )
						{
#ifdef DEBUG
						if(loyalBrigade->battleInfo)
							ASSERT(loyalBrigade->aiInfo != 0);
#endif
							if ( ( loyalBrigade->battleInfo ) && ( loyalBrigade->aiInfo->ToBeAttacked ) )
							{
								OnBattlefield = True;
								return;
							}
							loyalBrigade =	loyalBrigade->sister;
						}
						loyalDivision = loyalDivision->sister;
					}
				}
				loyalCorps = loyalCorps->sister;
			}
		}
		loyalArmy = loyalArmy->sister;
	}

}

void AI_Battle::SelectTactic()
{
#ifdef DEBUG
	aiBLog.printf("SelectTactic()");
#endif

  CombatValue loyalStr = CalcStrength( side );
  CombatValue enemyStr = CalcStrength( otherSide( side ) );
  
  if ( loyalStr > enemyStr ) TactAdvantage = True;
  else TactAdvantage = False;

  retValue eff = Efficiency_Test( senior->general, loyalStr, enemyStr, game->gameRand.getB() );

  Boolean fails = False;

  if ( eff == FAIL_BADLY || eff == FAIL ) fails = True;
  
  OrderMode action = senior->givenOrders.mode;

  if ( loyalStr && enemyStr / loyalStr > 3 )
  {
		action = ORDER_Withdraw;
		
		if ( (game->gameRand(3)) > 1 ) action = ORDER_Retreat;
  }
  else if ( loyalStr && enemyStr / loyalStr > 2 )
  {
		action = ORDER_Stand;
		
		if ( (game->gameRand(3)) > 1 ) action = ORDER_Defend;
  }

  int aggre = ( Aggressvalue( senior ) - 1 ) >> 1;

  char temp = tactics[ action ][ fails ][ aggre ][ 1 ];

  temp += game->gameRand(tactics[ action ][ fails ][ aggre ][ 2 ]);

  tact = ( AI_Tactic )temp;

  if ( loyalStr && enemyStr / loyalStr > 4 )
  {
		tact = Withdraw;
		
		if ( (game->gameRand(3)) > 1 ) tact = Retreat;
  }

#ifdef DEBUG
	aiBLog.printf("tact = %s, action = %s", getTactStr(tact), getOrderStr(action));
	aiBLog.printf("tactAdvantage = %s", getBoolStr(TactAdvantage));
#endif

}

CombatValue AI_Battle::CalcStrength( Side s )
{
#ifdef DEBUG
	aiBLog.printf("CalcStrength(%d)", (int) s);
#endif

	Strength strengthTotal = 0;

	President *p = ob->getPresident( s );

	Unit* loyalArmy = p->child;
	Unit* loyalCorps;
	Unit* loyalDivision;
	Brigade* loyalBrigade;
	Regiment* loyalRegiment;

	while ( loyalArmy )
	{
		if ( loyalArmy->battleInfo || loyalArmy->hasCampaignReallyDetached() )
		{
			if ( loyalArmy->battleInfo )
			{
				strengthTotal += calc_UnitCV( loyalArmy, True );
			}

			loyalCorps = loyalArmy->child;

			while( loyalCorps )
			{
				if ( loyalCorps->battleInfo || loyalCorps->hasCampaignReallyDetached() )
				{
					if ( loyalCorps->battleInfo )
					{
						strengthTotal += calc_UnitCV( loyalCorps, True );
					}

					loyalDivision = loyalCorps->child;

					while( loyalDivision )
					{
						if ( loyalDivision->battleInfo )
						{
							strengthTotal += calc_UnitCV( loyalDivision, True );
						}

						loyalBrigade = ( Brigade* )loyalDivision->child;

						while( loyalBrigade )
						{
							if ( loyalBrigade->battleInfo )
							{
								strengthTotal += calc_UnitCV( loyalBrigade, True );
							}

							loyalRegiment = ( Regiment* )loyalBrigade->child;

							loyalBrigade->AIbasicType = loyalRegiment->type.basicType;
							loyalBrigade->AIsubType = loyalRegiment->type.subType;

							while ( loyalRegiment )
							{
					 			if ( loyalRegiment->type.basicType < loyalBrigade->AIbasicType )
								{
									loyalBrigade->AIbasicType = loyalRegiment->type.basicType;
									loyalBrigade->AIsubType = loyalRegiment->type.subType;
								}
								else if ( loyalRegiment->type.subType > loyalBrigade->AIsubType )
								{
									loyalBrigade->AIsubType = loyalRegiment->type.subType;
								}
								loyalRegiment = ( Regiment* )loyalRegiment->sister;
							}
							loyalBrigade =	( Brigade* )loyalBrigade->sister;
						}
						loyalDivision = loyalDivision->sister;
					}
				}
				loyalCorps = loyalCorps->sister;
			}
		}
		loyalArmy = loyalArmy->sister;
	}

	return strengthTotal;
}


/*
 * Steven's shortened Simplified Version
 */


/*
 * Place AI's troops on battlefield
 *
 * If historical is set, then they are just given orders to move
 * to where they want to be.  Otherwise they will be placed their.
 *
 * if deploySide is the same as the AI's, then Armylist is also setup.
 */

void AI_Battle::DeployTroops(Side deploySide, Boolean historical)
{
#ifdef DEBUG
	aiBLog.printf("DeployTroops(%d, %s)",
		(int) deploySide,
		getBoolStr(historical));
#endif

	/*
	 * Preserve deployCentre if not AI side
	 */

	Location tempBL = deployCentre;

	FindDeployCentre(deploySide);

	Unit* top = ob->getPresident(deploySide);


	/*
	 * Count how many independant units are on the battlefield
	 */

	int count = 0;

	for(Unit* a = top->child; a; a = a->sister)
	{
		if(a->battleInfo)
			count++;
		else if(a->hasBattleReallyDetached())
		{
			for(Unit* c = a->child; c; c = c->sister)
			{
				if(c->battleInfo)
					count++;
				else if(c->hasBattleReallyDetached())
				{
					for(Unit* d = c->child; d; d = d->sister)
					{
						if(d->battleInfo)
							count++;
						else if(d->hasBattleReallyDetached())
						{
							for(Unit* b = d->child; b; b = b->sister)
							{
								if(b->battleInfo)
									count++;
							}
						}
					}
				}
			}
		}
	}

	if(!count)
		return;

	/*
	 * Make array of units to be placed
	 */

	struct DeployInfo {
		Unit* u;
		int strength;
	};

	DeployInfo* info = new DeployInfo[count];

#ifdef DEBUG
	int count1 = count;
#endif

	count = 0;

	for(a = top->child; a; a = a->sister)
	{
		if(a->battleInfo)
		{
			info[count].u = a;
			info[count].strength = calc_UnitCV(a, True);
			count++;
		}
		else if(a->hasBattleReallyDetached())
		{
			for(Unit* c = a->child; c; c = c->sister)
			{
				if(c->battleInfo)
				{
					info[count].u = c;
					info[count].strength = calc_UnitCV(c, True);
					count++;
				}
				else if(c->hasBattleReallyDetached())
				{
					for(Unit* d = c->child; d; d = d->sister)
					{
						if(d->battleInfo)
						{
							info[count].u = d;
							info[count].strength = calc_UnitCV(d, True);
							count++;
						}
						else if(d->hasBattleReallyDetached())
						{
							for(Unit* b = d->child; b; b = b->sister)
							{
								if(b->battleInfo)
								{
									info[count].u = b;
									info[count].strength = calc_UnitCV(b, True);
									count++;
								}
							}
						}
					}
				}
			}
		}
	}

#ifdef DEBUG
	if(count != count1)
		throw GeneralError("count (%d) != count1 (%d)", count, count1);
#endif


	/*
	 * Sort into strength order
	 * (Bubble Sort)
	 */

	if(count > 1)
	{
		Boolean sorted = False;
		while(!sorted)
		{
			sorted = True;

			for(int i = 0; i < (count - 1); i++)
			{
				if(info[i].strength < info[i+1].strength)
				{
					swap(info[i], info[i+1]);
					sorted = False;
				}
			}
		}
	}

	/*
	 * Deploy everything
	 */

	if(deploySide == side)
	{
		Armylist.init(count);
		time = new GameTime[count];
		MAXCorpsAL = count;
	}

	for(int i = 0; i < count; i++)
	{
		if(!historical)
			placeCorps(info[i].u, i, count);
		if(deploySide == side)
			Armylist[i] = info[i].u;
		clearSeperated(info[i].u, 1);
	}

	/*
	 * Restore deployCentre
	 */

	if(deploySide != side)
		deployCentre = tempBL;

	delete[] info;
}



void AI_Battle::placeCorps( Unit* loyalCorps, int which, int nocorps )
{
#ifdef DEBUG
	aiBLog.printf("placeCorps(%s, %d, %d)",
		loyalCorps ? loyalCorps->getName(True) : "Null",
		which, nocorps);
#endif

	if(!loyalCorps->battleInfo)
	{
#ifdef DEBUG
		aiBLog.printf("placeCorps(%s) has no battleInfo", loyalCorps->getName(True));
#endif
		return;
	}

	Distance width, height;

	Distance tempw, temph;

	loyalCorps->battleInfo->getArea( width, height );	

	Location temp( deployCentre.x, deployCentre.y );

	DoesCorpFit( loyalCorps );

	int addFlag = 0; 	// mcos( 2, ang );

	Wangle wang = ang;

	int changedAng = 0;

	if ( ( ( ang >= 0x800 ) &&  ( ang <= 0xc000 ) ) || ( ang <= 0x4000 ) )
	{
		ang = -ang + 0x4000;  
	}
	else
	{
		ang += 0xc000;
		ang = -ang;
	}	   


	if ( nocorps <= 2 )
 	{
		int tempn = 0;

		if ( nocorps == 1 )
		{
			while( deployTable[ tact ][tempn] != 4 ) tempn++;
		}
		else
		{
			while( deployTable[ tact ][tempn] != 1 ) tempn++;
		}

		which = tempn;
	}

	if ( which < 6 )
	{
		switch ( deployTable[ tact ][which] )
		{
			case 1:			// right front
				temp.x += mcos( width, ang );
				temp.y -= msin( width, ang );
				break;

			case 2:			// left front
				temp.x -= mcos( width, ang );
				temp.y += msin( width, ang );
				break;

			case 3:			// reserve back
				temp.x -= mcos( height, wang ); // w
				temp.y -= msin( height, wang ); // w
				break;

			case 4:			// centre front
				break;

			case 5:			// rear right
				temp.x += mcos( width, ang );
				temp.y -= msin( width, ang );
				temp.x -= mcos( height, wang );	 //w
				temp.y -= msin( height, wang );	 //w
				break;

			case 6:			// rear left
				temp.x -= mcos( width, ang );
				temp.y += msin( width, ang );
				temp.x -= mcos( height, wang );	 //w
				temp.y -= msin( height, wang );	 //w
				break;

			default:			// error
				#ifdef DEBUG
					cout << "Error in Deploy @ PlaceCorps";
					getch();
				#endif
				return;
				break;
		}

		if ( loyalCorps->rank == Rank_Corps )
			getKeyTerrain( loyalCorps, 1, &temp );
	}
	else
	{
		int times = ( ( which - 6 ) / 3 ) + 1;

		int tempwhich = ( which - 6 ) % 3;

		switch ( tempwhich )
		{
			case 0:			// centre rear;
				
				if ( tact < Defence_In_Depth ) 
				{
					temp.x -= ( mcos( height, ang ) * times );
					temp.y -= ( msin( height, ang ) * times );
				}
				else
				{
					temp.x += ( mcos( width, ang ) * times );
					temp.y -= ( msin( width, ang ) * times );
				}
				break;

			case 1:			// right rear
				if ( tact < Defence_In_Depth )
				{
					temp.x += mcos( width, ang ) * times;
					temp.y -= msin( width, ang ) * times;
					temp.x -= mcos( height, wang ) * times;
					temp.y -= msin( height, wang ) * times;
				}
				else
				{
					temp.x -= ( mcos( width, ang ) * times );
					temp.y += ( msin( width, ang ) * times );
				}
				break;

			case 2: 			// left rear
				if ( tact < Defence_In_Depth )
				{
					temp.x -= mcos( width, ang ) * times;
					temp.y += msin( width, ang ) * times;
					temp.x -= mcos( height, ang ) * times;
					temp.y -= msin( height, ang ) * times;
				}
				else
				{
					temp.x += mcos( width, ang ) * times;
					temp.y -= msin( width, ang ) * times;
					temp.x -= mcos( height, wang ) * times;
					temp.y -= msin( height, wang ) * times;
				}
				break;

			default:
				#ifdef DEBUG
					cout << "OI! That's imposs ( or at least it should be ) !!!";
				#endif
				return;
				break;
		}
	}

	Location bl( temp.x, temp.y );

	if ( !FitFlag )
	{
		temp.x = loyalCorps->battleInfo->where.x;
		temp.y = loyalCorps->battleInfo->where.y;
	}

	if ( tact > Feint_Right_Left ) 
	{	
		if ( !which )
		{
		 	temp.x -= addFlag;

			if ( loyalCorps->rank == Rank_Corps )
				placeDivision( loyalCorps, &temp, addFlag );
			else if ( loyalCorps->rank == Rank_Division )
				placeBrigade( loyalCorps, &temp, addFlag );
			else 
			{
				DeployToBL( temp, loyalCorps);
				temp.x += addFlag;
				if ( loyalCorps->getSide() == side ) 
				{
					OrderToBL( &temp, loyalCorps, 1 );
				}
				else DeployToBL( temp, loyalCorps);
			}

			/*
			 *  The above ensures that AIReachedClickPoint is called at the
			 *  begining of the game. Hopefully the game mechanics would of 
			 *  detected any army near enough to fire upon and incremented the
			 *  battleInfo's inCombat counter. This saves me doing any line of
			 *  sight calculations.
			 */
		}
		else
		{

			if ( loyalCorps->rank == Rank_Corps )
				placeDivision( loyalCorps, &temp );
			else if ( loyalCorps->rank == Rank_Division )
				placeBrigade( loyalCorps, &temp );
			else
				DeployToBL( temp, loyalCorps );
		}
	}
	else 
	{
		if ( loyalCorps->rank == Rank_Corps )
			placeDivision( loyalCorps, &temp );
		else if ( loyalCorps->rank == Rank_Division )
			placeBrigade( loyalCorps, &temp );
		else if ( loyalCorps->getSide() == side ) 
		{
			OrderToBL( &temp, loyalCorps );
		}
		else
			DeployToBL( temp, loyalCorps );
	}

	ang = wang;

	placeSuperior( loyalCorps, 0, &bl );

	return;

}


void AI_Battle::placeDivision( Unit* loyalCorps, Location* bl, int addFlag )
{
#ifdef DEBUG
	ASSERT(bl != 0);
	aiBLog.printf("placeDivision(%s, %ld,%ld, %d)",
		loyalCorps ? loyalCorps->getName(True) : "Null",
		bl->x, bl->y,
		addFlag);
#endif

  	Cord3D width, height;

  	Unit* loyalDivision = loyalCorps->child;
  
	Location* tempbl;

	tempbl = new Location( bl->x, bl->y );

   // loyalDivision->battleInfo->getArea( tempW, tempH );	

	Boolean deployedD = 0; 

#if 0
	width = tempW;
   height = tempH;
#else
	width = 0;
   height = 0;
#endif

	int which = 0;

	int noDiv = findNumChildren( loyalCorps );

	Unit* commander = loyalDivision;

	while ( !commander->isCampaignDetached() ) commander = commander->superior;

  	while ( loyalDivision )
  	{
		if( ( loyalDivision->battleInfo ) && ( !loyalDivision->isBattleDetached() ) )
		{
 			tempbl->x = bl->x;
 			tempbl->y = bl->y;

   		Distance tempW, tempH;
			loyalDivision->battleInfo->getArea( tempW, tempH );

	   	if ( tempW > width )
				width = tempW;
   		if ( tempH > height )
				height = tempH;

		  	if ( noDiv <= 2 )
 		  	{
		  		int temp = 0;

		  		if ( which == 0 )
		  		{
		  			while( deployTable[ tact ][temp] != 4 ) temp++;
		  		}
		  		else
		  		{
		  			while( deployTable[ tact ][temp] != 1 ) temp++;
		  		}

		  		which = temp;
		  	}

			if ( which < 6 )
			{
				switch ( deployTable[ tact ][ which ] )
				{
					case 1:			// right front
						tempbl->x += mcos( width, ang );
						tempbl->y -= msin( width, ang );
						break;

					case 2:			// left front
						tempbl->x -= mcos( width, ang );
						tempbl->y += msin( width, ang );
						break;

					case 3:			// reserve back
						tempbl->x -= mcos( height, 0x4000 - ang  );
						tempbl->y -= msin( height, 0x4000 - ang );
						break;

					case 4:			// centre back
						break;

					case 5:			// rear right
						tempbl->x += mcos( width, ang );
						tempbl->y -= msin( width, ang );
						tempbl->x -= mcos( height, 0x4000 - ang );
						tempbl->y -= msin( height, 0x4000 - ang );
						break;

					case 6:			// rear left
						tempbl->x -= mcos( width, ang );
						tempbl->y += msin( width, ang );
						tempbl->x -= mcos( height, 0x4000 - ang );
						tempbl->y -= msin( height, 0x4000 - ang );
						break;

					default:			// error
						#ifdef DEBUG
						cout << "Error in Deploy @ PlaceCorps";
						#endif
						getch();
						return;
						break;
				}
			}
			else
			{
				int times = ( ( which - 6 ) / 3 ) + 1;

				int tempwhich = ( which - 6 ) % 3;

				switch ( tempwhich )
				{
					case 0:			// centre rear;
						tempbl->x -= mcos( height * times , 0x4000 - ang );
						tempbl->y -= msin( height * times , 0x4000 - ang );
						break;

					case 1:			// right rear
						tempbl->x += mcos( width, ang ) * times;
						tempbl->y -= msin( width, ang ) * times;
						tempbl->x -= mcos( height, 0x4000 - ang ) * times;
						tempbl->y -= msin( height, 0x4000 - ang ) * times;
						break;

					case 2: 			// left rear
						tempbl->x -= mcos( width, ang ) * times;
						tempbl->y += msin( width, ang ) * times;
						tempbl->x -= mcos( height, 0x4000 - ang ) * times;
						tempbl->y -= msin( height, 0x4000 - ang ) * times;
						break;

					default:
						#ifdef DEBUG
							cout << "Oi! That's imposs and if not, why not!!!";
						#endif
						return;
						break;
				}
			}
			placeBrigade( loyalDivision, tempbl, addFlag );

			which++;
		}
		loyalDivision = loyalDivision->sister;
  	}

	delete tempbl;
}

void AI_Battle::placeBrigade( Unit* loyalDivision, Location* bl, int addFlag )
{
#ifdef DEBUG
	ASSERT(bl != 0);
	aiBLog.printf("placeBrigade(%s, %ld,%ld, %d)",
		loyalDivision ? loyalDivision->getName(True) : "Null",
		bl->x, bl->y,
		addFlag);
#endif

  	Cord3D width, height;

  	Unit* loyalBrigade = loyalDivision->child;
  
	Location* temp;

	temp = new Location( bl->x, bl->y );

	Boolean deployedD = 0;


   // loyalBrigade->battleInfo->getArea( tempW, tempH );	

	width = 0;	// tempW;
   height = 0;	// tempH;

	int which = 0;

	int noDiv = findNumChildren( loyalDivision );

	Unit* commander = loyalBrigade;

	while ( !commander->isCampaignDetached() ) commander = commander->superior;

  	while ( loyalBrigade )
  	{
		if( ( loyalBrigade->battleInfo ) && ( !loyalBrigade->isBattleDetached() ) )
		{
 			temp->x = bl->x;
 			temp->y = bl->y;

			Distance tempW, tempH;
			loyalBrigade->battleInfo->getArea( tempW, tempH );

	   	if ( tempW > width ) width = tempW;
   		if ( tempH > height ) height = tempH;

		  	if ( noDiv <= 2 )
 		  	{
		  		int tempn = 0;

		  		if ( which == 0 )
		  		{
		  			while( deployTable[ tact ][tempn] != 4 ) tempn++;
		  		}
		  		else
		  		{
		  			while( deployTable[ tact ][tempn] != 1 ) tempn++;
		  		}

		  		which = tempn;
		  	}

			if ( which < 6 )
			{
				switch ( deployTable[ tact ][ which ] )
				{
					case 1:			// right front
						temp->x += mcos( width, ang );
						temp->y -= msin( width, ang );
						break;

					case 2:			// left front
						temp->x -= mcos( width, ang );
						temp->y += msin( width, ang );
						break;

					case 3:			// reserve back
						temp->x -= mcos( height, 0x4000 - ang );
						temp->y -= msin( height, 0x4000 - ang );
						break;

					case 4:			// centre back
						break;

					case 5:			// rear right
						temp->x += mcos( width, ang );
						temp->y -= msin( width, ang );
						temp->x -= mcos( height, 0x4000 - ang );
						temp->y -= msin( height, 0x4000 - ang );
						break;

					case 6:			// rear left
						temp->x -= mcos( width, ang );
						temp->y += msin( width, ang );
						temp->x -= mcos( height, 0x4000 - ang );
						temp->y -= msin( height, 0x4000 - ang );
						break;

					default:			// error
						// cout << "Error in Deploy @ PlaceCorps";
						// getch();
						// return;
						break;
				}
			}
			else
			{
				int times = ( ( which - 6 ) / 3 ) + 1;

				int tempwhich = ( which - 6 ) % 3;

				switch ( tempwhich )
				{
					case 0:			// centre rear;
						temp->x -= mcos( height * times, 0x4000 - ang );
						temp->y -= msin( height * times, 0x4000 - ang );
						break;

					case 1:			// right rear
						temp->x += mcos( width * times, ang );
						temp->y -= msin( width * times, ang );
						temp->x -= mcos( height * times, 0x4000 - ang );
						temp->y -= msin( height * times, 0x4000 - ang );
						break;

					case 2: 			// left rear
						temp->x -= mcos( width * times, ang );
						temp->y += msin( width * times, ang );
						temp->x -= mcos( height * times, 0x4000 - ang );
						temp->y -= msin( height * times, 0x4000 - ang );
						break;

					default:
						#ifdef DEBUG
							cout << "Oi! That's imposs!!!";
						#endif
						return;
						break;
				}
			}

			DeployToBL( *temp, loyalBrigade );

			which++;
		}
		loyalBrigade = loyalBrigade->sister;
  	}

	placeSuperior( loyalDivision );

	delete temp;
}


void AI_Battle::placeSuperior( Unit* super, Boolean deploy, Location* tempbl, Location* deployO )
{
#ifdef DEBUG
	ASSERT(tempbl != 0);
	aiBLog.printf("placeSuperior(%s, %s, %ld,%ld)",
		super ? super->getName(True) : "Null",
		getBoolStr(deploy),
		tempbl->x, tempbl->y);
#endif

	ASSERT(super->battleInfo != 0);

	if ( !super->battleInfo ) return;

	BattleLocation total( 0, 0 );

	Unit* daughter = super->child;

	int counter = 0;

#ifdef DEBUG_CHRIS
	PutInLog( "\r\nPS: FitFlag=" );
	PutInLog( FitFlag );
	PutInLog( "\r\n" );
#endif

	while ( daughter )
	{
		if ( daughter->battleInfo )
		{
			total.x += DistToBattle( daughter->battleInfo->where.x );
			total.z += DistToBattle( daughter->battleInfo->where.y );

			counter++;
		}

		daughter = daughter->sister;
	}

	if ( !counter ) 
	{
#ifdef DEBUG_CHRIS
		cout << "Error placing Superior" ;
		PutInLog( "\r\nError in num. of daughters\r\n" );
#endif
		if ( super->rank == Rank_Army )
		{
			daughter = super->child;

			while( daughter )
			{
				daughter->makeBattleDetached( False );

				total.x += DistToBattle( daughter->battleInfo->where.x );
				total.z += DistToBattle( daughter->battleInfo->where.y );

				counter++;

				daughter = daughter->sister;
			}

			if ( !counter ) return;
		}
		else return;
	}

	total.x /= counter;
	total.z /= counter;

	if ( super->childCount == 0 )
	{
		daughter = super->child;

	 	while ( daughter && !daughter->battleInfo ) daughter = daughter->sister;

		if ( daughter ) super->battleInfo->bJoining = True;
	}

	Location ltotal( BattleToDist( total.x ), BattleToDist( total.z ) );

	// new code

	daughter = super->child;

	OrderMode action = ORDER_CautiousAdvance;

	int xdiff = mcos( Yard( 400 ), ang );
	int ydiff = msin( Yard( 400 ), ang );

	super->battleInfo->where = ltotal;

	if ( tempbl ) clearSeperated( super );
	else clearSeperated( super, 1 );

	if ( distance( daughter->battleInfo->battleOrder.destination.x - daughter->battleInfo->where.x, daughter->battleInfo->battleOrder.destination.y - daughter->battleInfo->where.y ) < 2500 )
	{
		action = ORDER_Hold;
	}

	if ( tempbl || super->rank == Rank_Army )
	{
		if ( !tempbl )
		{
		 	ltotal.x = super->battleInfo->where.x - xdiff;
		 	ltotal.y = super->battleInfo->where.y - ydiff;
		}
		else
		{
			ltotal.x = tempbl->x - xdiff;
			ltotal.y = tempbl->y - ydiff;
		}

		Grid* gr = battle->grid->getGrid( Zoom_1 );

		int egx, egy;

		if ( FitFlag ) action = ORDER_Stand;
		else 
		{
			egx = DistToBattle( ltotal.x ) + battle->grid->getTotalWidth()  / 2;
			egy = DistToBattle( ltotal.y ) + battle->grid->getTotalHeight() / 2;

			egx /= gr->gridSize;
			egy /= gr->gridSize;

			int offset = ( 6 - super->rank ) << 1;

			if ( egx > offset && egy > offset && egx < (gr->gridCount - offset) && egy < (gr->gridCount - offset) ) action = ORDER_Stand;
			else action = ORDER_CautiousAdvance;
		}
		
	   	SentBattleOrder* order;

			order = new SentBattleOrder( action, ltotal, battle->gameTime );

			// super->battleInfo->battleOrder.destination = *tempbl;

			super->battleInfo->sendBattleOrder( order );
		#ifdef DEBUG_CHRIS
		PutInLog ( "\r\n\r\nCorps::" );
		if ( super->general ) PutInLog( super->general->name );
		PutInLog( " ordered to ");
		PutOrderMode( action );
		PutInLog ( " from " );

		PutOrderMode( super->battleInfo->battleOrder.mode );
		PutInLog ( "ing at " );

		egx = DistToBattle( super->battleInfo->where.x ) + battle->grid->getTotalWidth()  / 2;
		egy = DistToBattle( super->battleInfo->where.y ) + battle->grid->getTotalHeight() / 2;

		egx /= gr->gridSize;
		egy /= gr->gridSize;

		PutInLog( egx );
		PutInLog( "," );
		PutInLog( egy );

		PutInLog ( " to " );
	
		egx = DistToBattle( ltotal.x ) + battle->grid->getTotalWidth()  / 2;
		egy = DistToBattle( ltotal.y ) + battle->grid->getTotalHeight() / 2;

		egx /= gr->gridSize;
		egy /= gr->gridSize;

		PutInLog( egx );
		PutInLog( "," );
		PutInLog( egy );
 
		PutInLog( "\r\n" );

		#endif
	}
	else
	{
		Unit* best = daughter;

		while ( daughter ) 
		{
			if ( daughter->battleInfo && daughter->battleInfo->battleOrder.mode < action )
			{
		 		action = daughter->battleInfo->battleOrder.mode;
				best = daughter;

		  		#ifdef DEBUG_CHRIS
			  	PutInLog( action );
			  	PutInLog( ";" );
				#endif
			}
		
			daughter = daughter->sister;
		}

		daughter = best;

		if ( !daughter ) 
		{
			#ifdef DEBUG_CHRIS
	  			PutInLog( "\r\nError	placing superior!!!!");
	  			cout << "Error in ps";
			#endif

			return;
		}

		// super->battleInfo->battleOrder.destination = daughter->battleInfo->battleOrder.destination;
		
		super->battleInfo->battleOrder.destination = ltotal;

		super->battleInfo->battleOrder.mode = daughter->battleInfo->battleOrder.mode;

		// if ( side != super->getSide() ) super->battleInfo->battleOrder.mode = ORDER_CautiousAdvance;
		// else super->battleInfo->battleOrder.mode = ORDER_Hold;

		// SentBattleOrder* order;

		// order = new SentBattleOrder( action, ltotal, battle->gameTime );

		// super->battleInfo->sendBattleOrder( order );

		super->battleInfo->battleOrder.mode = action;

		#ifdef DEBUG_CHRIS
			PutInLog ( "\r\n" );
			if ( super->general ) PutInLog( super->general->name );
			PutInLog( " ordered to ");
			PutOrderMode( super->battleInfo->battleOrder.mode );
			PutInLog( " ( ps2 )\r\n" );
		#endif
	}

	if ( action < ORDER_Stand && super->getSide() == side ) super->battleInfo->AIFlag = True;

}

void AI_Battle::DeployToBL( Location& dest, Unit* loyal )
{
#ifdef DEBUG_CHRIS
	if ( loyal->general ) PutInLog( loyal->general->name );

	PutInLog( " rank " );

	PutInLog( loyal->rank );

#endif

	Grid* gr = battle->grid->getGrid( Zoom_1 );

	int egx, egy;
	egx = DistToBattle( loyal->battleInfo->where.x ) + battle->grid->getTotalWidth()  / 2;
	egy = DistToBattle( loyal->battleInfo->where.y ) + battle->grid->getTotalHeight() / 2;

	egx /= gr->gridSize;
	egy /= gr->gridSize;

#ifdef DEBUG_CHRIS
	PutInLog( " deployed from  " );
	PutInLog( egx );
	PutInLog( "," );
	PutInLog( egy );
	PutInLog( " and" );
#endif

	Location el = dest;

	egx = DistToBattle( dest.x ) + battle->grid->getTotalWidth()  / 2;
	egy = DistToBattle( dest.y ) + battle->grid->getTotalHeight() / 2;

	egx /= gr->gridSize;
	egy /= gr->gridSize;

	if ( ( egx < 1 || egy < 1 || egx > gr->gridCount || egy > gr->gridCount ) && !FitFlag )
	{
	  loyal->battleInfo->deployUnit( loyal->location );
	  // FitFlag = False;
	}
	else loyal->battleInfo->deployUnit( dest );

#ifdef DEBUG_CHRIS
	PutInLog( " deployed to " );
	PutInLog( egx );
	PutInLog( "," );
	PutInLog( egy );

	PutInLog( ": Height = " );

	Height8* hi = gr->getHSquare( egx, egy );

	PutInLog( hi->height );
	PutInLog( "\r\n" );

	if ( loyal->general ) aiBLog.printf("%s deployed to %d,%d\r\n", loyal->general->name, egx, egy);
	else 	aiBLog.printf("General ???? deployed to %d,%d\r\n", egx, egy);
#endif

	// if ( loyal->rank > senior->rank && loyal->getSide() == side && tact >= Withdraw && loyal->isBattleReallyDetached() ) loyal->rejoinBattle();

	if ( loyal->rank == Rank_Brigade && FitFlag ) placeSuperior( loyal, 1 );

	// rotate Unit to face enemy;
}

void AI_Battle::OrderToBL( Location* dest, Unit* loyalBrigade, Boolean Flag )
{
	// if ( !FitFlag ) return;

	//if ( loyalBrigade->getSide() == side || Flag ) 
	//{
		OrderMode amode = senior->givenOrders.mode;

		if ( FitFlag && IsArmyOnMainBF( loyalBrigade ) )
		{
			if ( senior->givenOrders.mode >= ORDER_Stand ) DeployToBL( *dest, loyalBrigade );
		}
		else
		{
			amode = ORDER_CautiousAdvance;
		}

		if ( Flag || !FitFlag ) amode = ORDER_CautiousAdvance;

		if ( ( amode >= ORDER_Stand ) && distance( loyalBrigade->battleInfo->where.x - dest->x, loyalBrigade->battleInfo->where.y - dest->y ) < 2000 )
		{
			amode = ORDER_CautiousAdvance;	
		}

		loyalBrigade->battleInfo->battleOrder.mode = amode;

		loyalBrigade->battleInfo->battleOrder.destination = *dest;
		
		// SentBattleOrder* order;

		// order = new SentBattleOrder( amode, *dest, battle->gameTime );

		// loyalBrigade->battleInfo->sendBattleOrder( order );

		// sendBattleOrder( order );

		// loyalBrigade->battleInfo->AIFlag = True;
	//}

#ifdef DEBUG_CHRIS
	if ( loyalBrigade->general ) PutInLog( loyalBrigade->general->name );
	PutInLog( " ordered to ");
	if ( !Flag ) PutOrderMode( senior->battleInfo->battleOrder.mode );
	else PutInLog( "Cautious Advance" );
	
	PutInLog ( " from " );
#endif

	Grid* gr = battle->grid->getGrid( Zoom_1 );

	Location el = loyalBrigade->battleInfo->where;

	int egx, egy;
#ifdef DEBUG_CHRIS

	egx = DistToBattle( el.x ) + battle->grid->getTotalWidth()  / 2;
	egy = DistToBattle( el.y ) + battle->grid->getTotalHeight() / 2;

	egx /= gr->gridSize;
	egy /= gr->gridSize;

	//if ( loyalBrigade->getSide() == side || Flag )
	//{
		PutInLog( egx );
		PutInLog( "," );
		PutInLog( egy );

		PutInLog ( " to " );
	
		if ( loyalBrigade->general ) aiBLog.printf("%s deployed from %d,%d", loyalBrigade->general->name, egx, egy);
		else aiBLog.printf("General ???? deployed from %d,%d\r\n", egx, egy);
#endif

		egx = DistToBattle( dest->x ) + battle->grid->getTotalWidth()  / 2;
		egy = DistToBattle( dest->y ) + battle->grid->getTotalHeight() / 2;

		egx /= gr->gridSize;
		egy /= gr->gridSize;
	//}
#ifdef DEBUG_CHRIS

	PutInLog( egx );
	PutInLog( "," );
	PutInLog( egy );

	PutInLog( "\r\n" );

	aiBLog.printf(" to %d,%d\r\n", egx, egy);
#endif

	if ( egx < 0 || egx > gr->gridCount || egy < 0 || egy > gr->gridCount )
	{
		if ( loyalBrigade->getSide() != side && loyalBrigade->battleInfo->battleOrder.mode >= ORDER_Stand )
		{
		 	loyalBrigade->battleInfo->battleOrder.mode = ORDER_CautiousAdvance;

	  //		Location tempbl( loyalBrigade->battleInfo->where.x, loyalBrigade->battleInfo->where.y );
	  //		DeployToBL( tempbl, loyalBrigade );
		}
#ifdef DEBUG_CHRIS
		else PutInLog( "\r\nError!!\r\n" );
#endif
	}
	
	// if ( loyalBrigade->rank > senior->rank && loyalBrigade->getSide() == side && tact >= Withdraw && loyalBrigade->isBattleReallyDetached() ) loyalBrigade->rejoinBattle();

}

void AI_Battle::FindDeployCentre( Side s, Unit* loyal )
{

	Location ll = averageEnemyLoc( s, loyal );
	Location el = closestEnemyLoc( otherSide( s ), 0, ll );

	Location tempBL = findClosestHigh( ll, el );

#ifdef DEBUG_CHRIS
	// UWORD gx, gy;

	Grid* gr = battle->grid->getGrid( Zoom_1 );

	int egx, egy, lgx, lgy;

	egx = DistToBattle( el.x ) + battle->grid->getTotalWidth() / 2;
	egy = DistToBattle( el.y ) + battle->grid->getTotalHeight() / 2;

	lgx = DistToBattle( ll.x ) + battle->grid->getTotalWidth() / 2;
	lgy = DistToBattle( ll.y ) + battle->grid->getTotalHeight() / 2;

	egx /= gr->gridSize;
	egy /= gr->gridSize;

	lgx /= gr->gridSize;
	lgy /= gr->gridSize;

	PutInLog( "For " );
	if ( loyal && loyal->general ) PutInLog( loyal->general->name );
	PutInLog( " the Average AI Location is at " );
	PutInLog( lgx );
	PutInLog( "," );
	PutInLog( lgy );
	PutInLog( "\r\nand the Average Enemy Location is at " );
	PutInLog( egx );
	PutInLog( "," );
	PutInLog( egy );

	lgx = DistToBattle( tempBL.x ) + battle->grid->getTotalWidth() / 2;
	lgy = DistToBattle( tempBL.y ) + battle->grid->getTotalHeight() / 2;

	lgx /= gr->gridSize;
	lgy /= gr->gridSize;

	PutInLog( "\r\nBestPlace for army is at " );
	PutInLog( lgx );
	PutInLog( "," );
	PutInLog( lgy );

	PutInLog( "\r\nLflag=" );
	PutInLog( Lflag );

#endif
	
	deployCentre.x = tempBL.x;
	deployCentre.y = tempBL.y;

	if ( Lflag ) el = findClosestHigh( el, tempBL );

	ang = direction( el.x - tempBL.x, el.y - tempBL.y );
  
#ifdef DEBUG_CHRIS
	egx = DistToBattle( el.x ) + battle->grid->getTotalWidth() / 2;
	egy = DistToBattle( el.y ) + battle->grid->getTotalHeight() / 2;

	egx /= gr->gridSize;
	egy /= gr->gridSize;

	PutInLog( " and the best place for enemy is at " );
	PutInLog( egx );
	PutInLog( "," );
	PutInLog( egy );

	PutInLog( "\r\nAng = " );
	PutInLog( ang );
	PutInLog( "\r\n" );

	PutInLog( "Found Deploy centre\r\n" );

#endif
}

struct FindCentreInfo {
	Location l;
	int count;

	FindCentreInfo() { l = Location(0,0); count = 0; }
	void averageLocations(Unit* u, Boolean all);
	void add(const Location& l1) { l += l1; count++; }
	Location result()
	{
		if(count)
			return l / count;
		else
			return l;
	}
};

void FindCentreInfo::averageLocations(Unit* u, Boolean all)
{
	if(u->rank == Rank_Brigade)
	{
		if(u->battleInfo && (all || onBattleField(u->battleInfo->where)))
			add(u->battleInfo->where);
	}
	else if(u->battleInfo || u->hasCampaignReallyDetached())
	{
		u = u->child;
		while(u)
		{
			averageLocations(u, all);
			u = u->sister;
		}
	}
}


Location AI_Battle::averageEnemyLoc( Side s, Unit* loyal, Boolean allFlag )
{
#if 0	// CHRIS's VERSION

	President *p = ob->getPresident( s );

	Unit* enemyArmy = p->child;
	Unit* enemyCorps;
	Unit* enemyDivision;								 
	Unit* enemyBrigade;

	Grid* gr = battle->grid->getGrid( Zoom_1 );

	Distance ext = 0, eyt = 0;

	Distance ex, ey;

	Distance fx, xy;

	Location l(0, 0 );

	int number = 0;
	int Total = 0;

	int No_corps = corpsTotal;

	// Unit* CorpsCommander = senior;

	if ( loyal ) enemyArmy = loyal;
	
	if ( !allFlag )
	{
		while ( enemyArmy )
		{
			if ( enemyArmy->battleInfo || enemyArmy->hasCampaignReallyDetached() )
			{
				enemyCorps = enemyArmy->child;

				while( enemyCorps )
				{
					if ( enemyCorps->battleInfo || enemyCorps->hasCampaignReallyDetached() )
					{
						enemyDivision = enemyCorps->child;

						while( enemyDivision )
						{
							enemyBrigade = enemyDivision->child;
				
							while ( enemyBrigade )
							{
								if ( enemyBrigade->battleInfo && IsArmyOnMainBF( enemyBrigade ) ) 
								{
					  				if ( enemyBrigade->battleInfo->battleOrder.mode < ORDER_Stand )
									{
 										ex = ( DistToBattle( enemyBrigade->battleInfo->battleOrder.destination.x ) + battle->grid->getTotalWidth() / 2 ) / gr->gridSize;
										ey = ( DistToBattle( enemyBrigade->battleInfo->battleOrder.destination.y ) + battle->grid->getTotalHeight() / 2 ) / gr->gridSize;
									}
									else
									{
										ex = ( DistToBattle( enemyBrigade->battleInfo->where.x ) + battle->grid->getTotalWidth() / 2 ) / gr->gridSize;
										ey = ( DistToBattle( enemyBrigade->battleInfo->where.y ) + battle->grid->getTotalHeight() / 2 ) / gr->gridSize;
									}

									if ( ex >= 0 && ex <= gr->gridCount && ey >= 0 && ey <= gr->gridCount )
									{
										ext += ex;
										eyt += ey;

										number++;
									}
  								}
								enemyBrigade = enemyBrigade->sister;
							}
							enemyDivision = enemyDivision->sister;
						}
					}
				enemyCorps = enemyCorps->sister;
				}
			}
	 	enemyArmy = enemyArmy->sister;

	 	if ( loyal ) enemyArmy = 0;
		}
	}

	if ( !number )
	{	
#ifdef DEBUG_CHRIS

		if ( !allFlag )
		{
			PutInLog( "\r\nError: No unit of side " );
			PutInLog( s );
			PutInLog( " found on battlefield!\r\n" );
		}
#endif

		enemyArmy = p->child;

		while ( enemyArmy )
		{
			enemyCorps = enemyArmy->child;

			while( enemyCorps )
			{
				enemyDivision = enemyCorps->child;

				while( enemyDivision )
				{
					enemyBrigade = enemyDivision->child;
				
					while ( enemyBrigade )
					{
						if ( enemyBrigade->battleInfo ) 
						{
							ext += ( DistToBattle( enemyBrigade->battleInfo->where.x ) + battle->grid->getTotalWidth() / 2 ) / gr->gridSize;
							eyt += ( DistToBattle( enemyBrigade->battleInfo->where.y ) + battle->grid->getTotalHeight() / 2 ) / gr->gridSize;
				
							number++;
  						}
						enemyBrigade = enemyBrigade->sister;
					}
					enemyDivision = enemyDivision->sister;
				}
				enemyCorps = enemyCorps->sister;
			}
	 		enemyArmy = enemyArmy->sister;

		}

		if ( number == 0 ) 
		{
#ifdef DEBUG_CHRIS
			PutInLog( "There is no-one even in the battle!!\r\n" );
			if ( !loyal->battleInfo ) PutInLog( "Unit not in battle!\r\n" );
#endif
			ext = 9;
			eyt = 9;
			number = 1;
		}
	}
	 
	l.x = BattleToDist( ( ( ext / number ) * gr->gridSize ) - battle->grid->getTotalWidth()  / 2 );
	
	l.y = BattleToDist( ( ( eyt / number ) * gr->gridSize ) - battle->grid->getTotalHeight() / 2 );

	return l;

#else

	Unit* a;

	if(loyal)
		a = loyal;
	else
	{
		President *p = ob->getPresident(s);
		a = p->child;
	}

	Unit* top = a;


	/*
	 * Check for ones that are actually ON the battlefield
	 */

	FindCentreInfo info;

	if(!allFlag)
	{
		while(a)
		{
			info.averageLocations(a, False);

			if(loyal)
				break;
			else
				a = a->sister;
		}
	}

	/*
	 * If none left, check for anyone in battle
	 */


	if(!info.count)
		info.averageLocations(top, True);

	return info.result();


#endif

}



Location AI_Battle::closestEnemyLoc( Side s, Unit* loyal, Location el )
{
	President *p = ob->getPresident( s );

	Unit* enemyArmy = p->child;
	Unit* enemyCorps;
	Unit* enemyDivision;								 
	Unit* enemyBrigade;

	Distance bestd = 0;

	Location l(0, 0 );

	int number = 0;
	int Total = 0;

	int No_corps = corpsTotal;

	// Unit* CorpsCommander = senior;

	if ( loyal ) enemyArmy = loyal;
	
	while ( enemyArmy )
	{
		if ( enemyArmy->battleInfo || enemyArmy->hasCampaignReallyDetached() )
		{
			enemyCorps = enemyArmy->child;

			while( enemyCorps )
			{
				if ( enemyCorps->battleInfo || enemyCorps->hasCampaignReallyDetached() )
				{
					enemyDivision = enemyCorps->child;

					while( enemyDivision )
					{
						enemyBrigade = enemyDivision->child;
			
						while ( enemyBrigade )
						{
							if ( enemyBrigade->battleInfo && IsArmyOnMainBF( enemyBrigade ) ) 
							{
				  				if ( enemyBrigade->battleInfo->battleOrder.mode < ORDER_Stand )
								{
									Distance d = distanceLocation( enemyBrigade->battleInfo->battleOrder.destination, el );

									if ( !bestd || d < bestd )
									{
										l = enemyBrigade->battleInfo->battleOrder.destination;
										bestd = d;
									}
								}
								else
								{
									Distance d = distanceLocation( enemyBrigade->battleInfo->where, el );

									if ( !bestd || d < bestd )
									{
										l = enemyBrigade->battleInfo->where;

										bestd = d;
									}
								}
  							}
							enemyBrigade = enemyBrigade->sister;
						}
						enemyDivision = enemyDivision->sister;
					}
				}
			enemyCorps = enemyCorps->sister;
			}
		}
		enemyArmy = enemyArmy->sister;

		if ( loyal ) enemyArmy = 0;
	}
	
	return l;

}

int AI_Battle::findNumCorpsPerArmy( Unit* loyalArmy )
{
	Unit* loyalCorps;

	int No_corps = 0;

	if ( loyalArmy && loyalArmy->battleInfo )
	{
		loyalCorps = loyalArmy->child;
		
		while( loyalCorps )
		{
			if ( loyalCorps->battleInfo )
			{
				No_corps++;
			}
			else
			{
				if ( senior->rank == Rank_Army )
				{
					if ( loyalCorps->superior->battleInfo )
					{
						No_corps++;
					}
				}
			}

			loyalCorps = loyalCorps->sister;
		}
	}

	return No_corps;
}

int AI_Battle::findNumCorps()
{
	Unit* loyalArmy = pres->child;
	Unit* loyalCorps;

	int No_corps = 0;

	while ( loyalArmy )
	{
		if ( loyalArmy->battleInfo || loyalArmy->hasCampaignReallyDetached() )
		{
			loyalCorps = loyalArmy->child;
		
			while( loyalCorps )
			{
				if ( loyalCorps->battleInfo )
				{
					No_corps++;
				}
				loyalCorps = loyalCorps->sister;
			}
		}
		loyalArmy = loyalArmy->sister;
	}

	return No_corps;
}

int AI_Battle::findNumDivisions()
{
	Unit* loyalArmy = pres->child;
	Unit* loyalCorps;
	Unit* loyalDivision;
	// Unit* loyalBrigade;

	int No_corps = 0;

	while( loyalArmy )
	{
		if ( loyalArmy->battleInfo || loyalArmy->hasCampaignReallyDetached() )
		{
			loyalCorps = loyalArmy->child;
		
			while( loyalCorps )
			{
				if ( loyalCorps->battleInfo || loyalCorps->hasCampaignReallyDetached() )
				{
					loyalDivision = loyalCorps->child;

					while ( loyalDivision )
					{
						if ( loyalDivision->battleInfo )
						{
							No_corps++;
						}
						loyalDivision = loyalDivision->sister;
					}
				}
				loyalCorps = loyalCorps->sister;
			}
		}
		loyalArmy = loyalArmy->sister;
	}

	return No_corps;
}


int AI_Battle::findNumBrigades()
{
	Unit* loyalArmy = pres->child;
	Unit* loyalCorps;
	Unit* loyalDivision;
	Unit* loyalBrigade;

	int No_corps = 0;

	while( loyalArmy )
	{
		if ( loyalArmy->battleInfo || loyalArmy->hasCampaignReallyDetached() )
		{
			loyalCorps = loyalArmy->child;
		
			while( loyalCorps )
			{
				if ( loyalCorps->battleInfo || loyalCorps->hasCampaignReallyDetached() )
				{
					loyalDivision = loyalCorps->child;

					while ( loyalDivision )
					{
						loyalBrigade = loyalDivision->child;

						while ( loyalBrigade )
						{
							if ( loyalBrigade->battleInfo )
							{
								No_corps++;
							}
							loyalBrigade = loyalBrigade->sister;
						}
						loyalDivision = loyalDivision->sister;
					}
				}
				loyalCorps = loyalCorps->sister;
			}
		}
		loyalArmy = loyalArmy->sister;
	}

	return No_corps;
}

int AI_Battle::findNumChildren( Unit* father, Boolean flag )
{
	int count = father->childCount;


	// if ( flag ) return ( count / 3 + 1 );

	if ( count > 3 ) return 3;

	return count;
}


retValue AI_Battle::CalcTactAdvantage()
{
	if ( senior->givenOrders.mode >= ORDER_Stand && senior->givenOrders.mode <= ORDER_Defend ) 
	{
		TactAdvantage = True;
		return PASS;
	}

	if ( senior->givenOrders.mode >= ORDER_Withdraw ) 
	{
		TactAdvantage = False;
		return FAIL;
	}

	if ( TactAdvantage == True ) return PASS;
	
	return FAIL;
}

/*
 * Assign independant units to someone on the battlefield
 */

static Unit* findSenior(Unit* top, Unit* u)
{
	ASSERT(top != 0);
	ASSERT(u != 0);

	Rank wantRank = promoteRank(u->getRank());

	while(top && (top->getRank() < wantRank))
	{
		top = top->child;
		while(top)
		{
			if(top->battleInfo)
				break;
			top = top->sister;
		}
	}

	return top;
}


void AI_Battle::doTempAssign(Unit* u)
{
#ifdef DEBUG
	aiBLog.printf("doTempAssign(%s)",
		u ? u->getName(True) : "Null");
#endif

	ASSERT(senior != 0);
	ASSERT(senior->rank >= Rank_Army);

	while(u)
	{
		if(u->battleInfo && 
			u->isCampaignReallyDetached() && 
			(senior->rank < u->rank))
		{
			u->battleInfo->reassign = u->superior;

			/*
			 * Find senior of appropriate rank
			 */

			Unit* top = findSenior(senior, u);

			Rank wantRank = promoteRank(u->getRank());

			ASSERT(top != 0);
			ASSERT(top->getRank() == wantRank);

			if(top && (top->getRank() == wantRank))
			{
				ob->assignUnit(u, top);

				u->reattachBattle(True);
			}

			// u->makeCampaignReallyIndependant( False );
		}

		if(u->child)
			doTempAssign(u->child);

		u = u->sister;
	}
}

void AI_Battle::tempAssignUnits()
{
#ifdef DEBUG
	aiBLog.printf("tempAssignUnits()");
#endif

	doTempAssign(pres->child);

#if 0

 	Unit* loyal1 = pres->child;

	while ( loyal1 )
	{
		Unit* loyal2 = loyal1->child;

		while ( loyal2 )
		{
			if ( loyal2->battleInfo && loyal2->isCampaignReallyDetached() && (senior->rank < loyal2->rank))
			{
				loyal2->battleInfo->reassign = loyal2->superior;
 	
				ob->assignUnit( loyal2, senior );

				loyal2->reattachBattle( True );

				// loyal2->makeCampaignReallyIndpendant( False );

			}
			
			Unit* loyal3 = loyal2->child;

			while( loyal3 )
			{
				if ( loyal3->battleInfo && loyal3->isCampaignReallyDetached() && senior->rank < loyal3->rank )
				{
					loyal3->battleInfo->reassign = loyal3->superior;
 	
					Unit* target = senior;

					while( target && target->rank != loyal3->rank ) target = target->child;

					if ( !target ) 
					{
						target = target->superior;

						while( !target->battleInfo ) target = target->sister;
					}

					if ( !target ) target = senior;

					ob->assignUnit( loyal3, target );

					loyal3->reattachBattle( True );

					// loyal3->makeCampaignReallyIndpendant( False );
				}

				Unit* loyal4 = loyal3->child;

				while( loyal4 )
				{
					if ( loyal4->battleInfo && loyal4->isCampaignReallyDetached() && senior->rank < loyal4->rank )
					{
						loyal4->battleInfo->reassign = loyal4->superior;
 	
						Unit* target = senior;
						
						while( target && target->rank != loyal3->rank ) target = target->child;

						if ( !target ) 
						{
							target = target->superior;

							while( !target->battleInfo ) target = target->sister;
						}

						if ( !target ) target = senior;

						ob->assignUnit( loyal4, target );
					
						loyal4->reattachBattle( True );
					}

					loyal4 = loyal4->sister;
				}

				loyal3 = loyal3->sister;
			}
		  	loyal2 = loyal2->sister;
		}
		loyal1 = loyal1->sister;
	}
	return;
#endif
}

/*
 * A Bugged function from Chris:
 *		assignUnit calls were backwards!
 */

void AI_Battle::doReassign(Unit* u)
{
#ifdef DEBUG
	aiBLog.printf("doReassign(%s)",
		u ? u->getName(True) : "Null");
#endif

	while(u)
	{
		if(u->battleInfo && u->battleInfo->reassign) 
		{
			ob->assignUnit(u, u->battleInfo->reassign);	
		
			u->battleInfo->reassign = 0;
			// u->makeCampaignReallyIndpendant( True );
		}

		if(u->child)
			doReassign(u->child);

		u = u->sister;
	}
}


void AI_Battle::ReassignUnits()
{
#ifdef DEBUG
	aiBLog.printf("ReassignUnits()");
#endif

	doReassign(pres->child);

#if 0	
	Unit* loyal1 = pres->child;

	/*
	 * For each Army
	 */

	while ( loyal1 )
	{
		// loyal1 = Armylist[ corpLoop ];

		doReasssign(loyal1);

		Unit* loyal2 = loyal1->child;

		while ( loyal2 )
		{
			doReasssign(loyal2);

			Unit* loyal3 = loyal2->child;

			while( loyal3 )
			{
				doReasssign(loyal3);

				Unit* loyal4 = loyal3->child;

				while(loyal4)
				{
					doReasssign(loyal4);

					loyal4 = loyal4->sister;
				}


				loyal3 = loyal3->sister;
			}
		  	loyal2 = loyal2->sister;
		}
		
		loyal1 = loyal1->sister;
	}

	return;
#endif
}

Boolean AI_Battle::IsArmyOnMainBF( Unit* enemyArmy )
{
	Grid* gr = battle->grid->getGrid( Zoom_1 );

	int size = gr->gridCount;

	Unit* tempu = enemyArmy->child;

	while ( tempu )
	{
		Location bloc = tempu->battleInfo->where;

		bloc.x = DistToBattle( bloc.x ) + battle->grid->getTotalWidth()  / 2;
		bloc.y = DistToBattle( bloc.y ) + battle->grid->getTotalHeight() / 2;

		bloc.x /= gr->gridSize;
		bloc.y /= gr->gridSize;

		if ( ( bloc.x < 0 || bloc.x > size )
		  || ( bloc.y < 0 || bloc.y > size ) ) return False;

		tempu = tempu->sister;
	}

	return True;
}
	

void AI_Battle::DoesCorpFit( Unit* loyalCorps )
{
	Unit* child1 = loyalCorps->child;
	Unit* child2;

	Grid* gr = battle->grid->getGrid( Zoom_1 );

	int lgx, lgy;

	if ( child1->rank > Rank_Brigade )
	{
		lgx = DistToBattle( loyalCorps->battleInfo->where.x ) + battle->grid->getTotalWidth() / 2;
		lgy = DistToBattle( loyalCorps->battleInfo->where.y ) + battle->grid->getTotalHeight() / 2;
	 	
		lgx /= gr->gridSize;
		lgy /= gr->gridSize; 

		if ( lgx < 1 || lgy < 1 || lgx > (gr->gridCount-1) || lgy > (gr->gridCount-1) )
		{
			FitFlag = False;		 
		}
		else
		{
		 	FitFlag = True;
		}

		return;
	}

	while( child1 )
	{
	 	child2 = child1->child;

		if ( child2->rank > Rank_Brigade	)
		{
			lgx = DistToBattle( child1->battleInfo->where.x ) + battle->grid->getTotalWidth() / 2;
			lgy = DistToBattle( child1->battleInfo->where.y ) + battle->grid->getTotalHeight() / 2;
	
			lgx /= gr->gridSize;
			lgy /= gr->gridSize; 
		
			if ( lgx < 1 || lgy < 1 || lgx > ( gr->gridCount - 1 ) || lgy > (gr->gridCount-1) )
			{
				FitFlag = False;		 
			}
			else
			{
				FitFlag = True;
			}

			return;
		 }
		 else
	 	 {

			while( child2 )
			{
				lgx = DistToBattle( child2->battleInfo->where.x ) + battle->grid->getTotalWidth() / 2;
				lgy = DistToBattle( child2->battleInfo->where.y ) + battle->grid->getTotalHeight() / 2;
	  	
				lgx /= gr->gridSize;
				lgy /= gr->gridSize; 

				if ( lgx < 1 || lgy < 1 || lgx > (gr->gridCount-1) || lgy > (gr->gridCount-1) )
				{
					FitFlag = False;		 
		
					#ifdef DEBUG_CHRIS
						PutInLog( "\r\n**** The whole unit of " );
						if ( loyalCorps->general )  PutInLog( loyalCorps->general->name );
						else PutInLog( "General ????" );
						PutInLog( "	is not on the battleField!\r\n" );
					#endif
		
					return;
				}

				child2 = child2->sister;
			}
		}
		child1 = child1->sister;
	}

	FitFlag = True;
}

Location AI_Battle::getDestOffset( Unit* loyal, Location& dest )
{
	Unit* daughter = loyal->child;

	Wangle direct = ang;

	Wangle attempt;

   Wangle best = 0xffff; 

	Unit* bestunit = daughter;

	while ( daughter )
	{
		attempt = direction( daughter->battleInfo->where.x - loyal->battleInfo->where.x, daughter->battleInfo->where.y - loyal->battleInfo->where.y );

		if ( abs(attempt - direct) < best )
 		{
			bestunit = daughter;

			best = abs( attempt - direct );
		}

		daughter = daughter->sister;
	}
  
   signed int xdif, ydif;
	
	if ( bestunit ) 
	{
  		xdif = bestunit->battleInfo->where.x - loyal->battleInfo->where.x;
		ydif = bestunit->battleInfo->where.y - loyal->battleInfo->where.y;
	}
	else
	{
		xdif = 0;
		ydif = 0;
	}

  	dest.x -= xdif;
	dest.y -= ydif;

#ifdef DEBUG_CHRIS
	if ( !bestunit ) PutInLog( "No bestunit(!): " );
	PutInLog( " xdif=" );
	PutInLog( xdif );
	PutInLog( ", ydif=" );
	PutInLog( ydif );
	PutInLog( "\r\n" );
#endif

   return dest;
}

void AI_Battle::faceArmies( Side s )
{
  	Unit* loyal1 = pres->child;

	Location el = averageEnemyLoc( otherSide( s ), 0, 0 );

	while ( loyal1 )
	{	
		if(loyal1->battleInfo)
		{
			Location diff = el - loyal1->battleInfo->where;
		  	loyal1->battleInfo->formationFacing = direction(diff.x, diff.y);
		}

		Unit* loyal2 = loyal1->child;

		while ( loyal2 )
		{
			if(loyal2->battleInfo)
			{
				Location diff = el - loyal2->battleInfo->where;
				loyal2->battleInfo->formationFacing = direction(diff.x, diff.y);
			}

			Unit* loyal3 = loyal2->child;

			while( loyal3 )
			{
				if(loyal3->battleInfo)
				{
					Location diff = el - loyal3->battleInfo->where;
					loyal3->battleInfo->formationFacing = direction(diff.x, diff.y);
				}

				Unit* loyal4 = loyal3->child;

				while( loyal4 )
				{
				 	if(loyal4->battleInfo)
					{
						Location diff = el - loyal4->battleInfo->where;
						loyal4->battleInfo->formationFacing = direction(diff.x, diff.y);
					}

				 	loyal4 = loyal4->sister;
				}
				loyal3 = loyal3->sister;
			}
		  	loyal2 = loyal2->sister;
		}
		loyal1 = loyal1->sister;
	}
}

Boolean AI_Battle::IsArmyOffMainBF( Unit* enemyArmy )
{
	Grid* gr = battle->grid->getGrid( Zoom_1 );

	int size = gr->gridCount;

	Unit* tempu = enemyArmy->child;

	while ( tempu )
	{
		Location bloc = tempu->battleInfo->where;

		bloc.x = DistToBattle( bloc.x ) + battle->grid->getTotalWidth()  / 2;
		bloc.y = DistToBattle( bloc.y ) + battle->grid->getTotalHeight() / 2;

		bloc.x /= gr->gridSize;
		bloc.y /= gr->gridSize;

		if ( ( bloc.x >= 0 && bloc.x <= size )
		  && ( bloc.y >= 0 && bloc.y <= size ) ) return False;

		if ( tempu->rank < Rank_Brigade && !IsArmyOffMainBF( tempu ) ) return False;

		tempu = tempu->sister;
	}

	return True;
}

