/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign AI: Send Orders to Units
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdlib.h>
#include "ai_camp.h"
#include "ai_unit.h"
#include "aic_misc.h"
#include "campaign.h"
#include "campwld.h"
#include "camptab.h"
#include "unititer.h"
#include "combval.h"
#include "myassert.h"
#include "options.h"

#ifdef DEBUG
#include "dialogue.h"
#endif

/*
 * Compare AI_Unit's current status with an event
 */

Boolean isSameOrder(const UnitAI* ai, const AIC_Event* event)
{
	if(ai->ai.action != event->action)
		return False;

	switch(ai->ai.action)
	{
	case AIA_AttackTown:
	case AIA_DefendTown:
		ASSERT(ai->ai.who == 0);
		ASSERT(event->who == 0);
		return (ai->ai.where == event->where);
	case AIA_AttackUnit:
	case AIA_ReinforceUnit:
		ASSERT(ai->ai.where == 0);
		ASSERT(event->where == 0);
		return (ai->ai.who == event->who);
	default:
		throw GeneralError("Illegal value for ai action %d", (int) ai->ai.action);
	}
}

/*
 * Clear all aiInfo variables that refer to event
 */

void clearAIorder(Unit* top, AIC_Event* event)
{
	UnitIter iter(top, True, False, Rank_Army, Rank_Brigade);
	while(iter.ok())
	{
		Unit* unit = iter.get();

		ASSERT(unit->aiInfo != 0);

		// If same order then clear it

		if( (unit->aiInfo->ai.action == event->action) &&
			(unit->aiInfo->ai.where == event->where) &&
			(unit->aiInfo->ai.who == event->who))
		{
			unit->aiInfo->ai.clear();
		}
	}
}

/*-----------------------------------------------------------------
 * List of Units sorted by likelihood to perform action at a location
 *
 * The likelihood is found by combining:
 *		Distance from location (nearer is more likely)
 *		Priority of existing action (low is more likely)
 *
 * The resultant value must be better than their existing priority.
 */

struct UnitDistElement {
	Unit* unit;				// What unit are we referring to
	AIC_Priority priority;		// Priority
	AIC_Priority sortPriority;
	CombatValue cv;		// Combat Value for this unit
#ifdef DEBUG
	int dist;				// In Miles
#endif
};


class UnitListDist {
	UnitDistElement* elements;
	int howMany;
	int next;

public:
	UnitListDist();
	~UnitListDist();

	int make(Unit* top, const AIC_Event* event);

	Boolean get(UnitDistElement*& unit);										// Get unit nearest to location
	Boolean isFinished() const;
};

UnitListDist::UnitListDist()
{
	elements = 0;
	howMany = 0;
	next = 0;
}

UnitListDist::~UnitListDist()
{
	if(elements)
		delete[] elements;
}

/*
 * Compare function for qsort
 */

int compareUnitDist(const void* e1, const void* e2)
{
	const UnitDistElement* el1 = (UnitDistElement*) e1;
	const UnitDistElement* el2 = (UnitDistElement*) e2;

	return el2->sortPriority - el1->sortPriority;
}

/*
 * Combine priority and distance
 *
 * This needs to be a low pass filter function rather than 1/x
 */

AIC_Priority combinePriority(AIC_Priority priority, Distance d)
{
#if 0
	const int D1 = 150;		// 1/8 priority at 150 Miles
	const int R1 = 8;
	const int K = D1 / (R1 - 1);
#else
	/*
	 * K depends on priority... big priority objects increase D1
	 *
	 * NB: Changing these numbers can make quite a large effect on choice
	 *     between:
	 *			distant high priority events and
	 *			nearby low priority event.
	 *
	 * Increasing MIN_D makes low priority near events more likely
	 * Increasing MAX_D makes high priority distant events more likely
	 */

	const int MIN_D = 100;		// Minimum value for D1 at P=0
	const int MAX_D = 200;		// Maximum value for D1 at P=255

	int D1 = MIN_D + ((MAX_D - MIN_D) * priority) / 256;
	const int R1 = 8;				// 1/8 Priority at D1
	int K = D1 / (R1 - 1);
	 
#endif

	return (K * priority) / (unitToMile(d) + K);
}

/*
 * Inline function to calculate a new value as a linear change in
 * percentage
 */

inline int adjustLinearPercent(int value, int x, int minP, int maxP)
{
	return (value * minP) / 100 +
			 ( ( (value * (maxP - minP)) / 100) * x) / MaxAttribute;
}

/*
 * Linear with clipping
 *
 *   |
 *   |
 *   |			  (x2,p2)
 * ^ |		    /-----------
 * | |		   /
 * p |		  /
 *   |		 /
 *   |		/
 *   |-----/ (x1,p1)
 *   |
 *   +------------------------
 *          x ->
 */

inline int adjustWithAttribute(int value, int x, int x1, int p1, int x2, int p2)
{
	int p;

	if(x <= x1)
		p = p1;
	else if(x >= x2)
		p = p2;
	else
		p = p1 + ((p2 - p1) * x) / (x2 - x1);

	return (value * p) / 100;
}


/*
 * Is unit eligible for given event?
 */

static Boolean considerUnitEvent(Unit* unit, const AIC_Event* event, AIC_Priority& p)
{
	ASSERT(unit != 0);
	ASSERT(event != 0);
	ASSERT(unit->aiInfo != 0);


#ifdef DEBUG1
	aiLog.printf("Considering %s", unit->getName(True));
#endif

	/*
	 * Don't re-order withdrawing units
	 */

	if(unit->realOrders.basicOrder() == ORDERMODE_Withdraw)
	{
		return False;
	}

	/*
	 * If currently same event, then return True
	 */

	if(isSameOrder(unit->aiInfo, event))
	{
#ifdef DEBUG1
		aiLog.printf("Same Order");
#endif
		// unit->aiInfo->ai.priority = priority;
		// p = priority;

		/*
		 * return its previous priority because it may have been set
		 * by a unit trying to attack and is using this location to
		 * rally its forces.
		 */

		p = unit->aiInfo->ai.priority;
		return True;
	}

	Location l;
	event->getLocation(l);
	Distance d = distanceLocation(l, unit->location);
	AIC_Priority priority = combinePriority(event->priority, d);

	/*
	 * Adjust priority based on General's abilities
	 *
	 * 
	 *   +------------+-----------+----------+
	 *   |            |  ATTACK   |  DEFEND  |
	 *   +------------+-----------+----------+
	 *   | Efficiency |	  0 	   |    0	  |
	 *   | Agression  | -n .. +n  | +n .. -n |
	 *   | Ability    |	  0      |    0	  |
	 *   | Experience | +n .. 0   | +n .. 0  |
	 *   +------------+-----------+----------+
	 */

	General* g = unit->general;
	if(g)
	{
		/*
		 * Low experience increase priority
		 */

		priority = adjustLinearPercent(priority, g->experience, 150, 100);

		/*
		 * High agression means more likely to attack less likely to defend
		 */

		if( (event->action == AIA_AttackTown) ||
			 (event->action == AIA_AttackUnit) )
			priority = adjustLinearPercent(priority, g->aggression, 75, 125);
		else
			priority = adjustLinearPercent(priority, g->aggression, 125, 75);

		/*
		 * High ability means more likely to want to carry out an order
		 */

		priority = adjustLinearPercent(priority, g->ability, 80, 120);

		/*
		 * Low efficiceny, means less likely to carry out order
		 */

		priority = adjustLinearPercent(priority, g->efficiency, 50, 100);
	}

	/*
	 * Alter using unit's attributes
	 */

	UnitInfo ui(True, False);
	unit->getInfo(ui);

	if( (event->action == AIA_AttackTown) ||
		 (event->action == AIA_AttackUnit) )
	{
		// priority = adjustLinearPercent(priority, ui.fatigue, 120, 10);
		// priority = adjustLinearPercent(priority, ui.morale, 10, 150);
		// priority = adjustLinearPercent(priority, ui.supply, 5, 150);
		priority = adjustWithAttribute(priority, ui.fatigue, 50,100,  255, 10);
		priority = adjustWithAttribute(priority, ui.morale,   0, 10,  150,100);
		priority = adjustWithAttribute(priority, ui.supply,   0, 10,  150,100);
	}
	else
	{
		// priority = adjustLinearPercent(priority, ui.fatigue, 75, 125);
		// priority = adjustLinearPercent(priority, ui.morale, 125, 75);
		// priority = adjustLinearPercent(priority, ui.supply, 125, 75);
		priority = adjustWithAttribute(priority, ui.fatigue, 50,100, 255,150); 
		priority = adjustWithAttribute(priority, ui.morale,   0,125, 150,100);
		priority = adjustWithAttribute(priority, ui.supply,   0,125, 150,100);
	}
	priority = adjustLinearPercent(priority, ui.experience, 125, 75);

	/*
	 * Alter using event->nearPriority
	 */

	const int N1 = 32;		// Reduction is 1/4 at nearPriority = 32
	const int V1 = 4;
	const int K = N1 / (V1 - 1);
	const int R = 2;			// Allow priority to have 2*P added to it

	int newPriority = priority + (K * R * priority) / (event->nearPriority + K);

	// int newPriority = priority + priority / 2 - (priority * event->nearPriority) / (2 * 256);

	if(newPriority > 255)
		newPriority = 255;
	priority = newPriority;


	/*
	 * If priority acceptable then return True
	 */

	if(unit->aiInfo->ai.priority < priority)
	{
#ifdef DEBUG1
		aiLog.printf("Priority %d < %d",
			unit->aiInfo->ai.priority, p);
#endif
		p = priority;
		return True;
	}

#ifdef DEBUG1
	aiLog.printf("Not eligible");
#endif

	return False;
}


/*
 * Create the Unit List
 * Returns the number of units in the list
 */

int UnitListDist::make(Unit* top, const AIC_Event* event)
{
	ASSERT((elements == 0) && (howMany == 0));
	ASSERT(event != 0);
	ASSERT(event->action != AIA_None);

	/*
	 * Count the units
	 */

	int count = 0;

	UnitIter iter(top, True, False, Rank_Army, Rank_Brigade);

	while(iter.ok())
	{
		Unit* unit = iter.get();
	
		ASSERT(unit != 0);

		AIC_Priority p;

		if(considerUnitEvent(unit, event, p))
			count++;
	}

	if(count)
	{
		elements = new UnitDistElement[count];
		howMany = count;
		next = 0;

		iter.reset();

		while(iter.ok())
		{
			Unit* unit = iter.get();
	
			ASSERT(unit != 0);

			AIC_Priority p;

			if(considerUnitEvent(unit, event, p))
			{
				/*
				 * If unit already has an order, then adjust priority
				 * so that it is sorted lower down list
				 */

				AIC_Priority p1;

				if( (unit->aiInfo->ai.action != AIA_None) && !isSameOrder(unit->aiInfo, event))
					p1 = (p * 128) / (unit->aiInfo->ai.priority + 128);
				else
					p1 = p;

			 	elements[next].unit = unit;
			 	elements[next].priority = p;
			 	elements[next].sortPriority = p1;
				elements[next].cv = calc_UnitCV(unit, False);

#ifdef DEBUG
				Location l;
				event->getLocation(l);
				elements[next].dist = unitToMile(distanceLocation(l, unit->location));
#endif

			 	next++;
			}
			ASSERT(next <= count);
		}
	
		ASSERT(next == count);
		qsort(elements, howMany, sizeof(elements[0]), compareUnitDist);

		/*
		 * Set next to 0 so get() will start at the beginning again
		 */

		next = 0;

#ifdef DEBUG
		aiLog.printf("%d units sorted by priority distance", howMany);
		for(int i = 0; i < howMany; i++)
		{
			Unit* u = elements[i].unit;

			ASSERT(u != 0);
			ASSERT(u->aiInfo != 0);

			const char* what = "";

			if(u->aiInfo->ai.where)
				what = u->aiInfo->ai.where->getNameNotNull();
			else if(u->aiInfo->ai.who)
				what = u->aiInfo->ai.who->getName(True);

			aiLog.printf("%3d %3d: %5lu: %4d : %-40s [%d %s %s]",
				(int) elements[i].priority,
				(int) elements[i].sortPriority,
				(unsigned long) elements[i].cv,
				(int) elements[i].dist,
				u->getName(True),
				(int) u->aiInfo->ai.priority,
				actionNames[u->aiInfo->ai.action],
				what);
		}
#endif
	}

	return howMany;
}

/*
 * Get next unit
 * Return True if successful
 *
 * Note that I'm doing it like this as suggested in book about writing
 * bugfree code, so as to seperate the unit from whether or not there is
 * anything to get.
 */

Boolean UnitListDist::get(UnitDistElement*& unit)
{
	if(next < howMany)
	{
		unit = &elements[next++];
		return True;
	}
	else
		return False;
}

Boolean UnitListDist::isFinished() const
{
	ASSERT(next <= howMany);		// Should never be bigger than howMany

	return (next >= howMany);
}

/*-----------------------------------------------------------------
 * List of Units to send
 */

class SendElement {
	friend class SendList;
private:
	SendElement* next;
public:
	Unit* unit;
	AIC_Priority priority;
	CombatValue cv;
};

class SendList {
	SendElement entry;
public:
	SendList() { entry.next = 0; }
	~SendList();

	void add(Unit* unit, AIC_Priority priority, CombatValue cv);
	Boolean get(SendElement*& element);

	Boolean isEmpty() const { return entry.next == 0; }
};

SendList::~SendList()
{
	SendElement* el = entry.next;
	while(el)
	{
		SendElement* next = el->next;
		delete el;
		el = next;
	}
	entry.next = 0;
}

void SendList::add(Unit* unit, AIC_Priority priority, CombatValue cv)
{
	/*
	 * Adjust unit, so that if superior has no dependant units then
	 * rejoin and order the commander instead
	 */

	
	while(unit->getRank() > Rank_Army)
	{
		Unit* superior = unit->superior;
		Unit* u = superior->child;
		int cCount = 0;
		while(u)
		{
			if(!u->isCampaignDetached() && (u != unit))
			{
				cCount++;
				break;
			}
			u = u->sister;
		}

		if(cCount == 0)
		{
#ifdef DEBUG
			aiLog.printf("Upgrading order from %s to %s", unit->getName(True), superior->getName(True));
#endif
#ifdef DEBUG
			campaign->world->ob.check(__FILE__, __LINE__);
#endif
			// unit->makeCampaignDetached(False);
			unit->rejoinCampaign();

#ifdef DEBUG
			campaign->world->ob.check(__FILE__, __LINE__);
#endif
			unit = superior;
		}
		else
			break;
	}

#ifdef DEBUG
	campaign->world->ob.check(__FILE__, __LINE__);
#endif

	unit->makeCampaignDetached(True);

#ifdef DEBUG
	campaign->world->ob.check(__FILE__, __LINE__);
#endif

	SendElement* el = new SendElement;
	el->unit 		= unit;
	el->priority	= priority;
	el->cv 			= cv;

	el->next = entry.next;
	entry.next = el;

	/*
	 * Make Unit Independant...
	 */


#ifdef DEBUG
	aiLog.printf("SendList::add(%s, %d, %lu)",
		unit->getName(True), (int) priority, (unsigned long) cv);
#endif
}

Boolean SendList::get(SendElement*& element)
{
	SendElement* el = entry.next;

	if(el)
	{
		element = el;
		entry.next = el->next;
		el->next = 0;
		return True;
	}

	return False;
}

/*-----------------------------------------------------------------
 * Clear all Units variables at start of AI
 */

void CampaignAI::clearUnits()
{
	/*
	 * Deal with friendly side first
	 */

	UnitIter iter(campaign->world->ob.getPresident(aiSide), False, False, Rank_Army, Rank_Brigade);
	while(iter.ok())
	{
		Unit* unit = iter.get();
		UnitAI* ai = unit->aiInfo;

		ASSERT(ai != 0);

		ai->ai.clear();
	}

	// iter.setup(campaign->world->ob.getPresident(otherSide(side)), False, Rank_Brigade);
	iter.reset();
	while(iter.ok())
	{
		Unit* unit = iter.get();
		UnitAI* ai = unit->aiInfo;

		ASSERT(ai != 0);

		// Clear enemy variables

		ai->needAction = False;
	}

}

/*-----------------------------------------------------------------
 * Check all units for completed orders, clear flags
 */

void CampaignAI::aic_InitUnits()
{
#ifdef DEBUG
	aiLog.printf("aic_InitUnits()");
#endif

	UnitIter iter(campaign->world->ob.getPresident(aiSide), False, False, Rank_Army, Rank_Brigade);
	while(iter.ok())
	{
		Unit* unit = iter.get();
		UnitAI* ai = unit->aiInfo;
		ASSERT(ai != 0);

#if 1
		ai->ai.clear();
#else
	 	Boolean clearAction = False;
#ifdef DEBUG
		char* whyClear = "?";
#endif
		
		if(ai->ai.action != AIA_None)
		{
			switch(ai->ai.action)
			{
			case AIA_AttackTown:
				ASSERT(ai->ai.where != 0);
				if(ai->ai.where->side == aiSide)
				{
					clearAction = True;
#ifdef DEBUG
					whyClear = "Town has been taken";
#endif
				}
				break;

			case AIA_DefendTown:
				ASSERT(ai->ai.where != 0);
				if(ai->ai.where->side != aiSide)
				{
					clearAction = True;
#ifdef DEBUG
					whyClear = "Town has been lost";
#endif
				}
				break;

			case AIA_AttackUnit:
			case AIA_ReinforceUnit:
			default:
#ifdef DEBUG
				throw GeneralError("Missing case for AI_Action (%d) in %s Line %d",
					(int) ai->ai.action, __FILE__, __LINE__);
#endif
				break;
			}


			/*
			 * Check that orders are still valid and clear if completed or not valid
			 *
			 * For now we will just clear them if the unit is holding.
			 */

			if(!clearAction && (unit->realOrders.basicOrder() == ORDERMODE_Hold))
			{
				clearAction = True;
#ifdef DEBUG
				whyClear = "Holding";
#endif
			}

			/*
			 * Or if at destination
			 */

			if(!clearAction && (distanceLocation(unit->location, unit->realOrders.destination) < brigadeBattleDistance))
			{
				clearAction = True;
#ifdef DEBUG
				whyClear = "at location";
#endif
			}

			if(clearAction)
			{
#ifdef DEBUG
				aiLog.printf("Clearing %s action for %s because %s",
					actionNames[ai->ai.action],
					unit->getName(True),
					whyClear);
#endif
				ai->ai.clear();
			}
		}
#endif	// Always Clear them
	}
}

/*
 * Try add a unit to task force
 *
 * Note that this is recursive
 */

struct AddInfo {
	AIC_Priority priority;
	CombatValue neededCV;
	CombatValue gotCV;
	CombatValue lowestCV;
	Boolean finished;
};

void tryToAdd(SendList& sendList, Unit* unit, CombatValue unitCV, AddInfo& info)
{

	if( (info.gotCV + unitCV) > info.neededCV)
	{
		if(info.gotCV > info.lowestCV)
		{
#ifdef DEBUG
			aiLog.printf("Not sending %s", unit->getName(True));
#endif
			info.finished = True;
			return;
		}

		if(unit->getRank() < Rank_Brigade)
		{
#ifdef DEBUG
			aiLog.printf("Splitting %s", unit->getName(True));
#endif
			/*
			 * Recurse into dependant children
			 */

			Unit* c = unit->child;
			while(c && !info.finished && (info.gotCV < info.neededCV))
			{
				if(!c->isDetached())
				{
					CombatValue cCV = calc_UnitCV(c, False);

#ifdef DEBUG
					aiLog.printf("Recursing for %s (cv=%lu)", c->getName(True), (unsigned long) cCV);
#endif

					tryToAdd(sendList, c, cCV, info);
				}

				c = c->sister;
			}

			return;
		}

		info.finished = True;		// Cut out an extra iteration...
	}

	info.gotCV += unitCV;

	// ... Might want to do some more tests in here
	// e.g. use general's abilities, morale, etc.

	sendList.add(unit, info.priority, unitCV);

#ifdef DEBUG
	aiLog.printf("Adding %s to task force ==> %lu",
		unit->getName(True),
			(unsigned long) info.gotCV);
#endif
}


/*
 * Allocate tasks to units
 */

void CampaignAI::aic_ProcUnits()
{
#ifdef DEBUG
	aiLog.printf("===================================");
	aiLog.printf("aic_ProcUnits()");
	events.print();

	char popupBuffer[200] = "";

	PopupText popup(0);
#endif

	/*
	 * Prepare some useful variables
	 */

	President* aiPresident = campaign->world->ob.getPresident(aiSide);

	/*
	 * Process events
	 */

	Boolean changesMade = False;

	int passes = 0;

	do
	{
		++passes;
#ifdef DEBUG
		aiLog.printf("-------");
		aiLog.printf("Pass %d", passes);
		aiLog.printf("-------");
		int popCount = 0;
#endif

		changesMade = False;
		events.rewind();

		AIC_Event* event;

		int loopCount = 0;

		while((loopCount++ < 1000) && events.next(event))
		{
			ASSERT(event != 0);
			ASSERT(event->action != AIA_None);

#ifdef DEBUG
			if(popCount-- == 0)
			{
				sprintf(popupBuffer, "Pass %d\rLoop %d",
					passes, loopCount-1);
				popup.showText(popupBuffer);
				popCount = 9;
			}


			aiLog.printf("-------------- %d ------------------------", loopCount);
			event->print();
#endif

			CombatValue enemyCV = event->enemyCV;
			CombatValue friendlyCV = event->friendlyCV;

			Boolean needForce = False;		// Set if not worth sending anything unless we have a full CV

			/*
		 	 * Work out Combat Value needed
		 	 * and combat value already allocated to task
		 	 *
		 	 * NB: Since this may be taken into account whilst allocating
		 	 *     priorities, it might be best to store these values along
		 	 *     with cities and units.
		 	 */

			switch(event->action)
			{
			case AIA_AttackTown:
				ASSERT(event->where != 0);
				needForce = True;
				break;

			case AIA_DefendTown:
				ASSERT(event->where != 0);
				needForce = False;
				break;

			case AIA_AttackUnit:
			case AIA_ReinforceUnit:
			default:
	#ifdef DEBUG
				throw GeneralError("Missing case for AI_Action (%d) in %s Line %d",
					(int) event->action, __FILE__, __LINE__);
	#endif
				break;
			}


	#ifdef DEBUG
			aiLog.printf("enemyCV = %lu, friendlyCV = %lu", (unsigned long) enemyCV, (unsigned long) friendlyCV);
	#endif

			/*
		 	* Work out combat value required based on the abilities of the
		 	* general closest to the action.
		 	*
		 	* ... To Be written properly
		 	* For now just use difference between enemy and friendly
		 	*
		 	* enemyCV is increased because most generals overestimated enemy
		 	* strength.
		 	*
		 	* Note that a comparison must be done before 
		 	* subtracting friendlyCV, because CombatValue is unsigned.
		 	*/

			AddInfo addInfo;

			addInfo.neededCV = enemyCV;
			addPercent(addInfo.neededCV, 30);		// This should be random...ish
			
			if(addInfo.neededCV > friendlyCV)
			{
				addInfo.neededCV -= friendlyCV;

				/*
			 	 * Calculate lowest acceptable CV
			 	 */

				addInfo.lowestCV = enemyCV;
				if(addInfo.lowestCV > friendlyCV)
					addInfo.lowestCV -= friendlyCV;
				else
					addInfo.lowestCV = 0;

#ifdef DEBUG
				aiLog.printf("neededCV = %lu, lowestCV = %lu",
					(unsigned long) addInfo.neededCV, (unsigned long) addInfo.lowestCV);
#endif

				/*
			 	 * Build list of independant units sorted by distance
			 	 *
			 	 * If no units are found, then stop processing actions.
			 	 * We can do this because we know that we are processing
			 	 * actions in reverse order of priority.
			 	 */

				UnitListDist unitList;
				if(unitList.make(aiPresident, event) != 0)
				{
#ifdef DEBUG
					aiLog.printf("");
#endif
					/*
				 	 * Keep getting units until enough CV is got
				 	 */

					UnitDistElement* unitElement;

					addInfo.gotCV = 0;		// CV that we can muster
					addInfo.finished = False;

					SendList sendList;

					while(!addInfo.finished && (addInfo.gotCV <= addInfo.neededCV) && unitList.get(unitElement))
					{
						ASSERT(unitElement != 0);

						Unit* unit = unitElement->unit;

						ASSERT(unit != 0);

						addInfo.priority = unitElement->priority;
						tryToAdd(sendList, unit, unitElement->cv, addInfo);
					}

					/*
				 	 * Do we have an acceptable force?
					 *
				 	 */

					if((needForce && (addInfo.gotCV <= addInfo.lowestCV)) || (addInfo.gotCV <= 0))
					{
#ifdef DEBUG
						aiLog.printf("Not enough forces... removing");
						if(changesMade)
							aiLog.printf("and rewinding");
#endif
						/*
					 	 * Not enough forces
					 	 *
					 	 * Remove all orders for this action and rewind
					 	 */

						clearAIorder(aiPresident, event);
#ifdef DO_REMOVE
						events.remove();			// Remove current event
#endif

#ifdef DO_REWIND
						if(changesMade)
						{
							events.rewind();
							changesMade = False;
						}
#endif
					}
					else
					{
#ifdef DEBUG
						aiLog.printf("");
#endif
						/*
						 * If it was an attack order and not enough forces
						 * available yet, then change it to defend near town
						 */

						AIC_Event newEvent = *event;

						if(event->action == AIA_AttackTown)
						{
							if(!event->canAttack && event->closeFriend && !sendList.isEmpty())
							{
								newEvent.action = AIA_DefendTown;
								newEvent.where = event->closeFriend;
#ifdef DEBUG
								aiLog.printf("Changing event to %s %s",
									actionNames[newEvent.action],
									newEvent.where->getNameNotNull());
#endif
							}
						}

						/*
					 	 * We have enough forces, so set up their potential order
					 	 */

						SendElement* sendElement;
						while(sendList.get(sendElement))
						{
							ASSERT(sendElement != 0);
							Unit* unit = sendElement->unit;
							ASSERT(unit != 0);

							ASSERT(unit->aiInfo != 0);

							/*
							 * If order is different then set changesMade flag
							 */

#ifdef DEBUG
							Boolean newOrder = True;
#endif

							if(!isSameOrder(unit->aiInfo, &newEvent))
							{
#ifdef DEBUG
								newOrder = True;
#endif
								if(unit->aiInfo->ai.action != AIA_None)
									changesMade = True;

								unit->aiInfo->ai.action = newEvent.action;
								unit->aiInfo->ai.where 	= newEvent.where;
								unit->aiInfo->ai.who 	= newEvent.who;
							}

							unit->aiInfo->ai.priority = sendElement->priority;	// find.bestPriority;

#ifdef DEBUG
							aiLog.printf("Setting %s [p=%d], %s",
								unit->getName(True),
								(int) unit->aiInfo->ai.priority,
								newOrder ? "New Order" : "-");
#endif
							delete sendElement;
						}
					}
				} // unitList.make()
				else
				{
#ifdef DEBUG
					aiLog.printf("No Units: Removing from events");
#endif
					clearAIorder(aiPresident, event);
#ifdef DO_REMOVE
					events.remove();
#endif
				}
			} // needCV > friendlyCV
		}	// while events
	} while(changesMade && (passes < aiPasses));

	/*
	 * Go through all units and send their orders
	 */

#ifdef DEBUG
	aiLog.printf("-------------------------------------");
#endif

	UnitIter iter(aiPresident, True, False, Rank_Army, Rank_Brigade);
	while(iter.ok())
	{
		Unit* unit = iter.get();

		ASSERT(unit != 0);
		ASSERT(unit->aiInfo != 0);

		UnitAI* ai = unit->aiInfo;

		if(ai->ai.action != AIA_None)
		{
			Location l;

			ASSERT((ai->ai.where != 0) || (ai->ai.who != 0));

#ifdef DEBUG
			const char* whereText;

			if(ai->ai.where)
				whereText = ai->ai.where->getNameNotNull();
			else if(ai->ai.who)
				whereText = ai->ai.who->getName(True);
#endif

			if(ai->ai.where)
				l = ai->ai.where->location;
			else if(ai->ai.who)
				l = ai->ai.who->location;

			/*
			 * Decide which order to use out of:
			 *		Advance : Normal
			 *		Cautious Advance : If bad morale, fatigue, supply.
			 *		Attack : If close to destination and attacking something with good odds
			 */

			Order newOrder = Order(ORDER_Advance, ORDER_Land, l);
			campaign->world->getBestRoute(unit->location, l, newOrder, aiSide);
			unit->sendOrder(newOrder);

#ifdef DEBUG
			aiLog.printf("%s ordered to %s %s",
				unit->getName(True),
				actionNames[ai->ai.action],
				whereText);
#endif
		}
#ifdef DEBUG
		else
			aiLog.printf("%s was not ordered to do anything", unit->getName(True));
#endif
	}
}


