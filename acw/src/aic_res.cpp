/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign AI: Resources
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "ai_camp.h"
#include "city.h"
#include "campaign.h"
#include "campwld.h"
#include "combval.h"
#include "unititer.h"
#include "memptr.h"
#include "camptab.h"
#include "mobilise.h"
#include "game.h"

#ifdef DEBUG
#include "tables.h"

LogFile aiResLog("ai_res.log");
#endif

struct BuildTable {
	int probability;
};

struct BuildInfo {
	CombatValue cvNeeded;
	CombatValue cvGot;
	CombatValue seaNavalGot;
	CombatValue riverNavalGot;
	CombatValue seaNavalNeeded;
	CombatValue riverNavalNeeded;

	BuildTable table[Mob_HowMany];
	int totalProb;
	Boolean canBuildNow;
	Facility* f;

	void make();
	void pick();
#ifdef DEBUG
	void logTable();
#endif
};


#ifdef DEBUG
/*
 * Display probability table to log file
 */

void BuildInfo::logTable()
{
	ASSERT(f != 0);

	MemPtr<char> logBuffer(500);

	ResourceSet* localRsc = f->getLocalResources();
	ResourceSet* externRsc = f->getGlobalResources();

	sprintf(logBuffer, "%-20s: L=", f->getNameNotNull());
	
	localRsc->print(logBuffer + strlen(logBuffer));
	strcat(logBuffer, ", G=");
	externRsc->print(logBuffer + strlen(logBuffer));
	aiResLog.printf("%s", (char*) logBuffer);

	int i;
	BuildTable* btPtr;

	for(i = 0, btPtr = table; i < Mob_HowMany; i++, btPtr++)
	{
		aiResLog.printf("-- %3d: %s",
			btPtr->probability,
			language(buildCosts[i].description));
	}
}
#endif

/*
 * Create the probabilty table for a given facility
 */


void BuildInfo::make()
{
	ASSERT(f != 0);

	MobiliseList& mList = campaign->world->mobList;

 	totalProb = 0;
	canBuildNow = False;

	int mType;
	BuildTable* btPtr = table;
	for(mType = 0; mType < Mob_HowMany; mType++, btPtr++)
	{
		if(mList.canEverBuild(f, MobiliseType(mType)))
		{
			/*
			 * Each type has its own method of working out
			 * probability
			 */

			switch(mType)
			{
			case Mob_InfantryRegular:
			case Mob_InfantryMilitia:
			case Mob_InfantrySharpShooter:
			case Mob_Engineer:
			case Mob_cavalryRegular:
			case Mob_cavalryMilitia:
			case Mob_cavalryRegularMounted:
			case Mob_cavalryMilitiaMounted:
			case Mob_ArtillerySmoothbore:
			case Mob_ArtilleryLight:
			case Mob_ArtilleryRifled:
			case Mob_ArtillerySiege:
				{
					const int K4 = 56;	// 119;			// Days
					const int K2 = 3;
					const int K1a = 9;			// 9/14
					const int K1b = 14;
					const int K3 = -8;

					UBYTE days = buildCosts[mType].days;

					int p = (K4 - days) * cvNeeded;
					p /= (long) (cvGot + K2);
					p += (days * K1a) / K1b + K3;

					if(p > 0)
						btPtr->probability = p;
					else
						btPtr->probability = 0;
				}
				break;
			case Mob_Naval:
			case Mob_NavalIronClad:
				{
					const int K4 = 119;			// Days
					const int K2 = 30; 			// was 3
					const int K1a = 9;			// 9/14
					const int K1b = 14;
					const int K3 = -8;

					UBYTE days = buildCosts[mType].days;

					int p = (K4 - days) * seaNavalNeeded;
					p /= (long)(seaNavalGot * K2 + 1);		// +1 to avoid /0
					p += (days * K1a) / K1b + K3;

					if(p > 0)
						btPtr->probability = p;
					else
						btPtr->probability = 0;
				}
			case Mob_Riverine:
			case Mob_RiverineIronClad:
				{
					const int K4 = 70;			// Days
					const int K2 = 30;			// was 3
					const int K1a = 9;			// 9/14
					const int K1b = 14;
					const int K3 = -8;

					UBYTE days = buildCosts[mType].days;

					int p = (K4 - days) * riverNavalNeeded;
					p /= (long)(riverNavalGot * K2 + 1);
					p += (days * K1a) / K1b + K3;

					if(p > 0)
						btPtr->probability = p;
					else
						btPtr->probability = 0;
				}
				break;
			case Mob_SupplyWagon:
				{
					// int p = 200 - (200 * f->supplies) / (MaxSupply - 50);
					if(f->supplies < (MaxSupply - 50))
						btPtr->probability = 250 - ((250 - 50) * f->supplies) / MaxSupply;
						// btPtr->probability = 150 - ((150 - 50) * f->supplies) / MaxSupply;
				}
				break;
			case Mob_Fortification:
				btPtr->probability = (f->aiDefend * (100 - 20)) / 256 + 20;
				break;
			case Mob_SupplyDepot:
				btPtr->probability = 200 - (200 * f->supplies) / MaxSupply;
				break;
			case Mob_Recruitment:
				btPtr->probability = (cvNeeded * 50) / cvGot;
				break;
			case Mob_Training:
				btPtr->probability = (cvNeeded * 30) / cvGot;
				break;
			case Mob_RailHead:
				btPtr->probability = 50;
				break;
			case Mob_RailEngineer:
				btPtr->probability = 50;
				break;
			case Mob_Hospital:
				btPtr->probability = 50;
				break;
			case Mob_POW:
				btPtr->probability = 30;
				break;
			case Mob_Blockade:
				btPtr->probability = 50;
				break;
#ifdef DEBUG
			default:
				throw GeneralError("Missing case (%d) in " __FILE__ ", Line %d", mType, __LINE__);
#endif
			}

			totalProb += btPtr->probability;

			if((btPtr->probability != 0) && mList.canIbuild(f, MobiliseType(mType)))
				canBuildNow = True;
		}
		else
			btPtr->probability = 0;
	} // for
}



void BuildInfo::pick()
{
	MobiliseList& mList = campaign->world->mobList;
	Boolean finish = False;
	while(!finish)
	{
		int pick = game->gameRand(totalProb * 2);
#ifdef DEBUG
		aiResLog.printf("Picked %d", pick);
#endif

		finish = True;

		int mType;
		BuildTable* btPtr;
		for(mType = 0, btPtr = table; mType < Mob_HowMany; mType++, btPtr++)
		{
			pick -= btPtr->probability;
			if(pick < 0)
			{
#ifdef DEBUG
				aiResLog.printf("Attempting to build %s",
					language(buildCosts[mType].description));
#endif
				if(mList.canIbuild(f, MobiliseType(mType)))
				{
#ifdef DEBUG
					aiResLog.printf("Building it...");
#endif
					mList.startBuild(MobiliseType(mType), f, 1);
					finish = False;		// We're rich, lets try something else
				}
#ifdef DEBUG
				else
				{
					aiResLog.printf("Can't afford it");
				}
#endif
				break;
			}
		}

	}
}

/*-----------------------------------------------------------------
 * Build facilities, mobilise regiments
 */

void CampaignAI::aic_Resources()
{
#ifdef DEBUG
	aiLog.printf("aic_Resources()... see ai_res.log");
	aiResLog.setTime(0);		// Disable Time display
	aiResLog.printf("");
	aiResLog.printf("Processing resources for %s", sideStr[aiSide]);
#endif

	/*
	 * Add up CV needed and CV got
	 */

	BuildInfo info;

	info.cvNeeded = events.addEnemyCV();

	info.cvGot = 0;
	UnitIter iter(campaign->world->ob.getPresident(aiSide), False, True, Rank_Army, Rank_Brigade);
	while(iter.ok())
	{
		Unit* unit = iter.get();

		info.cvGot += calc_UnitCV(unit, True);
	}
	if(info.cvGot <= 0)
		info.cvGot = 1;		// Ensure that it is not zero!

	/*
	 * Count our boats...
	 */

	info.seaNavalGot = seaNavalGot;
	info.riverNavalGot = riverNavalGot;
	info.seaNavalNeeded = seaNavalNeeded;
	info.riverNavalNeeded = riverNavalNeeded;

#ifdef DEBUG
	aiResLog.printf("neededCV=%ld, gotCV=%ld", info.cvNeeded, info.cvGot);
	aiResLog.printf("seaNavalNeeded=%ld, seeNavalGot=%ld",
		info.seaNavalNeeded, info.seaNavalGot);
	aiResLog.printf("riverNavalNeeded=%ld, riverNavalGot=%ld",
		info.riverNavalNeeded, info.riverNavalGot);
	aiResLog.printf("");
#endif

	FacilityList& fList = campaign->world->facilities;
	MobiliseList& mList = campaign->world->mobList;

	/*
	 * Process each facility on our side
	 */


	for(int fIndex = 0; fIndex < fList.entries(); fIndex++)
	{
		Facility* f = fList[fIndex];

		if(f->side == aiSide)
		{
			info.f = f;

#ifdef DEBUG_ALL
			ResourceSet* localRsc = f->getLocalResources();
			ResourceSet* externRsc = f->getGlobalResources();

			MemPtr<char> logBuffer(200);
			sprintf(logBuffer, "%-20s: L=", f->getNameNotNull());
			localRsc->print(logBuffer + strlen(logBuffer));
			strcat(logBuffer, ", G=");
			externRsc->print(logBuffer + strlen(logBuffer));

			aiResLog.printf("%s", logBuffer);
#endif

			info.make();

			/*
			 * If anything can be built, then pick something
			 */

			if(info.canBuildNow)
			{

#ifdef DEBUG
				info.logTable();
#endif	// DEBUG

				info.pick();
			}
#ifdef DEBUG_ALL
			else
			{
				aiResLog.printf("-- Can not build anything");
			}
#endif

		}
	}	// for(facilityList)
#ifdef DEBUG
	aiResLog.printf("--------");
	aiResLog.printf("Finished");
	aiResLog.printf("--------");
	aiResLog.close();
#endif
}


