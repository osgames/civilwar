/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Miscellaneous functions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1994/07/11  14:26:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/06/06  13:17:39  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdlib.h>
#include <string.h>
#include "language.h"

const char* makeNths(int number)
{
	if(isLanguage("NL"))
	{
		if( (number == 1) || (number == 8) || (number >= 20))
			return "ste";
		else
			return "de";
	}
	else	// Assume English
	{
		if((number < 20) && (number >= 10))	// 10th..19th
			number = 0;
		else
			number %= 10;

		switch(number)
		{
		case 1:
			return "st";
		case 2:
			return "nd";
		case 3:
			return "rd";
		default:
			return "th";
		}
	}
}

static char* romanNumbers[] = {
	"0",
	"I",
	"II",
	"III",
	"IV",
	"V",
	"VI",
	"VII",
	"VIII",
	"IX",
	"X",
	"XI",
	"XII",
	"XIII",
	"XIV",
	"XV",
	"XVI",
	"XVII",
	"XVIII",
	"XIX",
	"XX"
};

const char* romanNumber(int n)
{
	if(n > 20)
	{
		static char buffer[10];

		itoa(n, buffer, 10);
		strcat(buffer, makeNths(n));
		return buffer;
	}
	else
		return romanNumbers[n];
}


