/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	CAL Pickup choices
 *
 * DIsplays popup box with choices of what to do and
 * puts CAL in correct mode
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.1  1994/07/11  14:26:46  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "calmain.h"
#include "calmulti.h"
#include "generals.h"
#include "dialogue.h"
#include "language.h"
#include "ob.h"
#include "ed_troop.h"
#include "game.h"

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
#include "unit3d.h"
#include "batldata.h"
#endif

enum CAL_Order {
	CALO_None,

	CALO_Rejoin,
	CALO_Reattach,
	CALO_ReattachAll,
	CALO_Find,
	CALO_Order,
	CALO_TransferCommander,
	CALO_TransferUnits,

#ifdef DEBUG
	CALO_Kill,
#endif

#if !defined(FOURMEG) && !defined(COMPUSA_DEMO)
	CALO_EditUnit,
	CALO_EditGeneral,
	CALO_NewGeneral,
#endif

	CALO_Cancel
};


void CAL::doPickup(Unit* unit, General* general, PointerIndex icon, const Point& p)
{
	MenuChoice choices[CALO_Cancel+7];		// Maximum choices
	MenuChoice* nextChoice = choices;

	/*
	 * Display unit/general name
	 */

	if(unit && unit->getName(True))
		*nextChoice++ = MenuChoice(unit->getName(True), 0);
	if(general && general->getName())
		*nextChoice++ = MenuChoice(general->getName(), 0);
	*nextChoice++ = MenuChoice("-", 0);								// Divider

	/*
	 * Add commands if applicable
	 */

	Boolean needDivide = False;

	// These commands only applicable to units

	if(unit)
	{
		updateTops(unit);		// Force Unit onto display
		setRedraw();

		/*
		 * Rejoin/Reattach block
		 */

		// Rejoin if it is a corps or below and independant

		if(unit->isDetached() &&
			(unit->getRank() >= Rank_Corps) &&
			unitIsValid(unit->superior))
		{
			*nextChoice++ = MenuChoice( language( lge_rejoinCommand ), CALO_Rejoin);
			needDivide = True;
		}

		// Reattach if any unattached children

		if(unit->hasUnattached(False))
		{
			*nextChoice++ = MenuChoice(language( lge_ReattachUnits), CALO_Reattach);
			needDivide = True;
		}

		if(unit->hasUnattached(True))
		{
			*nextChoice++ = MenuChoice(language( lge_ReattachAllUnits), CALO_ReattachAll);
			needDivide = True;
		}

		/*
		 * Find/Order block
		 */

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
		if(campaignMode || (unit->battleInfo && !unit->battleInfo->isDead && onBattleField(unit->battleInfo->where)))
		{
#endif
			if(unit->superior && (unit->location != Location(0,0)))	// Not unallocated!
			{
				if(needDivide)
					*nextChoice++ = MenuChoice("-", 0);
				*nextChoice++ = MenuChoice( language( lge_FindonMap), CALO_Find);
				needDivide = True;

				if(!campaignMode || (unit->moveMode != MoveMode_Transport) || (unit->getCampaignAttachMode() == Detached))
				{
					if(unit->getRank() <= Rank_Brigade)
						*nextChoice++ = MenuChoice( language( lge_IssueOrders), CALO_Order);
				}
			}
#if !defined(CAMPEDIT) && !defined(TESTCAMP)
		}
#endif

	}

	/*
	 * Transfer Block
	 */

	if(needDivide)
	{
		*nextChoice++ = MenuChoice("-", 0);
		needDivide = False;
	}

#if !defined(TESTBATTLE)
	if(campaignMode)
	{
		if(general != 0)
		{
			*nextChoice++ = MenuChoice( language( lge_TransferGeneral), CALO_TransferCommander);
			needDivide = True;
		}

		if(unit != 0)
		{
			if(unit->moveMode != MoveMode_Transport)
			{
				if(unit->getRank() >= Rank_Corps)
				{
					*nextChoice++ = MenuChoice( language( lge_TransferUnits ), CALO_TransferUnits);
					needDivide = True;
				}
			}
		}
	}
#endif	// TESTBATTLE

	if(needDivide)
		*nextChoice++ = MenuChoice("-", 0);

	/*
	 * New Edit feature!
	 *
	 * Only if single player...
	 */

#if !defined(FOURMEG) && !defined(COMPUSA_DEMO)
	if(!game->isRemoteActive())
	{
		if(unit)
		{
			if(unit->getRank() == Rank_Army)
			{
				*nextChoice++ = MenuChoice(language(lge_EditArmyName), CALO_EditUnit);
				needDivide = True;
			}
			else if(unit->getRank() == Rank_Regiment)
			{
				*nextChoice++ = MenuChoice(language(lge_EditRegiment), CALO_EditUnit);
				needDivide = True;
			}
		}
		if(general)
		{
			*nextChoice++ = MenuChoice(language(lge_EditGeneral), CALO_EditGeneral);
			needDivide = True;
		}
		if(!unit || (unit->getRank() < Rank_Regiment))
		{
			*nextChoice++ = MenuChoice(language(lge_NewGeneral), CALO_NewGeneral);
			needDivide = True;
		}
	}
#endif

#ifdef DEBUG
	if(needDivide)
		*nextChoice++ = MenuChoice("-", 0);

	if(unit && (unit->moveMode != MoveMode_Transport))
	{
		*nextChoice++ = MenuChoice("Kill Units", CALO_Kill);
		needDivide = True;
	}
#endif

	/*
	 * Always add Cancel option
	 */

	if(needDivide)
		*nextChoice++ = MenuChoice("-", 0);

	*nextChoice++ = MenuChoice( language( lge_cancel ), CALO_Cancel);
	*nextChoice = MenuChoice(0, 0);

	switch(menuSelect(this, choices, p))
	{
	case CALO_Rejoin:
		txCALCal(ob, Connect_CAL_Rejoin, unit);

		break;

	case CALO_Reattach:
		txCALCal(ob, Connect_CAL_Reattach, unit);
		break;

	case CALO_ReattachAll:
		txCALCal(ob, Connect_CAL_ReattachAll, unit);
		break;

	case CALO_Find:
		setCurrentUnit(unit);
		setFinish(CAL_Find);
		break;

	case CALO_Order:
		setCurrentUnit(unit);
		setFinish(CAL_GiveOrder);
		break;

#if !defined(TESTBATTLE)
	case CALO_TransferCommander:
		if(general->allocation)
		{
	 		txCALTransferGeneral(ob, general, 0);
		}
		else
		{
			dragRecursive = False;
			pickedUnit    = unit;
			pickedGeneral = general;
			setDropMode(icon);
			setRedraw();
		}
		break;

	case CALO_TransferUnits:
		if( (unit->getRank() == Rank_Regiment) && unit->superior)
		{
			txCALTransferUnit(ob, unit, 0);
		}
		else
		{
			dragRecursive = True;
			pickedUnit    = unit;
			pickedGeneral = general;
			setDropMode(icon);
			setRedraw();
		}
		break;
#endif	// TESTBATTLE

#ifdef DEBUG
	case CALO_Kill:
		currentUnit = unit->superior;
		ob->deleteUnit(unit);
		updateTops(currentUnit);
		setRedraw();
		break;
#endif

#if !defined(FOURMEG) && !defined(COMPUSA_DEMO)
	case CALO_EditUnit:
		editUnit(unit);
		setRedraw();
		break;

	case CALO_EditGeneral:
		editGeneral(general);
		setRedraw();
		break;

	case CALO_NewGeneral:
		{
			General* g = new General("Unnamed", top->getSide());

			g->efficiency = game->miscRand(MaxAttribute);
			g->ability    = game->miscRand(MaxAttribute);
			g->aggression = game->miscRand(MaxAttribute);
			g->experience = game->miscRand(MaxAttribute);

			ob->assignGeneral(g, unit);
			editGeneral(g);
			setRedraw();
		}
		break;
#endif

	case CALO_Cancel:
		break;
	}
}
