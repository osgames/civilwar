/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Sound and Music Interface to DSMI
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:21:53  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
// Revision 1.1  1994/08/22  16:02:09  Steven_Green
// Initial revision
//
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "sound.h"
#include "nosound.h"
#include "dialogue.h"
#include "menudata.h"
#include "text.h"
#include "colours.h"
#include "system.h"
#include "screen.h"
#include "fstream.h"
#include "cd_open.h"
#include "language.h"
#include "sndconfi.h"
#include "memptr.h"

/*
 * Global sound class
 */

Sound sound;

#if defined(SOUND_DSMI)

/*
 * Initialise DSMI
 */

void Sound::init()
{
	if(!initialised)
	{
		getConfiguration();

		/*
		 * Set up CDI
		 *
		 * GUS is special case
		 */

		if(scard.ID == ID_GUS)
		{
			// Initialize GUS player
			scard.extraField[2] = 1;    // Use DMA downloading
			scard.extraField[3] = 0;    // Don't use GUS timer
			gusInit(&scard);

			// Initialize GUS heap manager
			gushmInit();

			// Init CDI
			cdiInit();

			// Register GUS into CDI
			// cdiRegister(&CDI_GUS,0,SoundChannelCount-1);
			cdiRegister(&CDI_GUS,0,31);

			// Add GUS event playing engine into Timer Service
			gusInterruptTag = tsAddRoutine((void(*)(void))gusInterrupt,GUS_TIMER);

		}
		else if(scard.ID == ID_NOSOUND)
		{
			// Initialize Channel Distributor

			cdiInit();

			// Register MCP into CDI

			// cdiRegister(&CDI_NOSOUND,0,SoundChannelCount-1);
			cdiRegister(&CDI_NOSOUND,0,31);
		}
		else
		{
			ushort options = mcp.options;
   		memset(&mcp, 0, sizeof(mcp));
			mcp.options = options;

			/*
		 	 * Get appropriate routine
		 	 */

			SDI_INIT sdi = 0;

   		switch(scard.ID)
   		{
      		case ID_SB :
            		sdi = &SDI_SB;
            		break;
      		case ID_SBPRO :
            		sdi = SDI_SBPro;
            		break;
      		case ID_PAS :
      		case ID_PASPLUS :
      		case ID_PAS16 :
            		sdi = SDI_PAS;
            		break;
      		case ID_SB16 :
            		sdi = SDI_SB16;
            		break;
#if 0
				case ID_ARIA :
            		sdi = SDI_ARIA;
            		break;
#endif
				case ID_WSS :
            		sdi = SDI_WSS;
            		break;
   		}

			mcpInitSoundDevice(sdi, &scard);



			// Calculate mixing buffer size

			// size_t bufsize = ((2800 * (scard.sampleSize << scard.stereo)) * rate) / 22000;
         size_t bufsize = ((2048 * (int)scard.sampleSize << (int)scard.stereo) * (long)rate) / 44100l;

			size_t a;
			if(mcp.options & MCP_QUALITY )
			{
   			// 8-bit or 16-bit sound card?
   			if(scard.sampleSize == 1)
					a = MCP_TABLESIZE + MCP_QUALITYSIZE;
   			else
					a = MCP_TABLESIZE16 + MCP_QUALITYSIZE16;
			}
			else
				a = MCP_TABLESIZE;

			/*
			 * Make sure no bigger than 64K
			 */

			a += bufsize + 16;		// The 16 rounds it up to a segment
			if(a > 65500)
			{
				bufsize -= a - 65500;
				a = 65500;
			}

			// Allocate volume table + mixing buffer

			mcp.bufferBase   = (D_DOSAlloc(a) & 0xFFFF) << 4;
			mcp.bufferPhysical = mcp.bufferBase;
			mcp.bufferSeg    = D_MakeSelector(mcp.bufferBase, a);

			// Bodge... reduce bufsize so that buffer is really much bigger than needed
			// bufsize /= 2;

			mcp.bufferSize   = bufsize;
			mcp.reqSize 	  = bufsize / 2;
			mcp.samplingRate = rate;

			// Initialize Multi Channel Player

			if(mcpInit(&mcp))
			{
#ifndef INTROPLAYER
				dialAlert(0, language(lge_badMCP), language(lge_cancel));
#endif
				return;
			}

			// Initialize Channel Distributor

			cdiInit();

			// Register MCP into CDI

			// cdiRegister(&CDI_MCP,0,SoundChannelCount-1);
			cdiRegister(&CDI_MCP,0,31);
		}

   	// Try to initialize AMP

		if(ampInit(0))
		{
#ifndef INTROPLAYER
			dialAlert(0, language(lge_badAMP), language(lge_cancel));
#endif
			return;
		}

		// Hook AMP player routine into Timer Service
   
		ampInterruptTag = tsAddRoutine( (void(*)()) ampInterrupt, AMP_TIMER);

   	if(scard.ID == ID_GUS )
			gusStartVoice();
		else if(scard.ID != ID_NOSOUND)
			mcpStartVoice(); // Start voice output

		/*
		 * Make my own volume table for 200%
		 */

    	for(int t = 0; t < 32; t++ )
      	volTable[t] = t*500/100+1;

		cdiSetupChannels(0, 32, volTable);
		cdiSetMasterVolume(0, 64);

		initialised = True;
	}
}

/*
 * Close down DSMI
 */

void Sound::closeDown()
{
	initialised = False;

	if(scard.ID == ID_GUS)
		tsRemoveRoutine(gusInterruptTag);

	tsRemoveRoutine(ampInterruptTag);

	ampClose();
	cdiClose();
	gusClose();
	mcpClose();
	if(mcp.bufferBase)
	{
		D_DOSFree(mcp.bufferBase);
		mcp.bufferBase = 0;
		mcp.bufferPhysical = 0;
	}
	if(mcp.bufferSeg)
	{
		D_FreeSelector(mcp.bufferSeg);
		mcp.bufferSeg = 0;
	}
}


/*======================================================
 * Song handling
 */

MODULE* Sound::loadMusic(const char* name)
{
	return ampLoadAMF( getPathAndCheck(name), 0);
}

void Sound::playMusic(MODULE* song, Boolean loop)
{
	if(song->channelCount > SoundChannelCount)
		cdiSetupChannels(0, song->channelCount, volTable);

   ampPlayModule(song, loop ? PM_LOOP : 0);
}

void Sound::stopMusic(MODULE* song)
{
	ampStopModule();
}

void Sound::unloadSong(MODULE* song)
{
	ampFreeModule(song);
}

/*
 * Read Sound Configuration File
 */

char* soundTokens[] = {
	sc_ID,
	sc_version,
	sc_name,
	sc_ioPort,
	sc_dma,
	sc_irq,
	sc_minRate,
	sc_maxRate,
	sc_stereo,
	sc_mixer,
	sc_sampSize,
	sc_extra,
	sc_rate,
	sc_quality,
	0
};

enum SoundToken {
	SC_ID,
	SC_version,
	SC_name,
	SC_ioPort,
	SC_dma,
	SC_irq,
	SC_minRate,
	SC_maxRate,
	SC_stereo,
	SC_mixer,
	SC_sampSize,
	SC_extra,
	SC_rate,
	SC_quality,

	SC_Illegal = -1,
};

SoundToken getSoundToken(char* s)
{
	char** sPtr = soundTokens;
	int i = 0;

	while(*sPtr)
	{
		if(!strncmp(s, *sPtr, strlen(*sPtr)))
			return SoundToken(i);

		i++;
		sPtr++;
	}

	return SC_Illegal;
}

Boolean Sound::getConfiguration()
{
	Boolean retVal = False;

	/*
	 * Default settings as per NO_SOUND
	 */

	memset(&scard, 0, sizeof(SOUNDCARD));
	scard.ID = ID_NOSOUND;
	strcpy(scard.name, language(lge_None));

	FILE* fp = fopen(sc_fileName, "r");
	if(fp)
	{
		MemPtr<char> line(80);		// Space for a line of text
		
		fgets(line, 79, fp);

		if(strncmp(line, sc_title, strlen(sc_title)))
			goto error;

		while(!feof(fp))
		{
			if(fgets(line, 79, fp))
			{
				// Remove trailing white spaces

				char* s = line;
				char* lastSpace = s;
				while(*s)
				{
					if(!isspace(*s++))
						lastSpace = s;
				}
				*lastSpace = 0;

				s = strchr(line, '=');

				if(s)
				{
					while(*s && isspace(*s) || (*s == '='))
						s++;

					switch(getSoundToken(line))
					{
					case SC_ID:
						scard.ID = atoi(s);
						break;
					case SC_version:
						{
							int v1;
							int v2;

							sscanf(s, "%d.%02d", &v1, &v2);
							scard.version = (v1 << 8) + v2;
						}
						break;

					case SC_name:
						strncpy(scard.name, s, sizeof(scard.name));
						break;

					case SC_ioPort:
						{
							int h1;

							sscanf(s, "%04x", &h1);
							scard.ioPort = h1;
						}
						break;

					case SC_dma:
						scard.dmaChannel = atoi(s);
						break;

					case SC_irq:
						scard.dmaIRQ = atoi(s);
						break;

					case SC_minRate:
						scard.minRate = atoi(s);
						break;

					case SC_maxRate:
						scard.maxRate = atoi(s);
						break;

					case SC_stereo:
						scard.stereo = atoi(s);
						break;

					case SC_mixer:
						scard.mixer = atoi(s);
						break;

					case SC_sampSize:
						scard.sampleSize = atoi(s);
						break;

					case SC_extra:
						{
							for(int i = 0; i < 8; i++)
							{
								scard.extraField[i] = atoi(s);
								while(*s && isdigit(*s))
									s++;
								while(*s && isspace(*s) || (*s == ','))
									s++;
							}
						}
						break;

					case SC_rate:
						sound.rate = atoi(s);
						break;

					case SC_quality:
						sound.mcp.options = atoi(s);
						break;

					case SC_Illegal:
#ifdef DEBUG
						{
							MemPtr<char> buf(200);

							sprintf(buf, "Illegal Line in %s\r\r%s",
								sc_fileName,
								line);
							dialAlert(0, buf, language(lge_OK));
						}
#endif
						break;
					}
				}
			}
		}

		retVal = !ferror(fp);
	error:
		fclose(fp);
	}

	return retVal;

}

#endif   // defined(SOUND_DSMI)
