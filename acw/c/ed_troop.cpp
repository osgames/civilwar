/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Troop Editor
 *
 * Based on calpick.cpp
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>

#include "ed_troop.h"

#ifdef CAMPEDIT
#include "camped.h"
#endif

#ifdef BATEDIT
#include "bated.h"
#endif


#include "cal.h"
#include "calmain.h"
#include "calicon.h"
#include "generals.h"
#include "tables.h"

#include "dialogue.h"

#include "text.h"
#include "system.h"
#include "screen.h"
#include "mouselib.h"
#include "mapwind.h"

#if defined(BATEDIT) || defined(TESTBATTLE)
#include "ob.h"
#include "unit3d.h"
#else
#include "campwld.h"
#endif

#include "strutil.h"

/*=================================================================
 * Troop Mode
 */

#ifdef CAMPEDIT

static MenuChoice unitChoices[] = {
	MenuChoice(0, 					0 			),						// User fills in this
	MenuChoice("-", 				0 			),
	MenuChoice("Move", 			DoMove 	),
	MenuChoice("Set Order",		DoOrder	),
	// MenuChoice("Edit Unit", 	DoEdit 	),
	MenuChoice("Occupy Facility", DoOccupy),
	MenuChoice("Siege Facility", DoSiege),
	MenuChoice("CAL", 			DoCal 	),
	MenuChoice("-", 				0 			),
	MenuChoice("Cancel", 		-1 		),
	MenuChoice(0,					0 			)
};

#endif

#if defined(CAMPEDIT) || defined(BATEDIT)

void Unit::showInfo(Region* r)
{
	control->setupInfo();

	TextWindow& textWind = *control->textWin;
	textWind.setFont(Font_Heading);
	textWind.setColours(Black);

	textWind.setHCentre(True);

	textWind.setPosition(Point(4,4));
	textWind.draw(rankStr(getRank()));
	textWind.newLine();
	textWind.draw(getName(False));
	textWind.setHCentre(False);
	textWind.newLine();
	textWind.moveDown(4);

#ifndef BATEDIT
	if(occupying)
	{
		static char* siegeStr[] = {
			"None",
			"Occupying",
			"Besieging",
			"Standing"
		};

		textWind.wprintf("%s %s\r", siegeStr[siegeAction], occupying->getName());
	}
#endif

	if(general)
	{
		textWind.wprintf("General: %s\r", general->getName());
		textWind.wprintf("Rank: %s\r", rankStr(general->rank));
		textWind.wprintf("Efficiency: %d\r", (int)general->efficiency);
		textWind.wprintf("Ability: %d\r", (int)general->ability);
		textWind.wprintf("Aggression %d\r", (int)general->aggression);
		textWind.wprintf("Experience %d\r", (int)general->experience);
	}
	else
		textWind.draw("General: None\r");

	textWind.moveDown(4);

	UnitInfo info(False, False);

	getInfo(info);
	textWind.wprintf("Strength: %ld\r", (long) info.strength);
	textWind.wprintf("Stragglers: %ld\r", (long) info.stragglers);
	textWind.wprintf("Casualties: %ld\r", (long) info.casualties);
	textWind.wprintf("Fatigue: %d\r", (int) info.fatigue);
	textWind.wprintf("Morale: %d\r", (int) info.morale);
	textWind.wprintf("Supply: %d\r", (int) info.supply);
	textWind.wprintf("Experience: %d\r", (int) info.experience);

	textWind.wprintf("Infantry: %ld\r", (long) info.infantryStrength);
	textWind.wprintf("Cavalry: %ld\r", (long) info.cavalryStrength);
	textWind.wprintf("Artillery: %ld\r", (long) info.artilleryStrength);

#ifdef BATEDIT
	UnitBattle* ub = battleInfo;

	textWind.wprintf("\rOrder: %s\r", getOrderStr(ub->battleOrder));
	textWind.wprintf("Dest: %ld,%ld", ub->battleOrder.destination.x, ub->battleOrder.destination.y);
#endif
}
#endif	// defined(CAMPEDIT) || defined(BATEDIT)

/*
 * Regiment editor
 */

enum edRegimentId {
	ER_None,
	ER_Cancel,
	ER_OK,
	ER_Name,
	ER_BasicType,
	ER_SubType,
	ER_Strength,
	ER_Stragglers,
	ER_Casualties,
	ER_Fatigue,
	ER_Morale,
	ER_Supply,
	ER_Experience
};

#define MaxRegimentName 40

static char r_name[MaxRegimentName];
static char r_basicTypeStr[20];
static char r_subTypeStr[40];

static char r_strengthStr[6];
static char r_stragglerStr[6];
static char r_casualtyStr[6];
static char r_fatigueStr[4];
static char r_moraleStr[4];
static char r_supplyStr[4];
static char r_experienceStr[4];


DialItem edRegimentItems[] = {
	DialItem(Dial_Button, 		ER_None,	  		Rect(  0,  0,256,16), "Edit Regiment"),
	DialItem(Dial_Button, 		ER_None,	  		Rect(  0, 16, 40,16), "Name"),
	DialItem(Dial_TextInput,   ER_Name,			Rect( 40, 16,216,16), r_name, sizeof(r_name)),
	DialItem(Dial_Button,		ER_BasicType,	Rect(  0, 32,128,16), r_basicTypeStr, sizeof(r_basicTypeStr)),
	DialItem(Dial_Button,		ER_SubType,		Rect(128, 32,128,16), r_subTypeStr, sizeof(r_subTypeStr)),

	DialItem(Dial_Button,		ER_None,			Rect(  0, 48, 76,16), "Strength"),
	DialItem(Dial_NumberInput,	ER_Strength,	Rect( 76, 48, 48,16), r_strengthStr, sizeof(r_strengthStr)),
	DialItem(Dial_Button,		ER_None,			Rect(  0, 64, 76,16), "Stragglers"),
	DialItem(Dial_NumberInput,	ER_Stragglers,	Rect( 76, 64, 48,16), r_stragglerStr, sizeof(r_stragglerStr)),
	DialItem(Dial_Button,		ER_None,			Rect(  0, 80, 76,16), "Casualties"),
	DialItem(Dial_NumberInput,	ER_Casualties,	Rect( 76, 80, 48,16), r_casualtyStr, sizeof(r_casualtyStr)),


	DialItem(Dial_Button,		ER_None,			Rect(132, 48,100,16), "Fatigue"),
	DialItem(Dial_NumberInput,	ER_Fatigue,		Rect(232, 48, 24,16), r_fatigueStr, sizeof(r_fatigueStr)),
	DialItem(Dial_Button,		ER_None,			Rect(132, 64,100,16), "Morale"),
	DialItem(Dial_NumberInput,	ER_Morale,		Rect(232, 64, 24,16), r_moraleStr, sizeof(r_moraleStr)),
	DialItem(Dial_Button,		ER_None,			Rect(132, 80,100,16), "Supply"),
	DialItem(Dial_NumberInput,	ER_Supply,		Rect(232, 80, 24,16), r_supplyStr, sizeof(r_supplyStr)),
	DialItem(Dial_Button,		ER_None,			Rect(132, 96,100,16), "Experience"),
	DialItem(Dial_NumberInput,	ER_Experience,	Rect(232, 96, 24,16), r_experienceStr, sizeof(r_experienceStr)),
	
	DialItem(Dial_Button, 		ER_Cancel,  	Rect( 32,114, 64,16), "Cancel"),
	DialItem(Dial_Button, 		ER_OK,  			Rect(160,114, 64,16), "OK"),
	DialItem(Dial_End)
};

class RegimentEdit : public Dialogue {
	Regiment* regiment;
	RegimentType type;
public:
	RegimentEdit(Regiment* r);
	void doIcon(DialItem* item, Event* event, MenuData* d);
	void updateButtons();
};

RegimentEdit::RegimentEdit(Regiment* r)
{
	regiment = r;
	type = r->type;

	const char* regName = regiment->getRealName();
	if(regName)
		strcpy(r_name, regName);
	else
		strcpy(r_name, "New Regiment");

	sprintf(r_strengthStr, "%ld", regiment->strength);
	sprintf(r_stragglerStr, "%ld", regiment->stragglers);
	sprintf(r_casualtyStr, "%ld", regiment->casualties);

	sprintf(r_fatigueStr, "%d", (int)regiment->fatigue);
	sprintf(r_moraleStr, "%d", (int)regiment->morale);
	sprintf(r_supplyStr, "%d", (int)regiment->supply);
	sprintf(r_experienceStr, "%d", (int)regiment->experience);

	updateButtons();
	Dialogue::setup(edRegimentItems);
}

void RegimentEdit::updateButtons()
{
	static char* basicTypeStr[] = {
		"Infantry",
		"Cavalry",
		"Artillery",
		"Illegal"
	};

	static char* subTypeStr[3][4] = {
		{ "Regular", "Militia", "Sharpshooter", "Engineer" },
		{ "Regular", "Militia", "Reg. Mounted", "Mil. Mounted" },
		{ "12lb", "6lb", "Rifled", "Siege" },
	};

	strcpy(r_basicTypeStr, basicTypeStr[type.basicType]);

	if(type.subType >= 4)
		strcpy(r_subTypeStr, "Illegal");
	else
		strcpy(r_subTypeStr, subTypeStr[type.basicType][type.subType]);
}

void RegimentEdit::doIcon(DialItem* item, Event* event, MenuData* d)
{
	if(event->buttons)
	{
		switch(item->id)
		{
		case ER_Cancel:
			d->setFinish(ER_Cancel);
			break;
		case ER_OK:
			d->setFinish(ER_OK);
			if((regiment->getRealName() == 0) || strcmp(regiment->getRealName(), r_name) )
			{
				regiment->setName(copyString(r_name));
			}
			regiment->strength = atol(r_strengthStr);
			regiment->stragglers = atol(r_stragglerStr);
			regiment->casualties = atol(r_casualtyStr);

			regiment->fatigue = atoi(r_fatigueStr);
			regiment->morale = atoi(r_moraleStr);
			regiment->supply = atoi(r_supplyStr);
			regiment->experience = atoi(r_experienceStr);

			regiment->type = type;

#ifdef BATEDIT
			/*
			 * Update the dead flag
			 */

			{
				Unit* u = regiment;
				while(u)
				{
					if(u->battleInfo)
						u->battleInfo->isDead = (u->getStrength() == 0);
					u = u->superior;
				}
			}
#endif

			break;
		case ER_BasicType:
			if(type.basicType == Artillery)
				type.basicType = Infantry;
			else
				type.basicType++;
			updateButtons();
			d->setRedraw();
			break;
		case ER_SubType:
			type.subType = (type.subType + 1) & 3;
			updateButtons();
			d->setRedraw();
			break;
		}
	}

}

void editUnit(Unit* oldUnit)
{
	if(oldUnit->getRank() == Rank_Army)
	{	// Get its name
		Army* army = (Army*)oldUnit;

		char newName[40];

		const char* armyName = army->getRealName();
		if(armyName)
			strcpy(newName, armyName);
		else
			strcpy(newName, "New Army");

		if(dialInput(0, "Type in Army's Name:", "OK|Cancel", newName, sizeof(newName)) == 0)
		{
			army->setName(copyString(newName));
		}

	}
	else if(oldUnit->getRank() == Rank_Regiment)
	{	// Edit Regiment's values

		/*
		 * Basic Type
		 * Sub Type
		 * Strength
		 * stragglers
		 * casualties
		 * fatigue
		 * morale
		 * experience
		 * name
		 */

		RegimentEdit edit((Regiment*)oldUnit);
		if(edit.process() == ER_OK)
		{
#if defined(CAMPEDIT) || defined(BATEDIT)
			obChanged = True;
			changed = True;
#endif	// defined(CAMPEDIT) || defined(BATEDIT)
		}
	}

}

/*
 * General Editor
 *
 * Name
 * Rank
 * efficiency
 * ability
 * aggression
 * experience
 */

enum edGeneralId {
	EG_None,
	EG_Cancel,
	EG_OK,
	EG_Name,
	EG_Rank,
	EG_Efficiency,
	EG_Ability,
	EG_Aggression,
	EG_Experience
};

#define MaxGeneralName 40

static char g_name[MaxGeneralName];
static char g_efficiencyStr[4];
static char g_abilityStr[4];
static char g_aggressionStr[4];
static char g_experienceStr[4];
static char g_rankStr[40];

enum edGenEnum {
	EGE_Title,
	EGE_NameTitle,
	EGE_Name,
	EGE_Rank,
	EGE_EfficiencyTitle,
	EGE_EfficiencyStr,
	EGE_AbilityTitle,
	EGE_AbilityStr,
	EGE_AggressionTitle,
	EGE_AgressionStr,
	EGE_ExperienceTitle,
	EGE_ExperienceStr,
	EGE_Cancel,
	EGE_OK
};

DialItem edGeneralItems[] = {
	DialItem(Dial_Button, 		EG_None,	  		Rect(  0,  0,256,16), "Edit General"),
	DialItem(Dial_Button, 		EG_None,	  		Rect(  0, 16, 40,16), "Name"),
	DialItem(Dial_TextInput,   EG_Name,			Rect( 40, 16,216,16), g_name, sizeof(g_name)),
	DialItem(Dial_Button,		EG_Rank,			Rect(  0, 32,256,16), g_rankStr, sizeof(g_rankStr)),

	DialItem(Dial_Button,		EG_None,			Rect(  0, 48,100,16), "Efficiency"),
	DialItem(Dial_NumberInput,	EG_Efficiency,	Rect(100, 48,156,16), g_efficiencyStr, sizeof(g_efficiencyStr)),

	DialItem(Dial_Button,		EG_None,			Rect(  0, 64,100,16), "Ability"),
	DialItem(Dial_NumberInput,	EG_Ability,		Rect(100, 64,156,16), g_abilityStr, sizeof(g_abilityStr)),
	
	DialItem(Dial_Button,		EG_None,			Rect(  0, 80,100,16), "Aggression"),
	DialItem(Dial_NumberInput,	EG_Aggression,	Rect(100, 80,156,16), g_aggressionStr, sizeof(g_aggressionStr)),
	
	DialItem(Dial_Button,		EG_None,			Rect(  0, 96,100,16), "Experience"),
	DialItem(Dial_NumberInput,	EG_Experience,	Rect(100, 96,156,16), g_experienceStr, sizeof(g_experienceStr)),

	DialItem(Dial_Button, 		EG_Cancel,  	Rect( 32,114, 64,16), "Cancel"),
	DialItem(Dial_Button, 		EG_OK,  			Rect(160,114, 64,16), "OK"),
	DialItem(Dial_End)
};

class GeneralEdit : public Dialogue {
	General* general;
	Rank rank;
public:
	GeneralEdit(General* g);
	void doIcon(DialItem* item, Event* event, MenuData* d);
	void updateButtons();
};

GeneralEdit::GeneralEdit(General* g)
{
	general = g;

	const char* gName = general->getRealName();
	if(gName)
		strcpy(g_name, gName);
	else
		strcpy(g_name, "New General");
	sprintf(g_efficiencyStr, "%d", (int)general->efficiency);
	sprintf(g_abilityStr, "%d", (int)general->ability);
	sprintf(g_aggressionStr, "%d", (int)general->aggression);
	sprintf(g_experienceStr, "%d", (int)general->experience);
	rank = general->rank;
	updateButtons();
	Dialogue::setup(edGeneralItems);
}

void GeneralEdit::updateButtons()
{
	sprintf(g_rankStr, "Rank: %s", rankStr(rank));
}

void GeneralEdit::doIcon(DialItem* item, Event* event, MenuData* d)
{
	if(event->buttons)
	{
		switch(item->id)
		{
		case EG_Cancel:
			d->setFinish(EG_Cancel);
			break;
		case EG_OK:
			d->setFinish(EG_OK);
			if((general->getRealName() == 0) || strcmp(general->getRealName(), g_name))
			{
				general->setName(copyString(g_name));
			}
			general->efficiency = atoi(g_efficiencyStr);
			general->ability = atoi(g_abilityStr);
			general->aggression = atoi(g_aggressionStr);
			general->experience = atoi(g_experienceStr);
			general->rank = rank;
			break;
		case EG_Rank:
			if(rank == Rank_Army)
				rank = Rank_Brigade;
			else
				rank = promoteRank(rank);
			updateButtons();
			d->setRedraw();
			break;
		case EG_Name:
		case EG_Efficiency:
		case EG_Ability:
		case EG_Aggression:
		case EG_Experience:
			break;
		}
	}

}


void editGeneral(General* oldGeneral)
{
	GeneralEdit edit(oldGeneral);
	if(edit.process() == EG_OK)
	{
#if defined(CAMPEDIT) || defined(BATEDIT)
		obChanged = True;
		changed = True;
#endif	// defined(CAMPEDIT) || defined(BATEDIT)
	}
}

#ifdef CAMPEDIT
void CampaignEditControl::troopOverMap		(MapWindow* map, Event* event, MenuData* d, const Location* l)
{
	if(trackMode == Tracking)
	{
		trackFor(map, event, OT_Unit);

		if(event->buttons)
		{
			DoWhat what;
			Unit* unit = 0;

			if(currentObject)
			{
				unit = (Unit*) currentObject;

				if(event->buttons & Mouse::LeftButton)
				{

					unitChoices[0].text = unit->getName(True);

					what = DoWhat(menuSelect(0, unitChoices, machine.mouse->getPosition()));
				}
				else
					what = DoCal;
			}
			else
				what = DoNothing;


			switch(what)
			{
			case DoEdit:
				editUnit(unit);
				break;

			case DoCal:
			{
				int giveOrders;
				Unit* newUnit = doCampaignCAL(&world->ob, unit, &giveOrders, 0);
				setUpdate();
				if(newUnit)
				{
					if(newUnit != unit)
					{
						setCurrentObject(newUnit);
						map->setStyle(MapWindow::Zoomed, newUnit->location);
					}

					if(giveOrders)
						what = DoMove;
				}
				break;
			}

			case DoMove:
				trackMode = Moving;
				break;

			case DoOrder:
			{
				static MenuChoice orderChoice[] = {
					MenuChoice("Advance",				1+ORDER_Advance),
					MenuChoice("Cautious Advance",	1+ORDER_CautiousAdvance),
					MenuChoice("Attack",					1+ORDER_Attack),
					MenuChoice("Stand",					1+ORDER_Stand),
					MenuChoice("Hold",					1+ORDER_Hold),
					MenuChoice("Defend",					1+ORDER_Defend),
					MenuChoice("Withdraw",				1+ORDER_Withdraw),
					MenuChoice("Retreat",				1+ORDER_Retreat),
					MenuChoice("Hasty Retreat",		1+ORDER_HastyRetreat),
					MenuChoice("-", 						0),
					MenuChoice("Cancel", 				-1),
					MenuChoice(0,							0)
				};


				int order = menuSelect(0, orderChoice, machine.mouse->getPosition());

				if(order > 0)
				{
					orderMode = (OrderMode) (order - 1);

					if(getBasicOrder(orderMode) == ORDERMODE_Hold)
					{
						unit->realOrders = Order(orderMode, ORDER_Land, unit->location);
						unit->givenOrders = unit->realOrders;
					}
					else
					{
						trackMode = GetDestination;
						hint("Click on Destination for %s order", getOrderStr(orderMode));
					}
				}
				break;
			}

			case DoSiege:
				edittingUnit = unit;
				trackMode = GetSiege;
				hint("Select Facility to Siege");
				break;

			case DoOccupy:
				edittingUnit = unit;
				trackMode = GetOccupy;
				hint("Select Facility to Occupy");
				break;
			}
		}
	}
	else if(trackMode == Moving)
	{
		if(event->buttons)
		{
			hint(0);

			if(currentObject)
			{
		 		Unit* unit = (Unit*)currentObject;

				if(event->buttons & Mouse::LeftButton)
				{	// Move it!
					// unit->makeCampaignIndependant(True);
					// unit->makeCampaignReallyIndependant(True);
					unit->setCampaignAttachMode(Detached);
					unit->setLocation(*l);

					unit->realOrders = Order(ORDER_Stand, ORDER_Land, unit->location);
					unit->givenOrders = unit->realOrders;

					unit->occupying = 0;
					unit->siegeAction = SIEGE_None;

					unit->moveMode = MoveMode_Standing;

					obChanged = True;
					changed = True;
				}
			}

		 	trackMode = Tracking;
		 	objectIsNew = False;
		}
	}
	else if(trackMode == GetDestination)
	{
		if(event->buttons)
		{
			hint(0);

			if(currentObject && (event->buttons & Mouse::LeftButton))
			{
		 		Unit* unit = (Unit*)currentObject;
				if(event->buttons & Mouse::LeftButton)
				{
					unit->realOrders = Order(orderMode, ORDER_Land, *l);
					unit->givenOrders = unit->realOrders;

					unit->moveMode = MoveMode_Marching;

					unit->occupying = 0;
					unit->siegeAction = SIEGE_None;
				}
			}

		 	trackMode = Tracking;
		}
	}
	else if((trackMode == GetSiege) || (trackMode == GetOccupy))
	{
		trackFor(map, event, OT_Facility);

		if(event->buttons)
		{
			hint(0);

			if(currentObject && (event->buttons & Mouse::LeftButton))
			{
				Facility* f = (Facility*)currentObject;

				edittingUnit->location = f->location;
				edittingUnit->occupying = f;
				if(trackMode == GetSiege)
					edittingUnit->siegeAction = SIEGE_Besieging;
				else
				{
					edittingUnit->siegeAction = SIEGE_Occupying;
					f->side = edittingUnit->getSide();
				}
				edittingUnit->moveMode = MoveMode_Standing;
				edittingUnit->realOrders = Order(ORDER_Stand, ORDER_Land, edittingUnit->location);
			}

		 	trackMode = Tracking;
		}
	}
}

void CampaignEditControl::troopOptions		(const Point& p)
{
	int giveOrders;
	Unit* newUnit = doCampaignCAL(&world->ob, 0, &giveOrders, 0);
	setUpdate();
	if(newUnit)
	{
		setCurrentObject(newUnit);
		mapArea->setStyle(MapWindow::Zoomed, newUnit->location);

		if(giveOrders)
			trackMode = Moving;
	}
}

void CampaignEditControl::troopOffMap		()
{
}

#endif	// CAMPEDIT

#if defined(CAMPEDIT) || defined(BATEDIT)

/*
 * CAL interface and replacement routines
 */

enum CAL_Order {
	CALO_None,

	CALO_DeleteUnit,
	CALO_TransferUnit,
	CALO_FindUnit,
	CALO_EditUnit,
	CALO_CreateNewUnder,

	CALO_Rejoin,
	CALO_Break,

	CALO_Reattach,

	CALO_EditGeneral,
	CALO_TransferGeneral,
	CALO_DemoteGeneral,
	CALO_NewGeneral,
	CALO_DeleteGeneral,

	CALO_Cancel
};



void CAL::doPickup(Unit* unit, General* general, PointerIndex icon, const Point& p)
{
	MenuChoice choices[CALO_Cancel+5];		// Maximum choices
	MenuChoice* nextChoice = choices;

	/*
	 * Display unit/general name
	 */

	if(unit && unit->getName(True))
		*nextChoice++ = MenuChoice(unit->getName(True), 0);
	if(general && general->getName())
		*nextChoice++ = MenuChoice(general->getName(), 0);
	*nextChoice++ = MenuChoice("-", 0);								// Divider

	/*
	 * Add commands if applicable
	 */

	// These commands only applicable to units

	if(unit)
	{
		updateTops(unit);		// Force unit onto display

		*nextChoice++ = MenuChoice("Delete Unit", CALO_DeleteUnit);

		if(unit->getRank() < Rank_Regiment)
			*nextChoice++ = MenuChoice("Create New Child", CALO_CreateNewUnder);

		/*
		 * Rejoin/Reattach block
		 */

		// Rejoin if it is a corps or below and independant

		if(unit->getRank() >= Rank_Corps)
		{
			*nextChoice++ = MenuChoice("Transfer Units", CALO_TransferUnit);

			if(unit->getRank() < Rank_Regiment)
			{
				if(unit->isDetached() || unit->isReallyDetached())
					*nextChoice++ = MenuChoice("Rejoin Command", CALO_Rejoin);
				else
					*nextChoice++ = MenuChoice("Make Detached", CALO_Break);
			}
		}

		// Reattach if any unattached children

		if(unit->hasUnattached(True))
			*nextChoice++ = MenuChoice("Reattach Units", CALO_Reattach);

		/*
		 * Find/Order block
		 */

		if(unit->getRank() <= Rank_Brigade)
			*nextChoice++ = MenuChoice("Find", CALO_FindUnit);

		if((unit->getRank() == Rank_Army) || (unit->getRank() == Rank_Regiment))
			*nextChoice++ = MenuChoice("Edit Unit", CALO_EditUnit);

		*nextChoice++ = MenuChoice("-", 0);								// Divider
	}

	/*
	 * Transfer Block
	 */

	if(general)
	{
		*nextChoice++ = MenuChoice("Edit General", CALO_EditGeneral);
		*nextChoice++ = MenuChoice("Transfer General", CALO_TransferGeneral);

		if(unit)
			*nextChoice++ = MenuChoice("Demote General", CALO_DemoteGeneral);
	

		*nextChoice++ = MenuChoice("Delete General", CALO_DeleteGeneral);
	}

	if(!unit || (unit->getRank() < Rank_Regiment))
		*nextChoice++ = MenuChoice("Create General", CALO_NewGeneral);


	/*
	 * Always add Cancel option
	 */

	*nextChoice++ = MenuChoice("-", 0);
	*nextChoice++ = MenuChoice("Cancel", CALO_Cancel);
	*nextChoice = MenuChoice(0, 0);

	switch(menuSelect(this, choices, p))
	{
	case CALO_DeleteUnit:
	{
		Unit* newTop;
		newTop = unit->superior;

		if(unit->general)
			ob->assignGeneral(unit->general, 0);

		ob->deleteUnit(unit);
		updateTops(newTop);
		setRedraw();
		break;
	}
	case CALO_TransferUnit:
		dragRecursive = True;
		goto startTransfer;
		break;

	case CALO_FindUnit:
		setCurrentUnit(unit);
		setFinish(CAL_Find);
		break;

	case CALO_EditUnit:
		editUnit(unit);
		setRedraw();
		break;

	case CALO_Rejoin:
#ifdef CAMPEDIT
		unit->makeCampaignDetached(False);
		unit->makeCampaignReallyDetached(False);
#elif defined(BATEDIT)
		unit->makeBattleDetached(False);
		unit->makeBattleReallyDetached(False);
#endif
		setRedraw();
		break;

	case CALO_Break:
#ifdef CAMPEDIT
		unit->makeCampaignDetached(True);
		unit->makeCampaignReallyDetached(True);
#elif defined(BATEDIT)
		unit->makeBattleDetached(True);
		unit->makeBattleReallyDetached(True);
#endif
		setRedraw();
		break;

	case CALO_Reattach:
#ifdef CAMPEDIT
		unit->reattachCampaign(True);
#endif
#ifdef BATEDIT
		unit->reattachBattle(True);
#endif
		setRedraw();
		break;

	case CALO_EditGeneral:
		editGeneral(general);
		setRedraw();
		break;

	case CALO_TransferGeneral:
		dragRecursive = False;
		goto startTransfer;
		break;

	case CALO_DemoteGeneral:
		ob->assignGeneral(general, 0);
		setRedraw();
		break;

	case CALO_NewGeneral:
		{
			General* g = new General;

			g->setName(copyString("Unnamed"));
			g->side = top->getSide();

			ob->assignGeneral(g, unit);
			editGeneral(g);
			setRedraw();
		}
		break;

	case CALO_DeleteGeneral:
		if(unit)
			unit->general = 0;
		else
			ob->removeFreeGeneral(general);
		delete general;
		setRedraw();
		break;

	case CALO_CreateNewUnder:
		{
#ifdef BATEDIT
			Unit* newUnit;
			
			if(unit->battleInfo)
				newUnit = ob->makeNewUnder(unit, unit->battleInfo->where);
			else
				newUnit = ob->makeNewUnder(unit, unit->location);
			// newUnit->setupBattle();
#else
			Unit* newUnit = ob->makeNewUnder(unit, unit->location);
#endif

			updateTops(newUnit);
			setRedraw();
			editUnit(newUnit);
		}
		break;


	startTransfer:
		pickedUnit    = unit;
		pickedGeneral = general;
		setDropMode(icon);
		setRedraw();
		break;

	case CALO_Cancel:
		break;
	}
}



void CAL_NewIcon::createNewUnit(CAL* cal)
{
	Location l;

	if(superior->getRank() == Rank_President)
		l = Location(0,0);
	else
	{
#ifdef BATEDIT
	if(superior->battleInfo)
		l = superior->battleInfo->where;
	else
		l = superior->location;
#else
	l = superior->location;
#endif
	}

	Unit* newUnit = cal->ob->makeNewUnder(superior, l);

#ifdef BATEDIT
	// newUnit->setupBattle();
#endif


	editUnit(newUnit);

	cal->updateTops(newUnit);
	cal->setRedraw();
}

#endif	// defined(CAMPEDIT) || defined(BATEDIT)


void zeroRegimentAttributes(Unit* u)
{
	if(u->getRank() == Rank_Regiment)
	{
		Regiment* r = (Regiment*) u;
		r->stragglers = 0;
		r->casualties = 0;
		r->fatigue = 0;
		r->morale = MaxAttribute;
		r->supply = MaxAttribute;
		r->experience = 0;
	}

	u = u->child;
	while(u)
	{
		zeroRegimentAttributes(u);
		u = u->sister;
	}
}

void zeroRegimentAttributes(OrderBattle* ob)
{
	Unit* u = ob->getSides();

	while(u)
	{
		zeroRegimentAttributes(u);
		u = u->sister;
	}
}

/*
 * User prompt for options
 */

#if defined(CAMPEDIT) || defined(BATEDIT)

void CAL::options()
{
	static MenuChoice choices[] = {
		MenuChoice("CAL Options",						0),
		MenuChoice("-",									0),
#if 0
		MenuChoice("Check for Missing General",	1),
		MenuChoice("Check for Missing Children",	2),
#endif
		MenuChoice("Zero Regiment Attributes",		5),
		MenuChoice("-", 									0),
		MenuChoice("Edit USA Side", 					3),
		MenuChoice("Edit CSA Side", 					4),
		MenuChoice("Cancel", 						  -1),
		MenuChoice(0,										0)
	};

	switch(menuSelect(0, choices, machine.mouse->getPosition()))
	{
	case 1:	// Check missing generals
	case 2:	// Check missing children
		dialAlert(0, "Check functions\rUnwritten", "OK");
		break;
	case 3:	// Edit USA
		top = ob->getPresident(SIDE_USA);
		setCurrentUnit(top->child);
		setRedraw();
		break;

	case 4:	// Edit CSA
		top = ob->getPresident(SIDE_CSA);
		setCurrentUnit(top->child);
		setRedraw();
		break;

	case 5:	// Zero regiment attributes
		zeroRegimentAttributes(ob);
		break;
	}
}

#endif	// defined(CAMPEDIT) || defined(BATEDIT)
