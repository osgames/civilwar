/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Reading and writing of battlegame save game and historical battles
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "batfile.h"
#include "dfile.h"
#include "obfile.h"
#include "filesup.h"
#include "batldata.h"
#include "map3d.h"
#include "staticob.h"

static const UWORD battleFileVersion = 0x01;
static const UWORD battleHeaderVersion = 0;
static const UWORD battleMapVersion = 1;
static const UWORD staticObjectVersion = 0;

static const ChunkID BattleHeaderID = 'BATL';
static const ChunkID BattleMapID = 'BMAP';
static const ChunkID StaticObjectID = 'SOBS';

#if 0
class BattleFileRead : public ReadWithPopup {
public:
	BattleFileRead(const char* fileName) : ReadWithPopup(fileName, FT_Battle) { }
};
#endif

/*=============================================================
 * Battle Header Chunk
 */

struct D_BattleHeader {
	IWORD version;
	IBYTE battleType;		// Used to indicate whether stand-alone, etc

	D_GameTime gameTime;
};


void writeBattleHeader(ChunkFileWrite& file, BattleData* battle)
{
	file.startWriteChunk(BattleHeaderID);

	D_BattleHeader data;
	memset(&data, 0, sizeof(data));

	putWord(&data.version, battleHeaderVersion);
	putByte(&data.battleType, 0);		// ... To be written
	putGameTime(&data.gameTime, battle->gameTime);

	file.write(&data, sizeof(data));

	file.endWriteChunk();
}

void readBattleHeader(ReadWithPopup& file, BattleData* battle)
{
	if(file.startReadChunk(BattleHeaderID) == CE_OK)
	{
		D_BattleHeader data;
		file.read(&data, sizeof(data));
		getGameTime(&data.gameTime, battle->gameTime);
		battle->timeInfo = battle->gameTime;
	}
}

/*======================================================
 * Battle Maps (Height and terrain)
 */

struct D_BattleMap {
	IWORD version;
	IWORD gridCount;	// Size of maps

	// This is then followed by:
	//		Height8[gridCount+1]^2
	//		TerrainType[gridCount]^2
};

void writeBattleMap(ChunkFileWrite& file, BattleData* battle)
{
	file.startWriteChunk(BattleMapID);

	const Grid* grid = battle->grid->getGrid(Zoom_Min);

	D_BattleMap data;
	memset(&data, 0, sizeof(data));
	putWord(&data.version, battleMapVersion);
	putWord(&data.gridCount, grid->gridCount);
	file.write(&data, sizeof(data));
	
	file.write(grid->heights, grid->pointCount * grid->pointCount * sizeof(grid->heights[0]));
	file.write(grid->terrains, grid->gridCount * grid->gridCount * sizeof(grid->terrains[0]));

	file.endWriteChunk();
}


/*
 * Conversion of Terrain types in map version 0.0 to 0.1
 */

static TerrainType terrain0_0_To_0_1[] = {
	 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15,	// River
	16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,	// Road1
	32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47,	// Road2
	BuiltupTerrain,
	HeavyWoodTerrain,
	LightWoodTerrain,
	HeavyOrchardTerrain,
	LightOrchardTerrain,
	PastureTerrain,
	PloughedFieldTerrain,
	CornFieldTerrain,
	WheatFieldTerrain,
	HeavyOrchardTerrain,
	HeavyWoodTerrain,
	MeadowTerrain,
	CampSiteTerrain,
	WaterTerrain,
	WaterTerrain,
	MarshTerrain,
	RoughTerrain,
	RockyDesertTerrain,
	DesertTerrain
};

void readBattleMap(ReadWithPopup& file, BattleData* battle)
{
	// ... To be written

	if(file.startReadChunk(BattleMapID) == CE_OK)
	{
		D_BattleMap data;
		file.read(&data, sizeof(data));

		const Grid* grid = battle->grid->getGrid(Zoom_Min);

		UWORD gridSize = getWord(&data.gridCount);

		if(gridSize != grid->gridCount)
		{
			throw GeneralError("Grid size is wrong in battle file");
		}

		file.read(grid->heights, grid->pointCount * grid->pointCount * sizeof(grid->heights[0]));
		file.read(grid->terrains, grid->gridCount * grid->gridCount * sizeof(grid->terrains[0]));

		UWORD version = getWord(&data.version);

		/*
		 * Remap Terrain Types if old version of file
		 */

		if(version == 0)
		{
			TerrainType* tPtr = grid->terrains;
			int count = grid->gridCount * grid->gridCount;
			while(count--)
			{
				TerrainType t = *tPtr;

				if(t < NormalTerrainCount_0_0)
					t = terrain0_0_To_0_1[t];
				else
					t = PastureTerrain;

				*tPtr++ = t;
			}
		}

		battle->grid->makeZoomedGrids(battle->data3D);
	}
}

/*======================================================
 * Static Objects
 */

struct D_StaticObjectHeader {
	IWORD version;
	IWORD count;
};

struct D_StaticObjectData {
	IBYTE type;
	D_BattleLocation where;
	IBYTE what;
	IBYTE facing;
};

void writeStaticObjects(ChunkFileWrite& file, StaticObjectData* objects)
{
	StaticObjectIter iter(objects);

	BattleObject* ob;
	int howMany= 0;

	while( (ob = iter.next()) != 0)
		howMany++;

	/*
	 * Write Header
	 */

	file.startWriteChunk(StaticObjectID);

	D_StaticObjectHeader head;
	memset(&head, 0, sizeof(head));
	putWord(&head.count, howMany);
	putWord(&head.version, staticObjectVersion);
	file.write(&head, sizeof(head));

	iter.reset();

	while( (ob = iter.next()) != 0)
	{
		TreeObject* tob = (TreeObject*) ob;

		D_StaticObjectData data;
		memset(&data, 0, sizeof(data));

		ObjectSaveData saveData;
		tob->getSaveData(saveData);

		putByte(&data.type, saveData.type);
		putBattleLocation(&data.where, saveData.where);
		putByte(&data.what, saveData.what);
		putByte(&data.facing, saveData.facing);

		file.write(&data, sizeof(data));
	}

	file.endWriteChunk();
}

void readStaticObjects(ReadWithPopup& file, StaticObjectData* objects)
{
	objects->clear();

	if(file.fileVersion >= 0x01)
	{
		if(file.startReadChunk(StaticObjectID) == CE_OK)
		{
			D_StaticObjectHeader head;
			file.read(&head, sizeof(head));

			UWORD howMany = getWord(&head.count);

			while(howMany--)
			{
				D_StaticObjectData data;
				file.read(&data, sizeof(data));

				BattleLocation l;
				getBattleLocation(&data.where, l);
				StaticSpriteType n = StaticSpriteType(getByte(&data.what));
				Qangle facing = getByte(&data.facing);

				BattleObject* ob;

#if 0
				if(getByte(&data.type) == SO_Tree)
#endif
					ob = new TreeObject(n, l, facing);
#if 0
				else
					ob = new BattleObject(l);
#endif

				objects->add(ob);
			}
		}
	}
#ifdef BATEDIT
	else
		objects->makeRandom();
#endif
}

/*======================================================
 * Global Functions
 */

void writeBattleGame(WriteWithPopup& file, BattleData* battle)
{
#ifdef DEBUG
	file.showInfo("\rHeader");
#endif
	writeBattleHeader(file, battle);

#ifdef DEBUG
	file.showInfo("\rMap");
#endif
	writeBattleMap(file, battle);
	
#ifdef DEBUG
	file.showInfo("\rOrder of Battle");
#endif
	writeOB(file, battle->ob);

#ifdef DEBUG
	file.showInfo("\rStatic Objects");
#endif
	writeStaticObjects(file, battle->miscObjects);
}

void writeHistoricalBattle(const char* fileName, BattleData* battle)
{
	WriteWithPopup file(fileName, FT_HistoricalBattle, "Battle Game", battleFileVersion, 4);

	writeBattleGame(file, battle);

	if(!file.isGood())
		throw GeneralError("Error writing %s", fileName);
}

void readBattleGame(ReadWithPopup& file, BattleData* battle)
{
#ifdef DEBUG
	file.showInfo("\rOrder of Battle");
#endif
	readBattleHeader(file, battle);
	readBattleMap(file, battle);
	readOB(file, battle->ob);
#ifdef DEBUG
	file.showInfo("\rStatic Objects");
#endif
	readStaticObjects(file, battle->miscObjects);
#ifdef DEBUG
	file.showInfo("\rEnd of readBattleGame()");
#endif
}


void readHistoricalBattle(const char* fileName, BattleData* battle)
{
	ReadWithPopup file(fileName, FT_HistoricalBattle);

	readBattleGame(file, battle);

#ifdef DEBUG
	file.showInfo("\rafter readBattleGame()");
#endif

	if(!file.isGood())
		throw GeneralError("Error Reading %s", fileName);
}
