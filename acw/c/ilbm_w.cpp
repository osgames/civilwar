/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Write ILBM file
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.5  1994/09/02  21:25:33  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.3  1994/05/19  17:44:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/05/04  22:09:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/04/05  12:28:09  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <iostream.h>
#include <fstream.h>

#include "ilbm.h"
#include "ilbmdata.h"
#include "image.h"
#include "palette.h"
// #include "vesa.h"

typedef streampos ChunkPos;

class ILBMoStream : public ofstream {
public:
	ILBMoStream(char* s) : ofstream(s, ios::out | ios::binary) { }

	void writeChunkName(const char* name);
	ChunkPos startChunk(const char* chunkName);
	void endChunk(ChunkPos pos);

	void writeByte(UBYTE b);
	void writeWord(UWORD w);

	void writeBody(Image& image);
	// void writeBody(Vesa* screen);
	void writeLine(UBYTE*ad, int width);
};

void ILBMoStream::writeChunkName(const char* name)
{
	write(name, 4);
}

void ILBMoStream::writeByte(UBYTE b)
{
	put(b);
}

void ILBMoStream::writeWord(UWORD w)
{
	UBYTE b[2];

	b[0] = UBYTE(w >> 8);
	b[1] = UBYTE(w);

	write(b, 2);
}

ChunkPos ILBMoStream::startChunk(const char* chunkName)
{
	writeChunkName(chunkName);

	ChunkPos pos = tellp();

	UBYTE b[4];
	write(b, 4);

	return pos;
}

void ILBMoStream::endChunk(ChunkPos pos)
{
	streampos here = tellp();
	streampos length = here - pos - 4;
	
	if(length & 1)	 			// Add pad byte to make it even
	{
		length++;
		writeByte(0);
	}

	seekp(pos);
	UBYTE b[4];
	b[0] = UBYTE(length >> 24);
	b[1] = UBYTE(length >> 16);
	b[2] = UBYTE(length >> 8);
	b[3] = UBYTE(length);
	write(b, 4);

	seekp(here);
}

/*
 * Write an RLE line of an image
 */

void ILBMoStream::writeLine(UBYTE* ad, int width)
{
	int x = 0;

	while(x < width)
	{
		if((x != (width - 1)) && (ad[0] == ad[1]))		// Byte Run
		{
			int i = 1;
			UBYTE v = *ad++;
			x++;

			while((x < width) && (ad[0] == v))
			{
				ad++;
				i++;
				x++;
			}

			while(i)
			{
				int l;
				if(i > 128)
					l = 128;
				else
					l = i;

				writeByte(257 - l);
				writeByte(v);
				i -= l;
			}
		}
		else							// Byte Copy
		{
			UBYTE* start = ad;

			int i = 1;
			UBYTE v = *ad++;
			x++;

			/*
			 * Need a run of at least 3 to make it worth breaking the copy
			 */

			while( (x < width) && ( (ad[0] != v) || (ad[1] != v)))
			{
				v = *ad++;
				i++;
				x++;
			}

			if(x < width)
			{
				i--;
				ad--;
				x--;
			}

			while(i)
			{
				int l;

				if(i > 128)
					l = 128;
				else
					l = i;

				writeByte(l - 1);
				write(start, l);
				start += l;
				i -= l;
			}
		}
	}
}

/*
 * Write body as RLE
 */

void ILBMoStream::writeBody(Image& image)
{
	UBYTE* lineAd = image.getAddress();
	int lineCount = image.getHeight();

	UWORD fileWidth = image.getWidth();
	if(fileWidth & 1)
		fileWidth++;

	UBYTE* lineBuf = new UBYTE[fileWidth];
	memset(lineBuf, 0, fileWidth);

	while(lineCount--)
	{
		memcpy(lineBuf, lineAd, image.getWidth());

		writeLine(lineBuf, fileWidth);

		lineAd += image.getWidth();
	}
}





void writeILBM(const char* fname, Image& image, Palette& palette, UBYTE transparent)
{
#ifdef DEBUG
	cout << "Writing " << fname << endl;
#endif

	ILBMoStream stream((char*)fname);
	if(stream)
	{

		ChunkPos pos1;
		pos1 = stream.startChunk("FORM");

		stream.writeChunkName("PBM ");

		ChunkPos pos;
		pos = stream.startChunk("BMHD");
		stream.writeWord(image.getWidth());			// w
		stream.writeWord(image.getHeight());		// h
		stream.writeWord(0);								// x
		stream.writeWord(0);								// y
		stream.writeByte(8);								// planes
		stream.writeByte(mskNone);						// masking
		stream.writeByte(cmpByteRun);					// compression
		stream.writeByte(0);								// pad
		stream.writeWord(transparent);				// transparent
		stream.writeByte(1);								// xAspect
		stream.writeByte(1);								// yAspect
		stream.writeWord(image.getWidth());			// pageWidth
		stream.writeWord(image.getHeight());	   // pageHeight
		stream.endChunk(pos);

		pos = stream.startChunk("CMAP");

		// stream.write(palette.address(), palette.size());

		for(int i = 0; i < 256; i++)
		{
			UBYTE c[3];

			palette.getColour(i, c[0], c[1], c[2]);
			stream.write(c, 3);
		}

		stream.endChunk(pos);

		pos = stream.startChunk("BODY");
		stream.writeBody(image);
		stream.endChunk(pos);

		stream.endChunk(pos1);
	}
}

#if !defined(NO_VESA)

#if 0

/*
 * Write body as RLE
 */

void ILBMoStream::writeBody(Vesa* screen)
{
	Image lineBuf(screen->getW(), 1);

	for(int lineCount = 0; lineCount < screen->getH(); lineCount++)
	{
		screen->unBlit(lineBuf, Point(0, lineCount));
		writeLine(lineBuf.getAddress(), screen->getW());
	}
}

void writeILBM(const char* fname, Vesa* screen, UBYTE transparent)
{
	ILBMoStream stream((char*)fname);
	if(stream)
	{

		ChunkPos pos1;
		pos1 = stream.startChunk("FORM");

		stream.writeChunkName("PBM ");

		ChunkPos pos;
		pos = stream.startChunk("BMHD");
		stream.writeWord(screen->getW());		// w
		stream.writeWord(screen->getH());		// h
		stream.writeWord(0);						// x
		stream.writeWord(0);						// y
		stream.writeByte(8);								// planes
		stream.writeByte(mskNone);						// masking
		stream.writeByte(cmpByteRun);					// compression
		stream.writeByte(0);								// pad
		stream.writeWord(transparent);				// transparent
		stream.writeByte(1);								// xAspect
		stream.writeByte(1);								// yAspect
		stream.writeWord(screen->getW());				// pageWidth
		stream.writeWord(screen->getH());			   // pageHeight
		stream.endChunk(pos);

		pos = stream.startChunk("CMAP");
		stream.write(screen->palette.address(), screen->palette.size());
		stream.endChunk(pos);

		pos = stream.startChunk("BODY");
		stream.writeBody(screen);
		stream.endChunk(pos);

		stream.endChunk(pos1);
	}
}
#endif


#endif	// NO_VESA
