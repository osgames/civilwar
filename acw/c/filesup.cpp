/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	File Chunk Support
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "filesup.h"
#include "measure.h"
#include "gametime.h"

void putLocation(D_Location* dest, const Location& l)
{
	putLong(&dest->x, l.x);
	putLong(&dest->y, l.y);
}

void getLocation(D_Location* src, Location& l)
{
	l.x = getLong(&src->x);
	l.y = getLong(&src->y);
}

void putBattleLocation(D_BattleLocation* dest, const BattleLocation& l)
{
	putLong(&dest->x, l.x);
	putLong(&dest->y, l.z);
}

void getBattleLocation(D_BattleLocation* src, BattleLocation& l)
{
	l.x = getLong(&src->x);
	l.z = getLong(&src->y);
}



void putGameTime(D_GameTime* dest, const TimeBase& t)
{
	putWord(&dest->day, t.days);
	putLong(&dest->ticks, t.ticks);
}

void getGameTime(D_GameTime* src, TimeBase& t)
{
	t.days = getWord(&src->day);
	t.ticks = getLong(&src->ticks);
}

