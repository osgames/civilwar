/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Display of the front face making the landscape look like a
 * chunk of earth.
 *
 * This works by storing a list of points at the front edge making
 * up a continuous line.
 *
 * Vertical lines are drawn from the lines connecting these points
 * down to the bottom of the screen using a texture.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */


#include "earth.h"
#include "image.h"
#include "batldata.h"
#include "batlib.h"
#include "sprlib.h"


#ifdef DEBUG
#include "error.h"
#endif


LineList::LineList()
{
	texture = 0;
	skyTexture = 0;
	bottom = 500;		// Set arbitary bottom value until I can be bothered to calculate it
	clear();
}

LineList::~LineList()
{
	if(texture)
		battle->spriteLib->release(EarthTexture);
	if(skyTexture)
		battle->spriteLib->release(SkyTexture);
}

void LineList::clear()
{
	nextP = 0;
}

void LineList::add(const Point3D& p)
{
	if(nextP < MaxEarthPoints)
	{
		points[nextP++] = p;
	}
#ifdef DEBUG
	else
		throw GeneralError("Ran out of earthface points");
#endif
}


/*
 * Display a vertical section of earth
 *
 * Top is x,y
 * Logical bottom is x,bottom
 * Real bottom is x,y+length
 */

#define EarthTextureW 32
#define EarthTextureH 128

void putEarthLine(Region* bitmap, Image* texture, SDimension x, SDimension y, SDimension length, Cord3D bottom)
{
	if( (x < 0) || (x >= bitmap->getW()))
		return;

	if( (y + length) > bottom)
		length = bottom - y;

	if(length <= 0)
		return;

	int randX = x & 0x1f;

	UBYTE* srcPtr = texture->getAddress();
	UBYTE* destPtr = bitmap->getAddress(x,y);

	/*
	 * Prepare differential terms
	 */

	int dy = bottom - y;	// always positive
	int dz = texture->getHeight();

	int remainder = dz / 2;

	if((y >= 0) && (y < bitmap->getH()))
		*destPtr = srcPtr[randX];

	destPtr += bitmap->getLineOffset();
	length--;

	while(length)
	{
		while(remainder < dz)
		{
			remainder += dy;
			srcPtr += texture->getWidth();
		}

		randX = (randX + 11) & 0x1f;

		remainder -= dz;

		if(y >= 0)
			*destPtr = srcPtr[randX];

	 	destPtr += bitmap->getLineOffset();
		length--;
	}
}


void LineList::draw(Region* bitmap)
{
	if(!texture)
		texture = battle->spriteLib->read(EarthTexture);

	/*
	 * Test by just joining the dots!
	 */

	int n = 0;
	Point* p = points;

	while(++n < nextP)
	{
		if( (p[0].y < bitmap->getH()) || (p[1].y < bitmap->getH()) )
		{

			/*
			 * Run a bresenham line between 2 points drawing vertical lines
			 */

			int dx = p[1].x - p[0].x;
			int dy = p[1].y - p[0].y;
			int x = p[0].x;
			int y = p[0].y;

			unsigned int adx = abs(dx);
			unsigned int ady = abs(dy);

			int sx = sgn(dx);
			int sy = sgn(dy);

			int remainder = ady/2;

			SDimension length = bitmap->getH() - y;
			if(length > 0)
			{
				putEarthLine(bitmap, texture, x, y, length, bottom);
				// bitmap->VLine(x, y, length, 0x10 + (x & 15));
			}

			while(x < p[1].x)
			{
				while(remainder < ady)
				{
					remainder += adx;
					y += sy;
				}

				remainder -= ady;
				x += sx;

				length = bitmap->getH() - y;
				if(length > 0)
					putEarthLine(bitmap, texture, x, y, length, bottom);
					// bitmap->VLine(x, y, length, 0x10 + (x & 15));
			}
		}

		p++;
	}
}

void LineList::fillSky(Region* bm)
{
	if(!skyTexture)
		skyTexture = battle->spriteLib->read(SkyTexture);


	UBYTE* src = skyTexture->getAddress();
	UBYTE* dest = bm->getAddress();

	int y = bm->getH();
	if(y > skyTexture->getHeight())
		y = skyTexture->getHeight();

	while(y--)
	{
		UBYTE *dest1 = dest;

		int x = bm->getW();

		while(x >= skyTexture->getWidth())
		{
			memcpy(dest1, src, skyTexture->getWidth());
			dest1 += skyTexture->getWidth();
			x -= skyTexture->getWidth();
		}

		if(x)
			memcpy(dest1, src, x);	// skyTexture->getWidth());


		src += skyTexture->getWidth();
		dest += bm->getLineOffset();
	}
}
