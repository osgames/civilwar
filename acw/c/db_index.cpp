/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Database Index implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "db_index.h"
#include "db_dplay.h"
#include "datab.h"
#include "dialogue.h"
#include "text.h"
#include "system.h"
#include "screen.h"
#include "mouselib.h"


/*
 * Index Implementation
 */

void Index::distroy()
{
	delete iwidth;
	delete iheight;
	delete ixpos;
	delete iypos;
	delete remove;
	delete highlight;
}

/*
 * 	Displays an index;
 */


Index::Index( Dplay *display, FontID nn )
{
	TextWindow *window;
	Region *bitMap;
	// char *text;
	int count = 0;
	// *font = fontSet(n);
	int loop, length;
	int perincre;
	// const char wlen = 50;
	char word[ MaxIndexLength + 1 ];
	Point where, temp;
	int items;
	int offset = 0;
	int num = 0;
	int fincre = 4;
	short int head = 0;

	n = nn;

	font = fontSet(n);

	twinline = 1;

	//
	//	If > 2 columns define items above as array with dimension [ column + 1 ]
	//

	scr = display;

	const int cols = scr->getcols();

	remove = new int [ cols + 1 ];
	highlight = new int [ cols + 1 ];

	for ( loop = 0; loop < (cols + 1); loop++ )
	{
		remove[ loop ] = 0;
		highlight[ loop ] = 255;
	}

	char *textptr = scr->getOrig();
	char dat;
	char *orig = textptr;
	char* origin = scr->getOriginal();
	int tsize = scr->getPtrsz();

	iwidth = new short int [ cols ];
	iheight = new short int [ cols ];
	ixpos  = new short int [ cols ];
	iypos  = new short int [ cols ];

	where.x = 0;
	where.y = 0;

	for ( loop = 0; loop < cols; loop++ )
	{
		iwidth [ loop ] = scr->getTW( loop );
		iheight[ loop ] = scr->getTH( loop );
		ixpos  [ loop ] = scr->getTX( loop );
		iypos  [ loop ] = scr->getTY( loop );
	}

#if 0
	textptr += display->getPointerOffset ( display->getFormat() - '0' );

	while ( (int)( textptr - orig ) < tsize )     //*textptr != '\0' )
	{				  
	 	if ( *textptr++ == '@' && *(textptr - 2 ) == '\0' ) 
		{
			count++;
			if ( *textptr == '\0' && *(textptr - 2) == '\0' ) break;
		}
	}
#else
	count = display->findlastTFP();
#endif	

	numitems = count;	  

	if ( count == 0 ) return;

	if ( cols > 1 )
	{
		items = numitems / cols;
		if ( numitems % cols ) items++;
		// if ( scr->getFormat() == '9' ) items = 25;
	}
	else 	items = numitems;

	while ( num++ < cols )
	{
		window = scr->retWin ( num - 1 );
		bitMap = scr->retReg ( num - 1 );
		
		window->setColours ( c_Text );

		fincre = ( bitMap->getH() / items ); // - font->getHeight();
		
		perincre = ( bitMap->getH() % items ) - fincre/2;

		where.y = fincre / 2;
		
		for ( loop = 0; loop < items; loop++ )
		{
			length = 0;
			display->setTFP2( loop + offset );
			textptr = origin + scr->getFilePos() + 2;
			
			while ( ( ( dat = *textptr++ ) != '~' ) && ( dat != '(' ) ) 
			{
				if ( dat != 10 )
				{
					if ( dat != 13 )
						word[ length++ ] = dat;
					else
						word [ length++ ] = ' ';

					if(length == MaxIndexLength) 
					{ 
#ifdef DEBUG
						dialAlert(0, "Error in Index word length", "Ooops!");
#endif
						//cout << "Error in Index word length"; 
						
						length--;
						break;
					}
				}
			}
			
			window->setPosition( where );

			word [ length ] = '\0';
			window->draw ( word );

			// where.y += fincre;
			
			if ( *textptr == '^' ) 
			{
				length = 0;
				textptr++;

				where.y += font->getHeight();

				if ( twinline == 1 ) 
				{
					twinline = 2;
					perincre -= fincre/2;
				}

				while ( ( ( dat = *textptr++ ) != '~' ) && ( dat != '(' ) ) 
				{
					if ( dat != 10 )
					{
						if ( dat != 13 )
							word[ length++ ] = dat;
					  	else
							word [ length++ ] = ' ';
						if ( length == MaxIndexLength ) 
						{ 
#ifdef DEBUG
							dialAlert(0, "Error in Index word length", "Ooops!");
#endif
							//cout << "Error in Index word length"; 
							length--;
						}
					}
				}
				window->setPosition( where );

				word [ length ] = '\0';
				window->draw ( word );
				where.y -= font->getHeight();

			}
			
			where.y += fincre; 

			if ( perincre > 0 ) { where.y++; perincre--; }
			// else if ( perincre < 0 ) { where.y++; perincre++; }
		}

		items = numitems / cols;
		// if ( scr->getFormat() == '9' ) items = numitems - 25;

		offset = loop;
	}

	keylit = 0;
}

Point Index::MoveMousePtr( int direction, char ind, int clear, Point temp )
{
	int items;
	int fincre, offset = 0, perincre;
	char col;

	const char cols = scr->getcols();

	if ( ( direction == 3 ) && ( cols == 1 ) ) return Point( 0, 0 );

	Point location = machine.mouse->getPosition();

	Region *bitMap;
	TextWindow *window;

	if ( cols > 1 )
		{
			items = numitems / cols;
			if ( numitems % cols ) items++;
		}
	else items = numitems;

	col = ind - 1;

	if ( col >= 1 ) 
		{
			offset = items;

			items = numitems / 2;
		}

	window = scr->retWin ( col );
	bitMap = scr->retReg ( col );

	if ( temp.y >= bitMap->getH() )
		{
			location.y -= ( temp.y - bitMap->getH() + 2 );
			machine.mouse->moveMouse( location );
		}

	fincre = bitMap->getH() / items; 
	perincre = ( bitMap->getH() % items ) - ( twinline * fincre/2 );

	if (clear)
		{
			/* if ( col == 0 ) keylit = 0;
				else keylit = offset + 1;
			*/

			if ( scr->getFormat() == '2' ) offset = 75;
			else offset = 30;

			if ( ( location.y > ( bitMap->getH() + offset ) ) || ( location.y < offset ) ) location = Point( 0, 0 );

			// machine.mouse->moveMouse( Point( location.x , location.y + fincre + 5 ) );

			return location;
		}

	switch( direction )
		{
			case 1:						  // up
			  if ( keylit > offset )
					{
				   	location.y -= fincre;
						
						keylit--;
						if ( ( keylit ) && ( keylit < perincre ) ) location.y--;
						
						machine.mouse->moveMouse( Point( location.x, location.y ) );
					}
				break;

			case 2:						  // Down
				if ( ( keylit - offset ) < items-1 )
					{
						if ( (temp.y + fincre) < bitMap->getH() )
							{
								location.y += fincre;

								keylit++;
								if ( keylit < perincre ) location.y++;
								machine.mouse->moveMouse( Point( location.x, location.y ) );
							}
						else
							{
								location.y += fincre / 2;
								
								keylit++;
								// if ( keylit < perincre ) location.y++;
								machine.mouse->moveMouse( Point( location.x, location.y ) );
							}
					}
				break;

			default:
				break;
		}

  return location;	
}


/*
 *   Highlights a index line;
 */

void Index::light( Point mousepos, char ind, char off )
{
	Point where;
	int ypos, items, length = 0;
	int fincre, offset = 0, perincre;
	char col, dat;
	char *textptr = scr->getOrig();
	char* origin = scr->getOriginal();
	char word[MaxIndexLength + 1];
	
	Region *bitMap;
	TextWindow *window;

	int textcolour = c_PageTitle;

	const char cols = scr->getcols();

	if ( cols > 1 )
	{
		items = numitems / cols;
		if ( numitems % cols )
			items++;
	}
	else
		items = numitems;

	if ( mousepos.x < 0 )
		off = 1; 
	if ( mousepos.y < 0 )
		off = 1;

	col = ind - 1;

	if ( off > 0 ) 
	{
		if (  highlight[ ind ] == 255 )
			return;
		
		if (  highlight[ ind ] >= items )
		{
				highlight[ ind ] -= items;
		}
				
	}
	else if (  highlight[ ind ] != 255 )
		return;

	if ( col >= 1 ) 
	{ 
		offset = items; 
		items = numitems / 2;
	}

	window = scr->retWin ( col );
	bitMap = scr->retReg ( col );

	fincre = bitMap->getH() / items; 
	perincre = ( bitMap->getH() % items ) - ( twinline * fincre/2 );
	
	ypos = mousepos.y - font->getHeight() / 2;
	
	if ( perincre > 0 )
	{
		if ( ( ypos / fincre ) <= perincre ) 
			ypos -= ypos / fincre;
		else
			ypos -= perincre;
	}
	
	ypos = ypos / fincre;
	
	if ( ( highlight[ ind ] == ( ypos + offset ) ) || ( highlight[ ind ] == ypos ) )
	{
		if ( ( mousepos.x >= 0 ) && ( mousepos.y >= 0 ) )
		{
			if ( ( mousepos.x <= bitMap->getW() ) && ( mousepos.y <= bitMap->getH() ) )
				return;
		}
	}

	if ( off == 0 ) 
	{ 
		if ( ypos == items )
			ypos = items - 1;
		highlight[ ind ] = ypos + offset;
		remove[ind] = 1;
		keylit = ypos + offset;
	} 
	else
	{
		if ( ( ypos == items ) && ( ypos = (highlight[ ind ] - 1) ) && ( mousepos.y < bitMap->getH() ) ) 
			return;
		ypos =  highlight[ ind ]; 
		highlight[ ind ] = 255;
		remove[ind] = 0;
	}

	if ( ypos < 0 )
		ypos = 0;

	if ( ypos >= items )
		return;

	where.x = 0;
	where.y = ypos * fincre + fincre/2;

	if ( perincre > 0 )
	{
		if ( ypos <= perincre ) 
			where.y += ypos;
		else 
			where.y += perincre;
	}

	if ( where.y >= bitMap->getH() ) 
		return;

	length = 0;

	scr->setTFP2( ypos + offset );
	textptr = origin + scr->getFilePos() + 2;
	
	if ( off == 0 ) 
	{
		if ( scr->getFormat() == '2' ) 
		{
			if ( ind == 1 )
				textcolour = c_IndexUnion; //	window->setColours( 1 );
			else
				textcolour = c_IndexHighlight;  		  //  window->setColours( 8 );
		}
		else 
			textcolour = c_IndexHighlight; 					  //	window->setColours( 15 );
	}
//	else window->setColours( c_Text );
	
	while ( ( ( dat = *textptr++ ) != '~' ) && ( dat != '(' ) ) 
	{
		if ( dat != 10 )  
		{
			if ( dat != 13 ) 
				word[ length++ ] = dat;
  			else 
				word [ length++ ] = ' ';
		}
	}

	word [ length ] = '\0';
	
	if ( col == 0 ) 
	{
		//scr->upDatewin();
		Point winoffset = scr->getwhere( 0 );
		Region *indexreg;
		TextWindow *indexwin;

		indexreg = new Region ( machine.screen->getImage(), Rect( winoffset.x + where.x, winoffset.y + where.y, font->getWidth( word ), font->getHeight() ) );
		indexwin = new TextWindow ( indexreg, n ); 
		
		indexwin->setColours( textcolour );
		indexwin->setPosition( Point( 0,0 ) );
		indexwin->draw( word );
		machine.screen->setUpdate( *indexreg );
		
		delete indexreg;
		delete indexwin;
	}
	else 
	{
		window->setColours( textcolour );
		window->setPosition( where );
		window->draw ( word );
		machine.screen->setUpdate( *bitMap );
	}
  	
	// if ( off == 0 ) window->setColours( c_Text );
}

















