/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#include <string.h>
#include <conio.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>


void main()
{
 	char buffer[] = "This is a test: string";

	char *ptr = strstr( buffer, "test:" );

	if ( !ptr ) printf( "No instance found!");

	ptr += 4;

	printf( ":%s:", ptr );

	printf( "\n:%s:", buffer );
}
