/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Test 3D Shapes
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.18  1994/03/10  14:27:18  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.17  1993/12/23  09:26:01  Steven_Green
 * TestMouse class defined to force the mouse's SuperSprite to be freed.
 *
 * Revision 1.16  1993/12/21  00:31:04  Steven_Green
 * Added some 3d sprites.
 *
 * Revision 1.15  1993/12/15  20:56:36  Steven_Green
 * Initialise terrain
 *
 * Revision 1.14  1993/12/13  21:59:58  Steven_Green
 * Lightsource set using height/bearing angles
 *
 * Revision 1.13  1993/12/13  17:11:43  Steven_Green
 * Display of lightsource
 *
 * Revision 1.12  1993/12/13  14:09:20  Steven_Green
 * removed things that weren't used
 *
 * Revision 1.11  1993/12/10  16:06:39  Steven_Green
 * Gourad shading testing
 *
 * Revision 1.10  1993/12/04  16:22:51  Steven_Green
 * catching of exceptions added.
 * MousePtr class defined to force mouse to be deallocated even if an
 * exception happens.
 *
 * Revision 1.9  1993/12/04  01:06:10  Steven_Green
 * Loads in LBM screen.
 * Uses new palette class.
 *
 * Revision 1.8  1993/12/03  17:25:23  Steven_Green
 * Icon initialisation moved into testicon
 *
 * Revision 1.7  1993/12/01  15:11:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1993/11/30  02:57:11  Steven_Green
 * Some attempted optimisation
 *
 * Revision 1.5  1993/11/26  22:30:38  Steven_Green
 * Testing of viewpoint (rotate around viewed point instead of eye).
 *
 * Revision 1.4  1993/11/25  04:40:06  Steven_Green
 * map grid object added
 *
 * Revision 1.3  1993/11/24  09:32:52  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/11/19  18:59:26  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/16  22:42:06  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


#include <conio.h>
#include <strstrea.h>

#include "test3d.h"
#include "testicon.h"
#include "testshp.h"
#include "view3d.h"
#include "map3d.h"
#include "mouse.h"
#include "ilbm.h"
#include "palette.h"
#include "vesa.h"
#include "timer.h"
#include "testtime.h"
#include "text.h"
#include "trig.h"
#include "terrain.h"
#include "sprlib.h"
#include "sprite3d.h"

/*
 * Program completed
 */

Boolean quitProgram = False;

Palette myPalette;

#ifdef SHOW_RENDER
Mouse* globalMouse = 0;
#endif

/*
 * Make a little Sprite using an image
 */

class TestMouse : public Mouse {
public:
	TestMouse(Vesa* screen);
	~TestMouse() {	delete sprite; }
};

TestMouse::TestMouse(Vesa* screen) : Mouse()
{
	BitMap tinyBM(32,32);

	tinyBM.line(Point(1,0), Point(31,30), 1);
	tinyBM.line(Point(0,1), Point(30,31), 1);
	tinyBM.line(Point(0,15), Point(31,15), 1);
	tinyBM.line(Point(0,17), Point(31,17), 1);
	tinyBM.line(Point(15,0), Point(15,31), 1);
	tinyBM.line(Point(17,0), Point(17,31), 1);
	tinyBM.line(Point(30,0), Point(0,30), 1);
	tinyBM.line(Point(31,1), Point(1,31), 1);

	tinyBM.line(Point(0,0), Point(31,31), 10);		// Draw a cross
	tinyBM.line(Point(31,0), Point(0,31), 11);
	tinyBM.line(Point(0,16), Point(31,16), 12);
	tinyBM.line(Point(16,0), Point(16,31), 13);

	/*
	 * Copy the image into a sprite
	 */

	SuperSprite* sprite = new SuperSprite(tinyBM);

	/*
	 * Make it into the mouse
	 */

	// return new Mouse(screen, sprite, Point(16,16));
	init(screen, sprite, Point(16,16));
}


/*
 * Display the palette as 16 by 16 boxes of 4x4
 */

void drawPalette(BitMap& bm, const Point& where)
{
	int lineCount = 16;
	Colour col = 0;
	Point lineP = where;

	while(lineCount--)
	{
		int colCount = 16;

		Point p = lineP;

		while(colCount--)
		{
			bm.frame(p.getX(), p.getY(), 5, 5, 0);
			bm.box(p.getX()+1, p.getY()+1, 3, 3, col++);

			p.x += 4;
		}

		lineP.y += 4;
	}

}



/*
 * Simple automovement code for testing movement
 */

class Move {
	Cord3D* value;
	Cord3D min;
	Cord3D max;
	Cord3D speed;
public:
	Move(Cord3D& p, Cord3D from, Cord3D to, Cord3D s)
	{
		value = &p;
		min = from;
		max = to;
		speed = s;
	}

	void update();

};

void Move::update()
{
	*value += speed;

	if( (*value <= min) || (*value >= max) )
	{
		speed = -speed;
		*value += speed;
	}
}


void main()
{
 try
 {
	Vesa screen(0x101, 0x101);
	TestMouse mouse(&screen);
#ifdef SHOW_RENDER
	globalMouse = &mouse;
#endif

	const SDimension windW = 480;
	const SDimension windH = 340;
	const SDimension windX = 32;
	const SDimension windY = 32;
	const Rect mapRect(windX, windY, windW, windH);

	// SetVgaPalette(palette,256);
	
	{
		BitMap full(&screen, Colour(255));

		readILBM("art\\testscr1", &full, &myPalette);

		full.frame(windX - 1, windY - 1, windW + 2, windH + 2, Colour(32), Colour(34));
		full.frame(windX - 2, windY - 2, windW + 4, windH + 4, Colour(33), Colour(35));

		font.move(Point(200, 4));
		full.text("Steven's 3D Shape Tester");

		drawPalette(full, Point(560, 256 + 40));

		myPalette.set();
		mouse.blit(full, Point(0, 0));

#if 0
		/*
		 * Put a test Gourad shaded triangle on screen
		 */

		full.setClip(0, 0, 0, 150);	// Force bottom to be clipped off
		Polygon<Point3DI> gPoly(12);
		gPoly.setColor(0x60);

		ViewPoint viewPoint(&full);
		{
			PolyClip3DI clip(&gPoly, &viewPoint);

			clip.clipPoint(Point3DI(100,220, 500, Intensity(intensityRange-1)));
			clip.clipPoint(Point3DI(200, 50, 500, Intensity(0)));
			clip.clipPoint(Point3DI(50, 70, 500, Intensity(intensityRange/2)));
		}
		gPoly.render(&full);
		mouse.blit(full, Point(0, 0));
#endif

	}

	/*
	 * Make an object list
	 */

	ObjectList obList;
#if 0
	obList.insert(&ob1);
	obList.insert(&ob2);
	obList.insert(&ob3);
	obList.insert(&ob4);
	// obList.insert(&obFloor);
#endif


	BitMap mapArea(windW, windH);
	BitMap infoBM(600, 18);


	ViewPoint view(&mapArea, Position3D(Point3D(0, 0, 0), Wangle3D(0xeaaa, 0x2000,0)));
	// ViewPoint view(&mapArea, Position3D(Point3D(0, 0, 0), Wangle3D(0xc000, 0x2000,0)));
	// LightSource lightSource = Point3D(-200, -100, 50);	// Directly above
	LightSource lightSource = LightSource(Degree(30), 0);		// 30 degrees down from north

	const int gridRes = 4;		// 4 grids per mile
	const int gridMiles = 8;	// 8 Miles total
	const int gridCount = gridMiles * gridRes + 1;
	const Cord3D gridSize = 1760 / gridRes;

	terrain.init();

	MapGrid map3d(gridCount, gridCount, gridSize);
	map3d.create();
	WorldObject obMap(&map3d, Point3D(0,0,0));
	obList.insert(&obMap);

	SpriteLibrary library("art\\testspr");

	Sprite3D sprite1(library, 0);
	WorldObject obSprite1(&sprite1, Point3D(0, map3d.getHeight(0,0), 0));
	obList.insert(&obSprite1);

	Sprite3D sprite2(library, 1);
	WorldObject obSprite2(&sprite2, Point3D(300, map3d.getHeight(300,0), 0));
	obList.insert(&obSprite2);

	Sprite3D sprite3(library, 2);
	WorldObject obSprite3(&sprite3, Point3D(0, map3d.getHeight(0,500), 500));
	obList.insert(&obSprite3);
	// Move move3( (obSprite3.getPosition())->x, -800, +800, 20);
	Move move3x(obSprite3.x, -800, +800, 20);
	Move move3z(obSprite3.z, -800, +800, 30);

	Move move2x(obSprite2.x, -900, +900, 25);
	Move move2z(obSprite2.z, -900, +900, 15);

	IconData icons(&view, &mouse, &lightSource);
	icons.make();
	icons.draw();
	
	int count = 0;


	while(!quitProgram)
	{
		testTimer.init();

		mapArea.fill(0);

		testTimer.mark("Fill");

		icons.execute();
		if(quitProgram)
			break;

		move3x.update();
		move3z.update();
		move2x.update();
		move2z.update();
		obSprite3.y = map3d.getHeight(obSprite3.x, obSprite3.z);
		obSprite2.y = map3d.getHeight(obSprite2.x, obSprite2.z);

		{
			ostrstream buffer;

			buffer <<
					view.getX() << "," <<
				  	view.getY() << "," <<
				  	view.getZ() << " : " <<
				  	view.getXR().get() << "," <<
				  	view.getYR().get() << "," <<
				  	view.getZR().get() << " : " <<
				  	view.getDistance() << "," <<
				  	view.getFront() << "," <<
				  	view.getViewWidth() << " : " <<
					((view.getProjection() == Perspective) ? "Perspective" : "Parallel") <<
					'\0';

			char* s = buffer.str();

			infoBM.fill(0xfe);
			font.move(Point( (infoBM.getW() - strlen(s) * 8) / 2, 1));
			infoBM.text(s);

			delete s;

			mouse.blit(infoBM, Point(20, 380));
		}

		{
			ostrstream buffer;

			buffer <<
					"Lighting: " <<
					lightSource.getX() << "," <<
				  	lightSource.getY() << "," <<
				  	lightSource.getZ() << " : " <<
					"Height: " << Degree(lightSource.getHeight()) << ", " <<
					"Bearing: " << Degree(lightSource.getBearing()) <<
					'\0';


			char* s = buffer.str();

			infoBM.fill(0xfe);
			font.move(Point( (infoBM.getW() - strlen(s) * 8) / 2, 1));
			infoBM.text(s);

			delete s;

			mouse.blit(infoBM, Point(20, 400));
		}

		testTimer.mark("Test");

		drawObjectList(&view, &obList, &lightSource);

		testTimer.mark("Delete");

		timer->waitRel(1);
		mouse.blit(mapArea, mapRect);

		testTimer.mark("Blit");

		/*
		 * Show results of timer counts
		 */

		{
			ostrstream buffer;

			testTimer.show(buffer);

			char* s = buffer.str();

			infoBM.fill(0xfe);
			font.move(Point( (infoBM.getW() - strlen(s) * 8) / 2, 1));
			infoBM.text(s);

			delete s;

			mouse.blit(infoBM, Point(20, 420));
		}

	}

	obList.clear();

 }
 catch(Vesa::VesaError e)
 {
 	cout << "Vesa Error: " << e.get() << endl;
 }
 catch(Image::ImageError)
 {
 	cout << "Image Error: " << endl;
 }
 catch(GeneralError e)
 {
	cout << "Untrapped Error: ";
	if(e.get())
		cout << e.get();
	else
		cout << "No description";
	cout << endl;
 }
 catch(...)
 {
 	cout << "Caught some kind of error" << endl;
 }
}
