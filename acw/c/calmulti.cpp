/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	CAL MultiPlayer functions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "calmulti.h"
#include "game.h"
#include "connect.h"
#if !defined(TESTBATTLE) && !defined(BATEDIT)
#include "campaign.h"
#include "campwld.h"
#endif
#if !defined(TESTBATTLE)
#include "ob.h"
#endif	// !TESTBATTLE
#ifndef TESTCAMP
#include "batldata.h"
#endif
#include "calmain.h"

#if !defined(TESTBATTLE)
struct TransferUnitPacket {
	UnitID unit;
	UnitID dest;
};
#endif

struct CALPacket {
	UnitID unit;
};

#if !defined(TESTBATTLE)
void txCALTransferUnit(OrderBattle* ob, Unit* unit, Unit* dest)
{
	TransferUnitPacket* packet = new TransferUnitPacket;

	packet->unit = ob->getID(unit);
	packet->dest = ob->getID(dest);
	game->connection->sendMessage(Connect_CAL_TransferUnit, (UBYTE*)packet, sizeof(TransferUnitPacket));
}

void txCALTransferGeneral(OrderBattle* ob, General* g, Unit* dest)
{
	TransferUnitPacket* packet = new TransferUnitPacket;

	packet->unit = ob->getID(g);
	packet->dest = ob->getID(dest);
	game->connection->sendMessage(Connect_CAL_TransferGeneral, (UBYTE*)packet, sizeof(TransferUnitPacket));
}

void txCALMakeNew(OrderBattle* ob, Unit* unit, General* general)
{
	TransferUnitPacket* packet = new TransferUnitPacket;

	packet->unit = ob->getID(unit);
	packet->dest = ob->getID(general);
	game->connection->sendMessage(Connect_CAL_MakeNew, (UBYTE*)packet, sizeof(TransferUnitPacket));
}
#endif	// TESTBATTLE

void txCALCal(OrderBattle* ob, PacketType type, Unit* unit)
{
	CALPacket* packet = new CALPacket;

#ifdef DEBUG
	netLog.printf("sendCalPacket(%s, %s)",
		packetStr(type),
		unit->getName(True));
#endif

	packet->unit = ob->getID(unit);
	game->connection->sendMessage(type, (UBYTE*)packet, sizeof(CALPacket));
}

OrderBattle* getOrderBattle()
{
#ifdef TESTCAMP
	return &campaign->world->ob;
#elif defined(TESTBATTLE) || defined(BATEDIT)
	return battle->ob;
#else
	if(battle)
		return battle->ob;
	else if(campaign)
		return &campaign->world->ob;
#ifdef DEBUG
	else
		throw GeneralError("No Order of Battle");
#else
	else
		return 0;
#endif
#endif
}

#if !defined(TESTBATTLE)
void rxCALTransferUnit(Packet* packet)
{
	TransferUnitPacket* pkt = (TransferUnitPacket*) packet->data;

	OrderBattle* ob = getOrderBattle();

	Unit* unit = ob->getUnitPtr(pkt->unit);
	Unit* dest = ob->getUnitPtr(pkt->dest);

#ifdef DEBUG
	netLog.printf("Transfer Unit %s to %s",
		unit->getName(True),
		dest ? dest->getName(True) : "Null");
#endif

	if( (unit->getRank() == Rank_Regiment) && (dest->getRank() == Rank_Regiment) )
	{
		ob->mergeRegiment( (Regiment*)unit, (Regiment*)dest);
		unit = dest;
	}
	else if( (unit->getRank() == Rank_Regiment) && (dest == 0))
	{
		ob->addFreeRegiment((Regiment*)unit);
		unit = 0;
	}
	else
		ob->assignUnit(unit, dest);

	if(cal && (packet->sender == RID_Local))
	{
		if(unit)
			cal->updateTops(unit);	// Force unit onto screen
		cal->setRedraw();
	}
}

void rxCALTransferGeneral(Packet* packet)
{
	TransferUnitPacket* pkt = (TransferUnitPacket*) packet->data;

	OrderBattle* ob = getOrderBattle();

	General* g = ob->getGeneralPtr(pkt->unit);
	Unit* dest = ob->getUnitPtr(pkt->dest);

#ifdef DEBUG
	netLog.printf("Transfer General %s to %s",
		g->getName(),
		dest ? dest->getName(True) : "Null");
#endif

	ob->assignGeneral(g, dest);

	if(cal && (packet->sender == RID_Local))
	{
		if(dest)
			cal->updateTops(dest);	// Force unit onto screen
		cal->setRedraw();
	}
}

void rxCALMakeNew(Packet* packet)
{
	TransferUnitPacket* pkt = (TransferUnitPacket*) packet->data;

	OrderBattle* ob = getOrderBattle();

	Unit* unit = ob->getUnitPtr(pkt->unit);
	General* g = ob->getGeneralPtr(pkt->dest);

#ifdef DEBUG
	netLog.printf("Making New with General %s under %s",
		g ? g->getName() : "Null",
		unit ? unit->getName(True) : "Null");
#endif

	Unit* p = ob->makeNew(unit, g);

	if(cal && (packet->sender == RID_Local))
	{
		cal->updateTops(p);	// Force unit onto screen
		cal->setRedraw();
	}
}

#endif	// TESTBATTLE

#if !defined(BATEDIT) && !defined(TESTBATTLE) && !defined(CAMPEDIT)

void rxCALRejoinCampaign(Packet* packet)
{
	CALPacket* pkt = (CALPacket*) packet->data;

	OrderBattle* ob = getOrderBattle();

	Unit* unit = ob->getUnitPtr(pkt->unit);

#ifdef DEBUG
	netLog.printf("%s rejoin", unit->getName(True));
#endif

	unit->rejoinCampaign();

	if(cal && (packet->sender == RID_Local))
		cal->setRedraw();
}

void rxCALReattachCampaign(Packet* packet)
{
	CALPacket* pkt = (CALPacket*) packet->data;

	OrderBattle* ob = getOrderBattle();

	Unit* unit = ob->getUnitPtr(pkt->unit);

#ifdef DEBUG
	netLog.printf("%s reattach", unit->getName(True));
#endif

	unit->reattachCampaign(False);

	if(cal && (packet->sender == RID_Local))
		cal->setRedraw();
}

void rxCALReattachAllCampaign(Packet* packet)
{
	CALPacket* pkt = (CALPacket*) packet->data;

	OrderBattle* ob = getOrderBattle();

	Unit* unit = ob->getUnitPtr(pkt->unit);

#ifdef DEBUG
	netLog.printf("%s reattach all", unit->getName(True));
#endif

	unit->reattachCampaign(True);

	if(cal && (packet->sender == RID_Local))
		cal->setRedraw();
}

#endif

#if !defined(CAMPEDIT) && !defined(TESTCAMP) && !defined(BATEDIT)

void rxCALRejoinBattle(Packet* packet)
{
	CALPacket* pkt = (CALPacket*) packet->data;

	OrderBattle* ob = getOrderBattle();

	Unit* unit = ob->getUnitPtr(pkt->unit);

#ifdef DEBUG
	netLog.printf("%s rejoin", unit->getName(True));
#endif

	unit->rejoinBattle();

	if(cal && (packet->sender == RID_Local))
		cal->setRedraw();
}

void rxCALReattachBattle(Packet* packet)
{
	CALPacket* pkt = (CALPacket*) packet->data;

	OrderBattle* ob = getOrderBattle();

	Unit* unit = ob->getUnitPtr(pkt->unit);

#ifdef DEBUG
	netLog.printf("%s reattach", unit->getName(True));
#endif

	unit->reattachBattle(False);

	if(cal && (packet->sender == RID_Local))
		cal->setRedraw();

}

void rxCALReattachAllBattle(Packet* packet)
{
	CALPacket* pkt = (CALPacket*) packet->data;

	OrderBattle* ob = getOrderBattle();

	Unit* unit = ob->getUnitPtr(pkt->unit);

#ifdef DEBUG
	netLog.printf("%s reattach all", unit->getName(True));
#endif

	unit->reattachBattle(True);

	if(cal && (packet->sender == RID_Local))
		cal->setRedraw();

}

#endif

