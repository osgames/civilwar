/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Railway handling
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.10  1994/07/28  18:57:04  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/07/25  20:32:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/07/19  19:53:03  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/07/13  13:50:17  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/06/21  18:45:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/06/07  18:29:54  Steven_Green
 * Removed old code.
 *
 * Revision 1.4  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.3  1994/05/25  23:31:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/05/21  13:16:19  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/05/19  17:44:37  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG
#include <stdio.h>
#endif

#include "railway.h"
#include "campwld.h"
#include "campaign.h"

#include "mapwind.h"
#include "city.h"
#include "colours.h"
#include "image.h"
#include "generals.h"

#if defined(DEBUG) && !defined(CAMPEDIT)
#include "msgwind.h"
#include "log.h"
#include "timer.h"

#include "clog.h"

LogFile railLog("railway.log");
#endif




#ifdef CAMPEDIT

void showRailways(MapWindow* map, FacilityList& facilities)
{
	FacilityID fCount = 0;
	while(fCount < facilities.entries())
	{
		Facility* f = facilities[fCount];

		if(f->connections.entries())
		{
			Point fromP = map->locationToPixel(f->location);

			UBYTE rCount = 0;
			while(rCount < f->connections.entries())
			{
				FacilityID con = f->connections[rCount++];

				if(con > fCount)		// Don't draw routes twice
				{
					Facility* dest = facilities[con];
					Point toP = map->locationToPixel(dest->location);

					map->drawBM->line(fromP, toP, Yellow);
				}
			}
		}

		fCount++;
	}
}

#else

void showRailways(MapWindow* map, FacilityList* facilities, RailSectionList* rails)
{
	FacilityID fCount = 0;
	while(fCount < facilities->entries())
	{
		Facility* f = facilities->get(fCount);

		UBYTE jCount = f->railCount;
		if(jCount)
		{
			RailSectionID rCount = f->railSection;
			Point fromP = map->locationToPixel(f->location);

			while(jCount--)
			{
				FacilityID con = rails->get(rCount++);

				if(con > fCount)		// Don't draw routes twice
				{
					Facility* dest = facilities->get(con);
					Point toP = map->locationToPixel(dest->location);

					map->drawBM->line(fromP, toP, Black);
				}
			}
		}

		fCount++;
	}
}

#endif	// CAMPEDIT

#ifndef CAMPEDIT
/*
 * Get time to get from one station to another
 * based on straight line distance moving at speed of 30mph.
 */

inline GameTicks railDistance(Facility* f1, Facility* f2)
{
	Distance d = distance(f1->location.x - f2->location.x,
								 f1->location.y - f2->location.y);

	const int TrainSpeed = 30;		// 30 mph

	return unitToMile(d) * (GameTicksPerHour / TrainSpeed);	// () because 60/30=2
}


inline GameTicks railDistance(FacilityID from, FacilityID to)
{
	return railDistance(campaign->world->facilities.get(from),
							  campaign->world->facilities.get(to));
}


/*
 * Non recursive wide search
 */

/*
 * A queue system
 */

struct RailQueueItem {
	FacilityID from;
	GameTicks cost;

	RailQueueItem() { }
	void set(FacilityID city, GameTicks t) { from=city; cost = t; }
	void get(FacilityID& city, GameTicks& t) { city = from; t = cost; }
};

class RailQueue {
	RailQueueItem* items;		// Array of items
	int howMany;
	int head;						// Where to put next item
	int tail;						// Where to pull item
public:
	RailQueue(int n);
	~RailQueue();

	void put(FacilityID from, GameTicks cost);
	Boolean get(FacilityID& from, GameTicks& cost);
	Boolean isEmpty() { return head == tail; }

#ifdef DEBUG
	int maxUsage;
#endif
};

RailQueue::RailQueue(int n)
{
	items = new RailQueueItem[n];
	howMany = n;
	head = 0;
	tail = 0;
#ifdef DEBUG
	maxUsage = 0;
#endif
}

RailQueue::~RailQueue()
{
	delete[] items;
}

void RailQueue::put(FacilityID from, GameTicks cost)
{
	int nextHead = head + 1;
	if(nextHead >= howMany)
		nextHead = 0;

	if(nextHead == tail)		// queue full
		throw GeneralError("rail search queue is full");

	items[head].set(from, cost);
	head = nextHead;
#ifdef DEBUG
	int usage;

	if(head > tail)
		usage = head - tail;
	else
		usage = head + howMany - tail;

	if(usage > maxUsage)
		maxUsage = usage;
#endif
}

/*
 * Return True if got, False if queue empty
 */

Boolean RailQueue::get(FacilityID& from, GameTicks& cost)
{
	if(head == tail)
		return False;
	else
	{
		items[tail].get(from, cost);

		if(++tail >= howMany)
			tail = 0;

		return True;
	}
}

/*
 * Determine a rail route between 2 facilities.
 *
 * | allowEnemy: If set then routes are allowed through enemy, otherwise
 * |            only friendly/neutral stations are allowed.
 *      side : If not neutral, then don't allow routes through enemy station
 *     routes: If non-zero, this is filled with an array of RailRoute.
 *             It is up to the caller to delete it when finished with.
 *
 * The return value is the next facility to move to on-route
 */

FacilityID CampaignWorld::findRailRoute(FacilityID from, FacilityID to, Side side, RailRouteInfo* routes)
{
	if( (from == NoFacility) || (to == NoFacility) )
	{
#ifdef DEBUG
		railLog.printf("findRailRoute called with NoFacility %d,%d",
			(int) from,
			(int) to);
#endif
		return NoFacility;
	}

	if(from == to)
		return NoFacility;

	/*
	 * Set up cost table
	 * Use the user's table if it was supplied
	 */

	RailRoute* costs;
	
	if(routes && routes->route)
		costs = routes->route;
	else
		costs = new RailRoute[facilities.entries()];

	/*
	 * Initial table to unvisited
	 */

	int i = facilities.entries();

	RailRoute* costPtr = costs;
	while(i--)
	{
		costPtr->cost = UndefinedTime;
		costPtr->from = NoFacility;
		costPtr++;
	}

	/*
	 * Set up queue
	 */

	RailQueue queue(facilities.entries());		// This should be big enough

	/*
	 * Add initial entry to queue
	 */

	queue.put(from, 0);
	costs[from].cost = 0;
	costs[from].from = 0;

	/*
	 * Loop until finished
	 */

	FacilityID city;
	GameTicks cost;

	while(queue.get(city, cost))
	{
		Facility* f = facilities.get(city);
	
		UBYTE jCount = f->railCount;
		if(jCount)
		{
			RailSectionID rCount = f->railSection;

			while(jCount--)
			{
				FacilityID connection = railways.get(rCount++);
				Facility* cf = facilities.get(connection);

				/*
				 * Station belongs to enemy?
				 */

				if(onOtherSide(side, cf->side))
					continue;

				/*
				 * Work out arrival time and see if we have found a quicker way
				 * NB: This should be calculated based on section length
				 */

				GameTicks arrival = cost + railDistance(f, cf);

				if(costs[connection].cost < arrival)
				{
					continue;
				}

				costs[connection].cost = arrival;
				costs[connection].from = city;

				/*
				 * Is it where we're trying to get to?
				 */

				if(connection == to)
				{
				}
				else if(cf->railCount == 1)
				{		// Its only got 1 connection, so can't get anywhere
				}
				else // It's not where we're going so add it to queue
				{
					queue.put(connection, arrival);
				}
			}
		}
	}

	/*
	 * We've finished the search and have a table of arrival times and routes
	 */

	FacilityID result;

	if(costs[to].from == NoFacility)
	{
		result = NoFacility;
	}
	else
	{
		/*
		 * Trace back to find first route
		 */

		result = to;
		while( (costs[result].from != from) && (costs[result].from != NoFacility) )
		{
			result = costs[result].from;

		}

	}

	/*
	 * Return the array to the user if he asked for it
	 * But it is up to him to delete it when finished!
	 */

	if(routes)
		routes->route = costs;
	else
		delete[] costs;

	return result;
}

#ifdef CREATE_WORLD

/*
 * Check that railway structures are correct
 * i.e. each facility has a 2 way link
 */

void CampaignWorld::checkRails()
{
	if(logFile && logFile->isShown(30))
		*logFile << "\n\nChecking rail links" << endl;

	int cityCount = 0;
	while(cityCount < facilities.entries())
	{
		Facility* f = facilities.get(cityCount);
		UBYTE jCount = f->railCount;

		if(logFile && logFile->isShown(30))
			*logFile << "Facility " << cityCount << "->" << (int)jCount;

		if(jCount)
		{
			if(logFile && logFile->isShown(30))
				*logFile << "(";

			RailSectionID rCount = f->railSection;

			while(jCount--)
			{
				if(logFile && logFile->isShown(30))
				{
					*logFile << (int)railways.get(rCount);
					if(jCount)
						*logFile << ",";
				}

				Facility* connect = facilities.get(railways.get(rCount++));
				int cjCount = connect->railCount;
				int matched = 0;
				RailSectionID crCount = connect->railSection;
				while(cjCount--)
				{
					if(railways.get(crCount++) == cityCount)
						matched++;
				}
				if(matched == 0)
				{
					if(logFile && logFile->isShown(30))
						*logFile << "!!!No Match!!!";
				}
				else if(matched > 1)
				{
					if(logFile && logFile->isShown(30))
						*logFile << "!!!Matched " << matched << " times!!!";
				}

			}

			if(logFile && logFile->isShown(30))
				*logFile << ")";
		}

		if(logFile && logFile->isShown(30))
			*logFile << endl;



		cityCount++;
	}

	if(logFile && logFile->isShown(30))
		*logFile << "\n\n\n\n" << endl;
}


#endif		// CREATE_WORLD

/*
 * Display a rail route on the map.
 *
 * Doesn't display start and end point, but does draw lines between them
 * Only junctions are displayed.
 */

void RailRouteInfo::drawRoute(MapWindow* map, Region* bm, FacilityList& facilities, FacilityID from, FacilityID to) const
{
	if(route && (from != to))
	{
		/*
		 * Initialise values to Destination point
		 */

		Facility* f = facilities.get(to);
		Point prev = map->locationToPixel(f->location);
		FacilityID id = route[to].from;

		while(id != NoFacility)
		{
			f = facilities.get(id);

			Point dest = map->locationToPixel(f->location);

			Colour col;
#if 0
			if(f->side == SIDE_CSA)
				col = CSA_HColour;
			else if(f->side == SIDE_USA)
				col = USA_HColour;
			else
				col = Neutral_HColour;
#else
			col = White;
#endif
			bm->line(dest, prev, col);

			if(id == from)
				break;

			if(f->railCount > 2)
				bm->frame(dest.x - 1, dest.y - 1, 3, 3, col);

			id = route[id].from;
			prev = dest;
		}
	}
}

#endif	// CAMPEDIT

/*===================================================================
 * Platforms
 */

/*
 * Platform Queue Constructor
 */

RailNetwork::RailNetwork()
{
	trains = 0;
	platforms = 0;
	// lastTime = 0;

	lastStart = NoFacility;
	lastDestination = NoFacility;
	lastStation = NoFacility;
}

/*
 * Platform Queue Destructor
 */

RailNetwork::~RailNetwork()
{
	clear();
}

void RailNetwork::clear()
{
	Platform* p = platforms;
	while(p)
	{
		Platform* p1 = p;
		p = p->next;
		delete p1;
	}
	platforms = 0;

	Train* t = trains;
	while(t)
	{
		Train* t1 = t;
		t = t->next;
		delete t1;
	}
	trains = 0;
}

#ifndef CAMPEDIT

/*
 * Add an item to the platform queue
 */

void RailNetwork::add(Unit* unit, FacilityID from, FacilityID to)
{
	ASSERT(unit != 0);

	if( (from == NoFacility) || (to == NoFacility))
	{
#ifdef DEBUG
		railLog.printf("Can't add %s to railnet because NoFacility %d,%d",
			unit->getName(True),
			(int) from,
			(int) to);
#endif
		unit->setMoveMode(MoveMode_Marching);
		unit->realOrders.how = ORDER_Land;
		return;
	}

	/*
	 * Can't have a platform if there is no rail capacity
	 */

	Facility* f = campaign->world->facilities[from];
	if(!f->railCapacity)
	{
#ifdef DEBUG
		railLog.printf("%s has no rail capacity", f->getNameNotNull());
#endif

		unit->setOrderNow(Order(ORDER_Stand, ORDER_Land, unit->location));
		unit->newOrder = True;
		unit->setMoveMode(MoveMode_Marching);
		return;
	}

#ifdef DEBUG
	railLog.printf("%s added to railnet at %s",
		unit->getName(True), f->getNameNotNull());
#endif

	/*
	 * Determine the next station en-route
	 * (This can be speeded up if there are likely to be many units
	 *  all going to the same destination, by also storing a long term
	 *  destination in the platform structure, and doing an initial
	 *  scan through platforms).
	 */

	FacilityID nextStation;
	if((from == lastStart) && (to == lastDestination))
		nextStation = lastStation;
	else
	{
		nextStation = campaign->world->findRailRoute(from, to, unit->getSide());
		if(nextStation == NoFacility)
			nextStation = campaign->world->findRailRoute(from, to, SIDE_None);

		lastStart = from;
		lastDestination = to;
		lastStation = nextStation;
	}

	if(nextStation == NoFacility)
	{
#ifdef DEBUG
		Facility* f1 = campaign->world->facilities[from];
		Facility* f2 = campaign->world->facilities[to];

		railLog.printf("There is no route between %s and %s",
			f1->getNameNotNull(),
			f2->getNameNotNull());
#endif
		return;
	}

	/*
	 * Find out if there is already a platform and create it if not
	 */

	Platform* p = platforms;
	while(p)
	{
		if((p->station == from) && (p->destination == nextStation))
			break;

		p = p->next;
	}

	if(!p)
	{
		p = new Platform(from, nextStation, unit->getSide());
		p->next = platforms;
		platforms = p;
	}

	/*
	 * Add unit to the end of the queue
	 */

	ASSERT(unit != 0);
	p->waiting.append(unit);

	/*
	 * Let the momement code know that it is waiting at a platform
	 */

	unit->setMoveMode(MoveMode_Waiting);

#ifdef DEBUG
	{
		Facility* f;

		f = campaign->world->facilities[from];
		const char* nameFrom = f->getNameNotNull();
		f = campaign->world->facilities[to];
		const char* nameTo = f->getNameNotNull();
		f = campaign->world->facilities[nextStation];
		const char* nameVia = f->getNameNotNull();

		campaign->msgArea->printf("%s is waiting for a train\rat %s to %s via %s",
			unit->getName(True), nameFrom, nameTo, nameVia);
		railLog.printf("%s is waiting for a train\rat %s to %s via %s",
			unit->getName(True), nameFrom, nameTo, nameVia);
	}
#endif
}

// Patch... 7th February 1996
/*
 * Remove a unit from the rail network
 * (e.g. he might have been ordered to move elsewhere)
 */

void RailNetwork::removeUnit(Unit* unit)
{
	/*
	 * Search each platform queue for unit and then remove it.
	 */

	Train* train = trains;
	Train* lTrain = 0;
	while(train)
	{
		PtrDListIter<Unit> iter = train->passengers;
		while(++iter)
		{
			Unit* u = iter.current();

			if(u == unit)
			{
				iter.remove();

				if(train->passengers.entries() == 0)
				{
					if(lTrain)
						lTrain->next = train->next;
					else
						trains = train->next;

					delete train;
				}

				return;
			}
		}

		lTrain = train;
		train = train->next;
	}

	/*
	 * Search each train queue for unit and remove it
	 */

	Platform* platform = platforms;
	Platform* lPlatform = 0;
	while(platform)
	{
		PtrDListIter<Unit> iter = platform->waiting;

		while(++iter)
		{
			Unit* u = iter.current();

			if(u == unit)
			{
				iter.remove();

				if(platform->waiting.entries() == 0)
				{
					if(lPlatform)
						lPlatform->next = platform->next;
					else
						platforms = platform->next;

					delete platform;
				}

				return;
			}
		}

		lPlatform = platform;
		platform = platform->next;
	}

}
//.... end of patch

void RailNetwork::process(const TimeBase& t)
{
	/*
	 * Process Railways once every 10 minutes
	 */

	const GameTicks RailRate = GameTicksPerMinute * 10;

	GameTicks elapsed;
	TimeBase now = t;

	if(!lastTime)
	{
		lastTime = now - RailRate;
		elapsed = RailRate;
	}
	else
		elapsed = GameTicks(now - lastTime);

	while(elapsed >= RailRate)
	{
		elapsed -= RailRate;
		lastTime += RailRate;

		/*
		 * Clear the platform routing (situation changes with time!)
		 */

		lastStart = NoFacility;
		lastDestination = NoFacility;

		/*
		 * Go through each platform
		 *   IF spare railhead capacity or there is non-full train waiting
		 *     Create a train and load it up with units
		 *     Delete platform if empty
		 */

		Platform* platform = platforms;
		Platform* last = 0;
		while(platform)
		{
			Facility* f = campaign->world->facilities.get(platform->station);

			Boolean trainFull = False;
			Boolean noTrains = False;

			while(!noTrains && platform->waiting.entries())
			{
				/*
				 * Find if there is trains already loading
				 */

				Train* train = 0;

				if(!trainFull)
				{
					Train* tr = trains;
					while(tr)
					{
						if( (tr->mode == Train::TR_Loading) &&
						 	(tr->lastStation == platform->station) &&
						 	(tr->nextStation == platform->destination) &&
							tr->spaces)
						{
							train = tr;
							break;
						}

						tr = tr->next;
					}
				}

				/*
				 * If no train was found then try to make a new one
				 */

				if(!train && f->freeRailCapacity)
				{
					/*
					 * Create a train and put into loading mode
					 */

					train = makeTrain(platform, now);
					trainFull = False;
				}
				else
				{
					/*
					 * There are no trains available, so break out of the loop
					 */

					noTrains = True;
				}

				if(train)
				{

					/*
					 * We've got a train... load some people onto it
					 *
					 * This should split up the units into brigades
					 * and assign each seperately
					 *
					 * But to get things working, I'm loading the whole
					 * lot onto a train at once!
					 */
	
					/*
					 * Get the 1st waiting unit
					 */

					while(!trainFull && platform->waiting.entries())
					{
						Unit* unit = platform->waiting.find();

						// Patch 23Jan96
						ASSERT(unit != 0);

						// Patch 1stFeb97
						// Prevent endless loop

						if(unit == 0)
						{
							platform->waiting.get();
#ifdef PATCHLOG
							patchLog.printf("NULL unit removed from platform at %s for %s",
								(const char*) campaign->world->facilities[platform->station].getNameNotNull(),
								(const char*) campaign->world->facilities[platform->destination].getNameNotNull() );
#endif
						}

						/*
						 * While there is space on the train
						 * pull out each brigade and put it on the train
						 * Move a commander with its last unit
						 * If the entire unit has been added then remove it
						 * from the platform queue
						 */

						Unit* u = unit;

						/*
						 * Go down and find lowest non-independant unit
						 * down 1st branch that will fit on a train!
						 */

						UnitInfo info(True, False);

						while(u)
						{
							info.clear();
							u->getInfo(info);
							if(info.strength <= train->spaces)
							{
								break;		// There's room!
							}
							else		// Step down a level
							{
								if(u->rank < Rank_Brigade)
								{
									Unit* u1 = u->child;
			
									// while(u1 && u1->isCampaignReallyIndependant())
									while(u1 && (u1->getCampaignAttachMode() != Attached))
										u1 = u1->sister;

									if(u1)
										u = u1;
									else
									{
										trainFull = True;
										break;
									}
								}
								else		// If already at brigade then there's no room
								{
									trainFull = True;
									break;
								}
							}
						}

						// Patch 23Jan96
						/*
						 * Added u to check, because it is possible for above
						 * logic to come out with u==0 and add a ghost passenger
						 */

						if(u && !trainFull)
						{
							if(u->getCampaignAttachMode() == Attached)
								u->setCampaignAttachMode(Joining);

							train->passengers.append(u);
							u->setMoveMode(MoveMode_Transport);
							train->spaces -= info.strength;

							/*
							 * If everyone has fitted on the train
							 * then remove from the platform queue
							 */

							if(u == unit)
							{
								platform->waiting.get();
								// u = 0;	// Get out the loop
#ifdef DEBUG
								Facility * f;

								f = campaign->world->facilities[platform->station];
								const char* nameFrom = f->getNameNotNull();
								f = campaign->world->facilities[platform->destination];
								const char* nameTo = f->getNameNotNull();

								TimeInfo tm = train->arrival;

								campaign->msgArea->printf("%s has boarded a train\rat %s to %s, departing at %d:%02d:%02d",
									unit->getName(True), nameFrom, nameTo,
									tm.hour, tm.minute, tm.second);
								railLog.printf("%s has boarded a train\rat %s to %s, departing at %d:%02d:%02d",
									unit->getName(True), nameFrom, nameTo,
									tm.hour, tm.minute, tm.second);
#endif	// DEBUG
				  			}	// ENDIF(u==unit)
				  		}	// ENDIF(!trainFull)
					}	// While !trainfull AND queue not empty
				}	// ENDIF train

			}	// while platform is not empty and there are trains

			/*
			 * If the platform is now empty then remove it
			 */

			Platform* next = platform->next;

			if(!platform->waiting.entries())
			{
				if(last)
					last->next = next;
				else
					platforms = next;
				delete platform;
			}
			else
				last = platform;

			platform = next;
		}	// while there are more platforms in the list

		/*
		 * Process Trains
		 */

		Train* train = trains;
		Train* lastTrain = 0;
		while(train)
		{
			Train* nextTrain = train->next;		// Get this now, because it could be deleted

			/*
			 * Move the train and delete it if neccessary
			 */

			if(train->moveTrain(this, now))
			{
				if(lastTrain)
					lastTrain->next = nextTrain;
				else
					trains = nextTrain;

				train->returnHome();
				delete train;
			}
			else
				lastTrain = train;

			train = nextTrain;
		}


	}	// for each Hour
}


Train* RailNetwork::makeTrain(Platform* where, const TimeBase& now)
{
	/*
	 * Create a train
	 */

	Train* train = new Train(where->station, where->destination, where->side, now);

	/*
	 * Add it to the linked list
	 */

	train->next = trains;
	trains = train;

#ifdef DEBUG
	Facility* f = campaign->world->facilities[where->station];
	railLog.printf("Train created at %s", f->getNameNotNull());
#endif

	return train;
}

#endif	// !CAMPEDIT

/*===================================================================
 * Platforms
 */

/*
 * Construct a platform
 */

Platform::Platform()
{
	next = 0;
}

#if !defined(CAMPEDIT)

Platform::Platform(FacilityID start, FacilityID end, Side owner)
{
	next = 0;
	station = start;
	destination = end;
	side = owner;
}
#endif	// !CAMPEDIT

Platform::~Platform()
{
}

/*===================================================================
 * Trains
 */

Train::Train()
{
	next = 0;
}

Train::Train(FacilityID home, FacilityID destination, Side side, const TimeBase& now)
{
	next = 0;
	mode = TR_Loading;
	Train::side = side;
	arrival = now + GameTicksPerHour;	// Wait for an hour
	spaces = 10000;		// Allow each rail capacity to carry a whole brigade

	owner = home;
	lastStation = home;
	nextStation = destination;

	Facility* f = campaign->world->facilities.get(home);
	f->freeRailCapacity--;
}

Train::~Train()
{
}

#if !defined(CAMPEDIT)

void Train::returnHome()
{
	Facility* home = campaign->world->facilities.get(owner);
	home->freeRailCapacity++;
}

void Train::moveToNextStation()
{
 	/*
 	 * Get time to move between stations
 	 */

	GameTicks travelTime = railDistance(lastStation, nextStation);
	arrival += travelTime;

#ifdef DEBUG
	Facility* f;

	f = campaign->world->facilities[lastStation];
	const char* nameFrom = f->getNameNotNull();
	f = campaign->world->facilities[nextStation];
	const char* nameTo = f->getNameNotNull();

	TimeInfo tm = arrival;

	campaign->msgArea->printf("Train at %s is moving to %s\rArrival at %d:%02d:%02d",
		nameFrom, nameTo,
		tm.hour, tm.minute, tm.second);

	railLog.printf("Train at %s moving to %s", nameFrom, nameTo);
	railLog.printf("Arrival at %d:%02d:%02d", tm.hour, tm.minute, tm.second);
#endif
}

/*
 * Move a train
 * return True if it must be returned to home
 */

Boolean Train::moveTrain(RailNetwork* net, const TimeBase& now)
{
	/*
	 * Do something if the timer has finished
	 */

	if(now >= arrival)
	{
		/*
		 * If it was loading then start moving
		 */

		if(mode == TR_Loading)
		{
			/*
			 * We could I suppose check whether the next station is
			 * enemy controlled or not?
			 */

			mode = TR_Moving;

			moveToNextStation();
			return False;
		}

		/*
		 * If it was moving then process getting to a station
		 */

		else		// Assume Moving
		{
			Facility* f = campaign->world->facilities.get(nextStation);
			Boolean unloadAll = False;	// Set to empty the train
			Boolean onFoot = False;		// clear to wait for another train
			Location l = f->location;

			/*
			 * Update passengers locations
			 */

			PtrDListIter<Unit> iter = passengers;
			while(++iter)
			{
				Unit* unit = iter.current();

				// Patch 23Jan96
				ASSERT(unit != 0);
				if(unit != 0)
				{
					unit->setLocation(f->location);
					unit->realOrders.onFacility = nextStation;
					unit->givenOrders.onFacility = nextStation;
				}
			}

			/*
			 * If this station is enemy controlled
			 * then everyone must get off a few miles away from
			 * the station.
			 */

			if(onOtherSide(side, f->side))
			{
				unloadAll = True;
				onFoot = True;

				/*
				 * Set up a location a few miles away from the station
				 */

				Facility* f1 = campaign->world->facilities.get(lastStation);

				const Distance GetOffDistance = Mile(10);

				Distance d = distanceLocation(f->location, f1->location);
				if(d < GetOffDistance)
					l = (f->location + f1->location) / 2;		// Midpoint
				else
				{
					Wangle a = directionLocation(f->location, f1->location);

					l.x = f->location.x + mcos(GetOffDistance, a);
					l.y = f->location.y + msin(GetOffDistance, a);
				}

#ifdef DEBUG
				railLog.printf("%s is enemy controlled.  Distance from %s is %d miles",
					f->getNameNotNull(),
					f1->getNameNotNull(),
					unitToMile(d));
#endif
				lastStation = nextStation;

			}

			/*
			 * If it doesn't have 2 connections then unload everyone
			 * (end of line).
			 */

			else
			if(f->railCount == 2)
			{
				/*
				 * Set up for next section
				 * There are only 2 junctions, so get the one that we havn't
				 * just come from!
				 */

				FacilityID nextStep = campaign->world->railways.get(f->railSection);
				if(nextStep == lastStation)
					nextStep = campaign->world->railways.get(f->railSection + 1);

				/*
				 * If next station is enemy controlled then unload everyone
				 */

				Facility* f1 = campaign->world->facilities.get(nextStep);
				
#if 0
				if(onOtherSide(side, f1->side))
				{
					unloadAll = True;
					onFoot = True;
				}
				else
#endif
				{
					/*
					 * OR If anyone on the train wants to get off then unload
					 */

					onFoot = True;
				}

				/*
				 * Set stations for next segment
				 */

				lastStation = nextStation;
				nextStation = nextStep;
			}
			else
			{
				/*
				 * Unload everyone if there are not 2 connection
				 * because it is either the end of a line or
				 * they must change trains
				 */

				lastStation = nextStation;
				unloadAll = True;
			}

	 		net->unloadPassengers(unloadAll, onFoot, this, l);

			/*
			 * If train is empty then return to home
			 */

			if(!passengers.entries())
			{
				return True;
			}

			else

			/*
			 * Otherwise get moving to next station
			 */

			{
				moveToNextStation();
				return False;
			}

		}

	}
	else
	{	// Show intermediate location

		if(mode != TR_Loading)
		{
			if((nextStation != NoFacility) && (lastStation != NoFacility))
			{
				Facility* f1 = campaign->world->facilities[lastStation];
				Facility* f2 = campaign->world->facilities[nextStation];

				Location l1 = f1->location;
				Location l2 = f2->location;

				int timeLeft = (arrival - now) / GameTicksPerMinute;
				int totalTime = railDistance(f1, f2) / GameTicksPerMinute;

				if(timeLeft > totalTime)
					timeLeft = totalTime;

				if(totalTime)
				{
					Location midPoint;

					// These calculations are abit complex so as to keep numbers manageable sizes

					int delta = (l1.x - l2.x) >> 16;
					delta = (delta * timeLeft) / totalTime;
					midPoint.x = l2.x + (delta << 16);

					delta = (l1.y - l2.y) >> 16;
					delta = (delta * timeLeft) / totalTime;
					midPoint.y = l2.y + (delta << 16);

					PtrDListIter<Unit> iter = passengers;
					while(++iter)
					{
						Unit* unit = iter.current();

						// Patch 23Jan96
						ASSERT(unit != 0);
						if(unit != 0)
							unit->setLocation(midPoint);
					}
				}
			}
		}
	}


	return False;
}

#ifdef DEBUG
const char* boolStr(Boolean b)
{
	if(b)
		return "True";
	else
		return "False";
}
#endif

/*
 * Unload passengers from a train
 *
 * IF all is True then ALL passengers are removed
 * otherwise only those whose destination is the trains location
 * will disembark
 *
 * IF foot is TRUE then they will get off the rail network completely
 * at the given location otherwise they will wait for a new train
 */

void RailNetwork::unloadPassengers(Boolean all, Boolean foot, Train* train, const Location& where)
{
	Facility* f = campaign->world->facilities.get(train->lastStation);

#ifdef DEBUG
	railLog.printf("unloadPassengers(all=%s,foot=%s) at %s",
		boolStr(all),
		boolStr(foot),
		f->getNameNotNull());
#endif

	PtrDListIter<Unit> iter = train->passengers;
	while(++iter)
	{
		Unit* unit = iter.current();

		// Patch 23Jan96
		ASSERT(unit != 0);
		if(unit)
		{
			if(all || (unit->realOrders.offFacility == train->lastStation) || (unit->realOrders.offFacility == NoFacility))
			{
				iter.remove();			// Remove current item

				/*
			 	 * Adjust train's space if only some getting off
			 	 */

				// ... TBW ...

#ifdef DEBUG
				railLog.printf("unloading %s", unit->getName(True));

				if(unit->realOrders.offFacility == NoFacility)
				{
					railLog.printf("offFacility=NoFacility");
				}
				else
				{
					Facility* offF = campaign->world->facilities[unit->realOrders.offFacility];
					railLog.printf("offFacility=%s", offF->getNameNotNull());
				}
#endif

				if(foot || (unit->realOrders.offFacility == train->lastStation) || (unit->realOrders.offFacility == NoFacility))
				{
#if 0
					/*
				 	* Get them walking to their real destination
				 	*/

					unit->setMoveMode(MoveMode_Marching);

					/*
				 	* 	If line below is included will get infinite loop. CJW
				 	*/

					// unit->newOrder = True;
					unit->realOrders.how = ORDER_Land;

					unit->givenOrders.how = ORDER_Land;
					unit->givenOrders.destination = unit->realOrders.destination;
#else
					if(foot)
					{
						unit->setOrderNow(Order(ORDER_Advance, ORDER_Land, f->location));
						unit->setLocation(where);
					}
					else
						unit->setOrderNow(Order(unit->givenOrders.mode, ORDER_Land, unit->realOrders.destination));
					unit->setMoveMode(MoveMode_Marching);
#endif
				}
				else
				{
					add(unit, train->lastStation, unit->realOrders.offFacility);
				}
			}
		}
	}
}

#endif	// !CAMPEDIT


