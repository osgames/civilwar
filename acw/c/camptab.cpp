/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Tables and editable values for the Campaign Game
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "camptab.h"

/*
 * Command Ranges
 */

Distance campaignCommandRange[] = {
	Mile(500),			// President has range over all america
	Mile(20),			// Army
	Mile(10),			// Corps
	Mile(5),				// Division has 5 mile command radius
	Mile(2),				// Brigade
};

/*
 * Effect of Orders on Speed as a percentage
 *
 * Also takes into account that they wouldn't march solidly for
 * the entire day, thus typically the speed is halved.
 */

UBYTE orderSpeedEffect[] = {
	50,	// ORDER_Advance
	45,	// ORDER_CautiousAdvance
	70,	// ORDER_Attack

	50,	// ORDER_Stand
	50,	// ORDER_Hold
	50,	// ORDER_Defend

	50,	// ORDER_Withdraw
	50,	// ORDER_Retreat
	70,	// ORDER_HastyRetreat
};

/*
 * Effect of Terrain on speed
 * 0 means impassable
 */

#if 0
UBYTE terrainSpeedEffect[] = {
	0,			// CT_Unused0
	0,			// CT_Sea
	0,			// CT_Coastal
	50,		// CT_River
	85,		// CT_Stream
	40,		// CT_Swamp
	80,		// CT_Wooded
	100,		// CT_Farmland
	80,		// CT_Mountain
	70,		// CT_Rough
	0,			// CT_Impassable
	0,			// CT_Unused11
	0,			// CT_Unused12
	0,			// CT_Unused13
	0,			// CT_Unused14
	0,			// CT_Unused15
};
#else
UBYTE terrainSpeedEffect[] = {
	0,			// CT_Unused0
	0,			// CT_Sea
	0,			// CT_Coastal
	10,		// CT_River
	15,		// CT_Stream
	30,		// CT_Swamp
	70,		// CT_Wooded
	100,		// CT_Farmland
	60,		// CT_Mountain
	70,		// CT_Rough
	0,			// CT_Impassable
	0,			// CT_Unused11
	0,			// CT_Unused12
	0,			// CT_Unused13
	0,			// CT_Unused14
	0,			// CT_Unused15
};

UBYTE terrainRouteCost[] = {
	0,			// CT_Unused0
	0,			// CT_Sea
	0,			// CT_Coastal
	160/10,		// CT_River
	160/15,		// CT_Stream
	160/30,		// CT_Swamp
	160/70,		// CT_Wooded
	160/100,		// CT_Farmland
	160/60,		// CT_Mountain
	160/70,		// CT_Rough
	0,			// CT_Impassable
	0,			// CT_Unused11
	0,			// CT_Unused12
	0,			// CT_Unused13
	0,			// CT_Unused14
	0,			// CT_Unused15
};
#endif

/*
 * Distance that a brigade can be from a facility to trigger a battle
 */

// Distance cityBattleDistance = Mile(8);
Distance cityBattleDistance = Mile(4);

/*
 * Radius of a city for a unit to be heading towards it
 * i.e. if a brigade will pass within this distance of the city center
 *      then it is treated as passing through it.
 */

Distance cityWidth = Mile(2);

/*
 * Distance that brigades try to keep between themselves within their division
 */

Distance brigadeSideSpacing = Mile(2);
Distance brigadeFrontSpacing = Mile(2);
 
/*
 * Distance that brigades must be to trigger a battle
 */

Distance brigadeBattleDistance = Mile(8);

/*
 * Width of a brigade used to calculate whether they are moving towards each other
 * i.e. if a brigade will pass within this distance of the brigade center
 *      then it is treated as moving towards it.
 */

Distance brigadeWidth = Mile(2);

/*
 * Distance that a conflict can be from the centre of an existing battlefield
 * to be considered part of the same battle
 */

Distance sameBattleDistance = Mile(4);

/*
 * Distance that a unit can be from battlefield centre to be sucked into
 * the battle.
 */

Distance involvedBattleDistance = Mile(8);

/*
 * Speed of a Merchant naval boat ferrying troops in miles per hour
 */

UBYTE ferrySpeed = 20;
UBYTE seaSpeed = 75;  		// Time speed up percentage for being at sea
UBYTE riverSpeed = 150;		// Time slow down percentage for being in a river

/*
 * Naval Look up tables
 */

UBYTE navalCombat[] = {
	3, 5, 1, 2
};

UBYTE navalBombardment[] = {
	3, 3, 1, 2
};

UBYTE navalSpeeds[] = {	// In MPH
	15, 5, 10, 5
};

/*
 * Mobilising times
 *
 * Each entry has:
 *		(Human, Horses, Food, Materials), Days, multi, Description
 *
 * A full city will have 16383 resources in it
 */

MobiliseCost buildCosts[] = {

	/*
	 * Infantry
	 */

	MobiliseCost(ResourceSet( 200,   0,   0, 400),  6*7, True, True, True, False, Simple,   Simple,   Simple,   lge_RegularInfantry),	//	Mob_InfantryRegular,
	MobiliseCost(ResourceSet( 200,   0,   0, 200),  4*7, True, True, True, False, Average,  Simple,   Simple,   lge_MilitiaInfantry),	//	Mob_InfantryMilitia,
	MobiliseCost(ResourceSet( 200,   0,   0, 800),  8*7, True, True, True, False, Complex1, Simple,   Simple,   lge_Sharpshooter),		//	Mob_InfantrySharpshooter,
	MobiliseCost(ResourceSet( 100,  20,   0, 300), 12*7, True, True, True, False, Complex2, Simple,   Simple,   lge_Engineer),				//	Mob_Engineer,

	/*
	 * Cavalry
	 */

	MobiliseCost(ResourceSet( 200, 200,   0,2400), 16*7, True, True, True, False, Simple,   Simple,   Simple,   lge_RegularCavalry),				//	Mob_cavalryRegular,
	MobiliseCost(ResourceSet( 200, 200,   0,1600),  8*7, True, True, True, False, Average,  Simple,   Simple,   lge_MilitiaCavalry),				//	Mob_cavalryMilitia,
	MobiliseCost(ResourceSet( 200, 200,   0,4000), 24*7, True, True, True, False, Complex1, Simple,   Simple,   lge_RegularMountedInfantry),	//	Mob_cavalryRegularMounted,
	MobiliseCost(ResourceSet( 200, 200,   0,3600), 20*7, True, True, True, False, Complex2, Simple,   Simple,   lge_MilitiaMountedInfantry),	//	Mob_cavalryMilitiaMounted,

	/*
	 * Artillery
	 */

	MobiliseCost(ResourceSet(  20,  20,   0, 300),  8*7, True, True, True, False, Simple,   Simple,   Simple,   lge_SmoothboreArtillery),	//	Mob_ArtillerySmoothbore,
	MobiliseCost(ResourceSet(  20,  20,   0, 200),  8*7, True, True, True, False, Average,  Simple,   Simple,   lge_LightArtillery), 		//	Mob_ArtilleryLight,
	MobiliseCost(ResourceSet(  20,  20,   0, 400), 12*7, True, True, True, False, Complex1, Simple,   Simple,   lge_RifledArtillery),		//	Mob_ArtilleryRifled,
	MobiliseCost(ResourceSet( 100,   0,   0,1000), 20*7, True, True, True, False, Complex2, Simple,   Simple,   lge_SiegeArtillery), 		//	Mob_ArtillerySiege,

	/*
	 * Naval
	 */

	MobiliseCost(ResourceSet(  40,   0,   0,1600), 16*7, True, False, True, False, Simple,   Simple,   Simple,   lge_NavalUnit),				//	Mob_Naval,
	MobiliseCost(ResourceSet(  40,   0,   0,2400), 18*7, True, False, True, False, Complex2, Simple,   Simple,   lge_NavalIronClad),		//	Mob_NavalIronClad,
	MobiliseCost(ResourceSet(  20,   0,   0,1000),  8*7, True, False, True, False, Average,  Simple,   Simple,   lge_Riverine),					//	Mob_Riverine,
	MobiliseCost(ResourceSet(  20,   0,   0,1600), 12*7, True, False, True, False, Complex1, Simple,   Simple,   lge_RiverineIronClad),	//	Mob_RiverineIronClad,

	/*
	 * Supplies
	 */

	MobiliseCost(ResourceSet(  20, 100, 160,  40),  2*7, False, False, False, False, Simple, Simple,   Complex2,   lge_SupplyWagon),		//	Mob_SupplyWagon,

	/*
	 * Facilities
	 */

	// MobiliseCost(ResourceSet(  20,   0,   0, 800),  4*7, False, False, False, False, Simple,   Simple,   Simple,   lge_Fortification),			//	Mob_Fortification,
	MobiliseCost(ResourceSet(  20,   0,   0, 800),  4*7, False, False, False, True, Simple,   Simple,   Simple,   lge_Fortification),			//	Mob_Fortification,
	MobiliseCost(ResourceSet(  20, 300,   0, 800),  4*7, False, False, False, True,  Simple,   Simple,   Complex2, lge_SupplyDepot),				//	Mob_SupplyDepot,
	MobiliseCost(ResourceSet(  20,   0,   0, 800),  2*7, False, False, True,  True,  Simple,   Simple,   Simple,   lge_RecruitmentCentre),		//	Mob_Recruitment,
	MobiliseCost(ResourceSet(  20, 100, 200, 800),  4*7, False, False, True,  True,  Simple,   Complex1, Simple,   lge_TrainingCamp),			//	Mob_Training,
	// MobiliseCost(ResourceSet(  20,   0,   0, 800),  4*7, False, False, False, False, Simple,   Average,  Simple,   lge_RailheadCapacity),		//	Mob_RailHead,
	MobiliseCost(ResourceSet(  20,   0,   0, 800),  4*7, False, False, False, True, Simple,   Average,  Simple,   lge_RailheadCapacity),		//	Mob_RailHead,
	MobiliseCost(ResourceSet( 100,  20,   0, 400),  8*7, False, False, True,  True,  Complex3, Simple,   Simple,   lge_RailEngineer),			//	Mob_RailEngineer,
	MobiliseCost(ResourceSet(  20,   0,   0, 800),  4*7, False, False, True,  True,  Simple,   Complex2, Simple,   lge_Hospital),					//	Mob_Hospital,
	MobiliseCost(ResourceSet(  20,   0,   0, 800),  4*7, False, False, True,  True,  Simple,   Complex3, Simple,   lge_POWCamp),					// Mob_POW,
	MobiliseCost(ResourceSet(   0,   0,   0,1000),  4*7, False, False, True,  False, Complex3, Simple,   Simple,   lge_Blockade),					//	Mob_Blockade,
};


/*
 *  The distance a unit has to be from a facility to be in supply range
 */

// Distance citySupplyRange = Mile( 10 );
Distance citySupplyRange = Mile(30);

Distance CampaignVisibleDistance = Mile( 35 );

/*
 * Table of what resources a type of city will generate
 * each day.
 *
 * This is the amount a maximum sized city will generate.
 * A city of minimum size will only generate 1/64 of this amount.
 */

ResourceSet resourceGeneration[] = {
	ResourceSet(255,  64, 100, 255),		// Industrial
	ResourceSet( 64, 128, 255, 96),		// Agricultural (was 10,128,255,50)
	ResourceSet(150, 100, 200, 150),		// Mixed
	ResourceSet(128, 128, 128, 128)		// Other (Undefined)
};


/*	End of camptab.cpp */
