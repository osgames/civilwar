/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Support Functions for AI
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <iostream.h>

#include "aitests.h"
#include "generals.h"
#include "game.h"

#ifdef DEBUG
#define AITEST_LOG
#endif

#ifdef AITEST_LOG
#include "clog.h"
LogFile aitestLog("aitest.log");
#endif

aitests::aitests()
{
}

aitests::~aitests()
{
}

retValue aitests::Efficiency_Test( General *general, int loyal, int enemy, AttributeLevel radomNum, int additional )
{
#ifdef AITEST_LOG
	aitestLog.printf("EfficiencyTest(%s, %d, %d, %d, %d)",
		general ? general->getName() : "Null",
		loyal,
		enemy,
		radomNum,
		additional);
#endif

	int modratio, remainder;

	retValue alright = FAIL; 

	if ( enemy )
		modratio = loyal / enemy;
	else
		return PASS;

#ifdef AITEST_LOG
	aitestLog.printf("modratio=%d", modratio);
#endif

	if ( loyal )
		remainder = enemy / loyal;
	else
		return FAIL;

#ifdef AITEST_LOG
	aitestLog.printf("remainder=%d", remainder);
#endif

	if ( (modratio == 0) && (remainder >= 4) )
		return FAIL_BADLY;

	const char siz = 255;

	int ga;
	
	if ( general )
		ga = general->ability + additional;
	else
		ga = game->gameRand.getB() + additional;
	
	switch( modratio )
	{
		case 0:
	  	{
			if ( ( remainder == 3 ) && ( ga >= 200 ) )
				alright = PASS;
			else
			if ( ( remainder == 2 ) && ( ( 2 + (ga * ( 26 - ( game->gameRand(11)) ) ) / siz ) >= radomNum ) )
				alright = PASS;
			// if ( remainder >= 2 )
				break;
		}
		
		case 1:
			if ( ( 13 + (ga * 26) / siz ) >= radomNum )
				alright = PASS;
			break;

		case 2:
			if ( ( 51 + (ga * 26) / siz ) >= radomNum )
				alright = PASS;
			break;

		case 3:
			if ( ( 128 + (ga * 128) / siz ) >= radomNum )
				alright = PASS;
			break;

		default:
			alright = PASS;
			break;
	}
	return alright;
}		

retValue aitests::Ability_Test( General *general ) const
{								
	if(!general)
		return PASS;

	/*
	 * Version as per game design!
	 */

	int dice = game->gameRand(100);

	int passLevel = 90 - (70 * general->ability) / MaxAttribute;
	int failLevel = 30 - (25 * general->ability) / MaxAttribute;

	if(dice >= passLevel)
		return PASS;
	else if(dice >= failLevel)
		return FAIL;
	else
		return FAIL_BADLY;
}
 
retValue aitests::Aggression_Test( General *general ) const
{
	const char siz = 100;

	if ( !general ) return PASS;


	/*
	 * Version as per game design!
	 */

	int dice = game->gameRand(100);

	int passLevel = 90 - (70 * general->aggression) / MaxAttribute;
	int failLevel = 60 - (55 * general->aggression) / MaxAttribute;

	if(dice >= passLevel)
		return PASS;
	else if(dice >= failLevel)
		return EQUAL;
	else
		return FAIL;
}

int aitests::Aggressvalue( Unit* loyal )
{
	if(!loyal)
	{
#ifdef AITEST_LOG
		aitestLog.printf("aggressvalue() called with NULL");
#endif

		return game->gameRand(3);
	}


	General* gen = findArmyGeneral( loyal );

	if ( !gen ) 
	{
#ifdef AITEST_LOG
		aitestLog.printf("aggressvalue(%s) has no general", loyal->getName(True));
#endif

		return game->gameRand(3);
	}

 	retValue rv = Aggression_Test( gen );
	
	if ( rv == FAIL ) return 1;
	if ( rv == PASS ) return 5;
	return 3;
}

/*
 *  Find the Army General associated with a given unit;
 *
 * Note that this may result in a NULL general
 */


General* aitests::findArmyGeneral( Unit *un )
{
	General *gen = 0;

 	while(un && (un->getRank() >= Rank_Army))
	{
		if(un->general)
			gen = un->general;
		un = un->superior;
	}

	return gen;
}
