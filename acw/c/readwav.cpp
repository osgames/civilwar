/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	WAV file reader
 *
 * Note that this reads in data as structures, so relies on the
 * architecture of the processor.  This works on an 80x86 based PC.
 * For other architectures, some structure elements will need fiddling
 * with.  Normally I would write my code in a portable manner, but I
 * am in a hurry and havn't the time.  No doubt I will regret this when
 * I come to write a MAC or Amiga version.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

// #include <stdio.h>
#include <string.h>
#include "sndlib.h"
#include "cd_open.h"

/*
 * WAV structures
 */

struct RIFF {
	char rID[4];		// "RIFF"
	ULONG rLen;
};

struct WAV_Block {
	char id[4];			// "WAVE"
};

struct WAV_Format {
	// char fid[4];		// "fmt "
	// ULONG fLen;
	UWORD wFormatTag;
	UWORD nChannels;
	ULONG nSamplesPerSec;
	ULONG nAvgBytesPerSec;
	UWORD nBlockAlign;
	UWORD bitsPerSample;
};

enum WAV_Type {
	WAV_PCM = 1
};

/*
 * Read in a WAV file and set up info in sample class
 * Note that samples are converted into Mono 8 bit samples
 */

int readWAV(const char* fileName, Sample& sample)
{
	int retVal = -1;

	// FILE* fp = FileOpen(fileName, "rb");

	FILE* fp = fopen( getPathAndCheck(fileName ), "rb");

	if(fp)
	{
		/*
		 * Read in the RIFF header
		 */

		RIFF riff;

		if(fread(&riff, sizeof(riff), 1, fp) != 1)
			goto error;
		if(strncmp(riff.rID, "RIFF", sizeof(riff.rID)))
			goto error;

		/*
		 * Read in the WAVE header
		 */

		WAV_Block wavBlock;

		if(fread(&wavBlock, sizeof(wavBlock), 1, fp) != 1)
			goto error;
		if(strncmp(wavBlock.id, "WAVE", sizeof(wavBlock.id)))
			goto error;

		/*
		 * Read in the format chunk
		 */

		if(fread(&riff, sizeof(riff), 1, fp) != 1)
			goto error;
		if(strncmp(riff.rID, "fmt ", sizeof(riff.rID)))
			goto error;

		{
			/*
		 	 * Read in format data
		 	 */

			WAV_Format* wavFmt = (WAV_Format*) new char[riff.rLen];
			if(fread(wavFmt, riff.rLen, 1, fp) != 1)
				goto error1;

			/*
		 	 * Load data chunk header
		 	 */

			if(fread(&riff, sizeof(riff), 1, fp) != 1)
				goto error1;
			if(strncmp(riff.rID, "data", sizeof(riff.rID)))
				goto error1;

			{	// Prevent compiler error about data not being initialised
				/*
		 	 	 * Load data chunk
		 	 	 */

				UBYTE* data = new UBYTE[riff.rLen];
				ULONG realLength = riff.rLen;

				if(fread(data, riff.rLen, 1, fp) != 1)
				{
					delete[] data;
					goto error1;
				}

				/*
		 	 	 * Process data
		 	 	 * If it is in stereo or 16 bit then it needs converting
		 	 	 * to 8 bit
		 	 	 */

				// ... to be written

				if(wavFmt->nBlockAlign != 1)
				{
					Boolean is16 = (wavFmt->bitsPerSample == 16);

					int bytesPerTick = wavFmt->nAvgBytesPerSec / wavFmt->nSamplesPerSec;
					int bytesPerSample = wavFmt->bitsPerSample / 8;
					int samplesPerTick = bytesPerTick / bytesPerSample;

					realLength = riff.rLen / bytesPerTick;

					UBYTE* dest = new UBYTE[realLength];

					ULONG l = realLength;
					UBYTE* dPtr = dest;
					UBYTE* sPtr = data;
					while(l--)
					{
						if(is16)
						{
							UWORD value = 0;

							for(int i = 0; i < samplesPerTick; i++)
							{
								UBYTE low = *sPtr++;
								UBYTE hi = *sPtr++;

								UWORD v1 = ((ULONG) hi * 256 + low) + 0x8000;

								value = (value + v1) / (i + 1);
							}
							*dPtr++ = value / 256;
						}
						else
						{
							UBYTE val = 0;

							for(int i = 0; i < samplesPerTick; i++)
								val = ((UWORD) val + *sPtr++) / (i + 1);

							*dPtr++ = val;
						}

					}

					delete[] data;
					data = dest;
				}

#if 0
				/*
				 * Convert to 8 bit unsigned
				 */

				ULONG l = riff.rLen;
				UBYTE* ad = data;
				while(l--)
				{
					*ad += 0x80;

					ad++;
				}
#endif

				/*
		 	 	 * Fill in data for sample
		 	 	 */

				sample.sampleRate = wavFmt->nSamplesPerSec;
				sample.sample = data;
				sample.length = realLength;
				sample.loopstart = 0;
				sample.loopend = 0;
				sample.mode = 0;
				sample.sampleID = 0;

				retVal = 0;
			}

		error1:
			delete[] wavFmt;
		}
	error:
		fclose(fp);
	}

	return retVal;
}
