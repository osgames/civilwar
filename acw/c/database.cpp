/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
//#pragma warning * 0;
//#pragma warning 389 8;

/*
 *----------------------------------------------------------------------
 * $Id$	testdb.cpp 1.0 1994/07/07 15:48:00 Chris Wall $
 *----------------------------------------------------------------------
 *
 *		This routine handles the main functions and procedure to acess the
 *	a database. It expects any keywords to be in a file called 'keywords.dat'
 *
 *																				  
 *	~s Underlined ( ends with a '~' )
 *	
 *	~b	bold 		  ( 		 "			)
 *
 *	~h	Heading ( drawn in win03, centred both vertically and horizontally.
 *   	Ends with a '~'  )
 *
 *	~j Job ( or rank ) ( One line centred horizontally, ends with a '~' )
 *
 *	~t tab. Adds four spaces.
 *
 *
 *	  @ signifies beginning or end of keyword 
 *
 *  @@ signifies end of chapter
 *
 *	@@@ signifies end of textfile
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *	TESTDB.CPP $
 *
 *----------------------------------------------------------------------
 */

#include "database.h"
#include "datab.h"
#include "db_dplay.h"



void doDatabase()
{
	Dplay disp;

	disp.mainloop();
}


