/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Copyright (C) 1995, Steven Morle-Green, All rights Reserved
 *
 *----------------------------------------------------------------------
 *
 *	Campaign Route Planner
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdlib.h>
#include "route.h"
#include "campaign.h"
#include "campwld.h"
#include "camptab.h"
#include "colours.h"
#include "options.h"
#include "poly2d.h"

#define USE_HEAP
// #define USE_NEW
// #define USE_DELTA

#if defined(USE_HEAP)
#include "membodge.h"
#endif

#define MaxRouteDistance 40		// Only search thoroughly in a 50 square radius

#ifdef DEBUG

#include "timer.h"

LogFile routeLog("route.log");
#endif

/*===================================================================
 * Route Planner Constructor
 */

RoutePlan::RoutePlan()
{
	nodes = 0;
	width = 0;
	height = 0;
#ifdef DEBUG
	timeTaken = timer->getFastCount();
	iterations = 0;
	maxCost = 0;
	dupCount = 0;
#endif
}

void RoutePlan::init()
{
	ASSERT(nodes == 0);

	CampaignTerrain& cTerrain = campaign->world->terrain;

	width = cTerrain.getWidth();
	height = cTerrain.getHeight();
#if defined(USE_HEAP)
	nodes = (RouteNode*) memHeap->allocate(width * height * sizeof(RouteNode));
#else
	nodes = new RouteNode[width * height];
#endif

#ifdef DEBUG
	routeLog.printf("");
	routeLog.printf("Allocating %ld nodes of size %ld", (long) width * height, (long) sizeof(RouteNode));
#endif
	for(int i = 0; i < (width * height); i++)
		nodes[i].init();
}

/*===================================================================
 * Route Planner Destructor
 */

RoutePlan::~RoutePlan()
{
	if(nodes)
	{
#if defined(USE_HEAP)
		memHeap->free(nodes);
#else
		delete[] nodes;
#endif
	}
}

/*
 * Convert direction into order to evaluate directions
 *
 * Build table of directions:
 *    0 1 2
 *    3 4 5
 *    6 7 8
 *
 *      3 1
 *     7	  5
 *     6	  4
 *      2 0
 */

enum OctDir { Left, Down, Right, Up, NoDirection };

static struct DirOffset {
	BYTE x;
	BYTE y;
} directionOffsets[4] = {
	{ -1, 0 },		// Left
	{  0, 1 },		// Down
	{  1, 0 },		// Right
	{  0, -1 },		// Up
};

#ifdef USE_DELTA
static inline OctDir* getDirectionOrder(WORD dX, WORD dY, OctDir& direction, UWORD& delta)
#else
static inline OctDir* getDirectionOrder(WORD dX, WORD dY)
#endif
{
	static OctDir directions[8][4] = {
		{  Down, Right,  Left,    Up },
		{    Up, Right,  Left,  Down },
		{  Down,  Left, Right,    Up },
		{    Up,  Left, Right,  Down },
		{ Right,  Down,    Up,  Left },
		{ Right,    Up,  Down,  Left },
		{  Left,  Down,    Up, Right },
		{  Left,    Up,  Down, Right }
	};

	int lookUp;

	UWORD aX = abs(dX);
	UWORD aY = abs(dY);

	if(aX > aY)
		lookUp = 4;
	else
		lookUp = 0;

	if(sgn(dX) < 0)
		lookUp += 2;

	if(sgn(dY) < 0)
		lookUp += 1;

	OctDir* retVal = directions[lookUp];

#ifdef USE_DELTA
	if(retVal[0] == direction)
	{
		if(aX > aY)
		{
			delta += aY;
			if(delta >= aX)
			{
				delta -= aX;
				lookUp ^= 4;
			}
		}
		else
		{
			delta += aX;
			if(delta >= aY)
			{
				delta -= aY;
				lookUp ^= 4;
			}
		}
		retVal = directions[lookUp];
	}
	else
		delta = 0;

	direction = retVal[0];
#endif

	return retVal;
	// OctDir* dPtr = directions[lookUp];
}

/*=====================================================
 * Queue Structure (a queue is probably more efficient than a stack)
 */

struct RouteQueueItem {
	UWORD x;
	UWORD y;

#ifdef USE_DELTA
	OctDir direction;		// Direction moved to get to this square
	UWORD delta;			// Running remainder
#endif
};

class RouteQueue {
	RouteQueueItem* items;

	int howMany;
	int head;
	int tail;

#ifdef DEBUG
public:
	int usage;
#endif

public:
	RouteQueue(int size)
	{
		items = new RouteQueueItem[size];
		howMany = size;
		head = 0;
		tail = 0;
#ifdef DEBUG
		usage = 0;
#endif
#ifdef DEBUG
		routeLog.printf("Queue items allocated = %d (%d bytes)", size, (int) size * sizeof(RouteQueueItem));
#endif
	}

	~RouteQueue()
	{
		delete[] items;
	}

#ifdef USE_DELTA
	void put(UWORD x, UWORD y, OctDir direction, UWORD delta);
	Boolean get(UWORD &x, UWORD &y, OctDir& direction, UWORD& delta);
#else
	void put(UWORD x, UWORD y);
	Boolean get(UWORD &x, UWORD &y);
#endif
	Boolean isEmpty() { return head == tail; }

};

#ifdef USE_DELTA
void RouteQueue::put(UWORD x, UWORD y, OctDir direction, UWORD delta)
#else
void RouteQueue::put(UWORD x, UWORD y)
#endif
{
	int nextHead = head + 1;
	if(nextHead >= howMany)
		nextHead = 0;

	ASSERT(nextHead != tail);

	if(nextHead == tail)
		return;
		// throw GeneralError("Route queue full (%d allocated)", howMany);

	RouteQueueItem* item = &items[head];

	item->x = x;
	item->y = y;
#ifdef USE_DELTA
	item->direction = direction;
	item->delta = delta;
#endif

	head = nextHead;

#ifdef DEBUG
	int usg;

	if(head > tail)
		usg = head - tail;
	else
		usg = head + howMany - tail;
	if(usg > usage)
		usage = usg;
#endif
}

#ifdef USE_DELTA
Boolean RouteQueue::get(UWORD &x, UWORD &y, OctDir& direction, UWORD& delta)
#else
Boolean RouteQueue::get(UWORD &x, UWORD &y)
#endif
{
	ASSERT(head != tail);		// Queue empty situation

	if(head == tail)
		return False;
	else
	{
		RouteQueueItem* item = &items[tail];

		x = item->x;
		y = item->y;
#ifdef USE_DELTA
		direction = item->direction;
		delta = item->delta;
#endif

		if(++tail >= howMany)
			tail = 0;

		return True;
	}
}


inline void RoutePlan::getBest(RouteCost& best, CampaignTerrainCoord& bestTCord, long x, long y)
{
	if(x < 0)
		return;
	if(y < 0)
		return;
	if(x > (width - 1))
		return;
	if(y > (height - 1))
		return;

	RouteNode* node = getNode(x, y);
	RouteCost nCost = node->getCost();

	if(nCost < best)
	{
		best = nCost;
		bestTCord.x = x;
		bestTCord.y = y;
	}
}

/*===================================================================
 * See if route is blocked
 *
 * Return True if blocked
 */

Boolean isBlocked(const CampaignTerrainCoord& from, const CampaignTerrainCoord& to)
{
	WORD dx = to.x - from.x;
	WORD dy = to.y - from.y;

	if((dx == 0) && (dy == 0))
		return False;

	UWORD aX = abs(dx);
	UWORD aY = abs(dy);

	CampaignTerrain& cTerrain = campaign->world->terrain;

	if(aX > aY)
	{
		CampaignTerrainCoord p;

		if(dx < 0)
		{
			dx = -dx;
			dy = -dy;
			p = to;
		}
		else
			p = from;

		UBYTE effect = terrainRouteCost[cTerrain.getVal(p.x, p.y)];

		if(effect == 0)
			return True;

		WORD sy = sgn(dy);
		UWORD value = aX / 2;

		while(dx--)
		{
			p.x++;

			value += aY;
			if(value >= aX)
			{
				p.y += sy;
				value -= aX;
			}

			effect = terrainRouteCost[cTerrain.getVal(p.x, p.y)];

			if(effect == 0)
				return True;

		}
	}
	else		// Y is major axis
	{
		CampaignTerrainCoord p;

		if(dy < 0)
		{
			dx = -dx;
			dy = -dy;
			p = to;
		}
		else
			p = from;

		UBYTE effect = terrainRouteCost[cTerrain.getVal(p.x, p.y)];

		if(effect == 0)
			return True;

		WORD sx = sgn(dx);
		UWORD value = aY / 2;

		while(dy--)
		{
			p.y++;

			value += aX;
			if(value >= aY)
			{
				p.x += sx;
				value -= aY;
			}

			UBYTE effect = terrainRouteCost[cTerrain.getVal(p.x, p.y)];

			if(effect == 0)
				return True;
		}
	}
	return False;
}

Boolean isDifferentTerrain(const CampaignTerrainCoord& from, const CampaignTerrainCoord& to)
{
	WORD dx = to.x - from.x;
	WORD dy = to.y - from.y;

	if((dx == 0) && (dy == 0))
		return False;

	UWORD aX = abs(dx);
	UWORD aY = abs(dy);

	CampaignTerrain& cTerrain = campaign->world->terrain;

	if(aX > aY)
	{
		CampaignTerrainCoord p;

		if(dx < 0)
		{
			dx = -dx;
			dy = -dy;
			p = to;
		}
		else
			p = from;

		UBYTE startTerrain = terrainRouteCost[cTerrain.getVal(p.x, p.y)];

		WORD sy = sgn(dy);
		UWORD value = aX / 2;

		while(dx--)
		{
			p.x++;

			value += aY;
			if(value >= aX)
			{
				p.y += sy;
				value -= aX;
			}

			UBYTE effect = terrainRouteCost[cTerrain.getVal(p.x, p.y)];

			if(effect != startTerrain)
				return True;

		}
	}
	else		// Y is major axis
	{
		CampaignTerrainCoord p;

		if(dy < 0)
		{
			dx = -dx;
			dy = -dy;
			p = to;
		}
		else
			p = from;

		UBYTE startTerrain = terrainRouteCost[cTerrain.getVal(p.x, p.y)];

		WORD sx = sgn(dx);
		UWORD value = aY / 2;

		while(dy--)
		{
			p.y++;

			value += aX;
			if(value >= aY)
			{
				p.x += sx;
				value -= aY;
			}

			UBYTE effect = terrainRouteCost[cTerrain.getVal(p.x, p.y)];

			if(effect != startTerrain)
				return True;
		}
	}
	return False;
}


/*===================================================================
 * Complex Route Planner
 */


Boolean RoutePlan::complexRoute(const CampaignTerrainCoord& fromTCord, const CampaignTerrainCoord& destTCord)
{
#ifdef DEBUG
	routeLog.printf("-----------------------------");
	routeLog.printf("Invoking complex route finder");
	routeLog.printf("From: %u,%u to %u,%u",
		(unsigned int) fromTCord.x, (unsigned int) fromTCord.y,
		(unsigned int) destTCord.x, (unsigned int) destTCord.y);
#endif

	CampaignTerrain& cTerrain = campaign->world->terrain;

	if(terrainRouteCost[cTerrain.getVal(destTCord.x, destTCord.y)] == 0)
	{
#ifdef DEBUG
		routeLog.printf("Destination is in illegal area");
#endif
		return False;
	}

	/*
	 * Fill with MaxValue
	 */

	init();

	/*
	 * Set up a Queue
	 */

	RouteQueue queue((width + height) * 2);

	UWORD x = fromTCord.x;
	UWORD y = fromTCord.y;
	RouteCost cost = 0;
	RouteNode* node = getNode(x, y);
	RouteCost bestCost = MaxNodeCost;

	/*
	 * Bodge in case units start off in impassable terrain
	 */

	Boolean allowIllegal = False;
	if(terrainRouteCost[cTerrain.getVal(x,y)] == 0)
	{
#ifdef DEBUG
		routeLog.printf("Start is in illegal terrain");
#endif
		allowIllegal = True;
	}

#ifdef USE_DELTA
	queue.put(x, y, NoDirection, 0);
#else
	queue.put(x, y);
#endif
	node->setCost(cost);
	node->setInQueue();

	while(!queue.isEmpty())
	{
#ifdef DEBUG
		iterations++;
#endif

#ifdef USE_DELTA
		OctDir direction;
		UWORD delta;

		queue.get(x, y, direction, delta);
#else
		queue.get(x, y);
#endif

		node = getNode(x, y);
		node->clearInQueue();
		cost = node->getCost();

		if(cost < bestCost)
		{
			/*
			 * Add other nodes in order of direction to destination
			 */

#ifdef USE_DELTA
			OctDir* dPtr = getDirectionOrder(destTCord.x - x, destTCord.y - y, direction, delta);
#else
			OctDir* dPtr = getDirectionOrder(destTCord.x - x, destTCord.y - y);
#endif

			int i = 4;
			while(i--)
			{
				OctDir dir = *dPtr++;
				DirOffset* off = &directionOffsets[dir];
			
				long x1 = x + off->x;
				long y1 = y + off->y;

				if(x1 < 0)
					continue;
				if(y1 < 0)
					continue;
				if(x1 > (width - 1))
					continue;
				if(y1 > (height - 1))
					continue;

				RouteNode* node = getNode(x1, y1);

				RouteCost nCost = node->getCost();

				if(cost < nCost)
				{
					// UBYTE effect = terrainRouteCost[cTerrain.getVal(x, y)];
					UBYTE effect = terrainRouteCost[cTerrain.getVal(x1, y1)];
					if(effect || allowIllegal)
					{
						/*
						 * Simple route finder treats all types of terrain the same
						 */

						if(routeMode == RM_Simple)
						{
							if(allowIllegal)
							{
								if(effect)
									allowIllegal = False;
								else
									effect = 16;
							}
							else
								effect = 1;
						}
						else
						{
							if(allowIllegal)
							{
								if(effect == 0)
									effect = 16;
								else
									allowIllegal = False;
							}
							else
							{
								/*
							 	 * If a long way from destination, ignore all
							 	 * terrain effects except impassable
							 	 */

								UWORD aX = abs(destTCord.x - x1);
								UWORD aY = abs(destTCord.y - y1);

								if( (aX > MaxRouteDistance) || (aY > MaxRouteDistance) )
									effect = 16;
							}
						}

						RouteCost newCost = cost + effect;

						if( (newCost < nCost) && (newCost < bestCost) )
						{
							if((x1 == destTCord.x) && (y1 == destTCord.y))
							{
								bestCost = newCost;
#ifdef DEBUG
								routeLog.printf("Found a route, cost=%d, iteration=%d, dupCount=%d, usage=%d",
									(int) bestCost,
									iterations,
									dupCount,
									queue.usage);
#endif
							}
							else
							if(!node->getInQueue())
							{
#ifdef USE_DELTA
								queue.put(x1, y1, dir, delta);
#else
								queue.put(x1, y1);
#endif
								node->setInQueue();
							}
#ifdef DEBUG
							else
								dupCount++;
#endif
							node->setCost(newCost);
#ifdef DEBUG
							if(newCost > maxCost)
								maxCost = cost;
#endif
						}
					}
				}

#ifdef USE_DELTA
				delta = 0;
#endif
			}
		}
	}

	node = getNode(destTCord.x, destTCord.y);

#ifdef DEBUG
	long timeCount = timer->getFastCount() - timeTaken;
	routeLog.printf("Iterations = %d", iterations);
	routeLog.printf("Queue items used = %d", queue.usage);
	routeLog.printf("maxCost = %d", (int) maxCost);
	routeLog.printf("dupCount = %d", (int) dupCount);

	if(timeCount)
		routeLog.printf("Iterations per second = %ld",
			(long) (iterations * timer->fastTPS) / timeCount);


	if(node->getCost() == MaxNodeCost)
		routeLog.printf("No route");
	else
		routeLog.printf("Route found at cost: %d", node->getCost());
	routeLog.printf("");
#endif

	return (node->getCost() < MaxNodeCost);
}

#ifdef USE_LINEMODE
/*==========================================================
 * Find a suitable midpoint of passable terrain
 */

Boolean findMidPoint(const CampaignTerrainCoord& from, const CampaignTerrainCoord& to, CampaignTerrainCoord& result)
{
	CampaignTerrain& cTerrain = campaign->world->terrain;

		/*
		 * Find a midpoint
		 */

	result.x = (from.x + to.x) / 2;
	result.y = (from.y + to.y) / 2;

	UBYTE effect = terrainRouteCost[cTerrain.getVal(result.x, result.y)];
	if(effect)
		return True;


	WORD dx = to.x - from.x;
	WORD dy = to.y - from.y;
	UWORD ax = abs(dx);
	UWORD ay = abs(dy);

	if(ax > ay)
	{	// Search vertically
		WORD y1 = result.y;
		WORD y2 = result.y;

		Boolean hitTop = False;
		Boolean hitBottom = False;

		while(!hitTop || !hitBottom)
		{
			if(!hitTop)
			{
				y1++;
				if(y1 < cTerrain.getHeight())
				{
					UBYTE effect = terrainRouteCost[cTerrain.getVal(result.x, y1)];
					if(effect)
					{
						result.y = y1;
						return True;
					}
				}
				else
					hitTop = True;
			}
			
			if(!hitBottom)
			{
				if(y2 == 0)
					hitBottom = True;
				else
				{
					y2--;
					UBYTE effect = terrainRouteCost[cTerrain.getVal(result.x, y2)];
					if(effect)
					{
						result.y = y2;
						return True;
					}
				}
			}
		}

		return (!hitBottom || !hitTop);
	}
	else
	{	// Search horizontally
		WORD x1 = result.x;
		WORD x2 = result.x;

		Boolean hitLeft = False;
		Boolean hitRight = False;

		while(!hitLeft || !hitRight)
		{
			if(!hitRight)
			{
				x1++;
				if(x1 < cTerrain.getWidth())
				{
					UBYTE effect = terrainRouteCost[cTerrain.getVal(x1, result.y)];
					if(effect)
					{
						result.x = x1;
						return True;
					}
				}
				else
					hitRight = True;
			}
			
			if(!hitLeft)
			{
				if(x2 == 0)
					hitLeft = True;
				else
				{
					x2--;
					UBYTE effect = terrainRouteCost[cTerrain.getVal(x2, result.y)];
					if(effect)
					{
						result.x = x2;
						return True;
					}
				}
			}
		}
	}
	return False;
}

/*===================================================================
 * Binary Chopping Line route planner
 */

#ifdef DEBUG
Boolean RoutePlan::_lineRoute(const CampaignTerrainCoord& from, const CampaignTerrainCoord& to, CampaignTerrainCoord& result)
#else
Boolean RoutePlan::lineRoute(const CampaignTerrainCoord& from, const CampaignTerrainCoord& to, CampaignTerrainCoord& result)
#endif
{
	if(isBlocked(from, to))
	{

		if(findMidPoint(from, to, result))
		{
			if(isBlocked(from, result))
			{
				CampaignTerrainCoord mid;
				if(!lineRoute(from, result, mid))
					return False;
				result = mid;
			}

			if(isBlocked(result, to))
			{
				CampaignTerrainCoord mid;
				if(!lineRoute(result, to, mid))
					return False;

				if(!isBlocked(from, mid))
					result = mid;
			}
			
			return True;
		}
		else
			return False;
	}
	else
	{
		result = to;		
		return True;
	}
}


#ifdef DEBUG
Boolean RoutePlan::lineRoute(const CampaignTerrainCoord& from, const CampaignTerrainCoord& to, CampaignTerrainCoord& result)
{
	static int depth = 0;

	if(depth == 0)
	{
		routeLog.printf("--------------------------");
		routeLog.printf("Invoking Binary Line Route");
		timeTaken = timer->getFastCount();
		iterations = 0;
	}

	depth++;
	if(depth > maxCost)
		maxCost = depth;

	iterations++;

 	Boolean value = _lineRoute(from, to, result);

	depth--;

	if(depth == 0)
	{
		long timeCount = timer->getFastCount() - timeTaken;
		routeLog.printf("Iterations = %d", iterations);
		routeLog.printf("Level of recursion = %d", maxCost);

		if(timeCount)
			routeLog.printf("Iterations per second = %ld",
				(long) (iterations * timer->fastTPS) / timeCount);
	}

	return value;
}
#endif
#endif	// USE_LINEMODE

void RoutePlan::getWayPoint(const CampaignTerrainCoord& fromTCord, const CampaignTerrainCoord& destTCord, CampaignTerrainCoord& result)
{
	ASSERT(nodes != 0);

#ifdef DEBUG
	routeLog.printf("Getting way point from %d,%d to %d,%d",
		(int) fromTCord.x, (int) fromTCord.y,
		(int) destTCord.x, (int) destTCord.y);
#endif

	CampaignTerrain& cTerrain = campaign->world->terrain;

	CampaignTerrainCoord currentCord = fromTCord;
	result = destTCord;

#ifdef USE_DELTA
	UWORD delta = 0;
	OctDir direction = NoDirection;
#endif

	RouteCost lastT = terrainRouteCost[cTerrain.getVal(fromTCord.x, fromTCord.y)];
	Boolean keepChecking = True;
	while(keepChecking && (currentCord != destTCord))
	{
		RouteCost best = MaxNodeCost;
		// CampaignTerrainCoord bestTCord = destTCord;
		result = destTCord;

		/*
		 * Check best direction in order of direction to source
		 */

#ifdef USE_DELTA
		OctDir* dPtr = getDirectionOrder(destTCord.x - currentCord.x, destTCord.y - currentCord.y, direction, delta);
#else
		OctDir* dPtr = getDirectionOrder(destTCord.x - currentCord.x, destTCord.y - currentCord.y);
#endif

		int i = 4;
		while(i--)
		{
			getBest(best, result, currentCord.x + directionOffsets[*dPtr].x, currentCord.y + directionOffsets[*dPtr].y);
			dPtr++;
		}

		if(best != MaxNodeCost)
		{
			/*
			 * Get terrain at new coordinate
			 */

			currentCord = result;
			RouteCost newT = terrainRouteCost[cTerrain.getVal(result.x, result.y)];

			if(routeMode == RM_Simple)
			{
				if(isBlocked(currentCord, fromTCord))
					keepChecking = False;
			}
			else // RM_Full
			{
				if(newT != lastT)
					keepChecking = False;
				else		// Check square containing from and best for same terrain type
				{
					if(isDifferentTerrain(currentCord, fromTCord))
						keepChecking = False;
				}
			}
		}
		else
		{
			keepChecking = False;		// No route found
#ifdef DEBUG
			routeLog.printf("No Route Found");
#endif
		}
	}

#ifdef DEBUG
	routeLog.printf(" via: %u,%u",
		(unsigned int) result.x, (unsigned int) result.y);
#endif
}

/*===================================================================
 * Route Planner entry point for planning the route
 */

void RoutePlan::planRoute(const Location& from, const Location& to, OrderHow method, Side allowSide)
{
	CampaignTerrain& cTerrain = campaign->world->terrain;

	/*
	 * Convert to Campaign Terrain grid coordinates
	 */

	CampaignTerrainCoord fromTCord;
	CampaignTerrainCoord destTCord;

	cTerrain.locationToCampTCord(fromTCord, from);
	cTerrain.locationToCampTCord(destTCord, to);

	/*
	 * Default to always going to destination
	 */

	local = to;

	/*
	 * If in same or adjacent grid, then simply set destination
	 */

	UWORD aX = abs(fromTCord.x - destTCord.x);
	UWORD aY = abs(fromTCord.y - destTCord.y);

	if((aX <= 1) && (aY <= 1))
	{
#ifdef DEBUG
		routeLog.printf("Same or adjacent grid");
#endif
	}
	else
	{
#ifdef DEBUG
		timeTaken = timer->getFastCount();
#endif

#ifdef USE_LINEMODE
		if(routeMode == RM_Line)
		{
			CampaignTerrainCoord wayPoint;

			if(lineRoute(fromTCord, destTCord, wayPoint))
			{
				cTerrain.campTCordToLocation(local, wayPoint);
			}
		}
		else
#endif
		{
			if((routeMode == RM_Full) || isBlocked(fromTCord, destTCord))
			{
				if(complexRoute(destTCord, fromTCord))
				{
					CampaignTerrainCoord wayTCord;

					getWayPoint(fromTCord, destTCord, wayTCord);
					cTerrain.campTCordToLocation(local, wayTCord);
				}
			}
		}

#ifdef DEBUG
		timeTaken = timer->getFastCount() - timeTaken;
		int seconds = timeTaken / timer->fastTPS;
		long microSeconds = timeTaken % timer->fastTPS;
		microSeconds = (microSeconds * 1000000) / timer->fastTPS;

		routeLog.printf("Time taken = %d fast Ticks = %ld.%03ld mS",
			timeTaken,
			microSeconds / 1000 + seconds * 1000,
			microSeconds % 1000);
#endif

	}
}

void drawArrow(Region* bm, const Point& p1, const Point& p2, Colour c)
{
	const int arrowLength = 10;
	const int arrowMidLength = 5;
	const int arrowWidth = 4;

	int dx = p2.x - p1.x;
	int dy = p2.y - p1.y;

	SDimension d = distance(dx, dy);

	if(d > arrowLength)
	{
		Point p;

		p.x = p2.x - (arrowLength * dx) / d;
		p.y = p2.y - (arrowLength * dy) / d;

		Point e1;

		e1.x = p.x + (arrowWidth * dy) / d;
		e1.y = p.y - (arrowWidth * dx) / d;

		Point e2;

		e2.x = p.x - (arrowWidth * dy) / d;
		e2.y = p.y + (arrowWidth * dx) / d;
	
		p.x = p2.x - (arrowMidLength * dx) / d;
		p.y = p2.y - (arrowMidLength * dy) / d;

#if 0
		// bm->line(e1, e2, c);
		bm->line(p2, e1, c);
		bm->line(p2, e2, c);
#else
		Point points[3];

		points[0] = p2;
		points[1] = e1;
		points[2] = p;

		Polygon2D poly(3, points);
		poly.setColour(c);
		poly.render(bm);

		points[1] = e2;
		poly.render(bm);


		bm->line(p2, e1, Red4);
		bm->line(p2, e2, Red5);
		bm->line(e1, p, Red4);
		bm->line(e2, p, Red5);
#endif
	}
}

void RoutePlan::mapDraw(MapWindow* map, Region* bm, const Location& from, const Location& to)
{
	if(nodes == 0)
	{
		bm->line(map->locationToPixel(from), map->locationToPixel(local), White);

		if(local != to)
			bm->line(map->locationToPixel(local), map->locationToPixel(to), White);
		return;
	}

	CampaignTerrain& cTerrain = campaign->world->terrain;

	CampaignTerrainCoord fromTCord;
	CampaignTerrainCoord destTCord;

	cTerrain.locationToCampTCord(fromTCord, from);
	cTerrain.locationToCampTCord(destTCord, to);

	Point prevPoint = map->locationToPixel(from);

	for(;;)
	{
		CampaignTerrainCoord bestTCord;

		getWayPoint(fromTCord, destTCord, bestTCord);
		fromTCord = bestTCord;

		Location newLocation;
		cTerrain.campTCordToLocation(newLocation, bestTCord);
		Point newPoint = map->locationToPixel(newLocation);
		// Point midPoint = (newPoint + prevPoint) / 2;

		// bm->line(midPoint, prevPoint, White);
		bm->line(newPoint, prevPoint, White);

		drawArrow(bm, prevPoint, newPoint, Red7);

		prevPoint = newPoint;

		/*
		 * If we are adjacent to destination then stop
		 */

		UWORD aX = abs(bestTCord.x - destTCord.x);
		UWORD aY = abs(bestTCord.y - destTCord.y);

		// if((aX <= 1) && (aY <= 1))
		if( (aX + aY) <= 1)
			break;
	}

	bm->line(map->locationToPixel(to), prevPoint, White);
}
