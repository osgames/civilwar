/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Sprites on Battle Map management
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/08/31  15:23:00  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include "sprlist.h"
#include "sprlib.h"
#include "data3d.h"

Sprite3DList* spriteList = 0;

void SpriteID::remove()
{
	if(element)
	{
		spriteList->remove(element);
		element = 0;
	}
}


Sprite3DList::Sprite3DList() : spritePool(sizeof(SpriteElement), 500)
{
	root = 0;
	spriteList = this;
}

Sprite3DList::~Sprite3DList()
{
	clear();
	spriteList = 0;
}

/*
 * Mark an element for deletion
 */

#ifdef USE_SPRITEID
void Sprite3DList::remove(SpriteID* id)
{
	SpriteElement* element = id->element;
	SpriteRange* range = element->range;

#ifdef CHECK_RANGES
	range->check();
#endif

	SpriteElement* next = element->next;
	SpriteElement* prev = element->prev;

	if(next)
		next->prev = prev;

	if(prev)
		prev->next = next;
	else
		range->entry = next;

	spritePool.release(element);

	range->count--;

	/*
	 * Delete range if empty?
	 */

#ifdef CHECK_RANGES
	range->check();
#endif
}
#else

void Sprite3DList::remove(SpriteElement* element)
{
	SpriteRange* range = element->range;

#ifdef CHECK_RANGES
	range->check();
#endif

	SpriteElement* next = element->next;
	SpriteElement* prev = element->prev;

	if(next)
		next->prev = prev;

	if(prev)
		prev->next = next;
	else
		range->entry = next;

	spritePool.release(element);

	range->count--;

	/*
	 * Delete range if empty?
	 */

#ifdef CHECK_RANGES
	range->check();
#endif
}
#endif

void Sprite3DList::clear()
{
	if(root)
	{
		SpriteRange* range = root;

		while(range->leftChild)
			range = range->left;

		while(range)
		{
			SpriteRange* r1 = range->right;
			Boolean hasRight = range->rightChild;

			clearRange(range);
			delete range;

			if(hasRight)
			{
				while(r1->leftChild)
					r1 = r1->left;
			}

			range = r1;
		}

		root = 0;
	}
}

void Sprite3DList::clearRange(SpriteRange* range)
{
#ifdef CHECK_RANGES
	range->check();
#endif

	SpriteElement* spr = range->entry;

	while(spr)
	{
		SpriteElement* next = spr->next;

		// delete spr;
		spritePool.release(spr);

		spr = next;
	}
}

#ifdef USE_SPRITEID
void Sprite3DList::add(Shape3D* spr, const Point3D& p, SpriteID* id)
#else
SpriteElement* Sprite3DList::add(Shape3D* spr, const Point3D& p)
#endif
{
	SpriteRange* range;

	if(root)
	{
		range = root;

		for(;;)
		{
			if(p.z > range->maxZ)
			{
				if(range->leftChild)
					range = range->left;
				else
					break;
			}
			else if(p.z < range->minZ)
			{
				if(range->rightChild)
					range = range->right;
				else
					break;
			}
			else
				break;
		}
	}
	else
	{
		range = new SpriteRange;
		root = range;
		range->maxZ = maxCord3D;
		range->minZ = minCord3D;
	}

	/*
	 * If there's no more space then split the range into 3.
	 */

	if(range->count >= MaxInRange)
	{
		SpriteRange* left = new SpriteRange;
		SpriteRange* right = new SpriteRange;

		left->left = range->left;
		left->leftChild = range->leftChild;

		left->right = range;
		left->rightChild = False;

		right->right = range->right;
		right->rightChild = range->rightChild;
		right->left = range;
		right->leftChild = False;

		range->left = left;
		range->leftChild = True;
		range->right = right;
		range->rightChild = True;

		/*
		 * Go to old left's rightmost node and set to left
		 */

		if(left->leftChild)
		{
			SpriteRange* r1 = left->left;
			while(r1->rightChild)
				r1 = r1->right;

			r1->right = left;
		}

		/*
		 * Go to old right's leftmost node and set to right
		 */

		if(right->rightChild)
		{
			SpriteRange* r1 = right->right;
			while(r1->leftChild)
				r1 = r1->left;
			r1->left = right;
		}



		left->entry = range->entry;
		int count = MaxInRange/3;

		left->count = count;
		range->count = MaxInRange - 2 * count;
		right->count = count;

		/*
		 * Set up left range
		 */

		right->minZ = range->minZ;
		left->maxZ = range->maxZ;

		SpriteElement* el = range->entry;
		while(--count)
		{
			el->range = left;
			el = el->next;
		}

		left->minZ = el->where.z;

		/*
		 * Do middle
		 */

		range->entry = el->next;
		el->next = 0;
		el->range = left;

		el = range->entry;
		el->prev = 0;

		range->maxZ = el->where.z;
		count = range->count;
		while(--count)
		{
			el = el->next;
		}

		range->minZ = el->where.z;

		/*
		 * Do Right
		 */

		right->entry = el->next;
		el->next = 0;
		el = right->entry;
		right->maxZ = el->where.z;
		el->prev = 0;

		while(el)
		{
			el->range = right;
			el = el->next;
		}


#ifdef CHECK_RANGES
		range->check();
		left->check();
		right->check();
#endif


		if(p.z < range->minZ)
			range = right;
		else if(p.z > range->maxZ)
			range = left;
	}

	/*
	 * Add new element into range
	 */

#if 1
	if(range->count++)
	{
		if(p.z < range->minZ)
			range->minZ = p.z;
		if(p.z > range->maxZ)
			range->maxZ = p.z;
	}
	else
	{
		range->minZ = p.z;
		range->maxZ = p.z;
	}
#else
	range->count++;
#endif

	SpriteElement* s = (SpriteElement*)spritePool.allocate();
	s->init(spr, p);

	SpriteElement* sp = range->entry;
	SpriteElement* prev = 0;
	while(sp && (sp->where.z >= p.z))
	{
		prev = sp;
		sp = sp->next;
	}

	/*
	 * Insert before sp
	 */

	if(sp)
	{
		s->next = sp;
		s->prev = sp->prev;

		sp->prev = s;

		if(prev)
			prev->next = s;
		else
			range->entry = s;
	}
	else	// Put at end
	{
		s->next = 0;
		s->prev = prev;

		if(prev)
			prev->next = s;
		else
			range->entry = s;
	}
	s->range = range;

#ifdef CHECK_RANGES
	range->check();
#endif

#ifdef USE_SPRITEID
	if(id)
	{
		id->element = s;
	}
#else

	return s;

#endif
}

SpriteRange::SpriteRange()
{
	left  = 0;
	right = 0;
	entry = 0;
	count = 0;

	leftChild = False;
	rightChild = False;
}

#ifdef CHECK_RANGES
void SpriteRange::check()
{
	SpriteElement* spr = entry;
	SpriteElement* prev = 0;
	while(spr)
	{
		if(spr->range != this)
			throw GeneralError("SpriteElement has wrong range");

		if(spr->prev != prev)
			throw GeneralError("SpriteElement has wrong prev value");

		prev = spr;
		spr = spr->next;
	}
}
#endif

void Sprite3DList::draw(const System3D& drawData)
{
	SpriteRange* range = root;

	if(range)
	{
		while(range->leftChild)
			range = range->left;

		while(range)
		{
			drawRange(drawData, range);

			if(range->rightChild)
			{
				range = range->right;

				while(range->leftChild)
					range = range->left;
			}
			else
				range = range->right;
		}
	}
}

void Sprite3DList::drawRange(const System3D& drawData, SpriteRange* range)
{
#ifdef CHECK_RANGES
	range->check();
#endif

	SpriteElement* spr = range->entry;
	while(spr)
	{
		spr->sprite->draw(drawData, spr->where);
		spr = spr->next;
	}
}

void SpriteElement::init(Shape3D* spr, const Point3D& p)
{
	sprite = spr;
	where = p;
	range = 0;
	next = 0;
	prev = 0;
}


void Sprite3D::draw(const System3D& drawData, const Point3D& where)
{
#ifdef USE_SCANBUF
	drawData.scanBuffer.blitSprite(drawData.bitmap, spriteBlock, where);
#else
	drawData.zBuffer.blitSprite(drawData.bitmap, spriteBlock, where);
#endif
}

Sprite3D::~Sprite3D()
{
	release();
}

void Sprite3D::release()
{
	if(spriteBlock)
	{
		spriteBlock->release();		// This will also release it from its library
		spriteBlock = 0;
	}
}
