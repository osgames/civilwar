/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Save Game File
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "savegame.h"
#include "wldfile.h"
#include "batfile.h"
#include "dfile.h"
#include "dialogue.h"
#include "memptr.h"
#include "game.h"
#if !defined(TESTBATTLE)
#include "campaign.h"
#endif
#include "campbatl.h"

#include "menudata.h"
#include "makename.h"
#if !defined(TESTCAMP) && !defined(CAMPEDIT)
#include "batldata.h"
#endif
#include "mp_file.h"
#include "batsel.h"
#include "language.h"
#if !defined(TESTCAMP) && !defined(CAMPEDIT) && !defined(TESTBATTLE) && !defined(BATEDIT)
#include "campwld.h"
#endif
#include "version.h"

static const UWORD SavedBattleFileVersion 		= 0x0001;
static const UWORD SavedCampaignVersion 			= 0x0001;
static const UWORD SavedCampaignBattleVersion 	= 0x0001;
static const UWORD GameChunkVersion 				= 0x0001;

static const ChunkID GameChunkID = 'GAME';

struct D_GameChunk_0_0 {
	// IWORD version;
	IBYTE difficulty[DifficultyCount];
	IBYTE gameType;		// Campaign or battle
	IBYTE playersSide;	// USA or CSA
	IBYTE playerCount;	// 1 or 2
};

struct D_GameChunk {
	// IWORD version;
	ILONG progVersion;
	IBYTE difficulty[DifficultyCount];
	IBYTE gameType;		// Campaign or battle
	IBYTE playersSide;
	IBYTE aiSide;
	IBYTE remoteSide;
};


#if !defined(COMPUSA_DEMO)

void writeGameChunk(WriteWithPopup& file)
{
	file.startWriteChunk(GameChunkID);

	IWORD iVersion;
	putWord(&iVersion, GameChunkVersion);
	file.write(&iVersion, sizeof(iVersion));

	D_GameChunk data;
	
	// putWord(&data.version, GameChunkVersion);
	for(int i = 0; i < DifficultyCount; i++)
		putByte(&data.difficulty[i], game->getDifficulty(DifficultyType(i)));
	putByte(&data.gameType, game->getGameType());

#if 0	// Old version
	putByte(&data.playersSide, game->getLocalSide());
	putByte(&data.playerCount, game->isRemoteActive() ? 2 : 1);
#else	// New version

	putLong(&data.progVersion, getVersionLong());

	Side pSide = SIDE_None;
	Side aiSide = SIDE_None;
	Side rSide = SIDE_None;

	for(SideIndex si = SideIndex(0); si < SideCount; INCREMENT(si))
	{
		Side mask = indexToSide(si);

		if(game->isPlayer(si))
			pSide = Side(pSide | mask);

		if(game->isAI(si))
			aiSide = Side(aiSide | mask);

		if(game->isRemote(si))
			rSide = Side(rSide | mask);
	}

	putByte(&data.playersSide, pSide);
	putByte(&data.remoteSide, rSide);
	putByte(&data.aiSide, aiSide);

#endif	


	file.write(&data, sizeof(data));
	
	file.endWriteChunk();
}

#endif		// DEMO

void readGameChunk(ChunkFileRead& file)
{
	if(file.startReadChunk(GameChunkID) == CE_OK)
	{
		IWORD iVersion;
		file.read(&iVersion, sizeof(iVersion));

		UWORD version = getWord(&iVersion);

		if(version < 0x0001)
		{
			D_GameChunk_0_0 data;

			file.read(&data, sizeof(data));
			for(int i = 0; i < DifficultyCount; i++)
				game->setDifficulty(DifficultyType(i), Realism(getByte(&data.difficulty[i])));
			game->setGameType(GameType(getByte(&data.gameType)));

			Side side = Side(getByte(&data.playersSide));
			int count = getByte(&data.playerCount);

			if(count > 1)
				game->setRemote();

			game->setupPlayerSide(side);
		}
		else
		{
			D_GameChunk data;

			file.read(&data, sizeof(data));
			for(int i = 0; i < DifficultyCount; i++)
				game->setDifficulty(DifficultyType(i), Realism(getByte(&data.difficulty[i])));
			game->setGameType(GameType(getByte(&data.gameType)));

			Side pSide = Side(getByte(&data.playersSide));
			Side rSide = Side(getByte(&data.remoteSide));
			Side aiSide = Side(getByte(&data.aiSide));

			game->setSides(pSide, aiSide, rSide);
		}
	}
}

#if !defined(COMPUSA_DEMO)
#if !defined(TESTBATTLE) && !defined(BATEDIT)

void saveCampaignGame(const char* fileName, Campaign* campaign)
{
	WriteWithPopup file(fileName, FT_SavedCampaign, "Saved Campaign", SavedCampaignVersion, 8);
	writeGameChunk(file);
	writeSavedCampaign(file, campaign);
}

#endif

#if !defined(TESTCAMP) && !defined(CAMPEDIT)

void saveBattleGame(const char* fileName, BattleData* battle)
{
#if !defined(TESTBATTLE) && !defined(BATEDIT)
	if(campaign)
	{
		/*
		 * Campaign Battle
		 */

		WriteWithPopup file(fileName, FT_BattleInCampaign, "Campaign Battle", SavedCampaignBattleVersion, 7);
		writeGameChunk(file);
		writeBattleGame(file, battle);

		// Also include... copy of temporary file

		writeCampaignBattleInfo(file, campaign);
		writeCampaignTemp(file, campaign);

		// Marked Battle List and other campaign info
	}
	else
#endif
	{
		/*
		 * Historical Battle
		 */

		WriteWithPopup file(fileName, FT_SavedBattle, "Saved Battle", SavedBattleFileVersion, 5);
		writeGameChunk(file);
		writeBattleGame(file, battle);
	}
}

#endif

#endif	// DEMO


/*=================================================================
 * Load Saved Game
 */

int loadSavedGame(const char* fileName)
{
	try
	{
		ReadWithPopup file(fileName, FT_DontCare);

		readGameChunk(file);

		switch(file.fileType)
		{
#if !defined(TESTBATTLE) && !defined(BATEDIT)
		case FT_SavedCampaign:
			campaign = new Campaign;
			campaign->beforeLoad();
			readSavedCampaign(file, campaign);
			campaign->afterLoad();
			return True;
#endif

#if !defined(TESTCAMP) && !defined(CAMPEDIT)
		case FT_SavedBattle:
			battle = new BattleData(0);
			readBattleGame(file, battle);
			return True;
#endif

#if !defined(TESTCAMP) && !defined(CAMPEDIT) && !defined(TESTBATTLE) && !defined(BATEDIT)
		case FT_BattleInCampaign:
			campaign = new Campaign;
			campaign->smallSetup();
			battle = new BattleData(&campaign->world->ob);
			readBattleGame(file, battle);
			readCampaignBattleInfo(file, campaign, battle->ob);
			readCampaignTemp(file, campaign);
			return True;
#endif
		default:
			break;
		}
	}
	catch(ChunkFileBase::ChunkFileError e)
	{
		MemPtr<char> buffer(100);

		sprintf(buffer, language(lge_errorReading), fileName);

		dialAlert(0, buffer, language(lge_OK));
		return False;
	}

	return False;
}

class TidyUp {
public:
	~TidyUp()
	{
#if !defined(TESTCAMP)
		if(battle)
		{
			delete battle;
			battle = 0;
		}
#endif
#if !defined(TESTBATTLE)
		if(campaign)
		{
			delete campaign;
			campaign = 0;
		}
#endif
	}
};

/*
 * PLay a saved game
 *
 * If loadName is 0 then a file selector is provided
 *
 * Returns:
 *		???
 */

int playSavedGame(const char* loadName)
{
	int result = 0;

	MemPtr<char> fileName(FILENAME_MAX);


	if(loadName)
		makeFilename(fileName, loadName, ".SAV", False);
	else
	{
		const char* wildCard = getSaveGameWildCard();
		if(!getFileName(fileName, language(lge_load), wildCard, True))
			return result;
	}

	/*
	 * fileName must be valid by here...
	 */

	TidyUp tidy;		// Forces campaign and battle to be destructed on exit

	if(loadSavedGame(fileName))
	{
#if !defined(TESTCAMP) && !defined(CAMPEDIT) && !defined(TESTBATTLE) && !defined(BATEDIT)
		if(campaign && battle)
		{
			MarkedBattle* bat = makeMarkedBattle(battle->ob);
			int result = runBattle(bat, True);			// Play the battle without deployment

			if(result == MenuData::MenuMainMenu)
			{
				delete bat;
				// delete battle;
				// battle = 0;
				// delete campaign;
				// campaign = 0;
			}
			else
			{
				campaign->afterBattle(battle->ob);
				campaign->afterLoad();			// Get AI re-initialised!
				// campaign->world->IncExperience(bat);
				bat->updateExperience(5);
				delete bat;
				delete battle;
				battle = 0;
				campaign->movementPhase(True);
				result = campaign->runCampaign();
				// delete campaign;
				// campaign = 0;

			}
			return result;
		}
#endif
#if !defined(TESTCAMP) && !defined(CAMPEDIT)
		if(battle)
		{
			MarkedBattle* bat = makeMarkedBattle(battle->ob);
			int result = runBattle(bat, True);		// Run battle without deployment
			delete bat;
			// delete battle;
			// battle = 0;
			return result;
		}
#endif
#if !defined(TESTCAMP) && !defined(CAMPEDIT) && !defined(TESTBATTLE) && !defined(BATEDIT)
		if(campaign)
		{
			result = campaign->runCampaign();
			// delete campaign;
			// campaign = 0;
			return result;
		}
#endif
	}

	return result;
}


const char* getSaveGameWildCard()
{
	if(!game->isRemoteActive())
		return "*.sav";
	else
		return "*.sv2";
}

