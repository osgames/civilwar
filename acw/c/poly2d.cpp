/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	2D Polygon renderer
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "poly2d.h"
#include "image.h"

/*
 * 2D Clipping
 */

class PolyClip;
class PolyClipEdge {
	Point first;
	Point last;
	Boolean needFirst;
public:
				PolyClipEdge() : first(), last() { needFirst = True; };

	friend class PolyClip;
};

class PolyClip { 
	Polygon2D* destPoly;
	const Region* bitmap;
	enum { None, Left, Right, Top, Bottom } closing;
	PolyClipEdge top;
	PolyClipEdge bottom;
	PolyClipEdge left;
	PolyClipEdge right;

	void clipLeft(Point p);
	void clipRight(Point p);
	void clipTop(Point p);
	void clipBottom(Point p);
	
public:
				PolyClip(Polygon2D* destPoly, const Region* bitmap)
				{
					closing = None;
					PolyClip::destPoly = destPoly;
					PolyClip::bitmap = bitmap;
				};

				~PolyClip();

		void	clipPoint(Point p);



};


Polygon2D::Polygon2D(PolyPointIndex nPoints, Point* points)
{
	maxPoints = nPoints;

	if(points)
	{
		Polygon2D::points = points;
		Polygon2D::nPoints = nPoints;
		pointsAlloced = False;
	}
	else
	{
		Polygon2D::points = new Point[nPoints];

#if 0
		if(!Polygon2D::points)
			throw NoMemory();
#endif

		Polygon2D::nPoints = 0;
		pointsAlloced = True;
	}
}


Polygon2D::~Polygon2D()
{
	if(pointsAlloced)
		delete[] points;
}





/*
 * 2D Clipping
 */

void PolyClip::clipLeft(Point p)
{
	if(left.needFirst)
	{
		left.needFirst = False;
		left.first = p;
		left.last = p;
	}

	// Moving from outside In?

	else if( ((p.x >= bitmap->x) && (left.last.x < bitmap->x) ) ||
			   ((p.x < bitmap->x) && (left.last.x >= bitmap->x) ) )
	{
		clipRight(Point(bitmap->x, p.y + ((p.y - left.last.y) * (bitmap->x - p.x)) / (p.x - left.last.x)));
	}

	left.last = p;

	if( (p.x >= bitmap->x) && (closing != Left) )
		clipRight(p);

}

void PolyClip::clipRight(Point p)
{
	SDimension limit = SDimension(bitmap->max.x - 1);

	if(right.needFirst)
	{
		right.needFirst = False;
		right.first = p;
		right.last = p;
	}

	// Moving from outside In?

	else if( ((p.x <  limit) && (right.last.x >= limit) ) ||
			   ((p.x >= limit) && (right.last.x <  limit) ) )
	{
 		clipBottom(Point(limit, p.y + ((p.y - right.last.y) * (limit - p.x)) / (p.x - right.last.x)));
	}

	right.last = p;

	if( (p.x < limit) && (closing != Right) )
		clipBottom(p);

}

void PolyClip::clipBottom(Point p)
{
	if(bottom.needFirst)
	{
		bottom.needFirst = False;
		bottom.first = p;
		bottom.last = p;
	}

	// Moving from outside In?

	else if( ((p.y >= bitmap->y) && (bottom.last.y < bitmap->y) ) ||
			   ((p.y < bitmap->y) && (bottom.last.y >= bitmap->y) ) )
	{
		clipTop(Point( p.x + ( (p.x - bottom.last.x) * (bitmap->y - p.y)) / (p.y - bottom.last.y), bitmap->y));
	}

	bottom.last = p;

	if( (p.y >= bitmap->y) && (closing != Bottom) )
		clipTop(p);

}

void PolyClip::clipTop(Point p)
{
	SDimension limit = SDimension(bitmap->max.y - 1);
	
	if(top.needFirst)
	{
		top.needFirst = False;
		top.first = p;
		top.last = p;
	}

	// Moving from outside In?

	else if( ((p.y <  limit) && (top.last.y >= limit) ) ||
			   ((p.y >= limit) && (top.last.y <  limit) ) )
	{
		destPoly->add(Point( p.x + ( (p.x - top.last.x) * (limit - p.y)) / (p.y - top.last.y), limit));
	}

	top.last = p;

	if( (p.y < limit) && (closing != Top) )
		destPoly->add(p);

}

void PolyClip::clipPoint(Point p)
{
	clipLeft(p);
}

/*
 * Close the polygon
 */

PolyClip::~PolyClip()
{
	if(!left.needFirst)
	{
		closing = Left;
		clipLeft(left.first);
	}

	if(!right.needFirst)
	{
		closing = Right;
		clipRight(right.first);
	}

	if(!bottom.needFirst)
	{
		closing = Bottom;
		clipBottom(bottom.first);
	}

	if(!top.needFirst)
	{
		closing = Top;
		clipTop(top.first);
	}
}

void Polygon2D::clipPoly(Polygon2D* destPoly, const Region* bitmap) const
{
	PolyClip clip(destPoly, bitmap);

	/*
	 * Go through each point
	 */

	PolyPointIndex i = nPoints;
	Point* p = points;

	while(i--)
		clip.clipPoint(*p++);

	// Closing is done automatically in the destructor

}


/*
 * Scan line class for storing min/max values
 */

class Scan2D {
	
	struct ScanLine {
		SDimension min;
		SDimension max;
	} *lines;

	SDimension count;
	SDimension yOffset;

public:
			Scan2D(const SDimension count, const SDimension yOffset = 0);
		  ~Scan2D();
	ScanLine*	get(SDimension y) const { return &lines[y - yOffset]; };
	ScanLine*	get() const { return lines; };

	void			setMin(SDimension y, SDimension x) { get(y)->min = x; };
	void			setMax(SDimension y, SDimension x) { get(y)->max = x; };

	SDimension	getH() const { return count; };
	SDimension  getY() const { return yOffset; };

	void			addLine(Point p1, Point p2);
	void			addPoint(Point p);

	void 			draw(Region* image, Colour color) const;
};


inline Scan2D::Scan2D(const SDimension count, const SDimension yOffset)
{
	Scan2D::count = count;
	Scan2D::yOffset = yOffset;
	lines = new ScanLine[count];

#if 0
	if(!lines)
		throw NoMemory();
#endif

	int i = count;
	ScanLine* line = lines;
	while(i--)
	{
		line->min = maxSDimension;
		line->max = 0;

		line++;
	}

}

inline Scan2D::~Scan2D()
{
	delete[] lines;
}

/*
 * Update a scan line
 */

void Scan2D::addPoint(Point p)
{
	ScanLine* line = get(p.y);

	if(p.x < line->min)
		line->min = p.x;

	if(p.x > line->max)
		line->max = p.x;
}

/*
 * Draw a line into the scanline buffer
 */

void Scan2D::addLine(Point from, Point to)
{
	Point diff = to - from;
	Point absDiff = diff.abs();
	Point dir = diff.sign();

	/*
	 * Which is major axis?
	 */

	if(absDiff.x > absDiff.y)
	{
		// X is main axis

		addPoint(from);

		if(from.x != to.x)
		{
			int value = 0x8000;
			int add = (absDiff.y * 0x10000) / absDiff.x;

			while(from.x != to.x)
			{
				value += add;
				if(value >= 0x10000)
				{
					from.y += dir.y;
					value -= 0x10000;
				}

				from.x += dir.x;

				addPoint(from);
			}
		}
	}
	else
	{
		// Y is main axis

		addPoint(from);

		if(from.y != to.y)
		{
			int value = 0x8000;
			int add = (absDiff.x * 0x10000) / absDiff.y;

			while(from.y != to.y)
			{
				value += add;
				if(value >= 0x10000)
				{
					from.x += dir.x;
					value -= 0x10000;
				}

				from.y += dir.y;

				addPoint(from);
			}
		}
	}
}



/*
 * Draw the scan lines
 */

void Scan2D::draw(Region* image, Colour color) const
{
	ScanLine* line = get();
	int count = getH();
	SDimension y = getY();

#if 0
	if(texture)
	{
		while(count--)
		{
			if(line->min <= line->max)
				image->HLineFast(line->min, y, line->max - line->min + 1, color, texture);

			line++;
			y++;
		}
	}
	else
#endif
	{
		while(count--)
		{
			if(line->min <= line->max)
				image->HLineFast(line->min, y, line->max - line->min + 1, color);

			line++;
			y++;
		}
	}
}

/*
 * Draw a polygon onto a bitmap
 *
 * This is confusing now:
 *   Should a routine like this belong to the BitMap class or the
 *   Polygon class?  Throughout the rest of the program, I have
 *   made functions belong to the class that they are written to,
 *   but for this function we have the strangeness that it is more
 *   logical to place it in this module in the polygon implementation.
 */

void Polygon2D::render(Region* bm) const
{
#ifdef DEBUG1
	/*
	 * Draw outline for testing
	 */

	{
		Rect oldClip = *this;
		setClip();


		PolyPointIndex i = poly.nPoints;
		Point* p = poly.points;
		Point* lastPoint = &p[i-1];
		while(i--)
		{
			line(*lastPoint, *p, color+1);
			lastPoint = p;

			p++;
		}

		setClip(oldClip.getX(), oldClip.getY(), oldClip.getW(), oldClip.getH());

	}
#endif

	/*
	 * Clip it
	 */

	Polygon2D clippedPoly(nPoints * 2 + 4);		// Rest of function will use this

	clipPoly(&clippedPoly, bm);

	if(clippedPoly.nPoints < 3)
		return;

#ifdef DEBUG1
	/*
	 * Draw outline for testing
	 */

	{
		PolyPointIndex i = clippedPoly.nPoints;
		Point* p = clippedPoly.points;
		Point* lastPoint = &p[i-1];
		while(i--)
		{
			line(*lastPoint, *p, color+1);
			lastPoint = p;

			p++;
		}
	}
#endif

	/*
	 * Work out rectangle enclosing the polygon
	 */

	Point minP = bm->max;			// Start with clipping rectangle extremes
	Point maxP(0,0);

	{
		PolyPointIndex i = clippedPoly.nPoints;
		Point* p = clippedPoly.points;
		while(i--)
		{
			if(p->x < minP.x)
				minP.x = p->x;
			if(p->x > maxP.x)
				maxP.x = p->x;
			if(p->y < minP.y)
				minP.y = p->y;
			if(p->y > maxP.y)
				maxP.y = p->y;

			p++;
		}
	}

	Point size = maxP - minP + Point(1,1);

	/*
	 * Make up scan line table
	 */

#ifdef DEBUG1
	// Draw a frame for testing

	frame(minP, size + Point(1,1), 64);
#endif

	Scan2D lines(size.y, minP.y);

	{
		PolyPointIndex i = clippedPoly.nPoints;
		Point* p = clippedPoly.points;
		Point* lastPoint = &p[i-1];
		while(i--)
		{
			lines.addLine(*lastPoint, *p);
			lastPoint = p;

			p++;
		}
	}

	lines.draw(bm, colour);
}
