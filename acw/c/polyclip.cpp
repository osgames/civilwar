/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	3D Polygon Clipping
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.8  1994/08/19  17:29:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/06/24  14:43:30  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.5  1994/03/18  15:07:16  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1993/12/10  16:06:39  Steven_Green
 * Point3DI clipping
 *
 * Revision 1.3  1993/12/03  16:45:51  Steven_Green
 * Clipping calculations always done in same direction to avoid rounding
 * errors caused by adjacent polygones.
 *
 * Revision 1.2  1993/11/26  22:30:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/24  09:32:52  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "polyclip.h"
#include "polygon.h"
#include "image.h"
#include "view3d.h"

/*
 * 2D Clipping
 */

void PolyClip::clipLeft(Point p)
{
	if(left.needFirst)
	{
		left.needFirst = False;
		left.first = p;
		left.last = p;
	}

	// Moving from outside In?

	else if( ((p.x >= bitmap->x) && (left.last.x < bitmap->x) ) ||
			   ((p.x < bitmap->x) && (left.last.x >= bitmap->x) ) )
	{
		clipRight(Point(bitmap->x, p.y + ((p.y - left.last.y) * (bitmap->x - p.x)) / (p.x - left.last.x)));
	}

	left.last = p;

	if( (p.x >= bitmap->x) && (closing != Left) )
		clipRight(p);

}

void PolyClip::clipRight(Point p)
{
	SDimension limit = SDimension(bitmap->max.x - 1);

	if(right.needFirst)
	{
		right.needFirst = False;
		right.first = p;
		right.last = p;
	}

	// Moving from outside In?

	else if( ((p.x <  limit) && (right.last.x >= limit) ) ||
			   ((p.x >= limit) && (right.last.x <  limit) ) )
	{
 		clipBottom(Point(limit, p.y + ((p.y - right.last.y) * (limit - p.x)) / (p.x - right.last.x)));
	}

	right.last = p;

	if( (p.x < limit) && (closing != Right) )
		clipBottom(p);

}

void PolyClip::clipBottom(Point p)
{
	if(bottom.needFirst)
	{
		bottom.needFirst = False;
		bottom.first = p;
		bottom.last = p;
	}

	// Moving from outside In?

	else if( ((p.y >= bitmap->y) && (bottom.last.y < bitmap->y) ) ||
			   ((p.y < bitmap->y) && (bottom.last.y >= bitmap->y) ) )
	{
		clipTop(Point( p.x + ( (p.x - bottom.last.x) * (bitmap->y - p.y)) / (p.y - bottom.last.y), bitmap->y));
	}

	bottom.last = p;

	if( (p.y >= bitmap->y) && (closing != Bottom) )
		clipTop(p);

}

void PolyClip::clipTop(Point p)
{
	SDimension limit = SDimension(bitmap->max.y - 1);
	
	if(top.needFirst)
	{
		top.needFirst = False;
		top.first = p;
		top.last = p;
	}

	// Moving from outside In?

	else if( ((p.y <  limit) && (top.last.y >= limit) ) ||
			   ((p.y >= limit) && (top.last.y <  limit) ) )
	{
		destPoly->add(Point( p.x + ( (p.x - top.last.x) * (limit - p.y)) / (p.y - top.last.y), limit));
	}

	top.last = p;

	if( (p.y < limit) && (closing != Top) )
		destPoly->add(p);

}

void PolyClip::clipPoint(Point p)
{
	clipLeft(p);
}

/*
 * Close the polygon
 */

PolyClip::~PolyClip()
{
	if(!left.needFirst)
	{
		closing = Left;
		clipLeft(left.first);
	}

	if(!right.needFirst)
	{
		closing = Right;
		clipRight(right.first);
	}

	if(!bottom.needFirst)
	{
		closing = Bottom;
		clipBottom(bottom.first);
	}

	if(!top.needFirst)
	{
		closing = Top;
		clipTop(top.first);
	}
}

void Polygon2D::clipPoly(Polygon2D* destPoly, const Region* bitmap) const
{
	PolyClip clip(destPoly, bitmap);

	/*
	 * Go through each point
	 */

	PolyPointIndex i = nPoints;
	Point* p = points;

	while(i--)
		clip.clipPoint(*p++);

	// Closing is done automatically in the destructor

}


/*
 * 3D Clipping
 */

PolyClip3D::~PolyClip3D()
{
	if(!front.needFirst)
	{
		closing = Front;
		clipFront(front.first);
	}
	if(!back.needFirst)
	{
		closing = Back;
		clipBack(back.first);
	}
	if(!left.needFirst)
	{
		closing = Left;
		clipLeft(left.first);
	}
	if(!right.needFirst)
	{
		closing = Right;
		clipRight(right.first);
	}
	if(!bottom.needFirst)
	{
		closing = Bottom;
		clipBottom(bottom.first);
	}
	if(!top.needFirst)
	{
		closing = Top;
		clipTop(top.first);
	}
}

void PolyClip3D::clipPoint(Point3D p)
{
	clipFront(p);
}

void PolyClip3D::clipFront(Point3D p)
{
	if(front.needFirst)
	{
		front.needFirst = False;
		front.first = p;
		front.last = p;
	}

	// Moving from outside In?

	else if((p.z >= view->getFront()) && (front.last.z < view->getFront()) )
	{
		clipBack(Point3D(
				p.x + ((p.x - front.last.x) * (view->getFront() - p.z)) / (p.z - front.last.z),
				p.y + ((p.y - front.last.y) * (view->getFront() - p.z)) / (p.z - front.last.z),
				view->getFront()
		));
	}
	else if( (p.z < view->getFront()) && (front.last.z >= view->getFront()) )
	{
		clipBack(Point3D(
				front.last.x + ((front.last.x - p.x) * (view->getFront() - front.last.z)) / (front.last.z - p.z),
			 	front.last.y + ((front.last.y - p.y) * (view->getFront() - front.last.z)) / (front.last.z - p.z),
				view->getFront()
		));
	}

	front.last = p;

	if( (p.z >= view->getFront()) && (closing != Front) )
		clipBack(p);
}

void PolyClip3D::clipBack(Point3D p)
{
	clipLeft(p);
}

void PolyClip3D::clipLeft(Point3D p)
{
	if(left.needFirst)
	{
		left.needFirst = False;
		left.first = p;
		left.last = p;
	}

	else if( (p.x >= view->getPixelWidth()) && (left.last.x < view->getPixelWidth()) )
	{
		clipRight(Point3D(view->getPixelWidth(),
				p.y + ((p.y - left.last.y) * (view->getPixelWidth() - p.x)) / (p.x - left.last.x),
				p.z + ((p.z - left.last.z) * (view->getPixelWidth() - p.x)) / (p.x - left.last.x)
		));
	}
	else if( (p.x < view->getPixelWidth()) && (left.last.x >= view->getPixelWidth()) )
	{
		clipRight(Point3D(view->getPixelWidth(),
				left.last.y + ((left.last.y - p.y) * (view->getPixelWidth() - left.last.x)) / (left.last.x - p.x),
				left.last.z + ((left.last.z - p.z) * (view->getPixelWidth() - left.last.x)) / (left.last.x - p.x)
		));
	}

	left.last = p;

	if( (p.x >= view->getPixelWidth()) && (closing != Left) )
		clipRight(p);
}

void PolyClip3D::clipRight(Point3D p)
{
	SDimension limit = SDimension(view->getPixelWidth() - 1);

	if(right.needFirst)
	{
		right.needFirst = False;
		right.first = p;
		right.last = p;
	}

	else if( (p.x <  limit) && (right.last.x >= limit) )
	{
		clipBottom(Point3D(limit,
			p.y + ((p.y - right.last.y) * (limit - p.x)) / (p.x - right.last.x),
			p.z + ((p.z - right.last.z) * (limit - p.x)) / (p.x - right.last.x)
		));
	}
	else if( (p.x >= limit) && (right.last.x <  limit) )
	{
		clipBottom(Point3D(limit,
			right.last.y + ((right.last.y - p.y) * (limit - right.last.x)) / (right.last.x - p.x),
			right.last.z + ((right.last.z - p.z) * (limit - right.last.x)) / (right.last.x - p.x)
		));
	}

	right.last = p;

	if( (p.x < limit) && (closing != Right) )
		clipBottom(p);

}

void PolyClip3D::clipBottom(Point3D p)
{
	if(bottom.needFirst)
	{
		bottom.needFirst = False;
		bottom.first = p;
		bottom.last = p;
	}

	// Moving from outside In?

	
	// Always clip from the inside to outside to avoid errors

	else if( (p.y >= view->getPixelHeight()) && (bottom.last.y < view->getPixelHeight()) )
	{
		clipTop(Point3D(
			p.x + ( (p.x - bottom.last.x) * (view->getPixelHeight() - p.y)) / (p.y - bottom.last.y),
			view->getPixelHeight(),
			p.z + ( (p.z - bottom.last.z) * (view->getPixelHeight() - p.y)) / (p.y - bottom.last.y)
		));
	}
	else if( (p.y < view->getPixelHeight()) && (bottom.last.y >= view->getPixelHeight()) )
	{
		clipTop(Point3D(
			bottom.last.x + ( (bottom.last.x - p.x) * (view->getPixelHeight() - bottom.last.y)) / (bottom.last.y - p.y),
			view->getPixelHeight(),
			bottom.last.z + ( (bottom.last.z - p.z) * (view->getPixelHeight() - bottom.last.y)) / (bottom.last.y - p.y)
		));
	}

	bottom.last = p;

	if( (p.y >= view->getPixelHeight()) && (closing != Bottom) )
		clipTop(p);

}

void PolyClip3D::clipTop(Point3D p)
{
	SDimension limit = SDimension(view->getPixelHeight() - 1);
	
	if(top.needFirst)
	{
		top.needFirst = False;
		top.first = p;
		top.last = p;
	}

	else if( (p.y <  limit) && (top.last.y >= limit) )
	{
		destPoly->add(Point3D(
				p.x + ( (p.x - top.last.x) * (limit - p.y)) / (p.y - top.last.y),
				limit,
				p.z + ( (p.z - top.last.z) * (limit - p.y)) / (p.y - top.last.y)
			));
	}
	else if( (p.y >= limit) && (top.last.y <  limit) )
	{
		destPoly->add(Point3D(
				top.last.x + ( (top.last.x - p.x) * (limit - top.last.y)) / (top.last.y - p.y),
				limit,
				top.last.z + ( (top.last.z - p.z) * (limit - top.last.y)) / (top.last.y - p.y)
			));
	}

	top.last = p;

	if( (p.y < limit) && (closing != Top) )
		destPoly->add(p);

}

void Polygon3D::clipPoly(Polygon3D* destPoly, const Region* bitmap) const
{
}

/*
 * 3DI Clipping
 *
 * Note that the intensity parameter for each clipping function needs calculating.
 */

PolyClip3DI::~PolyClip3DI()
{
	if(!front.needFirst)
	{
		closing = Front;
		clipFront(front.first);
	}
	if(!back.needFirst)
	{
		closing = Back;
		clipBack(back.first);
	}
	if(!left.needFirst)
	{
		closing = Left;
		clipLeft(left.first);
	}
	if(!right.needFirst)
	{
		closing = Right;
		clipRight(right.first);
	}
	if(!bottom.needFirst)
	{
		closing = Bottom;
		clipBottom(bottom.first);
	}
	if(!top.needFirst)
	{
		closing = Top;
		clipTop(top.first);
	}
}

void PolyClip3DI::clipPoint(Point3DI p)
{
	clipFront(p);
}

void PolyClip3DI::clipFront(Point3DI p)
{
	if(front.needFirst)
	{
		front.needFirst = False;
		front.first = p;
		front.last = p;
	}

	// Moving from outside In?

	else if( (p.z >= view->getFront()) && (front.last.z < view->getFront()) )
	{
		clipBack(Point3DI(
				p.x + ((p.x - front.last.x) * (view->getFront() - p.z)) / (p.z - front.last.z),
				p.y + ((p.y - front.last.y) * (view->getFront() - p.z)) / (p.z - front.last.z),
				view->getFront(),
				p.intensity + ((p.intensity - front.last.intensity) * (view->getFront() - p.z)) / (p.z - front.last.z)
		));
	}
	else if((p.z < view->getFront()) && (front.last.z >= view->getFront()) )
	{
		clipBack(Point3DI(
				front.last.x + ((front.last.x - p.x) * (view->getFront() - front.last.z)) / (front.last.z - p.z),
				front.last.y + ((front.last.y - p.y) * (view->getFront() - front.last.z)) / (front.last.z - p.z),
				view->getFront(),
				front.last.intensity + ((front.last.intensity - p.intensity) * (view->getFront() - front.last.z)) / (front.last.z - p.z)
		));
	}

	front.last = p;

	if( (p.z >= view->getFront()) && (closing != Front) )
		clipBack(p);
}

void PolyClip3DI::clipBack(Point3DI p)
{
	clipLeft(p);
}

void PolyClip3DI::clipLeft(Point3DI p)
{
	if(left.needFirst)
	{
		left.needFirst = False;
		left.first = p;
		left.last = p;
	}

	else if( (p.x >= 0) && (left.last.x < 0) )
	{
		clipRight(Point3DI(0,
				p.y + ((p.y - left.last.y) * (0 - p.x)) / (p.x - left.last.x),
				p.z + ((p.z - left.last.z) * (0 - p.x)) / (p.x - left.last.x),
				p.intensity + ((p.intensity - left.last.intensity) * (0 - p.x)) / (p.x - left.last.x)
		));
	}
	else if( (p.x < 0) && (left.last.x >= 0) )
	{
		clipRight(Point3DI(0,
				left.last.y + ((left.last.y - p.y) * (0 - left.last.x)) / (left.last.x - p.x),
				left.last.z + ((left.last.z - p.z) * (0 - left.last.x)) / (left.last.x - p.x),
				left.last.intensity + ((left.last.intensity - p.intensity) * (0 - left.last.x)) / (left.last.x - p.x)
		));
	}

	left.last = p;

	if( (p.x >= 0) && (closing != Left) )
		clipRight(p);
}

void PolyClip3DI::clipRight(Point3DI p)
{
	SDimension limit = SDimension(view->getPixelWidth() - 1);

	if(right.needFirst)
	{
		right.needFirst = False;
		right.first = p;
		right.last = p;
	}

	else if( (p.x <  limit) && (right.last.x >= limit) )
	{
		clipBottom(Point3DI(limit,
			p.y + ((p.y - right.last.y) * (limit - p.x)) / (p.x - right.last.x),
			p.z + ((p.z - right.last.z) * (limit - p.x)) / (p.x - right.last.x),
			p.intensity + ((p.intensity - right.last.intensity) * (limit - p.x)) / (p.x - right.last.x)
		));
	}
	else if( (p.x >= limit) && (right.last.x <  limit) )
	{
		clipBottom(Point3DI(limit,
			right.last.y + ((right.last.y - p.y) * (limit - right.last.x)) / (right.last.x - p.x),
			right.last.z + ((right.last.z - p.z) * (limit - right.last.x)) / (right.last.x - p.x),
			right.last.intensity + ((right.last.intensity - p.intensity) * (limit - right.last.x)) / (right.last.x - p.x)
		));
	}

	right.last = p;

	if( (p.x < limit) && (closing != Right) )
		clipBottom(p);

}

void PolyClip3DI::clipBottom(Point3DI p)
{
	if(bottom.needFirst)
	{
		bottom.needFirst = False;
		bottom.first = p;
		bottom.last = p;
	}

	// Moving from outside In?

	
	// Always clip from the inside to outside to avoid errors

	else if( (p.y >= 0) && (bottom.last.y < 0) )
	{
		clipTop(Point3DI(
			p.x + ( (p.x - bottom.last.x) * (0 - p.y)) / (p.y - bottom.last.y),
			0,
			p.z + ( (p.z - bottom.last.z) * (0 - p.y)) / (p.y - bottom.last.y),
			p.intensity + ( (p.intensity - bottom.last.intensity) * (0 - p.y)) / (p.y - bottom.last.y)
		));
	}
	else if( (p.y < 0) && (bottom.last.y >= 0) )
	{
		clipTop(Point3DI(
			bottom.last.x + ( (bottom.last.x - p.x) * (0 - bottom.last.y)) / (bottom.last.y - p.y),
			0,
			bottom.last.z + ( (bottom.last.z - p.z) * (0 - bottom.last.y)) / (bottom.last.y - p.y),
			bottom.last.intensity + ( (bottom.last.intensity - p.intensity) * (0 - bottom.last.y)) / (bottom.last.y - p.y)
		));
	}

	bottom.last = p;

	if( (p.y >= 0) && (closing != Bottom) )
		clipTop(p);

}

void PolyClip3DI::clipTop(Point3DI p)
{
	SDimension limit = SDimension(view->getPixelHeight() - 1);
	
	if(top.needFirst)
	{
		top.needFirst = False;
		top.first = p;
		top.last = p;
	}

	else if( (p.y <  limit) && (top.last.y >= limit) )
	{
		destPoly->add(Point3DI(
				p.x + ( (p.x - top.last.x) * (limit - p.y)) / (p.y - top.last.y),
				limit,
				p.z + ( (p.z - top.last.z) * (limit - p.y)) / (p.y - top.last.y),
				p.intensity + ( (p.intensity - top.last.intensity) * (limit - p.y)) / (p.y - top.last.y)
			));
	}
	else if( (p.y >= limit) && (top.last.y <  limit) )
	{
		destPoly->add(Point3DI(
				top.last.x + ( (top.last.x - p.x) * (limit - top.last.y)) / (top.last.y - p.y),
				limit,
				top.last.z + ( (top.last.z - p.z) * (limit - top.last.y)) / (top.last.y - p.y),
				top.last.intensity + ( (top.last.intensity - p.intensity) * (limit - top.last.y)) / (top.last.y - p.y)
			));
	}

	top.last = p;

	if( (p.y < limit) && (closing != Top) )
		destPoly->add(p);

}


void Polygon3DI::clipPoly(Polygon3DI* destPoly, const Region* bitmap) const
{
}

