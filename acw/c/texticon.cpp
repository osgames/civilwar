/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Text Input Icon
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.6  1994/07/11  14:26:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/06/29  21:42:34  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.3  1994/05/25  23:31:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/05/19  17:44:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/04/26  13:39:07  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <ctype.h>
#include "texticon.h"
#include "text.h"
#include "menudata.h"
#include "colours.h"
#include "system.h"
#include "screen.h"
#include "timer.h"

#if defined(COMPUSA_DEMO)
class GotoQuitGame {
};
#endif

// InputIcon::InputIcon(IconSet* set, Rect r, char* buf, size_t size, FontID f, Boolean number) :
InputIcon::InputIcon(IconSet* set, const Rect& r, char* buf, size_t size, FontID f) :
	Icon(set, r)
{
	buffer = buf;
	length = size - 1;
	font = f;
	active = False;
	// numeric = number;

	cursorPos = strlen(buf);
}


void InputIcon::execute(Event* event, MenuData* d)
{
	if(!d->currentInput)
	{
		d->currentInput = this;
		active = True;
		setRedraw();
	}

	if(event->buttons)
	{
		if(!active)
		{
			d->currentInput->active = False;
			d->currentInput = this;
			active = True;
			setRedraw();
		}
	}

	if(active)
	{
		if(event->key)
		{
			switch(event->key)
			{
			/*
		 	 * Deal with special case keys first
		 	 */

			case KEY_Escape:			// Escape Clears input
#if defined(COMPUSA_DEMO)
				throw GotoQuitGame();
#else
				cursorPos = 0;
				buffer[0] = 0;
				return;
#endif
			case KEY_Left:
			case KEY_CtrlLeft:
			case KEY_AltLeft:
				if(cursorPos)
					cursorPos--;
				break;


			case KEY_Right:
			case KEY_CtrlRight:
			case KEY_AltRight:
				if(buffer[cursorPos])
					cursorPos++;
				break;

			case KEY_Delete:				// Forward delete
			case KEY_CtrlDelete:
			case KEY_AltDelete:
				{
					size_t i = cursorPos;
					while((buffer[i] != 0) && (i < length))
					{
						buffer[i] = buffer[i + 1];

						i++;
					}
				}
				break;


			case KEY_BackSpace:			// Backwards delete
			case KEY_CtrlBackSpace:
			case KEY_AltBackSpace:
				if(cursorPos)
				{
					cursorPos--;

					size_t i = cursorPos;
					while(i < length)
					{
						buffer[i] = buffer[i + 1];

						i++;
					}
				}
				break;



			case KEY_Home:
			case KEY_CtrlHome:
			case KEY_AltHome:
				cursorPos = 0;
				break;

			case KEY_End:
			case KEY_CtrlEnd:
			case KEY_AltEnd:
				cursorPos = strlen(buffer);
				break;

			default:

				/*
				 * Call virtual key processor
				 */

				event->key = processKey(event->key);

#if 0
				if(numeric && !isdigit(event->key))
					event->key = 0;
#endif

				if( (event->key >= ' ') && (event->key < 127) )
				{
					/*
				 	 * Add key to buffer
				 	 */

					size_t buflen = strlen(buffer);

					if(buflen < length)
					{
						/*
					 	* Shift rest of string across
					 	*/

						size_t i = buflen;

						if(i >= length)
						{
							i = length - 2;
							buffer[length] = 0;
						}

						while(i > cursorPos)
						{
							buffer[i + 1] = buffer[i];

							i--;
						}
						buffer[cursorPos + 1] = buffer[cursorPos];
						buffer[cursorPos++] = char(event->key);
					}

					break;
				}
				else
					return;
			}

			setRedraw();
			event->key = 0;
		}
	}
}


void InputIcon::drawIcon()
{
	Region bitmap(machine.screen->getImage(), Rect(getPosition(), getSize()));
	bitmap.fill(White);
	TextWindow window(&bitmap, font);

	Colours cursorCol = 
		(timer->getCount() & 0x10) ?
			Blue : Red;

	bitmap.frame(0, 0, getW(), getH(), DGrey, LGrey);

	SDimension fontHeight = window.getFont()->getHeight();
	SDimension y = SDimension((getH() - fontHeight) / 2);

	bitmap.frame(2, SDimension(y-2), SDimension(getW() - 4), SDimension(fontHeight + 4), DGrey, LGrey);
	bitmap.box  (3, SDimension(y-1), SDimension(getW() - 6), SDimension(fontHeight + 2), Black);

#ifdef DEBUG1
	machine.blit(bitmap, getPosition());
#endif

	window.setColours(Black);
	window.setPosition(Point(4, y));
	size_t i = 0;
	while(buffer[i])
	{
		if(active && (i == cursorPos))
			window.setColours(LBlue, cursorCol);
		else
			window.setColours(White);

		window.draw(buffer[i]);
 
#ifdef DEBUG1
		machine.blit(bitmap, getPosition());
#endif

		i++;
	}
	
	if(active && (i == cursorPos))
	{
		const Point& p = window.getPosition();

		bitmap.box(p.x, p.y, window.getFont()->getWidth(' '), window.getFont()->getHeight(), cursorCol); 
#ifdef DEBUG1
		machine.blit(bitmap, getPosition());
#endif
	}

	machine.screen->setUpdate(bitmap);

	if(active)
		setRedraw();		// Get it redrawn so cursor flashes!
}

Key NumberInputIcon::processKey(Key k)
{
	if(!isdigit(k))
		return 0;
	else
		return k;
}
