/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Read in LBM files
 *
 * Written by Steven Green
 *
 * Credit where credit's due...
 *   This was mostly stolen from the source to TILE.C, which was written
 *   by Mark McCubbins, who in turn borrowed bits from Dave Shea.  I
 *   of course hacked it about a lot since...
 *
 *	Converted to C++ for use in Dagger Games December 1993
 *
 * It is a bit messy because of it being converted rather than rewritten
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.12  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.11  1994/05/04  22:09:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/04/05  12:28:09  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/01/20  20:04:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/01/17  20:14:26  Steven_Green
 * File is closed after loading.
 *
 * Revision 1.7  1994/01/07  23:43:18  Steven_Green
 * Some optimisation
 *
 * Revision 1.6  1994/01/06  22:38:30  Steven_Green
 * filename is const
 *
 * Revision 1.5  1993/12/23  09:26:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1993/12/21  00:31:04  Steven_Green
 * Exception class added.
 *
 * Revision 1.3  1993/12/16  22:19:35  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/12/04  01:06:10  Steven_Green
 * readBODY implemented.
 *
 * Revision 1.1  1993/12/03  16:45:51  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


// #define USE_STREAMS		// Uncomment this to use C++ Streams



#include <stdio.h>
#include <string.h>

#ifdef USE_STREAMS
#include <fstream.h>
#else
#include <stdio.h>
#endif

#include "ilbm.h"
#include "ilbmdata.h"
#include "makename.h"
#include "image.h"
#include "palette.h"
#include "cd_open.h"

/*
 * Forward references
 */

struct BMHD;

/*
 * Class to simplify file reading
 */


class ilbmstream  {
public:
#ifdef USE_STREAMS
	ifstream ilbms;

	ilbmstream(char* s,
			ios::openmode mode = ios::in |
										ios::binary |
										ios::nocreate) { fileOpen(ilbms, s, mode); }

	~ilbmstream() { ilbms.close(); }
#else
	FILE* fp;
	ilbmstream(char* s) { fp = FileOpen(s, "rb"); }
	~ilbmstream() { if(fp) fclose(fp); }
#endif

	Boolean isGood()
	{
	#ifdef USE_STREAMS
		return ilbms.good();
	#else
		return !ferror(fp) && !feof(fp);
	#endif
	}

	void read(void* ad, size_t length)
	{
	#ifdef USE_STREAMS
		ilbms.read(ad, length);
	#else
		fread(ad, length, 1, fp);
	#endif
	}

	ULONG readLong()
	{
		UBYTE b[4];

	#ifdef USE_STREAMS
		ilbms.read(b, 4);
	#else
		read(b, sizeof(b));
	#endif

		return (b[0] << 24) + (b[1] << 16) + (b[2] << 8) + b[3];
	}

	UWORD readWord()
	{
		UBYTE b[2];
	#ifdef USE_STREAMS
		ilbms.read(b, 2);
	#else
		read(b, sizeof(b));
	#endif
		return UWORD((b[0] << 8) + b[1]);
	}

	UBYTE readByte()
	{
	#ifdef USE_STREAMS
		return UBYTE(ilbms.get());
	#else
		return fgetc(fp);
	#endif
	}

	int readBMHD(BMHD& bmh);
	int readBODY(BMHD& bmhd, Image& image);
};

class ChunkType {
	char	ckID[4];
public:
	Boolean is(const char* type) const { return !strncmp(ckID, type, 4); };
	virtual int read(ilbmstream& f);
};

class Chunk : public ChunkType {
	ULONG	ckSize;
public:
	int read(ilbmstream& f);
	ULONG size() const { return ckSize; };
};


/*
 * Chunk functions
 */



int ilbmstream::readBMHD(BMHD& bmhd)
{
	bmhd.w 						= readWord();
	bmhd.h 						= readWord();
	bmhd.x 						= readWord();
	bmhd.y 						= readWord();
	bmhd.nPlanes 				= readByte();
	bmhd.masking 				= readByte();
	bmhd.compression 			= readByte();
	bmhd.pad1 					= readByte();
	bmhd.transparentColor 	= readWord();
	bmhd.xAspect 				= readByte();
	bmhd.yAspect 				= readByte();
	bmhd.pageWidth 			= readWord();
	bmhd.pageHeight 			= readWord();

#ifdef DEBUG1
	cout << "Bitmap Header:\n" <<
		"w = " 				<< bmhd.w 						<< ", " <<
		"h = " 				<< bmhd.h 						<< "\n" <<
		"x = " 				<< bmhd.x 						<< ", " <<
		"y = " 				<< bmhd.y 						<< "\n" <<
		"nPlanes = " 		<< int(bmhd.nPlanes) 		<< "\n" <<
		"masking = "		<< int(bmhd.masking)			<< "\n" <<
		"compression = "	<< int(bmhd.compression)	<< "\n" <<
		"transparent = "	<< bmhd.transparentColor	<< "\n" <<
		"xAspect = "		<< int(bmhd.xAspect)			<< ", " <<
		"yAspect = "		<< int(bmhd.yAspect)			<< "\n" <<
		"pageWidth = "		<< bmhd.pageWidth				<< ", " <<
		"pageHeight = "	<< bmhd.pageHeight			<< endl;
#endif

	return isGood();
}

int ilbmstream::readBODY(BMHD& bmhd, Image& image)
{
	if(image.getWidth() == 0)
		image.resize(bmhd.w, bmhd.h);
#ifdef TESTING
	else if( (bmhd.w != image.getWidth()) || (bmhd.h > image.getHeight()) )
		throw GeneralError("Trying to read ILBM into wrong sized image\n%d,%d -> %d,%d\n",
			bmhd.w, bmhd.h,
			image.getWidth(),
			image.getHeight());
#endif

	UBYTE* buffer = image.getAddress();

	UWORD fileWidth = bmhd.w;
	if(fileWidth & 1)
		fileWidth++;

	UBYTE* lineBuf = new UBYTE[fileWidth];

	int lineCount = bmhd.h;

	switch(bmhd.compression)
	{
	case cmpNone:
		while(lineCount--)
		{
			read(lineBuf, fileWidth);
			memcpy(buffer, lineBuf, bmhd.w);
			buffer += bmhd.w;
		}
		break;

	case cmpByteRun:

		while(lineCount--)
		{
			UBYTE* bufPtr = lineBuf;
			int colCount = fileWidth;
			while(colCount > 0)
			{
				UBYTE v = readByte();

				if(v > 127)		// Byte Run
				{
					int count = 256 - v;

					v = readByte();

					colCount -= count + 1;

					do
						*bufPtr++ = v;
					while(count--);
				}
				else		// Byte Copy
				{
					int count = v + 1;

					colCount -= count;

					read(bufPtr, count);

					bufPtr += count;
				}
			}

			memcpy(buffer, lineBuf, bmhd.w);
			buffer += bmhd.w;
		}

#ifdef DEBUG1
		if(l != 0)
			cout << "Warning: Byte Count did not match" << endl;
#endif
		break;

	default:
		return -1;

	}

	delete[] lineBuf;

	return isGood();
}

	
int ChunkType::read(ilbmstream& f)
{
	f.read(ckID, sizeof(ckID));

#ifdef DEBUG1
	if(f.good())
		cout << "Chunk Id = " << ckID[0] << ckID[1] << ckID[2] << ckID[3] << endl;
#endif

	return f.isGood();
}
 
 
int Chunk::read(ilbmstream& f)
{
	ChunkType::read(f);
	ckSize = f.readLong();
#ifdef DEBUG1
	if(f.good())
		cout << "Chunk Size = " << ckSize << endl;
#endif

	return f.isGood();
}


/*
 * Read an ILBM file
 *
 * returns:
 *   0 = OK
 *  non zero = ERROR
 *
 * parameters:
 *		name : Name of file to read
 *	 image : Where to put the image.  The image will be adjusted to be
 *          the correct size if its size is 0.  If NULL then the image
 *				is not loaded
 *  palette: Where to put the palette.  If NULL then the palette is not
 *           loaded.
 */

int readILBM(const char* name, Image* image, Palette* palette, BMHD* header)
{
	int retVal = -1;

	char fileName[FILENAME_MAX];
	makeFilename(fileName, name, ".LBM", False);

#ifdef DEBUG1
	cout << "Reading " << fileName << endl;
#endif
	
	ilbmstream file(fileName);

#ifdef USE_STREAMS
	if(file.ilbms)
#else
	if(file.fp)
#endif
	{
		Chunk chunk;

		chunk.read(file);

		if(chunk.is("FORM"))
		{
			/*
			 * Get the type
			 */

			ChunkType fileType;

			fileType.read(file);

			if(fileType.is("PBM "))
			{
				BMHD bmhd;		// Bitmap header
				Boolean gotBMHD = False;
				Boolean gotBODY = False;

				while(chunk.read(file))
				{
#ifdef USE_STREAMS
					streampos where = file.ilbms.tellg();
#else
					long where = ftell(file.fp);
#endif

					/*
					 * Specific type here....
					 */

					if(chunk.is("BMHD"))
					{
#ifdef DEBUG1
						cout << "Reading BMHD" << endl;
#endif
						if(file.readBMHD(bmhd))
							gotBMHD = True;
						else
							throw GeneralError("Error reading BMHD");

						if(header)
							*header = bmhd;
					}
					else if(chunk.is("CMAP"))
					{
						if(palette)
						{
#ifdef DEBUG1
							cout << "Reading CMAP" << endl;
#endif
							for(int i = 0; i < 256; i++)
							{
								UBYTE c[3];

								file.read(c, sizeof(c));

								palette->setColour(i, c);
							}

							if(!file.isGood())
								throw GeneralError("Error reading CMAP");
						}
#ifdef DEBUG1
						else
							cout << "Skipping CMAP" << endl;
#endif
					}
					else if(chunk.is("BODY"))
					{
						if(image)
						{
#ifdef DEBUG1
							cout << "Reading BODY" << endl;
#endif
							if(!gotBMHD)
								throw GeneralError("BMHD Missing");

							if(!file.readBODY(bmhd, *image))
								throw GeneralError("Error reading Body");
						}
#ifdef DEBUG1
						else
							cout << "Skipping BODY" << endl;
#endif
						gotBODY = True;
					}
#ifdef DEBUG1
					else
						cout << "Ignoring Chunk" << endl;
#endif
#ifdef USE_STREAMS
					file.ilbms.seekg(where + ((chunk.size() + 1) & ~1));	// Move to next chunk
#else
					fseek(file.fp, where + ((chunk.size() + 1) & ~1), SEEK_SET);
#endif
				}

				if(gotBMHD && gotBODY)
					retVal = 0;
				else
					throw GeneralError("Error reading Picture");
			}
			else
				throw GeneralError("Picture is not PBM");
		}
	}
	else
	{
		throw GeneralError("%s does not exist", fileName);
	}

	return retVal;

}


