/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battle Info for regiments
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/04/06  12:40:46  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "regbat.h"
#include "generals.h"

/*
 * Constructor
 */

RegimentBattle::RegimentBattle(Regiment* r)
{
	regiment = r;
}

/*
 * Destructor
 */

RegimentBattle::~RegimentBattle()
{
	regiment->battleInfo = 0;
}
