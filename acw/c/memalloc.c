/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Memory Manager
 * Replaces malloc and free
 *
 * Improvements over system functions are:
 *   Seperate heaps are maintained for small blocks (<256 bytes) thus
 *   reducing fragmentation.
 *
 *   DOS memory will be used when DPMI memory has run out
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <i86.h>
#include "memalloc.h"
#include "types.h"

#if !defined(SYSTEM_MALLOC)


#ifdef DEBUG
#define MEM_CHECK
// #define CHECK_POOLS
#include <conio.h>
#endif



#ifdef DEBUG

/*==========================================================
 * Running Totals
 */

size_t currentAllocated = 0;
size_t maximumAllocated = 0;
long mallocCalls = 0;
long freeCalls = 0;

#endif

/*
 * Types of memory
 */

typedef enum {
#if !defined(NO_DOS)
	MEM_DOS,				// Has been allocated via DOS
#endif
	MEM_DPMI,			// Allocated via DPMI
	MEM_Block			// Belongs to another MemPool
} MemType;

/*
 * Memory Block
 */

typedef struct {
  	size_t length;		// Length including header
	void* next;
} FreeMemBlock;

typedef struct {
  	size_t length;		// Length including header
#ifdef DEBUG
	char id[4];
#endif
} UsedMemBlock;

/*
 * Global Memory Blocks as received directly from DPMI or DOS
 */

typedef struct _MemPool MemPool;

struct _MemPool {
#ifdef MEM_CHECK
	char id[8];					// "MemPool\0"
#endif
	MemPool* next;
	// MemType type;
	UBYTE type;					// Really MemType, but sizeof enum is compiler dependant
	UBYTE reserved[3];		// To keep it multiple of 4 bytes
	void* address;
	ULONG handle;				// Selector or DPMI handle
	size_t length;				// Length not including this header
	size_t amountUsed;		// Running totals to speed up usage
	size_t amountFree;
	FreeMemBlock* freeBlocks;
};

typedef struct {
	MemPool* entry;
} PoolList;

/*=========================================================
 * Global Variables
 */

PoolList globalMemPools = { 0 };		// List of memory pools
PoolList smallMemPools = { 0 };		// Memory pools used for small data
PoolList hugeMemPools = { 0 };

#define SmallAlloc 256							// Memory size to use small pools
#define SmallBlockSize ((SmallAlloc * 16)	- sizeof(UsedMemBlock)) // Amount to allocate at once
#define BigAlloc 0x20000						// Bigger than 128K

// #define PoolBlockSize 0x10000		// 		Amount to allocate for pool blocks
#define PoolBlockSize 0x8000		// 		Amount to allocate for pool blocks

#ifdef MEM_CHECK
static char poolID[] = "MemPool";
#endif

#ifdef DEBUG
static char usedMemID[] = "MEMB";
#endif

/*=========================================================
 * Memory Information
 */


void setUMBstatus(UWORD status);
#pragma aux setUMBstatus =	\
	"mov ax,5803h"		\
	"int 21h"			\
	parm [bx]			\
	modify [eax ebx ecx edx]

UBYTE getUMBstatus();
#pragma aux getUMBstatus = \
	"mov ax,5802h"		\
	"int 21h"			\
	modify [eax ebx ecx edx]	\
	value [al]


MemInfo memInfo;			// Global Information

static Boolean gotMemInfo = False;

void setupMemInfo()
{
	/*
	 * Get DPMI Info
	 */

	struct {
		unsigned long largest;					// Largest free block of DPMI memory
		unsigned long maxPage;
		unsigned long largeBlock;
		unsigned long linSpace;					// Total Linear memory Space
		unsigned long freePages;				// Total Number of free pages
		unsigned long physPages;				// Total Number of free Physical Pages
		unsigned long totalPhysicalPages;	// Total Number of Physical Pages
		unsigned long freeLinSpace;			// Free Linear Space
		unsigned long sizeofPage;				// Size of paging partition
		unsigned long reserved[3];
	} DPMIInfo;

	union REGS regs;
	struct SREGS sregs;

	gotMemInfo = True;


	regs.x.eax = 0x500;
	memset(&sregs, 0, sizeof(sregs));
	sregs.es = FP_SEG(&DPMIInfo);
	regs.x.edi = FP_OFF(&DPMIInfo);
	int386x(0x31, &regs, &regs, &sregs);

	/*
	 * DOS Memory
	 */

	setUMBstatus(1);

	memset(&sregs, 0, sizeof(sregs));
	regs.x.eax = 0x0100;
	regs.x.ebx = 0xffff;		// Allocate more than is available

	int386(0x31, &regs, &regs);

	memInfo.totalPhysical = DPMIInfo.totalPhysicalPages * 0x1000;
	memInfo.freePhysical = DPMIInfo.largest;	// In reality this is it...
	memInfo.freeDOS = regs.w.bx << 4;

}

/*=========================================================
 * Get some raw memory 
 */

void* getDPMImemory(size_t amount, ULONG* handle)
{
	/*
	 * Replace this with assembly!
	 */

	union REGS regs;
	regs.w.ax = 0x501;
	regs.w.bx = amount >> 16;
	regs.w.cx = amount & 0xffff;

	int386(0x31, &regs, &regs);

	if(regs.w.cflag & INTR_CF)
		return 0;

	*handle = (regs.w.si << 16) + regs.w.di;

	return (void*)((regs.w.bx << 16) + regs.w.cx);
}

void freeDPMImemory(ULONG handle)
{
	union REGS regs;
	regs.w.ax = 0x502;
	regs.w.si = handle >> 16;
	regs.w.di = handle & 0xffff;

	int386(0x31, &regs, &regs);
}

#if !defined(NO_DOS)

void* getDOSmemory(size_t amount, ULONG* handle)
{
	if(amount > (0x10000 - 16))
		return 0;
	else
	{
		union REGS regs;
		regs.w.ax = 0x100;
		regs.w.bx = (amount + 15) >> 4;

		int386(0x31, &regs, &regs);

		if(regs.w.cflag & INTR_CF)
			return 0;

		*handle = regs.w.dx;

		return (void*)(regs.w.ax << 4);
	}
}

void freeDOSmemory(ULONG handle)
{
	union REGS regs;
	regs.w.ax = 0x101;
	regs.w.dx = handle;

	int386(0x31, &regs, &regs);
}

#endif	// NO_DOS

#ifdef MEM_CHECK

/*=========================================================
 * Check Pools for corruption
 */

void memoryError(const char* s)
{
	cprintf("Memory Error: %s\r\n", s);
}

#ifdef CHECK_POOLS

void checkPool(PoolList* poolList)
{
	MemPool* pool = poolList->entry;
	while(pool)
	{
		FreeMemBlock* block;

		if(strcmp(pool->id, poolID) != 0)
		{
			// An error!!!

			memoryError("Pool has wrong ID!!!");
		}


		block = pool->freeBlocks;
		while(block)
		{
			// Check Block...
			block = block->next;
		}

		pool = pool->next;
	}
}

void checkPools()
{
	checkPool(&hugeMemPools);
	checkPool(&globalMemPools);
	checkPool(&smallMemPools);
}

#endif	// CHECK_POOL
#endif	// MEM_CHECK

#define roundUp(start, pSize)	(((start) + (pSize) - 1) & ~((pSize) - 1))

/*=========================================================
 * Create new pools
 *
 * If smaller than PoolBlockSize a full block will try to be allocated
 * If not enough space, then the actual amount is allocated
 *
 * If there is no DPMI memory, then DOS memory is allocated instead
 */

MemPool* makeNewPool(size_t amount)
{
	MemPool* pool = 0;
	size_t bigAmount;
	size_t wantAmount;
	ULONG handle;
	MemType memType;

	/*
	 * If this is the 1st call, then get initial memory configuration
	 */

	if(!gotMemInfo)
		setupMemInfo();

	amount += sizeof(MemPool);

	bigAmount = amount;

	if(bigAmount < PoolBlockSize)
		bigAmount = PoolBlockSize;

#if !defined(NO_DOS)
	/*
	 * If < 64K try DOS memory 1st
	 */

	// if(amount < (0x10000 - 16))
	if(bigAmount < (0x10000 - 16))
	{
		memType = MEM_DOS;
		wantAmount = roundUp(bigAmount, 0x10);		// Round to 16 Bytes
		pool = (MemPool*)getDOSmemory(wantAmount, &handle);

		if(!pool)
		{
			wantAmount = roundUp(amount, 0x10);		// Round to 16 Bytes
			pool = (MemPool*)getDOSmemory(wantAmount, &handle);
		}
	}
#endif	// NO_DOS

	/*
	 * Try DPMI memory
	 */

	if(!pool)
	{

		handle = 0;
		memType = MEM_DPMI;

		wantAmount = roundUp(bigAmount, 0x1000);		// Round to 4K
		pool = (MemPool*)getDPMImemory(wantAmount, &handle);	// temporarily use malloc
	}

	if(!pool)
	{
		wantAmount = roundUp(amount, 0x1000);		// Round to 4K
		pool = (MemPool*)getDPMImemory(wantAmount, &handle);	// temporarily use malloc
	}

#if !defined(NO_DOS)
	/*
	 * Try DOS memory again
	 */

	if(!pool && (bigAmount < 0x10000))
	{
		memType = MEM_DOS;

		wantAmount = roundUp(bigAmount, 0x10);		// Round to 16 Bytes
		pool = (MemPool*)getDOSmemory(wantAmount, &handle);
	}

	if(!pool && (amount < 0x10000))
	{
		wantAmount = roundUp(amount, 0x10);		// Round to 16 Bytes
		pool = (MemPool*)getDOSmemory(wantAmount, &handle);
	}
#endif		// NO_DOS

	if(!pool)
			return 0;

	wantAmount -= sizeof(MemPool);

	pool->type = memType;
	pool->handle = handle;
	pool->address = pool + 1;
	pool->length = wantAmount;
	pool->amountUsed = 0;
	pool->amountFree = wantAmount;
	pool->freeBlocks = 0;

#ifdef MEM_CHECK
	strcpy(pool->id, poolID);
#endif

	return pool;
}

/*=========================================================
 * Allocate from pool functions
 */

void* poolAlloc(PoolList* poolList, size_t amount)
{
	size_t totalAmount;

	/*
	 * Round up the amount to a long alignment
	 */

	amount = (amount + 3) & ~3;
	totalAmount = amount + sizeof(UsedMemBlock);

	for(;;)
	{
		MemPool* pool = poolList->entry;
		FreeMemBlock* block;
		UsedMemBlock* usedBlock;

		while(pool)
		{
			if(pool->amountFree >= totalAmount)
			{
				FreeMemBlock* lastBlock = 0;
				block = pool->freeBlocks;

				while(block)
				{
					if(block->length >= totalAmount)
					{
						/*
					 	 * We found a big enough block... use it
					 	 */

						/*
					 	 * Consider splitting it
					 	 */

						if(block->length >= (totalAmount + sizeof(UsedMemBlock) + 4))
						{
							FreeMemBlock* newBlock = (FreeMemBlock*)(((char*)block) + totalAmount);

							newBlock->next = block->next;
							newBlock->length = block->length - totalAmount;

							block->next = newBlock;
							block->length = totalAmount;
						}

						/*
					 	 * remove from list
					 	 */

						if(lastBlock)
							lastBlock->next = block->next;
						else
							pool->freeBlocks = block->next;

						pool->amountUsed += block->length;
						pool->amountFree -= block->length;
#ifdef CHECK_POOLS
						checkPools();
#endif
						usedBlock = (UsedMemBlock*)block;
#ifdef DEBUG
						memcpy(usedBlock->id, usedMemID, 4);
						memset((void*)(usedBlock+1), 'S', usedBlock->length - sizeof(UsedMemBlock));
#endif						
						return (void*)(usedBlock + 1);

					}

					lastBlock = block;
					block = block->next;
				}
			}

			pool = pool->next;
		}

		/*
 	 	 * Nothing was found, which means we must allocate a new block
	 	 */

		if(amount <= SmallAlloc)
		{
			pool = poolAlloc(&globalMemPools, SmallBlockSize);

			if(!pool)
				break;

			pool->type = MEM_Block;
			pool->address = pool + 1;
			pool->length = SmallBlockSize - sizeof(MemPool);
			pool->amountUsed = 0;
			pool->amountFree = SmallBlockSize - sizeof(MemPool);
			pool->freeBlocks = 0;
#ifdef MEM_CHECK
			strcpy(pool->id, poolID);
#endif
		}
		else
		{
			pool = makeNewPool(amount + sizeof(UsedMemBlock));

			if(!pool)
				break;
		}


		pool->next = poolList->entry;
		poolList->entry = pool;

		block = (FreeMemBlock*)(pool + 1);
		pool->freeBlocks = block;

		block->next = 0;
		block->length = pool->amountFree;
#ifdef CHECK_POOLS
		checkPools();
#endif
	}	

	return 0;
}

/*=========================================================
 * User callable Malloc function
 */

void* malloc(size_t amount)
{
	void* result;

#ifdef CHECK_POOLS
	checkPools();
#endif

	if(amount <= SmallAlloc)
		result = poolAlloc(&smallMemPools, amount);
	else if(amount >= BigAlloc)
		result = poolAlloc(&hugeMemPools, amount);
	else
		result = poolAlloc(&globalMemPools, amount);

#ifdef DEBUG
	if(result)
		memset(result, 0x55, amount);
#endif

#ifdef CHECK_POOLS
	checkPools();
#endif

#ifdef DEBUG
	if(result)
	{
		UsedMemBlock* block = ((UsedMemBlock *)result) - 1;

		currentAllocated += block->length;
		if(currentAllocated > maximumAllocated)
			maximumAllocated = currentAllocated;
		mallocCalls++;
	}
#endif
	
	return result;
}

void* _nmalloc(size_t amount)
{
	return malloc(amount);
}

/*=========================================================
 * Pool free
 */

void poolFree(PoolList* poolList, UsedMemBlock* block)
{
	MemPool* pool = poolList->entry;
	MemPool* lastPool = 0;

	while(pool)
	{
		if((block >= pool->address) && ((char*)block < ((char*)pool->address + pool->length)))
		{
			pool->amountUsed -= block->length;
			pool->amountFree += block->length;

			if(pool->amountUsed == 0)
			{
				if(lastPool)
					lastPool->next = pool->next;
				else
					poolList->entry = pool->next;

				if(pool->type == MEM_Block)
				{
					free(pool);
				}
#if !defined(NO_DOS)
				else if(pool->type == MEM_DOS)
				{
					freeDOSmemory(pool->handle);
				}
#endif	// NO_DOS
				else if(pool->type == MEM_DPMI)
				{
					freeDPMImemory(pool->handle);
				}
			}
			else
			{

				/*
			 	 * Add to free memory list
			 	 */

				FreeMemBlock* last = 0;
				FreeMemBlock* blk = pool->freeBlocks;
				FreeMemBlock* us = (FreeMemBlock*)block;

				while(blk && (blk < us))
				{
					last = blk;
					blk = blk->next;
				}

				us->next = blk;
				if(last)
					last->next = us;
				else
					pool->freeBlocks = us;

				/*
			 	 * Attempt to join adjacent blocks
			 	 * TBW...
			 	 */

				if(last && ( (((char*)last) + last->length) == (char*)us))
				{
					last->length += us->length;
					last->next = us->next;
					us = last;
				}

				if(blk && ( (((char*)us) + us->length) == (char*)blk))
				{
					us->length += blk->length;
					us->next = blk->next;
				}
			
			}
#ifdef CHECK_POOLS
			checkPools();
#endif
			return;
		}

		lastPool = pool;
		pool = pool->next;
	}

#ifdef CHECK_POOLS
	checkPools();
#endif

	/*
	 * Not found!!!
	 */

#ifdef DEBUG
	cprintf("Memory Block %p (%ld) not in any memory pool!!!\r\n", block, block->length);
#ifdef MEM_CHECK
	memoryError("Memory not in pool");
#endif
#endif

}

/*=========================================================
 * User callable Free function
 */

void free(void* ad)
{
#ifdef DEBUG
	if(ad == (void*)0xaaaaaaaa)
		memoryError("Freeing 0xaaaaaaaa");
	if(ad == (void*)0x55555555)
		memoryError("Freeing 0x55555555");
#endif

	if(ad)
	{
		UsedMemBlock* block = ((UsedMemBlock *)ad) - 1;

#ifdef DEBUG
		if(memcmp(block->id, usedMemID, 4))
		{
			memoryError("Freeing unallocated block of memory!");
		}

		memset(ad, 0xaa, block->length - sizeof(UsedMemBlock));

#endif

#ifdef CHECK_POOLS
		checkPools();
#endif

#ifdef DEBUG
		currentAllocated -= block->length;	// - sizeof(UsedMemBlock);
		freeCalls++;
#endif

		if(block->length <= (SmallAlloc + sizeof(UsedMemBlock)))
			poolFree(&smallMemPools, block);
		else if(block->length >= (BigAlloc + sizeof(UsedMemBlock)))
			poolFree(&hugeMemPools, block);
		else
			poolFree(&globalMemPools, block);

#ifdef CHECK_POOLS
		checkPools();
#endif
	}
}

void _nfree(void* ad)
{
	free(ad);
}


/*=========================================================
 * User callable Realloc function
 *
 * This could be more intelligent by:
 *   If n < existing size
 *     shrink block
 *   else see if free block follows
 *     expand block
 *   else
 *     copy to new area
 */

void* realloc(void* ad, size_t n)
{
	void* newAd = 0;
	UsedMemBlock* block = 0;

#ifdef DEBUG
	if(ad == (void*)0xaaaaaaaa)
		memoryError("Reallocing 0xaaaaaaaa");
	if(ad == (void*)0x55555555)
		memoryError("Reallocing 0x55555555");
#endif

	if(ad)
	{
		size_t amount = (n + 3) & ~3;
		amount += sizeof(UsedMemBlock);

		block = ((UsedMemBlock *)ad) - 1;

#ifdef DEBUG
		if(memcmp(block->id, usedMemID, 4))
		{
			memoryError("Reallocing unallocated block of memory!");
		}
#endif

		/*
		 * If new size is smaller than previous size
		 */

		if(block->length >= amount)
		{
#if 0			//.. This is more complicated than this
			size_t leftOver = block->length - amount;

			/*
			 * Consider splitting into 2.
			 */

			if(leftOver >= sizeof(UsedMemBlock) + 4)
			{
				FreeMemBlock* newBlock = (FreeMemBlock*)(((char*)block) + amount);


				newBlock->next = block->next;
				newBlock->length = block->length - amount;

				block->next = newBlock;
				block->length = amount;
			}
#endif

			return ad;
		}

		/*
		 * If there is sufficient memory following this block
		 */

		//... Can't be bothered writing this!

	}

#ifdef CHECK_POOLS
		checkPools();
#endif

	newAd = malloc(n);
	
	if(ad)
	{
		memcpy(newAd, ad, block->length - sizeof(UsedMemBlock));

		free(ad);
	}

	return newAd;
}

void* _nrealloc(void* ad, size_t n)
{
	return realloc(ad, n);
}

#ifdef DEBUG
/*=========================================================
 * Show current state of memory pools
 */

void showPool(PoolList* poolList)
{
	FreeMemBlock* block;
	MemPool* pool = poolList->entry;
	while(pool)
	{
		cprintf("Pool %p, start %p, length=%ld, used=%ld, free=%ld\r\n", pool, pool->address, pool->length, pool->amountUsed, pool->amountFree);

		block = pool->freeBlocks;
		while(block)
		{
			cprintf("  Block %p, length %ld\r\n", block, block->length);
			block = block->next;
		}

		pool = pool->next;
	}
}

void showPoolInfo()
{
	cprintf("\r\nHuge pool\r\n");
	showPool(&hugeMemPools);
	cprintf("\r\nGlobal pool\r\n");
	showPool(&globalMemPools);
	cprintf("\r\nSmall Pool\r\n");
	showPool(&smallMemPools);
}
#endif

#endif	// SYSTEM_MALLOC

#ifdef TESTING_MEMALLOC

/*=========================================================
 * Test Program
 */

void main()
{
	char* a = malloc(0x30);
	char* b = malloc(0x40);
	char* c = malloc(0x60);
	char* d;
	char* e;

	cprintf("a = %p\r\n", a);
	cprintf("b = %p\r\n", b);
	cprintf("c = %p\r\n", c);

	free(b);
	b = malloc(0x50);

	cprintf("b = %p\r\n", b);

	d = malloc(0x20);

	cprintf("d = %p\r\n", d);

	free(c);

	c = malloc(0x1000);

	cprintf("c = %p\r\n", c);

	e= malloc(0x40000);

	showPoolInfo();

	free(a);
	free(b);
	free(c);
	free(d);

	free(e);

	showPoolInfo();
}

#endif	// TESTING_MEMALLOC

