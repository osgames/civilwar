/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	System Icons
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */


#include "sysicon.h"
#include "system.h"
#include "screen.h"
#include "sprlib.h"
#include "colours.h"
#include "text.h"


#define Font_DummyIcon Font_EMFL15

/*
 * Basic Icon
 */

/*
 * Constructor
 */

SystemIcon::SystemIcon(IconSet* parent, SystemSprite n, Rect r, Key k1, Key k2) :
		Icon(parent, r, k1, k2),
		iconNumber(n)
{
	// Do nothing
}

/*
 * Draw simple icon
 */

void SystemIcon::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	machine.systemLibrary->drawCentredSprite(&bm, iconNumber);
	machine.screen->setUpdate(bm);
}

