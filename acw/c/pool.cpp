/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Memory Pool
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "pool.h"

Pool::Pool(size_t sz, int n)
{
	if(sz < sizeof(Link))
		sz = sizeof(Link);

	elSize = sz;
	blockSize = n;
	freeBlocks = 0;
	blocks = 0;
}


Pool::~Pool()
{
	while(blocks)
	{
		PoolBlock* b = blocks;
		blocks = blocks->next;

		delete[] b->data;
		delete b;
	}
}

void* Pool::allocate()
{
	if(!freeBlocks)
	{
		PoolBlock* pb = new PoolBlock;
		pb->data = new UBYTE[blockSize * elSize];
		pb->next = blocks;
		blocks = pb;

		int i = blockSize;
		UBYTE* mem = (UBYTE*)pb->data;

		while(i--)
		{
			Link* l = (Link*)mem;
			l->next = freeBlocks;
			freeBlocks = l;
			mem += elSize;
		}
	}

	if(freeBlocks)
	{
		void* r = freeBlocks;

		freeBlocks = freeBlocks->next;

		return r;
	}

	return 0;
}

void Pool::release(void* mem)
{
	Link* l = (Link*)mem;

	l->next = freeBlocks;
	freeBlocks = l;
}
