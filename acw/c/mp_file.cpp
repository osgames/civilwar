/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Multi-Player choose file...
 * One side is master, other is slave
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <string.h>
#include "mp_file.h"
#include "game.h"
#include "connect.h"
#include "filesel.h"
#include "strutil.h"
#include "cd_open.h"
#include "language.h"
#include "dialogue.h"

/*
 * A Simple MenuConnection class for sending information about
 * filenames
 */

class FileSelectConnection : public MenuConnection {
	char* fName;
public:
	Boolean gotFileName;
	Boolean fileOK;
public:
	FileSelectConnection(char* f);
	void processPacket(Packet* pkt);
};

FileSelectConnection::FileSelectConnection(char* f) :
	MenuConnection("FileSelect")
{
	fName = f;
	gotFileName = False;
	fileOK = False;
}

void FileSelectConnection::processPacket(Packet* packet)
{
	if(packet->sender != RID_Local)
	{
		switch(packet->type)
		{
		case Connect_LoadGame:
			if(packet->length)
				strcpy(fName, (char*) packet->data);
			else
				fName[0] = 0;
			gotFileName = True;
			break;
		case Connect_FileExists:
			fileOK = True;
			gotFileName = True;
			break;
		case Connect_FileNotExist:
			fileOK = False;
			gotFileName = True;
			break;
		}
	}
}

/*
 * Return False if Cancelled or some error occured
 * Return True if valid filename returned
 *
 * TBW: Need to check that file exists on both machines!
 */

Boolean getFileName(char* fileName, const char* prompt, const char* wildCard, Boolean mustExist)
{
	Boolean result = False;

	if(game->connection->isLocal())
	{
		strcpy(fileName, wildCard);

		if(fileSelect(fileName, prompt, mustExist) != 0)
			result = False;
		else
			result = True;
	}
	else
	{
		FileSelectConnection connect(fileName);

		if(game->connection->inControl)
		{
			strcpy(fileName, wildCard);

			if(fileSelect(fileName, prompt, mustExist) != 0)
			{
				game->connection->sendMessage(Connect_LoadGame, 0, 0);
				while(!game->connection->isLocal() &&
						game->connection->flush() &&
						!askRemoteNotRespond())
							;	// Do nothing
				result = False;
			}
			else
			{
				char* pData = copyString(fileName);
				game->connection->sendMessage(Connect_LoadGame, (UBYTE*)pData, strlen(pData) + 1);

  				/*
  			 	* Wait for other side to verify that file exists
  			 	*/

  				while(!connect.gotFileName)
					connect.processMessages(False);

				result = connect.fileOK;
			}
		}
		else
		{
			/*
		 	 * Wait for filename from other side
		 	 */

			{
				PopupText popup(language(lge_RemoteWaiting));

				while(!connect.gotFileName)
					connect.processMessages(False);
			}

			if(!fileName[0])
				result = False;
			else
			{

				/*
			 	 * Check if file exists and tell other side
			 	 */

				if(mustExist)
				{
					result = DoesFileExist(fileName);
					if(!result)
					{
						const char* newName = addPath(fileName);
						result = DoesFileExist(newName);
					}
				}
				else
					result = DoesFileExist(fileName);

	
				if(result)
				{
					game->connection->sendMessage(Connect_FileExists, 0, 0);
					result = True;
				}
				else
				{
					game->connection->sendMessage(Connect_FileNotExist, 0, 0);
					result = False;
				}
			}
		}

		connect.processMessages(True);	 
	}

	if(result && mustExist)
	{
		if(!DoesFileExist(fileName))
		{
			const char* newName = addPath(fileName);
			if(DoesFileExist(newName))
				strcpy(fileName, newName);
		}
	}

	return result;
}

