/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#include "textload.h"
#include <conio.h>
#include <fstream.h>


void main( int argc, char *argv[] )
{
 char *filename = argv[1];   //    "c:\\database\\text\\battles.asc";
 char format = *argv[2];
 char picincre = *argv[3] - '0';

 fstream tout, tin, txt;
 char joe[] = "Joe Johnston";
 char HowMany = 0;
 const char eor = '@';
 long tsize, count;
 char *textptr;
 char *original;
 char dat, a, b, men, sdat;
 int var;
 short int svar;
 char name[101];
 int fend;
 short int pic_count = 0 + picincre;
 
 TextLoad textl( filename );

 textl.getdata();

 original = textl.gettexdata();

 tsize = textl.getSize();
 
 tout.open ( "bio.ptr", ios::app | ios::binary );

 tout.seekg( 0, ios::end );

 fend = tout.tellg(); //  - 1;

#ifdef VERBOSE
 cout << "fend=" << fend;
#endif

	/*
	 * Process Text
	 */

 textptr = original + 2;

#ifdef VERBOSE
 cout << (int)tout.good() << "    " << *(textptr - 1) << "  ";
#endif

 count = 0;

  /*
   * Skip over ~H........~
   */

 if ( *(textptr - 1 ) == 'H')
 	{
		while ( *textptr++ != '~' );
		if ( *textptr == 13  ) textptr += 2;
		textptr += 2;
	} 

 if ( *textptr == 0 ) textptr++;

 /*
  * Scan 1st line
  */

 while ( ( ( dat = *textptr++ ) != '~' ) && ( dat != 13 ) && ( dat != ',' )  )
	{
#if 0	// This must always be true because of above while!
		if ( dat != '~' )
		{
#endif
			if ( ( ( dat == ' ' ) && ( *textptr == '/' ) ) || ( dat == '/' )  )
			{
				dat = '~';
				tout.write ( (unsigned char* ) &dat, sizeof ( char ) );
				tout.write ( (unsigned char* ) &dat, sizeof ( char ) );
				textptr++;
				if ( *textptr = ' ' ) textptr++;
			}
			else if ( ( dat != 13 ) && ( dat != 10 ) && ( dat != '\0' ) )
				tout.write ( (unsigned char* ) &dat, sizeof ( char ) );
#if 0
			}
		else 
			{
#ifdef VERBOSE
				cout << " b" << (int)*(textptr - 2) << " a" << (int)*textptr << " ";
#endif
				textptr--;
			}
#endif

#ifdef VERBOSE
		if ( dat >= 'A' ) cout << dat;
			else cout << (int)dat;
#endif
		count++;
		if ( count > 100 ) { cout << "Error in Lincoln loop\n"; break; }
	}
 
#ifdef VERBOSE
 cout << "  count=" << count << " ";
#endif

 men = 1;

 dat = '~';
 tout.write ( (unsigned char* ) &dat, sizeof ( char ) );

 tout.write ( (unsigned char* ) &format, sizeof ( char ) );

 var = 0;
 tout.write( (unsigned char* ) &var, sizeof ( int ) );
 // var = 0;
 tout.write( (unsigned char* ) &pic_count, sizeof ( short int ) );
 pic_count += picincre;

 svar = 0;
 tout.write( (unsigned char* ) &svar, sizeof ( short int ) );

 tout.write ( (unsigned char* ) &eor, sizeof ( char ) );
 
	/*
	 * Process rest of file
	 */

 count = 0;

 for ( long loop = 10; loop < tsize; loop++ )
 		{
		 dat = *textptr++;

		 /*
		  * End of Chapter: @@
		  */

		 if ( ( dat == '@' )	&& ( *textptr == '@' ) )
		 			{
#ifdef VERBOSE
		 			  if ( dat == '\0' ) cout << "\nDAT = \0";
#endif

						/*
						 * End of File: @@@
						 */

					  if ( *(textptr + 1) == '@' ) break;
					  while ( *textptr++ != '~' );

					  textptr++; // = 5;	     // control character

					  var = (int) (textptr - original - 2);
					  if ( *textptr == 0 ) textptr++;

					  while ( ( ( dat = *textptr++ ) != '~' ) && ( dat != 13 ) && ( dat != ',' ) )
							{
								if ( ( ( dat == ' ' ) && ( *textptr == '/' ) ) || ( dat == '/' )  )
									{
										dat = '~';
										name[count++] = dat;
										name[count++] = dat;
										textptr++;
										if ( *textptr = ' ' ) textptr++;
									}
								else if ( ( dat != 13 ) && ( dat != 10 ) ) name[count++] = dat;
#ifdef VERBOSE
								if ( dat >= 'A' ) cout << dat;
#endif
								
								if ( count > 100 ) { cout << "Error name to long!"; break; }
							}
					  
					  name[ count ] = '\0';

					  if ( ( strstr( name, "Johnston" ) != NULL ) && ( *(textptr + 2) == 'J' ) )
								 	{ 
										tout.write ( joe, 12 );
									}
							else {
							 			tout.write ( name, count );
								  }
					  count = 0;
					  dat = '~';
					  tout.write ( (unsigned char* ) &dat, sizeof ( char ) );
					  tout.write ( (unsigned char* ) &format, sizeof ( char ) );
					  tout.write( (unsigned char* ) &var, sizeof ( int ) );
#ifdef VERBOSE
					  cout << " : " << var << "  ";
#endif
					  tout.write( (unsigned char* ) &pic_count, sizeof ( short int ) );
					  pic_count += picincre;

					  svar = 0;
					  tout.write( (unsigned char* ) &svar, sizeof ( short int ) );

					  tout.write ( (unsigned char* ) &eor, sizeof ( char ) );

					  men++;
					  // if ( ( men == 22 ) && ( format == '9' ) ) format = 'D';
					 }
		 if ( dat == EOF ) break;
		}

 dat = '\0';
 tout.write ( (unsigned char* ) &dat, sizeof ( char ) );

 tout.close();

#ifdef SHOW_RESULTS

 tin.open ( "bio.ptr", ios::in | ios::binary );

 if ( !tin )
 		{
		 cout << "Error: can't open ptr file";
		 return;
		}

	/*
	 * Yet another pass
	 */

 count = 0;

 // cout << "\n" << (int)'\0' << "\n";

 tin.seekg ( fend, ios::beg );
 
#ifdef VERBOSE
 cout << "  " << (int)men << " instances found\n";
#endif 
 // if ( men > 9 ) men = 9;

 for ( a = 0; a < ( men - 2 ); a++ )
	 {
 	  	 tin.read ( ( unsigned char *) &dat, sizeof ( char ) );
#ifdef VERBOSE
		 cout << " k1=";
		 cout << (int)a << ":" << (int)dat << dat; // << dat;
#endif		 
		 while ( dat != '~' )
  			  {
					count++;
					if ( count > 100 ) { cout << "Error in count"; return; }
  				  	tin.read ( ( unsigned char *) &dat, sizeof ( char ) );
#ifdef VERBOSE
				  	cout << dat;
#endif
					if ( dat == '~' ) 
						{
							tin.read ( ( unsigned char *) &dat, sizeof ( char ) );
							sdat = dat;
							if ( dat == '~' ) 
								{
									tin.read ( ( unsigned char *) &dat, sizeof ( char ) );
#ifdef VERBOSE
									cout << ", k2=" << dat;
#endif
								}
							else dat = '~';
						}
					//	else cout << (int)dat << " ";
	  		  }
#ifdef VERBOSE
  		 cout << ":   ";
#endif
		 count = 0;
  		 dat = sdat;
		 // tin.read ( ( unsigned char *) &dat, sizeof ( char ) );

#ifdef VERBOSE
		 cout << " Format: " << dat << "  ";
#endif

		 tin.read ( ( unsigned char *) &var, sizeof ( int ) );	 // Text ptr
		
#ifdef VERBOSE
		 cout << var << " :";
#endif

  		 if ( var > 150000 ) { cout << "\nError textptr invalid (=" << var << ") "; return; }
  
		 for ( loop = 0; loop < 10; loop++ )
 				{
#ifdef VERBOSE
					 cout << *(original + var + loop);
#endif
				}

		 tin.read ( ( unsigned char *) &svar, sizeof ( short int ) );	 // Pic ptr
#ifdef VERBOSE
		 cout << "  pic_ptr =" << svar << ". ";
#endif

		 // cout << var << ":    ";
		 
		 tin.read ( ( unsigned char *) &svar, sizeof ( short int ) );	 // Anim ptr
		
		 tin.read ( ( unsigned char *) &dat, sizeof ( char ) );	 // skip End Of Record
		 		 
		 // cout << var << ": ";

#ifdef VERBOSE
	 	 cout << "\n";
#endif
	 }

 tin.close();

#endif	// SHOW_RESULTS

#if 0
 cout << "\nPress a key to continue...\n";
 getch();
#endif

}
