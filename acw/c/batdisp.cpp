/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Moveable Battle Objects
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "batdisp.h"
#include "batldata.h"
#include "sprlib.h"
#include "batlib.h"
#include "map3d.h"
#include "error.h"

#include "system.h"
#include "colours.h"

#ifdef DEBUG
#include <stdlib.h>
#include "trig.h"
#endif

BattleDisplayObject::BattleDisplayObject()
{
	next = 0;
	sprElements = 0;
	inDisplayList = False;
}

BattleDisplayObject::~BattleDisplayObject()
{
	if(inDisplayList && battle->displayedObjects)
	{
		battle->displayedObjects->remove(this);
		inDisplayList = False;
	}

	unLink();		// Force sprites to be released, etc
}

void BattleDisplayObject::unLink()
{
	if(sprElements)
	{
		delete[] sprElements;		// This removes them!
		sprElements = 0;
	}
}


/*
 * unDisplay is used to remove an object from the display list completely
 */

void BattleDisplayObject::unDisplay()
{
	if(inDisplayList)
	{
		battle->displayedObjects->remove(this);
		inDisplayList = False;
	}

	unLink();
	clearUp();
}

void BattleDisplayList::clear()
{
	BattleDisplayObject* ob = entry;
	while(ob)
	{
		// ob->reset();
		ob->unDisplay();
		ob = ob->next;
	}
}

void BattleDisplayList::add(BattleDisplayObject* ob)
{
	ob->next = entry;
	entry = ob;
}

void BattleDisplayList::remove(BattleDisplayObject* ob)
{
	if(ob == entry)
		entry = ob->next;
	else
	{
		BattleDisplayObject* prev = entry;

		while(prev && (prev->next != ob))
		{
			prev = prev->next;
		}

		if(prev)
			prev->next = ob->next;
#ifdef DEBUG
		else
			throw GeneralError("Removing BattleDisplayObject that is not in list");
#endif
	}
}

/*====================================================
 * Displayed Commander List
 */

DispCommandList::DispCommandList()
{
	entry = 0;
}

DispCommandList::~DispCommandList()
{
	clear();
}

void DispCommandList::clear()
{
	DispCommander* com = entry;
	while(com)
	{
		DispCommander* com1 = com;
		com = com->next;

		delete com1;
	}
	entry = 0;
}

void DispCommandList::add(Unit* u, const Point& p)
{
	DispCommander* com = new DispCommander;

	com->u = u;
	com->p = p;
	com->next = entry;
	entry = com;
}

/*
 * Find the closest commander to the mouse
 */


Unit* DispCommandList::getCloseCommander(const Point& p)
{
	Distance bestDist;
	Unit* bestUnit = 0;

	DispCommander* com = entry;
	while(com)
	{
		Distance d = distance(p.x - com->p.x, p.y - com->p.y);
		if(!bestUnit || (d < bestDist))
		{
			bestUnit = com->u;
			bestDist = d;
		}

		com = com->next;
	}

	/*
	 * Only return answer if close enough to be close
	 * This is arbitarily set to 32 pixels
	 */

	if(bestDist < 32)
		return bestUnit;
	else
		return 0;
}

