/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Debugging and memory Log
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.6  1994/09/02  21:25:33  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/07/19  19:53:03  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/03/10  14:27:18  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/02/15  23:22:28  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/02/09  14:59:26  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/12/23  09:26:01  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


 
#ifdef DEBUG	// Logging only active if compiled with DEBUG defined

#include <time.h>
#include <iomanip.h>

#include "log.h"
// #include "timer.h"

LogStream* logFile = 0;

ostream& date(ostream& l)
{
	time_t t;
	time(&t);

	l << ctime(&t);

	return l;
}



ostream& startLogLine(ostream& l)
{
	time_t t;
	time(&t);
	struct tm* tm = localtime(&t);

	l <<	setw(2) << setfill('0') << tm->tm_hour << ":" <<
			setw(2) << setfill('0') << tm->tm_min  << ":" <<
			setw(2) << setfill('0') << tm->tm_sec  <<
			// " <" << setw(6) << hex << timer->getFastCount() << "> " <<
			dec << setw(0) << setfill(' ');

	return l;
}

LogStream::LogStream(const char* s, int l): ofstream(s)
{
	*this << "Log Initialised at " << date << endl;
	level = l;
}

LogStream::~LogStream()
{
	*this << endl << "Log Closed at " << date << endl;
}

#endif	// DEBUG
