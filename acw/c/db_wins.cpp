/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Database SetupWin class implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG
#include <conio.h>
#endif

#include "db_wins.h"
#include "datab.h"
#include "dbmem.h"
#include "textload.h"
#include "sprite.h"
#include "text.h"
#include "dialogue.h"
#include "cd_open.h"
#include "language.h"
#include "system.h"
#include "screen.h"
#include "myassert.h"

#ifdef DEBUG
const int maxArpos = 32;
#endif

#define FONT_HEADLINE Font_EMMA14

/*
 * SetUpWins implementation
 */

SetUpWins::SetUpWins()
{
	jumps = new retrace;
}

SetUpWins::~SetUpWins()
{
	delete jumps;
}

void SetUpWins::setTextFP( unsigned short int tfp ) 
{ 
	textfileptr = tfp;
	memory.textbegin = original;
	memory.textend = original;
	resetwin();
	getTOffset();
}

void SetUpWins::modTextFP( signed char dat )
{
	textfileptr += dat;
	memory.textbegin = original;
	memory.textend = original;
	resetwin();
	getTOffset();
}

void SetUpWins::setTFP2(unsigned short tfp)
{
	textfileptr = tfp;
	getTOffset();
}

void SetUpWins::pushFormat( int offset, unsigned char NewNum, long int Npaper[] )
{
	jumps->push( format, NewNum, offset, textfileptr, Npaper );
}

int SetUpWins::popFormat( char *form, unsigned char* NewNum, long int Npaper[] )
{
	return jumps->pop( form, NewNum, &textfileptr, Npaper );
}

int SetUpWins::getjumpcount()
{
	return jumps->getcount();
}


void SetUpWins::dispointers() 
{  
	if ( tsize ) 
	{ 
		ptrs->Textdistroy(); 
		delete ptrs;
	}
	delete [] PointerOffset;
	PointerOffset = 0;

	delete[] where;
	where = 0;
}


void SetUpWins::deltextspr( short int totalWidth, short int totalHeight )
{
	// if ( textspr ) 
	delete textspr;
	delete region00;
	delete win00;
	textspr = new Sprite( totalWidth, totalHeight );
	region00 = new Region ( textspr, Rect( 0, 0, totalWidth, totalHeight ) );
	region00->fill(Colour(0));
	win00 = new TextWindow( region00, n );
}

/*
 * Skip to next Text File Pointer entry
 *
 * textptr should starts and ends pointing at title
 *
 * Returns True if at end
 */

typedef const char* CCP;		// Constant pointer to character

/*
 * Move textptr past title
 */

static void skipFTPtitle(CCP& textPtr)
{
	// Skip to end of title

	while(*textPtr++ != '~')
		;	

	// Handle double tilda in title

	if(*textPtr == '~') 
	{
		textPtr++;
		while(*textPtr++ != '~')
			;
	}
}

static Boolean nextFTP(CCP& textPtr)
{
	skipFTPtitle(textPtr);
	textPtr += 9;

	ASSERT(textPtr[-1] == 0);
	ASSERT(textPtr[0] == '@');

	if(*textPtr++ == '@')
	{
		if(*textPtr++ == 0)
			return True;
	}

	return False;
}


/*
 * Count how many entries in Text File Pointer
 */

unsigned short int SetUpWins::findlastTFP()
{
  if ( ( (format >= ':' ) &&
  			( format != 'D' ) &&
			( format != 'L' ) ) || 
		 ( format == '6' ) || 
		 ( format == '8' ) || 
		 ( format == '7' ) )
	  	return 0;

	unsigned short int count = 0;
  	const char* textptr = orig + PointerOffset[ format - '0' ];

	while(!nextFTP(textptr))
		count++;

	return count;
}


/*
 *   Returns the textfile offset for a given chapter ( ie for a textfilepointer );
 */


/*
 * Master's version
 */


unsigned int SetUpWins::getTOffset()
{
	filePos = 0;
	picflag = 0;

	if( ( (format >= ':') && 
			(format != 'D') &&
			(format != 'L') ) ||
			(format == '6') ||
			(format == '8') ||
			(format == '7') )			// Was '4'
	{
		return 0;
	}

	ASSERT((format - '0') <= MaxPageNumber);

  	const char* textptr = orig + PointerOffset[format - '0'];

	ASSERT(textptr != 0);

	unsigned short int count = textfileptr;
	while(count--)
	{
		Boolean finished = nextFTP(textptr);
		ASSERT(!finished);
		if(finished)
			return 0;
	}

	skipFTPtitle(textptr);

#if 0
	ASSERT((textptr - orig) <= 120);
	if((int)(textptr - orig) > 120)
		return 0;
#endif

	format = (int) *textptr++;
	filePos = textptr[0] + 
				(textptr[1] << 8) +
				(textptr[2] << 16) +
				(textptr[3] << 24);	
	textptr += 4;
	picflag = textptr[0] + (textptr[1] << 8);

#if 0  
	if  ( gotit == 0 ) 
		{
#ifdef DEBUG
			cout << "returning 2  tfp=" << textfileptr << "     ";
#endif
			return 2;
		}

   if ( (int)( textptr - orig ) >= tsize ) return 2;

#ifdef DEBUG
	if ( gotit > 150000 ) { cout << "Error in GotIt tfp=" << textfileptr << "    "; gotit = 0; }
#endif
	
	return gotit;
#endif

	// ASSERT(filePos != 0);
	ASSERT((textptr - orig) < tsize);
	ASSERT(filePos <= 150000);

	return filePos;
}


/*
 *  Return textfilepointer ( or chapter number ) for a given keyword;
 */

unsigned short SetUpWins::getKeyOffset(char key)	// , int *BasePtrOffset )
{
	long textptr = findkeyword(orig, key, tsize);

	if(textptr < 0) 
 	{
#ifdef DEBUG
 		dialAlert ( 0, "Sorry, text file is currently unavailable", "OK" );
#endif
 		return 0;
 	}
	
	if (orig[textptr] == '~' )						// '> 1 	keyword to same pointers'
	{
		textptr++;
		while (orig[textptr++] != '~' )
			;
	}

	format = orig[textptr];

	// *BasePtrOffset = 0;

  	unsigned short count = 0;

	for(int a = 0; a < textptr; a++)
	{
		if (orig[a] == '@' )
		{
			if((orig[a+1] == '\0') && (orig[a-1] == '\0'))
				count = 0;
			else
				count++;
		}
	}
	
	return count;
}

/*
 *		Used with memory feacility. When jumping into the middle of a chapter
 *   this routine checks for any headings (~h) or jobs (~j) within the
 *	  previous 'pages' and displays them if found;	
 */


void SetUpWins::getTitles()
{
	char* textptr;
	char* store;
	Boolean quit = False;
	char dat;
	signed int counter, loop = 0;
	int check = 0;
	int head, length = 0;
	char word[50];
	const char fincre = 2;
	Point where;
	// Font *font = fontSet(n);
	int count = 0;

	textptr = original + getFilePos();
	
	counter = (int)( memory.colbegin - textptr );
	
	if ( counter <= 0 ) 
	{ 
		if ( counter == 0 ) return;
#ifdef DEBUG
		cout << "Error in counter(=" << counter << "): getTitles()  "; 
		getch();
#endif
		return; 
	}
	
	while(loop++ < counter) 
	{
		/*
		 * Find ~h
		 */

		if ( *textptr++ == '~' )
		{
			if((*textptr == 'h') && (numa > 1))		 // Title
			{
				// store = textptr + 1;
				// textptr++;
				store = ++textptr;

				Region *rheading;
				TextWindow *wheading;

				rheading = retReg(columns + numa - 2); 	// + 1
				wheading = retWin(columns + numa - 2); 	// + 1
				
				while(!quit)
				{
					FontID largen; 
					
					if(page == Page_Features)
						largen = Font_EMMA14;	// Newspaper headline
					else
						largen = Font_EMFL32;

					Font* font = fontSet(largen); 
 					wheading->setFont (largen);
			
					where.y = 0;
					// textptr++;
					// if ( check == 1 ) loop++;
					if ( *textptr == ' ' ) textptr++; 
					
					if ( check != 0 )
					{
						
						wheading->setColours( c_Title );
						where.y = head;
						wheading->setPosition( where );
					}

					while ( *textptr != '~' ) 	
					{
						while ( ( ( dat = *textptr ) != 13 ) && ( *textptr++ != '~' ) )
						{
							if ( dat != ',' ) word[ length++ ] = dat;

						}
						if ( dat == 13 ) textptr += 2;
						if ( ( *textptr == '~' ) && ( *( textptr + 1 ) == '^' ) ) textptr += 2;
						if ( *(textptr - 1) == '~' ) textptr--;
						word [ length ] = '\0';
					
						where.x = ( rheading->getW() - font->getWidth ( word ) ) / 2;

						count = 0;
						
						while ( ( where.x < 1 ) && ( count != length ) ) 
						{
							while ( ( word [ count++ ] != ' ' )	&& ( count < length ) );
								
							if ( count != length )
							{
								if ( ( word [ count ] == '/' ) && ( word[ count + 1 ] == ' ' ) ) count += 2;
								word [ count - 1 ] = '\0';
								where.x = ( rheading->getW() - font->getWidth ( word ) ) / 2;
								if ( check != 0 ) 
										{
											wheading->setPosition( where );
											wheading->draw ( word );
										}
			
								where.y += font->getHeight() + fincre;
								
								for ( char h = 0; h < length - count; h++ )
									{
										word [ h ]	= word[ count + h ];
									}

								word [h] = '\0';
								where.x = ( rheading->getW() - font->getWidth ( word ) ) / 2;
							}
						}
					
						if (check != 0 )
							{
							  	wheading->setPosition( where );
								wheading->draw ( word );
							}

						where.y += font->getHeight() + fincre;
						
						if ( ( largen != FONT_HEADLINE ) && ( dat == 13 ) )
							{
								largen = FONT_HEADLINE;

								font = fontSet( largen );
								wheading->setFont ( largen );
							}
						length = 0;
					}
						
					if ( check == 0 )
					{
							head = ( rheading->getH() - where.y ) / 2;
					}
						 	  						where.x = 0;
					where.y = 0;
					if ( check != 0 )
					{
							wheading->setPosition( where );
							wheading->setColours( c_Text );
					}
					
					if ( check == 0 ) { check = 1; textptr = store; }
					else { quit = True; check = 0; }
					
				}
				
				// font = fontSet(n);
				wheading->setFont ( n );

				loop = loop + (int)( textptr - store );

				if ( *textptr == '~' ) { textptr++; loop++; }
				
				quit = False;
			} 
			else if(*textptr == 'j')
			{
				Font *font = fontSet(n);
				// temp = where;
				where.x = 0;
				where.y = 0;

				store = textptr + 1;

				Region *rjob;
				TextWindow *wjob;

				rjob = retReg ( numa + columns - 1 );	// -1
				wjob = retWin ( numa + columns - 1 );	// -1


				wjob->setPosition( where );
				
				wjob->setFont ( n );

				textptr++;
				wjob->setColours( c_Title );
				length = 0;
				while ( *textptr != '~' ) 	
				{
					while ( ( ( dat = *textptr ) != 13) && ( *textptr++ != '~' ) )
					{
						word[ length++ ] = dat;
					}
					if ( dat == 13 )
						textptr += 2;
					word [ length ] = '\0';
				
					if ( *textptr == 'l')
					{
						textptr++;
						where.x = 0;
						where.y = ( rjob->getH() - font->getHeight() ) / 2;
					}
					else
						where.x = ( rjob->getW() - font->getWidth ( word ) ) / 2;
					
					if ( *(textptr - 1) == '~' )
						textptr--; // So to quit first while loop

					// where.x = ( rjob->getW() - font->getWidth ( word ) ) / 2;
					
					wjob->setPosition( where );

					wjob->draw ( word );
					
					// where.y += font->getHeight() + fincre;
					length = 0;
				}
				wjob->setColours( c_Text );
				textptr++;
				while ( ( *textptr < '!' ) && ( *textptr != '\0' ) )
					textptr++;
				loop += ( int )( textptr - store );
			} 
		}
	}
}			

/*
 * 	Displays a 'line' of text at where.y, 0, in the textwindow win;
 */

	
char SetUpWins::writeline( TextWindow *win, Region *bm, const Point& wh, char line[], char keytotal, Font *font, short int totalWidth, short int totalHeight  )
{
  Point here = wh;

  char a = 0, b;
  // char loop;
  unsigned keystore = 0;
  char key = 0;
  Point temp;
  short int length;
  signed char ats, dat = 0, remain;
  signed int space;
  char numSpaces = 0;

  char word[MaxIndexLength + 1];
  char tildas = 0;	

  int len;
  int linelen;
  long templong;

  if ( keytotal >= 80 ) 
  	{
		if ( keytotal >= 190 )
			{
				keytotal -= 190;
				keystore = 190;
			}
		else if ( keytotal >= 150 ) 
  			{
				keytotal -= 150;
				keystore = 150;
			}
		else {
				keytotal -= 80;
				keystore = 80;
			}
 	}

  while ( keytotal >= 40 ) 
		{
			keytotal -= 40;
			keystore += 40;
		}
  ats = 0;
  len = strlen( line );
  if ( line[ len ] == ' ' ) { line [ len ] = '\0'; len = strlen( line ); }
  if ( line[ len - 1 ] == ' ' ) line [ len - 1 ] = '\0';
   
  for ( b = 0; b <= len; b++ )
  	{
	 if ( line[b] == '@' ) ats++;
 	 else if ( line[b] == '~' ) 
	 	{ 
			if ( !( ( bold == YES ) && ( line[b+1] < 'A' ) ) ) tildas++;
		}
	 // else if ( line[b] == '~' ) tildas++;
	 else if ( line[b] == ' ' ) numSpaces++;
	 else if ( line[b] == 13 ) key = 1;
	}
  if ( line[ 0 ] == ' ' ) { numSpaces--; a = 1; }

  if ( tildas > 0 ) 
  	{
	  tildas++;
	  tildas = (tildas *  3) / 2;   			// To account for the control character
	}

  if ( ( key == 0 ) && ( numSpaces != 0 ) )
  	 {
	 	linelen = font->getWidth( line ) - tildas * font->getWidth( '~' ) - ats * font->getWidth( '@' );
		if ( line[0] == ' ' ) linelen -= font->getWidth( ' ' );
		space  = ( bm->getW() - linelen ) / numSpaces;
  		remain = ( bm->getW() - linelen ) % numSpaces;
	 }
  else {
  		key = 0;
		space = 0; 
		remain = 0;
		numSpaces = 0;
		if ( line [ len - 1 ] == 13 ) line[ len - 1 ] = '\0';
	 }
  
  ats = 0;

	if ( bold == YES )
	{
		win->setColours( c_Bold );
		win->setBold(True);
	}

  while ( a < strlen( line ) )
	  {
  		dat = line[a++];

		if ( dat == ' ' )
					{
					   ats++;
						here = win->getPosition();
						here.x += space;
						
						if ( remain > 0 ) { here.x++; remain--; }
						if ( remain < 0 ) { here.x--; remain++; }
						if ( ats == numSpaces )
							{
								b = a;
								length = 0;
								if ( line [ b ] == '~' ) b +=2;
								if ( line [ b ] == '@' ) b++;
								while ( ( (dat = line[ b++ ]) != '\0' ) && ( length < 30 ) )
									word[ length++ ] = dat;

								word [ length ] = '\0';
								
								if ( ( word [ length - 1 ] == '~' ) || ( word[ length - 1 ] == '@') ) word [ length - 1 ] = '\0';
								if ( ( word [ length - 2 ] == '~' ) || ( word[ length - 2 ] == '@') ) { word [ length - 2 ] = word [ length - 1 ]; word [ length - 1 ] = '\0'; } 
								if ( word[ length - 3 ] == '@') word [ length - 1 ] = '\0';

								if ( (font->getWidth( word ) + here.x + font->getWidth ( ' ' ) ) < bm->getW() )
										{
											space = bm->getW() - ( font->getWidth( word ) + here.x + font->getWidth ( ' ' ) );
											here.x += space;
										}
								here.x -= 2;

								dat = ' ';
							}		
						
						win->setPosition( here );

					}
		
		if ( dat == '~' )
			{
				if ( bold == YES )
				{
					win->setColours( c_Text );
					bold = NO;
					win->setBold(False);
				}
				else if ( underline == YES )
				{
					win->setColours( c_Text );
					underline = NO;
				}
				else if ( line[a] == 'b'	) 						 // Bold
				{				   
					win->setColours( c_Bold );
					win->setBold(True);
					bold = YES;
					a++;
				}
				else if ( line[a] == 's' ) 
				{
					underline = YES;
					win->setColours( c_subTitle );
					a++;
				}
			 
				/*	if ( ats < numSpaces ) 
					{
						here = win->getPosition();
						here.x++;
						win->setPosition( here );
					}
			 */
			}
		else if ( dat == '@' )
			{
				if ( key == 1 )
					{
						key =0;
						win->setColours( c_Text );
					}
				else
  					{
					   length = 0;
					  	while ( ( ( dat = line [ a + length] ) != '@' ) && ( (a+length) < strlen( line ) ) )
								if ( length < MaxIndexLength ) word[ length++ ] = dat;
									else length++;

						word [ length ] = '\0';

					 	key = 1;
						
						temp = win->getPosition();
						
						if ( length >= MaxIndexLength ) 
							{
#ifdef DEBUG
								showString( Point( 0,0 ), "Keyword=%s", word );
								dialAlert( 0, "Keyword too long!", "Oops!" );
#endif
							}
						else 
							{
								keyinfo[ keytotal ].kx = temp.x + totalWidth - 1; // + due to starting point of window
								keyinfo[ keytotal ].ky = temp.y + totalHeight + 1;
																						
							  	dat = 0;

								for ( b = 0; b < length; b++ )
								 	{
									 	keyinfo[ keytotal ].name[b] = word[b];
										if ( word [ b ] == ' ' ) dat += space;
									}
						 
								 keyinfo[ keytotal ].name[ length ] = '\0';
						
								 keyinfo[ keytotal ].width = font->getWidth ( word ) + 2 + dat;
								 keyinfo[ keytotal ].height = font->getHeight() + 1;
						 
								 templong = 0;
								 if ( strcmp ( keyinfo[keytotal].name, language( lge_cavalry ) ) != 0 )
								 	templong = findkeyword( orig, keytotal, tsize );

								 if ( templong < 0)
								 	win->setColours( c_Text );
								 else
								 	win->setColours( c_Keyword );							  
						 
								 keytotal++;
						
								 if ( keytotal >= 28 ) 
								 	{ 
									#ifdef DEBUG
										cout << "Writeline raised to " << (int)keytotal << "!";
									#endif
										keytotal--;
									} 
							}
					}
			  }
		else {
				win->draw( dat );
			}
	}

 return (keytotal + keystore);
}	

/*
 *    Loads keywords and corresponding pointers, and all sets the 
 *   Pointer Offset element (screen format - '0') to the position in 
 *	  the pointer file where that format begins. Each format ends with
 *	  @ follewed by a '\0'.
 */
								  
void SetUpWins::UpWins( FontID nn ) // short int totalWidth [], short int totalHeight[], short int text_xpos[],	short int text_ypos [], FontID nn )
{
#if 0
	int count = 0;
	char element = 0;
#endif

	picflag = 0;

	error = 0;
	bold = NO;
	underline = NO;

	// numpages = 5;

	prevcols = 0;
	filePos = 0;
	columns = 0;

	numa = 0;

	size = 0;

	n = nn;												 // Set Font

	page = Page_SpecialUse;

	where = new Point[6];

#if 0
	char buffer[17];

	strcpy( buffer, getFileName( "dbdata\\ptr" ) );
#endif

	ptrs = new TextLoad(getFileName("dbdata\\ptr"));	 // load pointers

	if(!error)
	{
		error = ptrs->geterr();
		if(error)
			return;
	}

	ptrs->getdata();

	orig = ptrs->gettexdata();

	tsize = ptrs->getSize();

	/*
	 * Allocate pointers and clear to 0
	 */

	PointerOffset = new int [MaxPageNumber+1];

	for(UBYTE element = 0; element < (MaxPageNumber+1); element++ )
		PointerOffset [ element ] = 0;		 // Page Format at beginning of pointer file

	if(orig == NULL )
		error = 1;
	else
	{
#if 0		// Pre-Masters version
		int count = 0;

		while(count < tsize)
		{
			if(orig[count++] == '@') 
			{
				/*
				 * \0@\0 indicates new section
				 */

				if ( (orig[count] == '\0') && (orig[count - 2] == '\0') )
				{
					count++;

					while (( orig[count++] != '~') && (count < tsize))
						;
					if (count >= tsize)
						break; 

					if(orig[count] == '~' )
					{
						count++;
						while(orig[count++] != '~')
							;
					}

					element = orig[count] - '0';

					if(element > MaxPageNumber) 
					{
#ifdef DEBUG
						cout << " Error in Pointer offset in UpWins: element=" << (int)element << "! ";
						getch();
#endif
						// element = 0;
						continue;
					}

					if(PointerOffset[element] != 0)
						continue;

					PointerOffset[element] = count + 1;
				}
				else		// Skip past keyword
				{
					while( (orig[count++] != '~') && (count < tsize))
						;
					if(orig[count] == '~')
					{
						count++;
						while( (orig[count++] != '~') && (count < tsize))
							;
					}

					// Skip over format and file pointer

					count += 9;		// 1 for format, 4 for file pointer, 4 for ?

				}
			}
		}
#else		// Masters version
		const char* textptr = orig;
		while( (textptr - orig) < tsize)
		{
			if(nextFTP(textptr))
			{
				if(*textptr == 0)		// Got to end
					break;

				const char* tPtr = textptr;
				skipFTPtitle(tPtr);
				element = tPtr[0] - '0';
				ASSERT(element <= MaxPageNumber);
				ASSERT(PointerOffset[element] == 0);

				PointerOffset[element] = textptr - orig;
			}
		}
#endif
	}
}

Point SetUpWins::getwhere( int choice )
{
#ifdef DEBUG	  	
	if (choice > 2)
		dialAlert( 0, "Error in getwhere!!", "Drat" );
#endif
	return where[choice];

}


/*
 *   Restore all areas of the screen;
 */

void SetUpWins::resetwin( int titles )
{
 char z = 0;

 if ( z++ < ( columns + numa ) ) machine.blit(&under, where[ 0 ] );
 
 if ( z++ < ( columns + numa ) ) machine.blit(&untitle, where[ 1 ] );
 	else return;

 if ( z++ < ( columns + numa ) ) machine.blit(&unjob, where[ 2 ] );
 
 if ( titles != 0 ) return;

 if ( z++ < ( columns + numa ) ) machine.blit(&uncol2, where[ 3 ] );

 if ( z < ( columns + numa ) ) machine.blit(&uncol3, where[ 4 ] );
}

void SetUpWins::upDatewin()
{
	// char z = 0;

	// machine.screen->setUpdate( retAReg( 0 ) );
	if ( 0 < ( columns + numa ) )
		{
			machine.maskBlit( textspr, Point ( where[0] ) );
			machine.screen->setUpdate( Rect ( *region00 ) + where[0] );
		}

//	if ( z++ < ( columns + numa ) ) machine.screen->setUpdate( retAReg( 1 ) );

//	if ( z < ( columns + numa ) ) machine.screen->setUpdate( retAReg( 2 ) );

}


void SetUpWins::setwin( short int totalHeight[], short int totalWidth[] )
{
	char z = 0;

	if ( z++ < ( columns + numa ) )
		{
			under.resize(totalWidth[0], totalHeight[0]);
			machine.unBlit(&under, where[0]);
		}
		
	if ( z++ < ( columns + numa ) )
		{
			untitle.resize( totalWidth[1], totalHeight[1] );
			machine.unBlit(&untitle, where[ 1 ] );
		}
 
	if ( z++ < ( columns + numa ) )
		{ 
			unjob.resize( totalWidth[2], totalHeight[2] );
			machine.unBlit(&unjob, where[ 2 ] );
		}
	if ( z++ < ( columns + numa ) )
		{ 
			uncol2.resize( totalWidth[3], totalHeight[3] );
			machine.unBlit(&uncol2, where[ 3 ] );
		}
	if ( z < ( columns + numa ) )
		{ 
			uncol3.resize( totalWidth[4], totalHeight[4] );
			machine.unBlit(&uncol3, where[ 4 ] );
		}
}

void SetUpWins::resetarea( wareas area )
{

	switch( area )
		{
			case job:
				 machine.blit(&unjob, where[ 2 ] );
				 break;

			case text:
				 machine.blit(&under, where[ 0 ] );
				 break;

			case heading:
				 machine.blit(&untitle, where[ 1 ] );
				 break;

			case picback:
				 machine.blit( &underpic, where[ 3 ] );
				 break;

			default:
	 			 dialAlert(0, "Error bliting wrong part of screen", "Ooops!");
				 // cout << " Error bliting screen";
				 break;
		}
}

TextWindow* SetUpWins::retWin( char win )
{
	switch ( win )
		{
			case 0:
				return win00;

			case 1:
				return win01;

			case 2:
				return win02;

			case 3:
				return win03;

 			case 4:
				return win04;

			default:
#ifdef DEBUG	
			cout << "Error in retWin";
			getch();
#endif
				return win00;
		}
}

Region* SetUpWins::retReg( char win )
{
	switch ( win )
		{
			case 0:
				return region00;

			case 1:
				return region01;

			case 2:
				return region02;

			case 3:
				return region03;

			case 4:
				return region04;

			case 5:
				if ( picflag != 0 )
					{
						return picture;
					}
 
			default:
#ifdef DEBUG
				cout << "Error in retReg";
				getch();
#endif
				return region00;
		}

}

Region& SetUpWins::retAReg( char win )
{
	switch ( win )
		{
			case 0:
				return *region00;

			case 1:
				return *region01;

			case 2:
				return *region02;

			case 3:
				return *region03;

			case 4:
				return *region04;
			case 5:
				if ( picflag != 0 )
					{
						return *picture;
					}

			default:
#ifdef DEBUG
				cout << "Error in retReg";
				getch();
#endif
				return *region00;
		}

}

/*
 *   Changes the screen layout and deletes the old, for text screens;
 */


void SetUpWins::changewins( char cols, char Numareas, char pag, short int totalWidth [], short int totalHeight[], short int text_xpos[],	short int text_ypos [] )
{

	char Filename[ 26 ];
	int z;
	
	if ( pag == page ) return; 
 
   if ( n != Font_JAME08 ) n = Font_JAME08;

	textfileptr = 0;

#ifdef DEBUG
	if ( page == maxArpos ) cout << "Error in page number!!!!";
#endif

#ifdef DEBUG
	if ( page == maxTextpages ) cout << "Error in page number!!!!";
#endif

	switch( pag )
				{
					case Page_Main:
						Filename[ 0 ] = '\0';
						error = 10;
						break;
					case Page_Bio1:
						strcpy (	Filename, getFileName( "dbdata\\biotxt" ) );
						format = '2';
						break;
					case Page_Bio2:
						strcpy (	Filename, getFileName( "dbdata\\biotxt" ) );
						format = '2';
						break;

					case Page_Battles:
						strcpy (	Filename, getFileName( "dbdata\\battles" ) );
						format = '4';						  
						break;

					case Page_Chrono:
						strcpy (	Filename, getFileName( "dbdata\\chrono" ) );
						format = '5';
						break;
					case Page_Testtabl:
						strcpy ( Filename, "dbdata\\testtabl.txt" );
						format = '6';
						break;

					case 7:
						strcpy (	Filename, getFileName( "dbdata\\battles" ) );
						format = '4';
						break;

					case 8:
						strcpy ( Filename, getFileName( "dbdata\\hist1863") );
						format = '8';
						break;
					case Page_Features:
						strcpy ( Filename, getFileName( "dbdata\\features") );
						format = '9';
						// n = Font_JAME_5;
						n = Font_JAME08;
						break;
					case 10:
						strcpy ( Filename, getFileName( "dbdata\\briefhis") );
						format = ':';
						break;
					case Page_Features2:
						strcpy ( Filename, getFileName( "dbdata\\features" ) );
						format = '9';
						break;
					case 15:
						strcpy ( Filename, getFileName( "dbdata\\artill" ) );
						format = '?';
						break;
					case 16:
						strcpy ( Filename, getFileName( "dbdata\\cavtact" ) );
						format = '@';
						break;
					case 17:
						strcpy ( Filename, getFileName( "dbdata\\infweap" ) );
						format = 'A';
						break;
					case 18:
						strcpy ( Filename, getFileName( "dbdata\\hist1861") );
						format = 'B';
						break;
					case Page_Songs: 
						strcpy ( Filename, getFileName( "dbdata\\songs" ) );
						format = 'D';
						break;
					case 21:
						strcpy ( Filename, getFileName( "dbdata\\navtact" ) );
						format = 'E';
						break;
					case 22:
						strcpy( Filename, getFileName( "dbdata\\cavweap" ) );
						format = 'F';
						break;
					case 23:
						strcpy( Filename, getFileName( "dbdata\\inftact" ) );
						format = 'G';
						break;
					case 24:
						strcpy( Filename, getFileName( "dbdata\\arttact" ) );
						format = 'H';
						break;
					
					// Note that case 25 is special case used in the database
					// mechanics

					case 26:
						strcpy( Filename, getFileName( "dbdata\\navweap" ) );
						format = 'J';
						break;

					case 28:
						strcpy( Filename, getFileName( "dbdata\\pictures" ) );
						format = 'L';
						break;
					case Page_Songs2:
						strcpy ( Filename, getFileName( "dbdata\\songs" ) );
						format = 'D';
						break;

					default:
#ifdef DEBUG
						cout << "Error in switch in SetUpWins";
						getch();
#endif
						Filename[ 0 ] = '\0';
						error = 10;
						break;
				}


	if ( error == 10 ) { error = 0; return; }  // Non-text screen;

	prevcols = columns + numa;

	numa = Numareas;

	columns = cols;

	if ( page != 25 )
		{
			z = 0;
			if ( z++ < prevcols ) 
				{
					delete region00;
					delete win00;
					delete textspr;
				}
			if ( z++ < prevcols ) 
				{
					delete region01;
					delete win01;
				}
			if ( z++ < prevcols )
				{
					delete region02;
					delete win02;
				}
			if ( z++ < prevcols )
				{
					delete region03;
					delete win03;
				}
			if ( z < prevcols )
				{
					delete region04;
					delete win04;
				}
			if ( size ) textl->Textdistroy();
			if ( size ) delete textl;

			if ( page == Page_Main ) size = 0;

		}

	page = pag;

	where[0].x = text_xpos[0];
	where[0].y = text_ypos[0];
	where[1].x = text_xpos[1];
	where[1].y = text_ypos[1];
	where[2].x = text_xpos[2];
	where[2].y = text_ypos[2];
	where[3].x = text_xpos[3];
	where[3].y = text_ypos[3];

	// Setup columns'(area) regions

	z = 0;

	if ( z++ < ( Numareas + cols ) )
		{
			textspr = new Sprite( totalWidth[0], totalHeight[ 0 ] );
			region00 = new Region ( textspr, Rect( 0, 0, totalWidth[0], totalHeight[ 0 ] ) );
			region00->fill(Colour(0));
			win00 = new TextWindow( region00, n );
		 
		}

	if ( z++ < ( Numareas + cols ) )
		{
			region01 = new Region ( machine.screen->getImage(), Rect( where[1].x, where[1].y, totalWidth[1], totalHeight[ 1 ] ) );
			win01 = new TextWindow ( region01, n );
		}

	if ( z++ < ( Numareas + cols ) )
		{
			region02 = new Region ( machine.screen->getImage(), Rect( where[2].x, where[2].y, totalWidth[2], totalHeight[ 2 ] ) );
			win02 = new TextWindow ( region02, n );
		}
	
  	if ( z++ < ( Numareas + cols ) )
		{
		 	//where[3].x = text_xpos[3];
			//where[3].y = text_ypos[3];

			region03 = new Region ( machine.screen->getImage(), Rect( where[3].x, where[3].y, totalWidth[3], totalHeight[ 3 ] ) );
			win03 = new TextWindow ( region03, n );
		}

	if ( z++ < ( Numareas + cols ) )
		{
		 	where[4].x = text_xpos[4];
			where[4].y = text_ypos[4];

			region04 = new Region ( machine.screen->getImage(), Rect( where[4].x, where[4].y, totalWidth[4], totalHeight[ 4 ] ) );
			win04 = new TextWindow ( region04, n );
		}

	if ( strlen( Filename ) > 1 ) 
		{
			filename = strcpy ( new char [ strlen( Filename ) + 1 ], Filename );

			textl = new TextLoad( filename );			 // Load text

			if ( !error ) 
			{
				error = textl->geterr();

				if ( error )
				{
				 	delete filename;

					return;
				}
			}

			textl->getdata();

			original = textl->gettexdata();

			if (original == NULL ) error = 1;

			size = textl->getSize();

			if ( !error ) error = textl->bad();

			memory.textbegin = getOriginal();
			memory.textend = getOriginal();
			memory.colbegin = memory.textend;

			delete filename;
		}
}

void SetUpWins::changewins() // char pag, short int totalWidth[], short int totalHeight[], short int text_xpos[],	short int text_ypos [] )
{
  // Only call for non-text screens;


  // 	char Filename[ 20 ];
	char z;

	prevcols = columns + numa;

	columns = 0;
	numa = 0;
	picflag = 0;

	if ( page != 25 )
		{
			z = 0;
			if ( z++ < prevcols ) 
				{
					delete region00;
					delete win00;
					delete textspr;
				}
			if ( z++ < prevcols ) 
				{
					delete region01;
					delete win01;
				}
			if ( z++  < prevcols )
				{
					delete region02;
					delete win02;
				}
			if ( z++  < prevcols )
				{
					delete region03;
					delete win03;
				}
 			if ( z  < prevcols )
				{
					delete region04;
					delete win04;
				}
			if ( size ) textl->Textdistroy();

			if ( size ) delete textl;

		}

	page = Page_SpecialUse;

	size = 0;
}

