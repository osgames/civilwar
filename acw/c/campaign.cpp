/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	General Campaign Game stuff
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.22  1994/06/09  23:32:59  Steven_Green
 * Changed Fonts
 *
 * Revision 1.21  1994/06/07  18:29:54  Steven_Green
 * Started to implement sea/river data
 *
 * Revision 1.19  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.14  1994/04/22  22:14:22  Steven_Green
 * Icon Handling tidied up.
 *
 * Revision 1.4  1994/02/03  23:31:36  Steven_Green
 * Call to make test Armies
 *
 * Revision 1.1  1994/01/17  20:14:26  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#ifdef TESTING
#include <stdio.h>
#endif
#include <limits.h>
#include <direct.h>

#include "campaign.h"
#include "campwld.h"
#include "ai_camp.h"
#include "system.h"
#include "screen.h"
#include "mapwind.h"
#include "moblist.h"
#include "clock.h"
#include "generals.h"
#include "timer.h"
#include "game.h"
#include "sprlib.h"
#include "layout.h"
#include "files.h"
#include "savegame.h"
#include "wldfile.h"
#include "strutil.h"
#include "msgwind.h"
#include "campmusi.h"
// #include "memalloc.h"
#include "camptab.h"
#include "memptr.h"
#include "logo.h"
#include "options.h"

#ifdef TESTING
#include "railway.h"
#include "dialogue.h"
#endif
#ifdef DEBUG
#include "clog.h"
#include "memlog.h"
#endif

#include "myassert.h"

/*
 * Campaign Global Variables
 */

Campaign* campaign = 0;

AlertBase::~AlertBase()
{
}

/*
 * Initialise Campaign
 */

Campaign::Campaign()
{
	/*
	 * If short of memory close down music
	 */

	if(!game->musicInCampaign)	// 1.5 Megs
		game->gameMusic->disable();

	givingOrders = True;
	// updateMapFlag = True;
	lastCount = timer->getCount();
	workFileName = 0;
	world = 0;
	
	// ai = 0;

	for(int i = 0; i < SideCount; i++)
		aiControl[i] = 0;

	mapArea = 0;
	subMenu = 0;
	// message = 0;
	msgArea = new MessageWindow(machine.screen->getImage(), Rect(MSGW_X,MSGW_Y,MSGW_W,MSGW_H));
	alertArea = 0;
	infoArea = new Region(machine.screen->getImage(), Rect(INFO_X,INFO_Y,INFO_WIDTH,INFO_HEIGHT));	// was 315 high

	gameTime.setCampaignTime();
	// gameTime.set(dateToDays(12, April, 1861), SunSet);
	gameTime.set(dateToDays(22, May, 1861), SunSet);
	timeInfo = gameTime;

#ifdef DEBUG
	cLog.setTime(&timeInfo);
#endif
	
	clockIcon = 0;
	interface = 0;
	
	newDay = True;
	showEmptyWaterZones = False;

	// map->setStyle(MapWindow::Full, Location(MapMaxX/2, MapMaxY/2));
}

Campaign::~Campaign()
{
#ifdef DEBUG
	cLog.setRealTime();
#endif

	if(workFileName)
	{
		remove(workFileName);			// Remove temporary file!
		delete[] workFileName;
	}

#if 0		// Interface deletes this
	delete mapArea;
	mapArea = 0;
#endif

	// delete ai;
	// ai = 0;

	for(int i = 0; i < SideCount; i++)
	{
		delete aiControl[i];
		aiControl[i] = 0;
	}


	delete infoArea;
	infoArea = 0;

	delete msgArea;
	msgArea = 0;

	delete world;
	world = 0;

	delete alertArea;
	alertArea = 0;

	delete interface;
	interface = 0;

#if 0		// delete interface removes this
	delete clockIcon;
	clockIcon = 0;
#endif

	tempObjects.destroy();

	/*
	 * Re-enable Music
	 */

	if(!game->musicInCampaign)
		game->gameMusic->enable();
}

/*
 * Set up enough of the campaign to load a campaign battle
 */

void Campaign::smallSetup()
{
	world = new CampaignWorld;
	world->smallSetup();

#if 0
	/*
	 * If the game is a one player game then set up the AI
	 */

	if(game->playerCount == 1)
		ai = new CampaignAI;
#else
	for(SideIndex i = SideIndex(0); i < SideCount; INCREMENT(i))
	{
		if(game->isAI(i))
			aiControl[i] = new CampaignAI;
	}
#endif
}

/*
 * Initialise Campaign enough to load game into it
 *
 * The gamechunk must already be loaded to know what side the player is playing
 */

void Campaign::beforeLoad()
{
	world = new CampaignWorld;
	world->init();
 	readStaticWorld(startWorldName, world);

#if 0
	/*
	 * If the game is a one player game then set up the AI
	 */

	if(game->playerCount == 1)
		ai = new CampaignAI;
#else
	for(SideIndex i = SideIndex(0); i < SideCount; INCREMENT(i))
	{									 
		if(game->isAI(i))
			aiControl[i] = new CampaignAI;
	}
#endif

	// setVictory();
}

/*
 * Do any tidying up after loaded game
 */

void Campaign::afterLoad()
{
#if 0
	if(ai)
	{
		Side aiSide = otherSide(game->playersSide);

		ai->startCampaign(aiSide, True);
	}
#else
	for(SideIndex i = SideIndex(0); i < SideCount; INCREMENT(i))
	{
		if(game->isAI(i))
			aiControl[i]->startCampaign(indexToSide(i), True);
	}



#endif

	/*
	 * Calculate victory conditions if an old version was loaded!
	 */

	if(USAVictory == 0)
		setVictory();

	if(CSAStartVictory == 0)
	{
		ULONG total = USAVictory + CSAVictory;
		CSAStartVictory = (USAVictory * 256 + total/2) / total;
	}
}

/*
 * Initialise brand new campaign Game
 */


void Campaign::init(const char* fileName)
{
#ifdef DEBUG
	cLog.printf("Campaign::init(%s)", fileName ? fileName : "Null");
#endif

	world = new CampaignWorld;
	world->init();

#ifdef CAMPEDIT
	readWorld(fileName, world);
#else
	{
#ifdef DEBUG
		memLog->track("About to read world");
#endif

		readStartCampaign(startWorldName, this);
		world->ob.setVisibility();

#ifdef DEBUG
		memLog->track("Finished Reading");
#endif
	}
#endif

	/*
	 * If the game is a one player game then set up the AI
	 */

	for(SideIndex i = SideIndex(0); i < SideCount; INCREMENT(i))
	{
		if(game->isAI(i))
		{
			aiControl[i] = new CampaignAI;
			aiControl[i]->startCampaign(indexToSide(i), False);
		}
	}

	setVictory();
}

#ifdef CHRIS_AI
Boolean Campaign::CheckAI( Unit* u )
{
	if ( ai ) return ai->CheckAIClear( u );
	else return False;
}
#endif

/*
 * Mark campaign area as needing a redraw
 */

void Campaign::updateMap()
{
	if(mapArea)
		mapArea->mapRedraw();
}

/*
 * Run Campaign Game Logic
 */

void Campaign::ordersPhase()
{
	if(realTimeCampaign)
	{
		/*
		 * Is it night or day?
		 */

	 	/*
		 * Find how much time has passed since the last processing
		 */

		unsigned int count = timer->getCount();
		unsigned int passedTime = count - lastCount;

		if(newDay)
		{
			passedTime = 0;
			newDay = False;
		}


		/*
		 * Clip the time to a maximum of 10 seconds
		 */

		else
		{
			if(passedTime > (Timer::ticksPerSecond * 10))
				passedTime = Timer::ticksPerSecond * 10;
		}

		lastCount = count;
	
		/*
		 * Update the game time
		 */

		gameTime.addRealTime(passedTime);
		timeInfo = gameTime;				// Create Broken down time
	
		if( (timeInfo.ticksSinceMidnight >= SunRise) &&
	     	(timeInfo.ticksSinceMidnight < SunSet) )
		{
			givingOrders = False;
			gameTime.setTime(SunRise);
			timeInfo = gameTime;
		}
	
		if(clockIcon)
			clockIcon->setRedraw();
	}
}

/*
 * End the orders phase
 */

void Campaign::finishOrders()
{
	givingOrders = False;
	gameTime.advanceTime(SunRise);
	timeInfo = gameTime;
}

/*
 * Function called from mapwindow to display objects
 *
 * It means the mapwindow functions do not need to know
 * about the campaigns object lists
 */

void Campaign::drawObjects(MapWindow* map)
{
	/*
	 * Place states
	 */

	if(world->states.entries())
	{
		StateListIter list = world->states;

		while(++list)
			map->drawObject(&list.current());
	}

#ifdef TESTING
	if(map->displayType == MapWindow::Disp_Static)
	{
		if(optRailShow())
			showRailways(map, &world->facilities, &world->railways);
	}
#endif

	/*
	 * Display boats and things
	 */

#ifdef TESTING
	world->waterNet.showWaterZones(map, world);
#else
	world->waterNet.showWaterZones(map);
#endif

	/*
	 * Place Facilities
	 */

	if(world->facilities.entries())
	{
		FacilityIter list = world->facilities;

		while(++list)
			map->drawObject(list.current());
	}

	/*
	 * Units
	 */

	if(map->displayType == MapWindow::Disp_Moveable)
	{
		Unit* side = world->ob.sides;
		while(side)
		{
			drawUnits(map, side);
			side = side->sister;
		}

		/*
		 * Temporary Objects
		 */

		if(!tempObjects.isEmpty())
		{
			MobIListIter iter = &tempObjects;

			MobIL* mob;
			while((mob = iter.next()) != 0)
			{
				map->drawObject(mob);
			}
		}

	}

}


void Campaign::drawUnits(MapWindow* map, Unit* ob)
{
	/*
	 * Draw children only if appropriate to map scale
	 *
	 * This needs adjusting so that highlighted units can display their
	 * subordinates.
	 *
	 * Zoomed: Army/Corps/Division/Brigade
	 *   Full: Army/Corps
	 */

	/*
	 * Go through all objects because independant objects need
	 * to be drawn
	 */

	if(ob->childCount && (ob->getRank() <= Rank_Brigade))
	{
		Unit* u = ob->child;
		while(u)
		{
			drawUnits(map, u);
			u = u->sister;
		}
	}

	/*
	 * If its a brigade, draw it as a square
	 */

	/*
	 * Only draw Independant or highlighted units (not presidents)
	 */

#if 0
	if( (ob->getRank() != Rank_President) &&
	    (ob->isCampaignReallyIndependant() || ob->highlight) )
#else
	if(((ob->getRank() != Rank_President) &&
	    ((ob->getCampaignAttachMode() != Attached) || ob->highlight)) ||
		 (ob->getRank() == Rank_Brigade) )
#endif
		map->drawObject(ob);
}

void Campaign::clearInfo()
{
	putLogo(infoArea);
}

Boolean Campaign::EndGameDetection()
{
	const Day   endDay   = 8; 
	const Month endMonth = November; 	
	const Year  endYear  = 1864;

	Boolean won = False;
	Side winner = SIDE_None;
	CampWinHow how;

	if ( ( timeInfo.day == endDay ) && ( timeInfo.month == endMonth )
		&& ( timeInfo.year == endYear ) )
	{
		won = True;
		how = TimeOut;
		unsigned int ratio;
		if(USAVictory >= CSAVictory)
		{
			winner = SIDE_USA;
			if(CSAVictory)
				ratio = USAVictory / CSAVictory;
			else
				ratio = UINT_MAX;
		}
		else
		{
			winner = SIDE_CSA;
			if(USAVictory)
				ratio = CSAVictory / USAVictory;
			else	
				USAVictory = UINT_MAX;
		}

		if(ratio >= 2)
			how = BigTimeOut;
	}
	else
	{
		int usaPercent = (USAVictory * 100) / (USAVictory + CSAVictory);

		if(usaPercent <= CSAWinVictory)
		{
			winner = SIDE_CSA;
			won = True;
		}
		else if(usaPercent >= USAWinVictory)
		{
			winner = SIDE_USA;
			won = True;
		}

		how = Victory;
	}

	/*
	 * Also check for case of one side having no troops left!
	 */

	if(!won)
	{
		for(President* u = world->ob.getSides(); u; u = (President*) u->sister)
		{
			if( (u->childCount == 0) && (u->freeRegimentCount() == 0) )
			{
				winner = otherSide(u->getSide());
				won = True;
				how = Victory;
				break;
			}
		}
	}


	if (won)
		doEndGame(winner, how);

	return won;
}

void Campaign::setVictory()
{
	CSAVictory = 0;
	USAVictory = 0;

	ULONG total = 0;
	
	FacilityIter list = world->facilities;

	while( ++list )
	{
		Facility* f = list.current();	 
			
		total += f->getBigVictory();

		if ( f->side == SIDE_CSA )
			CSAVictory += f->getBigVictory();
		else
			USAVictory += f->getBigVictory();
	}
#ifdef DEBUG
	cLog.printf("USA victory = %ld, CSAvictory = %ld",
		USAVictory, CSAVictory);
#endif
	// CSAWinVictory = 86;
	// USAWinVictory = 68;

	/*
	 * Each side must get 2/3 of enemy resources to win.
	 */

	Realism dl = game->getDifficulty(Diff_Victory);

	/*
	 * Proportion that a player's side should have left to not have lost
	 */

	static UBYTE percent[RealismLevels] = {
		50,			// Simple
		46,			// Average
		42,			// Complex1
		38,			// Complex2
		33,			// Complex3
	};

	UBYTE pCSA = percent[dl];
	UBYTE pUSA = percent[dl];

	/*
	 * In single player game, computer always as 66%
	 * But player has better or worse odds.
	 */

	// if(game->playerCount == 1)
	if(!game->isRemoteActive())
	{
		// if(game->playersSide == SIDE_USA)
		if(game->getLocalSide() == SIDE_USA)
			pUSA = percent[Complex3];
		else
			pCSA = percent[Complex3];
	}

	USAWinVictory = 100 - (CSAVictory * pUSA) / total;
	CSAWinVictory =       (USAVictory * pCSA) / total;

	CSAStartVictory = (USAVictory * 256 + total/2) / total;
}

#if !defined(CAMPEDIT) && !defined(TESTCAMP)

/*========================================================
 * Interface to battle game
 */

char* Campaign::makeWorkFileName()
{
	MemPtr<char> fname(FILENAME_MAX);
	sprintf(fname, "%s\\workfile.tmp", workDir);
	workFileName = copyString(fname);
	mkdir(workDir);		// Make sure directory exists
	return workFileName;
}

/*
 * Prepare things before a battle and return a new Battle OB.
 */

OrderBattle* Campaign::beforeBattle(MarkedBattle* batl)
{
	/*
	 * Write out all dynamic data into a temporary file
	 * NOTE: This name may need to be unique, depending on how the saved
	 *       games are stored in a battle.  Alternatively this file might
	 *       simply be copied into the body of the saved battle game.
	 */

	if(workFileName)
	{
#ifdef DEBUG
		throw GeneralError("Campaign::beforeBattle is called\nbut there is already a workfilename of %s", (char*) workFileName);
#else
		remove(workFileName);
		delete[] workFileName;
		workFileName = 0;
#endif
		
	}

	makeWorkFileName();
	writeDynamicWorld(workFileName, this);

	/*
	 * Make a new OB!
	 */

	/*
	 * Convert aiInfo->facilities into IDs!
	 */

	world->ob.beforeBattle();

	world->railNet.clear();
#ifdef DEBUG
	memLog->track("Deleted RailNet");
#endif

	world->waterNet.clear();
#ifdef DEBUG
	memLog->track("Deleted WaterNet");
#endif

	/*
	 * Delete the Campaign World!
	 */

	world->facilities.clearAndDestroy();

#ifdef DEBUG
	memLog->track("Deleted facilities");
#endif

	world->railways.clear();
#ifdef DEBUG
	memLog->track("Deleted Railways");
#endif

	world->states.clear();
#ifdef DEBUG
	memLog->track("Deleted States");
#endif

	world->mobList.clear();

	return &world->ob;
}

/*
 * Restore Campaign Information after battle
 */

void Campaign::afterBattle(OrderBattle* battleOB)
{
	if(workFileName)
	{
		/*
		 * Recreate Campaign World
		 */

		readStaticWorld(startWorldName, world);
		readDynamicWorld(workFileName, this);
		remove(workFileName);
		delete workFileName;
		workFileName = 0;

		/*
		 * Update OB
		 */

		world->ob.afterBattle();

		/*
		 * Delete Battle OB
		 */
	}
#ifdef DEBUG
	else
		throw GeneralError("Campaign::afterBattle is called but workFileName is NULL");
#endif
}


void Unit::beforeBattle()
{
	convertedForBattle = True;

#ifdef CHRIS_AI
	/*
	 * Convert facilities to IDs
	 */

	if(aiInfo)
	{
		if(aiInfo->fac)
			aiInfo->facID = campaign->world->facilities.getID(aiInfo->fac);
		else
			aiInfo->facID = NoFacility;
	}
#endif

	if(occupying)
		occupyingID = campaign->world->facilities.getID(occupying);
	else
		occupyingID = NoFacility;

	Unit* u = child;
	while(u)
	{
		u->beforeBattle();
		u = u->sister;
	}
}

void Unit::afterBattle()
{
	convertedForBattle = False;

#ifdef CHRIS_AI
	/*
	 * Convert IDs back to faclities
	 */

	if(aiInfo)
	{
		if(aiInfo->facID == NoFacility)
			aiInfo->fac = 0;
		else
		{
			Facility* f = campaign->world->facilities[aiInfo->facID];
			aiInfo->fac = f;
		}
	}
#endif

	if(occupyingID == NoFacility)
		occupying = 0;
	else
	{
		Facility* f = campaign->world->facilities[occupyingID];

		occupying = f;

		if(!f->occupier || (f->occupier->rank > rank))
		{
#ifdef TESTING_NONE
		if(rank == Rank_Regiment)
			throw GeneralError("Regiment %s occupying facility %s!",
				getName(True),
				f->getNameNotNull());
#endif

			if(rank < Rank_Regiment)
			{
				if(siegeAction == SIEGE_Occupying)
					f->occupier = this;
				else if(siegeAction == SIEGE_Besieging)
					f->sieger = this;
			}
		}
	}

	Unit* u = child;
	while(u)
	{
		u->afterBattle();
		u = u->sister;
	}

}

#endif	// CAMPEDIT && TESTCAMP

#if 0
Side Campaign::getAISide() const
{
	ASSERT(ai != 0);

	if(ai)
		return ai->getSide();
	else
		return SIDE_None;
}
#endif
