/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Memory Logging
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.4  1994/08/31  15:23:00  Steven_Green
 * Allow use of module, even if system not initialised.
 *
 * Revision 1.3  1994/07/19  19:53:03  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/06/21  18:49:27  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/03/10  14:27:18  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include <malloc.h>

#include <i86.h>
// #include <dos.h>

#include "memlog.h"
#include "memalloc.h"
#include "error.h"

#ifdef DEBUG
#include "system.h"
#pragma INITIALISE before library
#endif

/*
 * Make sure initialised BEFORE anything else.
 */


/*
 * Resource Exhaustion handling
 */


static MemHandler memHandler = { 0, 0 };

#if 0
MemHandler* setMemHandler(MemHandler* f)
{
	MemHandler* old = memHandler;
	memHandler = f;
	return old;
}
#endif

void addMemHandler(MemHandler* handle)
{
	handle->next = memHandler.next;
	memHandler.next = handle;
}

void removeMemHandler(MemHandler* handle)
{
	MemHandler* last = &memHandler;
	while(last && (last->next != handle))
		last = last->next;
	if(last)
		last->next = handle->next;
}

Boolean freeUpMemory(size_t s)
{
	MemHandler* h = memHandler.next;
	while(h)
	{
		if(h->function(s))
			return True;
		h = h->next;
	}
	return False;
}

#ifdef DEBUG

/*
 * Memory Logging
 */

#include "log.h"

/*
 * Global variable
 */


MemoryLog* memLog = 0;

/*
 * Class to make sure memLog is destructed at end of program
 * otherwise anything that tries to free things afterwards fails!
 */

class MemoryControl {
public:
	MemoryControl() { memLog = 0; }
	~MemoryControl()
	{
		machine.reset();
		delete memLog;
		memLog = 0;
	}
};

static MemoryControl memControl;

static char memID[] = "MBLK";
static char endID[] = "END";


struct MemBlock {
	MemBlock* next;		// My own quick linked list
	MemBlock* prev;

	char id[4];
	size_t howMuch;

	int count;			// How many calls to new before this happens

	void init(size_t s);
	void remove();
};


MemBlock* MemoryLog::makeMemBlock(size_t s)
{
	size_t amount = s + sizeof(MemBlock) + sizeof(endID);

	void* ad = malloc(amount);

#if 0
	if(!ad)
		throw GeneralError("Ran out of memory!");
#else
	if(!ad)
		return 0;
#endif

	memset(ad, 0xaa, amount);

	MemBlock* m = (MemBlock*)ad;

	char* ad1 = (char*) (m + 1);
	ad1 += s;
	memcpy(ad1, endID, sizeof(endID));

	memcpy(m->id, memID, sizeof(m->id));
	m->howMuch = s;
	m->count = newCount++;

	if(last)
	{
		m->prev = last;
		m->next = 0;
		last->next = m;
		last = m;
	}
	else
	{
		m->next = 0;
		m->prev = 0;
		list = m;
		last = m;
	}

	total += s;
	if(total > maximum)
		maximum = total;

	return (MemBlock*) ad;
}

void MemoryLog::show()
{
	if(mode != None)
	{
		*logFile << "    Calls to    new: " << newCount << endl;
		*logFile << "    Calls to delete: " << deleteCount << endl;
		*logFile << "     Mismatch Count: " << newCount - deleteCount << endl;
		*logFile << "Maximum memory used: " << maximum << endl;
		*logFile << " Amount outstanding: " << total << endl << endl;

#if defined(MEMALLOC_H) && !defined(SYSTEM_MALLOC)
		*logFile << "  Calls to malloc: " << mallocCalls << endl;
		*logFile << "    Calls to free: " << freeCalls << endl;
		*logFile << "     Maximum Used: " << maximumAllocated << endl;
		*logFile << "Current Allocated: " << currentAllocated << endl << endl;
#endif

		if(list)
		{
			*logFile << "Memory Blocks still allocated:" << endl;

			MemBlock* block = list;

			while(block)
			{
				MemBlock* m = block;
				block = block->next;

				*logFile << "Call " << setw(6) << m->count << ", " << setw(6) << m->howMuch << " bytes at " << m << setw(0) << endl;	
			}
		}
		else
			*logFile << "No Outstanding memory blocks" << endl;

	}
}

void MemoryLog::track(const char* s)
{
	if(mode != None)
	{
		*logFile << "----| Memory Track: " << s << " |----" << endl;
		*logFile << "New: " << setw(6) << newCount;
		*logFile << ", del: " << setw(6) << deleteCount;
		*logFile << ", used: " << setw(6) << newCount - deleteCount;
		*logFile << ", Max: " << setw(8) << maximum;
		*logFile << ", inUse: " << setw(8) << total << setw(0) << endl << endl;
	}
}

void MemoryLog::addText(const char* s)
{
	if(mode != None)
		*logFile << "----| " << s << " |----" << endl;
}

void* MemoryLog::operator new(size_t s)
{
	return malloc(s);
}

void MemoryLog::operator delete(void* p)
{
	free(p);
}

void MemoryLog::setMode(const char* name, Mode m)
{

	mode = m;

	if(mode != None)
		logFile = new LogStream(name);
	else
		logFile = 0;

	if(logFile)
	{
		struct MemInfo {
			unsigned long largest;
			unsigned long maxPage;
			unsigned long largeBlock;
			unsigned long linSpace;
			unsigned long freePages;
			unsigned long physPages;
			unsigned long totalPhysicalPages;
			unsigned long freeLinSpace;
			unsigned long sizeofPage;
			unsigned long reserved[3];
		} memInfo;

		union REGS regs;
		struct SREGS sregs;
		regs.x.eax = 0x500;
		memset(&sregs, 0, sizeof(sregs));
		sregs.es = FP_SEG(&memInfo);
		regs.x.edi = FP_OFF(&memInfo);
		int386x(0x31, &regs, &regs, &sregs);

		*logFile << "DPMI Memory\n";
		*logFile << "Largest Block: " << memInfo.largest << "\n";
		*logFile << "Max unlocked Page: " << memInfo.maxPage << "\n";
		*logFile << "Largest Lockable: " <<	memInfo.largeBlock << "\n";
		*logFile << "Total Linear Space: " <<	memInfo.linSpace << "\n";
		*logFile << "Free Pages: " <<	memInfo.freePages << "\n";
		*logFile << "Free Physical Pages: " <<	memInfo.physPages << "\n";
		*logFile << "Total Physical Pages: " <<	memInfo.totalPhysicalPages << "\n";
		*logFile << "Free Linear Space: " <<	memInfo.freeLinSpace << "\n";
		*logFile << "Size of paging partition: " << memInfo.sizeofPage << "\n" << endl;

		/*
		 * See what DOS has to offer?
		 */

		memset(&sregs, 0, sizeof(sregs));
		regs.x.eax = 0x0100;
		regs.x.ebx = 0xffff;		// Allocate more than is available

		int386(0x31, &regs, &regs);

		*logFile << "DOS large block: " << ((long)regs.w.bx << 4) << " bytes\n" << endl;

	}
}

MemoryLog::MemoryLog()
{
	list = 0;
	last = 0;

	lockCount = 0;
	total = 0;

	newCount = 0;
	deleteCount = 0;

	maximum = 0;

	mode = None;
	logFile = 0;
}

MemoryLog::~MemoryLog()
{
	show();

	if(mode != None)
		delete logFile;

	memLog = 0;	// Don't try to use ourself now!

	MemBlock* m = list;

	while(m)
	{
		MemBlock* next = m->next;
		free(m);
		m = next;
	}

}


void* MemoryLog::allocate(size_t s)
{
	MemBlock* ad = makeMemBlock(s);

	if(!ad)
		return 0;

	if(!isLocked() && (mode == Full))
	{
		lock();
		*logFile << startLogLine << "Alloc " << ad << " " << setw(6) << s << " " << total << setw(0) << endl;
		unlock();
	}
	return ad + 1;
}


void MemoryLog::removeBlock(void* p)
{
	if(p)
	{
		MemBlock* m = ((MemBlock *)p) - 1;

		if(memcmp(m->id, memID, sizeof(m->id)))
		{
			if(mode != None)
			{
				lock();
				*logFile << "MEMORY ERROR: block at " << p << " has wrong ID" << endl;
				unlock();
			}

			throw GeneralError("Memory Error: bad ID");
		}

		char* ad1 = ((char*)p) + m->howMuch;
		if(memcmp(ad1, endID, sizeof(endID)))
			throw GeneralError("Memory Error: Overwritten end");

		total -= m->howMuch;
		deleteCount++;

		if(!isLocked() && (mode == Full))
		{
			lock();
			*logFile << startLogLine << "Free  " << m << " " << setw(6) << m->howMuch << " " << total << setw(0) << endl;
			unlock();
		}
		
		/*
		 * Delete
		 */

		MemBlock* next = m->next;
		MemBlock* prev = m->prev;

		if(next)
			next->prev = prev;
		else
			last = prev;
		if(prev)
			prev->next = next;
		else
			list = next;


		memset(m, 0x55, m->howMuch);
		free(m);
	}
}

/*
 * Replacements for system new/delete
 */

void* operator new(size_t s)
{
	if(memLog)
	{
		void* ad;
	
		while( (ad = memLog->allocate(s)) == 0)
		{
			// if(!memHandler || !memHandler(s))
			if(!freeUpMemory(s))
				throw GeneralError("Out of Memory!");
		}
	
		return ad;
	}
	else
	{
		void* ad;
	
		while( (ad = malloc(s)) == 0)
		{
			// if(!memHandler || !memHandler(s))
			if(!freeUpMemory(s))
				throw GeneralError("Out of Memory!");
		}
	
		return ad;
	}
}

void operator delete(void* p)
{
	if(memLog)
		memLog->removeBlock(p);
	else
	{
		free(p);
	}
}


#else	// Not DEBUG


/*
 * Replacements for system new/delete
 */

void* operator new(size_t s)
{
	void* ad;
	
	while( (ad = malloc(s)) == 0)
	{
		// if(!memHandler || !memHandler(s))
		if(!freeUpMemory(s))
			throw GeneralError("Out of Memory!");
	}
	
	return ad;
}

void operator delete(void* p)
{
	free(p);
}

#endif
