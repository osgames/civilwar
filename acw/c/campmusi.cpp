/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Music Player
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include "campmusi.h"

#if defined(SOUND_DSMI)

#include "sound.h"
#include "game.h"
#include "files.h"
#include "memptr.h"

/*
 * List of songs to be played
 */

static char* songNames[] = {
	"batlcry",
	"batlhymn",
	"batlmthr",
	"bonblufl",
	"cleartrk",
	"cumbrlnd",
	"dixie",
	"johnny",
	"lincoln",
	"maryland",
	"richmond",
	"rollalab",
	"tenting",
	"tramp",
	"ylorose",
	0
};

CampaignMusic::CampaignMusic()
{
	disabled = 0;		// Start off enabled
	forced = False;
	killMe = False;
	song = 0;

	int count = 0;
	char** sPtr = songNames;
	while(*sPtr++)
		count++;
	songCount = count;

	/*
	 * Don't Load song, etc, yet, because game is uninitialised
	 */

}

CampaignMusic::~CampaignMusic()
{
	stopSong();
}

void CampaignMusic::enable()
{
	if(disabled)  		// Shouldn't be necessary
		disabled--;
}

void CampaignMusic::stopSong()
{
	if(song)
	{
		sound.stopMusic(song);
		sound.unloadSong(song);
		song = 0;
	}
}

void CampaignMusic::disable()
{
	if(!disabled++)
		stopSong();
}

Boolean CampaignMusic::forceMusic(const char* fileName)
{
	stopSong();
	forced = True;
	song = loadSong(fileName);
	if(song)
	{
		sound.playMusic(song);
		return True;
	}
	else
		return False;
}

void CampaignMusic::endForce()
{
	forced = False;		// Let current song finish and then continue random
}


Boolean CampaignMusic::proc()
{
	if( ((ampGetModuleStatus() & MD_PLAYING) == 0) || !song)
	{
		if(!forced)
			stopSong();

		if(!song && !forced && game->musicInGame)
		{
			int sNum = game->miscRand(songCount);

			song = loadSong(songNames[sNum]);
		}
	
	 	if(song)
		{
			sound.playMusic(song);

			if((ampGetModuleStatus() & MD_PLAYING) == 0)
			{
				stopSong();
				game->musicInGame = False;		
			}
		}
	}

	return killMe;
}

MODULE* CampaignMusic::loadSong(const char* songName)
{
	MemPtr<char> fName(FILENAME_MAX);

	sprintf(fName, "%s\\%s.amf", musicDir, songName);

	return sound.loadMusic(fName);
}

#endif   // defined(SOUND_DSMI)
