/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Test Icons for shape test program
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.12  1994/02/28  23:04:17  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/01/07  23:43:18  Steven_Green
 * Use Image::getWidth/Height instead of accessing fields directly.
 *
 * Revision 1.10  1994/01/06  22:38:30  Steven_Green
 * Mouse button enumerations are Mouse::
 *
 * Revision 1.9  1993/12/15  20:56:36  Steven_Green
 * Gourad/Texture Icons are bigger and have a letter drawn in the middle.
 *
 * Revision 1.8  1993/12/14  23:29:01  Steven_Green
 * Gourad and Texture on/off icons added.
 *
 * Revision 1.7  1993/12/13  21:59:58  Steven_Green
 * Light source adjusted using height abnd bearing angles instead of x,y,z
 *
 * Revision 1.6  1993/12/13  17:11:43  Steven_Green
 * Keyboard shortcuts added
 *
 * Revision 1.5  1993/12/10  16:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1993/12/04  01:02:56  Steven_Green
 * Icons are static... automatic ones didn't exist after intialisation and
 * so the program bombed!
 *
 * Revision 1.3  1993/12/03  17:01:13  Steven_Green
 * Icon classes and setup done in here rather than burdening other modules.
 *
 * Revision 1.2  1993/12/01  15:11:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/30  02:57:11  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <conio.h>
#include <ctype.h>

#include "testicon.h"
#include "options.h"
#include "test3d.h"
#include "mouse.h"
#include "view3d.h"
#include "text.h"

/*
 * Icon primitives
 */

void Icon::draw(void* data)
{
	data;		// Avoid warning

	if(bm == 0)
		initDraw();

	bm->fill(0x20);
	bm->frame(0, 0, getW() - 1, getH() - 1, 0x21, 0x23);
	bm->frame(1, 1, getW() - 3, getH() - 3, 0x22, 0x24);
}

void Icon::endDraw(void* data)
{
	IconData& iconData = *(IconData*)data;

	iconData.mouse->blit(*bm, *this);
	delete bm;
	bm = 0;
};

void drawIcon(Icon* icon, void* data)
{
	icon->draw(data);
}

void executeIcon(Icon* icon, void* data)
{
	IconData& iconData = *(IconData*)data;

	if(icon->matchKey(iconData.lastKey))
		icon->execute(Mouse::LeftButton, data);
	else
	{
		Point mousePosition = iconData.mouse->getPosition();

		if( (mousePosition.x >= icon->getX()) &&
		 	(mousePosition.x < (icon->getX() + icon->getW())) &&
		 	(mousePosition.y >= icon->getY()) &&
		 	(mousePosition.y < (icon->getY() + icon->getH())) )
		{
			icon->execute(iconData.mouse->buttons, data);
		}
	}
}


class xPlaneIcon : public Icon {
public:

	xPlaneIcon(Rect r) : Icon(r) { };

	void execute(short buttons, void* data);
	void draw(void* data);
};

class yPlaneIcon : public Icon {
public:

	yPlaneIcon(Rect r) : Icon(r) { };

	void execute(short buttons, void* data);
	void draw(void* data);
};

class zPlaneIcon : public Icon {
public:

	zPlaneIcon(Rect r) : Icon(r) { };

	void execute(short buttons, void* data);
	void draw(void* data);
};

class xRotateIcon : public Icon {
public:

	xRotateIcon(Rect r) : Icon(r) { };

	void execute(short buttons, void* data);
	void draw(void* data);
};


class yRotateIcon : public Icon {
public:

	yRotateIcon(Rect r) : Icon(r) { };

	void execute(short buttons, void* data);
	void draw(void* data);
};


class zRotateIcon : public Icon {
public:

	zRotateIcon(Rect r) : Icon(r) { };

	void execute(short buttons, void* data);
	void draw(void* data);
};


class ViewFrontIcon : public Icon {
public:

	ViewFrontIcon(Rect r) : Icon(r) { };

	void execute(short buttons, void* data);
	void draw(void* data);
};


class ViewWidthIcon : public Icon {
public:

	ViewWidthIcon(Rect r) : Icon(r) { };

	void execute(short buttons, void* data);
	void draw(void* data);
};

class ProjectionIcon : public Icon {
public:

	ProjectionIcon(Rect r) : Icon(r) { };

	void execute(short buttons, void* data);
	void draw(void* data);
};


class ViewDistIcon : public Icon {
public:

	ViewDistIcon(Rect r) : Icon(r) { };

	void execute(short buttons, void* data);
	void draw(void* data);
};

class BattleQuitIcon : public Icon {
public:

	BattleQuitIcon(Rect r) : Icon(r, 27) { };

	void execute(short buttons, void* data);
	void draw(void* data);
};

class GouradIcon : public Icon {
public:

	GouradIcon(Rect r) : Icon(r, 'G') { };

	void execute(short buttons, void* data);
	void draw(void* data);
};

class TextureIcon : public Icon {
public:

	TextureIcon(Rect r) : Icon(r, 'T') { };

	void execute(short buttons, void* data);
	void draw(void* data);
};



/*
 * Specific Icons
 */


void xPlaneIcon::execute(short buttons, void* data)
{
	IconData& iconData = *(IconData*)data;

	if(buttons & Mouse::LeftButton)
		iconData.view->moveX(50);
	else if(buttons & Mouse::RightButton)
		iconData.view->moveX(-50);
}

void xPlaneIcon::draw(void* data)
{
	Icon::draw(data);

	SDimension midY = SDimension(getH() / 2);
	SDimension right = SDimension(getW() - 8);

	bm->line(Point(4, midY), Point(right, midY), Colour(0x30));
	bm->line(Point(5, midY-1), Point(5, midY+1), Colour(0x30));
	bm->line(Point(right-1, midY-1), Point(right-1, midY+1), Colour(0x30));

	endDraw(data);
}






void yPlaneIcon::execute(short buttons, void* data)
{
	IconData& iconData = *(IconData*)data;

	if(buttons & Mouse::LeftButton)
		iconData.view->moveY(50);
	else if(buttons & Mouse::RightButton)
		iconData.view->moveY(-50);
}

void yPlaneIcon::draw(void* data)
{
	Icon::draw(data);

	SDimension midX = SDimension(getW() / 2);
	SDimension bottom = SDimension(getH() - 8);

	bm->line(Point(midX, 4), Point(midX, bottom), Colour(0x30));
	bm->line(Point(midX-1, 5), Point(midX+1, 5), Colour(0x30));
	bm->line(Point(midX-1, bottom-1), Point(midX+1, bottom-1), Colour(0x30));

	endDraw(data);
}





void zPlaneIcon::execute(short buttons, void* data)
{
	IconData& iconData = *(IconData*)data;

	if(buttons & Mouse::LeftButton)
		iconData.view->moveZ(50);
	else if(buttons & Mouse::RightButton)
		iconData.view->moveZ(-50);
}

void zPlaneIcon::draw(void* data)
{
	Icon::draw(data);

	SDimension right = SDimension(getW() - 8);
	SDimension bottom = SDimension(getH() - 8);

	bm->line(Point(4, bottom), Point(right, 4), Colour(0x30));
	bm->line(Point(4, bottom-1), Point(5, bottom), Colour(0x30));
	bm->line(Point(right-1, 4), Point(right, 5), Colour(0x30));

	endDraw(data);
}



void xRotateIcon::execute(short buttons, void* data)
{
	IconData& iconData = *(IconData*)data;

	if(buttons & Mouse::LeftButton)
		iconData.view->rotX(Degree(5));
	else if(buttons & Mouse::RightButton)
		iconData.view->rotX(Degree(-5));
}

void xRotateIcon::draw(void* data)
{
	Icon::draw(data);

	SDimension midY = SDimension(getH() / 2);
	SDimension right = SDimension(getW() - 8);

	bm->line(Point(4, midY), Point(right, midY), Colour(0x30));
	bm->line(Point(getW() / 2, midY - 4), Point(getW() / 2, midY + 4), Colour(0x30));

	endDraw(data);
}


void yRotateIcon::execute(short buttons, void* data)
{
	IconData& iconData = *(IconData*)data;

	if(buttons & Mouse::LeftButton)
		iconData.view->rotY(Degree(5));
	else if(buttons & Mouse::RightButton)
		iconData.view->rotY(Degree(-5));
}

void yRotateIcon::draw(void* data)
{
	Icon::draw(data);

	SDimension midX = SDimension(getW() / 2);
	SDimension bottom = SDimension(getH() - 8);

	bm->line(Point(midX, 4), Point(midX, bottom), Colour(0x30));
	bm->line(Point(midX-4, getH()/2), Point(midX+4, getH()/2), Colour(0x30));

	endDraw(data);
}


void zRotateIcon::execute(short buttons, void* data)
{
	IconData& iconData = *(IconData*)data;

	if(buttons & Mouse::LeftButton)
		iconData.view->rotZ(Degree(5));
	else if(buttons & Mouse::RightButton)
		iconData.view->rotZ(Degree(-5));
}

void zRotateIcon::draw(void* data)
{
	Icon::draw(data);

	SDimension right = SDimension(getW() - 8);
	SDimension bottom = SDimension(getH() - 8);

	bm->line(Point(4, bottom), Point(right, 4), Colour(0x30));
	bm->line(Point(getW()/2-4, getH()/2), Point(getW()/2+4, getH()/2), Colour(0x30));


	endDraw(data);
}

void ViewFrontIcon::execute(short buttons, void* data)
{
	IconData& iconData = *(IconData*)data;

	if(buttons & Mouse::LeftButton)
		iconData.view->moveFront(1);
	else if(buttons & Mouse::RightButton)
		iconData.view->moveFront(-1);

	draw(data);
}

void ViewFrontIcon::draw(void* data)
{
	IconData& iconData = *(IconData*)data;

	Icon::draw(data);

	SDimension right = SDimension(getW() - 4);

	SDimension left = SDimension(right - iconData.view->getFront() * (right - 4) / 150);
	if(left < 4)
		left = 4;


	SDimension top = 4;
	SDimension bottom = SDimension(getH() - 4);
	SDimension midY = SDimension(getH() / 2);
	Point centre(left, midY);

	if(iconData.view->getProjection() == Parallel)
	{
		bm->line( Point(left, top), Point(right, top), 0x30 );
		bm->line( centre, Point(right, midY), 0x30);
		bm->line( Point(left, bottom), Point(right, bottom), 0x30 );
		bm->line( Point(right, top), Point(right, bottom), 0x30);
	}
	else
	{
		bm->line( centre, Point(right, top+2), 0x30);
		bm->line( centre, Point(right, bottom-2), 0x30);
		bm->line( centre, Point(right, midY), 0x30);
		bm->line( Point(right, top), Point(right, bottom), 0x30);
	}

	endDraw(data);
}

void ViewWidthIcon::execute(short buttons, void* data)
{
	IconData& iconData = *(IconData*)data;

	if(buttons & Mouse::LeftButton)
		iconData.view->moveViewWidth(1);
	else if(buttons & Mouse::RightButton)
		iconData.view->moveViewWidth(-1);

	draw(data);
}

void ViewWidthIcon::draw(void* data)
{
	IconData& iconData = *(IconData*)data;

	Icon::draw(data);

	SDimension right = SDimension(getW() - 4);

	SDimension left = SDimension(right - iconData.view->getFront() * (right - 4) / 150);
	if(left < 4)
		left = 4;


	SDimension top = 4;
	SDimension bottom = SDimension(getH() - 4);
	SDimension midY = SDimension(getH() / 2);
	Point centre(left, midY);

	if(iconData.view->getProjection() == Parallel)
	{
		bm->line( Point(left, top), Point(right, top), 0x3f );
		bm->line( centre, Point(right, midY), 0x3f);
		bm->line( Point(left, bottom), Point(right, bottom), 0x3f );
		bm->line( Point(right, top), Point(right, bottom), 0x30);
	}
	else
	{
		bm->line( centre, Point(right, top+2), 0x3f);
		bm->line( centre, Point(right, bottom-2), 0x3f);
		bm->line( centre, Point(right, midY), 0x3f);
		bm->line( Point(right, top), Point(right, bottom), 0x30);
	}

	endDraw(data);
}



void ProjectionIcon::execute(short buttons, void* data)
{
	IconData& iconData = *(IconData*)data;

	static Cord3D lastWidth = 2000;

	if(buttons & Mouse::LeftButton)
	{
		if(iconData.view->getProjection() != Parallel)
		{
			Cord3D oldW = iconData.view->getViewWidth();
			iconData.view->setViewWidth(lastWidth);
			lastWidth = oldW;
		}

		iconData.view->setParallel();
	}
	else if(buttons & Mouse::RightButton)
	{
		if(iconData.view->getProjection() != Perspective)
		{
			Cord3D oldW = iconData.view->getViewWidth();
			iconData.view->setViewWidth(lastWidth);
			lastWidth = oldW;
		}

		iconData.view->setPerspective();
	}

	draw(data);
}

void ProjectionIcon::draw(void* data)
{
	IconData& iconData = *(IconData*)data;

	Icon::draw(data);

	SDimension right = SDimension(getW() - 4);

	SDimension left = 4;

	SDimension top = 4;
	SDimension bottom = SDimension(getH() - 4);
	SDimension midY = SDimension(getH() / 2);
	Point centre(left, midY);

	Colour col1, col2;

	if(iconData.view->getProjection() == Parallel)
	{
		col1 = 0x30;
		col2 = 0x3f;
	}
	else
	{
		col1 = 0x3f;
		col2 = 0x30;
	}

	bm->line( Point(right, top), Point(right, bottom), 0x30);	// viewplane
	bm->line( centre, Point(right, midY), 0x30);						// Midpoint
	
	bm->line( Point(left, top), Point(right, top), col1 );
	bm->line( Point(left, bottom), Point(right, bottom), col1 );

	bm->line( centre, Point(right, top+2), col2);
	bm->line( centre, Point(right, bottom-2), col2);
	

	endDraw(data);
}

void ViewDistIcon::execute(short buttons, void* data)
{
	IconData& iconData = *(IconData*)data;

	if(buttons & Mouse::LeftButton)
		iconData.view->moveDistance(50);
	else if(buttons & Mouse::RightButton)
		iconData.view->moveDistance(-50);

}

void ViewDistIcon::draw(void* data)
{
	Icon::draw(data);

	SDimension right = SDimension(getW() - 8);
	SDimension bottom = SDimension(getH() - 8);

	bm->line(Point(4, bottom), Point(right, 4), Colour(0x30));
	bm->line(Point(4, bottom-1), Point(5, bottom), Colour(0x30));
	bm->line(Point(right-1, 4), Point(right, 5), Colour(0x30));

	endDraw(data);
}

void BattleQuitIcon::draw(void* data)
{
	Icon::draw(data);

	bm->frame(4, 4, getW() - 8, getH() - 8, 0x30);

	endDraw(data);
}

void BattleQuitIcon::execute(short buttons, void* data)
{
	data=data;

	if(buttons)
		quitProgram = True;
}

/*
 * Gourad and Texture on/off icons
 */

void GouradIcon::draw(void* data)
{
	Icon::draw(data);

	if(optGourad())
		bm->box(2, 2, getW() - 2, getH() - 2, 0x30);
	else
		bm->frame(2, 2, getW() - 2, getH() - 2, 0x30);

	font.move(Point(getW() / 2 - 4, getH() / 2 - 8));
	bm->text('G');

	endDraw(data);
}


void TextureIcon::draw(void* data)
{
	Icon::draw(data);

	if(optTexture())
		bm->box(2, 2, getW() - 2, getH() - 2, 0x30);
	else
		bm->frame(2, 2, getW() - 2, getH() - 2, 0x30);

	font.move(Point(getW() / 2 - 4, getH() / 2 - 8));
	bm->text('T');

	endDraw(data);
}

void GouradIcon::execute(short buttons, void* data)
{
	if(buttons)
	{
		IconData& iconData = *(IconData*)data;
		globalOptions.toggle(Options::Gourad);
		iconData.update();
	}
}

void TextureIcon::execute(short buttons, void* data)
{
	if(buttons)
	{
		IconData& iconData = *(IconData*)data;
		globalOptions.toggle(Options::Texture);
		iconData.update();
	}
}




/*
 * Light Source Movement
 */

class hLightIcon : public Icon {
public:
	hLightIcon(Rect r) : Icon(r) { };

	void execute(short buttons, void* data);
	void draw(void* data);
};

class bLightIcon : public Icon {
public:
	bLightIcon(Rect r) : Icon(r) { };

	void execute(short buttons, void* data);
	void draw(void* data);
};



void hLightIcon::execute(short buttons, void* data)
{
	IconData& iconData = *(IconData*)data;

	if(buttons & Mouse::LeftButton)
	{
		iconData.light->rotHeight(Degree(10));
		iconData.update();
	}
	else if(buttons & Mouse::RightButton)
	{
		iconData.light->rotHeight(Degree(-10));
		iconData.update();
	}
}

void bLightIcon::execute(short buttons, void* data)
{
	IconData& iconData = *(IconData*)data;

	if(buttons & Mouse::LeftButton)
	{
		iconData.light->rotBearing(Degree(10));
		iconData.update();
	}
	else if(buttons & Mouse::RightButton)
	{
		iconData.light->rotBearing(Degree(-10));
		iconData.update();
	}
}


void hLightIcon::draw(void* data)
{
	IconData& iconData = *(IconData*)data;

	Icon::draw(data);

	SDimension size = SDimension((iconData.light->x * (getW() - 8)) / 256);

	SDimension left, right;

	if(size < 0)
	{
		right = SDimension(getW() - 4);
		left = SDimension(right + size);
	}
	else
	{
		left = 4;
		right = SDimension(left + size);
	}

	SDimension midY = SDimension(getH() / 2);
	SDimension y = 0;

	while(y < midY)
	{
		bm->line(Point(left, midY + y), Point(right, midY + y), Colour(0x57));
		bm->line(Point(left, midY - y), Point(right, midY - y), Colour(0x57));

		y += 2;
	}

	endDraw(data);
}

void bLightIcon::draw(void* data)
{
	IconData& iconData = *(IconData*)data;

	Icon::draw(data);

	SDimension size = SDimension((iconData.light->y * (getH() - 8)) / 256);

	SDimension top, bottom;

	if(size < 0)
	{
		bottom = SDimension(getH() - 4);
		top = SDimension(bottom + size);
	}
	else
	{
		top = 4;
		bottom = SDimension(top + size);
	}

	SDimension midX = SDimension(getW() / 2);
	SDimension x = 0;

	while(x < midX)
	{
		bm->line(Point(midX + x, top), Point(midX + x, bottom), Colour(0x57));
		bm->line(Point(midX - x, top), Point(midX - x, bottom), Colour(0x57));

		x += 2;
	}

	endDraw(data);
}


static xPlaneIcon xIcon = Rect(520,32,32,32);
static yPlaneIcon yIcon = Rect(520,72,32,32);
static zPlaneIcon zIcon = Rect(520,112,32,32);
static xRotateIcon xRotIcon = Rect(560,32,32,32);
static yRotateIcon yRotIcon = Rect(560,72,32,32);
static zRotateIcon zRotIcon = Rect(560,112,32,32);
static ViewFrontIcon viewFrontIcon = Rect(520, 160, 32, 32);
static ViewWidthIcon viewWidthIcon = Rect(560, 160, 32, 32);
static ProjectionIcon projectionIcon = Rect(560, 200, 32, 32);
static ViewDistIcon viewDistIcon = Rect(520, 200, 32, 32);
static BattleQuitIcon battleQuitIcon = Rect(4,4,20,20);

static GouradIcon GouradIcon = Rect(520,240,16,16);
static TextureIcon TextureIcon = Rect(540,240,16,16);

static hLightIcon hLightIcon = Rect(600,32,32,32);
static bLightIcon bLightIcon = Rect(600,72,32,32);

void IconData::make()
{
	list.insert(&xIcon);
	list.insert(&yIcon);
	list.insert(&zIcon);
	list.insert(&xRotIcon);
	list.insert(&yRotIcon);
	list.insert(&zRotIcon);
	list.insert(&viewFrontIcon);
	list.insert(&viewWidthIcon);
	list.insert(&projectionIcon);
	list.insert(&viewDistIcon);
	list.insert(&hLightIcon);
	list.insert(&bLightIcon);

	list.insert(&GouradIcon);
	list.insert(&TextureIcon);

	list.insert(&battleQuitIcon);
}

void IconData::execute()
{
	if(kbhit())
		lastKey = toupper(getch());
	else
		lastKey = 0;

	list.forAll(executeIcon, this);

	if(redraw)
		draw();
}

void IconData::draw()
{
	list.forAll(drawIcon, this);
	redraw = False;
}

