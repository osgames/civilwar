/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Error trapping
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/06/09  23:32:59  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>

#include "error.h"
#include "types.h"

static Boolean GeneralError::doingError = False;
static Boolean GeneralError::firstError = False;

GeneralError::GeneralError()
{
	describe = 0;
}

GeneralError::GeneralError(const char* fmt, ...)
{
	char buffer[500];

	va_list vaList;
	va_start(vaList, fmt);
	_vbprintf(buffer, 500, fmt, vaList);
	va_end(vaList);

	/*
	 * Convert any \r's into \r\n
	 */

	char* s = buffer;
	while(*s)
	{
		if(*s++ == '\r')
		{
			char* s1 = s;
			char last = '\n';

			do
			{
				char c = *s1;
				*s1 = last;
				last = c;

				s1++;
			}  while(last);
			*s1 = 0;
		}
	}

	setMessage(buffer);
}

void GeneralError::setMessage(const char* s)
{
	firstError = doingError;
	doingError = True;

	describe = strdup(s);

	if(!firstError)
	{
		FILE* fp = fopen("acw.bug", "a");

		if(fp)
		{
			time_t t;
			time(&t);

			fprintf(fp, "------------------------------------------\n");
			fprintf(fp, "General Error Report\n");
			fprintf(fp, "at %s\n", ctime(&t));
			fprintf(fp, "%s\n", s);
			fclose(fp);
		}
	}
}

const char* GeneralError::get()
{
	return describe;
}

