/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Display Key Scan codes!
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <conio.h>

typedef enum {
	False,
	True
} Boolean;

void main()
{
	Boolean finished = False;

	while(!finished)
	{
		int key = getch();

		if(key == 0)
			key = getch() + 0x8000;

		printf("%04x\n", key);

		if(key == 27)
			finished = True;
	}
}
