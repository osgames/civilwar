/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Timer for testing
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.4  1994/03/10  14:27:18  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1993/12/10  16:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/12/01  15:11:02  Steven_Green
 * High Res timer used
 *
 * Revision 1.1  1993/11/30  02:57:11  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "testtime.h"

/*
 * Global Timer
 */

TestTimer testTimer;

/*
 * Functions
 */

void TestTimer::mark(const char *s)
{
	unsigned long newTimer = timer->getFastCount();
	timers[nextTimer] = newTimer - lastCount;
	description[nextTimer++] = s;
	lastCount = newTimer;
}

void TestTimer::show(ostrstream& buffer) const
{
	int i = 0;

	while(i < nextTimer)
	{
		buffer << description[i] << ":" << timers[i];
 		buffer << ", ";

		i++;
	}

	buffer << "All:" << timer->getFastCount() - start;

	buffer << '\0';
}
