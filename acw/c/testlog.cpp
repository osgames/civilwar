/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Test the log
 */

#include "log.h"
#include "timer.h"

void main()
{
	Timer* timer = new Timer;

	log("Hello!.. waiting for " << 50 << " ticks");
	timer->wait(50);
	log("Hello again.. waiting for another " << 20 << " ticks!");
	timer->wait(20);
	log("Right... we can say goodbye now!");
}

