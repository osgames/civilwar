/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Set up a battle from the campaign game
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "batsetup.h"
#include "batsel.h"
#include "batldata.h"
#include "campbatl.h"
#include "map3d.h"
#include "unit3d.h"
#include "game.h"
#include "staticob.h"
#include "batltab.h"
#include "campaign.h"
#include "campwld.h"
#include "dialogue.h"
#include "language.h"

/*
 * How to convert campaign terrain to battlefield
 */

struct TerrainConvert {
	TerrainType defaultTerrain;
	UBYTE hillChance;				// 0=flat, 255=maximum
	UBYTE riverChance;			// 0=no river, 255=always a river
	UBYTE roadChance;				// 0=no roads, 255=lots of roads
	UBYTE railChance;				// 0=no railways, 255=lots of railways
	UBYTE buildings;				// 0=no buildings, 255=lots of buildings

	UBYTE terrainChance[NormalTerrainCount-NormalTerrain];	// Chance of adding other types of terrain
};

static TerrainConvert terrainConvert[] = {
	// CT_Unused0

	{
		PastureTerrain, 32, 128, 128, 128, 128,
		{ 0, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16 }
	},

	// CT_Sea

	{
		WaterTerrain, 0, 0, 0, 0, 0,
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
	},

	// CT_Coastal

	{
		MarshTerrain, 8, 200, 16, 0, 32,
		{ 0, 0, 16, 0, 4, 8, 0, 0, 0, 8, 8, 16, 0, 0, 0, 0 }
	},

	// CT_River

	{
		MarshTerrain, 80, 255, 32, 16, 32,
		{ 0, 0, 16, 0, 4, 8, 0, 0, 0, 8, 8, 16, 0, 0, 0, 0 }
	},

	// CT_Stream

	{
		MarshTerrain, 100, 255, 64, 16, 32,
		{ 0, 0, 16, 0, 4, 8, 0, 0, 0, 8, 8, 16, 0, 0, 0, 0 }
	},

	// CT_Swamp

	{
		MarshTerrain, 4, 100, 70, 8, 30,
		{ 0, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16 }
	},

	// CT_Wooded

	{
		LightWoodTerrain, 48, 8, 80, 32, 32,
		{ 0, 128, 0, 64, 64, 16, 4, 4, 4, 16, 16, 4, 16, 16, 0, 0 }
	},

	// CT_Farmland

	{
		PastureTerrain, 48, 128, 128, 128, 64,
		{ 0, 16, 16, 16, 16, 0, 16, 16, 16, 16, 4, 4, 4, 0, 0, 0 }
	},

	// CT_Mountain

	{
		RoughTerrain, 200, 64, 64, 32, 32,
		{ 0, 12, 12, 12, 12, 4, 0, 0, 0, 0, 4, 4, 0, 0, 8, 8 }
	},

	// CT_Rough

	{
		RoughTerrain, 64, 64, 64, 32, 32,
		{ 0, 16, 16, 16, 16, 16, 16, 4, 4, 4, 8, 4, 4, 0, 8, 8 }
	},

	// CT_Impassable

	{
		PastureTerrain, 32, 128, 128, 128, 128,
		{ 0, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16 }
	},

	// CT_Unused11

	{
		PastureTerrain, 32, 128, 128, 128, 128,
		{ 0, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16 }
	},

	// CT_Unused12

	{
		PastureTerrain, 32, 128, 128, 128, 128,
		{ 0, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16 }
	},

	// CT_Unused13

	{
		PastureTerrain, 32, 128, 128, 128, 128,
		{ 0, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16 }
	},

	// CT_Unused14

	{
		PastureTerrain, 32, 128, 128, 128, 128,
		{ 0, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16 }
	},

	// CT_Unused15

	{
		PastureTerrain, 32, 128, 128, 128, 128,
		{ 0, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16 }
	}

};


#ifdef DEBUG_CREATE

class File {
public:
	FILE* fp;

	File(const char* name)
	{
		fp = fopen(name, "w");
	}

	~File()
	{
		if(fp)
			fclose(fp);
	}

	operator FILE* () { return fp; }
};

File createLog("create.log");

void dumpGrid(Grid* g, const char* title)
{
	static callCount = 0;

	callCount++;

	if(createLog.fp)
	{
		fprintf(createLog.fp, "\n-----\n%s\n-----\n\n", title);

		int line = g->pointCount;

		Height8* hp = g->heights;

		while(line--)
		{
			int col = g->pointCount;
			int x = 13;

			fprintf(createLog.fp, "%5d %5d: ", callCount, line);


			while(col--)
			{
				fprintf(createLog.fp, "%02x", hp->height);
				hp++;

				x += 3;

				if(x < 80)
					fprintf(createLog.fp, " ");
				else
				{
					fprintf(createLog.fp, "\n");
					x = 0;
				}
			}

			fprintf(createLog.fp, "\n");
		}
	}
}



#endif


void BattleData::makeBattleField(BattleCreate* parameters)
{
	PopupText popup( language( lge_CreateBF ) );
	Grid* g = grid->getGrid(Zoom_Min);		// this is where we are copying from

	/*
	 * Make up a terrain based on averaging the 9 terrains
	 *
	 * The middle grid has 8 times more weighting than the outside ones
	 */

	TerrainType defaultTerrain;
	UWORD hillChance = 0;				// 0=flat, 255=maximum
	UWORD riverChance = 0;			// 0=no river, 255=always a river
	UWORD roadChance = 0;				// 0=no roads, 255=lots of roads
	UWORD railChance = 0;				// 0=no railways, 255=lots of railways
	UWORD buildings = 0;				// 0=no buildings, 255=lots of buildings
	UWORD terrainChance[NormalTerrainCount-NormalTerrain];

	UBYTE totalWeight = 0;

	for(int i = 0; i < (NormalTerrainCount - NormalTerrain); i++)
		terrainChance[i] = 0;

	for(i = 0; i < 9; i++)
	{
		TerrainConvert* tcPtr = &terrainConvert[parameters->terrains[i]];

		/*
		 * Special case for middle
		 */

		if(i == 4)
			defaultTerrain = tcPtr->defaultTerrain;

		static UBYTE weighting[9] = {
			1,2,1,
			2,8,2,
			1,2,1
		};

		UBYTE weight = weighting[i];
		totalWeight += weight;

	  	hillChance += tcPtr->hillChance * weight;
	  	riverChance += tcPtr->riverChance * weight;
	  	roadChance += tcPtr->roadChance * weight;
	  	railChance += tcPtr->railChance * weight;
	  	buildings += tcPtr->buildings * weight;

	  	for(int j = 0; j < (NormalTerrainCount - NormalTerrain); j++)
	  		terrainChance[j] += tcPtr->terrainChance[j] * weight;
	}

	// Could check for /0, but we know what weighting is!

	hillChance	/= totalWeight;
	riverChance /= totalWeight;
	roadChance  /= totalWeight;
	railChance  /= totalWeight;
	buildings   /= totalWeight;

	for(int j = 0; j < (NormalTerrainCount - NormalTerrain); j++)
		terrainChance[j]  /= totalWeight;

	/*
	 * Fill terrain with default terrain first
	 */

	g->clearTerrain(defaultTerrain, False, False);

	// TBW... Add other bits of terrain depending on chances and surrounding terrain

	i = 10;
	while(i--)
	{
	  	for(int j = 0; j < (NormalTerrainCount - NormalTerrain); j++)
		{
			if(terrainChance[j] && (game->gameRand.getB() <= terrainChance[j]))
			{
				MapGridCord x = game->gameRand.get(g->gridCount);
				MapGridCord y = game->gameRand.get(g->gridCount);
				UBYTE size = game->gameRand.get(terrainChance[j]) + 1;
				g->placeTerrain(j + NormalTerrain, x, y, size, False, False);
			}
		}
	}

#ifdef DEBUG_CREATE
	dumpGrid(g, "after Terrain Placing");
#endif

	/*
	 * Make Hills
	 */

	g->flatten(hillChance / 2);

#ifdef DEBUG_CREATE
	dumpGrid(g, "after flatten");
#endif

	if(hillChance)
		g->roughen(hillChance / 2);

#ifdef DEBUG_CREATE
	dumpGrid(g, "after roughen");
#endif

	int hillCount = hillChance;
	while(hillCount--)
	{
		int hx = game->gameRand(g->pointCount);	// Random location
		int hz = game->gameRand(g->pointCount);

		UBYTE ht = game->gameRand(hillChance) + hillChance / 4 + 1;		// Maximum height
		int radius = game->gameRand(g->pointCount/2) + g->pointCount/8;

		g->makeHill(hx, hz, ht, radius);

#ifdef DEBUG_CREATE
	dumpGrid(g, "after Hill");
#endif
	}

	/*
	 * Make river
	 */

	if(game->gameRand.getB() < riverChance)
	{
		g->makeRiver();
#ifdef DEBUG_CREATE
	dumpGrid(g, "after River");
#endif
	}

	/*
	 * Make Roads and railway lines
	 */

	if(game->gameRand.getB() < roadChance)
		g->makeRoad(0, game->gameRand(g->gridCount), g->gridCount, game->gameRand(g->gridCount), 20);	// Across
	if(game->gameRand.getB() < roadChance)
		g->makeRoad(0, game->gameRand(g->gridCount), g->gridCount, game->gameRand(g->gridCount), 20);	// Across
	if(game->gameRand.getB() < roadChance)
		g->makeRoad(game->gameRand(g->gridCount), 0, game->gameRand(g->gridCount), g->gridCount, 20);	// Top to bottom
	if(game->gameRand.getB() < roadChance)
		g->makeRoad(game->gameRand(g->gridCount), 0, game->gameRand(g->gridCount), g->gridCount, 20);	// Top to bottom

#ifdef DEBUG_CREATE
	dumpGrid(g, "after Roads");
#endif

	/*
	 * Make some random buildings
	 *
	 * Note: This does not check for buildings on top of each other!
	 * Or for buildings in rivers, etc
	 */

	int count = game->gameRand.get(buildings) + buildings / 2;
	while(count--)
	{
		BattleLocation ar = BattleLocation(game->gameRand(BattleMile(8)) - BattleMile(4), game->gameRand(BattleMile(8)) - BattleMile(4));

		TreeObject* tob = new TreeObject(StaticSpriteType(ChurchObject + game->gameRand(BuildingCount)), ar, game->gameRand(3)); 
		UBYTE obSize = staticObjectSizes[tob->what];

		if(obSize)
		{
			MapGridCord gx = grid->cord3DtoGrid(tob->where.x);
			MapGridCord gy = grid->cord3DtoGrid(tob->where.z);

			tob->where.x = grid->gridToCord3D(gx);
			tob->where.z = grid->gridToCord3D(gy);

			if(obSize & 1)
			{
				tob->where.x += g->gridSize / 2;
				tob->where.z += g->gridSize / 2;
			}

			g->placeTerrain(BuiltupTerrain, gx, gy, obSize, False, False);
		}

		miscObjects->add(tob);
	}

	/*
	 * Tidy it up and make other grids
	 */

	g->tidy();
#ifdef DEBUG_CREATE
	dumpGrid(g, "after Tidy");
#endif
	g->smooth();
#ifdef DEBUG_CREATE
	dumpGrid(g, "after Smooth1");
#endif
	g->smooth();
#ifdef DEBUG_CREATE
	dumpGrid(g, "after Smooth2");
#endif
	g->smooth();
#ifdef DEBUG_CREATE
	dumpGrid(g, "after Smooth3");
#endif

	grid->makeZoomedGrids(battle->data3D);

	g->tidy();

	// gameTime.set(Day(12), Month(April), Year(1861), Hour(10), Minute(0));
	timeInfo = gameTime;

	data3D.light.set(Degree(30), 0, intensityRange/2, intensityRange/8); 	// North, 30 degree

	data3D.view.set(0, 0, 0x0000, 0x1555, Zoom_8);
#ifdef DEBUG_CREATE
	dumpGrid(g, "Finish");
#endif
}



/*
 * Play a battle from the Campaign Game
 */

class CampaignBattleControl {
public:
	~CampaignBattleControl()
	{
		delete battle;
		battle = 0;
	}
};

int playCampaignBattle(OrderBattle* ob, MarkedBattle* batl)
{
	CampaignBattleControl control;
	battle = new BattleData(ob);

	BattleCreate createParameters;

	/*
	 * Fill create parameters with correct terrain types
	 */

	campaign->world->terrain.makeGrid(createParameters.terrains, batl->where, 3);

	/*
	 * Set up the battle field
	 */

	battle->gameTime = batl->when;
	battle->makeBattleField(&createParameters);
	battle->setTroops(ob, batl);
	int result = runBattle(batl, False);

	return result;
}

#ifdef DEBUG

static char* terrainStr[] = {
	"Unused0",
	"Sea",
	"Coastal",
	"River",
	"Stream",
	"Swamp",
	"Wooded",
	"Farmland",
	"Mountain",
	"Rough",
	"Impassable",
	"Unused11",
	"Unused12",
	"Unused13",
	"Unused14",
	"Unused15"
};

static MenuChoice terrainChoice[] = {
	MenuChoice("Sea",				CT_Sea),
	MenuChoice("Coastal",		CT_Coastal),
	MenuChoice("River",			CT_River),
	MenuChoice("Stream",			CT_Stream),
	MenuChoice("Swamp",			CT_Wooded),
	MenuChoice("Wooded",			CT_Farmland),
	MenuChoice("Farmland",		CT_Rough),
	MenuChoice("Mountain",		CT_Swamp),
	MenuChoice("Rough",			CT_Mountain),
	MenuChoice("Impassable",	CT_Impassable),
	MenuChoice("-", 				0 			),
	MenuChoice("Cancel", 		-1 		),
	MenuChoice(0,					0 			)
};

class GeneratedBattleControl {
public:
	MarkedBattle* bat;

	GeneratedBattleControl()
	{
		bat = 0;
	}

	~GeneratedBattleControl()
	{
		delete bat;
		delete battle;
		battle = 0;
	}
};


int playGeneratedBattle()
{
	int result = 0;

	int tType = menuSelect(0, terrainChoice);
	if(tType > 0)
	{
		GeneratedBattleControl control;
		battle = new BattleData(0);

		BattleCreate createParameters;

		for(int i = 0; i < 9; i++)
			createParameters.terrains[i] = tType;


		battle->makeBattleField(&createParameters);
		battle->makeNewBattle();

		MarkedBattle* bat = makeMarkedBattle(battle->ob);

		result = runBattle(bat, False);

#if 0
		delete bat;
		delete battle;
		battle = 0;
#endif
	}
	return result;
}

#endif

