/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Connection Setup and general routines
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/07/19  19:53:03  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/05/25  23:31:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/05/19  17:44:37  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include "connect.h"

#if !defined(TESTBATTLE)
#include "ipx.h"
#include "serial.h"
#endif

#include "game.h"
#include "dialogue.h"
#include "timer.h"

#include "language.h"
#include "strutil.h"
#include "memptr.h"
#include "filesel.h"

#if !defined(COMPUSA_DEMO)
#include "savegame.h"
#endif

#ifdef DEBUG
LogFile netLog("network.log");

const char* packetNames[] = {
	"HELLO",
	"HelloAck",
	"Acknowledge",
	"Sync",
	"Ping",
	"Go_MainMenu",
	"Disconnect",
	"Yes",
	"No",
	"Finish",
	"MM_Setup",
	"MM_GameType",
	"MM_Side",
	// "MM_Finish",
	// "MM_StartGame",
	// "MM_NoStartGame",
	"RL_Change",
	// "RL_Finish",
	// "RL_StartGame",
	// "RL_NoStartGame",
	// "CS_Finish",
	// "CS_ExitScreen",
	// "CS_NoExitScreen",
	"CS_EndDay",
	"CS_Order",
	"CS_SendFleet",
	"CS_Mobilise",
	"CS_AlertResult",

	"CS_TimeUpdate",

	// "BS_Finish",
	// "BS_ExitScreen",
	// "BS_NoExitScreen",
	"BS_Order",
	"BS_Ticks",

	"CAL_TransferUnit",
	"CAL_TransferGeneral",
	"CAL_MakeNew",
	"CAL_Rejoin",
	"CAL_Reattach",
	"CAL_ReattachAll",

	"WaitSave",
	"SaveGame",
	"LoadGame",
	"FileExists",
	"FileNotExist",

	"TEST",
};

static char buffer[20];

const char* packetStr(PacketType type)
{
	if(type < Connect_HowMany)
		return packetNames[type];
	else
	{
		sprintf(buffer, "??? %d ???", (int) type);
		return buffer;
	}
}

#endif

static Connection* makeConnection(Connection* connection)
{
	if(connection->connect() == 0)
	{
		return connection;
	}
	else
	{
		delete connection;
		return 0;
	}
}

#if !defined(TESTBATTLE)
#if !defined(COMPUSA_DEMO)
Connection* connectIPX()
{
	return makeConnection(new IPXConnection);
}

Connection* connectSerial()
{
	return makeConnection(new SerialConnection);
}

Connection* connectModem()
{
	return makeConnection(new ModemConnection);
}
#endif	// DEMO
#endif

MenuConnection::MenuConnection(char* id)
{
#ifdef DEBUG
	netLog.printf("New MenuConnection: %s", id);
#endif

	menuID = id;
	gotSync = 0;

	waitingForReply = False;
	remoteAnswer = False;
	disconnected = False;
	waitSave = False;

	// processMessages(True);
}
	
MenuConnection::~MenuConnection()
{
#ifdef DEBUG
	netLog.printf("Destructing MenuConnection: %s", menuID);
#endif

#if 0
	// waitSync();
	processMessages(True);

#ifdef DEBUG
	netLog.printf("Destructed MenuConnection: %s", menuID);
#endif
#endif
}



/*
 * Tell the user that remote is not responding
 * Return:
 *   False: Retry
 *   True: Abort
 */


Boolean askRemoteNotRespond()
{
	switch(dialAlert(0,
		language( lge_RemoteNotRespond ),
		language( lge_RetryQuit )))
	{
	default:
	case 0:		// Retry
		return False;

	case 1:		// Quit

		/*
		 * Terminate the connection
		 */

		// Don;t do this yet as this function called from within Connection::
		// game->makeSinglePlayer();		
		return True;
	}
}


void MenuConnection::processMessages(Boolean needSync)
{
#if defined(BATEDIT)
	game->connection->processMessages(this, needSync);
#else
	if(game->connection->processMessages(this, needSync) != 0)
	{
		// Remote is off line... sync was not received or packets not acknowledged
		game->makeSinglePlayer();
	}

	if(disconnected && !game->connection->isLocal())
	{
		game->connection->flush();		// wait for acknowledge
		game->makeSinglePlayer();
		dialAlert(0, language( lge_RemoteQuit ), language( lge_OK ) );
		disconnected = False;
	}
#endif
}

int Connection::processMessages(MenuConnection* menu, Boolean needSync)
{
	/*
	 * Bodge so that local connections don't wait for syncs that will never arrive!
	 */

	if(isLocal())
		needSync = False;

	if(needSync)
		sendData(Connect_Sync, (UBYTE*) copyString(menu->menuID), strlen(menu->menuID) + 1);

	/*
	 * Process incomming messages
	 *
	 * If we are the controller then we process our own messages first.
	 * This means messages get processed in same order on both machines
	 */

	if(inControl)
	{
		// Process Local Messages first

		Packet* packet;

		while( (packet = getLocalPacket()) != 0)
			menu->handlePacket(packet);

		// Process Remote Messages

		int val = procRemoteMessages(menu, needSync);
		if(val != 0)
			return val;
	}
	else
	{
		Packet* packet;

		// Process Remote Messages

		int val = procRemoteMessages(menu, needSync);
		if(val != 0)
			return val;

		// Process Local Messages

		while( (packet = getLocalPacket()) != 0)
			menu->handlePacket(packet);

	}

	/*
 	 * Make sure all our messages have been acknowledged
	 */

	return flush();
}


/*
 * Process Remote messages
 * Return 0 if OK
 * 		-1 if sync needed but didn't arrive
 */

int Connection::procRemoteMessages(MenuConnection* menu, Boolean needSync)
{
	Boolean finished = False;
	unsigned int timeOut = timer->getCount() + timer->ticksPerSecond * TimeOutSecs;

	while(!finished && (!needSync || !menu->gotSync))
	{
		Packet* packet;

		if( (packet = receiveData()) == 0)
		{
			if(!needSync)
				return 0;

			if(timer->getCount() >= timeOut)
			{
				finished = askRemoteNotRespond();
				timeOut = timer->getCount() + timer->ticksPerSecond * TimeOutSecs;
			}
		}
		else
			menu->handlePacket(packet);
	}

	if(!needSync || menu->gotSync)
	{
		if(needSync)
		{
			menu->gotSync--;
#ifdef DEBUG
			if(menu->gotSync != 0)
				netLog.printf("Warning: Sync is %d", menu->gotSync);
#endif
		}
		return 0;
	}
	else
		return -1;
}


void MenuConnection::handlePacket(Packet* packet)
{
#ifdef DEBUG
	netLog.printf("%s::handle%sPacket(%8s %4d)",
		menuID,
		(packet->sender == RID_Local) ? "Local" : "Remote",
		packetStr(packet->type),
		(int)packet->length);
#endif

	switch(packet->type)
	{
	case Connect_Sync:
		if(packet->sender != RID_Local)
		{
			if(strcmp(menuID, (char*) packet->data) == 0)
				gotSync++;
#ifdef DEBUG
			else
				netLog.printf("Wrong MenuConnection::id \"%s\" instead of \"%s\"",
					packet->data,
					menuID);
#endif
		}
		break;

	case Connect_Disconnect:
		if(packet->sender != RID_Local)
		{
			disconnected = True;
			waitingForReply = False;
			remoteAnswer = True;
			waitSave = False;
		}
		break;

	case Connect_Ping:
		break;

	case Connect_Yes:
		if(packet->sender != RID_Local)
		{
			waitingForReply = False;
			remoteAnswer = True;
		}
		break;

	case Connect_No:
		if(packet->sender != RID_Local)
		{
			waitingForReply = False;
			remoteAnswer = False;
		}
		break;

	case Connect_Finish:
		if(waitingForReply)
		{
			if(packet->sender != RID_Local)
			{
				waitingForReply = False;
				remoteAnswer = False;
			}
		}
		else
			goto passBack;
		break;

	case Connect_WaitSave:
#if !defined(COMPUSA_DEMO)
		if(packet->sender == RID_Local)
		{
			MemPtr<char> fileName(FILENAME_MAX);

			strcpy(fileName, getSaveGameWildCard());

			if(fileSelect(fileName, language(lge_save), False) == 0)
			{
				char* pData = copyString(fileName);

				game->connection->sendMessage(Connect_SaveGame, (UBYTE*)pData, strlen(pData) + 1);
				// saveGame(fileName);
			}
			else
				game->connection->sendMessage(Connect_SaveGame, 0, 0);
		}
		else
		{
			PopupText popup(language(lge_RemoteWaiting));
			waitSave = True;
			while(waitSave)	
			 	processMessages(False);
		}
#endif
		break;

	case Connect_SaveGame:
#if !defined(COMPUSA_DEMO)
		waitSave = False;
		if(packet->length)
			saveGame( (char*) packet->data);
#endif
		break;

	default:
	passBack:
		if(!waitingForReply)
			processPacket(packet);
		break;
	}

	delete packet;
}

void MenuConnection::processPacket(Packet* pkt)
{
#ifdef DEBUG
	throw GeneralError("No packet handler to process type %d", pkt->type);
#endif
}


/*
 * Ask if user wants to do what remote machine wants to do
 * send a Yes/No Answer
 * Return True if answered Yes
 */

Boolean MenuConnection::askRemoteReply(const char* text, const char* buttons)
{
	/*
	 * Force a sync so that any messages in progress are interpreted first
	 */

	processMessages(True);

	/*
	 * Get user reply...
	 */

	if(dialAlert(0, text, buttons) == 0)
	{
		game->connection->sendData(Connect_Yes, 0, 0);
		return True;
	}
	else
	{
		game->connection->sendData(Connect_No, 0, 0);
		return False;
	}
}

/*
 * Wait for remote computer to reply to a message
 */

Boolean MenuConnection::waitRemoteReply()
{
	if(game->connection->isLocal())
		return True;


#ifdef DEBUG
	netLog.printf("waitRemoteReply()");
#endif

	/*
	 * Force a sync so that any messages in progress are interpreted first
	 */

	processMessages(True);

	/*
	 * Get on with the job of waiting for a reply.
	 */

	waitingForReply = True;

	unsigned int timeOut = timer->getCount() + timer->ticksPerSecond * TimeOutSecs;

	while(waitingForReply)
	{
		if(timer->getCount() >= timeOut)
		{
			if(askRemoteNotRespond())
			{
				disconnected = True;
				waitingForReply = False;
				// game->makeSinglePlayer();
#ifdef DEBUG
				netLog.printf("Remote didn't reply");
#endif
				return True;
			}
			else
				timeOut = timer->getCount() + timer->ticksPerSecond * TimeOutSecs;
		}

		processMessages(False);
	}

#ifdef DEBUG
	netLog.printf("waitRemoteReply()");
#endif
	return remoteAnswer;

}

/*=======================================================
 * Receipt Lists
 */

/*
 * Data needed for acknowledgements
 */

ReceiptList::ReceiptList()
{
	entry = 0;
	last = 0;
}

ReceiptList::~ReceiptList()
{
	clear();
}

void ReceiptList::clear()
{
	while(entry)
	{
		Receipt* next = entry->next;
		delete entry->data;
		delete entry;
		entry = next;
	}
	last = 0;
}

void ReceiptList::add(ULONG serial, PacketType type, UWORD length, UBYTE* data)
{
	Receipt* receipt = new Receipt;

	receipt->serial = serial;
	receipt->type = type;
	receipt->length = length;
	receipt->data = data;
	receipt->retries = MaxRetries;
	receipt->time = timer->getCount() + timer->ticksPerSecond / 2;	// Half a second
	receipt->next = 0;
	if(last)
		last->next = receipt;
	last = receipt;
	if(entry == 0)
		entry = receipt;
}

void ReceiptList::remove(ULONG serial)
{
	Receipt* prev = 0;
	Receipt* receipt = entry;
	while(receipt)
	{
		if(receipt->serial == serial)
		{
			if(prev)
				prev->next = receipt->next;
			else
				entry = receipt->next;
			if(receipt == last)
				last = prev;

			delete receipt->data;
			delete receipt;

			return;
		}

		prev = receipt;
		receipt = receipt->next;
	}
}



Connection::Connection()
{
	inControl = False;
	connected = False;
	serial = 0;
	rxSerial = 0;
}

Connection::~Connection()
{
	packetList.clearAndDestroy();
	localPackets.clearAndDestroy();
}

int Connection::sendMessage(PacketType type, UBYTE* data, size_t length)
{
	/*
	 * Add it to our local packet list
	 */

	Packet* packet = new Packet;
	packet->type = type;
	packet->length = length;
	packet->sender = RID_Local;
	if(length)
		memcpy(packet->data, data, length);
	localPackets.append(packet);

	return sendData(type, data, length);
}

int Connection::sendData(PacketType type, UBYTE* data, size_t length)
{
#ifdef DEBUG
	if(type != Connect_Sync)
		netLog.printf("Tx: %8s %4d",
			packetStr(type),
			(int)length);
#endif

	// checkAcks();

	/*
	 * Force previous packet to have been sent and acknowledged
	 */

	if(flush())
		return -1;

	/*
	 * Send it out to remote computer
	 */

	receiptList.add(serial, type, length, data);
	return sendRawData(type, data, length, True, serial++);
}


/*
 * Force all outgoing packets to be sent
 *
 * Return -1 if a receipt had too many retries
 */

int Connection::checkReceipts()
{
	processIncomming();

	Receipt* receipt = receiptList.entry;
	while(receipt)
	{
#ifdef DEBUG1
		netLog.printf("Waiting for receipt for %ld", receipt->serial);
#endif

		if(timer->getCount() >= receipt->time)
		{
			if(receipt->retries--)
			{
#ifdef DEBUG
				netLog.printf("Resending packet: %ld, %s",
					receipt->serial,
					packetStr(receipt->type));
#endif
				sendRawData(receipt->type, receipt->data, receipt->length, True, receipt->serial);
				receipt->time = timer->getCount() + timer->ticksPerSecond / 2;
				receipt = receiptList.entry;
			}
			else
			{
				receipt->retries = MaxRetries;
				return -1;
			}
		}
		else
			receipt = receipt->next;
	}

	return 0;
}

int Connection::flush()
{
	unsigned int timeOut = timer->getCount() + timer->ticksPerSecond * TimeOutSecs;

	while(receiptList.entry)
	{
#ifdef DEBUG1
		netLog.printf("Flushing for %ld", receiptList.entry->serial);
#endif

		if(checkReceipts() || (timer->getCount() > timeOut))
		{
#ifdef DEBUG
			netLog.printf("Flush timed out");
#endif
			if(askRemoteNotRespond())
				return -1;
			else
				timeOut = timer->getCount() + timer->ticksPerSecond * TimeOutSecs;
		}
	}
	return 0;
}

/*
 * Null Connection Implementation
 */


NullConnection::NullConnection()
{
}

NullConnection::~NullConnection()
{
}

int NullConnection::connect()
{
	return 0;
}

void NullConnection::disconnect()
{
}

int NullConnection::sendRawData(PacketType type, UBYTE* data, size_t length, Boolean receipt, ULONG serial)
{
	receiptList.clear();
	return 0;
}

Packet* NullConnection::receiveData()
{
	return 0;
}

void NullConnection::processIncomming()
{
	receiptList.clear();
}
