/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Movement
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.7  1994/08/09  15:42:15  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/07/25  20:32:53  Steven_Green
 * ..
 *
 * Revision 1.5  1994/07/19  19:53:03  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/07/13  13:50:17  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/07/11  14:26:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/06/29  21:38:52  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/06/24  14:43:30  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdlib.h>
#include "campmove.h"
#include "campcomb.h"
#include "campwld.h"
#include "campaign.h"
#include "camptab.h"
#include "generals.h"
#include "queue.h"
#include "error.h"
#include "campunit.h"
#include "realism.h"
#include "tables.h"
#include "game.h"
#include "route.h"

#ifdef DEBUG
#include "msgwind.h"
#include "options.h"
#endif


/*
 * Process a days movement
 *
 * Rail network
 * Water network
 * Troops
 */

void CampaignWorld::processDay()
{

	mobList.process();


	facilities.dailyProcess();

	/*
	 * Clear all the inBattle flags and do any other daily processing
	 */

	ob.daily();

	// ob.setVisibility();		// Done in daily!

	waterNet.searchForBombardable();
}

/*
 * Process an hour of the day
 *
 * This is not neccesarily an hour... in fact I've reduced it to
 * 10 minutes so that units move less than 1 terrain grid in a
 * movement phase.
 */

void CampaignWorld::processHour(const TimeBase& gameTime, GameTicks ticks)
{
	Boolean checkBattles = False;

	if(!lastBattleCheckTime)
		checkBattles = True;
	else
	{
		GameTicks elapsed = gameTime - lastBattleCheckTime;
		if(elapsed > GameTicksPerHour)
			checkBattles = True;
	}

	if(checkBattles)
		lastBattleCheckTime = gameTime;

	/*
	 * Do the transport networks
	 */

	railNet.process(gameTime);
	waterNet.process(gameTime);

	/*
	 * Move all the units!
	 */

	ob.moveCampaignUnits(gameTime, ticks);

	if(checkBattles)
	{
		
		/*
		 * Check all (moved) brigades against enemy units
		 */

		for(Unit* p = ob.getSides(); p; p = p->sister)
			for(Unit* a = p->child; a; a = a->sister)
				for(Unit* c = a->child; c; c = c->sister)
					for(Unit* d = c->child; d; d = d->sister)
						for(Brigade* b = (Brigade*)d->child; b; b = (Brigade*)b->sister)
						{
							if(!b->inBattle && !b->resting)
								checkUnitBattles(b);
						}


		/*
	 	 * Check for battles
	 	 *
	 	 * 1. Check all (moved) BRIGADES against facilities
	 	 */

		for(p = ob.getSides(); p; p = p->sister)
			for(Unit* a = p->child; a; a = a->sister)
				for(Unit* c = a->child; c; c = c->sister)
					for(Unit* d = c->child; d; d = d->sister)
						for(Brigade* b = (Brigade*)d->child; b; b = (Brigade*)b->sister)
						{
							if(!b->inBattle && !b->resting)
								checkCityBattles(b);
							b->moved = False;
						}
		ob.tidy();
	}
}


/*
 * Check any orders in transit
 */

void Unit::processOrderQueue(const TimeBase& timeNow)
{
	if(orderList.entries())
	{
		SentOrder* order;
		while( (order = orderList.find()) != 0)
		{
#ifdef DEBUG
			cLog.printf("Considering Order for %s (%s)",
				getFullName(),
				getOrderStr(*order));
#endif

			if(order->time <= timeNow)
			{
#ifdef DEBUG
				cLog.printf("Processing Order");
#endif
				/*
				 * Set the units order to the new order
				 */

				realOrders = *order;
				setCampaignAttachMode(Detached);
				makeCampaignDetached(True);

				/*
				 * Make sure orders valid...
				 */

				/*
				 * If move by rail/water and they don't have an
				 * advance order, then make it an advance order
				 */

				if(realOrders.how != ORDER_Land)
				{
					if(realOrders.basicOrder() != ORDERMODE_Advance)
						realOrders.mode = ORDER_Advance;
				}

				/*
				 * If hold order, then stop where they are
				 */

				else if(realOrders.basicOrder() == ORDERMODE_Hold)
					realOrders.destination = location;

				newOrder = True;

				/*
				 * And remove it from the list and from memory!
				 */

				delete orderList.get();
			}
			else
			{
#ifdef DEBUG
				cLog.printf("Order not delivered yet");
#endif
				break;		// Get out, because orders must be in time order
			}
		}
	}
}

/*
 * Move all Units in Campaign
 *
 * Stick with the old method of using a stack to emulate recursion
 */


/*
 * Process all the units one by one
 * Processing is done recursively, such that all of a units children
 * are processed before the unit itself.
 *
 * A stack is used to allow the function to be iterative instead of
 * recursive.
 */

/*
 * Structure saved in stack
 */

enum LeftRight {
	Left,
	Right
};

enum CampaignFormation {
	Parallel,
	Line
};

struct MoveStackItem {
	Unit* unit;			// Unit being processed now
	Unit* left;
	Unit* right;
	LeftRight edge;
	CampaignFormation formation;
	Location newDestination;
	Location parentDestination;
};

#define MoveStackSize 5

void OrderBattle::moveCampaignUnits(const TimeBase& gameTime, GameTicks ticks)
{
	Stack<MoveStackItem> stack(MoveStackSize);

	MoveStackItem item;

	/*
	 * Initialise to the 1st president
	 */

	item.unit = getSides();
	item.left = 0;
	item.right = 0;
	item.edge = Left;
	item.formation = Parallel;
	item.newDestination = Location(0,0);
	item.parentDestination = Location(0,0);

	do
	{
		item.newDestination = item.unit->realOrders.destination;

		item.unit->moveCampaignBeforeChildren(ticks, item);

		// Recurse into children unless a brigade

		if( (item.unit->getRank() != Rank_Brigade) &&
			 (item.unit->childCount) )
		{
			stack.put(item);
	
			item.unit = item.unit->child;
			item.left = 0;
			item.right = 0;
			item.edge = Left;
			item.formation = (item.unit->getCampaignAttachMode() != Attached) ? Parallel : Line;
			item.parentDestination = item.newDestination;
			continue;
		}

		/*
		 * Advance to next unit
		 */

		while(!item.unit->sister && !stack.isEmpty())
		{
			stack.get(item);

			item.unit->moveCampaignAfterChildren(ticks, item);
		}

		/*
		 * If stack was empty, then we must be at a president
		 * and so we can just get its sister, which will be 0
		 * if we've finished
		 */
		
		item.unit = item.unit->sister;

	} while(item.unit);

#ifdef DEBUG
	routeLog.close();
#endif
}

/*
 * Preprocess Unit
 *
 * Update orders if new ones arrived
 */

void Unit::moveCampaignBeforeChildren(GameTicks ticks, MoveStackItem& info)
{
	if(info.unit->getRank() != Rank_President)
	{
		info.unit->processOrderQueue(campaign->gameTime);

		/*
		 * Copy orders from commander
		 */

		ASSERT(superior != 0);

		if(getCampaignAttachMode() == Attached)
			realOrders = superior->realOrders;

		// if(!info.unit->inBattle)
		if(!inBattle)
		{
			// info.unit->moveCampaign(ticks, info);

			/*
	 		 * If moving by rail or water then handle seperately
	 		 */

			if( (moveMode == MoveMode_Marching) || (moveMode == MoveMode_Standing) )
			{
				/*---------------------------------------------------------
		 		 * If we're trying to rejoin command, see if we are close enough
				 * (Used to be done after movement, but that caused problems
				 *  because newly joining units (e.g brigade head of rest)
				 *  caused other units to think they had arrived at their
				 *  destination).
		 		 */

				if(getCampaignAttachMode() == Joining)
				{
					Distance d = distance(location.x - superior->location.x,
										 		location.y - superior->location.y);

					if(d <= campaignCommandRange[superior->rank])
					{
						setCampaignAttachMode(Attached);
						makeCampaignDetached(False);

						/*
				 		 * Bodge code added... SWG 1st June 1995
				 		 * Force Marching orders whilst joining
				 		 * and get commanders orders when not.
				 		 */

						realOrders = superior->realOrders;
						givenOrders = superior->realOrders;


#ifdef DEBUG
						campaign->msgArea->printf("%s has rejoined command", getName(True));
						cLog.printf("%s has rejoined command", getFullName());
#endif
					}

					/*
			 		 * Bodge code added... SWG 1st June 1995
			 		 * Force Marching orders whilst joining
			 		 * and get commanders orders when not.
			 		 */

					else
					{
						if(superior->realOrders.how == ORDER_Land)
						{
							if(realOrders.how == ORDER_Land)
							{
								realOrders = Order(ORDER_Advance, ORDER_Land, superior->location);
								campaign->world->getBestRoute(location, realOrders.destination, realOrders, getSide());
							}
						}
						else
						{
							if(realOrders.how == ORDER_Land)
							{
								realOrders = Order(ORDER_Advance, ORDER_Land, superior->realOrders.destination);
								campaign->world->getBestRoute(location, realOrders.destination, realOrders, getSide());
							}
						}
					}
				}

				/*
		 		 * See if out of command range
		 		 */

				else if(getCampaignAttachMode() == Attached)
				{
					Distance d = distance(location.x - superior->location.x,
										 		location.y - superior->location.y);

					if(d > campaignCommandRange[superior->rank])
						setCampaignAttachMode(Joining);
				}

				/*==================================================
				 * Step 1:
				 *		Determine a destination to move to.
				 */

				// Location newDestination = location;

				/*
	 	 		 * See if current unit is independant
	 	 		 *
	 	 		 * Independant unit's try to move their ordered click point
	 	 		 */

			 if( (getCampaignAttachMode() == Joining) ||
			 	  (realOrders.basicOrder() != ORDERMODE_Hold) )
			 {
				if(getCampaignAttachMode() != Attached)
				{
					/*
		 	 		 * Interpret Orders!
		 	 		 */

					if(realOrders.how != ORDER_Land)
					{
						Facility* f = campaign->world->facilities.get(realOrders.onFacility);

						info.newDestination = f->location;
					}
					else
					{
						/*
				 		 * If trying to rejoin command then set commander's location
				 		 * as destination!
				 		 *
				 		 * Otherwise go to ordered clickpoint
				 		 */

						if(getCampaignAttachMode() == Joining)
						{
							if(superior->realOrders.how == ORDER_Land)
								info.newDestination = superior->location;
							else
								// info.newDestination = superior->realOrders.destination;
								info.newDestination = info.parentDestination;
						}
						else
							info.newDestination = realOrders.destination;
					}

					if(occupying)
					{
						Distance d = distanceLocation(info.newDestination, occupying->location);
						if(d >= cityWidth)
							actOnCity(occupying, this, SIEGE_None);
					}
				}

				/*
	 	 		 * Controlled unit's try to get into position
	 	 		 */

				else		// Following leader
				{
					followLeader(info);
				}
		
				/*------------------------------------------------------------
				 * Step 2:
		 		 *   If its a brigade then really move it!
				 *   Otherwise determine if arrived at destination
		 		 */

				if(rank == Rank_Brigade)
				{
					Brigade* b = (Brigade*)this;

					b->reallyMoveCampaign(info.newDestination, ticks);
				}
				else	// Not brigade... set moveMode flag
				{
					/*
			 		 * Scan each non-independant child
			 		 * If any is not standing, then we are marching
			 		 *
			 		 * If this is the 1st time we've been called since a new order
			 		 * was given, then force us into marching mode to allow
			 		 * things to start moving again!
			 		 */

					if(newOrder)
					{
						moveMode = MoveMode_Marching;
						newOrder = False;

						Unit* ch = child;
						while(ch)
						{
							if(ch->getCampaignAttachMode() == Attached)
								ch->newOrder = True;
							ch = ch->sister;
						}
					}
					else
					{
						moveMode = MoveMode_Standing;
						Unit* ch = child;
						while(ch)
						{
							if(ch->getCampaignAttachMode() == Attached)
							{
								if(ch->moveMode == MoveMode_Marching)
								{
									moveMode = MoveMode_Marching;
									break;
								}
							}

							ch = ch->sister;
						}
					}
				}
			 }		// (NOT holding) or joining


				/*==============================================================
				 * What to do if arrived at destination
				 *
		 		 * If we are moving to a railway or boat and we are standing then we
		 		 * must have arrived, so add to the platform queue!
		 		 */

				if((getCampaignAttachMode() != Attached) && (moveMode == MoveMode_Standing))
				{
					/*
			 		 * Make sure they are not occupying town!
			 		 */

					if(realOrders.how == ORDER_Rail)
					{
						if(occupying)
							actOnCity(occupying, this, SIEGE_None);

						campaign->world->railNet.add(this, realOrders.onFacility, realOrders.offFacility);
					}
					else if(realOrders.how == ORDER_Water)
					{
						if(occupying)
							actOnCity(occupying, this, SIEGE_None);

						campaign->world->waterNet.makeFerry(this, realOrders.onFacility, realOrders.offFacility);
					}
					else
					{
						if(realOrders.how == ORDER_Land)
							setOrder(ORDER_Stand, realOrders.destination);
					}
				}
			}	// moveMode marching or standing
		}	// inBattle
	}	// !President
}

/*
 * Move Unit after its children have been processed
 *
 * Note: This is only called for Army/Corps/Division (NOT Brigade).
 *
 * It is really moving the general, rather than a unit.
 */

void Unit::moveCampaignAfterChildren(GameTicks ticks, MoveStackItem& info)
{
	// if(info.unit->getRank() != Rank_President)
	if(getRank() != Rank_President)
	{
		/*
		 * Get mid point of its children
		 */

		// if(info.unit->child && !info.unit->inBattle)
		if(child && !inBattle)
		{
			Location mid = Location(0,0);
			// Unit* child = info.unit->child;
			Unit* chl = child;
			int count = 0;
			while(chl)
			{
				if(chl->getCampaignAttachMode() == Attached)
				{
					mid += chl->location;
					count++;
				}
				chl = chl->sister;
			}

			if(count)
				mid /= count;
			else
			{
#if 0
				if(info.unit->child)
					mid = info.unit->child->location;
				else
					mid = info.unit->location;
#else
				if(child)
					mid = child->location;
				else
					mid = location;
#endif
			}
		
			// Speed s = campaign->world->getTerrainEffect( commanderMarchSpeed, info.unit->location );
			Speed s = campaign->world->getTerrainEffect( commanderMarchSpeed, location);

			/*
		 	 * Bodge it to stop them coming to a complete standstill
		 	 */

			const Speed MinSpeed = MilesPerHour(1) / 2;

			if(s < MinSpeed)
				s = MinSpeed;

			// info.unit->reallyMove(mid, s, ticks);
			reallyMove(mid, s, ticks);

			// moveLocation(info.unit->location, mid, s, ticks);
		}
	}
}


/*
 * Really Move a brigade on the campaign
 */

void Brigade::reallyMoveCampaign(const Location& newDestination, GameTicks ticks)
{
	UnitInfo inf(True, True);
	getInfo(inf);

#if 0		// Let battle checker clear this instead
	moved = False;		// This is set when moved
#endif

	/*===========================================================
	 * Update Resting values
	 *
	 * See if tired!
	 */

	Strength total = inf.strength + inf.stragglers;

	int stragRatio;

	ASSERT(total != 0);		// Shouldn't be here if dead!

	if(total)
		stragRatio = (inf.strength * 100) / total;
	else
		return;		// Unit shouldn't be moving as it is dead!

	/*
	 * Rest with hysteresis:
	 *
	 * Start resting if:
	 *		less than 5% of units are not straggling
	 *        OR
	 *    fatigue > 80%
	 * Stop resting when:
	 *		more than 50% of units are not straggling
	 *			 AND
	 *		fatigue < 50%
	 */


	if(resting)
	{
		if( (stragRatio > 50) && (inf.fatigue < (MaxAttribute / 2)))
		{
			resting = False;
#ifdef DEBUG
			cLog.printf("%s has stopped resting", getFullName());
#endif
		}
	}
	else
	{
		if( (stragRatio < 5) || (inf.fatigue > ((MaxAttribute * 80) / 100)) )
		{
			resting = True;
#ifdef DEBUG
			cLog.printf("%s has started resting", getFullName());
#endif
		}
	}


	/*===========================================================
	 * Move to new destination
	 */

	if(!resting && !holdBack && (location != newDestination))
	{
		Location diff = newDestination - location;

		facing = direction(diff.x, diff.y);

		/*
		 * Adjust speed for terrain
		 */

		Speed s = campaign->world->getTerrainEffect( inf.speed, location );

		/*
		 * Adjust speed for stragglers, fatigue and order.
		 */

		s = (s * orderSpeedEffect[realOrders.mode]) / 100;

		/*
		 * Bodge it to stop them coming to a complete standstill
		 */

		const Speed MinSpeed = MilesPerHour(1) / 2;

		if(s < MinSpeed)
			s = MinSpeed;

		reallyMove(newDestination, s, ticks);
		moved = True;
		movedTime += ticks;

		/*
		 * Check to see if wandered into any battle areas
		 */

		campaign->world->battles.checkUnitNearBattle(this);

	}

	if(location == newDestination)
		moveMode = MoveMode_Standing;
	else
		moveMode = MoveMode_Marching;

	holdBack =  False;
}

/*
 * Follow leader... Fill in newDestination
 */

void Unit::followLeader(MoveStackItem& info)
{
	/*
	 * If not independant then shouldn't be occupying town!
	 */

	// ASSERT(occupying == 0);

	if(occupying)
		setOccupy(0, SIEGE_None, True);

	Unit* parent = superior;

	ASSERT(parent != 0);

	realOrders = parent->realOrders;

	/*
	 * It is the 1st unit, so  go to the parent's destination
	 */

	if(info.right == 0)
	{
		info.right = this;
		info.left  = this;

		// info.newDestination = parent->nextDestination;
		// info.newDestination = parent->realOrders.destination;
		info.newDestination = info.parentDestination;
	}

	else
	{
		Boolean gotPosition = False;

		Unit* checkUnit = (info.edge == Left) ? info.left : info.right;

		ASSERT(checkUnit != 0);

		/*
		 * If the person we're following is too far away then ask them to
		 * wait.
		 */

		if(getRank() == Rank_Brigade)
		{
			ASSERT(checkUnit->getRank() == Rank_Brigade);

			Distance d = distanceLocation(location, checkUnit->location);

			if(d > (brigadeFrontSpacing * 2))
			{
				Brigade* checkBrigade = (Brigade*) checkUnit;

				checkBrigade->holdBack = True;
#ifdef DEBUG
				cLog.printf("%s asking %s to hold back",
					getFullName(),
					checkUnit->getFullName());
#endif
			}
		}


		/*
		 * If in parallel try to get a position to the side of apropriate
		 * unit.
		 */

		if(info.formation == Parallel)
		{
			/*
			 * Find check's 1st controlled brigade
			 */

			Unit* brig = checkUnit;

			while(brig && (brig->rank < Rank_Brigade))
			{
				brig = brig->child;

				while(brig && (brig->getCampaignAttachMode() != Attached))
					brig = brig->sister;
			}

			/*
			 * Get a point to the brigade's side
			 */

			if(brig)
			{
				Wangle dir = brig->facing;
				if(info.edge == Right)
					dir += 0x4000;
				else
					dir -= 0x4000;

				Location where = brig->location +
					Location(mcos(brigadeSideSpacing,dir), msin(brigadeSideSpacing,dir));

				/*
				 * Should now check that terrain is suitable!
				 */

				info.newDestination = where;
				gotPosition = True;
			}

		}

		/*
		 * Now try to get behind unit instead
		 */

		if(!gotPosition)
		{
			/*
			 * Find last controlled brigade
			 */

			Unit* brig = checkUnit;

			while(brig && (brig->rank < Rank_Brigade))
			{
				brig = brig->child;

				Unit* lastBrig = brig;

				while(lastBrig->sister)
				{
					lastBrig = lastBrig->sister;
					// if(!lastBrig->isCampaignReallyIndependant())
					if(lastBrig->getCampaignAttachMode() == Attached)
						brig = lastBrig;
				}
			}

			/*
			 * Get point behind brigade
			 * This distance should really be dependant on the
			 * type and number of men in each of the brigade's regiment!
			 */

			if(brig)
			{
				Wangle dir = brig->facing;
				dir += 0x8000;
				Location where = brig->location +
					Location(mcos(brigadeFrontSpacing,dir), msin(brigadeFrontSpacing,dir));

				/*
				 * Should now check that terrain is suitable!
				 */

				info.newDestination = where;
				gotPosition = True;
			}
		}

		/*
		 * Update left/right
		 */

		if(info.edge == Right)
		{
			info.right = this;
			info.edge = Left;
		}
		else
		{
			info.left = this;
			info.edge = Right;
		}
	}
}

/*
 *	PseudoCode:
 *		Convert nextDestination to Terrain Square coordinates
 *		Convert newDestination to Terrain Square coordinates
 *    IF needLocalDestination OR terrain square coordinates are different
 *       Calculate a new localDestination using route planner...
 *       Clear needLocalDestination flag
 *    Get current Terrain square coordinates
 *    Move unit's location towards localDestination
 *    If terrain square is different than before moving
 *       set needLocalDestination flag.
 *
 * To be considered:
 *    Route planner includes rail/water
 *    Need to have a local OrderHow flag
 *    Also update orders to have a ORDER_Fastest (as well as Land, Rail, Water).
 */

void Unit::reallyMove(const Location& newDestination, Speed s, GameTicks ticks)
{
	if(location == newDestination)
		return;

	/*
	 * Get Campaign Terrain in a handy place
	 */

	CampaignTerrain& cTerrain = campaign->world->terrain;

	/*
	 * Convert next and new Destinations to terrain grid coordinates
	 */

	CampaignTerrainCoord oldTCord;
	CampaignTerrainCoord newTCord;
	CampaignTerrainCoord currentTCord;

	cTerrain.locationToCampTCord(oldTCord, nextDestination);
	cTerrain.locationToCampTCord(newTCord, newDestination);
	cTerrain.locationToCampTCord(currentTCord, location);

	/*
	 * Set the new Destination
	 */

	nextDestination = newDestination;

	/*
	 * If trying to move to same grid, then just move it!
	 */

	if(currentTCord == newTCord)
	{
		localDestination = newDestination;
		needLocalDestination = False;
	}
	else
	{
		/*
		 * Get a new localDestination if needed
		 * Note that this involves calling the route planner
		 * so should be avoided unless a new route is likely
		 */

		/*
		 * Only call if oldTCord and newTCord are further than
		 * 4 squares (about 10 miles).
		 */

		UWORD dx = abs(oldTCord.x - newTCord.x);
		if(dx >= 4)
			needLocalDestination = True;
		else
		{
			dx = abs(oldTCord.x - newTCord.x);
			if(dx >= 4)
				needLocalDestination = True;
		}

		// if(needLocalDestination || (oldTCord != newTCord))
		if(needLocalDestination)
		{
#ifdef DEBUG
			routeLog.printf("-----");
			routeLog.printf("Finding route for %s", getFullName());
#endif

			RoutePlan route;
	
			route.planRoute(location, nextDestination, realOrders.how, getSide());

			localDestination = route.local;
			moveMethod = realOrders.how;

			needLocalDestination = False;

#ifdef DEBUG
			routeLog.printf("");
#endif
		}
	}

#ifdef DEBUG
	if(quickMove)
		location = localDestination;
	else
		moveLocation(location, localDestination, s, ticks);
#else
	moveLocation(location, localDestination, s, ticks);
#endif

	cTerrain.locationToCampTCord(currentTCord, localDestination);
	cTerrain.locationToCampTCord(newTCord, location);

	if(currentTCord == newTCord)
		needLocalDestination = True;
}


