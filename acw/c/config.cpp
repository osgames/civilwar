/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Configuration File Manager
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */


#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include "config.h"
#include "memptr.h"
#include "strutil.h"

ConfigFile::ConfigFile(const char* name)
{
	fp = 0;
	fileName = name;
}

ConfigFile::~ConfigFile()
{
	if(fp)
	{
		fclose(fp);
		fp = 0;
	}
}

const int ConfigFile::MaxLineLength = 100;

/*
 * Look at a buffer containing text such as:
 *   SomeString=SomeValue
 *
 * return the 1st character after the =
 * replace the = with 0
 *
 * Thus the caller can use:
 *		buffer : ID
 *    return : Value
 */

char* parseLine(char* buffer)
{
	/*
	 * Remove trailing white space, line feeds, etc
	 */

	char* s = buffer + strlen(buffer) - 1;
	while(isspace(*s))
		*s-- = 0;

	/*
	 * Split line up at '='
	 */

	s = strchr(buffer, '=');
	if(s)
		*s++ = 0;
	else
		s = buffer + strlen(buffer);

	return s;
}

void writeSetting(FILE* fp, const char* id, const char* setting)
{
	fputs(id, fp);
	if(setting && setting[0])
		fprintf(fp, "=%s", setting);
	fprintf(fp, "\n");
}

/*
 * Find a configuration variable
 *
 * Note that the return value muse be deleted after use.
 */

char* ConfigFile::get(const char* id)
{
	if(!fp)
		fp = fopen(fileName, "r");

	if(!fp)
		return 0;

	rewind(fp);

	MemPtr<char> buffer(MaxLineLength);

	while(!feof(fp))
	{
		if(fgets(buffer, MaxLineLength-1, fp))
		{
			buffer[MaxLineLength-1] = 0;

			if(buffer[0] && (buffer[0] != ';'))
			{
				char* s = parseLine(buffer);

				if(stricmp(buffer, id) == 0)
					return copyString(s);
			}
		}
	}

	return 0;
}

/*
 * Set a configuration variable
 */

void ConfigFile::set(const char* id, const char* setting)
{
	static const char tempFileName[] = "config.tmp";

	if(!fp)
		fp = fopen(fileName, "r");

	rewind(fp);

	Boolean written = False;

 	FILE* fpOut = fopen(tempFileName, "w");
	if(fpOut)
	{
		if(fp)
		{
			MemPtr<char> buffer(MaxLineLength);

			while(!feof(fp))
			{
				if(fgets(buffer, MaxLineLength-1, fp))
				{
					buffer[MaxLineLength-1] = 0;

					if(buffer[0] && (buffer[0] != ';'))
					{
						char* s = parseLine(buffer);

						if(stricmp(buffer, id) == 0)
						{
							writeSetting(fpOut, id, setting);
							written = True;
						}
						else
							writeSetting(fpOut, buffer, s);
					}
					else
						fputs(buffer, fpOut);
				}
			}

			fclose(fp);
			fp = 0;
		}
		else
		{
			fprintf(fpOut, "; Configuration File for \"The Civil War\"\n\n");
		}

		if(!written)
			writeSetting(fpOut, id, setting);

		if(fclose(fpOut) == 0)
		{
			remove(fileName);
			rename(tempFileName, fileName);
		}
		else
			remove(tempFileName);
	}

}

/*
 * Get a boolean config value
 */

Boolean ConfigFile::getBool(const char* id, Boolean defVal)
{
	char* s = get(id);
	if(s)
	{
		if(stricmp(s, "ON") == 0)
			defVal = True;
		else if(stricmp(s, "YES") == 0)
			defVal = True;
		else if(stricmp(s, "1") == 0)
			defVal = True;
		else if(stricmp(s, "OFF") == 0)
			defVal = False;
		else if(stricmp(s, "NO") == 0)
			defVal = False;
		else if(stricmp(s, "0") == 0)
			defVal = False;
		delete[] s;
	}

	return defVal;
}

void ConfigFile::setBool(const char* id, Boolean flag)
{
	set(id, flag ? "On" : "Off");
}

void ConfigFile::setNum(const char* id, int n)
{
	char buffer[20];		// Long enough for integer in decimal (10 digits)

	sprintf(buffer, "%d", n);

	set(id, buffer);
}

Boolean ConfigFile::getNum(const char* id, int& n)
{
	Boolean result = False;

	char* s = get(id);
	if(s)
	{
		if(isdigit(*s))
		{
			n = atoi(s);
			result = True;
		}
		delete[] s;
	}
	return result;
}

int ConfigFile::getEnum(const char* id, const char** enums, int defVal)
{
	char* s = get(id);

	if(s)
	{
		for(int v = 0; enums[v]; v++)
		{
			if(stricmp(enums[v], s) == 0)
			{
				defVal = v;
				break;
			}
		}
	}

	return defVal;
}

void ConfigFile::setEnum(const char* id, const char** enums, int val)
{
	set(id, enums[val]);
}

