/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Random Number Tester
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include "random.h"

void main()
{
	RandomNumber r(0x12345678);

	ULONG count[256];		// How many times has each number been picked?
	ULONG deltas[256];	// What was difference between consecutive numbers?
	ULONG dDeltas[256];	// Differences between deltas

	for(int i = 0; i < 256; i++)
	{
		count[i] = 0;
		deltas[i] = 0;
		dDeltas[i] = 0;
	}

	UBYTE last = r.getB();
	UBYTE lastDelta = 0;

	i = 100000;

	while(i--)
	{
		UBYTE n = r.getB();

		count[n]++;

		UBYTE change = n - last;
		deltas[change]++;
		last = n;

		UBYTE deltaChange = change - lastDelta;
		dDeltas[deltaChange]++;
		lastDelta = change;
	}

	ULONG minCount = count[0];
	ULONG maxCount = count[0];

	ULONG minDelta = deltas[0];
	ULONG maxDelta = deltas[0];

	ULONG mindDelta = dDeltas[0];
	ULONG maxdDelta = dDeltas[0];

	for(i = 0; i < 256; i++)
	{
		printf("%3d %6ld %6ld %6ld\n", i, count[i], deltas[i], dDeltas[i]);

		if(count[i] < minCount)
			minCount = count[i];
		if(count[i] > maxCount)
			maxCount = count[i];
		if(deltas[i] < minDelta)
			minDelta = deltas[i];
		if(deltas[i] > maxDelta)
			maxDelta = deltas[i];
		if(dDeltas[i] < mindDelta)
			mindDelta = dDeltas[i];
		if(dDeltas[i] > maxdDelta)
			maxdDelta = dDeltas[i];
	}

	printf("\nminCount = %ld, maxCount = %ld\n", minCount, maxCount);
	printf("\nminDelta = %ld, maxDelta = %ld\n", minDelta, maxDelta);
	printf("\nmindDelta = %ld, maxdDelta = %ld\n", mindDelta, maxdDelta);
}
