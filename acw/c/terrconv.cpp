/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Convert Terrain LBM into nybble file
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/05/04  22:09:38  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <iostream.h>
#include <fstream.h>
#include <stdio.h>

#include "ilbm.h"
#include "makename.h"
#include "image.h"
#include "campter.h"

/*
 * Write an RLE encoded line of data to output stream
 */

size_t rleWrite(ofstream& out, unsigned char* ad, size_t length)
{
	int x = 0;
	streampos startPos = out.tellp();

	while(x < length)
	{
		if((x != (length - 1)) && (ad[0] == ad[1]))		// Byte Run
		{
			int i = 1;
			UBYTE v = *ad++;
			x++;

			while((x < length) && (ad[0] == v))
			{
				ad++;
				i++;
				x++;
			}

			while(i)
			{
				int l;
				if(i > 128)
					l = 128;
				else
					l = i;

				out.put((unsigned char)(257 - l));
				out.put(v);
				i -= l;
			}
		}
		else							// Byte Copy
		{
			UBYTE* start = ad;

			int i = 1;
			UBYTE v = *ad++;
			x++;

			/*
			 * Need a run of at least 3 to make it worth breaking the copy
			 */

			while( (x < length) && ( (ad[0] != v) || (ad[1] != v)))
			{
				v = *ad++;
				i++;
				x++;
			}

			if(x < length)
			{
				i--;
				ad--;
				x--;
			}

			while(i)
			{
				int l;

				if(i > 128)
					l = 128;
				else
					l = i;

				out.put((unsigned char)(l - 1));
				out.write(start, l);
				start += l;
				i -= l;
			}
		}
	}

	return out.tellp() - startPos;
}


void convert(const char* srcName, const char* destName)
{
	Image image;

	cout << "Converting " << srcName << " " << flush;

	if(readILBM(srcName, &image) == 0)
	{
#ifdef DEBUG
		cout << image.getWidth() << " by " << image.getHeight() << endl;
#endif

		char outName[FILENAME_MAX];
		makeFilename(outName, destName, ".TER", False);

		ofstream out(outName, ios::out | ios::binary);
		if(out)
		{
			TerrainHeaderF header;
			
			streampos headerPos = out.tellp();

			putWord(&header.width, image.getWidth());
			putWord(&header.height, image.getHeight());
			putLong(&header.dataSize, 0);					// Fill this in later
			out.write((char*)&header, sizeof(header));

			/*
			 * Fill out dummy line pointers
			 */

			streampos linePos = out.tellp();

			int lineCount = image.getHeight();
			ILONG iLong;
			putLong(&iLong, 0);
			while(lineCount--)
				out.write((char*)&iLong, sizeof(iLong));

			streampos dataPos = out.tellp();
			unsigned long dataOffset = 0;

			unsigned char* outBuffer = new unsigned char[(image.getWidth() + 1) / 2];

			lineCount = image.getHeight();
			unsigned char* srcPtr = image.getAddress();
			while(lineCount--)
			{
				/*
				 * Update the offset table
				 */

				streampos here = out.tellp();
				out.seekp(linePos);
				putLong(&iLong, dataOffset);
				out.write((char*)&iLong, sizeof(iLong));
				out.seekp(here);
				linePos += sizeof(iLong);

				/*
				 * Copy line into buffer
				 */

				enum { Hi, Lo } nybble = Hi;
				unsigned char destNyble = 0;
				unsigned char* destPtr = outBuffer;

				int wCount = image.getWidth();
				while(wCount--)
				{
					unsigned char col = *srcPtr++;
					if(col & 0xf0)
					{
						cout << "Warning: Colour " << col << " is used" << endl;
						col &= 15;
					}

					if(nybble == Hi)
					{
						destNyble = (unsigned char)(col << 4);
						nybble = Lo;
					}
					else
					{
						destNyble |= col;

						*destPtr++ = destNyble;

						nybble = Hi;
						destNyble = 0;
					}

				}

				if(nybble == Lo)
					*destPtr++ = destNyble;

				dataOffset += rleWrite(out, outBuffer, destPtr - outBuffer);
			}

			putLong(&header.dataSize, out.tellp() - dataPos);
			out.seekp(headerPos);
			out.write((char*)&header, sizeof(header));


			delete outBuffer;
		}

	}
}

void usage()
{
	cout << "\nterrconv srcname[.LBM] destname[.TER]" << endl;
	cout << "\nConverts LBM into Terrain file\n" << endl;
}

void main(int argc, char* argv[])
{
	const char* srcName = 0;
	const char* outName = 0;

	int i = 0;
	while(++i < argc)
	{
		if(srcName)
		{
			if(outName)
			{
				usage();
				return;
			}
			else
				outName = argv[i];
		}
		else
			srcName = argv[i];
	}
	if(srcName && outName)
	{
		try
		{
			convert(srcName, outName);
		}
 		catch(GeneralError e)
 		{
			cout << "Untrapped Error: ";
			if(e.get())
				cout << e.get();
			else
				cout << "No description";
			cout << endl;
 		}
 		catch(...)
 		{
 			cout << "Caught some kind of error" << endl;
 		}
	}
	else
		usage();
}
