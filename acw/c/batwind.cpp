/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battle Map Window and Icon Interface
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.14  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.9  1994/04/22  22:14:22  Steven_Green
 * Icon Handling tidied up.
 *
 * Revision 1.1  1994/03/17  14:29:31  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "batwind.h"
#include "batldata.h"
#include "layout.h"
#include "system.h"
#include "screen.h"
#include "scroll.h"
#include "miscfunc.h"
#include "map3d.h"
#include "sprlist.h"
#include "earth.h"
#include "msgwind.h"
#include "language.h"
#ifdef DEBUG
#include "timer.h"
#endif
#include "colours.h"
#include "menudata.h"
#include "game.h"
#include "sprlib.h"
#include "baticon.h"
#include "membodge.h"



/*=======================================================
 * Icons in map view clump
 */

class RotLeftIcon : public BattleIcon {
public:
	RotLeftIcon(IconSet* parent, const Rect& r) : BattleIcon(parent, BS_RotLeft, r) { }
	void execute(Event* event, MenuData* data);
};

void RotLeftIcon::execute(Event* event, MenuData* data)
{
	data=data;

	if(event->overIcon)
		data->showHint( language( lge_rotFieldLeft ) );

	if(event->buttons)
	{
		battle->data3D.view.yRotate += 0x4000;
#if 0
		battle->rotBar->setRedraw();
		battle->UDBar->setRedraw();
		battle->LRBar->setRedraw();
#endif
		battle->updateView();
	}
}


class RotRightIcon : public BattleIcon {
public:
	RotRightIcon(IconSet* parent, const Rect& r) : BattleIcon(parent, BS_RotRight, r) { }
	void execute(Event* event, MenuData* data);
};

void RotRightIcon::execute(Event* event, MenuData* data)
{
	data=data;

	if(event->overIcon)
		data->showHint( language( lge_rotFieldRight ) );

	if(event->buttons)
	{
		battle->data3D.view.yRotate -= 0x4000;
#if 0
		battle->rotBar->setRedraw();
		battle->UDBar->setRedraw();
		battle->LRBar->setRedraw();
#endif
		battle->updateView();
	}
}

class ZoomInIcon : public BattleIcon {
public:
	ZoomInIcon(IconSet* parent, const Rect& r) : BattleIcon(parent, BS_ZoomIn, r) { }
	void execute(Event* event, MenuData* data);
};

void ZoomInIcon::execute(Event* event, MenuData* data)
{

	if(event->overIcon)
		data->showHint( language( lge_ZoomIn ) );

	if(event->buttons)
		battle->startZoomMode();
	else if(event->key == '+')
		battle->zoomIn();
}



class ZoomOutIcon : public BattleIcon {
public:
	ZoomOutIcon(IconSet* parent, const Rect& r) : BattleIcon(parent, BS_ZoomOut, r) { }
	void execute(Event* event, MenuData* data);
};

void ZoomOutIcon::execute(Event* event, MenuData* data)
{
	data=data;

	if(event->overIcon)
		data->showHint( language( lge_ZoomOut ) );

	if(event->buttons)
		battle->zoomOut();
	else if(event->key == '-')
		battle->zoomOut();
}


class ZoomIcon : public Icon {
	BattleSprite iconNumber;
	int level;
public:
	ZoomIcon(IconSet* parent, int zoomLevel, BattleSprite icon, const Rect& r);
	void execute(Event* event, MenuData* data);
	void drawIcon();
};

ZoomIcon::ZoomIcon(IconSet* parent, int zoomLevel, BattleSprite icon, const Rect& r) :
	Icon(parent, r)
{
	level = zoomLevel;
	iconNumber = icon; 
}


void ZoomIcon::drawIcon()
{
	BattleSprite icon = iconNumber;
	if(level == battle->data3D.view.zoomLevel)
		INCREMENT(icon);

	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	battle->spriteLib->drawCentredSprite(&bm, icon);

	machine.screen->setUpdate(bm);
}

void ZoomIcon::execute(Event* event, MenuData* data)
{
	data=data;

	if(event->overIcon)
	{
		BattleSprite a = BS_ZoomCentre;

		LGE message = lge_ZoomRightIn;

		while ( a < iconNumber ) 
		{
			INCREMENT( message );

			INCREMENT( a );

			if ( a < iconNumber ) INCREMENT( a );
		}
	
		data->showHint( language( message ) );
	}

	if(event->buttons)
	{
#ifdef BATEDIT	// Centre on battlefield
		if( (level < 0) && (event->buttons & Mouse::RightButton) )
		{
			battle->zoomIn(battle->data3D.view.zoomLevel, BattleLocation(0,0));
		}
		else
#endif
		{
			ZoomLevel l;

			if(level < 0)
				l = battle->data3D.view.zoomLevel;
			else
				l = ZoomLevel(level);

			battle->startZoomMode(l);
		}
	}
}



#if 0
class BankUpIcon : public BattleIcon {
public:
	BankUpIcon(IconSet* parent) : BattleIcon(parent, BS_BankUp, Rect(75,0,37,26)) { }
	void execute(Event* event, MenuData* data);
};

void BankUpIcon::execute(Event* event, MenuData* data)
{
	data=data;

	if(event->buttons)
	{
		// battle->data3D.view.centre.rotX(Degree(-5));

		Wangle dir = battle->data3D.view.xRotate;

		dir += Degree(5);
		if(dir > 0x4000)
		{
			if(dir < 0x8000)
				dir = 0x4000;
			else
				dir = 0;
		}

		battle->data3D.view.xRotate = dir;

#if 0
		battle->bankBar->setRedraw();
#endif
		battle->updateView();
	}
}

class BankDownIcon : public BattleIcon {
public:
	BankDownIcon(IconSet* parent) : BattleIcon(parent, BS_BankDown, Rect(75,26,37,26)) { }
	void execute(Event* event, MenuData* data);
};

void BankDownIcon::execute(Event* event, MenuData* data)
{
	data=data;

	if(event->buttons)
	{
		Wangle dir = battle->data3D.view.xRotate;

		dir -= Degree(5);
		if(dir > 0x4000)
		{
			if(dir < 0x8000)
				dir = 0x4000;
			else
				dir = 0;
		}

		battle->data3D.view.xRotate = dir;

#if 0
		battle->bankBar->setRedraw();
#endif
		battle->updateView();
	}
}
#endif

/*
 * Battle View Icon Clump
 */

#if 0
BattleViewIcons::BattleViewIcons(IconSet* parent) :
	IconSet(parent, Rect(524,428,109,49))
{
	icons = new IconList[11];

	icons[0] = new RotLeftIcon	(this, Rect( 0, 0,37,24));
	icons[1] = new RotRightIcon(this, Rect(38, 0,37,24));
	icons[2] = new ZoomInIcon	(this, Rect( 0,25,37,24));
	icons[3] = new ZoomOutIcon	(this, Rect(38,25,37,24));
	icons[4] = new ZoomIcon(this, -1,        BS_ZoomCentre, Rect(76, 1, 16,15));
	icons[5] = new ZoomIcon(this, Zoom_8,    BS_Zoom1,		  Rect(93, 1, 16,15));
	icons[6] = new ZoomIcon(this, Zoom_4,    BS_Zoom2,		  Rect(76,17, 16,15));
	icons[7] = new ZoomIcon(this, Zoom_2,    BS_Zoom3,		  Rect(93,17, 16,15));
	icons[8] = new ZoomIcon(this, Zoom_1,    BS_Zoom4,		  Rect(76,33, 16,15));
	icons[9] = new ZoomIcon(this, Zoom_Half, BS_Zoom5,		  Rect(93,33, 16,15));
	icons[10] = 0;
}
#else		// Adjust positions slightly to allow black border
BattleViewIcons::BattleViewIcons(IconSet* parent) :
	IconSet(parent, Rect(523,427,111,51))
{
	icons = new IconList[11];

	icons[0] = new RotLeftIcon	(this, Rect( 1, 1,37,24));
	icons[1] = new RotRightIcon(this, Rect(39, 1,37,24));
	icons[2] = new ZoomInIcon	(this, Rect( 1,26,37,24));
	icons[3] = new ZoomOutIcon	(this, Rect(39,26,37,24));
	icons[4] = new ZoomIcon(this, -1,        BS_ZoomCentre, Rect(77, 2, 16,15));
	icons[5] = new ZoomIcon(this, Zoom_8,    BS_Zoom1,		  Rect(94, 2, 16,15));
	icons[6] = new ZoomIcon(this, Zoom_4,    BS_Zoom2,		  Rect(77,18, 16,15));
	icons[7] = new ZoomIcon(this, Zoom_2,    BS_Zoom3,		  Rect(94,18, 16,15));
	icons[8] = new ZoomIcon(this, Zoom_1,    BS_Zoom4,		  Rect(77,34, 16,15));
	icons[9] = new ZoomIcon(this, Zoom_Half, BS_Zoom5,		  Rect(94,34, 16,15));
	icons[10] = 0;
}
#endif

/*
 * Bodge to draw a nice border around zoom icons
 */

void BattleViewIcons::drawIcon()
{
	Region bitmap(machine.screen->getImage(), Rect(getPosition(), getSize()));

	bitmap.fill(Black);

	IconSet::drawIcon();
}

/*==========================================================
 * Icons and scroll bars
 */
class UpDownBar : public VerticalScrollBar {
public:
	UpDownBar(IconSet* parent, Rect r) : VerticalScrollBar(parent, r) { }
	void clickAdjust(int v);
	void getSliderSize(SDimension& from, SDimension& to, SDimension size);
};

void UpDownBar::clickAdjust(int v)
{
	ViewPoint& view = battle->data3D.view;

	Wangle dir = view.yRotate;

	Cord3D d = v * battle->grid->getScrollDistance(view.zoomLevel);

	switch(dir & 0xc000)
	{
	case 0:
		view.zCentre += d;
		break;

	case 0x4000:
		view.xCentre += d;
		break;

	case 0x8000:
		view.zCentre -= d;
		break;

	case 0xc000:
		view.xCentre -= d;
		break;
	}

	battle->updateView();
}


void UpDownBar::getSliderSize(SDimension& from, SDimension& to, SDimension size)
{
	ViewPoint& v = battle->data3D.view;
	int w = battle->grid->getGrid(v.zoomLevel)->gridCount;
	int sz = v.gridH;
	Wangle dir = v.yRotate;
	int pos;

	switch(dir & 0xc000)
	{
	case 0:
		pos = w - v.gridZ - sz;
		break;
	case 0x4000:
		pos = w - v.gridX - sz;
		break;
	case 0x8000:
		pos = v.gridZ;
		break;
	case 0xc000:
		pos = v.gridX;
		break;
	}

	from = (pos * size) / w;
	to   = ( sz * size) / w + from;
}


class LeftRightBar : public HorizontalScrollBar {
public:
	LeftRightBar(IconSet* parent, Rect r) : HorizontalScrollBar(parent, r) { }
	void clickAdjust(int v);
	void getSliderSize(SDimension& from, SDimension& to, SDimension size);
};

void LeftRightBar::clickAdjust(int v)
{
	ViewPoint& view = battle->data3D.view;

	Wangle dir = view.yRotate;	// centre.getYR();

	Cord3D d = v * battle->grid->getScrollDistance(view.zoomLevel);

	switch(dir & 0xc000)
	{
	case 0:
		view.xCentre += d;
		break;

	case 0x4000:
		view.zCentre -= d;
		break;

	case 0x8000:
		view.xCentre -= d;
		break;

	case 0xc000:
		view.zCentre += d;
		break;
	}

	battle->updateView();
}

void LeftRightBar::getSliderSize(SDimension& from, SDimension& to, SDimension size)
{
	ViewPoint& v = battle->data3D.view;
	int w = battle->grid->getGrid(v.zoomLevel)->gridCount;
	int sz = v.gridW;
	int pos;

	Wangle dir = v.yRotate;
	switch(dir & 0xc000)
	{
	case 0:
		pos = v.gridX;
		break;
	case 0x4000:
		pos = w - v.gridZ - sz;
		break;
	case 0x8000:
		pos = w - v.gridX - sz;
		break;
	case 0xc000:
		pos = v.gridZ;
		break;
	}

	from = (pos * size) / w;
	to   = ( sz * size) / w + from;
}

#if 0
class BankBar : public VerticalScrollBar {
public:
	BankBar(IconSet* parent, Rect r) : VerticalScrollBar(parent, r, BM_BankUp, BM_BankDown) { }

	void clickAdjust(int v);
	void getSliderSize(SDimension& from, SDimension& to, SDimension size);
	void drawSlider(Region& bm, const Rect& r);
};

void BankBar::clickAdjust(int v)
{
	Wangle dir = battle->data3D.view.xRotate;	// centre.getXR();

	dir += Wangle(Degree(5 * v));

	if(dir > 0x4000)
	{
		if(dir < 0x8000)
			dir = 0x4000;
		else
			dir = 0;
	}

	battle->data3D.view.xRotate = dir;	// centre.setXR(dir);
	battle->updateView();
}

void BankBar::getSliderSize(SDimension& from, SDimension& to, SDimension size)
{
	Wangle dir = battle->data3D.view.xRotate;	// centre.getXR();
	const SDimension sSize = 16;

	size -= sSize;

	if(dir > 0x4000)
	{
		if(dir < 0x8000)
			dir = 0x4000;
		else
			dir = 0;
	}

	battle->data3D.view.xRotate = dir;

	dir = 0x4000 - dir;
	from = SDimension((dir * size) / 0x4000);

	to = SDimension(from + sSize);
}


void BankBar::drawSlider(Region& bm, const Rect& r)
{
	ScrollBar::drawSlider(bm, r);

	SDimension left = SDimension(r.getX() + 3);
	SDimension right = SDimension(r.getX() + r.getW() - 3);
	SDimension top = SDimension(r.getY() + 3);
	SDimension bottom = SDimension(r.getY() + r.getH() - 3);
	SDimension xsize = SDimension(right - left);
	SDimension ysize = SDimension(bottom - top);

	bm.line(Point(left, bottom), Point(right, bottom), Black);

	Wangle dir = battle->data3D.view.xRotate;	// centre.getXR();
	SDimension x = SDimension(mcos(xsize, dir));
	SDimension y = SDimension(msin(ysize, dir));
	bm.line(Point(left, bottom), Point(left+x, bottom-y), Black);
}
#endif

class RotateBar : public HorizontalScrollBar {
public:
	RotateBar(IconSet* parent, Rect r) : HorizontalScrollBar(parent, r, SCR_RotLeft, SCR_RotRight) { }

	void clickAdjust(int v);
	void getSliderSize(SDimension& from, SDimension& to, SDimension size);
	void drawSlider(Region& bm, const Rect& r);
};

void RotateBar::clickAdjust(int v)
{
	// battle->data3D.view.centre.rotY(Degree(-5 * v));
	// battle->data3D.view.centre.rotY(Degree((v < 0) ? 0xc000 : 0x4000));

	if(v < 0)
		battle->data3D.view.yRotate -= 0x4000;
	else
		battle->data3D.view.yRotate += 0x4000;


	battle->updateView();
}

void RotateBar::getSliderSize(SDimension& from, SDimension& to, SDimension size)
{
#if 0
	Wangle bank = -battle->data3D.view.yRotate;	// centre.getYR();
	const SDimension sSize = 16;

	size -= sSize;

	from = SDimension(((long)bank * size) / 0x10000);

	to = SDimension(from + sSize);
#else
	/*
	 * Bar only displays direction
	 */

#ifdef BEFORE_DUTCH
	from = size/3;
	to = size - size/3;
#else
	from = size/4;
	to = size - size/4;
#endif

#endif
}

void RotateBar::drawSlider(Region& bm, const Rect& r)
{
	ScrollBar::drawSlider(bm, r);

#if 0
	SDimension cx = SDimension(r.getX() + r.getW() / 2);
	SDimension cy = SDimension(r.getY() + r.getH() / 2);

	SDimension xSize = SDimension(r.getW()/2 - 2);
	SDimension ySize = SDimension(r.getH()/2 - 2);

	Wangle dir = -battle->data3D.view.yRotate;	// centre.getYR();
	SDimension x = SDimension(msin(xSize, dir));
	SDimension y = SDimension(mcos(ySize, dir));
	bm.line(Point(cx, cy), Point(cx+x, cy-y), Black);
#else
	/*
	 * Display text direction
	 */

	static LGE directionName[4] = {
		lge_FacingNorth,
		lge_FacingEast,
		lge_FacingSouth,
		lge_FacingWest,
	};

	TextWindow win(&bm, Font_EMFL_8);
	win.setFormat(True, True);
	win.setColours(White);
	win.draw( language( directionName[(battle->data3D.view.yRotate >> 14) & 3]) );


#endif
}


/*
 * The main display area!
 */

BattleMapArea::BattleMapArea(BattleMap* parent, const Rect& r) :
	Icon(parent, r)
{
	map = parent;
	bm = new Region(machine.screen->getImage(), Rect(parent->getPosition() + r, r.getSize()));
	buffer = new Bitmap(r.getSize());

	battle->data3D.view.setDisplaySize(r.getSize());
	battle->data3D.bitmap = buffer;
#ifdef USE_SCANBUF
	battle->data3D.scanBuffer.init(bm->getH());
#else
	battle->data3D.zBuffer.init(bm->getSize());
#endif
}

void BattleMapArea::execute(Event* event, MenuData* d)
{
	d=d;

	if(event->overIcon)
		battle->overMap(event, this, d);
		// map->overMap(event, this, d);
		// d->doEvent(event, this);
}

void BattleMapArea::drawIcon()
{
	/*
	 * Set up to draw
	 */


	/*
	 * Draw the map
	 */

#ifdef DEBUG
	/*
	 * Time how long it takes
	 */

	static unsigned long oldTick = 0;

	unsigned long prevTick = oldTick;
	unsigned long ticks = timer->getFastCount();

#endif

	bm->blit(buffer->getImage(), Point(0,0));

#ifdef DEBUG
	unsigned long ticks1 = timer->getFastCount();
#endif
	/*
	 * Render all the objects
	 */

	/*
	 * Put objects onto map
	 */

	battle->data3D.bitmap = bm;
	battle->drawObjects(battle->data3D);

#ifdef DEBUG
	unsigned long ticks2 = timer->getFastCount();
#endif

	battle->spriteList->draw(battle->data3D);

#ifdef DEBUG
	unsigned long ticks4 = timer->getFastCount();
#endif

	battle->earthFace->draw(bm);
	battle->data3D.bitmap = buffer;

#if defined(DEBUG) && !defined(BATEDIT)
	oldTick = timer->getFastCount();

	/*
	 * Display info down rhs...
	 */

	if(battle->showDisplayInfo)
	{
		Region textBm(machine.screen->getImage(), Rect(517, 110, 120, 260));
		textBm.fill(White);
		TextWindow win(&textBm, Font_EMFL_8);
		win.setColours(Black);

		int fps;
		int fpsd;

		unsigned long totalTicks = oldTick - prevTick;

		if(totalTicks)
		{
			fps = timer->fastTPS / totalTicks;
			fpsd = ((timer->fastTPS % totalTicks) * 1000) / totalTicks;
		}
		else
			fps = fpsd = 0;

		win.wprintf("BackBlit: %ld\r"
						"Draw Obs: %ld\r"
						"Draw Spr: %ld\r"
						" Earth: %ld\r"
						" other: %ld\r"
						" Total: %ld\r"
						"   FPS: %d.%03d\r",
			ticks1 - ticks,
			ticks2 - ticks1,
			ticks4 - ticks2,
			oldTick - ticks4,
			ticks - prevTick,
			totalTicks,
			fps, fpsd);

		win.wprintf("\rloc: %ld,%ld,%ld\rrot: %04x,%04x\rcen: %ld,%ld\r",
			battle->data3D.view.focus.x,
			battle->data3D.view.focus.y,
			battle->data3D.view.focus.z,
			battle->data3D.view.xRotate,
			battle->data3D.view.yRotate,
			battle->data3D.view.xCentre,
			battle->data3D.view.zCentre);

		win.wprintf("\rview: %ld,%ld,%ld\r",
			battle->data3D.view.getDistance(),
			battle->data3D.view.getFront(),
			battle->data3D.view.getViewWidth());
 
		win.wprintf("\rGrid: %d,%d %d by %d\r",
			battle->data3D.view.gridX,
			battle->data3D.view.gridZ,
			battle->data3D.view.gridW,
			battle->data3D.view.gridH);

		win.wprintf("Detail: %d\r", battle->data3D.view.zoomLevel);

		win.wprintf("\rearthY: %d\r", battle->earthFace->getBottom());

		machine.screen->setUpdate(textBm);
	}

#endif

#ifdef DEBUG
	Region r = *bm;
	r.setClip(bm->getX() + 300, bm->getY() + 2, 140, 10);
	TextWindow wind(&r, Font_JAME_5);
	wind.setColours(Black);
	wind.setFormat(True, True);

	r.fill(White);

	if(battle->gotMouseLocation)
	{
		wind.wprintf("%ld, %ld",
			battle->mouseLocation.x,
			battle->mouseLocation.z);
	}
	else
		wind.wprintf("Off land");
#endif

	/*
	 * Put onto the physical screen
	 */

	machine.screen->setUpdate(*bm);
}

BattleMapArea::~BattleMapArea()
{
	delete buffer;
	delete bm;
}

/*
 * Update the lightsource based on time of date
 */

void BattleData::setLight()
{
	const Intensity minAmbience = intensityRange / 5;
	const Intensity normalAmbience = intensityRange / 4;
	const Wangle sunHeight = 0x2800;
	const Intensity minIntensity = intensityRange / 4;
	// const Intensity normalIntensity = intensityRange - 1;
	const Intensity normalIntensity = (intensityRange * 3) / 4;	// Darkened for Masters version
	const Wangle dusk = Wangle(0x3000);		// 4:30am

	TimeInfo t = gameTime;

	/*
	 * calculate an angle, where 0=midnight, 0x8000=midday
	 */

	Wangle a = ((t.ticksSinceMidnight/256) * 0x10000) / (GameTicksPerDay/256);

	/*
	 * Height in the sky
	 */

	Wangle height = dusk + mcos(UWORD(sunHeight), a);
	Wangle bearing = 0x8000 + a;

	Intensity i;
	Intensity ambience;

	if( (a < dusk) || (a > Wangle(-dusk)) )
	{
		if(a > 0x8000)
			a = -a;

		a = dusk - a;

		if(a < 0x1000)
		{
			Intensity ir = Intensity(normalAmbience - minAmbience);

			ambience = Intensity(normalAmbience - (a * ir ) / 0x1000);
		}
		else
			ambience = minAmbience;
	
		i = minIntensity;	// small intensity to give a little shading
	}
	else
	{
		ambience = normalAmbience;
		i = normalIntensity;

		// Reduce intensity during last 0x800

		if(a > 0x8000)
			a = -a;
		a -= dusk;
		if(a < 0x800)
		{
			const Intensity ir = Intensity(normalIntensity - minIntensity);

			i = Intensity(minIntensity + (a * ir) / 0x800);
		}
	}

	data3D.light.setHeight(height);
	data3D.light.setBearing(bearing);
	data3D.light.setIntensity(i);
	data3D.light.setAmbience(ambience);
	data3D.light.update();

#ifdef DEBUG1
	char buf[256];

	sprintf(buf, "h=%04x, b=%04x, i=%04x, a=%04x",
		UWORD(height), UWORD(bearing), i, ambience);

	message->showInfo(buf);
#endif
}

/*
 * Battle Map Window including scroll bars.
 */


BattleMap::BattleMap(IconSet* parent) :
	IconSet(parent, Rect(0,22,518,350))
{
	icons = new IconList[8];

	icons[0] = mapArea = new BattleMapArea	(this, Rect(     BMapX,    BMapY,    BMapW,    BMapH));
	icons[1] = UDBar	 = new UpDownBar		(this, Rect( VScrollX2, VScrollY, VScrollW, VScrollH));
	icons[2] = rotBar	 = new RotateBar		(this, Rect(  HScrollX, VBorderY, HScrollW, HScrollH));
	icons[3] = LRBar	 = new LeftRightBar	(this, Rect(  HScrollX, HScrollY, HScrollW, HScrollH));
	icons[4] 			 = new BlankIcon		(this, Point(VBorderX2, VBorderY));
	icons[5] 			 = new BlankIcon		(this, Point(VBorderX2, HScrollY));
	icons[6]           = new MapBorder     (this, Rect(VBorderX1, VBorderY, VBorderW, VBorderH));
	icons[7] = 0;
}

