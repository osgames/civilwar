/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign AI: Order of Battle
 *
 * What it does:
 *   - Assign newly mobilised regiments to OB.
 *   - Balance the OB tree when units transferred/assigned including
 * 	 the creation of new armies
 *   - Reattach units with same order as superior
 *   - Transfer units (or create new organisations) when outside their
 *     commander's Field of Operation, or within the FO of another
 *     commander.
 *
 * Probably want to do this in 2 phases...
 *  1) At the start of the AI phase to attach new regiments
 *  2) At the end of the AI phase to transfer units.
 *
 * Todo:
 *   Make up new army names.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdlib.h>
// #include <string.hpp>
#include "ai_camp.h"
#include "tables.h"
#include "campaign.h"
#include "campwld.h"
#include "unititer.h"
#include "strutil.h"
#include "memptr.h"
#include "game.h"

#ifdef DEBUG
#include "clog.h"

static LogFile obLog("ai_ob.log");
#endif


/*
 * Maximum number of children allowed under each rank
 */

static int maxChildren[] = {
	255,			// President
	10,			// 10 Corps per army?
	10,			// 10 Divisions per corps
	10,			// 10 Brigades per division
	6,				// 6 Regiments per Brigade
	0,				// Nothing below Regiment!
};

#ifdef DEBUG

/*
 * Check that childCount matches actual number of children
 */


void checkChildren(Unit* u)
{
	Boolean hasSeperated = False;
	Boolean hasReallySeperated = False;

	int i = 0;
	Unit* unit = u->child;

	while(unit)
	{
		i++;

		if(unit->seperated)
			hasSeperated = True;
		if(unit->reallySeperated)
			hasReallySeperated = True;

		unit = unit->sister;
	}

	if(i != u->childCount)
		throw GeneralError("%s: childCount=%d, actual children=%d\n",
			u->getName(True),
			(int) u->childCount,
			i);

#if 0
	ASSERT(hasSeperated == u->hasSeperated);
	ASSERT(hasReallySeperated == u->hasReallySeperated);
#endif
}

#endif

/*
 * Add a Unit to OB attempting to keep balance
 */

void addUnit(OrderBattle& ob, Unit* parent, Unit* newUnit)
{

#ifdef DEBUG
	ob.check(__FILE__, __LINE__);
	checkChildren(parent);
#endif

	Unit* to = parent;
	Unit* unit = newUnit;

	/*
	 * Check that there are not too many units in organisation
	 * and split of neccessary
	 */

	Boolean finished = False;
	Boolean firstLoop = True;

	while(!finished)
	{
		finished = True;

		ASSERT(to != 0);
		ASSERT(unit != 0);
		ASSERT(to->getRank() == promoteRank(unit->getRank()));

#ifdef DEBUG
		obLog.printf("  adding %s to %s (children=%d)", unit->getName(True), to->getName(True), (int) unit->childCount);
#endif

		int maxC = maxChildren[to->getRank()];
		if(firstLoop)
		{
			firstLoop = False;
			maxC--;
		}


		if((to->getRank() > Rank_President) &&
		   (to->childCount > maxC) )
		{
			/*
			 * Split up the unit into 2!
			 */

#ifdef DEBUG
			obLog.printf("  Splitting %s", to->getName(True));
#endif

			/*
			 * Take off half the children and assign to new unit
			 */

			int childrenToTake = to->childCount / 2;

			int skipCount = to->childCount - childrenToTake - 1;

			unit = to->child;
			while(skipCount-- > 0)
			{
				ASSERT(unit != 0);
				unit = unit->sister;
			}

			ASSERT(unit != 0);

			Unit* last = unit;
			unit = unit->sister;
			last->sister = 0;

			to->childCount -= childrenToTake;
			to->updateHasSeperated();

#ifdef DEBUG
			checkChildren(to);
			ob.check(__FILE__, __LINE__);
#endif

			/*
			 * Create a new superior
			 */

			ASSERT(unit != 0);
			ASSERT(to->superior != 0);

			Unit* newUnit = to->superior->makeNewUnder(to->location);

			if(to->getRank() == Rank_Army)
			{
				Army* newArmy = (Army*)newUnit;
				newArmy->makeArmyName();
			}

			newUnit->child = unit;
			newUnit->childCount = childrenToTake;

			while(unit)
			{
				unit->superior = newUnit;
				unit = unit->sister;
			}

			newUnit->updateHasSeperated();

#ifdef DEBUG
			checkChildren(newUnit);
			ob.check(__FILE__, __LINE__);
#endif


#ifdef DEBUG
			ob.check(__FILE__, __LINE__);
#endif

			/*
			 * Make a general for the new Unit
			 */

			ob.makeGeneral(newUnit);

			/*
			 * Loop around to attach newUnit
			 */

			to = to->superior;
			unit = newUnit;

			finished = False;
		}
	}

	/*
	 * Now add it to organisation
	 */

	ob.assignUnit(newUnit, parent);


#ifdef DEBUG
	ob.check(__FILE__, __LINE__);
#endif
}

/*================================================================
 * Relocate Unit to organisation nearer to it
 */

static Distance fieldOfOperation[] = {
	Mile(2000),		// President
	Mile(250),		// Army
	Mile(150),		// Corps
	Mile(100),		// Division
	Mile(70),		// Brigade
};

inline Boolean inFop(Unit* unit, Unit* command)
{
	Distance fop = fieldOfOperation[command->getRank()];

	Distance d = distanceLocation(unit->location, command->location);

	if(d > fop)
	{
		Distance d1 = distanceLocation(unit->givenOrders.destination, command->location);

		if(d1 > fop)
		{
#ifdef DEBUG
			obLog.printf("%s out of Field Of Operation of %s",
				unit->getName(True),
				command->getName(True));
			obLog.printf("  d=%d, d1=%d",
				unitToMile(d),
				unitToMile(d1));
#endif
			return False;
		}
	}

	return True;
}

void relocateUnit(Unit* unit)
{
#ifdef DEBUG
	obLog.printf("");
	obLog.printf("Relocating %s", unit->getName(True));
#endif

	/*
	 * Step 1:
	 *   Move up ranks to find suitable organisation to attach to
	 */

	/*
	 * Patch made on 14th December 1995
	 * Problem was:
	 *		If have an OB like:
	 *		------Corps-------
	 *            |
	 *         Division
	 *            |
	 *         Brigade
	 *
	 * Where the brigade is out of FOP from Division but in FOP of corps
	 * the unassign unit function call deletes division, brigade and corps
	 * and then tries to attach to the deleted corps.
	 *
	 * Solution is to track the number of children while climbing up the
	 * tree and disallow any change if number of children is never greater
	 * than 1
	 */

	//----------- Patch 15dec95 --------------
	Boolean canTransfer = False;
	//----------- Patch 15dec95 --------------


	Unit* sup = unit->superior;
	ASSERT(sup != 0);
	while(sup->getRank() > Rank_President)
	{
		//----------- Patch 15dec95 --------------
		if(sup->childCount > 1)
			canTransfer = True;
		//----------- Patch 15dec95 --------------

		sup = sup->superior;
		ASSERT(sup != 0);

		Unit* bestChild = 0;
		Distance bestD = 0;
		Unit* c = sup->child;
		while(c)
		{
			if(inFop(unit, c))
			{
				Distance d = distanceLocation(unit->location, c->location);
				if(!bestChild || (d < bestD))
				{
					bestChild = c;
					bestD = d;
				}
			}
			c = c->sister;
		}

		if(bestChild)
		{
			sup = bestChild;
			break;
		}
	}

	//----------- Patch 15dec95 --------------
	if(!canTransfer)
	{
#ifdef DEBUG
		obLog.printf("Not transferring because in range of lone superior %s", sup->getName(True));
#endif
		return;
	}
	//----------- Patch 15dec95 --------------



	ASSERT(sup != 0);

#ifdef DEBUG
	obLog.printf("Attaching to %s", sup->getName(True));
#endif

	/*
	 * Step 2:
	 *		Move down to apropriate rank and attach
	 *		Or create new organisation if none found
	 */

	Rank pRank = promoteRank(unit->getRank());

	while(sup->getRank() != pRank)
	{
		Unit* bestChild = 0;
		Distance bestD = 0;
		Unit* c = sup->child;
		while(c)
		{
			if(inFop(unit, c))
			{
				Distance d = distanceLocation(unit->location, c->location);

				if(!bestChild || (d < bestD))
				{
					bestChild = c;
					bestD = d;
				}
			}
			c = c->sister;
		}

		if(c)
		{
			sup = c;
#ifdef DEBUG
			obLog.printf("Moving down to %s", sup->getName(True));
#endif
		}
		else
		{
#ifdef DEBUG
			obLog.printf("Creating new organisation below %s", sup->getName(True));
#endif
			break;
		}
	}

	campaign->world->ob.unassignUnit(unit);
	campaign->world->ob.placeUnit(unit, sup);
#ifdef DEBUG
	obLog.printf("");
#endif
}



/*===============================================================
 * Called early on from dailyProcess to add newly mobilised
 * regiments
 */

void CampaignAI::aic_AddNewRegiments()
{
#ifdef DEBUG
	aiLog.printf("aic_Add new regiments");
	obLog.setTime(0);			// Disable Time display
	obLog.printf("");
	obLog.printf("Add new regiments for %s", sideStr[aiSide]);
#endif

	/*
	 * Scan through free regiment list
	 */

	OrderBattle& ob = campaign->world->ob;
	President* pr = ob.getPresident(aiSide);

	ASSERT(pr != 0);

	Regiment* r = pr->getFreeRegiments();

	while(r)
	{
		ASSERT(r->getRank() == Rank_Regiment);

		Regiment* r1 = (Regiment*) r->sister;		// Preserve because unit is likely to be allocated elsewhere

#ifdef DEBUG
		obLog.printf("");
		obLog.printf("Free regiment: %s", r->getName(True));
#endif

		/*
		 * Find closest brigade and attach it.
		 *
		 * Iterate through all brigades on our side
		 */

		Unit* best = 0;
		Distance bestD = 0;

		UnitIter iter(pr, False, False, Rank_Brigade, Rank_Brigade);
		while(iter.ok())
		{
			Unit* unit = iter.get();

			Distance d = distanceLocation(r->location, unit->location);
			if(!best || (d < bestD))
			{
				best = unit;
				bestD = d;
			}
		}

		/*
		 * Assign it.
		 */

		if(best)
		{
#ifdef DEBUG
			obLog.printf("  Assigning to %s", best->getName(True));
#endif
			// Assign to this brigade splitting up as neccessary

			// For debugging, let us simply attach it.

			// ob.assignUnit(r, best);

			addUnit(ob, best, r);
		}
		else
		{
#ifdef DEBUG
			obLog.printf("No brigade's found!!!!");
#endif
			ob.assignUnit(r, pr);
		}



		r = r1;
	}


#ifdef DEBUG
	obLog.printf("-------------");
	obLog.close();
#endif
}



/*===============================================================
 * Called later in daily Process to transfer units that have
 * been ordered out of the Field of Operation of their commanders
 * into a new one
 */


void CampaignAI::aic_TransferUnits()
{
#ifdef DEBUG
	aiLog.printf("Transferring Units");
	obLog.setTime(0);			// Disable Time display
	obLog.printf("");
	obLog.printf("Transferring units for %s", sideStr[aiSide]);
#endif

	/*
	 * Scan for units with same orders as parent, and rejoin them
	 */

	OrderBattle& ob = campaign->world->ob;
	President* pr = ob.getPresident(aiSide);
	ASSERT(pr != 0);

	UnitIter iter(pr, True, False, Rank_Corps, Rank_Brigade);
	while(iter.ok())
	{
		Unit* unit = iter.get();

		ASSERT(unit != 0);

		// Is order roughly the same as parent's?

		Unit* parent = unit->superior;

		ASSERT(parent != 0);

		if( (parent->givenOrders.destination  == unit->givenOrders.destination ) &&
		 	 (parent->givenOrders.basicOrder() == unit->givenOrders.basicOrder()) )
		{
#ifdef DEBUG
			Facility* f =campaign->world->facilities.findClose(parent->givenOrders.destination, Mile(50));
			const char* wName;
			if(f)
				wName = f->getNameNotNull();
			else
				wName = "Nowhere";


			obLog.printf("");
			obLog.printf("Rejoining because same destination (%s) %ld,%ld",
				wName,
				unit->givenOrders.destination.x,
				unit->givenOrders.destination.y);
			obLog.printf("  %s's Orders are: %s",
				unit->getName(True),
				getOrderStr(unit->givenOrders));
			obLog.printf("  %s's Orders are: %s",
				parent->getName(True),
				getOrderStr(parent->givenOrders));
#endif
			unit->rejoinCampaign();
		}
	}

	/*
	 * Additional steps:
	 *   - Consider promoting generals who are better than their superiors.
	 *   	 Do this randomly and infrequently, e.g. let each get considered
	 *     for promotion about once a month.
	 *   - Check for any missing Generals
	 *   - Check for commanders with nobody directly below them (could be
	 *     part of tidyUnits() in ob.cpp)... also alter rejoin so that
	 *     units move towards largest part of new organisation (i.e. if
	 *     unit has no dependants then joined units stay where they are and
	 *     commander moves to them).
	 */

	/*
	 * For EVERY Army/Corps/Division/Brigade
	 */

	iter.setup(pr, False, False, Rank_Army, Rank_Brigade);
	while(iter.ok())
	{
		Unit* unit = iter.get();

		ASSERT(unit != 0);

		/*
		 * Do we have a general at all?
		 */

		General* g = unit->general;

		if(!g)
		{
			ob.makeGeneral(unit);

			ASSERT(unit->general != 0);

#ifdef DEBUG
			obLog.printf("Created missing general %s for %s",
				unit->general->getName(),
				unit->getName(True));
#endif
		}
		else
		{
			/*
			 * Are we better than our superior?
			 *
			 * Aprox once a month consider promotion
			 */

			if(game->gameRand(30) == 0)
			{
				if(unit->getRank() > Rank_Army)
				{
#ifdef DEBUG
					obLog.printf("Considering %s for promotion", g->getName());
#endif
					Unit* sup = unit->superior;

					ASSERT(sup != 0);

					General* g1 = sup->general;

					if(!g1 || generalBetter(g, g1))
					{
#ifdef DEBUG
						obLog.printf("%s promoted from %s to %s displacing %s",
								 g->getName(),
								 unit->getName(True),
								 sup->getName(True),
								 g1 ? g1->getName() : "<Nobody>");
#endif
						ob.assignGeneral(g1, sup);
					}
					else if(unit->getRank() == Rank_Brigade)
					{
						/*
						 * If its a brigade commander, then consider
						 * replace him with someone from free general list
						 */

						ob.assignGeneral(g, 0);
						ob.makeGeneral(unit);

						ASSERT(unit->general != 0);
#ifdef DEBUG
						obLog.printf("General %s of %s discharged and replaced with %s",
							g->getName(),
							unit->getName(True),
							unit->general->getName());
#endif
					}
				}



			}

		}

	}

	/*
	 * Look for units out of field of command
	 */

	iter.setup(pr, True, False, Rank_Corps, Rank_Brigade);
	while(iter.ok())
	{
		Unit* unit = iter.get();

		ASSERT(unit != 0);

		Unit* sup = unit->superior;

		ASSERT(sup != 0);

		/*
		 * Unit and its order must be out of command range
		 *
		 * But NOT on a boat or train
		 */

		if( (unit->realOrders.mode == ORDER_Land) &&
			 (sup->realOrders.mode == ORDER_Land) &&
			 (unit->moveMode != MoveMode_Transport) &&
			 (sup->moveMode != MoveMode_Transport))
		{
			if(!inFop(unit, sup))
			{
#ifdef DEBUG
				obLog.printf("%s out of Field Of Operation of %s",
					unit->getName(True),
					sup->getName(True));
#endif
			 	relocateUnit(unit);	// This will confuse interator!
			}
		}
	}


#ifdef DEBUG
	obLog.printf("-------------");
	obLog.close();
#endif
}




