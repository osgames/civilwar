/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Mouse Driver
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.14  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.10  1994/02/15  23:22:28  Steven_Green
 * Use "ptrlist" instead of WCSPtrList
 *
 * Revision 1.8  1994/01/07  23:43:18  Steven_Green
 * Mouse button events added
 *
 * Revision 1.6  1993/12/23  09:26:01  Steven_Green
 * seperate init function added, which is called by contructor or can
 * be called later.
 *
 * Revision 1.5  1993/12/04  16:22:51  Steven_Green
 * blit routine simplified by assuming that a zero width source implies
 * the entire image should be blitted.
 *
 * Revision 1.2  1993/11/09  14:01:40  Steven_Green
 * Mouse Update Modification
 *
 * Revision 1.1  1993/11/05  16:50:53  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

// #define MOUSE_UNDER_TIMER
#define MOUSE_CALLBACK



#ifdef TESTMOUSE
#include <iostream.h>
#include <conio.h>
#endif

#include <dos.h>
#include <stdlib.h>

#include "mouse.h"
#include "mouse_s.h"
#include "timer.h"
#include "screen.h"
#include "sprite.h"
#include "error.h"
#include "language.h"

inline SDimension pixToXMouse(SDimension x) { return x; }
inline SDimension xMouseToPix(SDimension x) { return x; }
inline SDimension pixToYMouse(SDimension y) { return y; }
inline SDimension yMouseToPix(SDimension y) { return y; }

/*
 * This is required so that the interrupt handler can get at it!
 */

#ifdef DEBUG
Mouse* globalMouse = 0;
#else
static Mouse* globalMouse = 0;
#endif

/*
 * Mouse handler
 */

// #ifdef __SW_S
// #pragma off (check_stack)
// #define NO_STACK_CHECKING
// #endif

#ifdef MOUSE_CALLBACK

void readMotionCounters();

void _mouseHandler(int event, int buttons, int _xMove, int _yMove)
{
	static volatile UBYTE lock = 0;

	if(++lock == 1)
	{
		Mouse* mouse = globalMouse;

		WORD xMove;
		WORD yMove;

#pragma aux readMotionCounters = \
	"mov ax,11"						\
	"int 033h"						\
	"mov [xMove],cx"				\
	"mov [yMove],dx"				\
	modify [eax ebx ecx edx]

		readMotionCounters();

		xMove += mouse->msX;
		if(xMove < 0)
			xMove = 0;
		else if(xMove >= mouse->screen->getW())
			xMove = mouse->screen->getW() - 1;
		
		yMove += mouse->msY;
		if(yMove < 0)
			yMove = 0;
		else if(yMove >= mouse->screen->getH())
			yMove = mouse->screen->getH() - 1;

		if( (mouse->msX != xMove) || (mouse->msY != yMove) )
			event |= 1;

#if 0
		xMove = xMouseToPix(xMove);
		yMove = yMouseToPix(yMove);
#endif

		// mouse->event = short(event);
		mouse->buttons = ButtonStatus(buttons);
		mouse->msX = xMove;
		mouse->msY = yMove;

		if(mouse && !mouse->isDisabled())
		{
			/*
	 	 	 * Move the mouse on screen
	 	 	 */

			if(event & 1)
			{
				Boolean showing = !mouse->isHidden() && mouse->screen && mouse->sprite;

				if(showing)
					mouse->hide();

				mouse->set(Point(xMove, yMove));

				if(showing)
					mouse->show();
			}
		}

		/*
		 * Process button presses
		 */

		if(buttons != mouse->lastButton)
		{
			/*
			 * Work out what has been Pressed (ignore releases)
			 */

			ButtonStatus change = ButtonStatus((mouse->lastButton ^ buttons) & buttons);

			if(change)
				mouse->events.add(change, mouse->position);

			mouse->lastButton = ButtonStatus(buttons);		// Update for next time
		}

	}
	--lock;
}

extern "C" {
void _loadds __pragma("MouseHandler") mouseHandler(int event, int buttons, int xMove, int yMove)
{
	_mouseHandler(event, buttons, xMove, yMove);
}
}

#else		// MOUSE_UNDER_TIMER

/*
 * Mouse Interrupt Routine
 * 
 * Called 50 Times per second
 */

void getMouseValues();
void setMouseStack();
void restoreMouseStack();
void readMotionCounters();


void _mouseHandler()
{
	static volatile UBYTE lock = 0;

	if(++lock == 1)
	{
		volatile UWORD buttons;

#pragma aux getMouseValues = \
	"mov ax,3"						\
	"int 033h"						\
	"mov [buttons],bx"			\
	modify [eax ebx ecx edx]

		volatile WORD xMove;
		volatile WORD yMove;

#pragma aux readMotionCounters = \
	"mov ax,11"						\
	"int 033h"						\
	"mov [xMove],cx"				\
	"mov [yMove],dx"				\
	modify [eax ebx ecx edx]

		Mouse* mouse = globalMouse;

		getMouseValues();

		readMotionCounters();

		xMove += mouse->msX;
		if(xMove < 0)
			xMove = 0;
		else if(xMove >= mouse->screen->getW())
			xMove = mouse->screen->getW() - 1;
		
		yMove += mouse->msY;
		if(yMove < 0)
			yMove = 0;
		else if(yMove >= mouse->screen->getH())
			yMove = mouse->screen->getH() - 1;

		Boolean moved = False;
		if( (mouse->msX != xMove) || (mouse->msY != yMove) )
			moved = True;

		mouse->buttons = ButtonStatus(buttons);
		mouse->msX = xMove;
		mouse->msY = yMove;

		if(mouse && !mouse->isDisabled())
		{
			/*
	 	 	 * Move the mouse on screen
	 	 	 */

			if(moved)
			{
				Boolean showing = !mouse->isHidden() && mouse->screen && mouse->sprite;

				if(showing)
					mouse->hide();

				mouse->set(Point(xMove, yMove));

				if(showing)
					mouse->show();
			}
		}

		/*
		 * Process button presses
		 */

		if(buttons != mouse->lastButton)
		{
			/*
			 * Work out what has been Pressed (ignore releases)
			 */

			ButtonStatus change = ButtonStatus((mouse->lastButton ^ buttons) & buttons);

			if(change)
				mouse->events.add(change, mouse->position);

			mouse->lastButton = ButtonStatus(buttons);		// Update for next time
		}
	}
	--lock;
}

extern "C"
{
	void mouseHandler()
	{
		_mouseHandler();
	}
}

#endif	// MOUSE_UNDER_TIMER

/*
 * Assembler functions, coded as pragma's to simplify things
 *
 * This stuff is machine dependant enough anyway!
 */

int initMouse();
#pragma aux initMouse = \
	"xor ax,ax"	\
	"int 033h"		\
	"cwde"		\
	modify [ebx ecx edx]	\
	value [eax]

void setMousePosition(short x, short y);
#pragma aux setMousePosition = \
	"mov ax,4"					\
	"int 033h"					\
	parm [cx] [dx]				\
	modify [eax ebx ecx edx]

void setMouseRange(short x, short y, short w, short h);
#pragma aux setMouseRange = \
	"add bx,ax"					\
	"dec bx"						\
	"add cx,dx"					\
	"dec cx"						\
	"push cx"					\
	"push dx"					\
	"mov cx,ax"					\
	"mov dx,bx"					\
	"mov ax,7"					\
	"int 033h"					\
	"pop dx"						\
	"pop cx"						\
	"mov ax,8"					\
	"int 033h"					\
	parm [ax] [dx] [bx] [cx]		\
	modify [eax ebx ecx edx]

void setMouseRatio(short x, short y);
#pragma aux setMouseRatio = \
	"mov ax,0fh"			\
	"int 033h"				\
	parm [cx] [dx]			\
	modify [eax ebx ecx edx]

#if 0
void _setMouseHandler(UWORD mask);
#pragma aux _setMouseHandler = 		\
	"push es"								\
	"mov ax,seg mouseInterrupt"		\
	"mov es,ax"								\
	"mov edx,offset mouseInterrupt"	\
	"mov ax,12"								\
	"int 033h"								\
	"pop es"									\
	parm [cx]							 	\
	modify [eax ebx ecx edx]

void setMouseHandler()
{
	_setMouseHandler(0x1f);
}
#endif


// #ifdef NO_STACK_CHECKING
// #pragma on (check_stack)
// #endif


void Mouse::init(Screen* screen, Sprite* sprite, Point hotSpot)
{
	Mouse::sprite = sprite;
	Mouse::screen = screen;
	Mouse::hotSpot = hotSpot;

	initialised = False;		// Start as not initialised
	hidden = 1;					// Start as hidden
	disabled = 1;				// Start as disabled

	/*
	 * See if mouse driver is installed
	 */

	if(initMouse() == 0)
	{
		throw GeneralError( language( lge_NoMouse ) );
	}

	behind = new Image(sprite->getWidth(), sprite->getHeight());

	/*
	 * Set the mouse range and location
	 */

	if(screen)
	{
		setMouseRange(pixToXMouse(0), pixToYMouse(0), pixToXMouse(screen->getW()), pixToYMouse(screen->getH()));
		setMouseRatio(8, 8);
		
		moveTo(Point(screen->getW() / 2, screen->getH() / 2));
		msX = position.x;
		msY = position.y;
	}

	/*
	 * Set up miscellaneous variables
	 */

	lastButton = 0;

	/*
	 * Install our handler
	 */

	globalMouse = this;

#ifdef MOUSE_CALLBACK
	setMouseHandler();
#else	// MOUSE_UNDER_TIMER
	timer->addRoutine((TimerFunction)mouseInterrupt, 50);		// Call mouseHandler 50 times per second
#endif

	/*
	 * Set flags to indicate we're done
	 */

	forceShow();
	disabled = 0;
	initialised = True;

}


Mouse::~Mouse()
{
	 if(initialised)
	 {
	 	/*
		 * Hide and disable Mouse
		 */

		disable();
		hide();

		/*
		 * Empty queue
		 */

		clearEvents();

		/*
		 * Remove the behind buffer
		 */

		delete behind;

		/*
	 	 * Reinitialise Mouse Driver
	 	 */

		initMouse();

		globalMouse = 0;
	 }
}

void Mouse::set(Point p)
{
	position = p;
	spritePosition = p - hotSpot;
	msX = p.x;
	msY = p.y;
}

void Mouse::hide()
{
	disable();
	++hidden;

	if(hidden == 1)
	{
		if(sprite && screen)
		{
			/*
			 * Copy screen buffer to real screen
			 */

			screen->physBlit(behind, spritePosition, Rect(0,0,0,0));
		}
	}
}

void Mouse::show()
{
	if(hidden == 1)
	{
		if(sprite && screen)
		{
			screen->physUnBlit(behind, spritePosition);
			screen->physMaskBlit(sprite, spritePosition);
		}
	}
	hidden--;
	enable();
}




void Mouse::forceShow()
{
	hidden = 1;
	show();
}

void Mouse::moveTo(Point pt)
{
	set(pt);
	setMousePosition(pixToXMouse(pt.x), pixToYMouse(pt.y));		// Tell the mouse driver where it is.
}

void Mouse::moveMouse(Point pt)
{
	hide();
	set(pt);
	setMousePosition(pixToXMouse(pt.x), pixToYMouse(pt.y));		// Tell the mouse driver where it is.
	show();
}


/*
 * Re-enable the mouse
 */

void Mouse::enable()
{
	if(disabled == 1)
	{
		/*
		 * If the mouse has moved while being disabled, then
		 * update it
		 */

		if( (msX != position.x) || (msY != position.y) )
		{
			position = Point(msX, msY);

			if(!isHidden() && screen && sprite)
				move(position);
		}

	}

	disabled--;
}

/*
 * Copy portion of image to the screen that is using this mouse
 */

void Mouse::blitWithMouse(Image* image, const Point& dest, const Rect& srcRect)
{
	Rect src = srcRect;

	if(src.getW() == 0)
	{
		src.setW(image->getWidth());
		src.setH(image->getHeight());
	}

	/*
	 * Find out if mouse overlaps area
	 */

	Point mouseInImage = spritePosition - dest + src;		// Mouse position relative to image

	Rect overlap = Rect(dest.getX(), dest.getY(), src.getW(), src.getH()) & 
						Rect(spritePosition.x, spritePosition.y, sprite->getWidth(), sprite->getHeight());

	Boolean overlaps = overlap.getW();

#if 0
	Image behind;

	if(overlaps)
		behind.resize(sprite->getWidth(), sprite->getHeight());
#endif

	/*
	 * Disable mouse
	 */

	// hide();
	disable();

	/*
	 * Copy src into behind buffer
	 */

	if(overlaps)
	{
		behind->blit(image, Point(0,0), Rect(mouseInImage, Point(sprite->getWidth(), sprite->getHeight())));
	}

	/*
	 * Copy mouse to src
	 */

	if(overlaps)
		image->maskBlit(sprite, mouseInImage, Rect(0,0, sprite->getWidth(),sprite->getHeight()));

	/*
	 * Copy src to screen
	 */

	screen->physBlit(image, dest, src);

	/*
	 * Copy mouse buffer back to src
	 */

	if(overlaps)
	{
		image->blit(behind, mouseInImage, Rect(0,0,sprite->getWidth(),sprite->getHeight()));
	}

	/*
	 * Enable mouse
	 */

	// show();
	enable();
}

MouseEvent* Mouse::getEvent()
{
	if(events.isEmpty())
		return 0;
	else
		return events.get();
}

MouseEvent* Mouse::waitEvent()
{
	while(events.isEmpty())
		;

	return events.get();
}

void Mouse::clearEvents()
{
	events.clear();
}


MouseEventQueue::MouseEventQueue()
{
	howMany = 16;
	events = new MouseEvent[howMany];
	head = 0;
	tail = 0;
}

MouseEventQueue::~MouseEventQueue()
{
	delete[] events;
}

void MouseEventQueue::clear()
{
	head = 0;
	tail = 0;
}

Boolean MouseEventQueue::isEmpty() const
{
	return (head == tail);
}

/*
 * Add item to event queue
 * Always leaves an empty slot before the tail so that anything
 * processing it can access it without fear of it being reused.
 */

MouseEvent* MouseEventQueue::add(ButtonStatus b, Point p)
{
	static Boolean lock = 0;	// Prevent rentrant adding

	MouseEvent* event = 0;

	if(++lock == 1)
	{
		int newHead = head + 1;
		if(newHead >= howMany)
			newHead = 0;

		int spareHead = newHead + 1;
		if(spareHead >= howMany)
			spareHead = 0;

		if( (newHead != tail) && (spareHead != tail) )
		{
			event = &events[head];

			head = newHead;

			event->buttons = b;
			event->where = p;
		}
	}

 	--lock;

	return event;
}

MouseEvent* MouseEventQueue::get()
{
	if(head != tail)
	{
		MouseEvent* event = &events[tail];

		if(++tail >= howMany)
			tail = 0;

		return event;
	}

	return 0;
}
