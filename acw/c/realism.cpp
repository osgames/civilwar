/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Set Realism Screens
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.20  1994/08/31  15:23:00  Steven_Green
 * Set up the icon sizes properly
 * Made Victory Condition similar to the other types.
 *
 * Revision 1.17  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.11  1994/04/22  22:14:22  Steven_Green
 * Icon Handling tidied up.
 *
 * Revision 1.3  1994/01/24  21:19:13  Steven_Green
 * execute functions only work if button pressed
 *
 * Revision 1.1  1994/01/11  22:29:03  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include "realism.h"
#include "hintstat.h"
#include "layout.h"
#include "system.h"
#include "screen.h"
#include "mouselib.h"
#include "menudata.h"
#include "gameicon.h"
#include "clock.h"
#include "game.h"
#include "connect.h"
#include "log.h"
#include "timer.h"
#include "dialogue.h"
#include "text.h"
#include "colours.h"
#include "language.h"

#if !defined(TESTBATTLE)
#include "campopt.h"
#endif

/*
 * Realism Connection Packet
 */

struct RealismPacket {
	DifficultyType type;
	Realism setting;
};

class RealismScreen : public MenuData, public MenuConnection, public HintStatus {
	Colour hintColour;
protected:
	UBYTE difficultyIcon[DifficultyCount];
public:
	RealismScreen();
	~RealismScreen() { processMessages(True); }

	void processPacket(Packet* pkt);
	void proc();
	void drawIcon();

	void updateSettingIcon(DifficultyType t);

	void showHint(const char* s) { HintStatus::showHint(s); shownHint = True; }
	void showStatus(const char* s) { HintStatus::showStatus(s); }

	void showHintStatus(const char* s);
#ifdef COMMS_OLD
	void checkFinished();
#endif
};

RealismScreen::RealismScreen() :
	MenuConnection("Realism")
{
	memset(difficultyIcon, 0, sizeof(difficultyIcon));
	processMessages(True);
}

void RealismScreen::updateSettingIcon(DifficultyType t)
{
	int iconNumber = difficultyIcon[t];
	if(iconNumber != 0)	// 1st icon is not a realism setting
		icons[iconNumber]->setRedraw();
}

void RealismScreen::showHintStatus(const char* s)
{
	Region bm(machine.screen->getImage(), Rect(10,1,497,8));
	TextWindow wind(&bm, Font_JAME_5);
	wind.setFormat(TW_CV | TW_CH);	// Vertically centre, Horizontal Centre
	wind.setColours(Black);

	bm.fill(hintColour);

	if(s)
		wind.draw(s);

	machine.screen->setUpdate(bm);
}

void RealismScreen::drawIcon()
{
	showFullScreen("art\\screens\\realism.lbm");

	/*
	 * Get the hint colour from the screen
	 */

	hintColour = *machine.screen->getImage()->getAddress(10,1);

	showHint( language( lge_battleRealism ) );
	IconSet::drawIcon();
}


/*
 * Clock Icon
 */

class RealismClockIcon : public ClockIcon {
public:
	RealismClockIcon(IconSet* set, Point p, TimeInfo* t) : ClockIcon(set, p, t, 0) { }
};

/*============================================================================
 * Campaign Realism Screen
 */

class RealismIcon : public Icon {
	RealismScreen* rs;
	DifficultyType type;
public:
	RealismIcon(RealismScreen* parent, DifficultyType t, Point p) :
		Icon(parent, Rect(p, Point(113, 106)))
	{
		rs = parent;
		type = t;
	}

	void execute(Event* event, MenuData* d);
	void drawIcon();
};

void RealismIcon::execute(Event* event, MenuData* data)
{
	if(event->buttons)
	{
#ifdef COMMS_OLD
		if(event->buttons == Mouse::LeftButton)
			game->incDifficulty(type);
		else if(event->buttons == Mouse::RightButton)
			game->decDifficulty(type);

		if(game->connection)
		{
			RealismPacket* pkt = new RealismPacket;
			pkt->type = type;
			pkt->setting = game->getDifficulty(type);
			game->connection->sendData(Connect_RL_Change, (UBYTE*)pkt, sizeof(RealismPacket));
		}
#else
		Realism level = game->getDifficulty(type);
		if(event->buttons == Mouse::LeftButton)
		{
			if(level == Complex3)
				level = Simple;
			else
				INCREMENT(level);
		}
		else if(event->buttons & Mouse::RightButton)
		{
			if(level == Simple)
				level = Complex3;
			else
				DECREMENT(level);
		}
			
		RealismPacket* pkt = new RealismPacket;
		pkt->type = type;
		pkt->setting = level;
		game->connection->sendMessage(Connect_RL_Change, (UBYTE*)pkt, sizeof(RealismPacket));
#endif
		setRedraw();
	}
	if ( event->overIcon )
	{
	 	char a = 0;

		LGE message = lge_resources;

		while ( a++ < type ) 
			INCREMENT( message );

		data->showHint( language( message ) );
	}
}

void RealismIcon::drawIcon()
{
	Realism level = game->getDifficulty(type);

	// Show the actual setting

	SpriteBlock* image = game->sprites->read(level + RS_DiffS1);
	machine.maskBlit(image, getPosition() + Point(0,53));
	image->release();

	// Display the title

	image = game->sprites->read(type + RS_Resource);
	machine.maskBlit(image, getPosition());
	image->release();

	machine.screen->setUpdate(Rect(getPosition(), getSize()));
}


/*
 * Global settings
 */

class GlobalRealismIcon : public Icon {
	RealismScreen* rs;
protected:
	Realism setting;
public:
	GlobalRealismIcon(RealismScreen* parent, Realism v, Point p) :
	 	Icon(parent, Rect(p, Point(123,75)))
	{
		rs = parent;
		setting = v;
	}

	void execute(Event* event, MenuData* d);
	void drawIcon();
};


void GlobalRealismIcon::drawIcon()
{
	SpriteBlock* image = game->sprites->read(setting + RS_Diff1);
	machine.maskBlit(image, getPosition());
	image->release();

	machine.screen->setUpdate(Rect(getPosition(), getSize()));
}

void GlobalRealismIcon::execute(Event* event, MenuData* d)
{
	if(event->buttons)
	{
#ifdef COMMS_OLD
		game->setDifficulty(Diff_Resource, 			setting);
		rs->updateSettingIcon(Diff_Resource);
		game->setDifficulty(Diff_Supply, 			setting);
		rs->updateSettingIcon(Diff_Supply);
		game->setDifficulty(Diff_RegimentTypes, 	setting);
		rs->updateSettingIcon(Diff_RegimentTypes);
		game->setDifficulty(Diff_Facilities, 		setting);
		rs->updateSettingIcon(Diff_Facilities);
		game->setDifficulty(Diff_CommandControl, 	setting);
		rs->updateSettingIcon(Diff_CommandControl);
		game->setDifficulty(Diff_Fatigue, 			setting);
		rs->updateSettingIcon(Diff_Fatigue);
		game->setDifficulty(Diff_TerrainEffects,	setting);
		rs->updateSettingIcon(Diff_TerrainEffects);
		game->setDifficulty(Diff_Morale,			 	setting);
		rs->updateSettingIcon(Diff_Morale);
		game->setDifficulty(Diff_Victory,		 	setting);
		rs->updateSettingIcon(Diff_Victory);

		if(game->connection)
		{
			RealismPacket* pkt = new RealismPacket;
			pkt->type = Diff_Global;
			pkt->setting = setting;
			game->connection->sendData(Connect_RL_Change, (UBYTE*)pkt, sizeof(RealismPacket));
		}

		// d->setUpdate();		// redraw it please
#else
		RealismPacket* pkt = new RealismPacket;
		pkt->type = Diff_Global;
		pkt->setting = setting;
		game->connection->sendMessage(Connect_RL_Change, (UBYTE*)pkt, sizeof(RealismPacket));
#endif
	}
	if ( event->overIcon )
	{
		char a = 0;

		LGE message = lge_simple;

		while ( a++ < setting ) 
			INCREMENT( message );

		d->showHint( language( message ) );
	}
}


/*
 * OK Icon
 */

class RealOkIcon : public BigOkIcon {
public:
	RealOkIcon(IconSet* parent, Point p, Key k1 = 0, Key k2 = 0);

	void execute(Event* event, MenuData* data);
};

RealOkIcon::RealOkIcon(IconSet* parent, Point p, Key k1, Key k2) :
		BigOkIcon(parent, p, k1, k2)
{
}


void RealOkIcon::execute(Event* event, MenuData* data)
{
	if(event->buttons)
	{
		/*
		 * Are you happy with selections?
		 */

		if(dialAlert(data, machine.mouse->getPosition(), language( lge_Happy ), language( lge_yesno ) ) == 0)
#ifdef COMMS_OLD
			data->setFinish(MenuData::MenuOK);
#else
		{
			FinishPKT *packet = new FinishPKT;
			packet->how = MenuData::MenuOK;

		  	game->connection->sendMessage(Connect_Finish, (UBYTE*)packet, sizeof(FinishPKT));
		}
#endif
	}
	if(event->overIcon)
		data->showHint( language( lge_play ) );

}

/*
 * Procedure to call every game frame
 */

void RealismScreen::proc()
{
	if(game->connection)
	{
#ifdef OLD_COMMS
		multiPlayer();
		checkFinished();
#else
		processMessages(False);
#endif
	}
}


#if !defined(TESTBATTLE)
/*
 * Campaign Realism screen
 */

class CampaignRealism : public RealismScreen {
	TimeInfo timeInfo;
public:
	CampaignRealism();

	void updateTime() { timeInfo.setRealTime(); }
};

CampaignRealism::CampaignRealism()
{
	icons = new IconList[22];

	/*
	 * Standard Icons along the bottom
	 */

	icons[0] = new MainMenuIcon		(this, Point(MAIN_ICON_X1,MAIN_ICON_Y));
	icons[1] = new RealismClockIcon	(this, Point(MAIN_ICON_X2,MAIN_ICON_Y), &timeInfo);
	icons[2] = new MiniSoundIcon			(this, Point(MAIN_ICON_X3,MAIN_ICON_Y+53));
	icons[3] = new CampaignOptionsIcon			(this, Point(MAIN_ICON_X3,MAIN_ICON_Y));
	icons[4] = new DatabaseIcon		(this, Point(MAIN_ICON_X4,MAIN_ICON_Y));

	icons[5] = new RealOkIcon			(this, Point(MAIN_ICON_X5,MAIN_ICON_Y));
	icons[6] = new QuitIcon(this);

	/*
	 * Global Realism Icons
	 */

	icons[ 7] = new GlobalRealismIcon(this, Simple,   Point(517,  1));
	icons[ 8] = new GlobalRealismIcon(this, Average,  Point(517, 75));
	icons[ 9] = new GlobalRealismIcon(this, Complex1, Point(517,149));
	icons[10] = new GlobalRealismIcon(this, Complex2, Point(517,223));
	icons[11] = new GlobalRealismIcon(this, Complex3, Point(517,297));

	/*
	 * Individual settings
	 */

	icons[12] = new RealismIcon(this, Diff_Resource, 			Point( 18, 13));
	difficultyIcon[Diff_Resource] = 12;
	icons[13] = new RealismIcon(this, Diff_Supply, 				Point(206, 13));
	difficultyIcon[Diff_Supply] = 13;
	icons[14] = new RealismIcon(this, Diff_RegimentTypes, 	Point(386, 13));
	difficultyIcon[Diff_RegimentTypes] = 14;
	icons[15] = new RealismIcon(this, Diff_Facilities, 		Point( 18,133));
	difficultyIcon[Diff_Facilities] = 15;
	icons[16] = new RealismIcon(this, Diff_CommandControl,	Point(206,133));
	difficultyIcon[Diff_CommandControl] = 16;
	icons[17] = new RealismIcon(this, Diff_Fatigue, 			Point(386,133));
	difficultyIcon[Diff_Fatigue] = 17;
	icons[18] = new RealismIcon(this, Diff_TerrainEffects, 	Point( 18,253));
	difficultyIcon[Diff_TerrainEffects] = 18;
	icons[19] = new RealismIcon(this, Diff_Morale,				Point(206,253));
	difficultyIcon[Diff_Morale] = 19;
	icons[20] = new RealismIcon(this, Diff_Victory,				Point(386,253));
	difficultyIcon[Diff_Victory] = 20;

	icons[21] = 0;
}

int campaignRealism()
{
	// game->getConfigDifficulty();

	CampaignRealism menuData;

	// showFullScreen("art\\screens\\realism.lbm");

#ifdef OLD_COMMS
	menuData.waitSync();
#endif

	while(!menuData.isFinished())
	{
		/*
		 * Display/update screen if required
		 */

		menuData.updateTime();

		/*
		 * Get icon/function
		 */

		menuData.process();
#if 0
		if(game->connection)
			menuData.checkFinished();
#endif
	}
	
	game->setConfigDifficulty();

#ifdef OLD_COMMS
	menuData.waitSync();
#endif

	return menuData.getMode();
}

#endif		// TESTBATTLE

/*============================================================================
 * Battle Realism Screen
 */

class BattleRealism : public RealismScreen {
	TimeInfo timeInfo;
public:
	BattleRealism();
	void updateTime() { timeInfo.setRealTime(); }
};

BattleRealism::BattleRealism()
{
	icons = new IconList[18];

	/*
	 * Standard Icons along the bottom
	 */

	icons[0] = new MainMenuIcon		(this, Point(MAIN_ICON_X1,MAIN_ICON_Y));
	icons[1] = new RealismClockIcon	(this, Point(MAIN_ICON_X2,MAIN_ICON_Y), &timeInfo);
	icons[2] = new SoundIcon			(this, Point(MAIN_ICON_X3,MAIN_ICON_Y));
	icons[3] = new DatabaseIcon		(this, Point(MAIN_ICON_X4,MAIN_ICON_Y));

	icons[4] = new RealOkIcon	(this, Point(MAIN_ICON_X5, MAIN_ICON_Y));
	icons[5] = new QuitIcon(this);

	/*
	 * Global Realism Icons
	 */

	icons[ 6] = new GlobalRealismIcon(this, Simple,   Point(517,  1));
	icons[ 7] = new GlobalRealismIcon(this, Average,  Point(517, 75));
	icons[ 8] = new GlobalRealismIcon(this, Complex1, Point(517,149));
	icons[ 9] = new GlobalRealismIcon(this, Complex2, Point(517,223));
	icons[10] = new GlobalRealismIcon(this, Complex3, Point(517,297));

	/*
	 * Individual settings
	 */

	icons[11] = new RealismIcon(this, Diff_Supply, 				Point(206, 13));
	difficultyIcon[Diff_Supply] = 11;
	icons[12] = new RealismIcon(this, Diff_RegimentTypes, 	Point(386, 13));
	difficultyIcon[Diff_RegimentTypes] = 12;
	icons[13] = new RealismIcon(this, Diff_CommandControl,	Point(206,133));
	difficultyIcon[Diff_CommandControl] = 13;
	icons[14] = new RealismIcon(this, Diff_Fatigue, 			Point(386,133));
	difficultyIcon[Diff_Fatigue] = 14;
	icons[15] = new RealismIcon(this, Diff_TerrainEffects, 	Point( 18,253));
	difficultyIcon[Diff_TerrainEffects] = 15;
	icons[16] = new RealismIcon(this, Diff_Morale,				Point(206,253));
	difficultyIcon[Diff_Morale] = 16;

	icons[17] = 0;
}

int battleRealism()
{
	// game->getConfigDifficulty();
	BattleRealism menuData;
	// menuData.proc = realismProc;

	// showFullScreen("art\\screens\\realism.lbm");

#ifdef OLD_COMMS
	menuData.waitSync();
#endif

	while(!menuData.isFinished())
	{
		/*
		 * Display/update screen if required
		 */

		menuData.updateTime();

		/*
		 * Get icon/function
		 */

		menuData.process();

#if 0
		if(game->connection)
			menuData.checkFinished();
#endif
	}

	game->setConfigDifficulty();

#ifdef OLD_COMMS
	menuData.waitSync();
#endif

	return menuData.getMode();
}

void RealismScreen::processPacket(Packet* packet)
{
#ifdef DEBUG
	char buffer[400];
#endif

	switch(packet->type)
	{
	case Connect_RL_Change:
		{	
			RealismPacket* pkt = (RealismPacket*)packet->data;
#ifdef DEBUG
			sprintf(buffer, "Realism setting %d changed to %d", pkt->type, pkt->setting);
			showStatus(buffer);
#else
			showStatus( language( lge_RemotechangedRealism ) );
#endif

			if(pkt->type == Diff_Global)
			{
				for(int i = 0; i < DifficultyCount; i++)
				{
					game->setDifficulty(DifficultyType(i), pkt->setting);
					updateSettingIcon(DifficultyType(i));
				}
			}
			else
			{
				game->setDifficulty(pkt->type, pkt->setting);
				updateSettingIcon(pkt->type);
			}
		}
		break;

	case Connect_Finish:
		if(!isFinished())
		{
			if(packet->sender == RID_Local)
			{
				if(waitRemoteReply())
				{
					FinishPKT* pkt = (FinishPKT*)packet->data;
					setFinish(pkt->how);
				}
				else
					dialAlert(this, language( lge_remoteNoleave ), language( lge_OK ) );
			}
			else
			{
				FinishPKT* pkt = (FinishPKT*)packet->data;

				const char* text;
				const char* buttons;

				switch(pkt->how)
				{
				default:
				case MenuOK:
					text = language(lge_RemoteStart);
					buttons = language(lge_startcancel);
					break;
				case MenuMainMenu:
					text = language(lge_RemoteGoMM);
					buttons = language(lge_mmCancel);
					break;
				}
			
				if(askRemoteReply(text, buttons))
					setFinish(pkt->how);
			}
#ifdef OLD_COMMS
			{
				FinishPKT* pkt = (FinishPKT*)packet->data;

				setRemoteWantFinish(pkt->how);
			}
#endif
		}
		break;

#ifdef OLD_COMMS
	case Connect_RL_StartGame:
		setRemoteAnswer(True);
		break;

	case Connect_RL_NoStartGame:
		setRemoteAnswer(False);
		break;
#endif

	default:
#ifdef DEBUG
		netLog.printf("Unknown packet received, type: %d, length:%d", (int) packet->type, (int)packet->length);
#endif
		break;
	}
}

#ifdef OLD_COMMS
void RealismScreen::checkFinished()
{
	/*
	 * If we tried to exit, wait for the remote player to make up his mind
	 */

	if(isFinished() && game->connection && !hasRemoteAnswered())
	{
		if(getMode() != MenuQuit)
		{
			FinishPKT *packet = new FinishPKT;
			packet->how = getMode();

		  	game->connection->sendData(Connect_Finish, (UBYTE*)packet, sizeof(FinishPKT));

			if(!waitRemoteReply())
			{
				clearFinish();
				dialAlert(this, language( lge_remoteNoleave ), language( lge_OK ));
			}
			else
				setFinish(getMode());
		}
	}

	if(getRemoteWantFinish())
	{
		clearRemoteWantFinish();

		int result;

		switch(getMode())
		{
		case MenuOK:
			// result = dialAlert(this, language( lge_RemoteStart ) , language( lge_startcancel ) );
			result = dialAlert(0, language( lge_RemoteStart ) , language( lge_startcancel ) );
			break;
		case MenuMainMenu:
			// result = dialAlert(this, language( lge_RemoteGoMM ), language( lge_mmCancel ) );
			result = dialAlert(0, language( lge_RemoteGoMM ), language( lge_mmCancel ) );
			break;
		}

		switch(result)
		{
		case 0:	// Start Game
			if(game->connection)
				game->connection->sendData(Connect_RL_StartGame, 0, 0);
			setFinish(getMode());
			setRemoteAnswer(True);
			break;

		case 1:	// Cancel
		default:
			if(game->connection)
				game->connection->sendData(Connect_RL_NoStartGame, 0, 0);
			clearRemoteAnswer();
			break;
		}
		// game->connection->sync();
		multiPlayer();
	}
}


#endif
