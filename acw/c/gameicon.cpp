/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	General purpose Icons used within game
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include "gameicon.h"
#include "menudata.h"
#include "dialogue.h"
#include "mouselib.h"
#include "memptr.h"
#include "filesel.h"
#include "language.h"
#include "config.h"
#include "gamecfg.h"
#include "timer.h"
#include "colours.h"

// #ifndef TESTBATTLE
#include "connect.h"
// #endif

#include "strutil.h"
#include "game.h"

#if !defined(CAMPEDIT) && !defined(BATEDIT)
 #if !defined(TESTBATTLE)
  #include "database.h"
 #endif
#include "campmusi.h"
#include "sndlib.h"
#endif

#include "system.h"
#include "screen.h"

#if !defined(CAMPEDIT) && !defined(BATEDIT)
void QuitIcon::execute(Event* event, MenuData* data)
{
	if(event->buttons)
	{
#if !defined(COMPUSA_DEMO)
		/*
		 * Set menudata to 0, so that game is paused.
		 */

		timer->pause();
		int result = dialAlert(0, machine.mouse->getPosition(), language( lge_QuitToDOS ), language(lge_quitCancel));
		timer->unPause();

		// if(dialAlert(data, machine.mouse->getPosition(), language( lge_QuitToDOS ), language( lge_quitCancel )) == 0)

		if(result == 0)
#endif
		{
			game->disconnect();
			throw GotoQuitGame();
		}
	}
}


#endif		// CAMPEDIT

void doOkIcon(Event* event, MenuData* data)
{
	if(event->buttons)
	{

		/*
		 * Are you happy with selections?
		 */

		if(dialAlert(data, machine.mouse->getPosition(), language( lge_Happy ), language( lge_yesno ) ) == 0)
#if defined(BATEDIT) || defined(CAMPEDIT)
			data->setFinish(MenuData::MenuOK);
#else
		{
			FinishPKT *packet = new FinishPKT;
			packet->how = MenuData::MenuOK;

		  	game->connection->sendMessage(Connect_Finish, (UBYTE*)packet, sizeof(FinishPKT));
		} 
#endif
	}
}

void MiniOkIcon::execute(Event* event, MenuData* data)
{
	if(event->overIcon)
		data->showHint( language( lge_cont ) );

	doOkIcon(event, data);
}

void BigOkIcon::execute(Event* event, MenuData* data)
{
	if(event->overIcon)
		data->showHint( language( lge_play ) );

	doOkIcon(event, data);
}


#if !defined(CAMPEDIT) && !defined(BATEDIT)

void SaveIcon::execute(Event* event, MenuData* data)
{
	if(event->overIcon)
		data->showHint( language( lge_save ) );

	if(event->buttons)
#if defined(COMPUSA_DEMO)
	{
		dialAlert(0, "Save Game Feature\ris not available\rin this demo version", "OK");
	}
#else
		game->connection->sendMessage(Connect_WaitSave, 0, 0);
#endif
}


MainMenuIcon::MainMenuIcon(IconSet* set, Point p, Key k1, Key k2) :
	MenuIcon(set, MM_MainMenu, Rect(p, Point(MAIN_ICON_W,MAIN_ICON_H)), k1, k2)
{
	if(isLanguage("GER"))
		setIcon(MM_MainMenuGerman);
}

void MainMenuIcon::execute(Event* event, MenuData* data)
{
	if(event->overIcon)
		data->showHint( language( lge_quitmm ) );

	if(event->buttons)
	{
		switch(dialAlert(data, getPosition() + getSize() / 2,
			language( lge_abort ),
			language( lge_mmCancel ) ))
		{
		case 0:		// Main Menu
#ifdef COMMS_OLD
	  		data->setFinish(MenuData::MenuMainMenu);
#else
			{
				FinishPKT *packet = new FinishPKT;
				packet->how = MenuData::MenuMainMenu;

		  		game->connection->sendMessage(Connect_Finish, (UBYTE*)packet, sizeof(FinishPKT));
			}
#endif
			break;
		default:	// Cancel
			break;
		}
	}
}

void SoundMusicIcon::execute(Event* event, MenuData* data)
{
	if(event->overIcon)
		data->showHint( language( lge_musicSound ) );

	if(event->buttons)
	{
		const char** menus = new const char*[7];

		Boolean finished = False;
		MemPtr<char> musicBuffer(50);
		MemPtr<char> soundBuffer(50);

		while(!finished)
		{
			const char** mPtr = menus;

			sprintf(musicBuffer, "%s: %s", language(lge_Music), language(game->musicInGame ? lge_On: lge_Off));
			sprintf(soundBuffer, "%s: %s", language(lge_FX), language(game->soundInGame ? lge_On: lge_Off));

			*mPtr++ = musicBuffer;
			*mPtr++ = soundBuffer;
			*mPtr++ = 0;

			switch(chooseFromList(data, 0, menus, language(lge_OK), getPosition() + getSize() / 2))
			{
			case 1:		// Music
				game->musicInGame = !game->musicInGame;
				if(!game->musicInGame)
					game->gameMusic->disable();
				if(!game->musicInGame)
					game->gameMusic->enable();
				{
					ConfigFile config(configFileName);
					config.setBool(cfgMusicID, game->musicInGame);
				}
				break;
			case 2:		// Sound
				game->soundInGame = !game->soundInGame;
				if(!game->soundInGame)
					soundManager.stopAll();
				{
					ConfigFile config(configFileName);
					config.setBool(cfgSoundID, game->soundInGame);
				}
				break;
			default:
				finished = True;
				break;
			}
		}
	}
}


void DosIcon::execute(Event* event, MenuData* data)
{
	if(event->overIcon)
		data->showHint( language( lge_quitProg ) );

	if(event->buttons)
	{
		switch(dialAlert(data, getPosition() + getSize() / 2,
			language( lge_Doquit ),
			language( lge_quitCancel )))
		{
		case 0:		// Quit
			data->setFinish(MenuData::MenuQuit);
			game->setQuit();
			game->disconnect();
			break;
		default:	// Cancel
			break;
		}
	}
}

#endif

#if !defined(CAMPEDIT) && !defined(BATEDIT)

/*
 * Database Icon
 */


void DatabaseIcon::execute(Event* event, MenuData* d)
{
#if !defined(TESTCAMP) && !defined(TESTBATTLE)
	// if(game->playerCount == 1)
	if(game->getRemoteSide() == SIDE_None)
	{
		if(event->overIcon)
			d->showHint( language( lge_ViewDB ) );

		if(event->buttons)
		{
#ifdef FOURMEG
			throw QuitDatabase();
#else
			doDatabase();
			d->setUpdate();	// Redraw the screen
#endif
		}
	}
#endif
}

void DatabaseIcon::drawIcon()
{
	MenuIcon::drawIcon();

	// if(game->playerCount != 1)
	if(game->getRemoteSide() != SIDE_None)
		machine.screen->getBitmap()->greyOut(Rect(getPosition(), getSize()), Black);
}
#endif
