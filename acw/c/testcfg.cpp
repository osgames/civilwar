/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Configuration File tester
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "config.h"

void main()
{
	ConfigFile config("test.cfg");

	config.set("Val1", "10");
	config.set("Val2", "20");
	config.set("Val3", 0);
	config.set("Val4", "40");
	config.set("Val2", "Twenty");

	printf("val1=%s\n", config.get("Val1"));
	printf("val2=%s\n", config.get("Val2"));
	printf("val3=%s\n", config.get("Val3"));
	printf("val4=%s\n", config.get("Val4"));
}
