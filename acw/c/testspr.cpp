/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Sprite Library Test Program
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.4  1994/07/11  14:26:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/01/05  15:57:26  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/12/21  00:31:04  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/12/16  22:19:35  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


#include <iostream.h>
#include <conio.h>
#include <stdlib.h>

#include "sprlib.h"
#include "ilbm.h"
#include "image.h"
#include "palette.h"
#include "vesa.h"

/*
 * Test program usage:
 *		testspr spritefile spritenumber
 *
 * Displays the specified sprite on screen
 */

void main(int argc, char *argv[])
{
 	try
	{
		if(argc != 3)
		{
			cout << "Usage: testspr filename sprnum" << endl;
			return;
		}

		SpriteLibrary slib(argv[1]);

		Palette myPalette;
		SpriteBlock* spriteImage;

		SpriteIndex n = atoi(argv[2]);

		spriteImage = slib.read(n);

		if(readILBM("art\\palette", 0, &myPalette))
			cout << "readILBM returned an error" << endl;
		else
		{
			Vesa screen(0x101, 0x101);

			myPalette.set();

			screen.maskBlit(*spriteImage);
			slib.release(spriteImage);

			while(!kbhit());
		}
	}
	catch(SpriteLibrary::SpriteLibError e)
	{
		cout << "Sprite Library Error: " << e.get() << endl;
	}
	catch(Vesa::VesaError e)
	{
		cout << "Vesa Error: " << e.get() << endl;
	}
	catch(GeneralError e)
	{
		cout << "Untrapped Error: ";
		if(e.get())
			cout << e.get();
		else
			cout << "No description";
		cout << endl;
	}
	catch(...)
	{
		cout << "Caught some kind of error" << endl;
	}
}
