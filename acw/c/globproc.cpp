/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Global Procedures Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "globproc.h"

/*
 * Global Procedure List Implementation
 */

GlobalProcList globalProcedures;		// Global List

GlobalProcList::GlobalProcList()
{
	entry = 0;
	interlock = False;
}

GlobalProcList::~GlobalProcList()
{
	while(entry)
	{
		GlobalProc* p = entry;
		entry = p->next;
		delete p;
	}
}

void GlobalProcList::proc()
{
	if(++interlock == 1)
	{
		GlobalProc* p = entry;
		GlobalProc* last = 0;

		while(p)
		{
			GlobalProc* p1 = p->next;
			if(p->proc())
			{
				if(last)
					last->next = p1;
				else
					entry = p1;

				delete p;
			}
			last = p;
			p = p1;
		}
	}
	interlock--;
}

void GlobalProcList::add(GlobalProc* p)
{
	p->next = entry;
	entry = p;
}

void GlobalProcList::remove(GlobalProc* p)
{
	GlobalProc* p1 = entry;
	GlobalProc* last = 0;
	while(p1)
	{
		if(p1 == p)
		{
			if(last)
				last->next = p->next;
			else
				entry = p->next;
			return;
		}
		last = p1;
		p1 = p1->next;
	}
}


