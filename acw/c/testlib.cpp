/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Example of how to use Dagger PC Library
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/07/28  18:57:04  Steven_Green
 * Modification to debugMode
 *
 * Revision 1.2  1994/07/04  13:26:52  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/06/29  21:38:52  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "daglib.h"		// This includes everything needed for library
#ifdef DEBUG
#include "memlog.h"
#include "memalloc.h"
#endif
#include "gamelib.h"

#ifdef DEBUG
class TestMemory {
public:
	TestMemory() { }
	~TestMemory() { showPoolInfo(); }
};

#pragma initialise before library
TestMemory memTest;		// force display of remaining memory in pool at end
#endif


/*
 * Example of using dialogue boxes
 * see dialogue.h for more info
 */

void testDialogues()
{
	/*
	 * Example of popup text boxes.
	 *
	 * This is included in its own braces, so that the box is
	 * destructed when it gets to the closing brace.
	 */

	{
		/*
		 * Display a popup text window and wait for a mouse click
		 */

		PopupText popup("Press any mouse button\rto clear this message");
		machine.screen->update();
		machine.mouse->waitEvent();

		/*
		 * Use the same box, but put some different text in.
		 */

		popup.showText("And again!");
		machine.screen->update();
		machine.mouse->waitEvent();
		// cleared automatically in clearup
	}

	/*
	 * Example of a dialogue box.
	 * This waits for the user to click one of the boxes.
	 * The 1st parameter is a function to be called every loop,
	 * while waiting for the user to click.
	 */

	dialAlert(0, "This is a message", "OK|Cancel");

	/*
	 * Example of Text Input.
	 *
	 * What the user types ends up in input[]
	 */

	const int bufferSize = 16;
	char input[bufferSize+1];
	strcpy(input, "Default");

	dialInput(0, "Type some text\rinto the box", "Done|Ignore|Quit", input, bufferSize);
}

/*
 * Grapics Testing
 */

void testGraphics()
{
	/*
	 * Set up a region on the backup screen
	 * It has coordinates of 50,100 and size 200,150
	 */

	Region region(machine.screen->getImage(), Rect(50, 100, 200, 150));

	/*
	 * Fill it in with some colour
	 */

	region.fill(Yellow);

	/*
	 * Draw some boxes and frames
	 */

	region.frame(10, 10, 100, 100, Black);

	for(int i = 0; i < 50; i++)
		region.box(SDimension(i+10), SDimension(i*2), 20, 30, Colour(i));

	/*
	 * Load a sprite and display it
	 */

	SpriteLibrary sprLib("art\\game");
	sprLib.drawSprite(&region, Point(30, 40), MM_Load);

	/*
	 * Do some text
	 */

	TextWindow window(&region, Font_EMFL32);
	window.setFormat(True, True);		// Center in region
	window.setWrap(True);
	window.setColours(Red);
	window.setBold(True);
	window.draw("Hello, This piece of text should probably wrap!");

	/*
	 * Tell the system that we have changed the region's rectangle
	 */

	machine.screen->setUpdate(region);

	/*
	 * Now tell the system to display the window and wait for mouse press
	 */

 	machine.screen->update();
 	machine.mouse->waitEvent();
}

/*=================================================================
 * Test Icon
 */

/*
 * Define an Icon class called MyIcon
 *
 * This is derived from the Icon class (see icon.h).
 *
 * execute() and drawIcon() are overloaded virtual functions
 *
 * The colour variables are just an example of how an icon might work.
 * This icon will change its colour when clicked with the left mouse
 * button, and exit the program when right clicked.
 */

 
class MyIcon : public Icon {
	Colour backColour;
	Colour crossColour;
public:
	MyIcon(IconSet* parent, Point where);

	void execute(Event* event, MenuData* d);
	void drawIcon();
};

/*
 * Constructor for MyIcon
 *
 * It just makes sure the initial colours are set to something
 *
 * It also calls the Icon constructor with its screen rectangle.
 */

MyIcon::MyIcon(IconSet* parent, Point where) :
	Icon(parent, Rect(where, Point(64,64)))
{
	backColour = 0;
	crossColour = 1;
}

/*
 * This is called whenever anything happens to the icon.
 * see icon.h for info about the Event structure.
 *
 * What we are doing here is:
 *   IF left button pressed, change the background colour
 *   IF right button pressed, quit the icon tester
 *   IF the mouse is over the icon then change the cross colour
 */

void MyIcon::execute(Event* event, MenuData* d)
{
	if(event->buttons & Mouse::LeftButton)
	{
		backColour++;
		setRedraw(); 			// Ask for it to be redrawn!
	}
	else if(event->buttons & Mouse::RightButton)
		d->setFinish(MenuData::MenuOK);
	else if(event->overIcon)
	{
		crossColour++;
		setRedraw();			// Ask for it to be redrawn!
	}
}

/*
 * This gets called to draw the icon
 *
 * getPosition() tells it where it should be on the full screen
 * getSize() returns its size.
 *
 */

void MyIcon::drawIcon()
{
	/*
	 * Make up a Region on the backup screen matching the icon's
	 * location and size.
	 */

	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));

	/*
	 * fill the region in Green
	 */

	bm.fill(Green);

	/*
	 * Draw a filled box in the current backColour 2 pixels inside the region
	 */

	bm.box(2, 2, bm.getW()-4, bm.getH()-4, backColour);

	/*
	 * Draw a diagonal cross on top of the icon in crossColour
	 */

	bm.line( Point(0,0),            Point(bm.getW()-1, bm.getH()-1), crossColour);
	bm.line( Point(bm.getW()-1, 0), Point(0, bm.getH()-1),           crossColour);

	/*
	 * Tell the screen manager that this bit of screen has been
	 * updated.
	 *
	 * It will get updated during menudata.process()
	 */

	machine.screen->setUpdate(bm);

}

/*
 * Define a control class for the testIcon function
 *
 * It is derived from MenuData (see menudata.h), which is
 * a handy class for handling an icon based screen, which
 * automatically looks after the mouse clicking, icon management
 * and screen updating.
 */

 
class TestIconControl : public MenuData {
public:
	TestIconControl();
};

/*
 * Set up the TestIcon screen
 *
 * Here there is only one icon, but you could easily set up 
 * as many as you like.
 */

TestIconControl::TestIconControl()
{
	icons = new IconList[2];		// Space for 1 Icon + NULL

	icons[0] = new MyIcon(this, Point(200,200));
	icons[1] = 0;
}

/*
 * Here is the actual test routine
 */

void testIcon()
{
	TestIconControl menuData;

	while(!menuData.isFinished())
	{
		menuData.process();
	}
}

/*
 * Main Function
 */

void main()
{
	/*
	 * Use try/catch to make use of exceptions
	 */

	try
	{
		/*
		 * Set up the system.
		 *
		 * This sets up the video mode, mouse, timer, fonts
		 *
		 * Things will automatically be restored when the
		 * program finishes and machine's destructor is called.
		 */

		machine.init();
#ifdef DEBUG
		memLog->setMode("memory.log", MemoryLog::Summary);
#endif

		/*
		 * Display a full screen picture
		 * Note that this only puts it in the backup screen and
		 * so a call to update is required to actually display it.
		 */

		showFullScreen("art\\screens\\playscrn.lbm");
 		machine.screen->update();

		/*
		 * Simple Icon Test
		 */

		testIcon();

		/*
		 * Do some dialogue box handling
		 */

		testDialogues();

		/*
		 * Do some graphics things
		 */

		testGraphics();

	}
 	catch(GeneralError e)
 	{
		machine.reset();
		cout << "Untrapped Error: ";
		if(e.get())
			cout << e.get();
		else
			cout << "No description";
		cout << endl;
 	}
 	catch(...)
 	{
		machine.reset();
 		cout << "Caught some kind of error" << endl;
 	}

#ifdef DEBUG
	machine.reset();
	showPoolInfo();
#endif
}
