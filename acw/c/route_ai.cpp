/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*			  
 *----------------------------------------------------------------------
 * $Id ROUTE_AI 1.0 Chris Wall $
 *----------------------------------------------------------------------
 *
 *	Class to control the route of the AI's armies on a Battlefield
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "route_ai.h"
#include <iostream.h>


Route_AI::Route_AI( const int number )
{
	if ( number == 0 ) 
	{ 
		MaxL = 0; 
		return;
	}

	clickpoints = new Location[ number ];
	local = 0;
	MaxL = number;
}

Route_AI::~Route_AI()
{
	if ( MaxL == 0 ) return;
	delete clickpoints;
}

void Route_AI::addLocation( const location& dest )
{
	if ( local < MaxL ) clickpoints[ local++ ] = dest;
	else
	{
		cout << "Error in route!!";
	}
}

Location* Route_AI::getLocation()
{
 	return clickpoints[ local-- ];
}
