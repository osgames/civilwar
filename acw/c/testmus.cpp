/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Music/Sound Testing Program
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *	 Revision 1.1  1994/08/22  16:02:09  Steven_Green
 *  Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <conio.h>
#include <ctype.h>
#include <stdlib.h>
#include "daglib.h"
#include "sound.h"
#include "strutil.h"
#include "dsmi.h"
#include "nosound.h"
#include "ilbm.h"
#include "colours.h"
#include "sndconfi.h"
#include "language.h"

/*=============================================================
 * User Interface to select sound card and parameters
 */

class SoundSetup : public MenuData {
public:
	SoundSetup(Sound* snd);
	~SoundSetup();
};

static struct SoundCardType {
	const char* name;
	char id;
	char stereo;
	char sampleSize;
	ushort minRate;
	ushort maxRate;

} cardNames[] = {
	{ "None",							ID_NOSOUND, 0, 1,    0,     0 },
	{ "Gravis Ultra Sound",			ID_GUS,		1, 1,19293, 44100 },
	{ "Sound Blaster",				ID_SB,		0, 1, 3000, 21000 },
	{ "Sound Blaster Pro",			ID_SBPRO,   1, 1, 3000, 22050 },
	{ "Sound Blaster 16",			ID_SB16,		1, 2, 4000, 44100 },
	{ "Pro Audio Spectrum",			ID_PAS,		1, 1, 4000, 44100 },
	{ "Pro Audio Spectrum+",		ID_PASPLUS, 1, 1, 4000, 44100 },
	{ "Pro Audio Spectrum 16",		ID_PAS16,   1, 2, 4000, 44100 },
	{ "Windows Sound System",		ID_WSS,     1, 2, 5000, 48000 },
#if 0
	{ "Aria Sound Card",				ID_ARIA,    1, 1, 4000, 44100 },
#endif
	{ 0, ID_NOSOUND }
};

#if 0
static const char* titles[] = {
	"Sound Card:",
	"I/O:",
	"IRQ:",
	"DMA:",
	"Rate:",
	"Quality:",
	0
};

#else	// Get them from string file

static char* titles[] = {
	0,
	0,
	0,
	0,
	0,
	0,
	0
};

static const LGE titleStrings[] = {
	lge_soundCard,
	lge_baseIO,
	lge_soundIRQ,
	lge_soundDMA,
	lge_rate,
	lge_quality,
	lge_None,
};

#endif

static void autoDetectSound()
{
	SOUNDCARD* scard = sound.getSoundInfo();

	/*
	 * 1st Try to autodetect the card
	 */

	memset(scard, 0, sizeof(SOUNDCARD));

	Boolean got;

	got = detectGUS(scard);
	if(got)
		got = detectPAS(scard);
	if(got)
		got = detectSB(scard);
	if(got)
	{
		memset(scard, 0, sizeof(SOUNDCARD));
		scard->ID = ID_NOSOUND;
		strcpy(scard->name, language(lge_None));
	}

	sound.mcp.options |= MCP_QUALITY;
	sound.rate = 21000;
}

static void getSoundConfiguration()
{
	SoundSetup dial(&sound);

	dial.redrawIcon();
	while(!dial.isFinished())
		dial.process();
}

#define Font_Text Font_EMMA14
#define Font_Title Font_EMFL15

/*
 * The outer box
 */

class SoundOuter : public IconSet {
	SDimension infoY;
	SDimension infoH;
	SDimension midX;
public:
	SoundOuter(IconSet* parent, const Rect& r, SDimension yi, SDimension hi, SDimension x);
	void drawIcon();
};

SoundOuter::SoundOuter(IconSet* parent, const Rect& r, SDimension yi, SDimension hi, SDimension x) :
	IconSet(parent, r)
{
	infoY = yi;
	infoH = hi;
	midX = x;
}

void SoundOuter::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	bm.fill(White);
	bm.frame(0,0,getW(),getH(), Yellow, Green);

	/*
	 * Draw the title
	 */

	bm.HLine(1, infoY, getW() - 2, Green);
	bm.HLine(1, infoY + infoH, getW() - 2, Green);

	Region r = bm;
	r.setClip(r.getX() + 1, r.getY() + 1, r.getW() - 2, infoY - 2);
	TextWindow textWind(&r, Font_Title);
	textWind.setFormat(True, True);
	textWind.setColours(Black);
	textWind.draw( language( lge_SoundSetup ) );

	machine.screen->setUpdate(bm);

	IconSet::drawIcon();
}

class SoundSetupIcon : public Icon {
	SDimension midX;
	int which;
protected:
	Sound* sound;
public:
	SoundSetupIcon(IconSet* parent, const Rect& r, SDimension mx, int w, Sound* snd);

	void drawIcon();

	virtual const char* getInfo(char* buffer) { buffer[0] = 0; return buffer; }
};

SoundSetupIcon::SoundSetupIcon(IconSet* parent, const Rect& r, SDimension mx, int w, Sound* snd) :
	Icon(parent, r)
{
	midX = mx;
	which = w;
	sound = snd;
}

void SoundSetupIcon::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	bm.frame(0,0,getW(),getH(), Yellow, DGrey);
	bm.box(midX,1,getW()-midX-1,getH()-2,Green);
	bm.box(0,1,midX-1,getH()-2,LGreen);
	TextWindow textWind(&bm, Font_Text);

	Font* f = fontSet(Font_Text);
	textWind.setPosition(Point(midX - 2 - f->getWidth(titles[which]), 2));
	textWind.setColours(Black);
	textWind.draw(titles[which]);

	textWind.setPosition(Point(midX + 1, 2));
	textWind.setColours(White);
	char buffer[40];
	textWind.draw(getInfo(buffer));

	machine.screen->setUpdate(bm);
}


class GetCardType : public SoundSetupIcon {
public:
	GetCardType(IconSet* parent, const Rect& r, SDimension mx, Sound* sound);
	const char* getInfo(char* buffer);
	void execute(Event* event, MenuData* data);
};

GetCardType::GetCardType(IconSet* parent, const Rect& r, SDimension mx, Sound* snd) :
	SoundSetupIcon(parent, r, mx, 0, snd)
{
}

const char* GetCardType::GetCardType::getInfo(char* buffer)
{
	return sound->scard.name;
}


void GetCardType::execute(Event* event, MenuData* data)
{
	if(event->buttons)
	{
		int choiceCount = 0;
		SoundCardType* sc = cardNames;
		while(sc++->name)
			choiceCount++;

		MenuChoice* choices = new MenuChoice[choiceCount + 3];
		MenuChoice* ch = choices;
		sc = cardNames;

		ch->text = language( lge_ChooseSoundCard );
		ch->value = 0;
		ch++;

		ch->text = "-";
		ch->value = 0;
		ch++;

		int i = 1;
		while(sc->name)
		{
			ch->text = sc->name;
			ch->value = i;

			i++;
			ch++;
			sc++;
		}
		ch->text = 0;

		i = menuSelect(0, choices) - 1;

		if( (i >= 0) && (i < choiceCount) )
		{
			sc = &cardNames[i];

			sound->scard.ID = sc->id;
			strcpy(sound->scard.name, sc->name);

			sound->scard.mixer = False;
			sound->scard.stereo = sc->stereo;
			sound->scard.sampleSize = sc->sampleSize;
			sound->scard.minRate = sc->minRate;
			sound->scard.maxRate = sc->maxRate;

			parent->setRedraw();
		}


	}
}


class GetIO : public SoundSetupIcon {
public:
	GetIO(IconSet* parent, const Rect& r, SDimension mx, Sound* snd);
	const char* getInfo(char* buffer);
	void execute(Event* event, MenuData* data);
};

GetIO::GetIO(IconSet* parent, const Rect& r, SDimension mx, Sound* snd) :
	SoundSetupIcon(parent, r, mx, 1, snd)
{
}

const char* GetIO::GetIO::getInfo(char* buffer)
{
	sprintf(buffer, "%03hx", sound->scard.ioPort);
	return buffer;
}

void GetIO::execute(Event* event, MenuData* data)
{
	if(event->buttons)
	{
		char buffer[80];

		sprintf(buffer, "%03hx", sound->scard.ioPort);

		if(dialInput(0, language(lge_baseIO), language( lge_OKCancel ), buffer, 4) == 0)
		{
			ushort r;
			if(sscanf(buffer, "%hx", &r) == 1)
			{
				sound->scard.ioPort = r;
				setRedraw();
			}
		}
	}
}


class GetIRQ : public SoundSetupIcon {
public:
	GetIRQ(IconSet* parent, const Rect& r, SDimension mx, Sound* snd);
	const char* getInfo(char* buffer);
	void execute(Event* event, MenuData* data);
};

GetIRQ::GetIRQ(IconSet* parent, const Rect& r, SDimension mx, Sound* snd) :
	SoundSetupIcon(parent, r, mx, 2, snd)
{
}

const char* GetIRQ::GetIRQ::getInfo(char* buffer)
{
	sprintf(buffer, "%d", (int)sound->scard.dmaIRQ);
	return buffer;
}

void GetIRQ::execute(Event* event, MenuData* data)
{
	if(event->buttons)
	{
		char buffer[80];

		sprintf(buffer, "%d", (int)sound->scard.dmaIRQ);

		if(dialInput(0, language(lge_soundIRQ), language( lge_OKCancel ), buffer, 4) == 0)
		{
			char* s = buffer;
			while(*s)
				if(!isdigit(*s++))
					return;
			sound->scard.dmaIRQ = atoi(buffer);
			setRedraw();
		}
	}
}


class GetDMA : public SoundSetupIcon {
public:
	GetDMA(IconSet* parent, const Rect& r, SDimension mx, Sound* snd);
	const char* getInfo(char* buffer);
	void execute(Event* event, MenuData* data);
};

GetDMA::GetDMA(IconSet* parent, const Rect& r, SDimension mx, Sound* snd) :
	SoundSetupIcon(parent, r, mx, 3, snd)
{
}

const char* GetDMA::GetDMA::getInfo(char* buffer)
{
	sprintf(buffer, "%d", (int)sound->scard.dmaChannel);
	return buffer;
}

void GetDMA::execute(Event* event, MenuData* data)
{
	if(event->buttons)
	{
		char buffer[80];

		sprintf(buffer, "%d", (int)sound->scard.dmaChannel);

		if(dialInput(0, language(lge_soundDMA), language( lge_OKCancel ), buffer, 4) == 0)
		{
			char* s = buffer;
			while(*s)
				if(!isdigit(*s++))
					return;
			sound->scard.dmaChannel = atoi(buffer);
			setRedraw();
		}
	}
}


class GetRate : public SoundSetupIcon {
public:
	GetRate(IconSet* parent, const Rect& r, SDimension mx, Sound* snd);
	const char* getInfo(char* buffer);
	void execute(Event* event, MenuData* data);
};

GetRate::GetRate(IconSet* parent, const Rect& r, SDimension mx, Sound* snd) :
	SoundSetupIcon(parent, r, mx, 4, snd)
{
}

const char* GetRate::getInfo(char* buffer)
{
	sprintf(buffer, "%hu", sound->rate);
	return buffer;
}

void GetRate::execute(Event* event, MenuData* data)
{
	if(event->buttons)
	{
		char buffer[80];
		char title[80];

		sprintf(title, language( lge_EnterRate ),
			sound->scard.minRate,
			sound->scard.maxRate);

		sprintf(buffer, "%hu", sound->rate);

		if(dialInput(0, title, language( lge_OKCancel ), buffer, 6) == 0)
		{
			char* s = buffer;
			while(*s)
				if(!isdigit(*s++))
					return;
			long r = atol(buffer);
			if( (r >= sound->scard.minRate) && (r <= sound->scard.maxRate) )
			{
				sound->rate = r;
				setRedraw();
			}
		}
	}
}

class GetQuality : public SoundSetupIcon {
public:
	GetQuality(IconSet* parent, const Rect& r, SDimension mx, Sound* snd);
	const char* getInfo(char* buffer);
	void execute(Event* event, MenuData* data);
};

GetQuality::GetQuality(IconSet* parent, const Rect& r, SDimension mx, Sound* snd) :
	SoundSetupIcon(parent, r, mx, 5, snd)
{
}

const char* GetQuality::GetQuality::getInfo(char* buffer)
{
	return (sound->mcp.options & MCP_QUALITY) ? language(lge_On) : language(lge_Off);
}

void GetQuality::execute(Event* event, MenuData* data)
{
	if(event->buttons)
	{
		sound->mcp.options ^= MCP_QUALITY;
		setRedraw();
	}
}


/*
 * Set up dialogue box
 */

SoundSetup::SoundSetup(Sound* snd)
{
	Font* textFont = fontSet(Font_Text);
	Font* titleFont = fontSet(Font_Title);
	SDimension iLineHeight = textFont->getHeight() + 4;
	SDimension tHeight = titleFont->getHeight() + 6;
	SDimension iHeight = iLineHeight * 6 + 4;
	SDimension bHeight = textFont->getHeight() + 8;
	SDimension height = tHeight + iHeight + bHeight;

	SDimension rWidth = 0;
	SoundCardType* sct = cardNames;
	while(sct->name)
	{
		UWORD w = textFont->getWidth(sct->name);
		if(w > rWidth)
			rWidth = w;
		sct++;
	}

	/*
	 * New... Get titles text from language:
	 */

	char** titlePtr = titles;
	const LGE* lgePtr = titleStrings;

	while(*lgePtr != lge_None)
	{
		*titlePtr = strdup(language(*lgePtr));
		titlePtr++;
		lgePtr++;
	}

	cardNames[0].name = strdup(language(lge_None));


	SDimension lWidth = 0;
	char **title = titles;
	while(*title)
	{
		UWORD w = textFont->getWidth(*title);
		if(w > lWidth)
			lWidth = w;
		title++;
	}

	lWidth += 4;
	rWidth += 4;

	SDimension width = lWidth + rWidth + 4;

	icons = new IconList[2];
	IconSet* outer = new SoundOuter(this,
		Rect( (getW() - width) / 2, (getH() - height) / 2, width, height),
		tHeight, iHeight, lWidth);
	icons[0] = outer;
	icons[1] = 0;

	/*
	 * Make Inner icons (7)
	 */

	IconList* innerIcons = new IconList[8];
	SDimension y = tHeight + 1;
	innerIcons[0] = new GetCardType(outer, Rect(1, y, width - 2, iLineHeight), lWidth, snd);
	y += iLineHeight;
	innerIcons[1] = new GetIO(outer, Rect(1, y, width - 2, iLineHeight), lWidth, snd);
	y += iLineHeight;
	innerIcons[2] = new GetIRQ(outer, Rect(1, y, width - 2, iLineHeight), lWidth, snd);
	y += iLineHeight;
	innerIcons[3] = new GetDMA(outer, Rect(1, y, width - 2, iLineHeight), lWidth, snd);
	y += iLineHeight;
	innerIcons[4] = new GetRate(outer, Rect(1, y, width - 2, iLineHeight), lWidth, snd);
	y += iLineHeight;
	innerIcons[5] = new GetQuality(outer, Rect(1, y, width - 2, iLineHeight), lWidth, snd);
	y += iLineHeight;
	innerIcons[6] = new ButtonIcon(outer, Rect(1, tHeight + iHeight + 1, width - 2, iLineHeight), Font_Text, 0, language(lge_finished), KEY_Enter, False);
	innerIcons[7] = 0;

	outer->setIcons(innerIcons);
}

SoundSetup::~SoundSetup()
{
}

void saveSoundConfig()
{
	SOUNDCARD* sndCard = sound.getSoundInfo();

	FILE* fp = fopen(sc_fileName, "w");
	if(fp)
	{
		fprintf(fp, "%s\n", sc_title);
		fprintf(fp, "%s=%d\n", sc_ID, (int) sndCard->ID);
		fprintf(fp, "%s=%d.%02d\n", sc_version, (int) (sndCard->version >> 8), (int)sndCard->version & 0xff );
		fprintf(fp, "%s=%s\n", sc_name, sndCard->name);
		fprintf(fp, "%s=%04x\n", sc_ioPort, (int) sndCard->ioPort);
		fprintf(fp, "%s=%d\n", sc_dma, (int) sndCard->dmaChannel);
		fprintf(fp, "%s=%d\n", sc_irq, (int) sndCard->dmaIRQ);
		fprintf(fp, "%s=%hu\n", sc_minRate, sndCard->minRate);
		fprintf(fp, "%s=%hu\n", sc_maxRate, sndCard->maxRate);
		fprintf(fp, "%s=%d\n", sc_stereo, (int) sndCard->stereo);
		fprintf(fp, "%s=%d\n", sc_mixer, (int) sndCard->mixer);
		fprintf(fp, "%s=%d\n", sc_sampSize, (int) sndCard->sampleSize);
		fprintf(fp, "%s=", sc_extra);
		for(int i = 0; i < 8; i++)
		{
			fprintf(fp, "%d", (int) sndCard->extraField[i]);
			if(i != 7)
				fprintf(fp, ",");
		}
		fprintf(fp, "\n");

		fprintf(fp, "%s=%hu\n", sc_rate, sound.rate);
		fprintf(fp, "%s=%d\n", sc_quality, (int) sound.mcp.options);


		fclose(fp);
	}
}

/*
 * Main Function
 */

void main(int argc, char *argv[])
{
	/*
	 * Use try/catch to make use of exceptions
	 */

	try
	{
		/*
		 * Process command line
		 */

		char *fliName = 0;

		int i = 0;
		while(++i < argc)
		{
			char* arg = argv[i];

			if((arg[0] == '-') || (arg[0] == '/'))
			{
				switch(toupper(arg[1]))
				{
				default:
					break;
				}
			}
 			else
			{
				if(arg)
					fliName = arg;
			}

		}

		if ( !fliName ) fliName = copyString( "dbdata\\ylorose.amf" );

		/*
		 * Set up the system.
		 *
		 * This sets up the video mode, mouse, timer, fonts
		 *
		 * Things will automatically be restored when the
		 * program finishes and machine's destructor is called.
		 */


		machine.init();

		machine.screen->getBitmap()->fill(Green);
		machine.screen->setUpdate();
		readILBM("art\\screens\\playscrn.lbm", 0, machine.screen->getDeferredPalette());
		machine.screen->updatePalette();



		if(!sound.getConfiguration())
			autoDetectSound();

		Boolean finished = False;

		while(!finished)
		{
			getSoundConfiguration();
			saveSoundConfig();

			/*
			 * Get out if no sound card selected
			 */

			if(sound.scard.ID == ID_NOSOUND)
				break;

			sound.init();

			MODULE* song = sound.loadMusic( fliName );
			if(song)
			{
				sound.playMusic(song);
			}
			else
			{
				dialAlert(0, "Error opening music file", "Ooops!");
				return;
			}

	
			int result = dialAlert(0, language(lge_canHear), language(lge_yesNo));

			sound.stopMusic(song);
			sound.unloadSong(song);
			song = 0;

			switch(result)
			{
			case 0:		// Yes
				saveSoundConfig();
				finished = True;
				break;

			case 1:		// No.. try again
				sound.closeDown();
				break;
			}
		}


#if 0
		/*
		 * Display something
		 */

		machine.screen->getBitmap()->fill(Green);
		machine.screen->setUpdate();

		/*
		 * Do something until user clicks button
		 */

		r.fill(White);
		r.frame(0,0,200,60, Yellow, LGreen);

		window.setFormat(True, True);
		window.setColours(Black);
		window.draw(language(lge_soundConfig));
		machine.screen->setUpdate(r);

		/*
		 * Area on screen to display information
		 */

		r.setClip(20, 140, 600, 52);
		r.fill(LGreen);
		r.frame(20,140,600,52, Yellow);
		window.setFormat(False, False);
		machine.screen->setUpdate(r);

		char buffer[80];
		int y = 4;

		if(song)
		{
			window.setFont(Font_EMMA14);
			window.setColours(Black);

			window.setPosition(Point(4, y));
			sprintf(buffer, "Song: %s", song->name);
			window.draw(buffer);
			y += 16;

			sprintf(buffer, "Channels: %d, Instruments: %d, patterns: %d, tracks: %d",
				(int)song->channelCount,
				(int)song->instrumentCount,
				(int)song->patternCount,
				(int)song->trackCount);
			window.setPosition(Point(4, y));
			window.draw(buffer);
			y += 16;

			sprintf(buffer, "Tempo: %d, Speed: %d",
				(int) song->tempo,
				(int) song->speed);
			window.setPosition(Point(4, y));
			window.draw(buffer);
			y += 16;

		}

		while(!machine.mouse->getEvent() && !kbhit())
		{
			int y1 = 4;

			r.setClip(20, 260, 600, 40);
			r.fill(LGreen);
			r.frame(20, 260, 600, 52, Yellow);

			sprintf(buffer, "Status: %s",
				(ampGetModuleStatus() == MD_PLAYING) ? "Playing" : "Paused");
			window.setPosition(Point(4, y1));
			window.draw(buffer);
			y1 += 16;

			sprintf(buffer, "Pattern: %d, Row: %d, Sync: %d",
				ampGetPattern(),
				ampGetRow(),
				ampGetSync());
			window.setPosition(Point(4, y1));
			window.draw(buffer);
			y1 += 16;

			machine.screen->setUpdate(r);
			machine.screen->update();
		}
	
		if ( kbhit() ) getch();
#endif
	}

 	catch(GeneralError e)
 	{
		cout << "Untrapped Error: ";
		if(e.get())
			cout << e.get();
		else
			cout << "No description";
		cout << endl;
 	}
 	catch(...)
 	{
 		cout << "Caught some kind of error" << endl;
 	}
}


