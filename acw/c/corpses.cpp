/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Corpse Display and maintenance
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "corpses.h"
#include "unit3d.h"
#include "game.h"
#include "batldata.h"

/*
 * Corpse Sprite Functions
 */

CorpseSprite::CorpseSprite(StaticSpriteType n, const BattleLocation& p, Qangle facing) :
	TreeObject(n, p, facing)
{
}

void addCorpseSprite(StaticSpriteType sprite, const BattleLocation& l, Cord3D variance)
{
	BattleLocation l1 = l;
	l1.x += game->gameRand(variance) - variance / 2;
	l1.z += game->gameRand(variance) - variance / 2;

	CorpseSprite* cs = new CorpseSprite(sprite, l1, game->gameRand(4));

	battle->corpseObjects->add(cs);
}

/*
 * Make some corpses of the type belonging to regiment r
 *
 * count is the actual number of sprites (not the number of men).
 */

void makeCorpses(int count, Regiment* r)
{
#ifdef DEBUG
	bLog.printf("Making %d corpses from %s", count, r->getName(True));
#endif

	UnitBattle* bi = r->battleInfo;

	Distance w, h;
	r->battleInfo->getArea(w, h);
	w += h;
	Cord3D v = DistToBattle(w);

	StaticSpriteType spriteType;

	if(r->getSide() == SIDE_USA)
		spriteType = DeadU;
	else
		spriteType = DeadC;
	addCorpseSprite(spriteType, bi->where, v);

	if(r->type.basicType != Infantry)
		addCorpseSprite(DeadHorse, bi->where, v);

	if(r->type.basicType == Artillery)
		addCorpseSprite(DeadArtillery, bi->where, v);
}
