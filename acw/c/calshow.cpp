/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Display Main CAL Area
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.19  1994/06/09  23:32:59  Steven_Green
 * Changed Fonts
 *
 * Revision 1.17  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.13  1994/04/22  22:14:22  Steven_Green
 * Icon Handling tidied up.
 *
 * Revision 1.12  1994/04/20  22:21:40  Steven_Green
 * Use TextWindow class
 *
 * Revision 1.1  1994/02/10  22:57:14  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "calshow.h"
#include "calmain.h"
#include "calicon.h"
#include "system.h"
#include "screen.h"
#include "mouselib.h"
#include "ob.h"
#include "game.h"
#include "generals.h"
#include "colours.h"
#include "text.h"

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
#include "unit3d.h"
#include "batldata.h"
#include "darken.h"
#endif

/*
 * Pointer to use for regiment
 */

GameSprites calRegimentIcon[] = {
	I_CAL_Infantry,
	I_CAL_Cavalry,
	I_CAL_Artillery
};

PointerIndex calRegimentMice[] = {
	M_CAL_Infantry,
	M_CAL_Cavalry,
	M_CAL_Artillery
};


/*
 * Set up the top values and makes sure they are valid
 */

void CAL::updateTops(Unit * topUnit)
{
	if(topUnit)
	{
		Unit* u = topUnit;
		do {
			if(u->getRank() <= Rank_Brigade)
			{
				Unit* u1;		// Pointer to 1st child
			
				if(u->superior)
					u1 = u->superior->child;
				else		// Must be a president!
					u1 = ob->sides;

				int count = 0;

				while(u1 && (u1 != u))
				{
					u1 = u1->sister;
					count++;
				}

				if(!u1)		 			// This should never happen!
					count = 0;

				if(u->getRank() == Rank_Brigade)
				{
					if(count > 10)		// Assume 10 brigades can fit on screen
						tops[u->getRank()] = count - 10;
					else
						tops[u->getRank()] = 0;
				}
				else
					tops[u->getRank()] = count;

				if(u == topUnit)
				{
					Rank r = u->getRank();
					while(r < Rank_Brigade)
					{
						r = demoteRank(r);
						tops[r] = 0;
					}
				}
			}

			u = u->superior;
		} while(u);
	}

	/*
	 * Set up current values
	 */

	Unit* p = 0;
	Unit* a = 0;
	Unit*	c = 0;
	Unit* d = 0;

	int count;
	p = ob->getSides();
	count = tops[Rank_President];
	while(p && count--)
		p = p->sister;
	if(p)
	{
		a = p->child;
		count = tops[Rank_Army];
		while(a && count--)
			a = a->sister;
		if(a)
		{
			c = a->child;
			count = tops[Rank_Corps];
			while(c && count--)
				c = c->sister;
			if(c)
			{
				d = c->child;
				count = tops[Rank_Division];
				while(d && count--)
					d = d->sister;
			}
		}
	}

	if(!d || (d->childCount && (tops[Rank_Brigade] >= d->childCount)))
	{
		tops[Rank_Brigade] = 0;
		if(d)
			tops[Rank_Division]++;
	}

	if(!c || (c->childCount && (tops[Rank_Division] >= c->childCount)))
	{
		tops[Rank_Division] = 0;
		tops[Rank_Brigade] = 0;
		if(c)
			tops[Rank_Corps]++;
	}

	if(!a || (a->childCount && (tops[Rank_Corps] >= a->childCount)))
	{
		tops[Rank_Corps] = 0;
		tops[Rank_Division] = 0;
		tops[Rank_Brigade] = 0;
		if(a)
			tops[Rank_Army]++;
	}

	if(!p || (p->childCount && (tops[Rank_Army] >= p->childCount)))
	{
		/*
		 * This should never happen!
	 	 */
	
		tops[Rank_Army] = 0;
		tops[Rank_Corps] = 0;
		tops[Rank_Division] = 0;
		tops[Rank_Brigade] = 0;
	}
}


/*
 * Data structure to store info used by showCommand()
 */

static struct CAL_ShowInfo {
	SDimension width;	 		// Spacing between items
	SDimension IWidth;	  	// Width of Icon
	SDimension IHeight;		// Height of Icon
	SDimension top; 			// Y coordinate on screen
	GameSprites graph1;		// Normal Graphic
	GameSprites graph1U;		// Graphic if havn't got commander
	PointerIndex mousePointer;
	PointerIndex mousePointerU;
} showInfo[Rank_Brigade + 1] = {
	{  0,  0,  0,   0, I_CAL_Army1, 	   I_CAL_Army1U, 		M_CAL_Army1, 	 M_CAL_Army1U 		},		// President
	{ 20, 65, 24,  12, I_CAL_Army1, 	   I_CAL_Army1U, 		M_CAL_Army1, 	 M_CAL_Army1U 		},		// Army
	{ 20, 55, 20,  68, I_CAL_Corps1, 	I_CAL_Corps1U, 	M_CAL_Corps1, 	 M_CAL_Corps1U  	},		// Corps
	{ 20, 35, 15, 118, I_CAL_Division1, I_CAL_Division1U, M_CAL_Division1,M_CAL_Division1U	},		// Division
	{ 20, 32, 12, 153, I_CAL_Brigade1,  I_CAL_Brigade1U, 	M_CAL_Brigade1, M_CAL_Brigade1U	}		// Brigade
};


/*
 * Update the current X coordinate
 */

void CAL_MainDisplay::updateX(int add)
{
	currentX += add;
	if(currentX >= (getW() - CAL_RegimentWidth - 4 - 24))
		noSpace = True;
}

/*
 * Put an icon on the display
 */

static Rect putCALIcon(Region* bm, SDimension x, SDimension y, GameSprites icon)
{
	SpriteBlock* image = game->sprites->read(icon);

	Rect r = Rect(x - image->getWidth() / 2, y, image->getWidth(), image->getHeight());

	bm->maskBlit(image, r);

	image->release();

	return r;
}

/*
 * Add Icon To Right of given point
 */

static Rect putRightIcon(Region* bm, SDimension x, SDimension y, GameSprites icon)
{
	SpriteBlock* image = game->sprites->read(icon);

	Rect r = Rect(x, y - image->getHeight() / 2, image->getWidth(), image->getHeight());

	bm->maskBlit(image, r);

	image->release();

	return r;
}

/*
 * Add icon to left of given point
 */

static Rect putLeftIcon(Region* bm, SDimension x, SDimension y, GameSprites icon)
{
	SpriteBlock* image = game->sprites->read(icon);

	Rect r = Rect(x - image->getWidth(), y - image->getHeight() / 2, image->getWidth(), image->getHeight());

	bm->maskBlit(image, r);

	image->release();

	return r;
}

#if !defined(TESTBATTLE)
int CAL_MainDisplay::showNewIcon(Region* bm, int x, int y, Unit* superior, Colour col)
{
	int midX = x + CAL_RegimentWidth/2;

	bm->VLine(midX, y - CAL_OverLine, CAL_OverLine, col);
	Rect r = putCALIcon(bm, midX, y, I_CAL_New);

	SDimension maxX = SDimension(r.getX() + r.getW());

	if(maxX > currentX)
		updateX(maxX - currentX);

	addIcon(new CAL_NewIcon(superior, r));

	return midX;
}
#endif

void CAL_MainDisplay::showUnitStatus(Region* bm, const Rect& r, Unit* u, CAL_Highlight hl)
{
	/*
	 * Highlight if current unit, etc
	 */

	switch(hl)
	{
	case HL_Picked:
		bm->frame(r, White, White);
		break;
	case HL_PickedChild:
		bm->frame(r, White, White);
		break;
	case HL_Current:
		bm->frame(r, LRed, LRed);
		break;
	case HL_CurrentChild:
		bm->frame(r, LRed, LRed);
		break;
	}

#if !defined(CAMPEDIT) && !defined(TESTCAMP)

	/*
	 * Cover with Red Cross on black background if Dead
	 */

	if( (u->battleInfo && u->battleInfo->isDead) || (u->getStrength() == 0) )
	{
		SDimension x1 = r.getX() + r.getW() - 1;
		SDimension y1 = r.getY() + r.getH() - 1;

		bm->line(Point(r.getX(), r.getY() + 1), Point(x1 - 1, y1), Black);
		bm->line(Point(r.getX() + 1, r.getY()), Point(x1, y1 - 1), Black);

		bm->line(Point(x1 - 1, r.getY()), Point(r.getX(), y1 - 1), Black);
		bm->line(Point(x1, r.getY() + 1), Point(r.getX() + 1, y1), Black);

		bm->line(Point(r.getX(), r.getY()), Point(x1, y1), Red);
		bm->line(Point(x1, r.getY()), Point(r.getX(), y1), Red);
	}
	else if(u->battleInfo)
	{
		/*
		 * Grey out if off battlefield
		 */

		if(!onBattleField(u->battleInfo->where))
			bm->greyOut(r, Green);
	}
	else if(!cal->campaignMode)
		greyRegion(bm, r);

#endif
}

void CAL_MainDisplay::drawCALCommander(Region* bm, int midX, Unit* unit, CAL_Highlight hl)
{
	CAL_ShowInfo* info = &showInfo[unit->getRank()];

	GameSprites graphic;
	PointerIndex pointer;

	if(unit->general)
	{
		graphic = info[0].graph1;
		pointer = info[0].mousePointer;
	}
	else
	{
		graphic = info[0].graph1U;
		pointer = info[0].mousePointerU;
	}


	if(unit->getSide() == SIDE_USA)
	{
		INCREMENT(graphic);
		// INCREMENT(pointer);

		pointer = PointerIndex(pointer + M_CAL_Army2 - M_CAL_Army1);
	}

	Rect r = putCALIcon(bm, midX, info[0].top + CAL_OverLine, graphic);
	addIcon(new CAL_CommandIcon(unit, r, pointer));

	/*
	 * Display unit's name underneath (not sure if this will work)
	 */

	if(unit->getRank() <= Rank_Corps)
	{
		const char* name = unit->getName(False);
		if(name)
		{
			TextWindow wind(bm, Font_EMFL_8);
			wind.setColours(Black);
			UWORD width = wind.getFont()->getWidth(name);
			SDimension x = midX - width / 2;
			wind.setPosition(Point(x, info[0].top + CAL_OverLine + info[0].IHeight + 3));
			wind.draw(name);
		}
	}

	showUnitStatus(bm, r, unit, hl);

}


int CAL_MainDisplay::showBrigade(Region* bm, Unit* brig, CAL_Highlight hl)
{
	int xStart = currentX;
	int xEnd = currentX;
	// Boolean needFirst = True;

	int regCount = 0;
	int y = CAL_RegimentTop;

	/*
	 * Update the highlight flag
	 */

	Colour topCol = Black;
	Boolean drawConnectionReal = True;
	Boolean drawConnectionGiven = True;

	AttachMode aMode = brig->getAttachMode();

	// if(brig->isReallyIndependant())
	if(aMode != Attached)
		drawConnectionReal = False;

	// if(brig->isIndependant() && !brig->isJoining())
	if(brig->isDetached() && (aMode != Joining))
		drawConnectionGiven = False;

	// if(brig->isReallyIndependant())
	if(aMode != Attached)
	{
		// if(brig->isJoining())
		if(aMode == Joining)
			topCol = DGrey;
		else
			topCol = LGrey;
	}
	else if(hl == HL_PickedChild)
		topCol = White;
	else if(hl == HL_CurrentChild)
		topCol = LRed;

	CAL_Highlight childHighlight = hl;
	if(brig == cal->pickedUnit)
	{
		hl = HL_Picked;
		if((cal->pickMode == CAL::Dropping) && cal->dragRecursive)
			childHighlight = HL_PickedChild;
	}
	else if(brig == cal->currentUnit)
	{
		hl = HL_Current;
		childHighlight = HL_CurrentChild;
	}

	Colour lineCol = Black;
	if(childHighlight == HL_PickedChild)
		lineCol = White;
	else if(childHighlight == HL_CurrentChild)
		lineCol = LRed;

	Unit* reg = (Regiment*) brig->child;
	while(reg && (regCount < MaxRegimentsPerBrigade))
	{
		showRegiment(bm, (Regiment*)reg, y, childHighlight);
		y += CAL_RegimentHeight;
		regCount++;
		reg = reg->sister;
	}

	int midX = currentX;

	if(drawConnectionReal)
		bm->VLine(midX, showInfo[Rank_Brigade].top, CAL_OverLine, topCol);
	if(drawConnectionGiven)
	{
		bm->VLine(midX+1, showInfo[Rank_Brigade].top, CAL_OverLine, topCol);
		bm->VLine(midX-1, showInfo[Rank_Brigade].top, CAL_OverLine, topCol);
	}

	if(regCount)
		bm->VLine(midX, showInfo[Rank_Brigade].top + CAL_OverLine, CAL_RegimentTop - showInfo[Rank_Brigade].top - CAL_OverLine, lineCol);

	/*
	 * Display Icon (debug using box)
	 */

	drawCALCommander(bm, currentX, brig, hl);

	updateX(CAL_RegimentWidth);

#ifdef DEBUG1
	game->screen.physBlit(bm.getImage(), *this, *this);	// getMouse()->blit(*bm, *this);
#endif

	return midX;
}

void CAL_MainDisplay::showRegiment(Region* bm, Regiment* reg, int y, CAL_Highlight hl)
{
	GameSprites icon = calRegimentIcon[reg->type.basicType];
	PointerIndex pointer = calRegimentMice[reg->type.basicType];

	Rect r = putCALIcon(bm, currentX, y, icon);
	addIcon(new CAL_CommandIcon(reg, r, pointer));

	if(reg == cal->currentUnit)
		hl = HL_Current;
	else if(reg == cal->pickedUnit)
		hl = HL_Picked;

	showUnitStatus(bm, r, reg, hl);


#ifdef DEBUG1
	game->getMouse()->blit(*bm, *this);
#endif
}

/*
 * Generic Show routine to handle army/corps/division
 */


int CAL_MainDisplay::showCommand(Region* bm, Unit* unit, CAL_Highlight hl)
{
	int xStart = currentX;
	int xEnd = currentX;
	Boolean needFirst = True;

	Rank rank = unit->getRank();
	CAL_ShowInfo* info = &showInfo[rank];

	Colour topCol = Black;

	Boolean drawConnectionReal = True;
	Boolean drawConnectionGiven = True;

	AttachMode aMode = unit->getAttachMode();

	// if(unit->isReallyIndependant())
	if(aMode != Attached)
		drawConnectionReal = False;

	// if(unit->isIndependant() && !unit->isJoining())
	if(unit->isDetached() && (aMode != Joining))
		drawConnectionGiven = False;

	// if(unit->isReallyIndependant())
	if(aMode != Attached)
	{
		if(unit->isJoining())
			topCol = DGrey;
		else
		{
			topCol = LGrey;
			hl = HL_None;
		}
	}
	else if(hl == HL_PickedChild)
		topCol = White;
	else if(hl == HL_CurrentChild)
		topCol = LRed;

	/*
	 * Update the highlight flag
	 */

	CAL_Highlight childHighlight = hl;
	if(unit == cal->currentUnit)
	{
		hl = HL_Current;
		childHighlight = HL_CurrentChild;
	}
	if(unit == cal->pickedUnit)
	{
		hl = HL_Picked;
		if((cal->pickMode == CAL::Dropping) && cal->dragRecursive)
			childHighlight = HL_PickedChild;
	}

	Colour lineCol = Black;
	if(childHighlight == HL_PickedChild)
		lineCol = White;
	else if(childHighlight == HL_CurrentChild)
		lineCol = LRed;

	int unitCount;
	if(doingFirst)
		unitCount = cal->tops[rank+1];
	else
		unitCount = 0;
	Boolean moreLeft = unitCount;

	Unit* u = unit->child;
	int count = unitCount;
	while(u && count--)
		u = u->sister;

	if(rank <= Rank_Army)
	{
		if(u)
			xStart = xEnd = showCommand(bm, u, childHighlight);
		needFirst = False;
		unitCount++;
	}
	else	// Must be a division or corps
	{
		while(!noSpace && u)
		{
			switch(rank)
			{
			case Rank_President:		// These should not happen here...
			case Rank_Army:
			case Rank_Corps:
				xEnd = showCommand(bm, u, childHighlight);
				break;
			case Rank_Division:
				xEnd = showBrigade(bm, u, childHighlight);
				doingFirst = False;
				break;
			case Rank_Brigade:		// These two should not happen here!
			case Rank_Regiment:
				break;
			}

			if(needFirst)
			{
				xStart = xEnd;
				needFirst = False;
			}

			u = u->sister;

			unitCount++;
		}
	}

	int midX;
	
	if(rank <= Rank_Corps)
	{
		midX = getW() / 2;

		if(midX < xStart)
			xStart = midX;
		if(midX > xEnd)
			xEnd = midX;
	}
	else
		midX = (xStart + xEnd) / 2;

#if !defined(TESTBATTLE)
	/*
	 * Display new Icon at end if room
	 */

	if(cal->campaignMode && ( (rank <= Rank_Army) || (unitCount >= unit->childCount)))
	{
		xEnd = showNewIcon(bm, xEnd + info[1].IWidth/2, info[1].top + CAL_OverLine, unit, lineCol);
		if(needFirst)
		{
			xStart = xEnd;
			needFirst = False;
		}
	}
#endif

	/*
	 * Display Right Scroll
	 */

	if(unitCount < unit->childCount)
	{
		bm->HLine(xEnd, info[1].top, info[1].IWidth/2, lineCol);

		Rect r = putRightIcon(bm, xEnd + (info[1].IWidth/2) + 8, info[1].top, I_CAL_Right);

		addIcon(new CAL_ScrollIcon(Rank(rank+1), CAL_ScrollIcon::Right, this, r));
	}

	if(xStart != xEnd)
		bm->HLine(xStart, info[1].top, xEnd - xStart, lineCol);

	/*
	 * Continuation
	 */

	if(moreLeft)
	{
		bm->HLine(xStart - info[1].IWidth/2, info[1].top, info[1].IWidth/2, lineCol);

		Rect r = putLeftIcon(bm, xStart - (info[1].IWidth/2 + 8), info[1].top, I_CAL_Left);

		addIcon(new CAL_ScrollIcon(Rank(rank+1), CAL_ScrollIcon::Left, this, r));
	}


	/*
	 * Draw our actual unit!
	 * (Up till now we've been dealing with the children)
	 */

	if(info[0].IWidth)
	{
		/*
		 * Line above box
		 */

		if(drawConnectionReal)
			bm->VLine(midX, info[0].top, CAL_OverLine, topCol);
		if(drawConnectionGiven)
		{
			bm->VLine(midX+1, info[0].top, CAL_OverLine, topCol);
			bm->VLine(midX-1, info[0].top, CAL_OverLine, topCol);
		}

#if 0
		/*
		 * Indicate if tied to another unit somehow
		 */

		if(unit->tied)
		{
			bm->VLine(midX+3, info[0].top + 4, CAL_OverLine - 4, topCol);
			bm->HLine(midX+3, info[0].top + 4, 5, topCol);

			const char* name = unit->tied->getName(False);
			TextWindow wind(bm, Font_EMFL_8);
			wind.setColours(Black);
			wind.setPosition(Point(midX+9, info[0].top + 2));
			wind.draw(name);
		}
#endif
		
		/*
		 * Line below box (if army/corps then leave gap for name)
		 */

		SDimension y2 = info[0].top + CAL_OverLine + info[0].IHeight;

		if(rank <= Rank_Corps)
		{
			bm->VLine(midX, y2, 2, lineCol);
			y2 += 12;
		}

		bm->VLine(midX, y2, info[1].top - y2, lineCol);
  
		/*
	 	 * Display Icon
	 	 */

		drawCALCommander(bm, midX, unit, hl);
	}

	updateX(info[1].width);

#ifdef DEBUG1
	game->getMouse()->blit(*bm, *this);
#endif

	return midX;
}



void CAL_MainDisplay::showUnit(Region* bm, Unit* unit, CAL_Highlight hl)
{
	switch(unit->getRank())
	{
	case Rank_President:
	case Rank_Army:
	case Rank_Corps:
	case Rank_Division:
		showCommand(bm, unit, hl);
		break;
	case Rank_Brigade:
		showBrigade(bm, unit, hl);
		break;
	case Rank_Regiment:
		showRegiment(bm, (Regiment*)unit, CAL_RegimentTop, hl);
		break;
	}
}

SDimension CAL_MainDisplay::getCALwidth(Unit* unit, SDimension x)
{
	static const SDimension maxX = getW() - CAL_RegimentWidth - 4 - 24;
	static Boolean doingFirst = False;

	Rank rank = unit->getRank();

	if(rank == Rank_President)
		doingFirst = True;

	int w = 0;

	Unit* u = unit->child;
	if(u)
	{
		if(rank < Rank_Brigade)
		{
			int unitCount;
			if(doingFirst)
				unitCount = cal->tops[rank + 1];
			else
				unitCount = 0;
			while(u && unitCount--)
				u = u->sister;
		}

		if(cal->campaignMode && (rank == Rank_Division))
			w += CAL_NewWidth;

		w += showInfo[rank].width;

		while((x < maxX) && u)
		{
			SDimension w1;

			if(rank >= Rank_Division)
			{
				w1 = CAL_RegimentWidth;
				doingFirst = False;
			}
			else
				w1 = getCALwidth(u, x);

			x += w1;

			if(x >= maxX)
				break;

			w += w1;

			if(rank < Rank_Corps)
				break;

			u = u->sister;
		}

	}

	return w;
}

/*
 * Draw the main CAL area
 */

void CAL_MainDisplay::drawIcon()
{
	PointerIndex oldMouse = machine.mouse->setPointer(M_Busy);

	removeIcons();		// Remove existing icons

	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));

	bm.fill(cal->fillPattern);

	/*
	 * Starting at given army:
	 */

	noSpace = False;
	doingFirst = True;
	currentX = 20 + 24;	// Leave enough space for an arrow

	Unit* p = cal->ob->getSides();
	int count = cal->tops[Rank_President];
	while(p && count--)
		p = p->sister;

	if(p)
	{
		currentX = (getW() - getCALwidth(p, 20+24)) / 2;

		showUnit(&bm, p, HL_None);
	}

	machine.mouse->setPointer(oldMouse);
	machine.screen->setUpdate(bm);

	/*
	 * Bodge to prevent dodgy clicking on icons that don't exist
	 */

	machine.mouse->clearEvents();

}





	
