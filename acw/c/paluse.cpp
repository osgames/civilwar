/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Assuming roughly similar palette arrangements
 * Find out what colour slots have been used and fill up unused
 * slots with a silly colour (Purple).
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.4  1994/06/06  13:17:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.2  1994/05/19  17:44:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/04/05  12:28:09  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <iostream.h>
#include <iomanip.h>
#include <stdlib.h>
#include <stdio.h>
#include <dos.h>
#include <wclist.h>
#include <wclistit.h>


#include "ilbm.h"
#include "image.h"
#include "palette.h"


class PaletteUse {
public:
	Palette p;
	int count[256];

	PaletteUse();

	void writeILBM(char* fname);
	UBYTE exists(Palette& palette, UBYTE c);
};

PaletteUse::PaletteUse()
{
	for(int i = 0; i < 256; i++)
	{
		p.setColour(i, 0xff, 0, 0xff);

		count[i] = 0;
	}
}

UBYTE findMatch(Palette& palette, UBYTE c, Palette& p)
{
	UBYTE r,g,b;

	palette.getColour(c, r, g, b);

	for(int i = 0; i < 256; i++)
	{
		UBYTE r1,g1,b1;

		p.getColour(i, r1,g1,b1);

		if( (r == r1) && (g == g1) && (b == b1) )
		{
#ifdef DEBUG
			if(i != c)
				cout << "Duplicate Colour " << int(c) << " and " << i << endl;
#endif
			return UBYTE(i);
		}
	}

	return c;
}

UBYTE PaletteUse::exists(Palette& palette, UBYTE c)
{
	return findMatch(palette, c, p);
}



void PaletteUse::writeILBM(char* fname)
{
	Image body(640,480);

	::writeILBM(fname, body, p);
}


/*
 * Process a file
 */


void processPicture(const char* fileName, PaletteUse& list)
{
	Image image;
	Palette palette;

	if(readILBM(fileName, &image, &palette) == 0)
	{
#ifdef DEBUG
		cout << image.getWidth() << " by " << image.getHeight() << endl;
#endif
		/*
		 * Make remap table to remove identical colours
		 */

		UBYTE remap[256];

		for(int i = 0; i < 256; i++)
			remap[i] = list.exists(palette, findMatch(palette, i, palette));



		UBYTE* ad = image.getAddress();		// Address of 1st pixel
		int pixelCount = image.getWidth() * image.getHeight();
		while(pixelCount--)
		{
			UBYTE c = remap[*ad++];

			if(list.count[c] == 0)
				list.p.setColour(c, palette.getColour(c));

			list.count[c]++;
		}
	}

	/*
	 * Image gets freed automatically during destructor
	 */

}

/*
 * Usage:
 *		palcount [filename]...
 *
 * filename may include DOS wildcards
 */


void main(int argc, char* argv[])
{
	/*
	 * Initialise palette table
	 */

	PaletteUse list;

	/*
	 * For each filename, analyse file
	 */


	int i = 0;

	while(++i < argc)
	{
		const char* fileName = argv[i];

#ifdef DEBUG
		cout << "Parameter: " << fileName << endl;
#endif

		char fullpath	[_MAX_PATH];
		char drive		[_MAX_DRIVE];
		char dir			[_MAX_DIR];
		char fname		[_MAX_FNAME];
		char ext			[_MAX_EXT];

		_fullpath(fullpath, fileName, _MAX_PATH);
		_splitpath(fullpath, drive, dir, fname, ext);

		struct find_t fileInfo;

		if(!_dos_findfirst(fileName, _A_NORMAL, &fileInfo))
		{
			do
			{
				sprintf(fullpath, "%s%s%s", drive, dir, fileInfo.name);
#ifdef DEBUG
				cout << "Reading: " << fullpath << ", " << fileInfo.size << " bytes" << endl;
#endif
				processPicture(fullpath, list);

			} while(!_dos_findnext(&fileInfo));
		}
	}

	list.writeILBM("palette.lbm");
}

