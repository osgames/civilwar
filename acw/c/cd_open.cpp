/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Open Files from Hard Disk or CD Rom as appropriate
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <string.h>
#include <fstream.h>
#include <io.h>
#include "cd_open.h"
#include "strutil.h"
#include "memptr.h"

/*
 * Stored as global, so auto destructed on exit
 */

Install install;		// Installation Information
static char fileStr[FILENAME_MAX];

Install::Install()
{
 	initialised = False;
 	strcpy(lang, "ENG");
 	cdPath = 0;
 	langFile = 0;
}

Install::~Install()
{
	if(langFile)
		fclose(langFile);
	if(cdPath)
		delete[] cdPath;
	delete[] stringPos;
}

void Install::setup_language()
{
	if(!install.initialised)
	{
		install.initialised = True;		// Flag to 

		ifstream installdat;

		installdat.open( "install.dat", ios::in | ios::binary );

		if(installdat ) 
		{
			char temp = 0;

			int count = 0;

			int pathCount = 0;

			installdat.seekg( 0, ios::end);
			int tsize = installdat.tellg ();

			installdat.seekg( 5, ios::beg );

			// getAfterKeyword( installdat, "PATH=" );

			MemPtr<char> buffer(100);

			while( temp != '\r' && count < tsize )
			{
				installdat.read ( ( unsigned char *)&temp, sizeof ( char ) );

				if ( temp != '\r' ) buffer[pathCount++] = temp;
				count++;
			}

			if(pathCount)
			{
				if ( buffer[pathCount-1] != '\\' ) buffer[pathCount++] = '\\';
				buffer[pathCount] = '\0';
				install.cdPath = copyString(buffer);
			}

			installdat.seekg( 5, ios::beg );    // restore read ptr position;

			count = 5;

			/*
	 	 	 * 1st line is the path of the CD...
	 	 	 */


			temp = 'a';

			while( temp != '\r' && count < tsize )
			{
				installdat.read ( ( unsigned char *)&temp, sizeof ( char ) );

				count++;
			}


			while( temp != 'L' && count < tsize )
			{
				installdat.read ( ( unsigned char *)&temp, sizeof ( char ) );

				count++;
			}

			installdat.seekg( 8, ios::cur );


			/*
		 	 * 2nd line is language extension
		 	 */

			temp = 0;

			if ( count != tsize ) count = 0;

			while( temp != '\r' && count < 3 )
			{
				installdat.read ( ( unsigned char *)&temp, sizeof ( char ) );

				if ( temp >= 'A' && temp <= 'Z' ) 
				{
					install.lang[ count++ ] = temp;
				}
			}

			install.lang[ count ] = '\0';

			installdat.close();
		}

		/*
		 * Open language file
		 */

		const char* lgeName = getFileName("dbdata\\language");

		install.langFile = FileOpen(lgeName, "rb");

		fread(&install.nStrings, sizeof(install.nStrings), 1, install.langFile);
		install.stringPos = new ULONG[install.nStrings];
		fread(install.stringPos, sizeof(ULONG), install.nStrings, install.langFile);
	}
}


const char* getFileName( char* name )
{
	install.setup();

	static char langStr[FILENAME_MAX];

	strcpy( langStr, name );
	strcat( langStr, "." );
	strcat( langStr, install.lang );

	return langStr;
}	


Boolean fileOpen(ifstream& txt, const char *name, const int mode)
{
	install.setup();

#if 0
	while(!txt)
	{
		txt.open( name, mode );
	
		if(!txt)
		{
			if(install.cdPath)
			{
				strcpy(fileStr, install.cdPath);
				strcat(fileStr, name);

				txt.open( fileStr, mode );
			}

			if(!txt)
			{
				MemPtr<char> buffer(512);
				sprintf(buffer, language(lge_FileMissing), name);
				if(dialAlert(0, buffer, language(lge_RetryCancel)) != 0)
					throw GeneralError(language(lge_FileMissing), name);
			}
		}
	}

	return True;
#else

	const char* s = getPathAndCheck(name);
	if(s)
	{
		txt.open(s, mode);
		if(txt)
			return True;
	}

	return False;

#endif
}

FILE* FileOpen( const char *name, const char* mode )
{
	install.setup();

#if 0
	FILE* handle = 0;
	
	while(!handle)
	{
		handle = fopen( name, mode );
	
		if(!handle)
		{
			if(install.cdPath)
			{
				strcpy(fileStr, install.cdPath);
				strcat(fileStr, name);

				handle = fopen(fileStr, mode);
			}

			if(!handle)
			{
				MemPtr<char> buffer(512);
				sprintf(buffer, language(lge_FileMissing), name);
				if(dialAlert(0, buffer, language(lge_RetryCancel)) != 0)
					throw GeneralError(language(lge_FileMissing), name);
			}
		}
	}

	return handle;
#else

	const char* s = getPathAndCheck(name);
	if(s)
		return fopen(s, mode);
	else
		return 0;

#endif
}

int fileOpen( const char *name, int mode )
{
	install.setup();

#if 0
	int handle = -1;

	while(handle < 0)
	{
		handle = open(name, mode);
	
		if(handle < 0)
		{
			if(install.cdPath)
			{ 
	  			strcpy(fileStr, install.cdPath);
	  			strcat(fileStr, name);

	  			handle = open(fileStr, mode);
			}

			if(handle < 0)
			{
				MemPtr<char> buffer(512);
				sprintf(buffer, language(lge_FileMissing), name);
				if(dialAlert(0, buffer, language(lge_RetryCancel)) != 0)
					throw GeneralError(language(lge_FileMissing), name);
			}
		}
	}

	return handle;
#else

	const char* s = getPathAndCheck(name);
	if(s)
		return open(s, mode);
	else
		return -1;

#endif
}


const char* getPathAndCheck(const char* name)
{
	for(;;)
	{
		if(access(name, R_OK) == 0)
			return name;

		install.setup();

		if(install.cdPath)
		{
			strcpy(fileStr, install.cdPath);
			strcat(fileStr, name);

			if(access(fileStr, R_OK) == 0)
				return fileStr;
		}

		MemPtr<char> buffer(512);
		sprintf(buffer, language(lge_FileMissing), name);
		if(dialAlert(0, buffer, language(lge_RetryCancel)) != 0)
			throw GeneralError(language(lge_FileMissing), name);
	}
}


const char* addPath( const char* path )
{ 
	install.setup();

	MemPtr<char> buffer(MaxStrLength);
	if(install.cdPath)
		strcpy(buffer, install.cdPath);
	else buffer[0] = '\0';
 
	strcat(buffer, path);

	return languageStrings.make(buffer, lge_NULL);
}

Boolean DoesFileExist( const char* fileName )
{
	return (access(fileName, R_OK) == 0);
}


