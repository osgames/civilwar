/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Editor - Water Zone and Naval Unit editting
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>

#include "camped.h"
#include "text.h"
#include "colours.h"
#include "campwld.h"
#include "system.h"
#include "screen.h"
#include "mouselib.h"
#include "tables.h"

static char* waterTypeStr[] = {
	"Coastal Zone",
	"Sea Zone",
	"River Zone"
};


/*
 * Miscellaneous functions
 */

void CampaignWorld::deleteWaterZone(WaterZone* zone)
{
	zone->facilityList.clear();
	zone->connections.clear();


	WaterZoneID id = waterNet.zones.getID(zone);

	waterNet.zones.removeID(id);

	/*
	 * Remove any references from other zones
	 */

	for(int i = 0; i < waterNet.zones.entries(); i++)
	{
		WaterZone& z = waterNet.zones[i];

		for(int j = 0; j < z.connections.entries(); j++)
		{
			if(z.connections[j] == id)
			{
				z.connections.removeID(j);
				j--;
			}
			else if(z.connections[j] > id)
				z.connections[j]--;;
		}
	}

	/*
	 * Adjust facility references
	 */

	for(i = 0; i < facilities.entries(); i++)
	{
		Facility* f = facilities[i];

		if(f->waterZone != NoWaterZone)
		{
			if(f->waterZone == id)
			{
				f->waterZone = NoWaterZone;
				f->facilities &= ~(Facility::F_Port | Facility::F_LandingStage);
				f->waterCapacity = 0;
			}
			else if(f->waterZone > id)
				f->waterZone--;
		}

		facilityChanged = True;
		changed = True;
	}
}

WaterZone* CampaignWorld::makeNewWaterZone()
{
	WaterZone* zone = waterNet.zones.add();

	zone->boats.clear();
	zone->side = SIDE_None;
	zone->type = WZ_Sea;
	zone->connections.init(0);
	zone->facilityList.init(0);

	return zone;
}

void removeWaterConnection(WaterZone* z, WaterZoneID id)
{
	for(int i = 0; i < z->connections.entries(); i++)
	{
		if(z->connections[i] == id)
		{
			z->connections.removeID(i);
			return;
		}
	}
}

void removeWaterFacility(WaterZone* z, FacilityID id)
{
	for(int i = 0; i < z->facilityList.entries(); i++)
	{
		if(z->facilityList[i] == id)
		{
			z->facilityList.removeID(i);
			return;
		}
	}
}


void WaterZone::showInfo(Region* r)
{
	control->setupInfo();

	TextWindow& textWind = *control->textWin;
	textWind.setFont(Font_Title);
	textWind.setColours(Black);
	textWind.setHCentre(True);
	textWind.setPosition(Point(4,4));

	textWind.draw(waterTypeStr[type]);
}


/*===================================================================
 * Water Zone Mode
 */


/*
 * Dialogue to contain:
 *   New
 *   Delete
 *   Move
 *   Set Type
 *   Set Side
 *   4 Boat counters
 *   Set Links
 *   Set Facilities
 *
 *   Cancel / OK
 */

enum {
	EW_None,
	EW_OK,
	EW_Cancel,
	EW_Type,
	EW_Boat1,
	EW_Boat2,
	EW_Boat3,
	EW_Boat4,
	EW_BoatB1,
	EW_BoatB2,
	EW_BoatB3,
	EW_BoatB4,
	EW_Side,
	EW_Move,
	EW_Link,
	EW_Facility,
	EW_New,
	EW_Delete
};

char w_boat1Str[4];
char w_boat2Str[4];
char w_boat3Str[4];
char w_boat4Str[4];
char w_sideStr[40];

enum {
	EWE_Type,
	EWE_NavalTitle,
	EWE_RiverineTitle,
	EWE_NavalUnitStr,
	EWE_Boat1Num,
	EWE_NavalIronStr,
	EWE_Boat2Num,
	EWE_RiverineStr,
	EWE_Boat3Num,
	EWE_RiverineIronStr,
	EWE_Boat4Num,
	EWE_Side,
	EWE_Move,
	EWE_Link,
	EWE_Facility,
	EWE_Cancel,
	EWE_OK,
	EWE_New,
	EWE_Delete
};

DialItem edWaterItems[] = {
	DialItem(Dial_Button,		EW_Type,			Rect(  0,  0,256,16), 0),

	DialItem(Dial_Button,		EW_None,			Rect(  0, 20,124,12), "Naval"),
	DialItem(Dial_Button,		EW_None,			Rect(132, 20,124,12), "Riverine"),

	DialItem(Dial_Button,		EW_BoatB1,		Rect(  0, 32,100,16), "Nav. Unit"),
	DialItem(Dial_NumberInput,	EW_Boat1,		Rect(100, 32, 24,16), w_boat1Str, sizeof(w_boat1Str)),
	DialItem(Dial_Button,		EW_BoatB2,		Rect(  0, 48,100,16), "Nav. IronClad"),
	DialItem(Dial_NumberInput,	EW_Boat2,		Rect(100, 48, 24,16), w_boat2Str, sizeof(w_boat2Str)),
	DialItem(Dial_Button,		EW_BoatB3,		Rect(132, 32,100,16), "Riv. Unit"),
	DialItem(Dial_NumberInput,	EW_Boat3,		Rect(224, 32, 24,16), w_boat3Str, sizeof(w_boat3Str)),
	DialItem(Dial_Button,		EW_BoatB4,		Rect(132, 48,100,16), "Riv. IronClad"),
	DialItem(Dial_NumberInput,	EW_Boat4,		Rect(224, 48, 24,16), w_boat4Str, sizeof(w_boat4Str)),

	DialItem(Dial_Button, 		EW_Side,	  		Rect(  0, 68,124,14), w_sideStr),
	DialItem(Dial_Button, 		EW_Move,	  		Rect(132, 68,124,14), "Move"),
	DialItem(Dial_Button, 		EW_Link,	  		Rect(  0, 88,124,14), "Link"),
	DialItem(Dial_Button, 		EW_Facility, 	Rect(132, 88,124,14), "Facilities"),

	DialItem(Dial_Button, 		EW_Cancel,  	Rect( 32,108, 64,16), "Cancel"),
	DialItem(Dial_Button, 		EW_OK,  			Rect(160,108, 64,16), "OK"),

	DialItem(Dial_Button, 		EW_New,  		Rect(  0,132,124,16), "New"),
	DialItem(Dial_Button, 		EW_Delete,  	Rect(132,132,124,16), "Delete"),

	DialItem(Dial_End)
};

class WaterEdit : public Dialogue {
	WaterZone* zone;
public:
	WaterEdit(WaterZone* z);
	void doIcon(DialItem* item, Event* event, MenuData* d);

	void setupButtons();
};

WaterEdit::WaterEdit(WaterZone* z)
{
	zone = z;
	setupButtons();
	setup(edWaterItems);
}

void WaterEdit::setupButtons()
{
	Boolean enableZone = (zone != 0);

	edWaterItems[EWE_Type].enabled 				= enableZone;
	edWaterItems[EWE_NavalTitle].enabled 		= enableZone;
	edWaterItems[EWE_RiverineTitle].enabled 	= enableZone;
	edWaterItems[EWE_NavalUnitStr].enabled 	= enableZone;
	edWaterItems[EWE_Boat1Num].enabled 			= enableZone;
	edWaterItems[EWE_NavalIronStr].enabled 	= enableZone;
	edWaterItems[EWE_Boat2Num].enabled 			= enableZone;
	edWaterItems[EWE_RiverineStr].enabled 		= enableZone;
	edWaterItems[EWE_Boat3Num].enabled 			= enableZone;
	edWaterItems[EWE_RiverineIronStr].enabled = enableZone;
	edWaterItems[EWE_Boat4Num].enabled 			= enableZone;
	edWaterItems[EWE_Side].enabled 				= enableZone;
	edWaterItems[EWE_Move].enabled 				= enableZone;
	edWaterItems[EWE_Link].enabled 				= enableZone;
	edWaterItems[EWE_Facility].enabled 			= enableZone;
	edWaterItems[EWE_OK].enabled 					= enableZone;
	edWaterItems[EWE_Delete].enabled 			= enableZone;

	if(zone)
	{
		if(zone->type == WZ_Sea)
		{
			edWaterItems[EWE_Facility].enabled = False;
			edWaterItems[EWE_RiverineStr].enabled 		= False;
			edWaterItems[EWE_Boat3Num].enabled 			= False;
			edWaterItems[EWE_RiverineIronStr].enabled = False;
			edWaterItems[EWE_Boat4Num].enabled 			= False;
		}
		else if(zone->type == WZ_River)
		{
			edWaterItems[EWE_NavalUnitStr].enabled 	= False;
			edWaterItems[EWE_Boat1Num].enabled 			= False;
			edWaterItems[EWE_NavalIronStr].enabled 	= False;
			edWaterItems[EWE_Boat2Num].enabled 			= False;
		}

		edWaterItems[EWE_Type].text = waterTypeStr[zone->type];

		sprintf(w_sideStr, "Side: %s", sideStr[zone->side]);
		sprintf(w_boat1Str, "%d", zone->boats.count[Nav_NavalUnit]);
		sprintf(w_boat2Str, "%d", zone->boats.count[Nav_NavalIronClad]);
		sprintf(w_boat3Str, "%d", zone->boats.count[Nav_Riverine]);
		sprintf(w_boat4Str, "%d", zone->boats.count[Nav_RiverineIronClad]);
	}
}

void WaterEdit::doIcon(DialItem* item, Event* event, MenuData* d)
{
	NavalType nt;

	if((item->id == EW_Boat1) || (item->id == EW_BoatB1))
		nt = Nav_NavalUnit;
	else if((item->id == EW_Boat2) || (item->id == EW_BoatB2))
		nt = Nav_NavalIronClad;
	else if((item->id == EW_Boat3) || (item->id == EW_BoatB3))
		nt = Nav_Riverine;
	else if((item->id == EW_Boat4) || (item->id == EW_BoatB4))
		nt = Nav_RiverineIronClad;
	
	if(event->buttons)
	{
		switch(item->id)
		{
		case EW_Cancel:
		case EW_OK:
		case EW_Move:
		case EW_Link:
		case EW_Facility:
		case EW_New:
		case EW_Delete:
			d->setFinish(item->id);
			break;
		case EW_Type:
			if(zone->type == WZ_Coast)
				zone->type = WZ_Sea;
			else if(zone->type == WZ_Sea)
				zone->type = WZ_River;
			else
				zone->type = WZ_Coast;
			setupButtons();
			d->setRedraw();
			break;
		case EW_Side:
			zone->side = inputSide(zone->side);
			setupButtons();
			d->setRedraw();
			break;
		case EW_BoatB1:
		case EW_BoatB2:
		case EW_BoatB3:
		case EW_BoatB4:
			if(event->buttons & Mouse::LeftButton)
				zone->boats.count[nt]++;
			else
				zone->boats.count[nt]--;
			setupButtons();
			d->setRedraw();
		}
	}

	if( (item->id == EW_Boat1) ||
		 (item->id == EW_Boat2) ||
		 (item->id == EW_Boat3) ||
		 (item->id == EW_Boat4) )
		zone->boats.count[nt] = atoi(item->text);
}

void CampaignEditControl::updateWaterZone()
{
	if(edittingZone)
	{
		/*
		 * Fiddle about to make sure values are valid
		 */
	
		if(edittedZone.type == WZ_Sea)
		{
			edittedZone.boats.count[Nav_Riverine] = 0;
			edittedZone.boats.count[Nav_RiverineIronClad] = 0;
			edittedZone.facilityList.clear();
		}
		else if(edittedZone.type == WZ_River)
		{
			edittedZone.boats.count[Nav_NavalUnit] = 0;
			edittedZone.boats.count[Nav_NavalIronClad] = 0;
		}


		*edittingZone = edittedZone;
		waterChanged = True;
		changed = True;
	}
}

void CampaignEditControl::editWaterZone(WaterZone* zone)
{
	if(zone)
	{
		edittingZone = zone;
		edittedZone = *zone;
	}


	int result;

	WaterEdit edit(&edittedZone);
	result = edit.process();

	switch(result)
	{
		case EW_Cancel:
			edittingZone = 0;
			break;

		case EW_Move:
			trackMode = Moving;
			hint("Click on new location for Water Zone");
			updateWaterZone();
			break;

		case EW_Link:
			hint("Left click links, Right to end");
			trackMode = GetLink;
			showLink = True;
			updateWaterZone();
			break;

		case EW_Facility:
			hint("Left click links, Right to end");
			trackMode = GetOwner;
			showLink = True;
			updateWaterZone();
			break;

		case EW_New:
			loseObject();
			objectIsNew = True;
			trackMode = Moving;
			hint("Click at location for New WaterZone");
			updateWaterZone();
			break;

		case EW_Delete:
			loseObject();
			world->deleteWaterZone(edittingZone);
			break;

		case EW_OK:
		default:
			updateWaterZone();
			edittingZone = 0;
			break;
	}
}

void CampaignEditControl::waterOverMap		(MapWindow* map, Event* event, MenuData* d, const Location* l)
{
	if(trackMode == Tracking)
	{
		if(world->waterNet.zones.entries() == 0)
		{
			trackMode = Moving;
			objectIsNew = True;
			hint("Click at location for New WaterZone");
		}

		trackFor(map, event, OT_WaterZone);

		if(event->buttons)
		{
			if(currentObject)
			{
				WaterZone* zone = (WaterZone*) currentObject;

			 	editWaterZone(zone);
			}
		}
	}
	else if(trackMode == Moving)
	{
		if(event->buttons)
		{
			hint(0);


			if(event->buttons & Mouse::LeftButton)
			{	// Move it!
			makeNew:
		 	
				WaterZone* zone = (WaterZone*)currentObject;
				
				if(objectIsNew)
					zone = world->makeNewWaterZone();

				if(zone)
				{
					zone->location = *l;
					waterChanged = True;
					changed = True;
				}

				trackMode = Tracking;

				if(objectIsNew)
				{
					loseObject();
					objectIsNew = False;
					editWaterZone(zone);
				}
			}
			else
			{
				trackMode = Tracking;
				objectIsNew = False;
			}
		}
	}
	else if(trackMode == GetLink)
	{
		edittingZone->highlight = 1;
		trackFor(map, event, OT_WaterZone);

		if(event->buttons & Mouse::RightButton)
		{
			hint(0);
			trackMode = Tracking;
			edittingZone->highlight = 0;
			edittingZone = 0;
			loseObject();
			showLink = False;
		}
		else if(event->buttons & Mouse::LeftButton)
		{
			if(currentObject && (currentObject != edittingZone))
			{
				/*
				 * Make or break links...
				 */

				WaterZone* z = (WaterZone*) currentObject;

				Boolean connected = False;
				WaterZoneID id = world->waterNet.getID(edittingZone);

				for(int i = 0; i < z->connections.entries(); i++)
				{
					if(z->connections[i] == id)
						connected = True;
				}

				WaterZoneID id1 = world->waterNet.getID(z);

				if(connected)
				{	// Break connection
					removeWaterConnection(edittingZone, id1);
					removeWaterConnection(z, id);
				}
				else
				{	// Make connection
					edittingZone->connections.add(id1);
					z->connections.add(id);
				}

				waterChanged = True;
				changed = True;

			}
		}
	}
	else if(trackMode == GetOwner)
	{
		edittingZone->highlight = 1;
		trackFor(map, event, OT_Facility);

		if(event->buttons & Mouse::RightButton)
		{
			hint(0);
			trackMode = Tracking;
			edittingZone->highlight = 0;
			edittingZone = 0;
			loseObject();
			showLink = False;
		}
		else if(event->buttons & Mouse::LeftButton)
		{
			if(currentObject)
			{
				/*
				 * Make or break link to facility...
				 */

				Facility* f = (Facility*) currentObject;
				WaterZoneID id = world->waterNet.getID(edittingZone);
				FacilityID fid = world->facilities.getID(f);

				if(f->waterZone == id)
				{
					// Break connection
					f->waterZone = NoWaterZone;
					f->facilities &= ~(Facility::F_Port | Facility::F_LandingStage);
					f->waterCapacity = 0;

					removeWaterFacility(edittingZone, fid);
				}
				else
				{
					if(f->waterZone != NoWaterZone)
					{
						removeWaterFacility(&world->waterNet.zones[f->waterZone], fid);
					}


					f->waterZone = id;
					edittingZone->facilityList.add(fid);

					f->facilities |= Facility::F_LandingStage;
					if(f->facilities & Facility::F_City)
					{
						City* c = (City*) f;
						
						if( (c->type == Industrial) || (c->type == Mixed) )
						{
							f->facilities |= Facility::F_Port;
						}
					}

					f->waterCapacity = 0xff;
				}
					

			}
		}
	}
}

void CampaignEditControl::waterOptions		(const Point& p)
{
	static MenuChoice choose[] = {
		MenuChoice("WaterZone Options",			0),
		MenuChoice("-",								0),
		MenuChoice("Remove All Zones",			1),
		// MenuChoice("Auto Link",						2),
		MenuChoice("-",								0),
		MenuChoice("Cancel",							-1),
		MenuChoice(0,									0)
	};

	switch(menuSelect(0, choose, machine.mouse->getPosition()))
	{
	case 1:		// Remove All
		while(world->waterNet.zones.entries())
			world->deleteWaterZone(&world->waterNet.zones[0]);
		break;
	}
}

void CampaignEditControl::waterOffMap		()
{
}




