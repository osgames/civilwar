/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign AI: Event list Class
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "aic_even.h"
#include "myassert.h"
#include "city.h"
#include "generals.h"

#ifdef DEBUG
#include "ai_camp.h"
#endif

#define SORT_HIGH				// High priority events processed first
// #define SORT_LOW			// Low priority events processed first

inline Boolean AIC_EventList::eventCompare(AIC_Event* event, AIC_Priority priority)
{
#if defined(SORT_HIGH)
	return (event->priority > priority);
#elif defined(SORT_LOW)	// Lowest first
	return (event->priority < priority);
#else
	#error "Some sort method must be defined!"
#endif
}


/*
 * Constructor
 */

AIC_EventList::AIC_EventList()
{
	entry.next = 0;
	current = &entry;
	prev = 0;
}

/*
 * Destructor
 */

void AIC_EventList::clear()
{
	while(entry.next)
	{
		AIC_Event* event = entry.next;
		entry.next = event->next;
		delete event;
	}

	prev = 0;
	current = &entry;
}

/*
 * Create a new entry sorted by priority
 */

AIC_Event* AIC_EventList::add(AIC_Priority priority)
{
	AIC_Event* newEvent = new AIC_Event;

	ASSERT(newEvent != 0);

	newEvent->action = AIA_None;
	newEvent->where = 0;
	newEvent->who = 0;
	newEvent->priority = priority;
	newEvent->enemyCV = 0;
	newEvent->friendlyCV = 0;
	newEvent->nearPriority = 0;
	newEvent->closeFriend = 0;
	newEvent->canAttack = False;

	AIC_Event* event = &entry;

	while(event->next && eventCompare(event->next, priority))
		event = event->next;

	ASSERT(event != 0);

	newEvent->next = event->next;
	event->next = newEvent;

	return newEvent;
}

/*
 * Make it so that next call to next() will return the 1st element
 */

void AIC_EventList::rewind()
{
	current = &entry;
	prev = 0;
}

/*
 * Return the next element in value
 * Return True if value was returned
 */

Boolean AIC_EventList::next(AIC_Event*& value)
{
	ASSERT(current != 0);		// Should not call if returned 0

	if(current->next)
	{
		prev = current;
		current = current->next;

		value = current;
		return True;
	}
	else
		return False;
}

/*
 * Remove the last value from the list
 */

void AIC_EventList::remove()
{
	ASSERT(prev != 0);
	ASSERT(current != 0);

	prev->next = current->next;
	delete current;
	current = prev;
	prev = 0;				// Make it illegal to call remove again until next() is called
}

void AIC_Event::getLocation(Location& l) const
{

	ASSERT( (where != 0) || (who != 0) );

	if(where)
		l = where->location;
	else if(who)
		l = who->location;
}

CombatValue AIC_EventList::addEnemyCV() const
{
	CombatValue total = 0;

	const AIC_Event* event = entry.next;
	while(event)
	{
		total += event->enemyCV;
		event = event->next;
	}

	return total;
}

#ifdef DEBUG

void AIC_Event::print()
{
	const char* whereText;
	const char* extraText;

	if(where)
		whereText = where->getNameNotNull();
	else if(who)
		whereText = who->getName(True);
	else
		whereText = "-";

	if(canAttack)
		extraText = "+";
	else if(closeFriend)
		extraText = closeFriend->getNameNotNull();
	else
		extraText = "-";

	aiLog.printf("%3d %5ld %5ld %3d %s %-18s %-18s",
		(int) priority,
		(long) enemyCV,
		(long) friendlyCV,
		(int) nearPriority,
		actionNames[action],
		whereText,
		extraText);
}

void AIC_EventList::print()
{
	aiLog.printf("EventList:");

	AIC_Event* event = entry.next;
	while(event)
	{
		event->print();
		event = event->next;
	}
}

#endif
