/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Unallocated Regiment Window
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.17  1994/06/09  23:32:59  Steven_Green
 * Changed Fonts
 *
 * Revision 1.15  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.10  1994/04/22  22:14:22  Steven_Green
 * Icon Handling tidied up.
 *
 * Revision 1.9  1994/04/20  22:21:40  Steven_Green
 * Use TextWindow class instead of old Font class
 *
 * Revision 1.1  1994/02/17  14:57:44  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>

#include "calmain.h"
#include "system.h"
#include "screen.h"
#include "ob.h"
#include "text.h"
// #include "game.h"
#include "scroll.h"
#include "generals.h"
#include "colours.h"
#include "sprlib.h"

// #define Font_RegimentWindow Font_EMMA14
#define Font_RegimentWindow Font_EMFL10


class RegIconBase {
public:
	CAL_Regiments* regArea;

	RegIconBase(CAL_Regiments* r) { regArea = r; }
};

#if 0
/*
 * Scroll Bar
 */

class RegScrollUpIcon : public ScrollIcon, public RegIconBase {
public:
	RegScrollUpIcon(CAL_Regiments* set, Point p) :
		ScrollIcon(set, Rect(p, Point(16,19)), SCR_UScroll, SCR_UScrollP),
		RegIconBase(set)
	{
	}

	void execute(Event* event, MenuData* d);
};

class RegScrollDownIcon : public ScrollIcon, public RegIconBase {
public:
	RegScrollDownIcon(CAL_Regiments* set, Point p) :
		ScrollIcon(set, Rect(p, Point(16,19)), SCR_DScroll, SCR_DScrollP),
		RegIconBase(set)
	{
	}

	void execute(Event* event, MenuData* d);
};

void RegScrollUpIcon::execute(Event* event, MenuData* d)
{
	d = d;

	if(event->buttons)
	{
		if(regArea->top > 0)
		{
			regArea->top--;
			regArea->setRedraw();
		}
	}
}

void RegScrollDownIcon::execute(Event* event, MenuData* d)
{
	d = d;

	if(event->buttons)
	{
		if(regArea->top < (regArea->cal->top->freeRegimentCount() - 1))
		{
			regArea->top++;
			regArea->setRedraw();
		}
	}
}

#else

class RegimentScroll : public VerticalScrollBar, public RegIconBase {
public:
	RegimentScroll(CAL_Regiments* parent, const Rect& r); 
	void clickAdjust(int v);
	void getSliderSize(SDimension& from, SDimension& to, SDimension size);
};

RegimentScroll::RegimentScroll(CAL_Regiments* parent, const Rect& r) :
	VerticalScrollBar(parent, r),
	RegIconBase(parent)
{
}

void RegimentScroll::clickAdjust(int v)
{
	int newTop = regArea->top;

	if(v > 0)
	{
		if(v == +1)
			newTop--;
		else
			newTop -= regArea->nLines;

		if(newTop < 0)
			newTop = 0;
	}
	else if(v < 0)
	{
		if(v == -1)
			newTop++;
		else
			newTop += regArea->nLines;

		int maxTop = regArea->cal->top->freeRegimentCount() - regArea->nLines;
		if(maxTop < 0)
			maxTop = 0;

		if(newTop > maxTop)
			newTop = maxTop;
	}

	if(newTop != regArea->top)
	{
		regArea->top = newTop;
		regArea->setRedraw();
	}
}

void RegimentScroll::getSliderSize(SDimension& from, SDimension& to, SDimension size)
{
	int howMany = regArea->cal->top->freeRegimentCount();

	if(howMany <= regArea->nLines)
	{
		from = 0;
		to = from + size;
	}
	else
	{
		int newSize = (regArea->nLines * size) / howMany;

		from = (regArea->top * size) / howMany;
		to = from + newSize;
	}
}

#endif



class RegDisplayArea : public Icon, public RegIconBase, public CAL_InfoIcon {
	Unit* currentRegiment;
public:
	RegDisplayArea(CAL_Regiments* parent);
	void drawIcon();
	void execute(Event* event, MenuData* data);
	void showInfo(CAL* cal);
	int lineHeight() const { return fontSet(Font_RegimentWindow)->getHeight(); }
};

RegDisplayArea::RegDisplayArea(CAL_Regiments* parent) :
	Icon(parent, Rect(0, 0, CALREG_WIDTH - 16, CALREG_HEIGHT)),
	RegIconBase(parent)
{
	currentRegiment = 0;
}

/*
 * Draw the Regiment Box
 */

void RegDisplayArea::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
#if 0
	bm.fill(CAL_FillColour);
#else
	bm.fill(regArea->cal->fillPattern);
#endif
	TextWindow window(&bm, Font_RegimentWindow);

	bm.frame(Point(0,0), getSize(), CAL_Frame1Colour, CAL_Frame2Colour);

	int count = regArea->top;

	Regiment* r = regArea->cal->top->getFreeRegiments();
	while(r && count--)
	{
		ASSERT(r->getRank() == Rank_Regiment);

		r = (Regiment*) r->sister;
	}

	int y = 2;

	while(r && (y < (getH() - lineHeight() - 2)))
	{
		ASSERT(r->getRank() == Rank_Regiment);

		window.setPosition(Point(2, y));

		if(r->joining)
			window.setColours(Red4);
		else
			window.setColours(Black);

		window.wprintf("%s %s", r->getName(False), r->getTypeStr());

		if(r == regArea->cal->pickedUnit)
			bm.frame(2, y, getW(), lineHeight(), White);

		y += lineHeight();
		r = (Regiment*) r->sister;
	}

	machine.screen->setUpdate(bm);
}

void RegDisplayArea::showInfo(CAL* cal)
{
	if(currentRegiment)
		cal->infoWindow->showInfo(currentRegiment);
	else
		cal->infoWindow->clear();
}

/*
 * Click on box
 */

void RegDisplayArea::execute(Event* event, MenuData* data)
{
	if(event->overIcon)
	{
	 	CAL* cal = regArea->cal;
		int which = (event->where.getY() / lineHeight()) + regArea->top;
		Regiment* r = regArea->cal->top->getFreeRegiments();
		while(r && which--)
			r = (Regiment*) r->sister;

		if(cal->currentIcon != this)		// Force update if new on this icon
			currentRegiment = 0;

		cal->currentIcon = this;

		if(r != currentRegiment)
		{
			currentRegiment = r;
			showInfo(cal);
		}

		if(event->buttons)
		{

			if( (cal->getPickMode() == CAL::Dropping) &&
				 (event->buttons & Mouse::RightButton) )
			{
				cal->setPickMode();
				setRedraw();
				return;
			}

			/*
			 * Doesn't matter what mode they are in, if they reclick on
			 * a different regiment then it can cancel the previous pickup
			 * and select a new one.
			 */

#if defined(BATEDIT) || defined(CAMPEDIT)
			if(r)
#else
			if(r && !r->joining)
#endif
			{
				// cal->showInfo(r);

				if(event->buttons & Mouse::LeftButton)
				{
					Regiment* reg = (Regiment*) r;
					PointerIndex mPtr = calRegimentMice[reg->type.basicType];
					cal->doPickup(r, 0, calRegimentMice[r->type.basicType], getPosition() + getSize()/2);
				}
			}
		}
	}
}

/*
 * Outer Regiment Box
 */

/*
 * Constructor
 */

CAL_Regiments::CAL_Regiments(CAL* c) :
	IconSet(c, Rect(CALREG_X, CALREG_Y, CALREG_WIDTH, CALREG_HEIGHT)),
	CALBase(c)
{
	if(c->campaignMode)
	{
		top = 0;

#if 0
		icons = new IconList[4];

		icons[0] = new RegScrollUpIcon(this, Point(CALREG_WIDTH-16, 0));
		icons[1] = new RegScrollDownIcon(this, Point(CALREG_WIDTH-16, CALREG_HEIGHT-19));
		icons[2] = new RegDisplayArea(this);
		icons[3] = 0;
#else

		icons = new IconList[3];
		icons[0] = new RegDisplayArea(this);
		icons[1] = new RegimentScroll(this, Rect(CALREG_WIDTH-16, 0, 16, CALREG_HEIGHT));
		icons[2] = 0;
#endif

		nLines = CALREG_HEIGHT / fontSet(Font_RegimentWindow)->getHeight();
	}
}

void CAL_Regiments::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
#if 0
	bm.fill(CAL_FillColour);
	bm.frame(Point(0,0), getSize(), CAL_Frame1Colour, CAL_Frame2Colour);
#else
	bm.fill(cal->fillPattern);
#endif
	machine.screen->setUpdate(bm);

	IconSet::drawIcon();
}
