/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	CAL General Window
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.17  1994/06/09  23:32:59  Steven_Green
 * Changed Fonts
 *
 * Revision 1.15  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.10  1994/04/22  22:14:22  Steven_Green
 * Icon Handling tidied up.
 *
 * Revision 1.9  1994/04/20  22:21:40  Steven_Green
 * Use TextWindow class instead of old Font class
 *
 * Revision 1.1  1994/02/15  23:22:28  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include "calmain.h"
#include "system.h"
#include "screen.h"
#include "scroll.h"
#include "ob.h"
#include "text.h"
#include "game.h"
#include "generals.h"
#include "colours.h"
#include "sprlib.h"
#include "language.h"

#define Font_GeneralWindow Font_EMFL10		// was 15
#define Font_ModeIcon		Font_EMMA14


class GenIconBase {
public:
	CAL_Generals* genArea;

	GenIconBase(CAL_Generals* g) { genArea = g; }
};

#if 0
/*
 * Scroll Bar
 */

class GenScrollUpIcon : public ScrollIcon, public GenIconBase {
public:
	GenScrollUpIcon(CAL_Generals* set, Point p) :
		ScrollIcon(set, Rect(p, Point(16,19)), SCR_UScroll, SCR_UScrollP),
		GenIconBase(set)
	{
	}

	void execute(Event* event, MenuData* d);
};

class GenScrollDownIcon : public ScrollIcon, public GenIconBase {
public:
	GenScrollDownIcon(CAL_Generals* set, Point p) :
		ScrollIcon(set, Rect(p, Point(16,19)), SCR_DScroll, SCR_DScrollP),
		GenIconBase(set)
	{
	}

	void execute(Event* event, MenuData* d);
};

void GenScrollUpIcon::execute(Event* event, MenuData* d)
{
	d = d;

	if(event->buttons)
	{
		if(genArea->top > 0)
		{
			genArea->top--;
			genArea->setRedraw();
		}
	}
}

void GenScrollDownIcon::execute(Event* event, MenuData* d)
{
	d = d;

	if(event->buttons)
	{
		if(genArea->top < (genArea->cal->top->getFreeGenerals()->entries() - 1))
		{
			genArea->top++;
			genArea->setRedraw();
		}
	}
}

#else

class GeneralScroll : public VerticalScrollBar, public GenIconBase {
public:
	GeneralScroll(CAL_Generals* parent, const Rect& r); 
	void clickAdjust(int v);
	void getSliderSize(SDimension& from, SDimension& to, SDimension size);
};

GeneralScroll::GeneralScroll(CAL_Generals* parent, const Rect& r) :
	VerticalScrollBar(parent, r),
	GenIconBase(parent)
{
}

void GeneralScroll::clickAdjust(int v)
{
	int newTop = genArea->top;

	if(v > 0)
	{
		if(v == +1)
			newTop--;
		else
			newTop -= genArea->nLines;

		if(newTop < 0)
			newTop = 0;
	}
	else if(v < 0)
	{
		if(v == -1)
			newTop++;
		else
			newTop += genArea->nLines;

		int maxTop = genArea->cal->top->getFreeGenerals()->entries() - genArea->nLines;
		if(maxTop < 0)
			maxTop = 0;

		if(newTop > maxTop)
			newTop = maxTop;
	}

	if(newTop != genArea->top)
	{
		genArea->top = newTop;
		genArea->setRedraw();
	}
}

void GeneralScroll::getSliderSize(SDimension& from, SDimension& to, SDimension size)
{
	int howMany = genArea->cal->top->getFreeGenerals()->entries();

	if(howMany <= genArea->nLines)
	{
		from = 0;
		to = from + size;
	}
	else
	{
		int newSize = (genArea->nLines * size) / howMany;

		from = (genArea->top * size) / howMany;
		to = from + newSize;
	}
}

#endif


class GenDisplayArea : public Icon, public GenIconBase, public CAL_InfoIcon {
	General* currentGeneral;
public:
	GenDisplayArea(CAL_Generals* parent);
	void drawIcon();
	void execute(Event* event, MenuData* data);
	void showInfo(CAL* cal);

	int lineHeight() const { return fontSet(Font_GeneralWindow)->getHeight(); }
};

GenDisplayArea::GenDisplayArea(CAL_Generals* parent) :
	Icon(parent, Rect(0, 0, CALGEN_WIDTH - 16, CALGEN_HEIGHT)),
	GenIconBase(parent)
{
	currentGeneral = 0;
}

/*
 * Draw the General Box
 */

void GenDisplayArea::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));

	bm.fill(genArea->cal->fillPattern);

	TextWindow window(&bm, Font_GeneralWindow);

	bm.frame(Point(0,0), getSize(), CAL_Frame1Colour, CAL_Frame2Colour);

	int count = genArea->top;

	int y = 2;

	while((count < genArea->cal->top->getFreeGenerals()->entries()) && (y < (getH() - lineHeight() - 2)))
	{
		window.setPosition(Point(2, y));

		General* g = genArea->cal->top->getFreeGenerals()->find(count);

		if(g->inactive)
			window.setColours(Red4);
		else
			window.setColours(Black);

		window.wprintf("%s %s", language(lge_General), g->getName());
		// window.draw(g->getName());

		if(g == genArea->cal->pickedGeneral)
			bm.frame(2, y, getW(), lineHeight(), White);

		y += lineHeight();
		count++;
	}

	machine.screen->setUpdate(bm);
}

/*
 * Icons for ranks
 */

#if 0
static IconNumbers rankIcon[] = {
	I_CAL_Army1, 			// President
	I_CAL_Army1,			// Army
	I_CAL_Corps1,			// Corps
	I_CAL_Division1,		// Division
	I_CAL_Brigade1,		// Brigade
	I_CAL_Infantry			// Regiment
};
#endif

static PointerIndex rankMice[] = {
	M_Arrow,					// President
	M_CAL_Army1U,			// Army
	M_CAL_Corps1U,			// Corps
	M_CAL_Division1U,		// Division
	M_CAL_Brigade1U,		// Brigade
	M_CAL_Infantry			// Regiment
};

void GenDisplayArea::showInfo(CAL* cal)
{
	if(currentGeneral)
		cal->infoWindow->showInfo(currentGeneral);
	else
		cal->infoWindow->clear();
}

/*
 * Click on box
 */

void GenDisplayArea::execute(Event* event, MenuData* data)
{
	if(event->overIcon)
	{
 		CAL* cal = genArea->cal;

		int which = (event->where.getY() / lineHeight()) + genArea->top;
		General* g = cal->top->getFreeGenerals()->find(which);

		if(cal->currentIcon != this)	// Force update if new on this icon
			currentGeneral = 0;

		cal->currentIcon = this;
		if(g != currentGeneral)
		{
			currentGeneral = g;
			showInfo(cal);
		}

		if(event->buttons)
		{
			/*
			 * Cancel Drop mode with Right Mouse
			 */

			if(cal->getPickMode() == CAL::Dropping)
			{
				if(event->buttons & Mouse::RightButton)
				{
					cal->setPickMode();
					setRedraw();
				}
			}
			else	// Must be pick mode
			{

				/*
			 	 * Doesn't matter what mode they are in, if they reclick on
			 	 * a different general then it can cancel the previous pickup
			 	 * and select a new one.
			 	 */

#if defined(BATEDIT) || defined(CAMPEDIT)
				if(g)
#else
				if(g && !g->inactive)
#endif
				{
					if(event->buttons & Mouse::LeftButton)
					{
						PointerIndex icon = rankMice[g->getRank()];

						if(g->side == SIDE_USA)
							icon = (PointerIndex) (icon + M_CAL_Army2 - M_CAL_Army1);
							// INCREMENT(icon);

						cal->doPickup(0, g, icon, getPosition() + getSize()/2);
					}
				}
#if defined(CAMPEDIT) || defined(BATEDIT)
				else
					cal->doPickup(0, 0, M_CAL_Brigade1, getPosition() + getSize() / 2);
#endif
			}

		}
	}
}

/*
 * Outer General Box
 */

/*
 * Constructor
 */

CAL_Generals::CAL_Generals(CAL* c) :
	IconSet(c, Rect(CALGEN_X, CALGEN_Y, CALGEN_WIDTH, CALGEN_HEIGHT)),
	CALBase(c)
{
	if(c->campaignMode)
	{
		top = 0;

#if 0
		icons = new IconList[4];

		icons[0] = new GenScrollUpIcon(this, Point(CALGEN_WIDTH-16, 0));
		icons[1] = new GenScrollDownIcon(this, Point(CALGEN_WIDTH-16, CALGEN_HEIGHT-19));
		icons[2] = new GenDisplayArea(this);
		icons[3] = 0;
#else

		icons = new IconList[3];
		icons[0] = new GenDisplayArea(this);
		icons[1] = new GeneralScroll(this, Rect(CALGEN_WIDTH-16, 0, 16, CALGEN_HEIGHT));
		icons[2] = 0;

#endif

		nLines = (CALGEN_HEIGHT - 4) / fontSet(Font_GeneralWindow)->getHeight();
	}
}

void CAL_Generals::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
#if 0
	bm.fill(CAL_FillColour);
	bm.frame(Point(0,0), getSize(), CAL_Frame1Colour, CAL_Frame2Colour);
#else
	bm.fill(cal->fillPattern);
#endif
	machine.screen->setUpdate(bm);

	IconSet::drawIcon();
}

