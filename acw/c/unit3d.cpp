/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Display of units on battlefield
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/09/23  13:28:28  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdlib.h>
#include "unit3d.h"
#include "generals.h"
#include "data3d.h"
#include "system.h"
#include "batldata.h"
#include "map3d.h"
#include "batlib.h"
#include "colours.h"
#ifdef DEBUG
#include "bat_opt.h"
#endif
#include "game.h"

const int HorseWalkAnimations = 6;
const int HorseChargeAnimations = 6;
const int InfantryWalkAnimations = 4;
const int InfantryReloadAnimations = 2;
const int InfantryChargeAnimations = 4;
#ifdef EMMA_GRAPHICS		// Emma's graphics
const int LimberWalkAnimations = 2;
#endif

/*
 * Sprite look up tables
 */


static UBYTE infantryReloadSprites[5][8] = {
	{  8,10,12,14, 0, 2, 4, 6 },
	{ 24,26,28,30,16,18,20,22 },
	{ 40,42,44,46,32,34,36,38 },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff }
};

static UBYTE infantryFireSprites[5][8] = {
	{  4, 5, 6, 7, 0, 1, 2, 3 },
	{ 12,13,14,15, 8, 9,10,11 },
	{ 20,21,22,23,16,17,18,19 },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff }
};

static UBYTE infantryMarchSprites[5][8] = {
	{  16, 20, 24, 28,  0,  4,  8, 12 }, 
	{  48, 52, 56, 60, 32, 36, 40, 44 },
	{  80, 84, 88, 92, 64, 68, 72, 76 },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff }
};

static UBYTE infantryStandSprites[5][8] = {
	{  4, 5, 6, 7, 0, 1, 2, 3 },
	{ 12,13,14,15, 8, 9,10,11 },
	{ 20,21,22,23,16,17,18,19 },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff }
};

static UBYTE infantryChargeSprites[5][8] = {
	{  16, 20, 24, 28,  0,  4,  8, 12 }, 
	{  48, 52, 56, 60, 32, 36, 40, 44 },
	{  80, 84, 88, 92, 64, 68, 72, 76 },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff }
};

static UBYTE horseStandSprites[5][8] = {
	{  4, 5, 6, 7, 0, 1, 2, 3 },
	{ 12,13,14,15, 8, 9,10,11 },
	{ 20,21,22,23,16,17,18,19 },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff }
};

static UBYTE horseMarchSprites[5][8] = {
	{  24, 30, 36, 42,  0,  6, 12, 18 },
	{  72, 78, 84, 90, 48, 54, 60, 66 },
	{ 120,126,132,138, 96,102,108,114 },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff }
};

static UBYTE cavalryStandSprites[5][8] = {
	{  4, 5, 6, 7, 0, 1, 2, 3 },
	{ 12,13,14,15, 8, 9,10,11 },
	{ 20,21,22,23,16,17,18,19 },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff }
};

static UBYTE cavalryMarchSprites[5][8] = {
	{  24, 30, 36, 42,  0,  6, 12, 18 },
	{  72, 78, 84, 90, 48, 54, 60, 66 },
	{ 120,126,132,138, 96,102,108,114 },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff }
};


#ifdef EMMA_GRAPHICS		// Emma's graphics

static UBYTE artUnlimberSprites[5][8] = {
	{  4, 5, 6, 7, 0, 1, 2, 3 },
	{ 12,13,14,15, 8, 9,10,11 },
	{ 20,21,22,23,16,17,18,19 },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff }
};

static UBYTE artLimberedSprites[5][8] = {
	{  8,10,12,14, 0, 2, 4, 6 },
	{ 24,26,28,30,16,18,20,22 },
	{ 40,42,44,46,32,34,36,38 },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff }
};

#else		// Strategy 1st's graphics

static UBYTE artUnlimberSprites[5][8] = {
	{  2, 3, 4, 5, 6, 7, 0, 1 },
	{ 10,11,12,13,14,15, 8, 9 },
	{ 18,19,20,21,22,23,16,17 },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff }
};

static UBYTE artLimberedSprites[5][8] = {
	{  6, 7, 0, 1, 2, 3, 4, 5 },
	{ 14,15, 8, 9,10,11,12,13 },
	{ 22,23,16,17,18,19,20,21 },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff },
	{ 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff }
};

#endif


static SpriteIndex limberedSprites[] = {
	SPR_SmoothboreLimber,
	SPR_LightLimber,
	SPR_RifledLimber
};

static SpriteIndex canonSprites[] = {
	SPR_SmoothboreCanon,
	SPR_LightCanon,
	SPR_RifledCanon
};


AnimationTable cannonSmoke = {
	15, SPR_c_fire_1,
	{
		{   75,   90,  105,    0,   15,   30,   45,   60 },
		{  195,  210,  225,  120,  135,  150,  165,  180 },
		{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff },
		{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff },
		{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff }
	}
};

AnimationTable gunSmoke = {
	6, SPR_gunfire1,
	{
		{   30,   36,   42,    0,    6,   12,   18,   24 },
		{   78,   84,   90,   48,   54,   60,   66,   72 },
		{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff },
		{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff },
		{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff }
	}
};

/*
 * Spacing
 */

struct DeltaSpacing {
	Cord3D hSpacing;
	Cord3D vSpacing;

	Cord3D xh;		// Horizontal Delta
	Cord3D yh;

	Cord3D xv;		// Vertical Delta
	Cord3D yv;

	void set(Cord3D h, Cord3D v, Wangle facing);
};

void DeltaSpacing::set(Cord3D h, Cord3D v, Wangle facing)
{
	hSpacing = h;
	vSpacing = v;

	xh = msin(h, facing);
	yh = -mcos(h, facing);

	xv = -mcos(v, facing);
	yv = -msin(v, facing);
}

/*====================================================================
 * Generic Unit
 */

UnitBattle::UnitBattle(Unit* owner)
{
	unit = owner;
	formation = Form_Line;
	animation = 0;
	animationDistance = 0;
#ifdef BATEDIT
	where = owner->location;
#else
	where = owner->location - battle->campaignLocation;
#endif

	formationFacing = owner->facing;
	menFacing = owner->facing;
#if 0
	bSeperated = owner->isCampaignIndependant();
	bHasSeperated = owner->hasCampaignIndependant();
	bReallySeperated = owner->isCampaignReallyIndependant();
	bHasReallySeperated = owner->hasCampaignReallyIndependant();
#else
	bSeperated = owner->isCampaignDetached();
	bHasSeperated = owner->hasCampaignDetached();
	bReallySeperated = owner->isCampaignReallyDetached();
	bHasReallySeperated = owner->hasCampaignReallyDetached();
#endif
	bJoining = False;
	followingOrder = False;

#ifdef BATEDIT
	isDead = False;
#else
	isDead = (owner->getStrength() == 0);
#endif
	routed = False;

	/*
	 * If on the battlefield, then Stand
	 * Otherwise give them an order to advance to the centre of the battlefield!
	 */

	// if(owner->isCampaignReallyIndependant())
	if(owner->isCampaignReallyDetached())
	{
		battleOrder = BattleOrder(ORDER_Advance, Location(0,0));
	}
	else
		battleOrder = BattleOrder(ORDER_Stand, where);

	// lastBattleTime = 0;
	following = 0;
	followHow = Follow_None;

	inCombat = 0;

	/*
	 *  AI Data
	 */

	longRoute = False;
	AIFlag = False;
	AI_action = 0;
	// AIdestination = 0;
	reassign = 0;
	enemy = 0;
	
}

UnitBattle::~UnitBattle()
{
}

#if 0
void UnitBattle::adjustInCombat(int change)
{
	UnitBattle* info = this;

	do
	{
		if((change < 0) && info->inCombat == 0)
		{
#ifdef DEBUG
			GeneralError("%s's inCombat counter is 0", unit->getName(True));
#endif
			return;
		}

		info->inCombat += change;

		if(info->bSeperated)
			break;

		Unit* u = info->unit;
		if(u->getRank() > Rank_Corps)
			info = u->superior->battleInfo;
		else
			break;
	} while(info);
}
#endif

void UnitBattle::setInCombat()
{
	inCombat = True;

	Unit* u = unit;

	while( (u->getRank() > Rank_Corps) && u->battleInfo && !u->battleInfo->bSeperated)
	{
		u = u->superior;

		if(u->battleInfo)
			u->battleInfo->inCombat = True;
	}
}

void UnitBattle::clearInCombat()
{
	inCombat = False;

	Unit* u = unit->superior;
	while(u->battleInfo && (u->getRank() >= Rank_Corps))
	{
		Unit* c = u->child;
		u->battleInfo->inCombat = False;
		while(c)
		{
			UnitBattle* info = c->battleInfo;

			if(info && !info->bSeperated && !info->bJoining && info->inCombat)
			{
				u->battleInfo->inCombat = True;
				return;
			}

			c = c->sister;
		}

		u = u->superior;
	}
}


void Unit::draw3D(System3D& drawData)
{
	if(battleInfo && !battleInfo->isDead)
		battleInfo->draw3D(drawData);
}


/*
 * Find out how much physical space a unit is taking up
 */

void UnitBattle::getArea(Distance& w, Distance& h)
{
	/*
	 * Temporarily, just fill it with preset values
	 */

	switch(unit->rank)
	{
	case Rank_President:
	case Rank_Army:
		w = Mile(2); // 4
		h = Mile(2); // 4
		break;
	case Rank_Corps:
		w = Mile(1); // 2
		h = Mile(1); // 2
		break;
	case Rank_Division:
		w = Yard(1300 ); // 2270);
		h = Yard(1300 ); // 2270);
		break;
	case Rank_Brigade:
		w = Yard(600 ); // 740);
		h = Yard(600 ); // 740);
		break;
	case Rank_Regiment:		// Should not happen because regiment has its own function
#ifdef DEBUG
		throw GeneralError("UnitBattle::getArea() called for regiment");
#endif
		break;
	}
}

Boolean UnitBattle::OrderPresent()
{
	SentBattleOrder* order;

	if ( battleOrderList.entries())
	{
		order = battleOrderList.find();

		if ( order->mode < ORDER_Stand || order->mode > ORDER_Defend )
		{
	 		return True;
		}
	}

	return False;
}

/*=================================================================
 * Commanders...
 */

UnitBattle*  President::makeBattleInfo()
{
	return 0;
}

UnitBattle*  Unit::makeBattleInfo()
{
	if(battleInfo)
		delete battleInfo;
	battleInfo = new CommanderBattle(this);
	return battleInfo;
}

UnitBattle* Unit::setupBattle()
{
	Boolean hasChildren = False;

	Unit* u = child;
	while(u)
	{
		if(!u->isCampaignReallyDetached())
			if(u->setupBattle())
				hasChildren = True;
		u = u->sister;
	}

	if(hasChildren || (rank == Rank_Regiment))
		return makeBattleInfo();
	else
	{
		if(battleInfo)
		{
			delete battleInfo;
			battleInfo = 0;
		}
		return 0;
	}
}

void Unit::clearupBattle()
{
	delete battleInfo;
	battleInfo = 0;

	Unit* u = child;
	while(u)
	{
		u->clearupBattle();
		u = u->sister;
	}
}

CommanderBattle::CommanderBattle(Unit* owner) :
	UnitBattle(owner)
{
}

CommanderBattle::~CommanderBattle()
{
	unDisplay();
}

void CommanderBattle::clearUp()
{
	if(sprite[0].spriteBlock)
	{
 		sprite[0].spriteBlock->release();
		sprite[0].spriteBlock = 0;
	}
	if(sprite[1].spriteBlock)
	{
		sprite[1].spriteBlock->release();
		sprite[1].spriteBlock = 0;
	}
}


/*
 * Display a commander as a horse and flag
 */

void CommanderBattle::draw3D(System3D& drawData)
{
	if(drawData.view.isOnDisplay(where))
	{
		unLink();
		clearUp();

		Point3D p;

		p.x = DistToBattle(where.x);
		p.z = DistToBattle(where.y);
		p.y = battle->grid->getHeightView(drawData.view, p.x, p.z);

		drawData.view.transform(p);


		if(!sprElements)
			sprElements = new SpriteID[2];
		SpriteID* nextShape = sprElements;

		/*
	 	 * Work out which flag to use into sprite[0]
	 	 */

		SpriteIndex sprNum = SPR_batflag + (unit->getRank() - 1) * 4;

		if(unit->getSide() == SIDE_USA)
			sprNum += 2;

		// Highlight flag if it is the current unit

		if(unit == battle->currentUnit)
			sprNum++;

		sprite[0].spriteBlock = battle->spriteLib->read(sprNum);

		Point3D p1 = p;
		p1.x -= sprite[0].spriteBlock->anchorX;
		p1.y -= sprite[0].spriteBlock->anchorY;

		nextShape++->element = battle->spriteList->add(&sprite[0], p1);

		if(drawData.view.zoomLevel <= Zoom_2)
		{
			// Bangle dir = wangleToBangle(menFacing + 0x1000 - drawData.view.yRotate);
			UBYTE octant = facingOctant(menFacing, drawData.view.yRotate);

#ifdef NO_CAV_4MILE
			if(drawData.view.zoomLevel <= Zoom_1)
			{
#endif
				sprNum = cavalryMarchSprites[drawData.view.zoomLevel][octant];
				sprNum += animation % HorseWalkAnimations;
	 			if(unit->getSide() == SIDE_CSA)
	 				sprNum += Cav_CSA_Walk;
	 			else
	 				sprNum += Cav_USA_Walk;
#ifdef NO_CAV_4MILE
			}
			else
			{
				sprNum = horseMarchSprites[drawData.view.zoomLevel][octant];
				sprNum += Horse_Walk;
			}
#endif

			sprite[1].spriteBlock = battle->spriteLib->read(sprNum);

			Point3D p2 = p;
			p2.x -= sprite[1].spriteBlock->anchorX;
			p2.y -= sprite[1].spriteBlock->anchorY;
			nextShape++->element = battle->spriteList->add(&sprite[1], p2);

			// Move flag up a bit

			p1.y -= sprite[1].spriteBlock->anchorY / 2;
		}

		if(!inDisplayList)
		{
			battle->displayedObjects->add(this);
			inDisplayList = True;
		}

		battle->displayedCommanders->add(unit, Point(p.x, p.y));
	}
	else
		unDisplay();
}

/*===============================================================
 * Brigade Code
 */

UnitBattle*  Brigade::makeBattleInfo()
{
	if(battleInfo)
		delete battleInfo;
	battleInfo = new BrigadeBattle(this);
	return battleInfo;
}

 
BrigadeBattle::BrigadeBattle(Unit* owner) :
	CommanderBattle(owner)
{
	flipped = False;
}

/*===============================================================
 * Regiment Code
 */

/*
 * Constructor for regiment
 */

RegimentBattle::RegimentBattle(Regiment* r) :
	UnitBattle(r)
{
	// If coming from the campaign game then it has a superior but no location

	if(r->superior && r->superior->battleInfo)
	{
		where = r->superior->battleInfo->where;
	}

	expFrame = 0;
	soundCount = 0;

	animation = 0;
	previousFacing = r->facing;

	setFormationRows(True);
	// rows = 0;		// Start in actual formation
	// columns = 0;
	wantFormation = formation;

	target = 0;
	combatMode = CM_None;
	disordered = False;
	moving = False;
	doneDecideCheck1 = False;
	doneDecideCheck2 = False;
	canTurn = False;

	fired = False;
	loadStatus = 0;
	fireCount = 0;
	meleeTime = 0;

	movedDistance = 0;

	/*
	 * Set up initial corpse count
	 */

	Strength corpseRemainder = r->strength % menPerSprite();

	if(corpseRemainder)
		corpseCount = menPerSprite() - corpseRemainder;
	else
		corpseCount = 0;

	if(r->type.basicType == Cavalry)
		limbMounted = True;
	else
		limbMounted = False;
	wantLimbMounted = False;
	wantUnLimbMounted = False;
	// limberCount = USHRT_MAX;
	limberCount = 0;					// Master's edition change
}

Strength RegimentBattle::menPerSprite() const
{
	Regiment* r = getRegiment();
	if(r->type.basicType == Infantry)
		return MenPerInfantrySprite;
	else if(r->type.basicType == Cavalry)
		return MenPerCavalrySprite;
	else
		return MenPerArtillerySprite;
}

CavInfantryBattle::CavInfantryBattle(Regiment* r) :
	RegimentBattle(r),
	drawInfo(r->getSide(), r->type.basicType)
{
}

#ifndef BATEDIT
Boolean CavInfantryBattle::isMountedInfantry() const
{
	Regiment* r = getRegiment();

	if(r->type.basicType == Cavalry)
		return (r->type.subType == Cav_RegularMounted) ||
				 (r->type.subType == Cav_MilitiaMounted);
	else
		return False;
}

#endif

ArtilleryBattle::ArtilleryBattle(Regiment* r) :
	RegimentBattle(r),
	drawInfo(r->getSide(), r->type.basicType)
{
}

/*
 * Destructor
 */

RegimentBattle::~RegimentBattle()
{
}


UnitBattle*  Regiment::makeBattleInfo()
{
	if(battleInfo)
	{
		delete battleInfo;
		battleInfo = 0;
	}

	switch(type.basicType)
	{
	case Infantry:
		battleInfo = new CavInfantryBattle(this);
		break;
	case Cavalry:
		battleInfo = new CavInfantryBattle(this);
		break;
	case Artillery:
		if(type.subType != Art_Siege)
			battleInfo = new ArtilleryBattle(this);
		break;
	default:
		battleInfo = 0;
		break;
	}
	return battleInfo;
}

void RegimentBattle::getArea(Distance& w, Distance& h)
{
	Regiment* r = getRegiment();
	switch(r->type.basicType)
	{
	case Infantry:
		w = columns * Yard(22);
		h = rows * Yard(22);
		break;
	case Cavalry:
		w = columns * Yard(28);
		h = rows * Yard(56);
		break;
	case Artillery:
		w = columns * Yard(80);
		h = rows * Yard(125);
		break;
#ifdef DEBUG
	default:
		throw GeneralError("RegimentBattle::getArea()... illegal basicType");
		break;
#endif
	}
}

DrawnRegiment::DrawnRegiment(Side s, UBYTE t)
{
	side = s;
	type = t;
}

void DrawnRegiment::draw(const System3D& drawData, const Point3D& where)
{
	SDimension y = where.y - height;

	Colour sideColour;

	if(side == SIDE_CSA)
		sideColour = Grey5;	// Grey
	else
		sideColour = Blue7;	// Blue

	/*
	 * Single pixel, then make it the side's colour
	 */


	if(height == 1)
	{
		drawData.bitmap->plot(Point(where.x, y), sideColour);
		return;
	}

	int h = height;

	switch(type)
	{
		case Infantry:
		{
			const Colour hatCol = BlueGrey3;
			const Colour faceCol = RedBrownb;

			/*
	 		 * Start with hat colour
	 		 */

			drawData.bitmap->plot(Point(where.x, y++), hatCol);
			--h;

			/*
	 		 * Then a pixel of face
	 		 */

			if(h > 1)
			{
				drawData.bitmap->plot(Point(where.x, y++), faceCol);
				h--;
			}

			/*
		 	 * Finish off with side's colour
		 	 */

			if(h)
				drawData.bitmap->VLine(where.x, y, h, sideColour);

			break;
		}

		case Cavalry:
		{
			const Colour horseColour = RedBrown3;

			drawData.bitmap->plot(Point(where.x, y++), sideColour);
			h--;
			drawData.bitmap->VLine(where.x, y, h, horseColour);

			break;
		}

		case Artillery:
		{
			const Colour limberColour = RedBrown7;

			drawData.bitmap->box(where.x, y, h, h, limberColour);
			drawData.bitmap->plot(Point(where.x + h/2, y + h/2), sideColour);

			break;
		}
	}
}

/*================================================================
 * Infantry and Cavalry Display
 */


CavInfantryBattle::~CavInfantryBattle()
{
	unDisplay();
}

void CavInfantryBattle::clearUp()
{	
	// RegimentBattle::reset();

	if(sprite.spriteBlock)
	{
		sprite.spriteBlock->release();
		sprite.spriteBlock = 0;
	}

	if(expSprite.spriteBlock)
	{
		expSprite.spriteBlock->release();
		expSprite.spriteBlock = 0;
	}
}


void CavInfantryBattle::draw3D(System3D& drawData)
{
	if(!drawData.view.isOnDisplay(where))
	{
		unDisplay();
		fired = False;
		expFrame = 0;
		return;
	}

	Regiment* r = getRegiment();

	unLink();
	clearUp();

	Shape3D* shape;

	Cord3D adjustX;
	Cord3D adjustY;

	Cord3D expAdjustX;
	Cord3D expAdjustY;

	Boolean hasExplosions = False;

	if(fired)				// Reset explosion back to start if fired again
		expFrame = 0;

	if(drawData.view.zoomLevel > Zoom_2)
	{
#ifdef BATEDIT
		if(drawData.view.zoomLevel > Zoom_8)
			return;
#endif
		drawInfo.height = 2 << (Zoom_8 - drawData.view.zoomLevel);
		drawInfo.side = unit->getSide();

		adjustX = 0;
		adjustY = 0;

		shape = &drawInfo;

	}
	else
	{
		/*
		 * Note: The angle here should be the direction that the men are facing
		 * which might be different to the direction the regiment is facing
		 */

		UBYTE octant = facingOctant(menFacing, drawData.view.yRotate);

		SpriteIndex snum;

		if(isMounted())
		{
#ifdef NO_CAV_4MILE
			if(drawData.view.zoomLevel == Zoom_2)
			{
				snum = horseMarchSprites[drawData.view.zoomLevel][octant];
				if(moving)
					snum += animation % HorseWalkAnimations;
				snum += Horse_Walk;
			}
			else
#endif
			if(!moving)
			{
				snum = cavalryStandSprites[drawData.view.zoomLevel][octant];

			 	if(r->getSide() == SIDE_CSA)
			 		snum += Cav_CSA_Stand;
			 	else
			 		snum += Cav_USA_Stand;
			}
			else if(battleOrder.mode == ORDER_Attack)
			{
				UBYTE offset = cavalryMarchSprites[drawData.view.zoomLevel][octant];
				snum = (animation % HorseChargeAnimations) + offset;

			 	if(r->getSide() == SIDE_CSA)
			 		snum += Cav_CSA_Charge;
			 	else
			 		snum += Cav_USA_Charge;
			}
			else
			{
				UBYTE offset = cavalryMarchSprites[drawData.view.zoomLevel][octant];
				snum = (animation % HorseWalkAnimations) + offset;

			 	if(r->getSide() == SIDE_CSA)
			 		snum += Cav_CSA_Walk;
			 	else
			 		snum += Cav_USA_Walk;
			}
		}
		else	// Not mounted
		if(!moving)
		{
#if !defined(BATEDIT)

#if defined(DEBUG)
			if(showInfantryLoading)
			{
				UBYTE offset = infantryReloadSprites[drawData.view.zoomLevel][octant];

				snum = offset + (animation % InfantryReloadAnimations);

				if(r->getSide() == SIDE_USA)
					snum += INF_USA_Loading;
				else	// Assume USA
					snum += INF_CSA_Loading;
			}
			else
#endif

			// Reworked so reloading sprites displayed more often
			if( (combatMode == CM_Firing) &&
				(fired || expFrame || fireCount || (loadStatus >= LoadShowAnimation)) )
			{
				UBYTE offset = infantryFireSprites[drawData.view.zoomLevel][octant];

				snum = offset;

				if(r->getSide() == SIDE_USA)
					snum += INF_USA_firing;
				else	// Assume USA
					snum += INF_CSA_firing;
			}
			else if(canLoadFire() && !fireCount && (loadStatus < LoadShowAnimation))
			{
				UBYTE offset = infantryReloadSprites[drawData.view.zoomLevel][octant];

				snum = offset + ((animation /8) % InfantryReloadAnimations);

				if(r->getSide() == SIDE_USA)
					snum += INF_USA_Loading;
				else	// Assume USA
					snum += INF_CSA_Loading;
			}
			else
#endif	// BATEDIT
			{
				UBYTE offset = infantryStandSprites[drawData.view.zoomLevel][octant];

				snum = offset;

				if(r->getSide() == SIDE_CSA)
					snum += INF_CSA_Stand;
				else	// Assume USA
					snum += INF_USA_Stand;
			}
		}
		else if(battleOrder.mode == ORDER_Attack)
		{
			UBYTE offset = infantryChargeSprites[drawData.view.zoomLevel][octant];

			snum = (animation % InfantryChargeAnimations) + offset;
			if(r->getSide() == SIDE_CSA)
				snum += INF_CSA_Charging;
			else	// Assume USA
				snum += INF_USA_Charging;
		}
		else
		{
			UBYTE offset = infantryMarchSprites[drawData.view.zoomLevel][octant];

			snum = (animation % InfantryWalkAnimations) + offset;
			if(r->getSide() == SIDE_CSA)
				snum += INF_CSA_March;
			else	// Assume USA
				snum += INF_USA_March;
		}


		sprite.spriteBlock = battle->spriteLib->read(snum);

		adjustX = sprite.spriteBlock->anchorX;
		adjustY = sprite.spriteBlock->anchorY;

		shape = &sprite;

		/*
		 * Set up explosion shape if necessary
		 */

		if(fired || expFrame)
		{
			UBYTE sprOffset = gunSmoke.sprites[drawData.view.zoomLevel][octant];
			if(sprOffset != 0xff)
			{
				SpriteIndex sprNum = sprOffset + gunSmoke.baseSprite + expFrame;
				expSprite.spriteBlock = battle->spriteLib->read(sprNum);
				expAdjustX = expSprite.spriteBlock->anchorX;
				expAdjustY = expSprite.spriteBlock->anchorY;
				hasExplosions = True;

				expFrame++;
				if(expFrame >= gunSmoke.frames)
					expFrame = 0;
			}
		}
	}

	if(!hasExplosions)
		expFrame = 0;

	Wangle a1 = formationFacing;

	/*
	 * Work out top left coordinate and delta values
	 */

	DeltaSpacing delta;

	Cord3D dx;
	Cord3D dy;

	if(isMounted())
	{
		dx = DistToBattle(Yard(28));
		dy =DistToBattle(Yard(56));
	}
	else
	{
		dx = DistToBattle(Yard(22));
		dy = DistToBattle(Yard(22));
	}

	if(routed)
	{
		dx += dx/2;
		dy += dy/2;
	}
	
	delta.set(dx, dy, a1);


	/*
	 * Work out the formation grid size
	 *
	 * This is bodge code for testing...
	 *
	 * For now put them in line (2 lines of x men)
	 */

	int totalSprites;
	int totalMenSprites;
	
	if(isMounted())
		totalMenSprites = r->strength / MenPerCavalrySprite;
	else
		totalMenSprites = r->strength / MenPerInfantrySprite;
	UBYTE menHigh = rows;
	UBYTE menWide = columns;

	if(hasExplosions)
		totalSprites = totalMenSprites + menWide;
	else
		totalSprites = totalMenSprites;

	BattleLocation l = where;

	if(menWide > 1)
	{
		l.x -= (delta.xh * (menWide-1)) / 2;
		l.z -= (delta.yh * (menWide-1)) / 2;
	}
	if(menHigh > 1)
	{
		l.x -= (delta.xv * (menHigh-1)) / 2;
		l.z -= (delta.yv * (menHigh-1)) / 2;
	}

	/*
	 * Set up shapeList
	 */

	if(sprElements)
		delete[] sprElements;
	sprElements = new SpriteID[totalSprites];
	SpriteID* nextShape = sprElements;

	RandomNumber random(r->strength);

	for(int row = 0; row < menHigh; row++)
	{
		BattleLocation l1 = l;

		int col;
		if(totalMenSprites < menWide)
		{
			col = totalMenSprites;
			int missingMen = menWide - totalMenSprites;

			/*
			 * Adjust coordinate so men in centre of row
			 */

			l1.x += (missingMen * delta.xh) / 2;
			l1.z += (missingMen * delta.yh) / 2;
		}
		else
			col = menWide;
		totalMenSprites -= col;

		while(col--)
		{
			Point3D p, p1;

			p.x = l1.x;
			p.z = l1.z;

			if(routed)
			{
				p.x += random(delta.hSpacing/2) - (delta.hSpacing / 4);
				p.z += random(delta.hSpacing/2) - (delta.hSpacing / 4);
			}

			p.y = battle->grid->getHeightView(drawData.view, p.x, p.z);

			p1 = p;




			drawData.view.transform(p);

			p.x -= adjustX;
			p.y -= adjustY;

			nextShape++->element = battle->spriteList->add(shape, p);

			if(hasExplosions)
			{
				p1.y += DistToBattle(Yard(2));
				drawData.view.transform(p1);

				p1.x -= expAdjustX;
				p1.y -= expAdjustY;

				nextShape++->element = battle->spriteList->add(&expSprite, p1);
			}

			l1.x += delta.xh;
			l1.z += delta.yh;
		}

		hasExplosions = False;			// Only front line to have explosions

		l.x += delta.xv;
		l.z += delta.yv;
	}

	if(!inDisplayList)
	{
		battle->displayedObjects->add(this);
		inDisplayList = True;
	}

	fired = False;
}


/*================================================================
 * Artillery Display
 */

ArtilleryBattle::~ArtilleryBattle()
{
	unDisplay();
}

void ArtilleryBattle::clearUp()
{	
	for(int i = 0; i < ArtillerySpriteCount; i++)
		images[i].release();

	expSprite.release();
}

struct AddSprite {
	Cord3D xh;
	Cord3D yh;
	Cord3D xv;
	Cord3D yv;
	BattleLocation mid;
	SpriteID* nextShape;
	System3D* drawData;

	void add(Sprite3D* sprite, short xOffset, short yOffset);
};

void AddSprite::add(Sprite3D* sprite, short xOffset, short yOffset)
{
	Point3D p;
	p.x = mid.x;
	p.z = mid.z;
	p.x += (xh * xOffset) / 256;
	p.z += (yh * xOffset) / 256;
	p.x += (xv * yOffset) / 256;
	p.z += (yv * yOffset) / 256;
	p.y = battle->grid->getHeightView(drawData->view, p.x, p.z);
	drawData->view.transform(p);
	p.x -= sprite->spriteBlock->anchorX;
	p.y -= sprite->spriteBlock->anchorY;
	nextShape++->element = battle->spriteList->add(sprite, p);
}


void ArtilleryBattle::draw3D(System3D& drawData)
{
	if(!drawData.view.isOnDisplay(where))
	{
		unDisplay();
		fired = False;
		expFrame = 0;
		return;
	}

	Regiment* r = getRegiment();

	unLink();
	clearUp();

	int spritesPerObject = 1;

	Cord3D expAdjustX;
	Cord3D expAdjustY;

	Boolean hasExplosions = False;

	if(fired)				// Reset explosion back to start if fired again
		expFrame = 0;

	Boolean drawAsLimbered = (isLimbered() && (limberCount == 0));

	if(drawData.view.zoomLevel > Zoom_2)
	{
#ifdef BATEDIT
		if(drawData.view.zoomLevel > Zoom_8)
			return;
#endif

		drawInfo.height = 2 << (Zoom_8 - drawData.view.zoomLevel);
		drawInfo.side = unit->getSide();
		spritesPerObject = 1;
	}
	else
	{
		/*
		 * Note: The angle here should be the direction that the men are facing
		 * which might be different to the direction the regiment is facing
		 */

		UBYTE octant = facingOctant(menFacing, drawData.view.yRotate);

		SpriteIndex snum;

		if(drawAsLimbered)
		{
			/*
			 * Limbered Display....
			 *   Each canon is represented as a cannon and 2 horses
			 *
			 * 2 Images are required, a canon and a horse
			 * all canons and horses will thus share the same image
			 */

			// images = new Sprite3D[2];


			/*
			 * Get the limber graphic
			 */

			UBYTE offset = artLimberedSprites[drawData.view.zoomLevel][octant];

			if(offset == 0xff)	// Should never happen
				return;

			snum = offset;
			
#ifdef EMMA_GRAPHICS		// Emma's graphics

			if(drawData.view.zoomLevel <= Zoom_2)
				snum += (animation % LimberWalkAnimations);

			if(drawData.view.zoomLevel == Zoom_Half)
				snum += limberedSprites[(unsigned)r->type.subType];
			else
				snum += LimberSprites;
#else
			snum += limberedSprites[(unsigned)r->type.subType];
#endif

			images[0].spriteBlock = battle->spriteLib->read(snum);

			/*
			 * Get the horse graphics
			 */

			snum = horseMarchSprites[drawData.view.zoomLevel][octant];
			snum += (animation % HorseWalkAnimations);
 			snum += Horse_Walk;

			images[1].spriteBlock = battle->spriteLib->read(snum);
		
			spritesPerObject = 3;	// 2 horse + 1 canon
		}
		else
		{
			/*
			 * Unlimbered Sprites are drawn as a canon, some men wandering around
			 * and maybe a horse or 2 at the rear.
			 *
			 * This requires at least 3 sprites:
			 *   Horse, canon and infantry men
			 *
			 * If each infantry man is independant, then many more may be needed!
			 */

		 	// images = new Sprite3D[3];

			/*
			 * Get the canon sprite
			 */

			snum = artUnlimberSprites[drawData.view.zoomLevel][octant];

#ifdef EMMA_GRAPHICS		// Emma's graphics
			if(drawData.view.zoomLevel != Zoom_Half)
				snum += CanonSprites;
			else
				snum += canonSprites[(unsigned)r->type.subType];
#else
			snum += canonSprites[(unsigned)r->type.subType];
#endif

			images[0].spriteBlock = battle->spriteLib->read(snum);

			/*
			 * Get a horse sprite
			 * Horses face backwards
			 */

#ifdef NO_CAV_4MILE
		 	if(drawData.view.zoomLevel == Zoom_2)
			{
				snum = horseMarchSprites[drawData.view.zoomLevel][octant ^ 4];
 				snum += Horse_Walk;
			}
			else
#endif
			{
				snum = horseStandSprites[drawData.view.zoomLevel][octant ^ 4];
 				snum += Horse_Stand;
			}
			images[1].spriteBlock = battle->spriteLib->read(snum);


			/*
			 * Get an infantry sprite
			 */

			snum = infantryStandSprites[drawData.view.zoomLevel][octant];
			if(r->getSide() == SIDE_CSA)
				snum += INF_CSA_Stand;
			else	// Assume USA
				snum += INF_USA_Stand;
			images[2].spriteBlock = battle->spriteLib->read(snum);

			spritesPerObject = 7;		// 1 canon, 2 horses, 4 men

			/*
			 * Set up explosion shape if necessary
			 */

			if(fired || expFrame)
			{
				UBYTE sprOffset = cannonSmoke.sprites[drawData.view.zoomLevel][octant];
				if(sprOffset != 0xff)
				{
					SpriteIndex sprNum = sprOffset + cannonSmoke.baseSprite + expFrame;
					expSprite.spriteBlock = battle->spriteLib->read(sprNum);
					expAdjustX = expSprite.spriteBlock->anchorX;
					expAdjustY = expSprite.spriteBlock->anchorY;
					hasExplosions = True;
					spritesPerObject++;

					expFrame++;
					if(expFrame >= cannonSmoke.frames)
						expFrame = 0;
				}
			}
		}
	}

	if(!hasExplosions)
		expFrame = 0;

	Wangle a1 = formationFacing;

	/*
	 * Work out top left coordinate and delta values
	 */

	DeltaSpacing delta;
	Cord3D dx = DistToBattle(Yard(80));
	Cord3D dy = DistToBattle(Yard(125));
	if(routed)
	{
		dx += dx/2;
		dy += dy/2;
	}

	delta.set(dx, dy, a1);

	/*
	 * Work out spacing for objects relative to facing
	 * Actual positions are calculated as a proportion of this spacing
	 */

	AddSprite addSprite;

	addSprite.xh = +msin(delta.hSpacing, menFacing) / 2;
	addSprite.yh = -mcos(delta.hSpacing, menFacing) / 2;
	addSprite.xv = +mcos(delta.vSpacing, menFacing) / 2;
	addSprite.yv = +msin(delta.vSpacing, menFacing) / 2;
	addSprite.drawData = &drawData;

	/*
	 * Work out the formation grid size
	 *
	 * This is bodge code for testing...
	 *
	 * For now put them in line (2 lines of x men)
	 */

	int totalSprites = (r->strength + MenPerArtillerySprite - 1) / MenPerArtillerySprite;
	UBYTE menHigh = rows;
	UBYTE menWide = columns;
	
	BattleLocation l = where;

	if(menWide > 1)
	{
		l.x -= (delta.xh * (menWide-1)) / 2;
		l.z -= (delta.yh * (menWide-1)) / 2;
	}
	if(menHigh > 1)
	{
		l.x -= (delta.xv * (menHigh-1)) / 2;
		l.z -= (delta.yv * (menHigh-1)) / 2;
	}

	/*
	 * Set up shapeList
	 */

	if(sprElements)
		delete[] sprElements;
	sprElements = new SpriteID[totalSprites * spritesPerObject];
	addSprite.nextShape = sprElements;

	RandomNumber random(r->strength);

	int row = menHigh;
	while(row--)
	{
		BattleLocation l1 = l;

		if(routed)
		{
			l1.x += random(delta.hSpacing/2) - delta.hSpacing/4;
			l1.z += random(delta.hSpacing/2) - delta.hSpacing/4;
		}

		int col;
		if(totalSprites < menWide)
		{
			col = totalSprites;

			int missingMen = menWide - totalSprites;

			/*
			 * Adjust coordinate so men in centre of row
			 */

			l1.x += (missingMen * delta.xh) / 2;
			l1.z += (missingMen * delta.yh) / 2;
		}
		else
			col = menWide;
		totalSprites -= col;

		while(col--)
		{
			addSprite.mid = l1;

			if(drawData.view.zoomLevel > Zoom_2)
			{
				Point3D p;
				p.x = l1.x;
				p.z = l1.z;
				p.y = battle->grid->getHeightView(drawData.view, p.x, p.z);
				drawData.view.transform(p);
				addSprite.nextShape++->element = battle->spriteList->add(&drawInfo, p);
			}
			else if(drawAsLimbered)
			{
				// Limber

				addSprite.add(&images[0], 0, -0x60);
				addSprite.add(&images[1], -0x80, 0x60);
				addSprite.add(&images[1], +0x80, 0x60);
			}
			else
			{
				struct LimberAddPos {
					int index;
					int x;
					int y;
				};

				static LimberAddPos spritePositions[] = {
					{ 0,	   0,  0x60 },
					{ 1, -0x80, -0xc0 },
					{ 1, +0x80, -0xc0 },
					{ 2, -0x80,  0xb0 },
					{ 2, +0x80,  0xb0 },
					{ 2, -0x80,  0x60 },
					{ 2,     0, -0x30 },
					{ -1, 0, 0 }
				};

				LimberAddPos* lap = spritePositions;
				while(lap->index >= 0)
				{
					int x = lap->x;
					int y = lap->y;

#if !defined(BATEDIT)
					if(routed)
					{
						if(lap->index != 0)		// Not gun
						{
							x += random(0x20) - 0x10;
							y += random(0x20) - 0x10;
						}
					}
					else if(limberCount != 0)
					{
						if(lap->index == 1)	// Horse
						{
							x += random(0x20) - 0x10;
							y += random(0x20) - 0x10;
						}
						else if(lap->index == 2)	// Man
						{
							x += random(0x80) - 0x40;
							y += random(0x80) - 0x40;
						}
					}
					else if(canLoadFire() && !fireCount && (loadStatus < LoadShowArtAnimation))
					{
						if(lap->index == 2)	// Man
						{
							x += game->miscRand(0x40) - 0x20;
							y += game->miscRand(0x40) - 0x20;
						}
					}
#endif

					addSprite.add(&images[lap->index], x, y);
					lap++;
				}


				if(hasExplosions)
				{
					addSprite.add(&expSprite, 0, 0xf0);
				}
			}

			l1.x += delta.xh;
			l1.z += delta.yh;
		}

		l.x += delta.xv;
		l.z += delta.yv;
	}

	if(!inDisplayList)
	{
		battle->displayedObjects->add(this);
		inDisplayList = True;
	}

	fired = False;
}


void UnitBattle::updateAnimation(Distance movedDistance)
{
	const Distance aDistance = Yard(6);

	animationDistance += movedDistance;
	if(animationDistance >= aDistance)
	{
		animation++;

		animationDistance = 0;
	}
}

