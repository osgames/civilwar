/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Font Management
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.4  1994/05/25  23:31:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/05/19  17:44:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/05/04  22:09:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/04/20  22:21:40  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include <fstream.h>
#include "font.h"
#include "makename.h"
#include "error.h"
#include "filedata.h"

#include "cd_open.h"

/*
 * Global fontset
 */

FontSet* fonts = 0;

/*
 * List of fonts (must match FontID in font.h
 */

static char* fontNames[] = {
	"art\\f_jame_5",
	"art\\f_jame08",
	"art\\f_jame15",
	"art\\f_jame32",
	"art\\f_emma14",
	"art\\f_emfl_8",
	"art\\f_emfl10",
	"art\\f_emfl15",
	"art\\f_emfl32",
	0,
};

/*
 * Disk File structure
 */

struct FontHeaderD {
	IBYTE height;
	IWORD firstChar;
	IWORD howMany;
	IWORD streamSize;
};

/*
 * This is followed on disk by:
 *   IBYTE widths[howMany];
 *   IBYTE bitStream[streamSize];
 */






class FontStream {
public:

	ifstream fontstream;

	FontStream(const char* s) { fileOpen( fontstream, s, ios::in | ios::binary | ios::nocreate); }
	~FontStream() { fontstream.close(); }
};

/*
 * Constructor
 */

Font::Font(const char* fileName)
{
	char fname[FILENAME_MAX];
	makeFilename(fname, fileName, ".FNT", False);

	/*
	 * Open File, read it in and initialise
	 */

	FontStream file(fname);
	if(file.fontstream.fail())
		throw GeneralError("Couldn't open Font File");

	/*
	 * Get the header
	 */

	FontHeaderD header;
	file.fontstream.read( (char*) &header, sizeof(header));
	if(file.fontstream.fail())
		throw GeneralError("Couldn't read Font header in %s", fname);

	height = getByte(&header.height);
	first = getWord(&header.firstChar);
	howMany = getWord(&header.howMany);

	widths = new UBYTE[howMany];
	offsets = new FontOffset[howMany];
	bitStream = new UBYTE[getWord(&header.streamSize)];

	/*
	 * Read the width table and use it to set up offset table
	 */

	file.fontstream.read(widths, howMany);
	if(file.fontstream.fail())
		throw GeneralError("Couldn't read Font width table in %s", fname);

	int count = howMany;
	FontOffset pos = 0;
	UBYTE* widthPtr = widths;
	FontOffset* offsetPtr = offsets;
	while(count--)
	{
		*offsetPtr++ = pos;
		pos += *widthPtr++ * height;
	}

	/*
	 * read the bitstream
	 */

	file.fontstream.read(bitStream, getWord(&header.streamSize));
	if(file.fontstream.fail())
		throw GeneralError("Couldn't read Font bit stream in %s", fname);
}

Font::~Font()
{
	delete[] widths;
	delete[] offsets;
	delete[] bitStream;
}


UBYTE Font::getWidth(UWORD c) const
{
	if(inSet(c))
		return widths[c-first];
	else
		return 0;
}

UWORD Font::getWidth(const char* s) const
{
	UWORD result = 0;

	UWORD x = 0;
	while(*s)
	{
		char c = *s++;

		if((c == '\r') || (c == '|'))
		{
			if(x > result)
				result = x;
			x = 0;
		}
		else
			x += getWidth(c) + 1;
	}
	if(x > result)
		result = x;

	return result;
}

UWORD Font::getLineWidth(const char* s) const
{
	UWORD result = 0;

	while(*s)
	{
		char c = *s++;

		if(c == '\r')
			break;

		result += getWidth(c) + 1;
	}

	return result;
}



void Font::getSize(const char* s, UWORD& width, UWORD& height)
{
	width = 0;
	height = getHeight();

	UWORD x = 0;

	const char* s1 = s;
	while(*s1)
	{
		char c = *s1++;

		if(c == '\r')
		{
			height += getHeight() + 1;
			if(x > width)
				width = x;
			x = 0;
		}
		else
			x += getWidth(c) + 1;
	}
	if(x > width)
		width = x;
}

/*
 * Font Set management
 */

FontSet::FontSet()
{
	int i = 0;
	char** sptr = fontNames;

	while(*sptr++)
		i++;

	howMany = UBYTE(i);
	fonts = new Font*[i];

	Font** fptr = fonts;

	sptr = fontNames;
	while(*sptr)
		*fptr++ = new Font(*sptr++);
}

FontSet::~FontSet()
{
	Font** f = fonts;
	int i = howMany;
	while(i--)
		delete *f++;
	delete fonts;
}
