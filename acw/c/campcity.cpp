/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Resource User Interface
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.9  1994/07/28  18:57:04  Steven_Green
 * Modifications in preparation for Water ferrying
 *
 * Revision 1.3  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.1  1994/05/21  13:16:19  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "campcity.h"
#include "cityinfo.h"
#include "campaign.h"
#include "campwld.h"
#include "colours.h"
#include "image.h"
#include "mapwind.h"
#include "trig.h"
#include "menuicon.h"
#include "game.h"
#include "menudata.h"
#include "system.h"
#include "screen.h"
#include "layout.h"
#include "connect.h"
#include "language.h"
#include "options.h"

#ifdef DEBUG
#include "camptab.h"
#endif

/*
 * Packet
 */

struct MobilisePacket {
	MobiliseType what;
	FacilityID where;
	UBYTE howMany;
};

/*===============================================================
 * City Info Icon box
 */

class CityInfoIcon : public IconSet {
	TrackCity* control;
	FacilityInfo info;
public:
	UBYTE justUpdated;			// Force update of icons for next n frames
public:
	CityInfoIcon(IconSet* parent, TrackCity* c);
	void drawIcon();
	void execute(Event* event, MenuData* d);

	void closeMenu();
	void update();
private:
	void makeIcons();
};

/*--------------------------------------------------------------
 * Sub menu Icons
 */

/*----------------------------------------------------------------
 * Cancel Icon
 */

class MobCancel : public MenuIcon {
	CityInfoIcon* control;
public:
	MobCancel(IconSet* parent, CityInfoIcon* c, SDimension y) :
		MenuIcon(parent, C2_MiniOK, Rect(MENU_ICON_X, y, MENU_ICON_W, MENU_ICON_H))	// width was 113
	{
		control = c;
	}

	void execute(Event* event, MenuData* d);
};

void MobCancel::execute(Event* event, MenuData* d)
{
	if(event->overIcon)
		d->showHint( language( lge_cancel ) );

	if(event->buttons)
		control->closeMenu();
		// campaign->subMenu->kill();
}

class BuildMenuIcon : public MenuIcon {
	CityInfoIcon* control;
protected:
	Facility* facility;		// Where to build
	MobiliseType what;
public:
	BuildMenuIcon(IconSet* parent, CityInfoIcon* c, Facility* f, MobiliseType type, GameSprites icon, SDimension y) :
		MenuIcon(parent, icon, Rect(MENU_ICON_X, y, MENU_ICON_W, MENU_ICON_H))
	{
		control = c;
		facility = f;
		what = type;
	}

	void execute(Event* event, MenuData* d);
};

void BuildMenuIcon::execute(Event* event, MenuData* d)
{
	if( event->overIcon )
		d->showHint(language(LGE(lge_mobRI + what)));

	if(event->buttons)
	{
		int added = campaign->world->mobList.askHowMany(what, facility, d);

		if(added)
		{
			MobilisePacket* pkt = new MobilisePacket;
			pkt->what = what;
			pkt->where = campaign->world->facilities.getID(facility);
			pkt->howMany = added;
			game->connection->sendMessage(Connect_CS_Mobilise, (UBYTE*)pkt, sizeof(MobilisePacket));
			control->justUpdated = 1;
		}

		control->closeMenu();
		// campaign->subMenu->kill();
	}
}

/*--------------------------------------------------------------
 * Mobilise Infantry
 */

class MobInfantryIcon : public Icon {
	TrackCity* control;
	CityInfoIcon* cityInfo;
public:
	MobInfantryIcon(CityInfoIcon* parent, TrackCity* c, const Rect& r) :
		Icon(parent, r)
	{
		control = c;
		cityInfo = parent;
	}

	void execute(Event* event, MenuData* d);
	void drawIcon() { }
};

void MobInfantryIcon::execute(Event* event, MenuData* d)
{
	if(event->overIcon)
		d->showHint( language( lge_createI ) );

	if(event->buttons)
	{
		MobiliseList& mobList = campaign->world->mobList;

		SubMenu* menu = campaign->subMenu;

		IconList* icons = new IconList[7];

		Icon** iconPtr = icons;

		SDimension y = MENU_ICON_Y1;
		
		if(mobList.canIbuild(control->currentFacility, Mob_InfantryRegular))
		{
			*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility, Mob_InfantryRegular, C1_InfReg, y);
			y += MENU_ICON_H;
		}

		if(mobList.canIbuild(control->currentFacility, Mob_InfantryMilitia))
		{
			*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility, Mob_InfantryMilitia, C1_InfMil, y);
			y += MENU_ICON_H;
		}

		if(mobList.canIbuild(control->currentFacility, Mob_InfantrySharpShooter))
		{
			*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility, Mob_InfantrySharpShooter, C1_Sharpshooter, y);
			y += MENU_ICON_H;
		}

		if(mobList.canIbuild(control->currentFacility, Mob_Engineer))
		{
			*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility, Mob_Engineer, C1_Engineer, y);
			y += MENU_ICON_H;
		}

		if(mobList.canIbuild(control->currentFacility, Mob_RailEngineer))
		{
			*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility, Mob_RailEngineer, C1_RailEngineer, y);
			y += MENU_ICON_H;
		}

		*iconPtr++ = new MobCancel(menu, cityInfo, y);
		*iconPtr++ = 0;

		menu->add(icons);
	}
}


/*--------------------------------------------------------------
 * Mobilise Cavalry
 */

class MobCavalryIcon : public Icon {
	TrackCity* control;
	CityInfoIcon* cityInfo;
public:
	MobCavalryIcon(CityInfoIcon* parent, TrackCity* c, const Rect& r) :
		Icon(parent, r)
	{
		control = c;
		cityInfo = parent;
	}
	
	void execute(Event* event, MenuData* d);
	void drawIcon() { }
};

void MobCavalryIcon::execute(Event* event, MenuData* d)
{
	if(event->overIcon)
		d->showHint( language( lge_createC ) );

	if(event->buttons)
	{
		MobiliseList& mobList = campaign->world->mobList;
		SubMenu* menu = campaign->subMenu;

		IconList* icons = new IconList[6];
		Icon** iconPtr = icons;

		SDimension y = MENU_ICON_Y1;

		if(mobList.canIbuild(control->currentFacility, Mob_cavalryRegular))
		{
			*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility,	Mob_cavalryRegular,			C1_CavReg, 		   y);
			y += MENU_ICON_H;
		}

		if(mobList.canIbuild(control->currentFacility, Mob_cavalryMilitia))
		{
			*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility,	Mob_cavalryMilitia,			C1_CavMil, 		   y);
			y += MENU_ICON_H;
		}

		if(mobList.canIbuild(control->currentFacility, Mob_cavalryRegularMounted))
		{
			*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility,	Mob_cavalryRegularMounted,	C1_CavRegMounted, y);
			y += MENU_ICON_H;
		}

		if(mobList.canIbuild(control->currentFacility, Mob_cavalryMilitiaMounted))
		{
			*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility,	Mob_cavalryMilitiaMounted,	C1_CavMilMounted, y);
			y += MENU_ICON_H;
		}

		*iconPtr++ = new MobCancel(menu, cityInfo, y);
		*iconPtr++ = 0;

		menu->add(icons);
	}
}

/*--------------------------------------------------------------
 * Mobilise Artillery
 */

class MobArtilleryIcon : public Icon {
	TrackCity* control;
	CityInfoIcon* cityInfo;
public:
	MobArtilleryIcon(CityInfoIcon* parent, TrackCity* c, const Rect& r) :
		Icon(parent, r)
	{
		control = c;
		cityInfo = parent;
	}
	
	void execute(Event* event, MenuData* d);
	void drawIcon() { }
};

void MobArtilleryIcon::execute(Event* event, MenuData* d)
{
	if(event->overIcon)
		d->showHint( language( lge_createA ) );

	if(event->buttons)
	{
		MobiliseList& mobList = campaign->world->mobList;
		SubMenu* menu = campaign->subMenu;

		IconList* icons = new IconList[6];

		Icon** iconPtr = icons;

		SDimension y = MENU_ICON_Y1;


		if(mobList.canIbuild(control->currentFacility, Mob_ArtillerySmoothbore))
		{
			*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility, Mob_ArtillerySmoothbore,C1_Smoothbore, y);
			y += MENU_ICON_H;
		}

		if(mobList.canIbuild(control->currentFacility, Mob_ArtilleryLight))
		{
			*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility, Mob_ArtilleryLight,		C1_ArtLight,  y);
			y += MENU_ICON_H;
		}

		if(mobList.canIbuild(control->currentFacility, Mob_ArtilleryRifled))
		{
			*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility, Mob_ArtilleryRifled,		C1_ArtRifled, y);
			y += MENU_ICON_H;
		}

		if(mobList.canIbuild(control->currentFacility, Mob_ArtillerySiege))
		{
			*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility, Mob_ArtillerySiege,		C1_ArtSiege, y);
			y += MENU_ICON_H;
		}

		*iconPtr++ = new MobCancel(menu, cityInfo, y);
		*iconPtr++ = 0;

		menu->add(icons);
	}
}


/*--------------------------------------------------------------
 * Build Naval Unit
 */

class MobNavalIcon : public Icon {
	TrackCity* control;
	CityInfoIcon* cityInfo;
public:
	MobNavalIcon(CityInfoIcon* parent, TrackCity* c, const Rect& r) :
		Icon(parent, r)
	{
		control = c;
		cityInfo = parent;
	}

	void execute(Event* event, MenuData* d);
	void drawIcon() { }
};

void MobNavalIcon::execute(Event* event, MenuData* d)
{
	const Facility* f = control->currentFacility;

	if(event->overIcon && (f->facilities & (Facility::F_Port | Facility::F_LandingStage)))
		d->showHint( language( lge_createN ) );

	if(event->buttons && (f->facilities & (Facility::F_Port | Facility::F_LandingStage)))
	{
		MobiliseList& mobList = campaign->world->mobList;
		SubMenu* menu= campaign->subMenu;

		IconList* icons = new IconList[7];

		Icon** iconPtr = icons;

		SDimension y = MENU_ICON_Y1;

		if(f->facilities & Facility::F_Port)
		{
			if(mobList.canIbuild(control->currentFacility, Mob_Naval))
			{
				*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility, Mob_Naval,				C1_Naval, 	  y);
				y += MENU_ICON_H;
			}

			if(mobList.canIbuild(control->currentFacility, Mob_NavalIronClad))
			{
				*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility, Mob_NavalIronClad,	C1_NavalIronClad,  y);
				y += MENU_ICON_H;
			}
		}

		if(f->facilities & Facility::F_LandingStage)
		{
			if(mobList.canIbuild(control->currentFacility, Mob_Riverine))
			{
				*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility, Mob_Riverine,				C1_Riverine, y);
				y += MENU_ICON_H;
			}
			if(mobList.canIbuild(control->currentFacility, Mob_RiverineIronClad))
			{
				*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility, Mob_RiverineIronClad,	C1_RiverineIronClad,  y);
				y += MENU_ICON_H;
			}
		}

		if(f->facilities & Facility::F_Port)
		{
			if(mobList.canIbuild(control->currentFacility, Mob_Blockade))
			{
				*iconPtr++ = new BuildMenuIcon(menu, cityInfo, control->currentFacility, Mob_Blockade, C1_Blockade, y);
				y += MENU_ICON_H;
			}
		}

		*iconPtr++ = new MobCancel(menu, cityInfo, y);
		*iconPtr++ = 0;

		menu->add(icons);
	}
}

/*
 * Icon on info area
 */

class BuildIcon : public Icon {
	TrackCity* control;
	MobiliseType what;
	CityInfoIcon* cityInfo;
public:
	BuildIcon(CityInfoIcon* parent, TrackCity* c, MobiliseType type, Rect r) : Icon(parent, r)
	{
		what= type;
		control = c;
		cityInfo = parent;
	}

	void execute(Event* event, MenuData* data);
	void drawIcon() { }
};

void BuildIcon::execute(Event* event, MenuData* data)
{
	if( event->overIcon )
	{
	  	char a = 0;

		LGE message = lge_mobRI;

		while ( a++ < what ) 
			INCREMENT( message );

		data->showHint( language( message ) );
	}

	if(event->buttons)
	{
		int added = campaign->world->mobList.askHowMany(what, control->currentFacility, data);

		if(added)
		{
			MobilisePacket* pkt = new MobilisePacket;
			pkt->what = what;
			pkt->where = campaign->world->facilities.getID(control->currentFacility);
			pkt->howMany = added;
			game->connection->sendMessage(Connect_CS_Mobilise, (UBYTE*)pkt, sizeof(MobilisePacket));
			cityInfo->justUpdated = 2;
		}

		cityInfo->update();

		// control->forceUpdate = True;
	}
}


#ifdef DEBUG

/*==========================================================================
 * Debug Icon
 */

class FacilityCheatIcon : public Icon {
	TrackCity* control;
	CityInfoIcon* cityInfo;
public:
	FacilityCheatIcon(CityInfoIcon* parent, TrackCity* c, Rect r) : Icon(parent, r)
	{
		control = c;
		cityInfo = parent;
	}

	void execute(Event* event, MenuData* data);
	void drawIcon() { }
};

void FacilityCheatIcon::execute(Event* event, MenuData* data)
{
	if( event->overIcon )
	{
		data->showHint("Cheat! Set resources to maximum");
	}

	if(event->buttons)
	{
		Facility* f = control->currentFacility;

		ResourceSet* resources = f->getLocalResources();

		if(f)
		{
			for(int i = 0; i < ResourceCount; i++)
				resources->values[i] = MaxResource;
			resources->adjust();
		}

		cityInfo->update();
	}
}

#endif

/*===============================================================
 * City Info Icon box Implementation
 */


CityInfoIcon::CityInfoIcon(IconSet* parent, TrackCity* c) :
	IconSet(parent, Rect(0,0,MENU_WIDTH,312))
{
	justUpdated = 0;
	control = c;

	makeIcons();
}

void CityInfoIcon::makeIcons()
{
#if 0
	if(icons)
		delete[] icons;
#else
	killIcons();
#endif

	Facility* f = control->currentFacility;

	info.makeInfo(f);

#ifdef DEBUG
	icons = new IconList[14];			// Up to 12 icons needed
#else
	icons = new IconList[13];			// Up to 12 icons needed
#endif

	Icon** iconPtr = icons;

	for(int count = 0; count < BuildCount; count++)
	{
		if(info.info[count].upgradeable)
		{
			if(buildSetup[count].what != Mob_Nothing)
				*iconPtr++ = new BuildIcon(this, control, buildSetup[count].what, buildSetup[count].r);
			else
			{
				switch(count)
				{
				case BuildInfantry:
					*iconPtr++ = new MobInfantryIcon(this, control, buildSetup[count].r);
					break;

				case BuildCavalry:
					*iconPtr++ = new MobCavalryIcon(this, control, buildSetup[count].r);
					break;

				case BuildArtillery:
					*iconPtr++ = new MobArtilleryIcon(this, control, buildSetup[count].r);
					break;

				case BuildNaval:
					*iconPtr++ = new MobNavalIcon(this, control, buildSetup[count].r);
					break;
				}
			}
		}
	}

#ifdef DEBUG
	*iconPtr++ = new FacilityCheatIcon(this, control, Rect(1, 206, 118, 44));
#endif

	/*
	 * Terminate
	 */

	*iconPtr++ = 0;
}


void CityInfoIcon::execute(Event* event, MenuData* d)
{
	IconSet::execute(event, d);

	if(control->forceUpdate || justUpdated)
	{
		update();
		if(justUpdated)
			justUpdated--;
	}
}

void CityInfoIcon::drawIcon()
{
	info.drawInfo(campaign->infoArea, True);
}

void CityInfoIcon::closeMenu()
{
	campaign->subMenu->kill();
	update();
}

void CityInfoIcon::update()
{
	setRedraw();
	makeIcons();
	control->forceUpdate = False;
}


/*
 * Close down mobilising completely
 */

class MobOK : public MenuIcon {
	TrackCity* control;
public:
	MobOK(IconSet* parent, TrackCity* c, SDimension y) :
		MenuIcon(parent, C2_MiniOK, Rect(0, y, MENU_WIDTH, 53))
	{
		control = c;
	}

	void execute(Event* event, MenuData* d);
};

void MobOK::execute(Event* event, MenuData* d)
{
	if(event->buttons)
		control->closeDown();

	if(event->overIcon)
		d->showHint( language( lge_cancel ) );
}


/*===============================================================
 * Track City Setup
 */

TrackCity::TrackCity(CampaignMode* p) : CampaignFunction(p)
{
	currentFacility = 0;
	mode = Tracking;
	forceUpdate = True;
	campaign->updateMap();		// Force a redraw
	// setPointer(M_TrackCity);
}

/*
 * Remove Track City
 */

TrackCity::~TrackCity()
{
	if(mode == Ordering)
		closeDown();

	if(currentFacility)
	{
		currentFacility->highlight = 0;
	 	campaign->updateMap();		// Force a redraw
	}
}

void TrackCity::closeDown()
{
	if(campaign->subMenu)
		campaign->subMenu->kill();
	mode = Tracking;
	forceUpdate = True;
	// setPointer(oldPointer);
}


/*
 * Function called when mouse is over map
 */

void TrackCity::overMap(MapWindow* map, Event* event, MenuData* d, const Location* l)
{
	if(campaign->newDay)
		forceUpdate = True;

	if(mode == Tracking)
	{
		d->setTempPointer(M_TrackCity);

		/*
		 * Find closest facility to mouse
		 */

		MapDisplayObject* best = 0;
		SDimension dist = 0;

		if(map->onScreenStatic.entries())
		{
			// MapDisplayIter list = map->onScreenStatic;
			MapDisplayIter list = map->onScreenMoveable;

			while(++list)
			{
				MapDisplayObject* dob = list.current();

				if(dob->ob->obType == OT_Facility)
				{
					SDimension d = distance(dob->screenPos.x - event->where.x, dob->screenPos.y - event->where.y);

					if(!best || (d < dist))
					{
						best = dob;			// ? Will this work?
						dist = d;
					}
				}
			}
		}
		if(best)
		{
			Facility* f = (Facility*)best->ob;

			if(f != currentFacility)
			{
				if(currentFacility)
					currentFacility->highlight = 0;

				currentFacility = f;
				currentFacility->highlight = 1;

				forceUpdate = True;
				campaign->updateMap();		// Force a redraw
			}

			if(forceUpdate)
			{
				forceUpdate = False;

				/*
			 	 * Display info on RHS
			 	 */

				currentFacility->showCampaignInfo(campaign->infoArea);
			}
		}
		else if(currentFacility || forceUpdate)
		{
			forceUpdate = False;
			if(currentFacility)
			{
				currentFacility->highlight = 0;
				currentFacility = 0;
				campaign->updateMap();		// Force a redraw
			}
			campaign->clearInfo();
		}
	}

	/*
	 * Do things if buttons pressed
	 */

	/*
	 * Right buttons does ????
	 */

	if(event->buttons & Mouse::RightButton)
	{
		if(mode == Ordering)
			closeDown();
	}
	else 

	/*
	 * Left button turns info area into icons
	 */

	if(event->buttons & Mouse::LeftButton)
	{	
		if(currentFacility && (mode == Tracking))
		{
#ifdef DEBUG
			// if(seeAll || (currentFacility->side == game->getLocalSide()))
			if(seeAll || (game->isPlayer(currentFacility->side)))
#else
			// if(currentFacility->side == game->getLocalSide())
			if(game->isPlayer(currentFacility->side))
#endif
			{
				mode = Ordering;

				IconList* icons = new IconList[3];

				icons[0] = new CityInfoIcon(campaign->subMenu, this);
				icons[1] = new MobOK(campaign->subMenu, this, 314);
				icons[2] = 0;

				campaign->subMenu->add(icons);

				setPointer(M_Arrow);
			}
		}
	}
}


void TrackCity::offMap()
{
	if( (mode == Tracking) && (forceUpdate || currentFacility))
	{
		forceUpdate = False;
		if(currentFacility)
		{
			currentFacility->highlight = 0;
			currentFacility = 0;
			campaign->updateMap();		// Force a redraw
		}
		campaign->clearInfo();
	}
}

void Campaign::rxMobilise(UBYTE* packet)
{
	MobilisePacket* pkt = (MobilisePacket*) packet;

	world->mobList.startBuild(pkt->what, world->facilities[pkt->where], pkt->howMany);

#ifdef DEBUG
	Facility* f = world->facilities[pkt->where];

	netLog.printf("Mobilise %s at %s",
		language(buildCosts[pkt->what].description),
		f->getNameNotNull());
#endif
}
