/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Data used inside a menu
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.14  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.1  1994/01/11  22:29:03  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <conio.h>
#include <ctype.h>

#include "menudata.h"
#include "mouselib.h"
#include "system.h"
#include "screen.h"
#include "scrndump.h"
#include "dialogue.h"
#include "globproc.h"

#if defined(COMPUSA_DEMO)
class GotoQuitGame {
};
#endif

/*
 * Execute all icons, called every game loop!
 */

void MenuData::process()
{
	shownHint = False;

	tempPointer = pointer;

	if(needUpdate())
	{
		setRedraw();
		clearUpdate();
	}

	/*
	 * redraw any icons that have asked to be updated
	 */

	drawUpdate();

	/*
	 * Update the physical screen
	 */

	machine.screen->update();


	/*
	 * Set up event data
	 */

	Event event;
	Boolean processed = False;

	event.overIcon = False;
	event.where = machine.mouse->getPosition();

	/*
	 * Process keyboard event
	 */

	if(kbhit())
	{
		// Key lastKey = Key(toupper(getch()));
		Key lastKey = Key(getch());
		if(lastKey == 0)
			lastKey = Key(getch() + SpecialKey);

		event.key = lastKey;
	}
	else
		event.key = 0;

#if defined(COMPUSA_DEMO)
	if(event.key == KEY_Escape)
		throw GotoQuitGame();
#endif

#ifdef SCREENDUMP
	if(event.key == 0x8086)		// F12
	{
		doScreenDump(this);
		event.key = 0;
	}
#endif

	/*
	 * Process Mouse events
	 */

	MouseEvent* mEvent;

	while( (mEvent = machine.mouse->getEvent()) != 0)
	{
		event.buttons = mEvent->buttons;
		event.where = mEvent->where;
		// delete mEvent;

		execute(&event, this);
		processed = True;
	}

	/*
	 * Process Area that mouse is over if no mouse event has happened
	 */

	if(!processed)
	{
		event.buttons = 0;
		event.where = machine.mouse->getPosition();
		execute(&event, this);
	}

	if(!shownHint)
		showHint(0);

	if(flags & FinishedFlag)
		tempPointer = M_Busy;

	machine.mouse->setPointer(tempPointer);

	globalProcedures.proc();

	runProc();		// Virtual process

}

/*
 * Initialise Menudata class
 */


MenuData::MenuData(MenuData* p) :
	IconSet(0, Rect(0,0,640,480))
{
 	mode = MenuOK;
 	flags = UpdateFlag;
	// proc = func;
	parent = p;
	pointer = M_Arrow;	// machine.mouse->getPointer();
	currentInput = 0;
	shownHint = False;
	paused = False;
}


void MenuData::showHint(const char* s)
{
	// Do nothing?
}

void MenuData::showStatus(const char* s)
{
	// Do nothing?
}


void MenuData::proc()
{
}

void MenuData::runProc()
{
	if(!isFinished())
	{
		proc();

		if(parent)
		{
			parent->runProc();

			if(parent->isFinished())
				setFinish(MenuQuit);			// Force a Cancel
		}
	}
}

