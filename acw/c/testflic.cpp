/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	FLI Tester
 *
 * Actually plays FLI files to a SVGA screen
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <conio.h>
#include "daglib.h"
#include "flicread.h"

void main(int argc, char* argv[])
{
	if(argc != 2)
	{
		cout << "Usage: testflic filename" << endl;
		return;
	}
	else
	{
		try
		{
			machine.init(0, MemoryLog::None);

			Flic fli(argv[1]);	// This opens it

			if(fli.getError())
			{
				cout << "Error in FLIC file " << argv[1] << endl;
				return;
			}

			const FlicHead* head = fli.getHeader();

			cout << "Header for " << argv[1] << endl;
			cout << "Size: " << head->size << endl;
			cout << "Type: " << hex << head->type << dec << endl;
			cout << "frames: " << head->frames << endl;
			cout << "width: " << head->width << endl;
			cout << "height: " << head->height << endl;
			cout << "depth: " << head->depth << endl;
			cout << "flags: " << head->flags << endl;
			cout << "speed: " << head->speed << endl;


			while(!fli.finished)
			{
				fli.nextFrame();

				/*
			 	 * Blit it to screen
			 	 */

				machine.blit(&fli.image, Point(0,0));
				machine.screen->setPalette(&fli.palette);
				machine.screen->update();
			}
		}
		catch( GeneralError e )
		{
			cout << "Error:";
			if ( e.get() )
		 		cout << e.get();
			else
				cout << " ??";
			cout << endl;
			getch();
		}
		catch( ...)
		{
		 cout << "Unknown error!\n";
		 getch();
		}
	}
}
