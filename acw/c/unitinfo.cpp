/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Display Unit Info on RHS of screen
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>

#include "unitinfo.h"
#include "generals.h"
#include "system.h"
#include "screen.h"
#include "colours.h"
#include "text.h"
#include "tables.h"
#include "game.h"
#include "barchart.h"
#include "gamelib.h"
#include "options.h"
#include "memptr.h"

#if !defined(TESTBATTLE)
#include "city.h"
#endif
#if !defined(TESTCAMP)
#include "unit3d.h"
#endif

#if defined(TESTCAMP)
#include "sprlib.h"
#endif

#define Font_UnitName		Font_JAME08	// Font_EMFL_8
#define Font_UnitNameSmall	Font_JAME_5	// Font_JAME08	// Font_EMFL_8
#define Font_CommandName	Font_JAME08	// Font_EMFL_8
#define Font_AttributeInfo	Font_JAME_5
#define Font_TroopStrength	Font_EMMA14

void randomizeStrength(RandomNumber& r, Strength& s)
{
	int tempRnd = 100 + r(50) - r(20);		// 80% .. 150%, average of 115%

	s = (s * tempRnd) / 100;
}

/*
 * Display Campaign Information
 */

void showUnitInfo(Unit* u, Region* window, Boolean inCampaign)
{
	game->sprites->drawSprite(window, Point(0,0), AI_Outer);

	Boolean canSee;

	if(game->isPlayer(u->getSide()))
		canSee = True;
	else
		canSee = False;

#ifdef DEBUG
	if(seeAll)
		canSee = True;
#endif

	/*
	 * Get the unit info
	 */

	UnitInfo info(True, True);
	u->getInfo(info);

	char buffer[200];		// Handy place for temporary string

	/*
	 * Display Unit name
	 */

	Region region = *window;
	region.setClip(window->getX() + 1, window->getY(), window->getW() - 2, 56);
	TextWindow textWind(&region, Font_UnitName);
	textWind.setWrap(True);

	textWind.setColours(Black);
	textWind.setFormat(True, True);		// Horizontal & Vertical centre

	/*
	 * 	Distort strengths if difficulty level is high enough
	 */

	if(!canSee && (game->getDifficulty(Diff_CommandControl ) >= Complex2))
	{
		RandomNumber r(u->location.x + u->location.y);

		randomizeStrength(r, info.infantryStrength);
		randomizeStrength(r, info.cavalryStrength);
		randomizeStrength(r, info.artilleryStrength);

#if 0
		if ( info.infantryStrength < ( info.infantryStrength * -tempRnd ) / 100 )
			info.infantryStrength  += ( info.infantryStrength * tempRnd ) / 100;

		if ( info.cavalryStrength  < ( info.cavalryStrength  * -tempRnd ) / 100 )
			info.cavalryStrength   += ( info.cavalryStrength  * tempRnd ) / 100;

		if ( info.artilleryStrength < ( info.artilleryStrength * -tempRnd ) / 100 )
			info.artilleryStrength += ( info.artilleryStrength  * tempRnd ) / 100;
#endif
	}

#if 0
	textWind.wprintf("%s\r%s %s",
		u->getName(False),
		getUnitTypeName(info),
		rankStr(u->rank));
#else
	MemPtr<char> nameBuf(500);
	nameBuf[0] = 0;

	const Unit* u1 = u;
	while(u1->getRank() >= Rank_Army)
	{
		sprintf(nameBuf + strlen(nameBuf), "%s\r", u1->getName(False));
		u1 = u1->superior;
	}
	sprintf(nameBuf + strlen(nameBuf), "%s %s", 
		getUnitTypeName(info),
		rankStr(u->rank));

#endif

#if defined(DEBUG) && !defined(TESTBATTLE)
	if(inCampaign)
	{
		/*
		 * Show whether occupying or sieging
		 */

		if(u->occupying)
		{
#if 0
			region.setClip(window->getX(), SDimension(window->getY() + 28), 122, 28);
			textWind.setPosition(Point(0,40));
			textWind.setFont(Font_CommandName);
		
			if(u->siegeAction == SIEGE_Occupying)
				textWind.setColours(Black);
			else if(u->siegeAction == SIEGE_Besieging)
				textWind.setColours(Red);
			else if(u->siegeAction == SIEGE_Standing)
				textWind.setColours(Green);

			textWind.draw(u->occupying->getNameNotNull());
#else
			if(u->siegeAction == SIEGE_Occupying)
				strcat(nameBuf, "\rOccupying: ");
			else if(u->siegeAction == SIEGE_Besieging)
				strcat(nameBuf, "\rSieging: ");
			else if(u->siegeAction == SIEGE_Standing)
				strcat(nameBuf, "\rStanding: ");

			strcat(nameBuf, u->occupying->getNameNotNull());
#endif

		}
	}

#endif
	if(u->getRank() >= Rank_Brigade)
		textWind.setFont(Font_UnitNameSmall);
	textWind.draw(nameBuf);

	/*
	 * Commander's name
	 */

	region.setClip(window->getX(), SDimension(window->getY() + 56), 122, 32);
	textWind.setFont(Font_CommandName);
	textWind.setColours(Black);

	if(u->general)
		sprintf(buffer, "%s\r%s", language(lge_CommandedBy), u->general->getName());
	else
		strcpy(buffer, language(lge_NoCommander));
	textWind.draw(buffer);

	/*
	 * Total troops of each kind (infantry/cavalry/infantry)
	 */

#if 0
	if(canSee || (game->getDifficulty(Diff_CommandControl ) < Complex3))
#endif
	{
		region = *window;		// Get back to complete window
		textWind.setFormat(False, False);		// No centering
		textWind.setFont(Font_TroopStrength);
		textWind.setPosition(Point(30,96));
		sprintf(buffer, "%lu", (unsigned long)info.infantryStrength);
		textWind.draw(buffer);

		textWind.setPosition(Point(30,121));
		sprintf(buffer, "%lu", (unsigned long)info.cavalryStrength);
		textWind.draw(buffer);
	
		textWind.setPosition(Point(30,146));
		sprintf(buffer, "%lu", (unsigned long)info.artilleryStrength);
		textWind.draw(buffer);
	}

	if(canSee || (game->getDifficulty(Diff_CommandControl ) < Complex1))
	{
		showStrengthBar(window, &info, Rect(2,169,118,7));
	 
		/*
	    * Morale Bar chart
	 	 */

		showBar(window, info.morale, MaxAttribute, Rect(22,184,99,7), moraleStr(info.morale));
		showBar(window, MaxAttribute - info.fatigue, MaxAttribute, Rect(22,200,99,7), fatigueStr(info.fatigue));
		showBar(window, info.supply, MaxAttribute, Rect(22,216,99,7), supplyStr(info.supply));

	   /*
	 	 * Current Order
	 	 */

		GameSprites icon;

#if defined(TESTCAMP)
		if(u->givenOrders.how == ORDER_Land)
			icon = (GameSprites) u->givenOrders.getOrderIcon();
		else if(u->givenOrders.how == ORDER_Rail)
			icon = C2_BigRail;
		else if(u->givenOrders.how == ORDER_Water)
			icon = C2_BigWater;
#elif defined(TESTBATTLE)
	  	icon = (GameSprites) u->battleInfo->battleOrder.getOrderIcon();
#else
		if(inCampaign)
		{
			if(u->givenOrders.how == ORDER_Land)
				icon = (GameSprites) u->givenOrders.getOrderIcon();
			else if(u->givenOrders.how == ORDER_Rail)
				icon = C2_Rail;
			else if(u->givenOrders.how == ORDER_Water)
				icon = C2_Water;
		}
		else
		{
			if(u->battleInfo)
				icon = (GameSprites) u->battleInfo->battleOrder.getOrderIcon();
			else
				icon = C2_HastyRetreat;
		}
#endif

		region.setClip(window->getX(), window->getY() + 229, window->getW(), 55);
		game->sprites->drawCentredSprite(&region, icon);
	}

	/*
	 * Senior Commander
	 */

	region.setClip(window->getX(), SDimension(window->getY() + 284), 122, 28);
	textWind.setFormat(True, True);
	textWind.setFont(Font_CommandName);

#if 0
	if(u->superior && u->superior->general)
		textWind.draw(u->superior->general->getName());
#else

	if(canSee || (game->getDifficulty(Diff_CommandControl ) < Complex2))
	{
#if !defined(TESTBATTLE)
		if(inCampaign)
			textWind.draw(getOrderStr(u->givenOrders));
#endif	// TESTBATTLE
#if !defined(TESTBATTLE) && !defined(TESTCAMP)
		else
#endif
#if !defined(TESTCAMP)
		{
			if(u->battleInfo)
				textWind.draw(getOrderStr(u->battleInfo->battleOrder));
			else
				textWind.draw(getOrderStr(ORDER_HastyRetreat));
		}
#endif	// TESTCAMP
	}
#endif	// 0

	machine.screen->setUpdate(*window);
}



#if !defined(TESTBATTLE)
void Unit::showCampaignInfo(Region* window)
{
	showUnitInfo(this, window, True);
}
#endif

#if !defined(TESTCAMP)
void Unit::showBattleInfo(Region* window)
{
	showUnitInfo(this, window, False);
}
#endif

void showUnitName(Region* window, const Unit* u)
{
	Region region = *window;

	// int y = window->getH() - 12;

	// region.setClip(window->getX(), window->getY(), window->getW(), y);

	region.frame(0, 0, region.getW(), region.getH(), Black, Greyc);

	TextWindow textWind(&region, Font_UnitName);
	textWind.setWrap(True);

	textWind.setColours(Black);
	textWind.setFormat(True, True);		// Horizontal & Vertical centre

	MemPtr<char> nameBuf(500);
	nameBuf[0] = 0;

	if(u->general)
		sprintf(nameBuf, "%s\r", u->general->getName());
	else
		sprintf(nameBuf, "%s\r", language(lge_NoCommander));

	UnitInfo info(True, True);
	u->getInfo(info);

	const Unit* u1 = u;
	while(u1->getRank() >= Rank_Army)
	{
		sprintf(nameBuf + strlen(nameBuf), "%s\r", u1->getName(False));
		u1 = u1->superior;
	}
	sprintf(nameBuf + strlen(nameBuf), "%s %s", 
		getUnitTypeName(info),
		rankStr(u->rank));

	if(u->getRank() >= Rank_Brigade)
		textWind.setFont(Font_UnitNameSmall);

	textWind.draw(nameBuf);

#if 0
	/*
	 * Commander's name
	 */

	region.setClip(window->getX(), window->getY() + y, window->getW(), 12);

	region.frame(0, 0, region.getW(), region.getH(), Black, Greyc);

	textWind.setFont(Font_CommandName);
	textWind.setColours(Black);

	if(u->general)
		textWind.draw(u->general->getName());
	else
		textWind.draw(language(lge_NoCommander));
#endif
}
