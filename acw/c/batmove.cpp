/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battle Movement for Units
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */
#include <stdio.h>
#include <string.h>
#include "batmove.h"
#include "batltab.h"
#include "tables.h"
#include "unit3d.h"
#include "generals.h"

#ifndef BATEDIT
#include "map3d.h"
#include "game.h"
#include "ai_bat.h"

#include "batldata.h"
#ifdef DEBUG
#include "msgwind.h"
#include "clog.h"

#endif

#endif	// BATEDIT

#ifdef DEBUG
#define DEBUG_LIMBER
#endif

/*
 * Brigade Formations
 */

/*
 * Indeces into cCountTable
 * This is looked up as brigadeFormation[aCountTable[artilleryCount] + infantryCount];
 */

static UBYTE aCountTable[7] = { 0, 7, 13, 18, 22, 25, 27 };
static UBYTE brigadeIndex[28] = {
	0,0,1,0,0,0,0,
	0,0,0,0,0,0,
	1,0,1,2,3,
	0,4,4,5,
	6,6,6,
	6,6,
	6
};

struct BrigadeFollowElement {
#if defined(NOBITFIELD)
	UBYTE followIndex;
	FollowHow followDir;
#else
	UBYTE followIndex:4;
	FollowHow followDir:4;
#endif
};

typedef BrigadeFollowElement BrigadeFormation[5];

static BrigadeFormation brigadeFormations[] = {
	{	// 0
		/* 
		 * 102
		 * 435
		 */
	    
		{ 0, Follow_Left },
		{ 0, Follow_Right },
		{ 0, Follow_Behind },
		{ 1, Follow_Behind },
		{ 2, Follow_Behind }
	},
	{	// 1
		/*
		 * 2013
		 * 4  5
		 */

		{ 0, Follow_Right },
		{ 0, Follow_Left },
		{ 1, Follow_Right },
		{ 2, Follow_Behind },
		{ 3, Follow_Behind }

	},
	{	// 2
		/*
		 * 2013
		 * 4 5
		 */

		{ 0, Follow_Right },
		{ 0, Follow_Left },
		{ 1, Follow_Right },
		{ 2, Follow_Behind },
		{ 1, Follow_Behind }

	},
	{	// 3
		/*
		 * 2013
		 *  45
		 */

		{ 0, Follow_Right },
		{ 0, Follow_Left },
		{ 1, Follow_Right },
		{ 0, Follow_Behind },
		{ 1, Follow_Behind }
	},
	{	// 4
		/*
		 * 42013
		 * 5
		 */

		{ 0, Follow_Right },
		{ 0, Follow_Left },
		{ 1, Follow_Right },
		{ 2, Follow_Left },
		{ 4, Follow_Behind }
	},
	{	// 5
		/*
		 * 42013
		 *   5
		 */

		{ 0, Follow_Right },
		{ 0, Follow_Left },
		{ 1, Follow_Right },
		{ 2, Follow_Left },
		{ 0, Follow_Behind }
	},
	{	// 6
		/*
		 * 420135
		 */

		{ 0, Follow_Right },
		{ 0, Follow_Left },
		{ 1, Follow_Right },
		{ 2, Follow_Left },
		{ 3, Follow_Right }
	},
};

inline BrigadeFormation* getBrigadeFormation(int aCount, int iCount, int cCount)
{
	return &brigadeFormations[brigadeIndex[aCountTable[aCount] + iCount]];
}

#ifndef BATEDIT

#if 0
inline Wangle rotUnit(Wangle rot, Wangle rate, int ticks)
{
	if( (rate * ticks) < 0x8000)
	{
		rate *= ticks;

		if(rot > 0x8000)
		{
			if(rot < Wangle(-rate))
				rot = Wangle(-rate);
		}
		else if(rot > rate)
			rot = rate;
	}

	return rot;
}
#else

inline Wangle rotUnit(Wangle start, Wangle want, Wangle rate, int ticks)
{
	Wangle diff = want - start;
	ULONG rot = rate * ticks;

	if(diff > 0x8000)
	{
		diff = -diff;
		if(rot < diff)
			start -= diff;
		else
			start = want;
	}
	else
	{
		if(rot < diff)
			start += diff;
		else
			start = want;
	}

	return start;
}

#endif

void UnitBattle::checkFinishedOrder()
{
	Boolean finished = True;

	Unit* u = unit->child;
	while(u)
	{
		if(u->battleInfo && !u->battleInfo->isDead && u->battleInfo->followingOrder && !u->battleInfo->bReallySeperated)
		{
			finished = False;
			break;
		}
		u = u->sister;
	}


	if(finished)
	{
#ifdef DEBUG
		bLog.printf("%s completed order", unit->getName(True));
		if (u && u->battleInfo) 
			bLog.printf("followingOrder = %d", u->battleInfo->followingOrder );
#endif

		followingOrder = False;

		/*
		 * Call AI if independant unit on AI's side
		 */

		if(bReallySeperated && battle->ai &&
			// (unit->getSide() != game->playersSide) &&
			game->isAI(sideIndex(unit->getSide())) &&
			(battleOrder.basicOrder() != ORDERMODE_Hold) )
		{
#ifdef DEBUG
			bLog.printf("AIReachedClickPoint called (order = %s)",
				getOrderStr(battleOrder));
#endif
			battle->ai->AIReachedClickPoint(unit);
		}
	}
}

/*
 * Move a Division or above
 */

Boolean UnitBattle::moveBattle(const TimeBase& timeNow)
{
	if(isDead)
		return False;

	if(timeNow == lastBattleTime)
		return True;

	processOrderQueue(timeNow);

	GameTicks ticks = lastBattleTime ? GameTicks(timeNow - lastBattleTime) : 1;
	lastBattleTime = timeNow;

	if(ticks == 0)
		return True;

	Location destination = where;	// Where they want to go this frame

	/*
	 * If joining, alter order
	 *
	 * Added 24/5/95
	 */

	// if(bReallySeperated && bJoining && unit->superior->battleInfo)
	if((unit->getBattleAttachMode() == Joining) && unit->superior->battleInfo)
	{
		// battleOrder.destination = unit->superior->battleInfo->battleOrder.destination;
		battleOrder = unit->superior->battleInfo->battleOrder;

	}

	/*
	 * If current Unit is president then basically ignore it because
	 * they never move or do anything.
	 */

	if(unit->rank == Rank_President)
	{
		/*
		 * President's don't really move
		 * But if we do want them to do anything it should be done here.
		 */

		 return False;
	}
	else
	{
		/*
		 * Work out average location of units
		 */

		int childCount = 0;
		Unit* child = unit->child;
		while(child)
		{
			if(child->battleInfo && !child->battleInfo->isDead && !child->battleInfo->routed)
			{
				if(!child->isBattleReallyDetached())
				{
#ifdef GOTO_MID
					/*
					 * Update average location
					 */

					if(childCount)
					{
						// destination.x = ((destination.x * childCount) + child->battleInfo->where.x) / (childCount + 1);
						// destination.y = ((destination.y * childCount) + child->battleInfo->where.y) / (childCount + 1);

						destination.x += child->battleInfo->where.x;
						destination.y += child->battleInfo->where.y;

					}
					else
						destination = child->battleInfo->where;
#else
					if(!childCount)
						destination = child->battleInfo->where;
#endif

					childCount++;

					/*
				 	 * Update children's orders
				 	 */

					// if(bReallySeperated && bJoining && unit->superior->battleInfo)
					if((unit->getBattleAttachMode() == Joining) && unit->superior->battleInfo)
					{
						if( (battleOrder.mode != ORDER_HastyRetreat) &&
							 (battleOrder.mode != ORDER_Attack) )
						{
							child->battleInfo->battleOrder.destination = unit->superior->battleInfo->where;
						}
					}
				}
			}

			child = child->sister;
		}

		if(childCount == 0)
		{
			/*
			 * No Children...
			 *   Try and attach to one
			 */

			child = unit->child;
			while(child)
			{
				if(child->battleInfo && !child->battleInfo->isDead && !child->battleInfo->routed)
					break;
				child = child->sister;
			}

			if(child)
			{
				child->rejoinBattle();

				destination = child->battleInfo->where;
			}
		}
#ifdef GOTO_MID
		else
		{
			destination.x /= childCount;
			destination.y /= childCount;
		}
#endif

		/*
	 	 * Sort out facing
	 	 */

		Wangle newFacing = menFacing;

		if(destination != where)
		{
			Location diff = destination - where;
			newFacing = direction(diff.x, diff.y);
		}

#if 0
		Wangle dirChange = newFacing - menFacing;
		if(dirChange)
			menFacing += rotUnit(dirChange, 0x400, ticks);
#else
		if(newFacing != menFacing)
			menFacing = rotUnit(menFacing, newFacing, 0x400, ticks);
#endif

		if(where != destination)
		{
			// UnitInfo inf;
			// unit->getInfo(inf, True);

			/*
		 	 * 	Code to allow for diff movement method for cautious Advance
		 	 *
		 	 *		All we do is change the speed of movement.
		 	 */

			Speed moveSpeed = commanderMarchSpeed;	// YardsPerHour(4500);
			getBattleOrderEffect(moveSpeed);
			getBattleTerrainEffect(moveSpeed);

			Distance md;
			moveLocation(where, destination, moveSpeed, ticks, &md);

			// animation++;
			updateAnimation(md);
		}
	}

	/*
	 * See if order is completed
	 * If all of its dependant children has finished, then the commander has as well
	 */

	if(followingOrder)
		checkFinishedOrder();

	/*
	 * If we're trying to rejoin command, see if we are close enough
	 */

	if(unit->superior && unit->superior->battleInfo)
	{
		Distance d = distance(where.x - unit->superior->battleInfo->where.x,
								 	where.y - unit->superior->battleInfo->where.y);

		if(d <= battleCommandRange[unit->superior->rank])
		{
			// if(bReallySeperated && bJoining)
			if((unit->getBattleAttachMode() == Joining))
			{
	 			unit->makeBattleDetached(False);
	 			// unit->makeBattleReallyDetached(False);
	 			// bJoining = False;
				unit->setBattleAttachMode(Attached);

#ifdef DEBUG
	 			battle->message->printf("%s has rejoined command", unit->getName(True));
	 			bLog.printf("%s has rejoined command", unit->getName(True));
#endif
			}
		}
		else if(!bReallySeperated)
		{
			unit->makeBattleDetached(True);
			// unit->makeBattleReallyDetached(True);
			// bJoining = True;
			unit->setBattleAttachMode(Joining);
#ifdef DEBUG
 			bLog.printf("%s too far from commander... rejoining", unit->getName(True));
#endif
		}
	}
	else
		bJoining = False;

	return False;
}

/*
 * Brigade Movement
 */

Boolean BrigadeBattle::moveBattle(const TimeBase& timeNow)
{
	if(timeNow == lastBattleTime)
		return True;

	processOrderQueue(timeNow);

	GameTicks ticks = lastBattleTime ? GameTicks(timeNow - lastBattleTime) : 1;
	lastBattleTime = timeNow;

	if(ticks == 0)
		return True;

	Location destination = where;	// Where they want to go this frame

	/*
	 * If joining, alter order
	 *
	 * Added 24/5/95
	 */

	// if(bReallySeperated && bJoining && unit->superior->battleInfo)
	if((unit->getBattleAttachMode() == Joining) && unit->superior->battleInfo)
	{
		// battleOrder.destination = unit->superior->battleInfo->battleOrder.destination;
		battleOrder = unit->superior->battleInfo->battleOrder;
	}

	{
		/*
		 * Work out average location of units
		 */

		int childCount = 0;
		Unit* child = unit->child;
		while(child)
		{
			if(child->battleInfo && !child->battleInfo->isDead)
			{
				if(!child->isBattleReallyDetached())
				{
					/*
				 	 * Update average location
				 	 */

					if(childCount)
					{
						// destination.x = ((destination.x * childCount) + child->battleInfo->where.x) / (childCount + 1);
						// destination.y = ((destination.y * childCount) + child->battleInfo->where.y) / (childCount + 1);
						destination.x += child->battleInfo->where.x;
						destination.y += child->battleInfo->where.y;
					}
					else
						destination = child->battleInfo->where;
					childCount++;

					/*
				 	 * Update children's orders
					 *
				 	 */

					// if(bReallySeperated && bJoining && unit->superior->battleInfo)
					if((unit->getBattleAttachMode() == Joining) && unit->superior->battleInfo)
					{
						if( (battleOrder.mode != ORDER_HastyRetreat) &&
							 (battleOrder.mode != ORDER_Attack) )
						{
							child->battleInfo->battleOrder.destination = unit->superior->battleInfo->where;
						}
					}
				}
				else
				{
					if(childCount == 0)
						destination = child->battleInfo->where;
				}
			}

			child = child->sister;
		}

		if(childCount)
		{
			destination.x /= childCount;
			destination.y /= childCount;
		}
	}

	if(followingOrder)
		checkFinishedOrder();

	Wangle newFacing = menFacing;
	if(destination != where)
	{
		Distance d = distanceLocation(destination, where);

		if(d > Yard(5))
			newFacing = directionLocation(where, destination);
		else
			destination = where;
	}

	Wangle dirChange = newFacing - formationFacing;

	if( (dirChange > 0x4000) && (dirChange < 0xc000) )
		flipped = !flipped;
	formationFacing = newFacing;	// Used by the 1st regiment to know which direction to face

	/*
	 * Rotate commander
	 */

	// dirChange = newFacing - menFacing;
	// if(dirChange)
	if(newFacing != menFacing)
	{
		// menFacing += rotUnit(dirChange, 0x400, ticks);
		menFacing = rotUnit(menFacing, newFacing, 0x400, ticks);
	}

	if(where != destination)
	{
		// UnitInfo inf;
		// unit->getInfo(inf, True);

		/*
		 	* 	Code to allow for diff movement method for cautious Advance
		 	*
		 	*		All we do is change the speed of movement.
		 	*/

		Speed moveSpeed = commanderMarchSpeed;	// YardsPerHour(4500);
		getBattleOrderEffect(moveSpeed);
		getBattleTerrainEffect(moveSpeed);

		Distance md;
		moveLocation(where, destination, moveSpeed, ticks, &md);

		updateAnimation(md);
		// animation++;
	}

	/*
	 * If we're trying to rejoin command, see if we are close enough
	 */

	if(unit->superior && unit->superior->battleInfo)
	{
		Distance d = distance(where.x - unit->superior->battleInfo->where.x,
								 	where.y - unit->superior->battleInfo->where.y);

		if(d <= battleCommandRange[unit->superior->rank])
		{
			// if(bReallySeperated && bJoining)
			if(unit->getBattleAttachMode() == Joining)
			{
				unit->makeBattleDetached(False);
				// unit->makeBattleReallyDetached(False);
				// bJoining = False;
				unit->setBattleAttachMode(Attached);

#ifdef DEBUG
				battle->message->printf("%s has rejoined command", unit->getName(True));
				bLog.printf("%s has rejoined command", unit->getName(True));
#endif
			}
		}
		else if(!bReallySeperated)
		{
			unit->makeBattleDetached(True);
			// unit->makeBattleReallyDetached(True);
			// bJoining = True;
			unit->setBattleAttachMode(Joining);
#ifdef DEBUG
 			bLog.printf("%s too far from commander... rejoining", unit->getName(True));
#endif
		}
	}
	else
		bJoining = False;

	return False;
}

#endif	// !BATEDIT

/*
 * Set up regiment formation
 * If place is TRUE then they are immediately placed in formation
 */

void BrigadeBattle::setupRegiments(Boolean place)
{
	/*
	 * Sort regiments into correct order (artillery, infantry, cavalry)
	 * and count how many of each
	 */

	int aCount = 0;
	int iCount = 0;
	int cCount = 0;

	int rCount = 0;			// Bodge to make sure no more than 6 regiments

	Regiment* rPtr[MaxRegimentsPerBrigade + 1];			// extra one to be NULL to mark end of list
	Regiment** rPtrPtr = rPtr;
	memset(rPtr, 0, sizeof(rPtr));		// Initialise to NULL

	Unit* child = unit->child;
	while(child && (rCount < MaxRegimentsPerBrigade))
	{
		// if(child->battleInfo && !child->battleInfo->isDead && !child->isBattleReallyDetached())
		if(child->battleInfo && !child->battleInfo->isDead && (child->getBattleAttachMode() != Detached))
		{
			Regiment* r = (Regiment*) child;
			if(r->type.basicType == Artillery)
			{
				*rPtrPtr++ = r;
				aCount++;
				rCount++;
			}
		}
		child = child->sister;
	}
	child = unit->child;
	while(child && (rCount < MaxRegimentsPerBrigade))
	{
		// if(child->battleInfo && !child->battleInfo->isDead && !child->isBattleReallyDetached())
		if(child->battleInfo && !child->battleInfo->isDead && (child->getBattleAttachMode() != Detached))
		{
			Regiment* r = (Regiment*) child;
			if(r->type.basicType == Infantry)
			{
				*rPtrPtr++ = r;
				iCount++;
				rCount++;
			}
		}
		child = child->sister;
	}
	child = unit->child;
	while(child && (rCount < MaxRegimentsPerBrigade))
	{
		// if(child->battleInfo && !child->battleInfo->isDead && !child->isBattleReallyDetached())
		if(child->battleInfo && !child->battleInfo->isDead && (child->getBattleAttachMode() != Detached))
		{
			Regiment* r = (Regiment*) child;
			if(r->type.basicType == Cavalry)
			{
				*rPtrPtr++ = r;
				cCount++;
				rCount++;
			}
		}
		child = child->sister;
	}

	/*
	 * Get the correct formation layout
	 */

	BrigadeFormation* bForm = getBrigadeFormation(aCount, iCount, cCount);
	BrigadeFollowElement* el = 0;

	/*
	 * Assign each regiment to the correct location
	 */

	rPtrPtr = rPtr;
	while(*rPtrPtr)
	{
		Regiment* r = *rPtrPtr++;

#ifndef BATEDIT
		r->battleInfo->setBattleOrder(battleOrder);
#endif

		if(el)
		{
			r->battleInfo->following = rPtr[el->followIndex];
			r->battleInfo->followHow = el->followDir;
			if(flipped)
			{
				if(r->battleInfo->followHow == Follow_Left)
					r->battleInfo->followHow = Follow_Right;
				else
				if(r->battleInfo->followHow == Follow_Right)
					r->battleInfo->followHow = Follow_Left;
			}

			el++;
		}
		else
		{
			/*
			 * 1st Regiment follows the brigade location
			 */

			r->battleInfo->following = 0;
			r->battleInfo->followHow = Follow_None;

			el = bForm[0];
		}
		
		if(place)
		{
			RegimentBattle* rb = (RegimentBattle*) r->battleInfo;
			rb->setFormationRows(True);
			rb->getFollowLocation(rb->where);
			rb->formationFacing = rb->menFacing = formationFacing;
		}
	}
}

void BrigadeBattle::placeRegiments()
{
	/*
	 * Put on top of brigade
	 */

	Unit* r = unit->child;

	while(r)
	{
		if(r->battleInfo && !r->battleInfo->isDead)
			r->battleInfo->where = where;

		r = r->sister;
	}

	/*
	 * Work out who's following who
	 */

	setupRegiments(True);

}


/*
 * Get the location that a regiment should be at
 *
 * Returns True if regiment is having to wait for the others
 * to catch up.  This is used by movement to avoid for example
 * artillery unlimbering whilst waiting for the rest of the
 * regiment.
 */

Boolean RegimentBattle::getFollowLocation(Location& destination)
{
	Boolean waiting = False;

	/*
	 * Bodge to force reseting of formation if who we're following is dead!
	 */

	if(following && !following->battleInfo)
	{
		if(unit->superior->battleInfo)
		{
			BrigadeBattle* bb = (BrigadeBattle*) unit->superior->battleInfo;
			bb->setupRegiments(False);
		}
		else
			following = 0;
	}

	if(following && following->battleInfo)
	{

		/*
		 * Work out the size of the thing we're following
		 */

		Distance fWidth;
		Distance fHeight;

		following->battleInfo->getArea(fWidth, fHeight);

		/*
		 * Get the size of ourself!
		 */

		Distance rWidth;
		Distance rHeight;

		getArea(rWidth, rHeight);

		/*
		 * Get resulting size
		 */

		rWidth = (fWidth + rWidth) / 2;
		rHeight = (fHeight + rHeight) / 2;

		/*
		 * Get the angle... relative to the brigade's formation facing
		 */

#if 0		// This code is a bit silly... and causes troops to rotate a lot
		Wangle rotation = unit->superior->battleInfo->formationFacing;


		Wangle beta = formationFacing - rotation;

		if(beta)
		{
			fWidth  = rWidth;
			fHeight = rHeight;

			rWidth  = abs(mcos(fWidth, beta)) + abs(msin(fHeight, beta));
			rHeight = abs(msin(fWidth, beta)) + abs(mcos(fHeight, beta));
		}
#else
		Wangle rotation;

		if(following->battleInfo->followingOrder)
			rotation = unit->superior->battleInfo->formationFacing;
		else
			rotation = following->battleInfo->formationFacing;
#endif

		/*
		 * Set up base location
		 */

		destination = following->battleInfo->where;

		/*
		 * Follow regiment properly!
		 */

		switch(followHow)
		{
		case Follow_Left:
			destination.x += -msin(rWidth, rotation);
			destination.y += +mcos(rWidth, rotation);
			break;

		case Follow_Right:
			destination.x += +msin(rWidth, rotation);
			destination.y += -mcos(rWidth, rotation);
			break;

		case Follow_Behind:
			destination.x += -mcos(rHeight, rotation);
			destination.y += -msin(rHeight, rotation);
			break;

			break;
		case Follow_None:
#ifdef DEBUG
			throw GeneralError("Unit has following set, but mode of Follow_None");
#endif
			break;
		}
	}
#ifndef BATEDIT
	else
	{
		destination = battleOrder.destination;

		/*
		 * If they are not being hasty, then try to keep with
		 * the rest of the brigade.
		 */

		if( (battleOrder.mode != ORDER_Attack) &&
		    (battleOrder.mode != ORDER_HastyRetreat) )
		{
			Location& supLoc = unit->superior->battleInfo->where;

			Distance d = distanceLocation(where, supLoc);
			Distance range = battleCommandRange[Rank_Brigade];

			/*
			 * If a long way away, then go towards commander
			 * If not so far, then wait for him.
			 * If nearer then just slow down.
			 */

			if(d > range)
				destination = supLoc;
			else if(d > ((range * 3) / 4))
			{
				destination = where;
				waiting = True;
			}
			else if(d > (range / 2))
			{
				waiting = True;
			}
		}
	}
#endif

	return waiting;
}


void RegimentBattle::setFormationRows(Boolean force)
{
	Regiment* r = getRegiment();

	int sprites;
	UBYTE desiredRows;
	UBYTE desiredCols;

	switch(r->type.basicType)
	{
	case Infantry:
		sprites = (r->strength + MenPerInfantrySprite - 1) / MenPerInfantrySprite;
		if(formation == Form_Column)
		{
			desiredCols = 4;
			desiredRows = (sprites + 3) / 4;
		}
		else
		{
			desiredRows = 2;
			desiredCols = (sprites + 1) / 2;
		}
		break;
	case Cavalry:
		sprites = (r->strength + MenPerCavalrySprite - 1) / MenPerCavalrySprite;
		if(formation == Form_Column)
		{
			desiredCols = 4;
			desiredRows = (sprites + 3) / 4;
		}
		else
		{
			desiredRows = 1;
			desiredCols = sprites;
		}
		break;
	case Artillery:
		sprites = (r->strength + MenPerArtillerySprite - 1) / MenPerArtillerySprite;
		if(formation == Form_Column)
		{
			desiredCols = 3;
			desiredRows = (sprites + 2) / 3;
		}
		else
		{
			desiredRows = 1;
			desiredCols = sprites;
		}
		break;
	}


	if(force || (rows == 0))
	{
		columns = desiredCols;
		rows = desiredRows;
	}
	else
	if(columns < desiredCols)	// These need adjusting for real time
	{
		columns++;
		rows = (sprites + columns - 1) / columns;
	}
	else if(columns > desiredCols)
	{
		columns--;
		rows = (sprites + columns - 1) / columns;
	}
}

#ifndef BATEDIT

void UnitBattle::getBattleTerrainEffect(Speed &moveSpeed)
{
	if(game->getDifficulty(Diff_TerrainEffects) >= Complex1)
	{

		// return;

		Grid* gr = battle->grid->getGrid( Zoom_1 );

		MapGridCord lgx = battle->grid->cord3DtoGrid(DistToBattle(where.x));
		MapGridCord lgy = battle->grid->cord3DtoGrid(DistToBattle(where.y));

		if(!gr->isValidTCord(lgx, lgy))
			return;

		TerrainType ter = *gr->getTSquare( lgx, lgy );

		const TerrainVal* Tval = battle->data3D.terrain.getValues( ter );

		int change = 255;

		if(unit->getRank() == Rank_Regiment)
		{
			Regiment* r = (Regiment*) unit;

			if(r->type.basicType == Infantry)
				change = Tval->infantrySpeed;
			else if(r->type.basicType == Cavalry)
				change = Tval->cavalrySpeed;
			else
				change = Tval->artillerySpeed;
		}
		else		// All commander's wander about on horses
		{
			change = Tval->cavalrySpeed;
		}

		if ( change != 255) 
			moveSpeed = (moveSpeed * change) / 256;	// was 255, but /256 is faster and not much less accurate

		if (!moveSpeed)
			moveSpeed = 10;
	}
}

void UnitBattle::getBattleOrderEffect(Speed &moveSpeed )
{
	// OrderMode mode = unit->battleInfo->battleOrder.mode;
	OrderMode mode = battleOrder.mode;

	if ( mode == ORDER_Advance || mode == ORDER_Retreat )
#if 0
		moveSpeed += ( moveSpeed * 20 ) / 100;
#else
		moveSpeed += moveSpeed / 5;
#endif

	else if ( mode == ORDER_Attack || mode == ORDER_HastyRetreat )
	{
#if 0	// What is Chris playing at here????
  		UnitInfo inf;
  		unit->getInfo(inf, True);

  		moveSpeed = inf.speed + ( moveSpeed * 50 ) / 100;
#else
		moveSpeed += moveSpeed / 2;
#endif
  	}
}

Boolean RegimentBattle::moveBattle(const TimeBase& timeNow)
{
	if(timeNow == lastBattleTime)
		return True;

	Regiment* r = getRegiment();

	processOrderQueue(timeNow);

	GameTicks ticks = lastBattleTime ? GameTicks(timeNow - lastBattleTime) : 1;
	lastBattleTime = timeNow;

	if(limberCount)
	{
		if(limberCount <= ticks)
			limberCount = 0;
		else
			limberCount -= ticks;
#ifdef DEBUG_LIMBER
		bLog.printf("%s limberCount=%d", unit->getName(True), (int) limberCount);
#endif
	}

	Wangle newFormFacing = formationFacing;
	Wangle newMenFacing = menFacing;

	if(ticks == 0)
		return True;

	Boolean faceChange = False;

	if(routed)
	{
		UnitInfo inf(False, True);
		unit->getInfo(inf);

		inf.speed += inf.speed/2;		// Increase speed

		Distance d = (inf.speed * ticks) / 256;

		where += Location(mcos(d, menFacing), msin(d, menFacing));

		/*
		 * Force it to be limbered
		 */

		if(canUnlimber())
		{
			limber();
			limberCount = 0;
		}

		/*
		 * Should do checks to stop routing after a while!
		 *
		 * For now let us do it after 500 yards.
		 */

		movedDistance += d;

		if(movedDistance > Yard(500))
		{
			stopRouting();
		}

		return False;
	}

	/*
	 * If joining, alter order
	 *
	 * Added 24/5/95
	 */

	// if(bReallySeperated && bJoining && unit->superior->battleInfo)
	if((unit->getBattleAttachMode() == Joining) && unit->superior->battleInfo)
	{
		// battleOrder.destination = unit->superior->battleInfo->battleOrder.destination;
		battleOrder = unit->superior->battleInfo->battleOrder;
	}


	switch(battleOrder.mode)
	{
	case ORDER_CautiousAdvance:
		wantFormation = Form_Line;
		faceChange = True;
		break;
	case ORDER_Advance:
		wantFormation = Form_Column;
		faceChange = True;
		break;
	case ORDER_Attack:
		faceChange = True;
		break;

	case ORDER_Stand:
	case ORDER_Hold:
	case ORDER_Defend:
	case ORDER_Withdraw:
	case ORDER_Retreat:
		// unchanged formation
		break;

	case ORDER_HastyRetreat:
#if 0
		{
			if(onLargeBattleField(where))
			{
				// menFacing = unit->superior->battleInfo->menFacing;

				UnitInfo inf(False, True);
				unit->getInfo(inf);

				Distance d = (inf.speed * ticks) / 256;

				where += Location(mcos(d, menFacing), msin(d, menFacing));
			}

			return False;
		}
#endif
		break;

	}


	Location destination = where;
	Boolean waitCatchUp = False;		// Waiting for rest of formation to catch up

	if(combatMode == CM_Firing)
	{
		if(target && (target->battleInfo))
		{
			Location diff = target->battleInfo->where - where;
			newMenFacing = direction(diff.x, diff.y);
			newFormFacing = newMenFacing;
		}
	}
	else
	if(battleOrder.basicOrder() == ORDERMODE_Hold)
	{
		if(battleOrder.destination != where)
		{
			Location diff = battleOrder.destination - where;
			newMenFacing = direction(diff.x, diff.y);
			newFormFacing = newMenFacing;
		}
	}
	else
		waitCatchUp = getFollowLocation(destination);

	/*
 	 * Move to new destination
 	 */


	if(where != destination)
	{
		// Bodge to prevent change of facing if distance < 5 Yards.

		Distance newDistance = distanceLocation(where, destination);

		if(newDistance >= Yard(5))
		{
			Location diff = destination - where;
			newMenFacing = direction(diff.x, diff.y);

			if(faceChange && unit->superior->battleInfo)
				newFormFacing = unit->superior->battleInfo->formationFacing;
		}
		else
			destination = where;
	}
	else
	{
		if(!waitCatchUp)
		{
			if(battleOrder.mode == ORDER_Withdraw)
				newMenFacing = previousFacing;
			else if(r->type.basicType == Artillery)
	 			newMenFacing = formationFacing;
		}
	}

	/*
 	 * Adjust menFacing
 	 */

	// Wangle dirChange = newMenFacing - menFacing;
	// if(dirChange)
	if(newMenFacing != menFacing)
	{										
		/*
	 	 * Infantry can change instantly
	 	 */

		if(r->type.basicType == Infantry)
			menFacing = newMenFacing;

		/*
	 	 * Otherwise cavalry and artillery must rotate slowly
	 	 */

		else
		{
			Wangle rate;

			if(r->type.basicType == Cavalry)
				rate = 65536 / (GameTicksPerSecond * 30);	// 3 seconds to rotate (0x400;)
			else	// assume artillery
			{
				if(isLimbered())
					rate = 65536 / (GameTicksPerSecond * 60);		// 0x200;
				else
					rate = 65536 / (GameTicksPerSecond * 180);	// 0x80;
			}

			// menFacing += rotUnit(dirChange, rate, ticks);
			menFacing = rotUnit(menFacing, newMenFacing, rate, ticks);
		}
	}

	/*
	 * Deal with Melee Flag
	 */

	if(combatMode == CM_Melee)
		doOngoingMelee();

	if(isDead)
		return False;

	/*
	 * Do the actual movement
	 */

	if(where != destination)
	{
		// UnitInfo inf;
		// unit->getInfo(inf, True);

		Location oldLocation = where;

		Distance d;
		
		/*
		 * 	Code to allow for diff movement method for cautious Advance
		 *
		 *		All we do is change the speed of movement.
		 */

		Speed moveSpeed = regimentMarchSpeeds[r->type.basicType][r->type.subType];
		if(waitCatchUp)
			moveSpeed = moveSpeed / 4;	// Slow down

		if(canUnlimber())
		{
			Distance diff = distanceLocation(where, destination);

			if(diff > Yard(20))
				limber();
		}

		if(limberCount != 0)
			moveSpeed = 0;
		adjustLimberSpeed(moveSpeed);
		getBattleOrderEffect(moveSpeed);
		getBattleTerrainEffect(moveSpeed);
		moving = !moveLocation(menFacing, where, destination, moveSpeed, ticks, &d);
		movedDistance += d;

		if(!moving)
			followingOrder = False;

		/*
		 * Do proximity checks and combat logic...
		 */

		combatCheck(oldLocation, menFacing);

		updateAnimation(d);
		// animation++;

	}
	else
	{
		if(!waitCatchUp && moving)
		{
			// Do combat check for having stopped somewhere

			moving = False;
			combatCheck();
		}
		else
			combatCheckStationary();
	}

	/*
	 * Bodge... for master's version
	 */

	if(!moving && !waitCatchUp)
	{
		followingOrder = False;

		if(canUnlimber())
			unLimber();
	}

	/*
	 * If nowhere near enemy then increase morale
	 * aproximately 1 every 20 seconds
	 */

	if(target == 0)
	{
		if(game->gameRand(GameTicksPerSecond * 20) <= ticks)
		{
			increaseMorale(1);
#ifdef DEBUG
			bLog.printf("Morale increased from time");
#endif
 		}
	}


	if(isDead)
		return False;

	/*
	 * Adjust rotation and formation on way to required wantFormation/facing
	 */

	Wangle dirChange = newFormFacing - formationFacing;

	/*
	 * Bodge for Artillery
	 *
	 * If stationary
	 *   IF rotation < 10 degrees
	 *     Stay still (only adjust menFacing)
	 *   ELSE
	 *     Unload guns and rotate
	 */

 	if( (r->type.basicType == Artillery) && !isLimbered())
	{
		if( (dirChange < 0x800) || (dirChange > 0xf800) )
			dirChange = 0;
		else
		{
			loadStatus = 0;
			fireCount = 0;
		}
	}


	if((formation != wantFormation) || dirChange)
	{
		formation = wantFormation;

		if( (dirChange > 0xca00) || (dirChange < 0x3600) )
		{	// Face on, filter
		}
		else if(dirChange < 0x4a00)
		{	// Right turn
			swap(rows, columns);
			formationFacing += 0x4000;
		}
		else if(dirChange < 0xb600)
		{	// Backwards
			formationFacing += 0x8000;
		}
		else
		{	// Left
			swap(rows, columns);
			formationFacing -= 0x4000;
		}

		dirChange = newFormFacing - formationFacing;
	}

	setFormationRows(False);

	if(dirChange)	// For now it rotates 0x400 per pass through this routine
	{
		/*
		 * Alter rate depending on unit type and speed
		 * Artillery to rotate very slowly when unlimbered
		 */

		const Wangle iRate =65536 / (GameTicksPerSecond * 60);	// 1 Minute to rotate
		const Wangle aRate =65536 / (GameTicksPerSecond * 180);	// 3 Minute to rotate

		// Wangle rate = 0x400;
		Wangle rate = iRate;
		if( (r->type.basicType == Artillery) && !isLimbered())
		{
			// rate = 0x40;
			rate = iRate;
		}


		// formationFacing += rotUnit(dirChange, rate, ticks);
		formationFacing = rotUnit(formationFacing, newFormFacing, rate, ticks);
	}


	/*
	 * Deal with Weapon loading and firing
	 */

	fired = False;
	if(!moving && canLoadFire())		// Type specific check... depends on limbered, etc
	{
		/*
		 * Reload weapon...
		 *
		 * Assume that it doesn't overflow?
		 */

		ULONG reload = ticks * ((LoadingResolution * loadingTimes[r->type.basicType][r->type.subType]) / (10 * GameTicksPerMinute));

		/*
		 * Adjust reload value based on regiment attributes
		 *  Fatigue: 0 -> 100%, Maximum -> 30%
		 */

		if(game->getDifficulty(Diff_Fatigue) >= Complex2)
		{
			const int minFatigueEffect = 30;	// was 60%
			int fatigueP = 100 - (r->fatigue * (100 - minFatigueEffect)) / MaxAttribute;

			reload = (fatigueP * reload) / 100;
		}

		/*
		 * Adjust for experience
		 */

		Unit* b = r->superior;
		ASSERT(b != 0);
		General* g = b->general;

		const int minExperienceEffect = 60;	// 60%
		int experienceP = minExperienceEffect + (r->experience * (100 - minExperienceEffect)) / MaxAttribute;
		reload = (experienceP * reload) / 100;

		/*
		 * Adjust for Generals efficiency
		 */

		const int minEfficiencyEffect = 50;	// 30%
		AttributeLevel efficiency = 0;
		if(g)
			efficiency = g->efficiency;
		int effP = minEfficiencyEffect + (efficiency * (100 - minEfficiencyEffect)) / MaxAttribute;
		reload = (effP * reload) / 100;


		reload += loadStatus;

		fireCount += reload / LoadingResolution;
		loadStatus = reload % LoadingResolution;

		animation++;			// Added 27th July 1995

		if(combatMode != CM_Firing)
		{
			if(fireCount)
			{
				fireCount = 0;
				loadStatus = LoadingResolution - 1;
			}
		}

		/*
		 * Now fire the guns
		 */

		if(target && target->battleInfo && !target->battleInfo->isDead && (combatMode == CM_Firing) && fireCount)
			fireGuns();
	}
	else
	{
		loadStatus = 0;		// Empty the guns!
		fireCount = 0;
	}

	return False;
}


void UnitBattle::deployUnit(const Location& loc)
{
	where = loc;

	BattleOrder order (ORDER_Hold, loc);

	setBattleOrder(order);
}

void BrigadeBattle::deployUnit(const Location& loc)
{
	UnitBattle::deployUnit(loc);
 	placeRegiments();
}

void UnitBattle::sendBattleOrder(SentBattleOrder* order)
{
#ifdef DEBUG
	if(this == 0)
		throw GeneralError("sendBattleOrder() has NULL this");
#endif

	/*
	 * Bodge it so hasty retreat order is made to be off the map
	 */

	if(order->mode == ORDER_HastyRetreat)
	{
		Wangle dir = directionLocation(where, order->destination);

		order->destination.x = where.x + mcos(BattleFieldSize, dir);
		order->destination.y = where.y + msin(BattleFieldSize, dir);
	}

	battleOrderList.append(order);
	unit->makeBattleDetached(True);
}

#endif	// BATEDIT

void UnitBattle::setBattleOrder(const BattleOrder& newOrder)
{
#if defined(DEBUG) && !defined(BATEDIT)
	if(newOrder != battleOrder)
	{
		bLog.printf("%s given new order from %s to %s",
			unit->getName(True),
			getOrderStr(battleOrder),
			getOrderStr(newOrder));
	}
#endif

	battleOrder = newOrder;
	followingOrder = True;

	switch(unit->rank)
	{
		case Rank_President:
			return;
		case Rank_Army:
		case Rank_Corps:
		case Rank_Division:
		{
			Location diff = newOrder.destination - where;

			Unit* child = unit->child;
			while(child)
			{
				// if(child->battleInfo && !child->battleInfo->isDead && !child->isBattleReallyDetached())
				if(child->battleInfo && !child->battleInfo->isDead && (child->getBattleAttachMode() != Detached))
				{
					child->battleInfo->setBattleOrder(BattleOrder(newOrder.mode, child->battleInfo->where + diff));
				}
				child = child->sister;
			}

			break;
		}

		case Rank_Brigade:
		{
			((BrigadeBattle*)this)->setupRegiments(False);

			break;
		}

		case Rank_Regiment:
		{
			RegimentBattle* r = (RegimentBattle*)this;
			r->previousFacing = r->menFacing;

			break;
		}
	}

#if 0
	if(newOrder.mode == ORDER_HastyRetreat)
	{
		Wangle dir = directionLocation(newOrder.destination, where);
		menFacing = dir;
	}
#endif
}

#if defined(BATEDIT)

void RegimentBattle::setBattleOrder(const BattleOrder& newOrder)
{
	previousFacing = menFacing;

	battleOrder = newOrder;

	switch(battleOrder.mode)
	{
	case ORDER_CautiousAdvance:
		formation = Form_Line;
		break;
	case ORDER_Advance:
		formation = Form_Column;
		break;
	case ORDER_Attack:
	case ORDER_Stand:
	case ORDER_Hold:
	case ORDER_Defend:
	case ORDER_Withdraw:
	case ORDER_Retreat:
		// unchanged formation
		break;

	case ORDER_HastyRetreat:
#if 0
		{
			Wangle dir = directionLocation(newOrder.destination, where);
			menFacing = dir;
		}
#endif
		break;
	}
}

#endif

#if !defined(BATEDIT)

void UnitBattle::processOrderQueue(const TimeBase& timeNow)
{
	if(battleOrderList.entries())
	{
		SentBattleOrder* order;
		while( (order = battleOrderList.find()) != 0)
		{
			if(order->time <= timeNow)
			{
				/*
			 	 * Set the units order to the new order
				 */

				// battleOrder.setOrder(order);
				setBattleOrder(*order);
				unit->makeBattleDetached(True);
				// unit->makeBattleReallyDetached(True);
				// bJoining = False;
				unit->setBattleAttachMode(Detached);

				/*
				 * Do AI Check for within firing range
				 * of one of enemy units.
				 */

				// if(battle->ai && (unit->getSide() == game->playersSide))
				if(battle->ai && game->isPlayer(unit->getSide()))
				{
#ifdef DEBUG
					bLog.printf("Checking %s for being in range of an AI unit",
						unit->getName(True));
#endif
					/*
					 * Must scan entire AI side for units in range!!!!!
					 */

					Unit* closest = 0;
					Distance closeDistance = 0;


					President* p = battle->ob->getPresident(otherSide(unit->getSide()));
					for(Unit* a = p->child; a; a = a->sister)
					{
	 					if((a->battleInfo && !a->battleInfo->isDead) || (a->hasCampaignReallyDetached()))
	 					{
							for(Unit* c = a->child; c; c = c->sister)
							{
		 						if((c->battleInfo && !c->battleInfo->isDead) || (c->hasCampaignReallyDetached()))
		 						{
									for(Unit* d = c->child; d; d = d->sister)
									{
			 							if((d->battleInfo && !d->battleInfo->isDead) || (d->hasCampaignReallyDetached()))
			 							{
											for(Unit* b = d->child; b; b = b->sister)
											{
				 								if(b->battleInfo && !b->battleInfo->isDead)
				 								{
													for(Regiment* r = (Regiment*)b->child; r; r = (Regiment*)r->sister)
													{
														Distance d = distanceLocation(battleOrder.destination, r->location);

														RegimentBattle* rb = (RegimentBattle*) r->battleInfo;

														if(!rb->isDead )
														{
															if(!closest || (d < closeDistance))
															{
																closest = r;
																// if (d < rb->getEffectiveRange())
																closeDistance = d;
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}

					if ( closeDistance && closest && closest->battleInfo )
					{
						// Wangle eangle = direction( unit->battleInfo->battleOrder.destination.x - closest->battleInfo->where.x, unit->battleInfo->battleOrder.destination.y - closest->battleInfo->where.y );
						Wangle eangle = direction(battleOrder.destination.x - closest->battleInfo->where.x, unit->battleInfo->battleOrder.destination.y - closest->battleInfo->where.y );
					
						Wangle langle = direction( closest->battleInfo->battleOrder.destination.x - closest->battleInfo->where.x, unit->battleInfo->battleOrder.destination.y - closest->battleInfo->where.y );

						// Distance ed = distanceLocation( unit->battleInfo->battleOrder.destination, closest->battleInfo->where );
						Distance ed = distanceLocation(battleOrder.destination, closest->battleInfo->where );

						Distance ld = distanceLocation( closest->battleInfo->battleOrder.destination, closest->battleInfo->where );

						if ( ld < Yard( 1300 ) ) 
						{
							if((closest->rank == Rank_Regiment) && (closest->superior->battleInfo))
								langle = closest->superior->battleInfo->formationFacing;
							else
								langle = closest->battleInfo->formationFacing;
						}

						if ( ( ( ed < ld ) && ( ( eangle - langle ) < 0x2500 ) || ( eangle - langle ) > 0xdb00 )
							|| ed < Yard( 1300 ) )	
								closeDistance = 1;
						else closeDistance = 0;
					}

					if(closeDistance)
					{
#ifdef DEBUG
					bLog.printf("%s is in range of AI unit %s",
						unit->getName(True), closest->getName(True));
#endif
						closest = closest->getTopCommand();
						battle->ai->NewPlayerClickPoint( unit, closest );
					}
				}


				/*
				 * And remove it from the list and from memory!
				 */

				delete battleOrderList.get();
			}
			else
				break;		// Get out, because orders must be in time order
		}
	}
}

void RegimentBattle::setBattleOrder(const BattleOrder& newOrder)
{
	RegimentBattle* r = this;

	battleOrder.destination = newOrder.destination;
	changeOrder(newOrder.mode);

	r->previousFacing = r->menFacing;

	/*
	 * Do any special setup for order
	 */

	switch(newOrder.mode)
	{
	case ORDER_Withdraw:
		doWithdrawCheck();
		break;
	case ORDER_Retreat:
		doRetreatCheck();
		break;
	case ORDER_HastyRetreat:
		doHastyRetreatCheck();
		menFacing = directionLocation(newOrder.destination, where);
		break;
	}
}


Boolean CavInfantryBattle::moveBattle(const TimeBase& timeNow)
{
	Location oldLocation = where;

	Boolean retVal = RegimentBattle::moveBattle(timeNow);

	Regiment* r = getRegiment();

	// if(r->type.basicType == Cavalry)
	if(canUnlimber())
	{
		/*
	 	 * Deal with mounted/unmounted
	 	 */


		if(limberCount == 0)
		{
			Boolean limberChange = False;

			// if(wantLimbMounted || (where != oldLocation))
			if(wantLimbMounted)
			{
				wantLimbMounted = False;
				wantUnLimbMounted = False;

				limbMounted = True;	// Must be mounteded if moving!

				limberChange = True;
			}
			else
			{
				/*
		 	 	 * get them to unmount if stopped and have correct orders
		 	 	 */

				if(wantUnLimbMounted)
#if 0
					||
			 		( isMountedInfantry() &&
			  		((battleOrder.basicOrder() == ORDERMODE_Hold) ||
		      		(battleOrder.mode == ORDER_Advance) ||
			   		(battleOrder.mode == ORDER_CautiousAdvance) ||
			   		(battleOrder.mode == ORDER_Withdraw) ) ) )
#endif
				{
					wantLimbMounted = False;
					wantUnLimbMounted = False;

		 			limbMounted = False;
					limberChange = True;
				}
			}

			if(limberChange)
			{
				limberCount = GameTicksPerSecond * 20; 
#ifdef DEBUG_LIMBER_ALL
				bLog.printf("%s limberCount=%d", unit->getName(True), (int) limberCount);
#endif
			}
		}
	}

	return retVal;
}

Boolean ArtilleryBattle::moveBattle(const TimeBase& timeNow)
{
	ASSERT(this != 0);

	Location oldLocation = where;

	Boolean retVal = RegimentBattle::moveBattle(timeNow);

	/*
	 * Emulate time to limber/unlimber
	 */

	Boolean limberChange = False;

	// following commented out, so can start to limber if halfway through unlimbering
	// if(limberCount < (GameTicksPerSecond * 30))

	if(canUnlimber())
	{
		/*
		 * Deal with limbering and unlimbering
		 */

		// if(wantLimbMounted || (where != oldLocation))
		if(wantLimbMounted)
		{
			wantUnLimbMounted = False;
			wantLimbMounted = False;

			if(!limbMounted)
			{
				limbMounted = True;	// Must be limbered if moving!

				/*
			 	 * Flip the thing 180 degrees to match up with current setting
			 	 */

				menFacing += 0x8000;
				// formationFacing += 0x8000;

				limberChange = True;

#ifdef DEBUG
				bLog.printf("%s limbered", unit->getName(True));
#endif
			}
		}
		else
		{
			/*
		 	 * get them to unlimber if stopped and have correct orders
		 	 */
#if 0
			if( wantUnLimbMounted ||
			 	(battleOrder.basicOrder() == ORDERMODE_Hold) ||
		    	(battleOrder.mode == ORDER_Advance) ||
			 	(battleOrder.mode == ORDER_CautiousAdvance) ||
			 	(battleOrder.mode == ORDER_Attack) ||		// New 20 Sep 95
			 	(battleOrder.mode == ORDER_Withdraw) )
#else

			if(wantUnLimbMounted)
#endif
			{
				wantUnLimbMounted = False;
				wantLimbMounted = False;
		
				if(limbMounted)
				{
					limbMounted = False;

					/*
				 	 * Flip the thing 180 degrees to match up with current setting
				 	 */

					menFacing += 0x8000;
					// formationFacing += 0x8000;

					limberChange = True;
#ifdef DEBUG
					bLog.printf("%s unlimbered", unit->getName(True));
#endif
				}
			}
#if 0
			else
			/*
			 * If stopped and not unlimbered, then get it to change next time
			 * This should reduce the problem of flicking for one frame
			 */

			if((battleOrder.basicOrder() == ORDERMODE_Hold) ||
		    	(battleOrder.mode == ORDER_Advance) ||
			 	(battleOrder.mode == ORDER_CautiousAdvance) ||
			 	(battleOrder.mode == ORDER_Attack) ||		// New 20 Sep 95
			 	(battleOrder.mode == ORDER_Withdraw) )
			{
				wantUnLimbMounted = True;
			}
#endif
		}

		/*
		 * If the limber mode was changed then start timer for how long it
		 * it will take to complete.
		 */

		if(limberChange)
		{
			Regiment* r = getRegiment();
			Unit* b = r->superior;
			ASSERT(b != 0);
			General* g = b->general;

			int seconds = 60;
			seconds += (255 - r->experience) / 4;		// Was /2
			seconds += r->fatigue;

			AttributeLevel eff = 0;
			if(g)
				eff = g->efficiency;

			// limberCount = GameTicksPerSecond * 60; 

			seconds += (255 - eff) / 4;					// Was 2

			UWORD newCount = GameTicksPerSecond * seconds;

			/*
			 * If half way through limbering/unlimbering,
			 * then won't take as long to go back again.
			 */

			if(newCount > limberCount)
			 	limberCount = newCount - limberCount;
			else
				limberCount = newCount;

#ifdef DEBUG_LIMBER
			bLog.printf("Limber time for %s = %d seconds",
				unit->getName(True),
				seconds);
#endif
		}

	}

	return retVal;
}

#endif	// !BATEDIT





#ifdef BATEDIT

/*
 * Set a unit to a particular location
 * If alone is False then all of its children are moved to relative
 * coordinates
 */

void UnitBattle::setPosition(const Location& l, Boolean alone)
{
	if(!alone)
	{
		Location moveDiff = l - where;

		Unit* u = unit->child;
		while(u)
		{
			UnitBattle* ub = u->battleInfo;

			ASSERT(ub != 0);

			ub->setPosition(ub->where + moveDiff, alone);

			u = u->sister;
		}
	}

	where = l;
	unit->location = l;
}

void BrigadeBattle::setPosition(const Location& l, Boolean alone)
{
	where = l;
	unit->location = l;
	placeRegiments();
}


void UnitBattle::setFacing(const Location& l)
{
	formationFacing = menFacing = directionLocation(where, l);

	Unit* u = unit->child;
	while(u)
	{
		ASSERT(u->battleInfo != 0);

		u->battleInfo->setFacing(l);
		u = u->sister;
	}
}

void BrigadeBattle::setFacing(const Location& l)
{
	formationFacing = menFacing = directionLocation(where, l);

	placeRegiments();
}

#endif	// BATEDIT

void UnitBattle::checkIndependant()
{
	if(unit->getRank() <= Rank_Brigade)
	{
#if 0
		Boolean has = False;
		Boolean hasReal = False;

		Unit* u = unit->child;
		while(u)
		{
			UnitBattle* bu = u->battleInfo;

			if(bu)
			{
				if(!bu->bSeperated)
					has = True;
				if(!bu->bReallySeperated)
					hasReal = True;
			}

			u = u->sister;
		}


		if(!has)
		{
			UnitBattle* bu = 0;
			u = unit->child;

			while(u)
			{
				if(u->battleInfo)
				{
					bu = u->battleInfo;
					break;
				}
				u = u->sister;
			}

			if(bu)
				u->makeBattleDetached(False);

#if defined(DEBUG_DEPENDANTS) && !defined(BATEDIT)
			bLog.printf("%s had no dependants", unit->getName(True));
#endif
		}

		if(!hasReal)
		{
			UnitBattle* bu = 0;
			u = unit->child;

			while(u)
			{
				if(u->battleInfo)
				{
					bu = u->battleInfo;
					break;
				}
				u = u->sister;
			}

			if(bu)
				u->makeBattleReallyDetached(False);
#if defined(DEBUG_DEPENDANTS) && !defined(BATEDIT)
			bLog.printf("%s had no Real dependants", unit->getName(True));
#endif
		}
#else
		Boolean has = False;

		Unit* u = unit->child;
		while(u)
		{
			UnitBattle* bu = u->battleInfo;

			if(bu)
			{
				if(bu->unit->getBattleAttachMode() != Detached)
					has = True;
			}

			u = u->sister;
		}

		if(!has)
		{
			UnitBattle* bu = 0;
			u = unit->child;

			while(u)
			{
				if(u->battleInfo)
				{
					bu = u->battleInfo;
					break;
				}
				u = u->sister;
			}

			if(bu)
				bu->unit->rejoinBattle();

#if defined(DEBUG_DEPENDANTS) && !defined(BATEDIT)
			bLog.printf("%s had no Real dependants", unit->getName(True));
#endif
		}
#endif
	}
}

#if !defined(BATEDIT)
Boolean CavInfantryBattle::canLoadFire()
{
	Regiment* r = getRegiment();

	if(r->type.basicType == Cavalry)
		return !isMounted() && (limberCount == 0);
	else
		return True;
}

Boolean ArtilleryBattle::canLoadFire()
{
	return !isLimbered() && (limberCount == 0); 
}

void CavInfantryBattle::adjustLimberSpeed(Speed& speed)
{
	Regiment* r = getRegiment();

	if(r->type.basicType == Cavalry)
	{
		if(limberCount != 0)
			speed = 0;
		else if(!isMounted())
			speed = YardsPerHour(1760);
	}

}

void ArtilleryBattle::adjustLimberSpeed(Speed& speed)
{
	if(limberCount != 0)
		speed = 0;
	else if(!isLimbered())
		speed = YardsPerHour(400);		// Very slow!
}


#endif
