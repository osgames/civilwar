/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Ascii to Binary Convertor
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <iostream.h>
#include <stdarg.h>
#include "wldfile.h"
#include "system.h"
#include "campwld.h"
#include "datafile.h"
#include "files.h"

#include "game.h"
#include "campaign.h"
#include "mapwind.h"
// #include "msgwind.h"
#include "ai_camp.h"


Campaign::Campaign()
{
	memset(this, 0, sizeof(*this));

	world = new CampaignWorld;
	gameTime.setCampaignTime();
	gameTime.set(dateToDays(12, April, 1861), SunSet);
	timeInfo = gameTime;
}

Campaign::~Campaign()
{
	delete world;
}

void main()
{
	try
	{
		machine.init();

		campaign = new Campaign;
  	
		readWorld("gamedata\\dwg_wld.dat", campaign->world);

		// writeStaticWorld("gamedata\\world_s.rrf", campaign->world);
		// writeWorldOB("gamedata\\troops.rrf", campaign->world);
		writeStartCampaign(startWorldName, campaign);

		delete campaign;
	}
 	catch(GeneralError e)
 	{
		cout << "General Error: ";
		if(e.get())
			cout << e.get();
		else
			cout << "No description";
		cout << endl;

 	}
 	catch(...)
 	{
 		cout << "Caught some kind of error" << endl;
 	}

}

/*
 * Some functions to keep the linker Happy
 */

void Unit::logInfo(Boolean isLast)
{
}

void Regiment::logInfo(Boolean isLast)
{
}

void Brigade::logInfo(Boolean isLast)
{
}


void Division::logInfo(Boolean isLast)
{
}


void Corps::logInfo(Boolean isLast)
{
}

void Army::logInfo(Boolean isLast)
{
}


void President::logInfo(Boolean isLast)
{
}


void Unit::draw3D(System3D& drawData) { }
UnitBattle* Unit::setupBattle() { return 0; }
void Unit::clearupBattle() { }
void playBattle(OrderBattle* ob, MarkedBattle* batl)
{
}

MapPriority Army::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Army::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

MapPriority Corps::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Corps::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

MapPriority Division::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Division::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

MapPriority Brigade::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}


void Brigade::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

MapPriority Regiment::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Regiment::mapDraw(MapWindow* map, Region* bm, Point where)
{
}


MapPriority Unit::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Unit::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

void Unit::drawCampaignUnit(MapWindow* map, Region* bm, Point& where, UWORD icon)
{
}

void Unit::showCampaignInfo(Region* window)
{
}




CampaignTerrain::CampaignTerrain()
{
}

CampaignTerrain::~CampaignTerrain()
{
}

void CampaignTerrain::makeGrid(CTerrainVal* grid, const Location& l, int size)
{
}

CTerrainVal CampaignTerrain::get(const Location& l)
{
	return 0;
}

void CampaignTerrain::init(const char* fileName)
{
}

Point MapWindow::locationToPixel( Location const & l)
{
	return Point(0,0);
}

void MapWindow::drawObject( MapObject* ob )
{
}

#define Font_SimpleMessage Font_EMFL10

void GameVariables::simpleMessage(const char* fmt, ...)
{
	va_list vaList;
	va_start(vaList, fmt);
	vprintf(fmt, vaList);
	va_end(vaList);
}

GameVariables* game = 0;


Campaign* campaign = 0;

#if 0
void Facility::writeData( DataFileOutStream& s)
{
}

void City::writeData( DataFileOutStream& s)
{
}
#endif

UnitAI::~UnitAI()
{
}

UnitAI::UnitAI()
{
}


void LogFile::printf( char const *fmt, ... )
{
}

LogFile::~LogFile()
{
}

LogFile::LogFile( char const * s, Boolean _showTime)
{
}

TimeInfo *LogFile::theTime = 0;

void writeWorld( char const *s, CampaignWorld *w )
{
}

#if 0
void MessageWindow::showInfo( char const *s )
{
}
#endif

void makeNewRegiment( City *c, char unsigned v, char unsigned w)
{
}

extern "C" {
void logArmies(Unit* top)
{
}
}

void Unit::beforeBattle()
{
}

void Unit::afterBattle()
{
}

void Unit::setOccupy(Facility* f, SiegeMode mode, Boolean clearInBat)
{
}

UnitBattle* Unit::makeBattleInfo()
{
	return 0;
}

UnitBattle* Regiment::makeBattleInfo()
{
	return 0;
}

UnitBattle* Brigade::makeBattleInfo()
{
	return 0;
}

UnitBattle* President::makeBattleInfo()
{
	return 0;
}

#include "sprlist.h"

void SpriteID::remove()
{
}

Boolean Campaign::CheckAI(Unit* u)
{
	return False;
}

int Campaign::doAlert(const char* text, const char* buttons, const Location* l, int defaultButton)
{
	return defaultButton;
}

void Facility::showCampaignInfo(Region* window)
{
}


