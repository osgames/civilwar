/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Zoomed Bitmap: Access to section of a bitmap file.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdlib.h>
#include "zoom_map.h"
#include "bitmap.h"
#include "myassert.h"
#include "image.h"

/*
 * Copy section of same bitmap
 */

void copyRegion(Image* img, Point from, Point to, Point size)
{
	if(from.y > to.y)
	{	// Copy top to bottom
		UBYTE* src = img->getAddress(from);
		UBYTE* dest = img->getAddress(to);

		while(size.y--)
		{
			memcpy(dest, src, size.x);
			src += img->getWidth();
			dest += img->getWidth();
		}
	}
	else if(from.y < to.y)
	{	// Copy from bottom to top
		from.y += size.y;
		to.y += size.y;
		
		UBYTE* src = img->getAddress(from);
		UBYTE* dest = img->getAddress(to);
		while(size.y--)
		{
			src -= img->getWidth();
			dest -= img->getWidth();
			memcpy(dest, src, size.x);
		}
	}
	else if(from.x > to.x)
	{	// Copy forward
		UBYTE* src = img->getAddress(from);
		UBYTE* dest = img->getAddress(to);

		while(size.y--)
		{
			memcpy(dest, src, size.x);
			src += img->getWidth();
			dest += img->getWidth();
		}
	}
	else	// from.x < to.x
	{	// Copy backwards
		UBYTE* src = img->getAddress(from);
		UBYTE* dest = img->getAddress(to);

		while(size.y--)
		{
			memmove(dest, src, size.x);
			src += img->getWidth();
			dest += img->getWidth();
		}
	}
}

/*
 * Public Functions
 */


DiskBM::DiskBM()
{
	valid = False;
}

DiskBM::~DiskBM()
{
}

void DiskBM::init(const char* fileName)
{
	fp.open(fileName);
	if(fp.isValid())
	{
		BM_Header header;

		fp.read(&header, sizeof(header));

		width = header.getWidth();
		height = header.getHeight();
	}

	valid = False;
}

void DiskBM::updateBM(Image* img, const Point& newP)
{
	ASSERT(fp.isValid());

	if(!valid || (newP != oldPosition))
	{
		/*
		 * Do areas intersect?
		 */


		int dx = newP.x - oldPosition.x;
		int dy = newP.y - oldPosition.y;

		int ax = abs(dx);
		int ay = abs(dy);

		if(valid && (ax < img->getWidth()) && (ay < img->getHeight()))
		{
			SDimension x1 = img->getWidth() - ax;
			SDimension y1 = img->getHeight() - ay;


			/*
			 * Split into 4 cases
			 */

			if(dx < 0)
			{
				if(dy < 0)
				{
					/*
					 * Copy from top left to bottom right
					 */

					copyRegion(img, Point(0,0), Point(ax, ay), Point(x1, y1));

					/*
					 * Load Top
					 */

					if(ay)
						readArea(img, Point(0,0), newP, Point(img->getWidth(), ay));

					/*
					 * Load Left
					 */

					if(ax)
						readArea(img, Point(0, ay), Point(newP.x, newP.y + ay), Point(ax, y1));
				}
				else
				{
					copyRegion(img, Point(0, ay), Point(ax, 0), Point(x1, y1));

					if(ay)
						readArea(img, Point(0, y1), Point(newP.x, newP.y + y1), Point(img->getWidth(), ay));
					if(ax)
						readArea(img, Point(0,0), newP, Point(ax, y1));
				}
			}
			else
			{
				if(dy < 0)
				{
					copyRegion(img, Point(ax, 0), Point(0, ay), Point(x1, y1));
					if(ay)
						readArea(img, Point(0, 0), newP, Point(img->getWidth(), ay));
					if(ax)
						readArea(img, Point(x1,ay), Point(newP.x + x1, newP.y + ay), Point(ax, y1));
				}
				else
				{
					copyRegion(img, Point(ax, ay), Point(0,0), Point(x1, y1));
					if(ay)
						readArea(img, Point(0, y1), Point(newP.x, newP.y + y1), Point(img->getWidth(), ay));
					if(ax)
						readArea(img, Point(x1, 0), Point(newP.x + x1, newP.y), Point(ax, y1));
				}
			}
		}
		else
			readArea(img, Point(0,0), newP, img->getSize());

		oldPosition = newP;
	}
}

void DiskBM::reset()
{
	valid = False;
}


/*
 * Read an Area from disk into memory
 */

void DiskBM::readArea(Image* img, Point to, Point from, Point sz)
{
	/*
	 * Clip area
	 */

	if(from.x < 0)
	{
		sz.x += from.x;
		from.x = 0;
	}

	if(from.y < 0)
	{
		sz.y += from.y;
		from.y = 0;
	}

	if( (from.x + sz.x) > width)
		sz.x = width - from.x;

	if( (from.y + sz.y) > height)
		sz.y = height - from.y;

	if( (sz.x < 0) || (sz.y < 0) )
		return;

	fp.seekSet(from.x + from.y * width + sizeof(BM_Header));

	UBYTE* dest = img->getAddress(to);

	while(sz.y--)
	{
		fp.read(dest, sz.x);
		if(sz.y)
		{
			dest += img->getWidth();
			fp.seekRel(width - sz.x);
		}
	}
}

