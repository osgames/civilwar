/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Memory Heap Manager
 *
 * Designed to handle fragmentation problems in 4 Meg machine.
 *
 * A large amount will be allocated at the start of the program
 * which will be used for the battle zbuffer and some other battle 
 * specific data.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "membodge.h"
#include "Myassert.h"

Heap* memHeap = 0;

struct UsedHeapItem {
	size_t length;
};

Heap::Heap(size_t howMuch)
{
	howMuch += sizeof(UsedHeapItem);

	buffer = new UBYTE[howMuch];
	length = howMuch;
	nextFree = buffer;
	amountLeft = howMuch;
}

Heap::~Heap()
{
	delete[] buffer;
}

void* Heap::allocate(size_t n)
{
	n += sizeof(UsedHeapItem);

	ASSERT(n <= amountLeft);

	if(n <= amountLeft)
	{
		void* ptr = nextFree;
		UsedHeapItem* h = (UsedHeapItem*)ptr;
		h->length = n;

		nextFree += n;
		amountLeft -= n;

		ptr = (void*) (h + 1);

		return ptr;
	}

	return 0;
}

void Heap::free(void* ad)
{
	UsedHeapItem* h = (UsedHeapItem*) ad;
	h--;

	UBYTE* ad1 = (UBYTE*) h;

	ASSERT((h->length + ad1) == nextFree);

	amountLeft += h->length;
	nextFree = ad1;
}

void Heap::reset()
{
	nextFree = buffer;
	amountLeft = length;
}

