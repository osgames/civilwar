/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Rename a file into a .BAK
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/05/04  22:09:38  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <fstream.h>
#include <stdio.h>
#include "makename.h"

void backup(const char* fname)
{
	char name[FILENAME_MAX];
	
	makeFilename(name, fname, ".bak", True);

	if(remove(name) == 0)
		cout << "Deleted " << name << endl;

	if(rename(fname, name) == 0)
		cout << "Renamed " << fname << " to " << name << endl;
}


