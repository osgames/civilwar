/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Work out global palette for a load of 256 colour graphic files
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.4  1994/06/06  13:17:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.2  1994/05/19  17:44:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/04/05  12:28:09  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <iostream.h>
#include <iomanip.h>
#include <stdlib.h>
#include <stdio.h>
#include <dos.h>
#include <wclist.h>
#include <wclistit.h>


#include "ilbm.h"
#include "image.h"
#include "palette.h"


/*
 * Palette storage
 */

class RGBColour : public WCDLink {
public:
	UBYTE red;
	UBYTE green;
	UBYTE blue;

	int usedCount;
	int fileCount;

	RGBColour(UBYTE red, UBYTE green, UBYTE blue, int count = 1);

	void rgbToHSV(int& h, int& s, int& v);
};

RGBColour::RGBColour(UBYTE r, UBYTE g, UBYTE b, int c)
{
	red = r;
	green = g;
	blue = b;
	usedCount = c;
	fileCount = 1;
}

/*
 * Tell me the absolute difference between two colour values
 */

inline cdiff(UBYTE c1, UBYTE c2)
{
	if(c1 > c2)
		return c1 - c2;
	else
		return c2 - c1;
}

int cdiff(RGBColour* c1, RGBColour* c2)
{
	int r = cdiff(c1->red, c2->red);
	int g = cdiff(c1->green, c2->green);
	int b = cdiff(c1->blue, c2->blue);

	return r * r + g * g + b * b;
}

void RGBColour::rgbToHSV(int& h, int& s, int& v)
{
	UBYTE max;
	UBYTE min;

	max = green;
	if(red > max)
		max = red;
	if(blue > max)
		max = blue;

	min = green;
	if(red <	min)
		min = red;
	if(blue < min)
		min = blue;

	v = max;				// Set V

	/*
	 * Calculate Saturation
	 */

	if(max)
		s = ((max - min) * 256) / max;
	else
		s = 0;

	/*
	 * Hue
	 */

	if(s)
	{
		int delta = max - min;

		if(red == max)
			h = ((green - blue) * 60) / delta;
		else if(green == max)
			h = ((blue - red) * 60) / delta + 120;
		else	// assume blue
			h = ((red - green) * 60) / delta + 240;

		if(h < 0)
			h += 360;
		if(h >= 360)
			h -= 360;

	}
	else
		h = 0;			// Should be undefined!


}

class ColourList : public WCIsvDList<RGBColour> {
public:
	int add(UBYTE red, UBYTE green, UBYTE blue, int count);
	void show();
	void writeILBM(char* fname);
	void sortPopularity();
	void removeUnPopular();
	void sortColour();
};

typedef WCIsvDListIter<RGBColour> ColourIter;


/*
 * Add colour to list
 *
 * returns 0 if already in list
 */

int ColourList::add(UBYTE red, UBYTE green, UBYTE blue, int count)
{
	/*
	 * Scan list for colour match
	 */


	if(entries())
	{
		ColourIter list = *this;

		while(++list)
		{
			RGBColour* item = list.current();

			if((item->red == red) && (item->green == green) && (item->blue == blue))
			{
				item->usedCount += count;
				item->fileCount++;
				return 0;
			}
		}
	}

	/*
	 * Colour is not in list, so add it
	 */

	append(new RGBColour(red, green, blue, count));

	return 1;
}

void ColourList::show()
{
	cout << endl << "Summary" << endl;
	cout << entries() << " different colours" << endl;

	if(entries())
	{
		ColourIter list = *this;
		while(++list)
		{
			RGBColour* item = list.current();

			cout << hex << setfill('0') <<
				setw(2) << int(item->red) << "/" <<
				setw(2) << int(item->green) << "/" <<
				setw(2) << int(item->blue) <<
				dec << setfill(' ') <<
				" used " << setw(8) << item->usedCount << " times in " <<
				setw(3) << item->fileCount << " files" << endl;
		}
	}
}



void ColourList::writeILBM(char* fname)
{
	/*
	 * Create Palette
	 */

	Palette palette;
	if(entries())
	{
		ColourIter list = *this;
#if 0
		UBYTE* ad = palette.address();
#endif
		int count = 0;
		while(++list && (count < 256))
		{
			RGBColour* item = list.current();

#if 0
			ad[0] = item->red;
			ad[1] = item->green;
			ad[2] = item->blue;

			ad += 3;
#else
			palette.setColour(count, item->red, item->green, item->blue);
#endif

			count++;
		}

		while(count < 256)
		{
#if 0
			ad[0] = 0xff;
			ad[1] = 0;
			ad[2] = 0xff;

			ad += 3;
#else
			palette.setColour(count, 0xff, 0, 0xff);
#endif
			count++;
		}

	}

	Image body(640,480);

	::writeILBM(fname, body, palette);
}

/*
 * Sort the colour list based on popularity (least used first!)
 */

void ColourList::sortPopularity()
{
	if(entries())
	{
		ColourList newList;

		while(entries())
		{
			RGBColour* item = get();
		
			if(newList.entries())
			{
				ColourIter list = newList;

				Boolean added = False;

				while(++list)
				{
					RGBColour* col = list.current();

					if(col->usedCount >= item->usedCount)
					{
						list.insert(item);
						added = True;
						break;
					}
				}

				if(!added)
					newList.append(item);
			}
			else
				newList.append(item);
		}

		/*
		 * Now copy newList onto ourselves!
		 */

		while(newList.entries())
			append(newList.get());
	}
}

/*
 * Sort the colour list based on Colours
 */

void ColourList::sortColour()
{
	if(entries())
	{
		ColourList newList;

		while(entries())
		{
			RGBColour* item = get();
		
			if(newList.entries())
			{
				ColourIter list = newList;

				Boolean added = False;

				while(++list)
				{
					RGBColour* col = list.current();
					int h1, s1, v1;
					int h2, s2, v2;

					item->rgbToHSV(h1, s1, v1);
					col->rgbToHSV(h2, s2, v2);

					if(h1 < h2)
						continue;
					if(h1 == h2)
					{
						if(v1 < v2)
							continue;
						if(v1 == v2)
						{
							if(s1 < s2)
								continue;
						}
					}

					list.insert(item);
				  	added = True;
				  	break;
				}

				if(!added)
					newList.append(item);
			}
			else
				newList.append(item);
		}

		/*
		 * Now copy newList onto ourselves!
		 */

		while(newList.entries())
			append(newList.get());
	}
}



/*
 * Remove unpopular colours
 *
 * For each colour:
 *   Find closest colour
 *   	 if colour difference is very small then remove the least popular one.
 */


void ColourList::removeUnPopular()
{
	if(entries())
	{
		ColourList newList;
		int lostCount = 0;

		while(entries())
		{
			RGBColour* item = get();

			int bestDiff = 0;
			RGBColour* bestItem = 0;

			ColourIter list1 = *this;

			while(++list1)
			{
				RGBColour* item1 = list1.current();

				if(item != item1)
				{
					int diff = cdiff(item, item1);

					if(!bestItem || (diff < bestDiff))
					{
						bestDiff = diff;
						bestItem = item1;
					}
				}
			}

#ifdef DEBUG
			if(bestItem)
			{
				cout << "Best Match for " <<
					hex << setfill('0') <<
					setw(2) << int(item->red) << "/" <<
					setw(2) << int(item->green) << "/" <<
					setw(2) << int(item->blue) <<
					" is " <<
					setw(2) << int(bestItem->red) << "/" <<
					setw(2) << int(bestItem->green) << "/" <<
					setw(2) << int(bestItem->blue) <<
					dec << setfill(' ') <<
					" with a difference of " << bestDiff << endl;
			}
#endif
			if(bestItem && (bestDiff < 144))
			{
#ifdef DEBUG
				cout << "Removing it!" << endl;
#endif
				delete item;
				lostCount++;
			}
			else
				newList.append(item);

		}

		cout << "Removed a total of " << lostCount << " colours" << endl;

		/*
		 * Now copy newList back onto ourselves!
		 */

		while(newList.entries())
			append(newList.get());

	}
}

/*
 * Process a file
 */


void processPicture(const char* fileName, ColourList& list)
{
	Image image;
	Palette palette;

	if(readILBM(fileName, &image, &palette) == 0)
	{
#ifdef DEBUG
		cout << image.getWidth() << " by " << image.getHeight() << endl;
#endif

		/*
		 * Go through a pixel at a time and buid histogram
		 */

		int used[256];		// Table of flags to indicate palette usage

		for(int i = 0; i < 256; i++)
			used[i++] = 0;

		UBYTE* ad = image.getAddress();		// Addres of 1st pixel
		int pixelCount = image.getWidth() * image.getHeight();
		while(pixelCount--)
			used[*ad++]++;

		int colourCount = 0;
		int uniqueCount = 0;
#if 0
		UBYTE* palPtr = palette.address();
#endif
		for(i = 0; i < 256; i++)
		{
			if(used[i])
			{
				colourCount++;

#if 0
				if(list.add(palPtr[0], palPtr[1], palPtr[2], used[i]))
					uniqueCount++;
#else
				UBYTE r, g, b;
				palette.getColour(i, r, g, b);

				if(list.add(r, g, b, used[i]))
					uniqueCount++;
#endif
			}

#if 0
			palPtr += 3;
#endif
		}

		cout << colourCount << " Colours, of which " << uniqueCount << " are new" << endl;
	}

	/*
	 * Image gets freed automatically during destructor
	 */

}

/*
 * Usage:
 *		palcount [filename]...
 *
 * filename may include DOS wildcards
 */


void main(int argc, char* argv[])
{
	/*
	 * Initialise palette table
	 */

	ColourList list;

	/*
	 * For each filename, analyse file
	 */


	int i = 0;

	while(++i < argc)
	{
		const char* fileName = argv[i];

#ifdef DEBUG
		cout << "Parameter: " << fileName << endl;
#endif

		char fullpath	[_MAX_PATH];
		char drive		[_MAX_DRIVE];
		char dir			[_MAX_DIR];
		char fname		[_MAX_FNAME];
		char ext			[_MAX_EXT];

		_fullpath(fullpath, fileName, _MAX_PATH);
		_splitpath(fullpath, drive, dir, fname, ext);

		struct find_t fileInfo;

		if(!_dos_findfirst(fileName, _A_NORMAL, &fileInfo))
		{
			do
			{
				sprintf(fullpath, "%s%s%s", drive, dir, fileInfo.name);
#ifdef DEBUG
				cout << "Reading: " << fullpath << ", " << fileInfo.size << " bytes" << endl;
#endif
				processPicture(fullpath, list);

			} while(!_dos_findnext(&fileInfo));
		}
	}

	/*
	 * Analyse results
	 */

	list.sortPopularity();
	list.removeUnPopular();
	list.sortColour();
	list.show();

	list.writeILBM("palette.lbm");
}
