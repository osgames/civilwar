/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id  C.J.Wall 21/12/94 $
 *----------------------------------------------------------------------
 *
 *		Functions associated with calculating a battle result
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "calcbat.h"
#include "combval.h"
#include "campbatl.h"
#include "generals.h"
#include "game.h"
#include "memptr.h"
#include "language.h"
#include "dialogue.h"
#include "campcomb.h"

#ifndef TESTBATTLE
#include "city.h"
#include "campaign.h"
#include "campwld.h"
#include "camptab.h"
#endif

#ifndef TESTCAMP
#include "batldata.h"
#include "unit3d.h"
#include "map3d.h"
#endif


#ifdef DEBUG
#include "tables.h"
#include "clog.h"

LogFile cbLog("calcbat.log");


#endif

enum Ratios {
	rSmall,
	r1to3,
 	r1to2,
	r1to1,
	r2to1,
	r3to1,
	r4to1,
	r5to1,
	r6to1,
	r7to1,
	r8to1,
	rBig,
};

#ifdef DEBUG
const char* ratioStr[] = {
	"small",
	"r1to3",
 	"r1to2",
	"r1to1",
	"r2to1",
	"r3to1",
	"r4to1",
	"r5to1",
	"r6to1",
	"r7to1",
	"r8to1",
	"big"
};
#endif

/*
 * Structure containing information about combat values
 */

struct CombatInfo {
	Facility* f;			// Facility involved (or NULL)
	MarkedBattle* batl;	// Marked Battle involved in battle
	Side initiative;		// Which side is attacking?
	CombatValue CSAvalue;		// CSA's Combat Value
	CombatValue USAvalue;		// USA's Combat Value
	Ratios ratio;			// Enumerated Ratio of initiative/defender

	CombatInfo(MarkedBattle* b, Facility* fac);
	void determineInitiative();
	void calcCombatValue();
	void causeCasualties();
	Side checkFacilityTakeover();

private:
	LONG calcIni(Side side);
	CombatValue calcCV(Side side);
	// long calcUnitCV(Unit* u);
};

CombatInfo::CombatInfo(MarkedBattle* b, Facility* fac)
{
	f = fac;
	batl = b;
	initiative = SIDE_None;
	CSAvalue = 0;
	USAvalue = 0;
	ratio = r1to1;
}


/*==================================================================
 * Support Functions
 */


/*
 * Get percentage adjustment to make for ability
 */

BYTE varwithAbi(int number)
{
	if ( number > 180 )
		return 20;

	if ( number > 90 )
		return 0;

	return -20;
}

BYTE varwithExp(Unit* brigade)
{
	Regiment* reg = ( Regiment* )brigade->child;

	int count = 0;

	int number = 0;

	while( reg )
	{
		count++;

		number += reg->experience;

		reg = ( Regiment* )reg->sister;
	}

	if ( count == 0 ) 
	{
#ifdef DEBUG
		cbLog.printf("Error in varExp");
#endif
		return 0;
	}
		
	number /= count;

	if ( number > 200 )
		return -10;

	if ( number > 125 )
		return 20;

	if ( number > 70 )
		return 10;

	if ( number > 10 )
		return 0;

	return
		-20;
}

BYTE varwithFat(Unit* brigade)
{
	Regiment* reg = ( Regiment* )brigade->child;

	int count = 0;

	int number = 0;

	while( reg )
	{
		count++;

		number += reg->fatigue;

		reg = ( Regiment* )reg->sister;
	}

	if ( count == 0 ) 
	{
	#ifdef DEBUG
		cbLog.printf("Error in varFat");
	#endif
		return 0;
	}
		
	number /= count;

#if 0		// Wrong way round!  0 = Fresh
	if ( number > 250 )
		return 10;

	if ( number > 170 )
		return 0;

	if ( number > 90 )
		return -10;

	return -20;
#else
	if(number < 5)
		return 10;
	else if(number < 86)
		return 0;
	else if(number < 166)
		return -10;
	else
		return -20;
#endif
}

BYTE varwithMor(Unit* brigade )
{
	Regiment* reg = ( Regiment* )brigade->child;

	int count = 0;

	int number = 0;

	while( reg )
	{
		count++;

		number += reg->morale;

		reg = ( Regiment* )reg->sister;
	}

	if ( count == 0 ) 
	{
	#ifdef DEBUG
		cbLog.printf("Error in varExp");
	#endif
		return 0;
	}
		
	number /= count;

	if ( number > 250 )
		return 20;

	if ( number > 190 )
		return 10;

	if ( number > 130 )
		return 0;

	if ( number > 70 )
		return -10;

	return -20;
}

BYTE varwithSep(Unit* brigade)
{
	Regiment* reg = ( Regiment* )brigade->child;

	int count = 0;

	int number = 0;

	while( reg )
	{
		count++;

		number += reg->supply;

		reg = ( Regiment* )reg->sister;
	}

	if ( count == 0 ) 
	{
	#ifdef DEBUG
		cbLog.printf("Error in varExp");
	#endif
		return 0;
	}
		
	number /= count;

	if ( number > 250 )
		return 0;

	if ( number > 170 )
		return -10;

	if ( number > 90 )
		return -20;

	return -30;
}

#ifndef TESTCAMP

/*
 * Return Percentage adjustment for height and Terrain
 */

BYTE varWithTerHeight(UnitBattle* bu)
{
	if(onBattleField(bu->where) && !bu->routed && !bu->isDead)
	{


		Grid* gr = battle->grid->getGrid(Zoom_Min);	

		MapGridCord gx = battle->grid->cord3DtoGrid(bu->where.x);
		MapGridCord gy = battle->grid->cord3DtoGrid(bu->where.y);

		TerrainType ter = gr->getTerrain(gx, gy);
		UBYTE h = gr->getHeight(gx, gy).height;

		int value = 0;

		if(battle->data3D.terrain.isWooded(ter))
			value += 5;

		value += h/10 - 10;		// -10..+15

		return value;
	}
	else
	{
		return -100;
	}
}

#endif	// TESTCAMP


CombatValue BrigadeCV(Unit* u)
{
	static BYTE orderVariation[ORDER_Max] = {
		0, 5,  10,			// Advance, Cautious advance, attack
		0, 10, 20,			// Stand, Hold, Defend
		-5, -10, -20		// Withdraw, Retreat, Hasty Retreat
	};

	CombatValue value = calc_UnitCV(u, True);

	int percent = 0;

	percent += varwithExp(u);
	percent += varwithFat(u);
	percent += varwithMor(u);
	percent += varwithSep(u);
	percent += varwithAbi(u->general->ability);

#ifndef TESTCAMP
	if(battle && u->battleInfo)
	{
		UnitBattle* bInfo = u->battleInfo;

		percent += varWithTerHeight(bInfo);
		percent += orderVariation[bInfo->battleOrder.mode];
	}
	else
#endif
		percent += orderVariation[u->givenOrders.mode];

#ifdef DEBUG
	CombatValue startValue = value;
#endif

	if(percent)
	{
		if(percent <= -100)
			value = 0;
		else
			value += (value * percent) / 100;
	}

#ifdef DEBUG
	cbLog.printf("CV for %s = %ld + %d%% = %ld",
		u->getName(True),
		startValue,
		percent,
		value);
#endif

	return value;
}

/*
 * Calculate combat value for a side
 *
 * I'm sure this can be simplified!
 */

CombatValue CombatInfo::calcCV(Side side)
{
	CombatValue count = 0;

	if ( batl )
	{
		BattleUnit* battleunit = batl->units;

		while ( battleunit )
		{
			if ( battleunit->unit->getSide() == side )
				count += calc_UnitCV(battleunit->unit, True);
			battleunit = battleunit->next;
		}
	}
#ifdef DEBUG
	else
		cbLog.printf("No batl!!!!!!");
#endif

#ifdef DEBUG
	cbLog.printf("calcCV(%s)=%ld", sideStr[side], count);
#endif

/*
 * Add on total for fortification
 */

#ifndef TESTBATTLE
	if(f && (f->side == side))
	{
		count += calc_townCV(f, True);
	}
#endif



	return count;
}



int CalcNumCas( Side si, MarkedBattle* batl)
{
	BattleUnit* battleUnit = batl->units;

	Strength count = 0;

	while(battleUnit)
	{
		Unit* unit = battleUnit->unit;

		if(unit->getSide() == si)
		{
			UnitInfo ui(True, False);		// Use Campaign Dependancy!

			unit->getInfo(ui);

			count += ui.casualties;
		}


		battleUnit = battleUnit->next;
	}

	return count;	
}

int getNumRegiments(Unit* u)
{
	if(u->getRank() < Rank_Brigade)
	{
		int count = 0;

		u = u->child;
		while(u)
		{
			// if(!u->isCampaignReallyIndependant())
			if(u->getCampaignAttachMode() == Attached)
				count += getNumRegiments(u);
			u = u->sister;
		}

		return count;
	}
	else
		return u->childCount;
}

int getNumRegiments(Side si, MarkedBattle* batl)
{
 	BattleUnit* battleunit = batl->units;

	int count = 0;

	while ( battleunit )
	{
		if ( battleunit->unit->getSide() == si)
			count += getNumRegiments(battleunit->unit);

		battleunit = battleunit->next;
	}

	return count;
}


Regiment* getReg(Unit* u, int& count)
{
	if(u->getRank() < Rank_Brigade)
	{
		u = u->child;
		while(u)
		{
			// if(!u->isCampaignReallyIndependant())
			if(u->getCampaignAttachMode() == Attached)
			{
				Regiment* r = getReg(u, count);
				if(r)
					return r;
			}

			u = u->sister;
		}
	}
	else
	{
		u = u->child;
		while(u)
		{
			// if(!u->isCampaignReallyIndependant())
			if(u->getCampaignAttachMode() == Attached)
			{
				if(!count--)
					return (Regiment*) u;
			}

			u = u->sister;
		}
	}

	return 0;
}

Regiment* getReg(int randMax, MarkedBattle* batl, Side si)
{
 	BattleUnit* battleunit = batl->units;

	while ( battleunit )
	{
		if(battleunit->unit->getSide() == si)
		{
			Regiment* r = getReg(battleunit->unit, randMax);
			if(r)
				return r;
		}

		battleunit = battleunit->next;
	}

	return 0;
}


/*
 * Cause some casualties to a regiment
 *
 * Simplified by SWG 5th April 95
 */

Boolean KillTroops( int casualties, Regiment* target)
{								
	/*
	 * Get random number: 0..20 biassed towards 10.
	 */

	int randNum = game->gameRand.getB(10) + game->gameRand.getB(10);

	/*
	 * 10..30% are going to be really dead!
	 */

	if(casualties > target->strength)
		casualties = target->strength;

	target->strength -= casualties;

	int dead = (casualties * (10 + randNum)) / 100;
	casualties -= dead;

	target->casualties += dead;
	target->stragglers += casualties;

#ifdef DEBUG
	cbLog.printf("Causing %d casualties, %d dead to %s = %d/%d/%d",
		casualties, dead, target->getName(True),
		target->strength, target->stragglers, target->casualties);
#endif

	return False;
}




/*
 * This is still a bit complicated
 */

void applyCasualties(Side side, Strength nCasualties, MarkedBattle* batl)
{
	/*
	 * Apply casualties here....
	 */


#ifdef DEBUG
	cbLog.printf("Apply Casualties(%s, %ld)", sideStr[side], nCasualties);
#endif


	// casualties in ratio 10:4:1

	int NumArtCas = nCasualties / 15;
	int NumCavCas = (nCasualties * 4) / 15;
	int NumInfCas = nCasualties - NumArtCas - NumCavCas;	// Remainder

#ifdef DEBUG
	cbLog.printf("Art: %d, Cav: %d, Inf: %d", NumArtCas, NumCavCas, NumInfCas);
#endif


	int numRegs = getNumRegiments(side, batl);
	int averageCasualties;
	
	if(numRegs)
		averageCasualties = 1 + nCasualties / numRegs;
	else
		averageCasualties = 1;

#ifdef DEBUG
	cbLog.printf("numRegs=%d", numRegs);
#endif

	int loopCounter = 0;

	while(numRegs && (NumArtCas || NumCavCas || NumInfCas))
	{
		/*
		 * Casualties random around the average
		 */

	 	int CasPerReg = game->gameRand(averageCasualties) + game->gameRand(averageCasualties);

		int rmax = game->gameRand.get(numRegs);

		Regiment* target = getReg( rmax, batl, side);

		if (target)
		{
			if(target->strength < CasPerReg)
				CasPerReg = target->strength;

			switch ( target->type.basicType )
			{
				case Infantry:
					if(NumInfCas < CasPerReg)
						CasPerReg = NumInfCas;

					NumInfCas -= CasPerReg;

					break;

				case Cavalry:
					if(NumCavCas < CasPerReg)
						CasPerReg = NumCavCas;

					NumCavCas -= CasPerReg;

					break;

				case Artillery:
					if(NumArtCas < CasPerReg)
						CasPerReg = NumArtCas;

					NumArtCas -= CasPerReg;

					break;
			}

			if(CasPerReg)
			  	if (KillTroops(CasPerReg, target))
			  		numRegs--;
		}

		loopCounter++;

		if(loopCounter > (8 * numRegs))
			break;
	}
}


void CombatInfo::causeCasualties()
{
#if defined(DEBUG) && !defined(TESTBATTLE)
	cbLog.printf("CauseCasualties():");
#endif
	
	Strength CSATotal = CSAvalue;
	Strength USATotal = USAvalue;


#ifdef DEBUG
	cbLog.printf("CSA side total: %ld", CSATotal);
	cbLog.printf("USA side total: %ld", USATotal);
#endif

	/*
	 * Adjust by +/- 5%
	 *
	 * Changed to 20% by SWG
	 */

	int percent = game->gameRand(20) - game->gameRand(20);
	if(percent > 0)
		CSATotal += (CSATotal * percent) / 100;
	else if(percent < 0)
		CSATotal -= (CSATotal * -percent) / 100;

	percent = game->gameRand(20) - game->gameRand(20);
	if(percent > 0)
		USATotal += (USATotal * percent) / 100;
	else if(percent < 0)
		USATotal -= (USATotal * -percent) / 100;

#ifdef DEBUG
	cbLog.printf("CSA Casualties: %ld", USATotal);
	cbLog.printf("USA Casualties: %ld", CSATotal);
#endif

	applyCasualties(SIDE_USA, CSATotal, batl);
	applyCasualties(SIDE_CSA, USATotal, batl);
}


/*
 * Calculate an "Initiative" Value.
 *
 * Calculated as type of order altered with the general's attributes
 */

LONG CombatInfo::calcIni(Side side)
{
#ifdef DEBUG
	cbLog.printf("calcIni(%s)", sideStr[side]);
#endif

	static BYTE orderIniVar[] = {
		// -1, 0, 5, 2, 1, 0, -5, -1, -2, -5

		40, 50, 30,		// Advance, Cautious, attack
		 0,  0,  0,		// Stand, Hold, Defend
		20, 10,  0		// Withdraw, Retreat, Rapid Retreat
		
	};

	LONG count = 0;

	/*
	 * Find most senior commander and order
	 */

	Unit* senior = 0;

	BattleUnit* battleUnit = batl->units;

	while(battleUnit)
	{
		Unit* unit = battleUnit->unit;

		if(unit->getSide() == side)
		{
#ifdef DEBUG
			cbLog.printf("Calculating %s", unit->getName(True));
#endif
			if(!senior || (senior->rank > unit->rank))
				senior = unit;

			count += orderIniVar[unit->givenOrders.mode];

#ifdef DEBUG
			cbLog.printf("orderIniVar ==> %ld", count);
#endif
			UnitInfo ui(True, False);

			unit->getInfo(ui);

			count += ui.regimentCounts[Cavalry][Cav_Regular];
#ifdef DEBUG
			cbLog.printf("Regular Cavalry ==> %ld", count);
#endif
			count += ui.regimentCounts[Cavalry][Cav_Militia];
#ifdef DEBUG
			cbLog.printf("Militia Cavalry ==> %ld", count);
#endif
			count += ui.regimentCounts[Cavalry][Cav_RegularMounted] * 2;
#ifdef DEBUG
			cbLog.printf("Regular Mounted Cavalry ==> %ld", count);
#endif
			count += ui.regimentCounts[Cavalry][Cav_MilitiaMounted] * 2;
#ifdef DEBUG
			cbLog.printf("Militia Mounted Cavalry ==> %ld", count);
#endif
		}

		
		battleUnit = battleUnit->next;
	}

	if(senior && count)
	{
		int percent = varwithAbi(senior->general->ability);

		if ( senior->general->experience < 10 )
			percent -= 10;
		else if ( senior->general->experience < 70 )
			;
		else if ( senior->general->experience < 125 )
			percent += 10;
		else if ( senior->general->experience < 210 )
			percent += 20;
		else
			percent -= 10;

		if ( senior->general->efficiency < 10 )
			percent += -10;
		else if ( senior->general->efficiency < 70 )
			percent += 0;
		else if ( senior->general->efficiency < 125 )
			percent += 10;
		else if ( senior->general->efficiency < 210 )
			percent += 20;
		else
			percent -= 10;

		if (percent)
			count += (count * percent) / 100;

#ifdef DEBUG
		cbLog.printf("Adjusted by %d%% for senior commander %s", percent, senior->getName(True));
#endif
	}

#ifdef DEBUG
	cbLog.printf("calcIni(%s) = %ld", sideStr[side], count);
#endif

	return count;
}


/*
 * Find out who should have won...
 */

void CombatInfo::determineInitiative()
{
#if !defined(TESTBATTLE)
	if(f && (f->side != SIDE_None))
		initiative = otherSide(f->side);
	else
#endif
	{
		LONG usaI = calcIni(SIDE_USA);
		LONG csaI = calcIni(SIDE_CSA);

		if ( csaI > usaI )
			initiative = SIDE_CSA;
		else
			initiative = SIDE_USA;
	}
#ifdef DEBUG
	cbLog.printf("determineInitiative() = %s", sideStr[initiative]);
#endif
}

void CombatInfo::calcCombatValue()
{
	USAvalue = calcCV(SIDE_USA);
	CSAvalue = calcCV(SIDE_CSA);

#ifdef DEBUG
	cbLog.printf("CalcCombatValue()");
	cbLog.printf("usa=%ld", USAvalue);
	cbLog.printf("csa=%ld", CSAvalue);
#endif

	if(!USAvalue || !CSAvalue)
	{
		if(!USAvalue && !CSAvalue)
			ratio = r1to1;
		else
		if(initiative == SIDE_USA)
		{
			if(USAvalue == 0)
				ratio = rSmall;
			else
				ratio = rBig;
		}
		else
		{
			if(CSAvalue == 0)
				ratio = rSmall;
			else
				ratio = rBig;
		}
	}
	else
	{
		int rat;

		Side biggest;

		if(USAvalue > CSAvalue)
		{
			rat = USAvalue / CSAvalue;
			biggest = SIDE_USA;
		}
		else
		{
			rat = CSAvalue / USAvalue;
			biggest = SIDE_CSA;
		}

#ifdef DEBUG
		cbLog.printf("Ratio = %d, biggest = %s", rat, sideStr[biggest]);
#endif

		if(biggest == initiative)
		{
			static Ratios ratioWinTab[] = {
				rSmall,
				r1to1,
				r2to1,
				r3to1,
				r4to1,
				r5to1,
				r6to1,
				r7to1,
				r8to1
			};

			if(rat > 8)
				ratio = rBig;	// r8to1;
			else
				ratio = ratioWinTab[rat];
		}
		else
		{
			static Ratios ratioLoseTab[] = {
				rBig,
				r1to1,
				r1to2,
				r1to3,
				rSmall,
			};

			if(rat > 3)
				ratio = rSmall;	// r1to3;
			else
				ratio = ratioLoseTab[rat];
		}
	}
#ifdef DEBUG
	cbLog.printf("ratio = %s", ratioStr[ratio]);
#endif
}

#ifndef TESTBATTLE
/*
 * Alter unit's orders in response to battle result
 */

void orderRemains(MarkedBattle* batl, Side winner)
{
#ifdef DEBUG
	cbLog.printf("orderRemains(%s)", sideStr[winner]);
#endif

	BattleUnit* battleunit = batl->units;
	
	while ( battleunit )
	{
		Unit* unit = battleunit->unit;

#ifdef DEBUG
		cbLog.printf("Considering %s", unit->getName(True));
#endif
		
		/*
		 * If unit is still alive!
		 */

		if(unit->superior)
		{
			unit = unit->getTopCommand();

#ifdef DEBUG
			cbLog.printf("Top command = %s", unit->getName(True));
#endif

			if (winner == SIDE_None)
			{
				unit->setOrderNow( Order( ORDER_Stand, ORDER_Land, unit->location) );
#ifdef DEBUG
				cbLog.printf("Order changed to Stand");
#endif
			}
			else if(unit->getSide() == winner)
			{
				unit->setOrderNow( Order( ORDER_Stand, ORDER_Land, unit->location) );
#ifdef DEBUG
				cbLog.printf("Order changed to Stand");
#endif
			}
			else
			{
				withdrawUnit(unit, batl->where, cityBattleDistance * 2);
			}

#ifdef CHRIS_AI		
#ifndef TESTBATTLE
			campaign->CheckAI( unit );
#endif
#endif
		}
#ifdef DEBUG
		else
			cbLog.printf("Unit died in battle");
#endif

		battleunit = battleunit->next;
	}
}
#endif	// TESTBATTLE

Unit* BattleResult::getSenior(MarkedBattle* batl, Side si)
{

	Unit* bestUnit = 0;
	Rank BestRank = Rank_Regiment;

 	BattleUnit* battleunit = batl->units;
	while ( battleunit )
	{
		Unit* unit = battleunit->unit;

		if((unit->getSide() == si) && (unit->rank < BestRank ))
		{
			BestRank = unit->rank;
			bestUnit = unit;
		}
		
		battleunit = battleunit->next;
	}

  return bestUnit;
}


/*=============================================================
 * Create the PreBattleInfo
 */

void PreBattleInfo::make(MarkedBattle* batl)
{
	CSAstrength = 0;
	USAstrength = 0;
	CSAstragglers = 0;
	USAstragglers = 0;
	CSAcasualties = 0;
	USAcasualties = 0;

	BattleUnit* battleUnit = batl->units;
	while(battleUnit)
	{
		Unit* unit = battleUnit->unit;

		UnitInfo ui(True, False);		// Campaign Dependancy

		unit->getInfo(ui);

		if(unit->getSide() == SIDE_CSA)
		{
			CSAstrength += ui.strength;
			CSAstragglers += ui.stragglers;
			CSAcasualties += ui.casualties;
		}
		else
		{
			USAstrength += ui.strength;
			USAstragglers += ui.stragglers;
			USAcasualties += ui.casualties;
		}

		battleUnit = battleUnit->next;
	}

#ifdef DEBUG
	cbLog.printf("PreBattleInfo::make");
	cbLog.printf("CSAstragglers = %ld, CSAcasualties = %ld",
		CSAstragglers, CSAcasualties);
	cbLog.printf("USAstragglers = %ld, USAcasualties = %ld",
		USAstragglers, USAcasualties);
	cbLog.close();
#endif
}

void BattleResult::make(MarkedBattle* bat, PreBattleInfo* preInfo, Boolean doCalculation)
{
#ifdef DEBUG
	cbLog.printf("Battle Results for");
	BattleUnit* battleUnit = bat->units;
	while(battleUnit)
	{
		UnitInfo ui(True, False);
		battleUnit->unit->getInfo(ui);

		cbLog.printf("%s (%ld)", battleUnit->unit->getName(True), ui.strength);
		battleUnit = battleUnit->next;
	}

#endif

	CombatInfo cInfo(bat, 0);

	USAsenior = getSenior(bat, SIDE_USA);
	CSAsenior = getSenior(bat, SIDE_CSA);

	cInfo.determineInitiative();
	cInfo.calcCombatValue();

	initiative = cInfo.initiative;

	PreBattleInfo newPreInfo;

	if(!preInfo)
	{
		preInfo = &newPreInfo;
		preInfo->make(bat);
		doCalculation = True;
	}

	if(doCalculation)
		cInfo.causeCasualties();
#ifdef DEBUG
	else
		cbLog.printf("causeCasualties() not called because battle was played");
#endif

	PreBattleInfo info;
	info.make(bat);

	CSAstragglers = info.CSAstragglers - preInfo->CSAstragglers;
	USAstragglers = info.USAstragglers - preInfo->USAstragglers;
	CSAcasualties = info.CSAcasualties - preInfo->CSAcasualties;
	USAcasualties = info.USAcasualties - preInfo->USAcasualties;

#ifdef DEBUG
	cbLog.printf("AfterBattle Info");
	cbLog.printf("USA: Stragglers %ld - %ld = %ld, Casualties %ld - %ld = %ld, Total %ld",
		info.USAstragglers, preInfo->USAstragglers, USAstragglers,
		info.USAcasualties, preInfo->USAcasualties, USAcasualties,
		USAstragglers + USAcasualties);
	cbLog.printf("USA: Stragglers %ld - %ld = %ld, Casualties %ld - %ld = %ld, Total %ld",
		info.CSAstragglers, preInfo->CSAstragglers, CSAstragglers,
		info.CSAcasualties, preInfo->CSAcasualties, CSAcasualties,
		CSAstragglers + CSAcasualties);
#endif

	/*
	 * Find who really won... and how
	 * This used to be in the display routine!!!!
	 */

	Strength USAlosses = USAstragglers + USAcasualties;
	Strength CSAlosses = CSAstragglers + CSAcasualties;

	/*
	 * New method of working winner if it is not against a facility...
	 *	  Calculate % losses for each side and use that as indicator
	 *   e.g. USA: 10000 troops with 2000 losses = 20% losses
	 *        CSA: 1000 troops with 900 losses = 90% losses
	 *        Thus USA win here even though they took more losses.
	 */

	{
		int USApercent = (USAlosses * 0xff) / preInfo->USAstrength;
		int CSApercent = (CSAlosses * 0xff) / preInfo->CSAstrength;

#ifdef DEBUG
		cbLog.printf("USApercent = %d, CSApercent = %d", USApercent, CSApercent);
#endif

		if(abs(USApercent - CSApercent) < 5)
			winner = SIDE_None;
		else if(USApercent < CSApercent)
			winner = SIDE_USA;
		else
			winner = SIDE_CSA;
	}

	if( (cInfo.ratio == rBig) || (cInfo.ratio == rSmall) )
		how = BW_RanAway;
	else
		how = BW_Win;

#ifdef DEBUG
	static char* howStr[] = {
		"Ran Away",
		"Win"
	};

	cbLog.printf("Winner = %s, how = %s, Initiative = %s",
		sideStr[winner], howStr[how], sideStr[initiative]);
#endif

	/*
	 * Respond to result
	 */

#ifndef TESTBATTLE
	if(campaign)
	{
		orderRemains(bat, winner);

		const int MenPerPoint = 50;		// Was 100


		LONG vicChange = ( (LONG) USAcasualties - (LONG) CSAcasualties) / MenPerPoint;


		campaign->CSAVictory += vicChange;
		campaign->USAVictory -= vicChange;

#ifdef DEBUG
		cbLog.printf("Victory point change: %ld => USA:%ld, CSA:%ld",
			vicChange,
			campaign->USAVictory,
			campaign->CSAVictory);
#endif
	}
#endif

#ifdef DEBUG
	cbLog.close();
#endif
}


void BattleResult::getTitle(char* buffer)
{
	if(initiative == SIDE_CSA)
	{
		sprintf(buffer, language(lge_cbats), CSAsenior->getGeneralName());
		sprintf(buffer + strlen(buffer), "\r%s\r", language(lge_Versus));
		sprintf(buffer + strlen(buffer), language(lge_ubats), USAsenior->getGeneralName());
	}
	else
	{
		sprintf(buffer, language(lge_ubats), USAsenior->getGeneralName());
		sprintf(buffer + strlen(buffer), "\r%s\r", language(lge_Versus));
		sprintf(buffer + strlen(buffer), language(lge_cbats), CSAsenior->getGeneralName());
	}

#ifdef DEBUG
	cbLog.printf("%s", buffer);
	cbLog.close();
#endif
}

void BattleResult::getBody(char* buffer)
{
	if(initiative == SIDE_CSA)
	{
		if(winner == SIDE_None)
			strcpy(buffer, language(lge_cbat_draw));
		else if(how == BW_RanAway)
		{
			if(winner == SIDE_USA)
				strcpy(buffer, language(lge_cbat_run));
			else
				strcpy(buffer, language(lge_cbat_win));
		}
		else if(winner == SIDE_USA)
			strcpy(buffer, language(lge_cbat_loose));
		else	// must be CSA
			strcpy(buffer, language(lge_cbat_conwin));
	}
	else	// Must be USA
	{
		if(winner == SIDE_None)
			strcpy(buffer, language(lge_ubat_draw));
		else if(how == BW_RanAway)
		{
			if(winner == SIDE_CSA)
				strcpy(buffer, language(lge_ubat_run));
			else
				strcpy(buffer, language(lge_ubat_win));
		}
		else if(winner == SIDE_CSA)
			strcpy(buffer, language(lge_ubat_loose));
		else	// must be USA
			strcpy(buffer, language(lge_ubat_conwin));
	}

	if( (winner != SIDE_None) && (how == BW_RanAway) &&
		 ( (winner == SIDE_CSA) && ( (USAstragglers + USAcasualties) == 0))
	 || ( (winner == SIDE_USA) && ( (CSAstragglers + CSAcasualties) == 0)) )
	{
		sprintf(buffer + strlen(buffer), " %s", language(lge_firing));
	}

#ifdef DEBUG
	cbLog.printf("%s", buffer);
	cbLog.close();
#endif
}

void BattleResult::getStats(char* buffer)
{
	Strength USAlosses = USAstragglers + USAcasualties;
	Strength CSAlosses = CSAstragglers + CSAcasualties;

	if(!USAlosses && !CSAlosses)
		strcpy(buffer, language(lge_nocas));
	else
	if(game->getLocalSide() == SIDE_USA)
	{
		if(USAlosses > 1)
			sprintf(buffer, language(lge_YouArmyCas), USAlosses);
		else if(USAlosses == 1)
			strcpy(buffer, language(lge_YouArmyOne));
		else
			strcpy(buffer, language(lge_YouNoArmyCas));

		strcat(buffer, "\r");

		if(CSAlosses > 1)
			sprintf(buffer + strlen(buffer), language(lge_cArmyCas), CSAlosses);
		else if(CSAlosses == 1)
			strcat(buffer, language(lge_cArmyOne));
		else
			strcat(buffer, language(lge_cNoArmyCas));
	}
	else	// Must be CSA
	{
		if(CSAlosses > 1)
			sprintf(buffer, language(lge_YouArmyCas), CSAlosses);
		else if(CSAlosses == 1)
			strcpy(buffer, language(lge_YouArmyOne));
		else
			strcpy(buffer, language(lge_YouNoArmyCas));

		strcat(buffer, "\r");

		if(USAlosses > 1)
			sprintf(buffer + strlen(buffer), language(lge_uArmyCas), USAlosses);
		else if(USAlosses == 1)
			strcat(buffer, language(lge_uArmyOne));
		else
			strcat(buffer, language(lge_uNoArmyCas));
	}
#ifdef DEBUG
	cbLog.printf("%s", buffer);
	cbLog.close();
#endif
}

void BattleResult::getButton(char* buffer)
{
	if(winner == SIDE_None)
		strcpy(buffer, language(lge_OK));
	// else if(game->playersSide == winner)
	else if(game->getLocalSide() == winner)
		strcpy(buffer, language(lge_hip));
	else
		strcpy(buffer, language(lge_drat));
#ifdef DEBUG
	cbLog.printf("%s", buffer);
	cbLog.close();
#endif
}

/*====================================================================
 * These Routines used by calcFortBattle
 */

#ifndef TESTBATTLE


Side CombatInfo::checkFacilityTakeover()
{
#ifdef DEBUG
	{
		BattleUnit* battleUnit = batl->units;

		cbLog.printf("checkFacilityTakeOver(ratio=%s, unit=%s, facility=%s)",
			ratioStr[ratio],
			battleUnit ? battleUnit->unit->getName(True) : "Null",
			f ? f->getNameNotNull() : "Null");
	}
#endif

	Side fside = f->side;


	/*
	 * This table is actually based on the ratio being between
	 *    attacker/defender
	 */

	enum SeigeCC { R, I, ID, C };

	SeigeCC SeigeCombatChart[][9] = {
		{  R,  R,  R,  R,  R,  R,  R,  R,  R },			// small
		{  R,  R,  R,  R,  I,  I,  I,  I, ID },			// 1to3
		{  R,  R,  R,  I,  I, ID, ID,  C,  C },			// 1to2
		{  R,  R,  I,  I, ID, ID,  C,  C,  C },			// 1to1
		{  R,  R,  I, ID, ID,  C,  C,  C,  C },			// 2to1
		{  R,  I, ID, ID,  C,  C,  C,  C,  C },			// 3to1
		{  R,  I, ID,  C,  C,  C,  C,  C,  C },			// 4to1
		{  R,  I,  C,  C,  C,  C,  C,  C,  C },			// 5to1
		{  R, ID,  C,  C,  C,  C,  C,  C,  C },			// 6to1
		{  I, ID,  C,  C,  C,  C,  C,  C,  C },			// 7to1
		{ ID,  C,  C,  C,  C,  C,  C,  C,  C },			// 8to1
		{  C,  C,  C,  C,  C,  C,  C,  C,  C },			// big
	};

	UnitInfo ui(True, False);

	char flag = 0;

	SeigeCC result = SeigeCombatChart[ratio][game->gameRand.getB(9)];

#ifdef DEBUG
	static char* SeigeCCStr[] = {
		"Occcupier Stays",
		"Inconclusive",
		"Lose fortification",
		"Attacker wins"
	};

	cbLog.printf("result=%s", SeigeCCStr[result]);
#endif

	switch( result )
	{
	 	case R:
			return fside; 
			break;

		case I:
			return SIDE_None;
			break;

		case ID:
			{
				BattleUnit* battleUnit = batl->units;

				while ( battleUnit )
				{
					if ( battleUnit->unit->getSide() != fside )
					{
						battleUnit->unit->getInfo(ui);

						if ( ui.strengths[ Artillery ][ Art_Siege ] )
							flag = 1;
					}
	
					battleUnit = battleUnit->next;
				}
			}

			if (f->fortification && flag)
				f->fortification--;

			if (!f->fortification)
				return otherSide(fside);

			return SIDE_None;

			break;

		case C:
			return otherSide( fside );
			break;
	}

	return SIDE_None;
}

/*===========================================================
 * Battles against Fortifications
 *
 * Returns:
 *		False: Battle Won
 *		True : Battle Lost
 */

Boolean calcFortBattle( MarkedBattle* batl, Facility* f )
{ 
	if (!batl || !batl->units)
		return True;

	CombatInfo info(batl, f);

#ifdef DEBUG
	cbLog.printf( "Fort Battle at %s (side=%s)", f->getNameNotNull(), sideStr[f->side] );

	BattleUnit* battleunit = batl->units;

	while ( battleunit )
	{
		if ( battleunit->unit->getSide() == f->side )
			cbLog.printf( " and %s", battleunit->unit->getGeneralName());
		battleunit = battleunit->next;
	}

	cbLog.printf( "versus " );

	battleunit = batl->units;
	
	while ( battleunit )
	{
		if ( battleunit->unit->getSide() != f->side )
		{
			cbLog.printf(" %s", battleunit->unit->getGeneralName());
		}
		battleunit = battleunit->next;
	}

#endif

	info.determineInitiative();
	info.calcCombatValue();

	int CSAcasBefore = CalcNumCas( SIDE_CSA, batl);
	int USAcasBefore = CalcNumCas( SIDE_USA, batl);
	
	info.causeCasualties();

	int CSAcasAfter = CalcNumCas( SIDE_CSA, batl);
	int USAcasAfter = CalcNumCas( SIDE_USA, batl);

	Strength CSAlosses = CSAcasAfter - CSAcasBefore;
	Strength USAlosses = USAcasAfter - USAcasBefore;

	/*
	 * Calculate and dislay who the real winner of a
	 * fight against fortifications is.
	 *
	 * Returns winning side.
	 */

	MemPtr<char> buffer(1000);

	Side fside = f->side;
	// Side si = game->playersSide;
	Side si = game->getLocalSide();
	Side result = info.checkFacilityTakeover();

	orderRemains(batl, result);

	Unit* unit = 0;

	BattleUnit* bu = batl->units;
	while(bu)
	{
		Unit* u = bu->unit;

		if( (u->getSide() != fside) && (!unit || u->getRank() < unit->getRank()))
			unit = u;
		bu = bu->next;
	}

#ifdef DEBUG
		cbLog.printf("DisplayFortResult()");
		cbLog.printf( "CSA casualties: %d", CSAlosses);
		cbLog.printf( "USA casualties: %d", USAlosses);
		cbLog.printf( "Ratio = %s", ratioStr[info.ratio] );
		cbLog.printf( "Result= %s\n",	sideStr[result] );
		if(unit)
			cbLog.printf("batunit: %s", unit->getName(True));
		if(f)
			cbLog.printf("Facility: %s", f->getNameNotNull());
#endif

	sprintf(buffer, "%s\r%s\r%s, %s\r\r",
		unit->getGeneralName(),
		language(lge_Versus),
		f->getNameNotNull(),
		campaign->world->states[f->state].fullName);

	if ( fside == SIDE_CSA )
	{
		if ( result == SIDE_None )
			strcat(buffer, language( lge_cinde ) );
		else if ( result != fside ) 
			strcat(buffer, language( lge_cbrave ) );
		else 
			strcat(buffer, language( lge_chordes ) );		
	}
	else
	{
		if ( result == SIDE_None )
			strcat(buffer, language( lge_uinde ) );
		else if ( result != fside ) 
			strcat(buffer, language( lge_ubrave ) );
		else  
			strcat(buffer, language( lge_uhordes ) );
	}

	strcat(buffer, "\r\r");
	
	if (USAlosses)
	{
		if ( si == SIDE_USA )
		{
			if (USAlosses > 1 )
				sprintf(buffer + strlen(buffer), language( lge_YouArmyCas ), USAlosses);
			else if (USAlosses == 1 )
				strcat(buffer, language( lge_YouArmyOne ) );
			else
				strcat(buffer, language( lge_YouNoArmyCas ) );
		}
		else
		{
			if (USAlosses > 1 )
				sprintf(buffer + strlen(buffer), language( lge_uArmyCas ), USAlosses);
			else if (USAlosses == 1 )
				strcat(buffer, language( lge_uArmyOne ) );
			else
				strcat(buffer, language( lge_uNoArmyCas ) );
		}
	}

	if (CSAlosses)
	{
		if(USAlosses)
			strcat(buffer, "\r");

		if ( si == SIDE_CSA )
		{
			if (CSAlosses > 1 )
				sprintf(buffer + strlen(buffer), language( lge_YouArmyCas ), CSAlosses);
			else if (CSAlosses == 1 )
				strcat(buffer, language( lge_YouArmyOne ) );
			else
				strcat(buffer, language( lge_YouNoArmyCas ) );
		}
		else
		{
			if (CSAlosses > 1 )
				sprintf(buffer + strlen(buffer), language( lge_cArmyCas ), CSAlosses);
			else if (CSAlosses == 1 )
				strcat(buffer, language( lge_cArmyOne ) );
			else
				strcat(buffer, language( lge_cNoArmyCas ) );
		}
	}

	const char* buttons;

	if(result == si)
		buttons = language( lge_hip );
	else
		buttons = language( lge_OK );

#ifdef DEBUG
	cbLog.printf("%s", (char*)buffer);
	cbLog.printf("%s", buttons);
#endif

	if(campaign)
		campaign->doAlert(buffer, buttons, 0, f, 0, batl);
	else
		dialAlert(0, buffer, buttons);

#ifdef DEBUG
	cbLog.printf("------------------");
	cbLog.close();
#endif

	if ((result == f->side) || (result == SIDE_None) )
		return True;

	return False;    
}
#endif

