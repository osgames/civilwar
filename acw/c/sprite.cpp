/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Sprite Blitting
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.7  1994/07/11  14:26:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.5  1994/04/20  22:21:40  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/01/07  23:43:18  Steven_Green
 * Use Image::getWidth/Height instead of accessing fields directly.
 *
 * Revision 1.3  1993/12/21  00:31:04  Steven_Green
 * Changes to prepare for 3D Sprites.
 *
 * Revision 1.2  1993/11/15  17:20:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/05  16:50:53  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <string.h>

#include "sprite.h"

// #include "screen.h"


/*
 * Copy an entire BitMap into a sprite
 */

Sprite::Sprite(Image& bm) : Image(bm.getWidth(), bm.getHeight())
{
	transparent = 0;
	memcpy(getAddress(), bm.getAddress(), getWidth() * getHeight());
}

#if 0

void SuperSprite::show(Screen* screen)
{
	// Copy from Screen to Buffer

	screen->unBlit(behind, where);
	
	// Blit image to screen

	screen->maskBlit(this, where);

}

void SuperSprite::hide(Screen* screen)
{
	// Copy buffer to screen

	screen->blit(behind, where);	
	
}

#endif

