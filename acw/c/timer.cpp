/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	BIOS Timer Interface
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.10  1994/09/23  13:28:28  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/09/02  21:25:33  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/08/22  16:02:09  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/08/09  15:42:15  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/03/11  23:12:15  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/03/10  14:27:18  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/02/28  23:04:17  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1993/11/16  22:42:06  Steven_Green
 * waitRel waits for at least one timer change.
 *
 * Revision 1.2  1993/11/15  17:20:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/09  14:01:40  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */


#pragma INITIALISE before library

#include "timer.h"

#ifdef DSMI_TIMER
#include "timeserv.h"
#endif


Timer* timer = 0;			// Automatically created at startup


const int Timer::ticksPerSecond = 80;
const int Timer::fastTPS = 320;


#ifdef DSMI_TIMER

// unsigned long timerCount;


void __saveregs __interrupt timerHandler()
{
	timer->timerCount++;			// 320Hz count
	if(! (timer->timerCount & 3) && !timer->paused)
		timer->count++;			// 80Hz Count
}

/*
 * Constructor for timer
 */

Timer::Timer() : count(0), lastCount(0), timerCount(0), paused(true)
{
	tsInit();
	tsAddRoutine((void(*)(void))timerHandler, 1193180/320);
	paused = False;
}

/*
 * Destructor for timer
 */

Timer::~Timer()
{
	tsClose();
}

long Timer::addRoutine(TimerFunction* function, long hz)
{
	return tsAddRoutine((void(*)(void))function, 1193180/hz);
}


void Timer::removeRoutine(long tag)
{
	tsRemoveRoutine(tag);
}

#else	// !DSMI_TIMER

/*
 * This routine called every 18ms or thereabouts
 */

// void far timerHandler()
void __saveregs __interrupt timerHandler()
{
	if(!timer->paused)
		timer->count++;
}

/*
 * Constructor for timer
 */

Timer::Timer() : count(0), lastCount(0), paused(true)
{
	installIRQ(timerHandler);
   paused = false;
}

/*
 * Destructor for timer
 */

Timer::~Timer()
{
	removeIRQ();
}

#endif

/*
 * Wait for x number of ticks
 *
 * In a multi-tasking environment, the wait functions should set an
 * event instead of sitting in a busy loop.
 */

void Timer::wait(unsigned int ticks)
{
	Boolean oldPause = paused;
	unPause();

	unsigned int end = getCount() + ticks;

	while( int(getCount()) < int(end))
		;

	paused = oldPause;
}

/*
 * Wait for a timer period relative to the last wait
 *
 * Always waits at least until the next counter interrupt.
 */

void Timer::waitRel(unsigned int ticks)
{
	Boolean oldPause = paused;
	unPause();

	unsigned int end;

	if(lastCount == 0)
		end = getCount() + ticks;
	else
		end = lastCount + ticks;

	unsigned int cnt = getCount();

	do {
		unsigned int cnt1;

		while( (cnt1 = getCount()) == cnt)
			;

		cnt = cnt1;
	} while(int(cnt) < int(end));

	lastCount = cnt;
	paused = oldPause;
}
