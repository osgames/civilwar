/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Rivers and Sea Zones
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.6  1994/08/09  15:42:15  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/07/28  18:57:04  Steven_Green
 * Naval Movement Implemented
 *
 * Revision 1.2  1994/06/09  23:32:59  Steven_Green
 * Modifications for reading from file
 *
 * Revision 1.1  1994/06/07  18:29:54  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>

#include "water.h"
#include "mapwind.h"
#include "text.h"
#include "system.h"
#include "screen.h"
#include "campaign.h"
#include "camptab.h"
#include "campcomb.h"
#ifdef CHRIS_AI
#include "clto.h"
#endif
#include "dialogue.h"
#include "language.h"
#include "memptr.h"
#include "game.h"
#include "sprlib.h"
#include "gamelib.h"
#include "language.h"
#include "colours.h"
#include "campwld.h"
#include "options.h"
#ifdef DEBUG
#include "system.h"
#include "tables.h"
#endif
#ifdef DEBUG
#include "log.h"

#ifndef CAMPEDIT
#include "clog.h"

LogFile waterLog("water.log");
#endif

#endif

#define Font_BoatCount Font_EMMA14

WaterZone::WaterZone() : MapObject(OT_WaterZone)
{
	side = SIDE_None;
	type = WZ_Sea;

#ifndef CAMPEDIT
	connectCount = 0;
	connections = 0;
	facilityCount = 0;
	facilityList = 0;
#endif

#ifdef DEBUG
	ai_cvNeeded = 0;
	ai_priority = 0;
#endif

}

WaterZone::~WaterZone()
{
}


WaterNetwork::WaterNetwork()
{
	// lastTime = 0;
	ferries = 0;
}

WaterNetwork::~WaterNetwork()
{
	clear();
}

void WaterNetwork::clear()
{
	Ferry* f = ferries;
	while(f)
	{
		Ferry* f1 = f;
		f = f->next;
		delete f1;
	}
	ferries = 0;
}



#ifdef DEBUG
void WaterNetwork::showWaterZones(MapWindow* map, CampaignWorld* world)
#else
void WaterNetwork::showWaterZones(MapWindow* map)
#endif
{
#ifdef DEBUG
  if(optWaterShow())
  {

  	for(int i = 0; i < zones.entries(); i++)
	{
		WaterZone& zone = zones[i];

		Point p = map->locationToPixel(zone.location);
		map->drawObject(&zone);

		/*
		 * Draw Lines to connections
		 */

#ifdef CAMPEDIT
		for(int j = 0; j < zone.connections.entries(); j++)
		{
			if(zone.connections[j] > i)	// Prevent drawing twice
			{
				WaterZone& zone1 = zones[zone.connections[j]];

				Point p1 = map->locationToPixel(zone1.location);

				map->drawBM->line(p, p1, LYellow);
			}
			
		}

		for(j = 0; j < zone.facilityList.entries(); j++)
		{
			Facility* f = world->facilities[zone.facilityList[j]];
			Point p1 = map->locationToPixel(f->location);
			map->drawBM->line(p, p1, LRed);
		}
#else
		int j = zone.connections;
		int k = zone.connectCount;
		while(k--)
		{
			WaterZone& zone1 = zones[connectList[j++]];
			Point p1 = map->locationToPixel(zone1.location);

			map->drawBM->line(p, p1, LYellow);
		}

		j = zone.facilityList;
		k = zone.facilityCount;
		while(k--)
		{
			Facility* f = getFacility(j++);
			Point p1 = map->locationToPixel(f->location);

			map->drawBM->line(p, p1, White);
		}
#endif

	}
  }
  else

  /*
   * Normal display routine follows
   */

#endif	// Debug

  {
  	for(int i = 0; i < zones.entries(); i++)
		map->drawObject(&zones[i]);
  }
}

#ifndef CAMPEDIT
#ifdef DEBUG_UPDATE_WATER

/*
 * Adjust water network so that zones map to sensible values
 */

void WaterNetwork::updateWater(CampaignWorld* world)
{
	/*
	 * Count how many landing stages or ports there are
	 * and assign them to coastal or river zones
	 */

	int wfCount = 0;

	for(FacilityID cityCount = 0; cityCount < world->facilities.entries(); cityCount++)
	{
		Facility* f = world->facilities.get(cityCount);

		if(f->facilities & Facility::F_Port)
		{
			wfCount++;

			/*
			 * Assign to a suitable coastal region
			 */

			CoastID best = NoCoast;
			Distance dist = 0;

			for(CoastID id = 0; id < coasts.entries(); id++)
			{
				CoastZone& zone = coasts.get(id);
				Distance d = distance(f->location.x - zone.location.x,
											 f->location.y - zone.location.y);
				if((best == NoCoast) || (d < dist))
				{
					best = id;
					dist = d;
				}
			}
			if(best && (dist < Mile(200)))
				f->waterZone = coastID(best);
			else
				f->facilities &= ~(Facility::F_Port | Facility::F_LandingStage);

		}
		else if(f->facilities & Facility::F_LandingStage)
		{
			wfCount++;

			/*
			 * Assign to a river Zone
			 */

			RiverID best = NoRiver;
			Distance dist = 0;

			for(RiverID id = 0; id < rivers.entries(); id++)
			{
				RiverZone& zone = rivers.get(id);
				Distance d = distance(f->location.x - zone.location.x,
											 f->location.y - zone.location.y);
				if((best == NoRiver) || (d < dist))
				{
					best = id;
					dist = d;
				}
			}
			if(best && (dist < Mile(100)))
				f->waterZone = riverID(best);
			else
				f->facilities &= ~Facility::F_LandingStage;
		}

	}

	/*
	 * Convert temporary values collected into the waternetwork structure
	 */

	facilityList.init(wfCount);
	int nextF = 0;

	/*
	 * Go through each zone and count the number of facilities using it!
	 */

	for(CoastID cid = 0; cid < coasts.entries(); cid++)
	{
		CoastZone& zone = coasts.get(cid);
		int count = 0;
		WaterZoneID wid;
		wid = coastID(cid);

		zone.facilityList = nextF;

		for(cityCount = 0; cityCount < world->facilities.entries(); cityCount++)
		{
			Facility* f = world->facilities.get(cityCount);

			if(f->waterZone == wid)
			{
				facilityList.set(nextF++, cityCount);
				count++;
			}
		}
		zone.facilityCount = count;
	}

	for(RiverID rid = 0; rid < rivers.entries(); rid++)
	{
		RiverZone& zone = rivers.get(rid);
		int count = 0;
		WaterZoneID wid;
		wid = riverID(rid);

		zone.facilityList = nextF;

		for(cityCount = 0; cityCount < world->facilities.entries(); cityCount++)
		{
			Facility* f = world->facilities.get(cityCount);

			if(f->waterZone == wid)
			{
				facilityList.set(nextF++, cityCount);
				count++;
			}
		}
		zone.facilityCount = count;
	}
}

#endif	// DEBUG_UPDATE_WATER
#endif	// CAMPEDIT

void WaterZone::mapDraw(MapWindow* map, Region* bm, Point where)
{
	/*
	 * Place a boat silouette/fill normal/highlight
	 */

#ifdef DEBUG
	if(campaign->showEmptyWaterZones || boats.totalBoats() || !fleets.isEmpty() || optWaterShow())
	{
#endif

		GameSprites icon;

		if(boats.totalBoats() || !fleets.isEmpty())
			icon = SPR_Naval1;		// Filled in
		else
			icon = SPR_Naval3;		// Silouette

		if(highlight)
			INCREMENT(icon);

		game->sprites->drawAnchoredSprite(bm, where, icon);


		/*
	 	 * Display flags if owned
	 	 */

		if(side & SIDE_USA)
		{
			icon = SPR_DivisionFlag1;
			if(highlight)
				INCREMENT(icon);

			Point p;
			if(side & SIDE_CSA)
				p = where - Point(6, 0);
			else
				p = where;

			game->sprites->drawAnchoredSprite(bm, p, icon);
		}

		if(side & SIDE_CSA)
		{
			icon = SPR_DivisionFlag3;
			if(highlight)
				INCREMENT(icon);

			Point p;
			if(side & SIDE_USA)
				p = where + Point(6, 0);
			else
				p = where;

			game->sprites->drawAnchoredSprite(bm, p, icon);
		}

#if !defined(CAMPEDIT)
		/*
		 * Display routes of fleets if any
		 */

		if(!fleets.isEmpty() && highlight && (seeAll || game->isPlayer(side)))
		{
			FleetIter fi = fleets;
			Fleet* fleet;

			while( (fleet = fi.next()) != 0)
			{
			 	WaterRoute* info = new WaterRoute;

			 	if(campaign->world->waterNet.findRoute(fleet->where, fleet->destination, True, 0, info) != NoWaterZone)
			 		info->drawRoute(map, bm, fleet->where, fleet->destination);

			 	delete info;
			}
		}
#endif

#ifdef DEBUG
	}

	/*
	 * Display ai values
	 */

	if(showAI)
	{
		TextWindow window(bm, Font_JAME_5);

		Colour sideColour[] = {
			Neutral_HColour,
			USA_HColour,
			CSA_HColour,
			Neutral_HColour
		};

		window.setColours(sideColour[side]);

		window.setPosition(where + Point(-15,-4));
		window.wprintf("%d", (int) ai_priority);
		window.setPosition(where + Point(-15,+4));
		window.wprintf("%d", (int) ai_cvNeeded);
		
	}


#endif


}

MapPriority WaterZone::isVisible(MapWindow* map) const
{
	if(map->displayType == MapWindow::Disp_Moveable)
	{
		if(highlight)
			return Map_Highlight;
#ifdef DEBUG
		else if(showAI || campaign->showEmptyWaterZones || boats.totalBoats() || !fleets.isEmpty() || optWaterShow())
#else
		else if(campaign->showEmptyWaterZones || boats.totalBoats() || !fleets.isEmpty())
#endif
			return Map_WaterZone;
	}

	return Map_NotVisible;
}

/*
 * Display Campaign Information
 */

void WaterZone::showCampaignInfo(Region* window)
{
	placeInfo(window, 0);
}

/*
 * Display Naval Info
 *
 * If fleet is non-zero, then it is really
 * displaying the icons, and fleet is the boats that are leaving
 */

void WaterZone::placeInfo(Region* window, Flotilla* fleet)
{
	Region region = *window;

	SDimension y = 0;
	for(int i = 0; i < Nav_TypeCount; i++)
	{
		region.setClip(window->getX(), window->getY() + y, window->getW(), 78);
		placeInfo(&region, (NavalType)i, fleet);
		y += 78;
	}
}


void WaterZone::placeInfo(Region* window, NavalType type, Flotilla* fleet)
{
	Region region = *window;

	/*
	 * Fill the background in first
	 */

	region.fill(LBlue);
	region.frame(0, 0, region.getW(), region.getH(), Black);

	/*
	 * Set up a textwindow
	 */

	TextWindow textWind(&region, Font_BoatCount);
	textWind.setFormat(True, True);
	textWind.setFont(Font_BoatCount);

 	UBYTE ourBoats = boats.count[type];
 	UBYTE otherBoats;
 
	if(fleet)
	{
		ourBoats -= fleet->count[type];
		otherBoats = fleet->count[type];
	}
	else
	{
		Flotilla allBoats;			// Total boats in all fleets
		fleets.total(allBoats);

		otherBoats = allBoats.count[type];
	}

	if(!ourBoats && !otherBoats)
		window->fill(Blue);
	else
		window->fill(LBlue);

	window->frame(0, 0, window->getW(), window->getH(), Black);

	region.setClip(window->getX(), window->getY(), window->getW(), 59);
	game->sprites->drawCentredSprite(&region, GameSprites(C1_Naval + type));


	if(ourBoats || otherBoats)
	{
		region.setClip(window->getX(), window->getY() + 59, window->getW()/2, 78-59);

		char buffer[20];
		sprintf(buffer, "%u", (unsigned int) ourBoats);
		textWind.draw(buffer);

		if(otherBoats || fleet)
		{
			region.setClip(window->getX()+window->getW()/2, window->getY() +59, window->getW()/2, 78-59);
			sprintf(buffer, "%u", (unsigned int) otherBoats);
			textWind.draw(buffer);
		}
	}
	else
		window->greyOut(Rect(0, 0, window->getW(), window->getH()), DGrey);

	machine.screen->setUpdate(*window);
}

#ifndef CAMPEDIT
WaterZoneID WaterZone::getID(const WaterNetwork* net) const
{
	return net->zones.getID(this);
}
#endif

#ifndef CAMPEDIT

void WaterNetwork::process(const TimeBase& t)
{
	/*
	 * Process Naval Units once every 15 minutes
	 */

	const GameTicks NavalRate = GameTicksPerMinute * 15;

	GameTicks elapsed;
	TimeBase now = t;

	if(lastTime == 0)
	{
		lastTime = now - NavalRate;
		elapsed = NavalRate;
	}
	else
		elapsed = now - lastTime;

	while(elapsed >= NavalRate)
	{
		elapsed -= NavalRate;
		lastTime += NavalRate;

		for(int sCount = 0; sCount < zones.entries(); sCount++)
			processFleets(zones[sCount], lastTime);

		/*
		 * Process Ferries
		 */

		Ferry* ferry = ferries;
		while(ferry)
		{
			Ferry* f = ferry;
			ferry = ferry->next;
			moveFerry(f, lastTime);
		}
	}
}

#ifdef DEBUG

char* sideName(Side side)
{
	if(side == SIDE_USA)
		return "USA";
	else if(side == SIDE_CSA)
		return "CSA";
	else if(side == SIDE_None)
		return "None";
	else
		return "Mixed";
}

char* flotillaName(Flotilla* boats)
{
	static char buf1[80];
	static char buf2[80];
	static int which = 0;

	char* s = which ? buf1 : buf2;
	which ^= 1;

	sprintf(s, "[%d,%d,%d,%d]",
		(int)boats->count[0],
		(int)boats->count[1],
		(int)boats->count[2],
		(int)boats->count[3]);


	return s;
}

char* fleetName(Fleet* fleet)
{
	static char buf1[80];
	static char buf2[80];
	static int which = 0;

	char* s = which ? buf1 : buf2;
	which ^= 1;

	sprintf(s, "%d %s %s",
		fleet->id,
		sideName(fleet->side),
		flotillaName(&fleet->boats));

	return s;
}

#endif

void WaterNetwork::processFleets(WaterZone& zone, const TimeBase& gameTime)
{
	FleetIter flist = zone.fleets;
	Fleet* fleet;

	while( (fleet = flist.next()) != 0)
	{
		if(gameTime >= fleet->arrival)
		{
			WaterZone* here = &zones[fleet->via];

#ifdef DEBUG
			waterLog.printf("Fleet %s arrived at zone %d",
				fleetName(fleet),
				(int) fleet->via);
			waterLog.printf("Zone contains %s %s", sideName(here->side), flotillaName(&here->boats));
#endif
			/*
			 * Fleet has arrived at new zone
			 */

			zone.fleets.remove(fleet);				// Remove fleet from old zone

			/*
			 * Check for enemy in zone and do combat if necessary
			 * If lost combat then retreat (set destination to somewhere else)
			 * If won then set enemy ships into retreat
			 *
			 * Check for enemy ought to include check for fortifications, etc.
			 */

			if(here->isEnemy(fleet->side))
			{
#ifdef DEBUG
				waterLog.printf("Fleet %s entering enemy zone containing %s",
					fleetName(fleet), flotillaName(&here->boats));
#endif

				if(here->fightNavalBattle(fleet->boats))
				{		// We've won so get in there!

#ifdef DEBUG
					waterLog.printf("Won Battle!\n");
#endif

					/*
					 * Take any existing ships and get them to retreat to
					 * a random neighbouring zone (priority to friendly)
					 */

					if(here->boats.totalBoats())
					{
						FindRetreatZone( here, here->side );

#ifdef DEBUG
						waterLog.printf("Retreated to Zone\n");
							
#endif
					}

					/*
					 * Make it ours!
					 */

					here->side = fleet->side;

				}
				else	// We lost and so must retreat
				{
					/*
					 * Retreat to where we just came from
					 * ... This really should be more complex:
					 *   IF where it just came from is not enemy
					 *     return to where it came from
					 *   ELSE if there are any neighbouring non-enemy zones
					 *     go to a random non-enemy zone
					 *   ELSE
					 *     go to a random neighbouring zone
					 */

					fleet->destination = fleet->where;
				}
			}
			else
				here->side = fleet->side;

			if(fleet->boats.totalBoats() == 0)
			{
#ifdef DEBUG
					TimeInfo tm = fleet->arrival;

					game->simpleMessage("Fleet %d [%d,%d,%d,%d] in zone %d completely destroyed",
						fleet->id,
						(int)fleet->boats.count[0],
						(int)fleet->boats.count[1],
						(int)fleet->boats.count[2],
						(int)fleet->boats.count[3],
						(int)fleet->where);

					waterLog.printf("Fleet %d [%d,%d,%d,%d] in zone %d completely destroyed",
						fleet->id,
						(int)fleet->boats.count[0],
						(int)fleet->boats.count[1],
						(int)fleet->boats.count[2],
						(int)fleet->boats.count[3],
						(int)fleet->where);
#endif

				delete fleet;
			}
			else

			/*
			 * IF at destination
			 *	  Add ships to zone and delete fleet
			 */

			if(fleet->destination == fleet->via)
			{
#ifdef DEBUG
					TimeInfo tm = fleet->arrival;

					game->simpleMessage("Fleet %d [%d,%d,%d,%d] arrived at zone %d",
						fleet->id,
						(int)fleet->boats.count[0],
						(int)fleet->boats.count[1],
						(int)fleet->boats.count[2],
						(int)fleet->boats.count[3],
						(int)fleet->destination);

					waterLog.printf("Fleet %d [%d,%d,%d,%d] arrived at zone %d",
						fleet->id,
						(int)fleet->boats.count[0],
						(int)fleet->boats.count[1],
						(int)fleet->boats.count[2],
						(int)fleet->boats.count[3],
						(int)fleet->destination);
#endif
				here->boats += fleet->boats;
				here->side = fleet->side;
				delete fleet;

			}

			/*
			 * IF not at destination
			 *  Calculate next via zone and get moving
			 */

			else
			{
				sendFleet(fleet, fleet->via, fleet->destination, gameTime);
				// here->fleets.add(fleet);
			}
		}
	}

#ifdef DEBUG
	waterLog.close();
#endif

}


void WaterNetwork::sendFleet(Fleet* fleet, WaterZoneID from, WaterZoneID to, const TimeBase& gameTime)
{
	WaterZone* here = &zones[from];

	fleet->where = from;
	fleet->destination = to;

	WaterRoute waterRoute;

	WaterZoneID via = findRoute(fleet->where, fleet->destination, False, &fleet->boats, &waterRoute);
	if(via == NoWaterZone)
		via = findRoute(fleet->where, fleet->destination, True, &fleet->boats, &waterRoute);

	ASSERT(via != NoWaterZone);
	if(via == NoWaterZone)
	{	// This should never happen?  If it does then send it straight there
#ifdef DEBUG
		TimeInfo tm = fleet->arrival;

		game->simpleMessage("Fleet %d [%d,%d,%d,%d] at %d has impossible route!",
			fleet->id,
			(int)fleet->boats.count[0],
			(int)fleet->boats.count[1],
			(int)fleet->boats.count[2],
			(int)fleet->boats.count[3],
			(int)fleet->where);
		waterLog.printf("Fleet %d [%d,%d,%d,%d] at %d has impossible route!",
			fleet->id,
			(int)fleet->boats.count[0],
			(int)fleet->boats.count[1],
			(int)fleet->boats.count[2],
			(int)fleet->boats.count[3],
			(int)fleet->where);
#endif
		via = to;
	}

	fleet->via = via;
	fleet->arrival = gameTime + waterRoute.route[via].cost;

#ifdef DEBUG
	TimeInfo tm = fleet->arrival;

	game->simpleMessage("Fleet %d [%d,%d,%d,%d] moving from zone %d to %d\rArrival at %d at %d:%02d:%02d on %s %d",
		fleet->id,
		(int)fleet->boats.count[0],
		(int)fleet->boats.count[1],
		(int)fleet->boats.count[2],
		(int)fleet->boats.count[3],
		(int)fleet->where,
		(int)fleet->destination,
		(int)fleet->via,
		tm.hour, tm.minute, tm.second, tm.monthStr, tm.day);
	waterLog.printf("Fleet %d [%d,%d,%d,%d] moving from zone %d to %d\rArrival at %d at %d:%02d:%02d on %s %d",
		fleet->id,
		(int)fleet->boats.count[0],
		(int)fleet->boats.count[1],
		(int)fleet->boats.count[2],
		(int)fleet->boats.count[3],
		(int)fleet->where,
		(int)fleet->destination,
		(int)fleet->via,
		tm.hour, tm.minute, tm.second, tm.monthStr, tm.day);
#endif

	here->fleets.add(fleet);

#ifdef DEBUG
	waterLog.close();
#endif
}

/*
 * IS a zone friendly or enemy?
 */

Boolean WaterZone::isEnemy(Side s)
{
	/*
	 * Check for enemy boats
	 */

	if(onOtherSide(s, side) && boats.totalBoats())
		return True;

	/*
	 * Check for enemy forts
	 */

	return False;
}

const char* boatName(int i, Boolean plural)
{
	static const char* boatNamesSingular[] = {
		"Naval Unit",
		"Naval Iron Clad",
		"Riverine Unit",
		"Riverine Iron Clad"
	};
	static const char* boatNamesPlural[] = {
		"Naval Units",
		"Naval Iron Clads",
		"Riverine Units",
		"Riverine Iron Clads"
	};

	if(plural)
		return boatNamesPlural[i];
	else
		return boatNamesSingular[i];
}

void putBoatName(char* buffer, const Flotilla* losses)
{
	Boolean isFirst = True;

	for(int i = 0; i < Nav_TypeCount; i++)
	{
		if(losses->count[i])
		{
			if(!isFirst)
				strcat(buffer, ", ");
			sprintf(buffer + strlen(buffer), "%d %s", losses->count[i], boatName(i, losses->count[i] > 1));
			isFirst = False;
		}
	}

	if(isFirst)
		strcat(buffer, language(lge_nothing));
}

/*
 * Process a naval battle in a zone
 *
 * return:
 *  False: Newcomer loses battle (newcomer must retreat)
 *  True: Newcomer wins battle (existing boats must retreat)
 */

Boolean WaterZone::fightNavalBattle(Flotilla& attacker)
{
	/*
	 * Total up the naval combat factor
	 */

	int attackValue = attacker.totalCombatFactor();
	int defendValue = boats.totalCombatFactor();

#ifdef DEBUG
	waterLog.printf("Naval Battle between: %s %s (%d) and %s %s (%d)",
		sideStr[side],
		flotillaName(&boats), defendValue,
		sideStr[otherSide(side)],
		flotillaName(&attacker), attackValue);
#endif

	/*
	 * Set it up so defender > attacker
	 */

	Boolean attackIsBig;
	Flotilla* larger;
	Flotilla* smaller;

	if(attackValue > defendValue)
	{
		attackIsBig = True;
		larger = &attacker;
		smaller = &boats;
		swap(attackValue, defendValue);
	}
	else
	{
		attackIsBig = False;
		larger = &boats;
		smaller = &attacker;
	}

	int ratio = defendValue / attackValue;
	if(ratio > 6)
		ratio = 6;

	static struct NavalCombatTable {
		UBYTE changeValue;		// dice >= change, then attacker can take control
		Boolean forceSink;		// IF set then at least one ship must be sunk on losing side
		UBYTE loss1;				// percent Losses to winner
		UBYTE loss2;				// percent Losses to loser
	} navalCombatResults[7] = 
	{
		{ 8, False, 0, 0 },	// 0:1
		{ 8, False, 0, 0 },	// 1:1
		{ 4, False, 0, 5 },	// 2:1
		{ 3, False, 0, 10 },	// 3:1
		{ 3, False, 5, 15 }, // 4:1
		{ 2, True,  5, 20 },	// 5:1
		{ 1, True,  5, 25 }	// 6:1
	};

	NavalCombatTable& tab = navalCombatResults[ratio];

	/*
	 * See if swap sides
	 */

	Boolean retreat = False;


	int dice = game->gameRand(10);
	if(dice >= tab.changeValue)
	{
		/*
		 * Attacker takes over?
		 */

		if(attackIsBig || (ratio == 0))
			retreat = True;
	}

	/*
	 * Display a message to the user!
	 */


	MemPtr<char> buffer(500);

	sprintf(buffer, "%s\r\r", language(lge_NavalBattle));

	if(side == SIDE_USA)
	{
		sprintf(buffer + strlen(buffer), "%s ", language(lge_CSAflotilla));
		putBoatName(buffer, &attacker);
		sprintf(buffer + strlen(buffer), " %s ", language(lge_attackUSAflotilla));
		putBoatName(buffer, &boats);
	}
	else
	{
		sprintf(buffer + strlen(buffer), "%s ", language(lge_USAflotilla));
		putBoatName(buffer, &attacker);
		sprintf(buffer + strlen(buffer), " %s ", language(lge_attackCSAflotilla));
		putBoatName(buffer, &boats);
	}

	if(retreat)
		sprintf(buffer + strlen(buffer), " %s", language(lge_forceRetreat));
	else
		sprintf(buffer + strlen(buffer), " %s", language(lge_hadToWithdraw));

	strcat(buffer, "\r\r");

	/*
	 * Apply losses
	 */

	Flotilla largeLoss;
	Flotilla smallLoss;

	if(tab.loss1)
		larger->sinkBoats(tab.loss1, False, largeLoss);

	if(tab.loss2)
		smaller->sinkBoats(tab.loss2, tab.forceSink, smallLoss);

#ifdef DEBUG
	waterLog.printf("Losses: %d%% / %d%%", tab.loss1, tab.loss2);
	waterLog.printf("Home ships: %s", flotillaName(&boats));
	waterLog.printf("Attacker: %s", flotillaName(&attacker));
#endif

	Side attackSide = otherSide(side);

	Flotilla* fUSA = &largeLoss;
	Flotilla* fCSA = &smallLoss;

	if(side != SIDE_USA)
		swap(fUSA, fCSA);
	if(attackIsBig)
		swap(fUSA, fCSA);

	Side winSide;
	if(retreat)
		winSide = attackSide;
	else
		winSide = side;


	/*
	 * Display losses
	 */

	if(fUSA->totalBoats())
	{
		sprintf(buffer + strlen(buffer), "%s ", language(lge_USAlost));
		putBoatName(buffer, fUSA);
	}
	else
		strcat(buffer, language(lge_USAnoDamage));

	strcat(buffer, "\r\r");

	if(fCSA->totalBoats())
	{
		sprintf(buffer + strlen(buffer), "%s ", language(lge_CSAlost));
		putBoatName(buffer, fCSA);
	}
	else
		strcat(buffer, language(lge_CSAnoDamage));
		
	MemPtr<char> buttons(200);

	if(game->isPlayer(winSide))
		strcpy(buttons, language(lge_hip));
	else
		strcpy(buttons, language(lge_drat));

#ifdef DEBUG
	waterLog.printf("Message: %s\n\n%s", (char*) buffer, (char*) buttons);
#endif

	campaign->doAlert(buffer, buttons, 0, 0, this, 0);

#ifdef DEBUG
	waterLog.close();
#endif

	return retreat;
}											  

/*
 * Find zone to retreat to
 *
 * These need expanding:
 *		Should pick a neighbouring zone that is not an enemy
 */

WaterZoneID WaterZone::getRetreatZone(Side side) const
{
	return campaign->world->waterNet.connectList[connections];
}


#endif		// CAMPEDIT

#ifndef CAMPEDIT
/*
 * Naval Route calculation
 */

struct WaterQueueItem {
	WaterZoneID from;
	GameTicks cost;

	void set(WaterZoneID zone, GameTicks t)
	{
		from = zone;
		cost = t;
	}

	void get(WaterZoneID& zone, GameTicks& t)
	{
		zone = from;
		t = cost;
	}
};

class WaterQueue {
	WaterQueueItem* items;
	int howMany;
	int head;
	int tail;
public:
	WaterQueue(int n);
	~WaterQueue();

	void put(WaterZoneID from, GameTicks cost);
	Boolean get(WaterZoneID& from, GameTicks& cost);
	Boolean isEmpty() { return head==tail; }
};

WaterQueue::WaterQueue(int n)
{
	items = new WaterQueueItem[n];
	howMany = n;
	head = 0;
	tail = 0;
}
 

WaterQueue::~WaterQueue()
{
	delete[] items;
}

void WaterQueue::put(WaterZoneID from, GameTicks cost)
{
	int nextHead = head+1;
	if(nextHead >= howMany)
		nextHead = 0;

	if(nextHead == tail)
		throw GeneralError("water queue full");

	items[head].set(from, cost);
	head = nextHead;
}

Boolean WaterQueue::get(WaterZoneID& from, GameTicks& cost)
{
	if(head == tail)
		return False;
	else
	{
		items[tail].get(from, cost);

		if(++tail >= howMany)
			tail = 0;

		return True;
	}
}


struct WaterRouteVars {
	WaterRouteNode* costs;
	WaterQueue queue;
	Boolean allowEnemy;
	const Flotilla* fleet;
	WaterZoneID to;
	WaterZoneID zone;
	GameTicks cost;
	WaterZone* zonePtr;
	Side side;
	Boolean noRiver;
	Boolean noSea;

	WaterRouteVars(int total, const Flotilla* fl) : queue(total), fleet(fl) { }
};


GameTicks waterDistance(WaterZone* from, WaterZone* to, UBYTE baseSpeed)
{
	/*
	 * Calculate distance to zone
	 */

	Distance d = distance(from->location.x - to->location.x,
							    from->location.y - to->location.y);

	GameTicks travelTime;
	travelTime = (unitToMile(d) * GameTicksPerHour) / baseSpeed;

	switch(from->type)
	{
	case WZ_Sea:
		travelTime = (travelTime * seaSpeed) / 100;
		break;
	case WZ_Coast:
		break;
	case WZ_River:
		travelTime = (travelTime * riverSpeed) / 100;
		break;
	default:
		return 0;
	}

	return travelTime;
}

GameTicks ferryTime(const Location& l1, const Location& l2)
{
	/*
	 * Calculate distance to zone
	 */

	Distance d = distance(l1.x - l2.x, l1.y - l2.y);

	GameTicks travelTime;

	travelTime = (unitToMile(d) * GameTicksPerHour) / ferrySpeed;	// 10 mph around coast

	return travelTime;
}

/*
 * For each connection from zone
 * IF it is enemy or disallowed type ignore it
 * calcuate cost to get there
 * IF new cost is > existing then ignore
 * Set connections cost and from
 * IF it is NOT the final destination
 *   Add to queue
 */


void WaterNetwork::processRoute(WaterZoneID id, WaterRouteVars& vars)
{
	WaterZone* zone = &zones[id];

	if(!zone)
		return;

	if(vars.noRiver && (zone->type == WZ_River))
		return;

	if(vars.noSea && (zone->type == WZ_Sea))
		return;

	/*
	 * Ignore if on wrong side
	 */

	if(!vars.allowEnemy && onOtherSide(zone->side, vars.side))
		return;

	GameTicks travelTime = waterDistance(zone, vars.zonePtr, vars.fleet ? vars.fleet->getSpeed() : ferrySpeed) + vars.cost;

	/*
	 * Is this route quicker?
	 */

	if(vars.costs[id].cost < travelTime)
		return;

	/*
	 * Update the cost table
	 */

	vars.costs[id].cost = travelTime;
	vars.costs[id].from = vars.zone;

	if(id == vars.to)	// arrived at destination!
		return;

	/*
	 * Add to movement queue
	 */

	vars.queue.put(id, travelTime);
}

WaterZoneID WaterNetwork::findRoute(WaterZoneID from, WaterZoneID to, Boolean allowEnemy, const Flotilla* fleet, WaterRoute* routes)
{
	ASSERT(from != NoWaterZone);
	ASSERT(to != NoWaterZone);

	if( (from == NoWaterZone) || (to == NoWaterZone))
		return NoWaterZone;

	/*
	 * Set up cost table
	 */

	int totalNodes = zones.entries();

	WaterRouteVars vars(totalNodes, fleet);
	vars.allowEnemy = allowEnemy;
	vars.to = to;
	vars.side = zones[from].side;

	if(routes && routes->route)
		vars.costs = routes->route;
	else
		vars.costs = new WaterRouteNode[totalNodes];

	/*
	 * Initialise cost table to unvisited
	 */

	WaterRouteNode* costPtr = vars.costs;
	int i = totalNodes;
	while(i--)
	{
		costPtr->cost = UndefinedTime;
		costPtr->from = NoWaterZone;

		costPtr++;
	}

	/*
	 * Precalculate a few things
	 */

	if(fleet)
	{
		vars.noRiver = fleet->count[Nav_NavalUnit] || fleet->count[Nav_IronClad];
		vars.noSea = fleet->count[Nav_Riverine] || fleet->count[Nav_Monitor];
	}
	else
	{
		vars.noRiver = False;
		vars.noSea = False;
	}

	/*
	 * Add initial entry to queue
	 */

	vars.queue.put(from, 0);
	vars.costs[from].cost = 0;
	vars.costs[from].from = from;

	/*
	 * Loop until finished
	 */


	while(vars.queue.get(vars.zone, vars.cost))
	{
		vars.zonePtr = &zones[vars.zone];

		int count = vars.zonePtr->connectCount;
		int con = vars.zonePtr->connections;
		while(count--)
		{
			processRoute(connectList[con++], vars);
		}
	}
#ifdef DEBUG
	/*
	 * Print results of search to log
	 */

	if(logFile && logFile->isShown(30))
	{
		*logFile << "Water Route Table:" << endl;
		for(int i = 0; i < totalNodes; i++)
		{
			*logFile << i << " ";
			
			if(vars.costs[i].from == NoWaterZone)
				*logFile << "No Route";
			else
				*logFile << vars.costs[i].from << " " << vars.costs[i].cost;
			*logFile << endl;
		}
		*logFile << endl;
	}
#endif

	/*
	 * Calculate return value for user
	 */

	WaterZoneID result;
	
	if(vars.costs[to].from == NoWaterZone)
		result = NoWaterZone;
	else
	{
		result = to;
		while( (vars.costs[result].from != from) && 
		       (vars.costs[result].from != NoWaterZone) )
		{
			result = vars.costs[result].from;
		}
	}

	if(routes)
		routes->route = vars.costs;
	else
		delete vars.costs;

	return result;
}


GameTicks WaterNetwork::findRouteTime(WaterZoneID from, WaterZoneID to, const Flotilla* fleet)
{
	WaterRoute route;

	WaterZoneID via = findRoute(from, to, True, fleet, &route);
	if(via == NoWaterZone)
		return 0;

	return route.route[to].cost;
}

void WaterRoute::drawRoute(MapWindow* map, Region* bm, WaterZoneID from, WaterZoneID to) const
{
	if(route && (from != to))
	{
		WaterNetwork& net = campaign->world->waterNet;

		WaterZone* z = &net.zones[to];
		Point prev = map->locationToPixel(z->location);
		WaterZoneID id = route[to].from;

		while(id != NoWaterZone)
		{
			z = &net.zones[id];

			Point dest = map->locationToPixel(z->location);

			bm->line(dest, prev, White);

			if(id == from)
				break;

			id = route[id].from;
			prev = dest;
		}
	}
}

#endif	// CAMPEDIT

#ifndef CAMPEDIT

/*
 * Ferries for convoying troops
 */

void WaterNetwork::addFerry(Ferry* ferry)
{
	ferry->next = ferries;
	ferries = ferry;
}

/*
 * Create a ferry to carry troops between facilities
 */

void WaterNetwork::makeFerry(Unit* unit, FacilityID from, FacilityID to)
{
	// Patch... 7th February 1996
	ASSERT(unit != 0);
	if(unit == 0)
		return;
	//.... end of patch

	const Facility* facility = campaign->world->facilities.get(from);
	WaterZoneID fromZone = facility->waterZone;
	const WaterZone* zone = &zones[fromZone];

	Ferry* ferry = new Ferry;

	ferry->next = ferries;
	ferries = ferry;

	ferry->lastZone = NoWaterZone;
	ferry->nextZone = fromZone;
	ferry->destination = to;

	ferry->side = unit->getSide();
	ferry->passenger = unit;
	ferry->mode = Ferry::FERRY_Moving;
	ferry->arrival = campaign->gameTime + ferryTime(facility->location, zone->location);

 	unit->setMoveMode(MoveMode_Transport);

#ifdef DEBUG
	const char* cityName = facility->getNameNotNull();

	Facility* f = campaign->world->facilities[to];
	const char* cityName2 = f->getNameNotNull();

	TimeInfo tm = ferry->arrival;

	game->simpleMessage("%s boarded boat at %s for %s\rArrive at zone %d at %d:%02d:%02d",
		unit->getName(False),
		cityName, cityName2,
		(int)fromZone,
		tm.hour, tm.minute, tm.second);
	waterLog.printf("%s boarded boat at %s for %s\rArrive at zone %d at %d:%02d:%02d",
		unit->getName(False),
		cityName, cityName2,
		(int)fromZone,
		tm.hour, tm.minute, tm.second);
#endif
}

#endif

Ferry::Ferry()
{
	next = 0;
}

Ferry::~Ferry()
{
}

#ifndef CAMPEDIT

// Patch... 7th February 1996

void WaterNetwork::removeFerry(Ferry* ferry)
{
	Ferry* f = ferries;
	Ferry* last = 0;
	while(f)
	{
		if(f == ferry)
		{
			if(last)
				last->next = ferry->next;
			else
				ferries = ferry->next;
			break;
		}

		last = f;
		f = f->next;
	}

#ifdef DEBUG
	if(f == 0)
		waterLog.printf("removeFerry()... ferry not in ferry list!");
#endif

	delete ferry;
}


void WaterNetwork::removeUnit(Unit* u)
{
	Ferry* f = ferries;
	Ferry* last = 0;
	while(f)
	{
		if(f->passenger == u)
		{
			if(last)
				last->next = f->next;
			else
				ferries = f->next;
			delete f;
			return;
		}

		f = f->next;
	}
}

//.... end of patch


void WaterNetwork::moveFerry(Ferry* ferry, const TimeBase& gameTime)
{
	// Patch... 7th February 1996
	ASSERT(ferry != 0);
	ASSERT(ferry->passenger != 0);

	if(ferry->passenger == 0)
		removeFerry(ferry);

	//.... end of patch

	if(gameTime >= ferry->arrival)
	{
		Facility* facility = campaign->world->facilities.get(ferry->destination);

		if(ferry->mode == Ferry::FERRY_Landing)
		{
			/*
			 * We've arrived!
			 */

			ferry->passenger->setOrderNow(Order(ORDER_Advance, ORDER_Land, ferry->passenger->realOrders.destination));
			ferry->passenger->setMoveMode(MoveMode_Marching);
			ferry->passenger->setLocation(facility->location);


#ifdef DEBUG
			const char* fname = facility->getNameNotNull();

			game->simpleMessage("%s has disembarked at %s",
				ferry->passenger->getName(False),
				facility->getNameNotNull());
			waterLog.printf("%s has disembarked at %s",
				ferry->passenger->getName(False),
				facility->getNameNotNull());
#endif

			/*
			 * Remove ferry
			 */

			// Patch... 7th February 1996
			removeFerry(ferry);

#if 0
			Ferry* f = ferries;
			Ferry* last = 0;
			while(f && (f != ferry))
			{
				last = f;
				f = f->next;
			}
			if(f)
			{
				if(last)
					last->next = ferry->next;
				else
					ferries = ferry->next;
			}
#endif
			//.... end of patch

		}
		else	// Assume moving mode
		{
			WaterZone* zone = &zones[ferry->nextZone];

			ferry->passenger->setLocation(zone->location);

			/*
			 * If arrived at destination zone, then land
			 */

			ferry->lastZone = ferry->nextZone;

			if(ferry->nextZone == facility->waterZone)
			{
				ferry->land(facility, zone, gameTime);
				return;
			}

			/*
			 * If new zone is enemy controlled, then try to land
			 * somewhere.
			 */



			/*
			 * Calculate next zone on route
			 */

			WaterZoneID via = findRoute(ferry->nextZone, facility->waterZone, False, 0, 0);
			if(via == NoWaterZone)
				via = findRoute(ferry->nextZone, facility->waterZone, True, 0, 0);
			if( (via == NoWaterZone) || (onOtherSide(zone->side, ferry->side)))
			{
#ifdef DEBUG
				waterLog.printf("%s must do an emergency landing",
					ferry->passenger->getName(True));
#endif

				/*
				 * Pick a random facility in current zone and land
				 */

				FacilityID land = zone->getEmergencyLand(ferry->side);

				if(land != NoFacility)
					ferry->destination = land;

				Facility* f = campaign->world->facilities.get(ferry->destination);
				ferry->passenger->realOrders.destination = f->location;
				ferry->passenger->givenOrders.destination = f->location;

				ferry->land(f, zone, gameTime);

			}
			else
			{	// Move onto next zone
				WaterZone* zone1 = &zones[via];

				ferry->nextZone = via;
				ferry->arrival = gameTime + ferryTime(zone->location, zone1->location);
#ifdef DEBUG
				TimeInfo tm = ferry->arrival;

				game->simpleMessage("%s moving from zone %d to %d\rArrive at %d:%02d:%02d",
					ferry->passenger->getName(False),
					(int)ferry->lastZone,
					(int)ferry->nextZone,
					tm.hour, tm.minute, tm.second);
				waterLog.printf("%s moving from zone %d to %d\rArrive at %d:%02d:%02d",
					ferry->passenger->getName(False),
					(int)ferry->lastZone,
					(int)ferry->nextZone,
					tm.hour, tm.minute, tm.second);
#endif

			}
		}
	}
	else
	{
		/*
		 * Set passenger's location to mid-points
		 */

		Location l1;
		Location l2;

		if(ferry->lastZone != NoWaterZone)
			l1 = zones[ferry->lastZone].location;
		else
		{
			FacilityID fid = ferry->passenger->realOrders.onFacility;
			if(fid == NoFacility)
				return;

			Facility* f = campaign->world->facilities[fid];
			l1 = f->location;
		}

		if(ferry->mode == Ferry::FERRY_Landing)
		{
			Facility* f = campaign->world->facilities.get(ferry->destination);
			l2 = f->location;
		}
		else
			l2 = zones[ferry->nextZone].location;

		int timeLeft = (ferry->arrival - gameTime) / GameTicksPerMinute;
		int totalTime = ferryTime(l1, l2) / GameTicksPerMinute;

		if(totalTime)
		{
			Location midPoint;

			// These calculations are abit complex so as to keep numbers manageable sizes

			int delta = (l1.x - l2.x) >> 16;
			delta = (delta * timeLeft) / totalTime;
			midPoint.x = l2.x + (delta << 16);

			delta = (l1.y - l2.y) >> 16;
			delta = (delta * timeLeft) / totalTime;
			midPoint.y = l2.y + (delta << 16);

			ferry->passenger->setLocation(midPoint);
		}
	}
}


void Ferry::land(Facility* facility, WaterZone* zone, const TimeBase& gameTime)
{
	nextZone = NoWaterZone;
	mode = Ferry::FERRY_Landing;
	arrival = gameTime + ferryTime(facility->location, zone->location);
#ifdef DEBUG
	const char* cityName = facility->getNameNotNull();

	TimeInfo tm = arrival;

	game->simpleMessage("%s is landing at %s\rArrive at %d:%02d:%02d",
		passenger->getName(False),
		cityName,
		tm.hour, tm.minute, tm.second);
	waterLog.printf("%s is landing at %s\rArrive at %d:%02d:%02d",
		passenger->getName(False),
		cityName,
		tm.hour, tm.minute, tm.second);
#endif
}


/*
 * Find somewhere for boat to land...
 * This needs refining... to find friendly places first!
 */

FacilityID WaterZone::getEmergencyLand(Side side) const
{
	if(facilityCount)
	{
		UBYTE which = game->gameRand(facilityCount);

		FacilityID id = campaign->world->waterNet.facilityList.get(which + facilityList);

#ifdef DEBUG
		Facility* f = campaign->world->facilities[id];
		waterLog.printf("Emergency land at %s", f->getNameNotNull());
#endif

		return id;
	}
	else
	{
#ifdef DEBUG
		waterLog.printf("Nowhere to land");
#endif
		return NoFacility;
	}
}


Facility* WaterNetwork::getFacility(FacilityListID id)
{
	return campaign->world->facilities.get(facilityList.get(id));
}

void WaterNetwork::FindRetreatZone( WaterZone* wz, Side s )
{
	if(wz == NULL)
		return;

	WaterZoneID rCount = zones.getID(wz);

	WaterZoneID from = NoWaterZone;
	WaterZoneID to = NoWaterZone;

	Flotilla flotilla = wz->boats;

	UWORD largest	= 0xffff;

	if ( !flotilla.totalBoats() ) 
		return;
 
	Boolean noRiver = flotilla.count[Nav_NavalUnit] || flotilla.count[Nav_IronClad];
	Boolean noSea = flotilla.count[Nav_Riverine] || flotilla.count[Nav_Monitor];

	UBYTE priority = 0;

 	for (int loop = 0; loop < wz->connectCount; loop++ )
	{	
		WaterZoneID rid = getConnect(loop + wz->connections);

		WaterZone& neighbour = zones[rid];

		/*
		 * Check 1st that zone is suitable for boats!
		 */

		if( (!noRiver || (neighbour.type != WZ_River)) &&
		    (!noSea || (neighbour.type != WZ_Sea) ) )
		{
			/*
			 * Friendly area adjacent to friendly area always good!
			 */

			if((neighbour.side == s) && WaterEnemyAdjacent(neighbour))
			{
				to = rid;
				break;
			}		
			else if((neighbour.side == s) && (priority < 3)) 
			{
				priority = 3;
				to = rid;
			}
			else if((priority < 2) && !neighbour.boats.totalBoats())
			{
				priority = 2;
				to = rid;
			}
			else if((priority < 1) && (neighbour.boats.totalBoats() < largest))
			{
				priority = 1;
				to = rid;
				largest = neighbour.boats.totalBoats();
			}
		}
	}

	from = rCount;

	if((from != NoWaterZone) && (to != NoWaterZone))
	{
#ifdef DEBUG
		waterLog.printf("Fleet retreating to %d", (int) to);
#endif

		// wz->boats -= flotilla;
		wz->boats.clear();
		Fleet* newFleet = new Fleet( flotilla, s );
		sendFleet ( newFleet, from, to, campaign->gameTime );
	}
	else	// Sink boats (This should not happen)
	{
#ifdef DEBUG
		waterLog.printf("Nowhere to retreat to... sinking");
#endif
		wz->boats.clear();
	}
}

Boolean WaterNetwork::WaterEnemyAdjacent( WaterZone& zone )
{ 
	char loop;

	for ( loop = 0; loop < zone.connectCount; loop++ )
	{
		WaterZoneID sid = getConnect(loop + zone.connections);

		WaterZone& szone = zones[sid];

		if((szone.side != zone.side) && szone.boats.totalBoats())
			return True;
	}

	return False;
}


void WaterNetwork::searchForBombardable()
{
	int cCount = zones.entries();
	
	if ( !cCount ) return;

	while ( cCount-- )
	{
		WaterZone& czone = zones[cCount];

		Flotilla& flotilla = czone.boats;

		if (flotilla.totalBoats() )
		{
			if ( czone.facilityCount )
			{
				int count = czone.facilityCount;
				
				FacilityListID flid = czone.facilityList;

				while( count-- )
				{
					Facility* f = getFacility( flid++ );	 
			
					if ( f->side != czone.side )
					{
						BombardPort( f, czone);
					}
				}
			}
		}
	}
}

/*
 * Bombard a port with a flotilla
 */

void WaterNetwork::BombardPort(Facility* f, WaterZone& zone)
{
	Flotilla& flotilla = zone.boats;
	Side cside = zone.side;

#ifdef DEBUG
	waterLog.printf("%s is being bombarded by %s %s",
		f->getNameNotNull(),
		sideName(cside),
		flotillaName(&flotilla));
#endif

	/*
	 * Calculate port attack/defense values
	 */

	int PortDefend = f->fortification * 10;
	int PortAttack = f->fortification;

	/*
	 * Adjust if any siege artillery
	 */

	if ( f->occupier ) 
	{
		UnitInfo ui(False, False);

		f->occupier->getInfo(ui);

		PortDefend += ( ui.regimentCount + 5 )/ 6;

		if ( ui.strengths[Artillery][Art_Siege] )
			PortAttack += (ui.strengths[Artillery][Art_Siege] +149) / 150;
	}

#ifdef DEBUG
	waterLog.printf("portAttack=%d, portDefend=%d", PortAttack, PortDefend);
#endif

	Flotilla losses;

	for (NavalType Ntype = Nav_NavalUnit; Ntype <= Nav_RiverineIronClad; INCREMENT( Ntype ) )
	{
		if(flotilla.count[Ntype])
		{
			int NavalDefend = getNavalCombat(Ntype);

#if 0
			for (int loop = 0; 
				(loop < f->fortification) && flotilla.count[NType];
				loop++ )
			{
				// int attackValue = game->gameRand( f->fortification );
				int attackValue = game->gameRand( PortAttack );

				if ( NavalDefend > attackValue )
					NavalDefend -= attackValue;
				else
					NavalDefend = 0;

				if ( NavalDefend == 0 )
				{
					flotilla.count[Ntype]--;
					losses.count[Ntype]++;

					NavalDefend = getNavalCombat(Ntype);
#ifdef DEBUG
					waterLog.printf("Boat %d sunk", (int)Ntype);
#endif
				}
			}
#else	// Simpler version

			// 1:20 chance of equal Attack:defend

			if( game->gameRand(NavalDefend * 20) <= game->gameRand(PortAttack) )
			{
				flotilla.count[Ntype]--;
				losses.count[Ntype]++;

				NavalDefend = getNavalCombat(Ntype);
#ifdef DEBUG
				waterLog.printf("Boat %d sunk", (int)Ntype);
#endif
			}
#endif

		}
	}

	/*
	 * Try and destroy fortifications
	 */

	int fortLoss = 0;

	for ( Ntype = Nav_NavalUnit; Ntype <= Nav_RiverineIronClad; INCREMENT( Ntype ) )
	{
	 	for (int loop = 0; loop < flotilla.count[ Nav_NavalUnit ]; loop++ )
		{
			int attackValue = 0;
			
			if ( getNavalBombardment( Ntype ) )
				attackValue = game->gameRand(getNavalBombardment( Ntype ) );
	
			if ( PortDefend > attackValue )
				PortDefend -= attackValue;
			else
				PortDefend = 0;

			if ( !PortDefend && f->fortification ) 
			{
				f->fortification--;
				fortLoss++;
				PortDefend = f->fortification * 5;
#ifdef DEBUG
				waterLog.printf("Fortification at %s destroyed", f->getNameNotNull());
#endif
			}
		}
	}

	// ConsultCombatChart

	enum Result { WIN = 0, DRAW, LOOSE };

	static Result comTab[][9] = {
	// <1-1	
		{ LOOSE, LOOSE, LOOSE, LOOSE, DRAW, DRAW, DRAW, DRAW, DRAW },
	// <2-1	
		{ LOOSE, LOOSE, LOOSE, DRAW, DRAW, DRAW, WIN, WIN, WIN },
	// <3-1
		{ LOOSE, LOOSE, DRAW, DRAW, WIN, WIN, WIN, WIN, WIN },
	// <4-1
		{ LOOSE, DRAW, DRAW, WIN, WIN, WIN, WIN, WIN, WIN },
	// <5-1
		{ DRAW, DRAW, WIN, WIN, WIN, WIN, WIN, WIN, WIN },
	// 5-1>
		{ DRAW, WIN, WIN, WIN, WIN, WIN, WIN, WIN, WIN }

	};

	int navalAttack = 0;
	for(Ntype = Nav_NavalUnit; Ntype <= Nav_RiverineIronClad; INCREMENT( Ntype ))
	{
		if(flotilla.count[Ntype])
			navalAttack += getNavalBombardment(Ntype) * flotilla.count[Ntype];
	}

	PortDefend = f->fortification * 10 + 1;
	int ratio = navalAttack / PortDefend;

#ifdef DEBUG
	waterLog.printf("PortDefend=%d, NavalAttack=%d, ratio=%d",
		PortDefend, navalAttack, ratio);
#endif

	if ( f->occupier ) 
	{
		UnitInfo ui(False, False);

		f->occupier->getInfo(ui);

		if ( ui.regimentCount )
			ratio /= ( ( ui.regimentCount / 12 ) + 1 );
	}

	if ( ratio > 5 )
		ratio = 5;

	MemPtr<char> buffer(200);
	buffer[0] = 0;

	switch(comTab[ratio][game->gameRand(9)])
	{
	 	case WIN:
			if(f->occupier)
			{
				withdrawUnit(f->occupier, f->location, cityBattleDistance * 2);
			}

			setCitySide(f, cside);

			sprintf(buffer, language(lge_WtakenOver), f->getNameNotNull());

			if ( f->side == SIDE_USA )
				strcat(buffer, language(lge_Wunion)); 
			else
				strcat(buffer, language(lge_Wconfed)); 


			break;

		case DRAW:
			sprintf(buffer, language(lge_Bombarded), f->getNameNotNull()); 

			if (cside == SIDE_USA )
				strcat(buffer, language(lge_Wunion) ); 
			else
				strcat(buffer, language(lge_Wconfed) ); 
			break;

		case LOOSE:
			// WaterZoneID wzid = f->waterZone;
			// WaterZone* wz = &zones[wzid];
			// wz->getRetreatZone(cside);

			sprintf(buffer, language(lge_BombardedByFort),
				language( (cside == SIDE_USA ) ? lge_Wunion : lge_Wconfed ),
				f->getNameNotNull()); 

			strcat(buffer, ", ");

			if(flotilla.totalBoats() == 0)
			{
				strcat(buffer, language(lge_AndWereSunk));
			}
			else
			{
				strcat(buffer, language(lge_AndIsWithdrawing));
			}

			// Must display message before retreating
			// otherwise the flotilla is moved, and totalBoats returns 0.

			FindRetreatZone(&zones[f->waterZone], cside);

			break;
	}

	/*
	 * Show losses
	 */

	if(losses.totalBoats())
	{
		strcat(buffer, "\r");
		putBoatName(buffer + strlen(buffer), &losses);
		strcat(buffer, " ");
		strcat(buffer, language( (losses.totalBoats() == 1) ? lge_WasSunk : lge_WereSunk));
	}

	if(fortLoss)
	{
		strcat(buffer, "\r");
		strcat(buffer, language(lge_FortDestroyed));
	}


	if(buffer[0])
	{
		const char* buttons = language(lge_OK);
		if(campaign)
			campaign->doAlert(buffer, buttons, 0, f, &zone, 0);
		else
			dialAlert(0, buffer, buttons);
	}
}

#endif	// !CAMPEDIT
