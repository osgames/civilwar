/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Measurements and Distances
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.6  1994/09/23  13:28:28  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/08/09  15:42:15  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/04/20  22:21:40  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/03/11  23:12:15  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/01/20  20:04:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/01/17  20:14:26  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "measure.h"
#include "trig.h"

#ifdef DEBUG
#include "options.h"
#endif

/*
 * Move a location towards destination at given speed
 * return True if unit arrived at destination
 */

Boolean moveLocation(Location& from, const Location& to, Speed speed, GameTicks ticks, Distance* dp)
{
#ifdef DEBUG
	if(optQuickMove())
	{
		from = to;
		return True;
	}
#endif

	if(speed == 0)
		return False;

	Location diff = to - from;

#if 0	
	Speed s = long(speed) * ticks;
	Distance d = long(s) / 256;
#else
	Distance d = (speed * ticks) / 256;
#endif

	if(dp)
		*dp = d;

	if(d >= distance(diff.x, diff.y))
	{
		from = to;
		return True;
	}
	else
	{
		if ( !d ) 
		{
			d = 10;
			*dp = d;
		}

		Wangle dir = direction(diff.x, diff.y);

		from += Location(mcos(d, dir), msin(d, dir));
		return False;
	}
}

Boolean moveLocation(Wangle dir, Location& from, const Location& to, Speed speed, GameTicks ticks, Distance* dp)
{
#ifdef DEBUG
	if(optQuickMove())
	{
		from = to;
		return True;
	}
#endif

	if(speed == 0)
		return False;

	Location diff = to - from;
	
	// Speed s = long(speed) * ticks;
	// Distance d = long(s) / 256;
	Distance d = (speed * ticks) / 256;

	if(dp)
		*dp = d;

	if(long(d) >= distance(diff.x, diff.y))
	{
		from = to;
		return True;
	}
	else
	{
		if ( !d ) 
		{
			d = 10;
			*dp = d;
		}

		from += Location(mcos(d, dir), msin(d, dir));
		return False;
	}
}


