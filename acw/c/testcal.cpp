/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	CAL Tester
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/07/11  14:26:46  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdarg.h>
#include <stdio.h>

#include "game.h"
#include "system.h"
#include "screen.h"
#include "campaign.h"
#include "cal.h"
#include "campwld.h"
#include "options.h"
#include "colours.h"
#include "text.h"

/*
 * Global Variables
 *
 * Merely defining this, automatically sets up the screen mode and mouse
 * pointer
 */

GameVariables* game;

class GameControl {
public:
	GameControl(GameType gt) { game = new GameVariables(gt); }
	~GameControl() { delete game; game = 0; }

	void play(Boolean skipMenu) { game->play(skipMenu); }
};


/*
 * Set up global variables
 */


GameVariables::GameVariables(GameType gt)
{
	quit = False;

	gameType = gt;
	playersSide = SIDE_USA;
	playerCount = 1;
	connection = 0;
}

GameVariables::~GameVariables()
{
}

/*======================================================================
 * Entry point
 */

void GameVariables::play(Boolean skipMenu)
{
	showMainMenuScreen();

	CampaignWorld* world = new CampaignWorld;
	world->init("gamedata\\dwg_wld.dat");
	doCAL(&world->ob, 0, 0);
	delete world;
}


/*
 * main function
 */


int main(int argc, char *argv[])
{
	try
	{
		/*
		 * Process command line
		 */

#ifdef DEBUG
		System::DebugMode debugMode = System::None;
		MemoryLog::Mode memMode = MemoryLog::None;
#endif

		int i = 0;
		while(++i < argc)
		{
			const char* arg = argv[i];

			if((arg[0] == '-') || (arg[0] == '/'))
			{
				switch(toupper(arg[1]))
				{
#ifdef DEBUG
				case 'M':										// -M for Memory Logging
					if(toupper(arg[2]) == 'S')				// -MS for memory summary
						memMode = MemoryLog::Summary;
					else
						memMode = MemoryLog::Full;
					break;

				case 'D':										// -D for debug log
					debugMode = System::Full;
					break;
#endif
#ifdef DEBUG
				case 'W':										// -W to create world data
					optCreateWorld = True;
					break;
				case 'S':
					optSaveWorld = True;
					break;
#endif
				}
			}
		}



#ifdef DEBUG
		machine.init(debugMode, memMode);
#else
		machine.init();
#endif

		GameControl game(CampaignGame);
		game.play(False);
	}
 	catch(GeneralError e)
 	{
		cout << "Untrapped Error: ";
		if(e.get())
			cout << e.get();
		else
			cout << "No description";
		cout << endl;
 	}
 	catch(...)
 	{
 		cout << "Caught some kind of error" << endl;
 	}

	return 0;
}


/*
 * Miscellaneous stuff to keep linked happy
 */

MapObject::MapObject()
{
}

City::City()
{
}

/*
 * Quick bodge to display a simple text box
 */

void GameVariables::simpleMessage(const char* fmt, ...)
{
	Region bm(machine.screen->getImage(), Rect(32,2,480,28));
	bm.fill(White);
	bm.frame(0,0,480,28, DGrey,LGrey);
	if(fmt)
	{
		char buffer[500];

		va_list vaList;
		va_start(vaList, fmt);
		vsprintf(buffer, fmt, vaList);
		va_end(vaList);

		TextWindow win(&bm, Font_EMMA14);
		win.setFormat(True, True);
		win.setColours(Black);
		win.draw(buffer);
	}
	machine.screen->setUpdate(bm);
}



