/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Reading of game datafiles
 *
 * Split up from the rather large datafile.cpp
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.5  1994/07/28  18:57:04  Steven_Green
 * Added regiment count to city.
 * world->update is correctly invoked after reading the world data
 *
 * Revision 1.4  1994/07/25  20:32:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/07/19  19:53:03  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/07/11  14:26:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/06/24  14:43:30  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <fstream.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

#include "datafile.h"
#include "campwld.h"
#include "generals.h"
#include "error.h"
#include "strutil.h"

#include "language.h"

#ifdef TESTING
#include "dialogue.h"
#endif
#ifdef DEBUG
#include "log.h"
#endif


// #define MOVE_ZONES_UP		// Move water zones up on map.
#ifdef MOVE_ZONES_UP
#include "mapwind.h"
#endif

/*
 * Read the global world datafile
 *
 * CampaignWorld has already been set up with default values of no
 * cities, no rail links, etc.
 */


const int FileLineLength = 200;

#ifdef USE_STREAMS
class DataFileInStream : public ifstream {
#else
class DataFileInStream {
	FILE* fp;
#endif

	friend void readWorld(const char* fileName, CampaignWorld* world);

	CampaignWorld* world;
	char buffer[FileLineLength];
	char* currentPos;

#ifdef TESTING
public:
	char popupBuffer[400];
	PopupText* popup;
#endif

public:
#ifdef USE_STREAMS
	DataFileInStream(const char* name, CampaignWorld* w) :
		ifstream( getPathAndCheck( name ) )
#else
	DataFileInStream(const char* name, CampaignWorld* w)
#endif
	{
#ifndef USE_STREAMS
		fp = FileOpen(name, "r");
		// fp = fopen(name, "r");
#endif

		world = w;
		currentPos = 0;		// Not parsing yet
		buffer[0] = 0;			// Nothing read in yet
#ifdef TESTING
		sprintf(popupBuffer, "Reading %s", name);
		popup = new PopupText(popupBuffer);
#endif
	}

#ifdef USE_STREAMS
#ifdef TESTING
	~DataFileInStream()
	{
		delete popup;
	}
#endif
#else
	~DataFileInStream()
	{
		if(fp)
			fclose(fp);

#ifdef TESTING
		delete popup;
#endif
	}
#endif

#ifndef USE_STREAMS
	int error()
	{
		if(!fp)
			return -1;
		else if(feof(fp))
			return -1;
		else
			return ferror(fp);
	}
#endif

	KeyWords getKeyword();	// Return next keyword
	char* getToken();			// Get next item from buffer (0 if nothing)
	void newChunk();

	void readLine();

	Boolean readValue(long& val);
	Boolean readValue(unsigned long& val);
	Boolean readValue(int& val);
	Boolean readValue(UWORD& val);
	Boolean readValue(UBYTE& val);
	KeyWords readKeyword();
	char* readString();

	void readStates();
	void readFacilities();
	void readWaterNetwork();

	void readOB();
private:
#ifdef CAMPEDIT
	void readFacility(Facility* f);
	void readCity(City* f);
#else
	void readFacility(Facility* f, RailSectionID& nextRail);
	// void readNamedFacility(NamedFacility* f, RailSectionID& nextRail);
	void readCity(City* f, RailSectionID& nextRail);
#endif
	void readResources(ResourceSet& r);
	void readLocation(Location& l);
	void readSide(Side& side);
	void readState(State& state);
	General*   readGeneral(Side side);
	President* readPresident();
	Army* 	  readArmy(Side side);
	Corps* 	  readCorps(Side side);
	Division*  readDivision(Side side);
	Brigade*   readBrigade(Side side);
	Regiment*  readRegiment(Side side);
	void 		  readUnit(Unit* unit, Side side);
	void 		  readOrders(Order& order);
	void 		  readSentOrder(SentOrder& order);

	void readWaterZone(WaterZone& zone, FacilityListID& facility, WaterZoneListID& nextLink);
};

void DataFileInStream::readLine()
{
	currentPos = 0;

	do
	{
#ifdef USE_STREAMS
		getline(buffer, FileLineLength);
#else
		fgets(buffer, FileLineLength, fp);
#endif

		/*
		 * Remove comments and new lines
		 */

		char* s = buffer;

		do
		{
			s = strpbrk(s, ";\n\r\"");

			if(s)
			{
				if(*s == quote)
				{
					s = strchr(s + 1, quote);

					if(s)
						s++;
					else
						throw GeneralError("Missing closing quote");
				}
				else
					*s = 0;
			}
		} while(s && *s);
#ifdef USE_STREAMS
	} while(ios::good() && !buffer[0]);
#else
	} while(!error() && !buffer[0]);
#endif
}

char* DataFileInStream::getToken()
{
	if(currentPos == 0)
		currentPos = buffer;

	/*
	 * Skip any white space
	 */

	char* s = currentPos;

	while(*s && isspace(*s))
		s++;
	currentPos = s;

	if(*s == 0)			// End of line
		return 0;
	if(*s == quote)	// Quote is special case... advance to closing quote
	{
		s++;					// Skip quote
		char* token = s;	// Set the return value

		while(*s && (*s != quote))
			s++;

		if(*s == quote)		// end of line
			*s++ = 0;			// null terminate
		currentPos = s;

		return token;
	}
	else					// Skip to next space
	{
		char* token = s;

		while(*s && !isspace(*s))
			s++;

		if(*s)
			*s++ = 0;
		currentPos = s;

		return token;
	}

}

KeyWords DataFileInStream::readKeyword()
{
	char* s = getToken();

	if(s)
		return keys.find(s);
	else
		return KW_Last;
}

Boolean DataFileInStream::readValue(int& val)
{
	char* s = getToken();

	if(s && (isdigit(s[0]) || (s[0] == '-')))
	{
		val = atoi(s);
		return True;
	}

	return False;
}

Boolean DataFileInStream::readValue(long& val)
{
	char* s = getToken();

	if(s && (isdigit(s[0]) || (s[0] == '-')))
	{
		val = atol(s);
		return True;
	}

	return False;
}

Boolean DataFileInStream::readValue(unsigned long& val)
{
	char* s = getToken();

	if(s && isdigit(s[0]))
	{
		val = atol(s);
		return True;
	}

	return False;
}


Boolean DataFileInStream::readValue(UWORD& val)
{
	char* s = getToken();

	if(s && isdigit(s[0]))
	{
		val = UWORD(atoi(s));
		return True;
	}

	return False;
}

Boolean DataFileInStream::readValue(UBYTE& val)
{
	char* s = getToken();

	if(s && isdigit(s[0]))
	{
		val = UBYTE(atoi(s));
		return True;
	}

	return False;
}

char* DataFileInStream::readString()
{
	return getToken();
}

/*
 * Get the next keyword from stream
 */

KeyWords DataFileInStream::getKeyword()
{
#ifdef USE_STREAMS
	while(ios::good())
#else
	while(!error())
#endif
	{
		readLine();

		char* s = getToken();

		if(*s == startChunk)
		{
			/*
			 * Skip through file until matching endchunk
			 */

			int braceCount = 1;

#ifdef USE_STREAMS
			while(ios::good() && braceCount)
#else
			while(fp && !ferror(fp) && braceCount)
#endif
			{
				readLine();
				s = getToken();
				if(*s == startChunk)
					braceCount++;
				else if(*s == endChunk)
					braceCount--;
			}

			s = 0;
		}
		else if(*s == endChunk)
			return KW_Last;

		if(s)
			return keys.find(s);
	}

#ifdef USE_STREAMS
	if(ios::eof())
#else
	if(fp && feof(fp))
#endif
		return KW_EndFile;
	else
		return KW_Error;
}

/*
 * Skip over opening brace
 *
 * It ought really to check that the line really does contain a '{'
 */

void DataFileInStream::newChunk()
{
	char* s;

	do
	{
		readLine();

		s = getToken();
#ifdef USE_STREAMS
	} while(ios::good() && !s);
#else
	} while(!error() && !s);
#endif

	if(!s || (s[0] != startChunk))
		throw GeneralError("Expecting opening brace in file");
}

/*
 * Read in the world data file
 */

void readWorld(const char* fileName, CampaignWorld* world)
{
	keys.create(keywords, KW_Last);

	DataFileInStream stream(fileName, world);

#ifdef USE_STREAMS
	if(stream)
#else
	if(!stream.error())
#endif
	{
		Boolean endFile = False;

		while(!endFile)
		{
			switch(stream.getKeyword())
			{
			case KW_WorldFile:	// This is what we're trying to read!
				{
					UWORD version;
					stream.readValue(version);

					Boolean finished = False;

					stream.newChunk();		// Tell stream that we are expecting a new chunk

					while(!finished)
					{
						switch(stream.getKeyword())
						{
						case KW_StateSection:
#ifdef TESTING
							strcat(stream.popupBuffer, "\rStates");
							stream.popup->showText(stream.popupBuffer);
#endif
							stream.readStates();
							break;

						case KW_FacilitySection:
#ifdef TESTING
							strcat(stream.popupBuffer, "\rFacilities");
							stream.popup->showText(stream.popupBuffer);
#endif
							stream.readFacilities();
							break;

						case KW_WaterNetwork:
#ifdef TESTING
							strcat(stream.popupBuffer, "\rWater Network");
							stream.popup->showText(stream.popupBuffer);
#endif
							stream.readWaterNetwork();
							break;

						case KW_OBSection:
#ifdef TESTING
							strcat(stream.popupBuffer, "\rOrder of Battle");
							stream.popup->showText(stream.popupBuffer);
#endif
							stream.readOB();
							break;

						case KW_NotFound:		// Ignore it!
						default:
#ifdef DEBUG
							if(logFile)
								*logFile << "Unknown keyword in DWG_WLD file" << 
									stream.buffer << endl;
#endif
							break;

						case KW_Last:			// Finished
						case KW_Error:			// Quit!
						case KW_EndFile:
							finished = True;
							break;
						}
					}
				}
				break;

			case KW_NotFound:		// Ignore it!
			default:
#ifdef DEBUG
				if(logFile)
					*logFile << "Unknown keyword in file" << stream.buffer << endl;
#endif
				break;

			case KW_Last:			// Finished
			case KW_EndFile:
				endFile = True;
				break;

			case KW_Error:			// Quit!
				throw GeneralError("Error reading dwg_wld.dat");
			}
		}
	}

	world->setUp();
}


void DataFileInStream::readStates()
{
	StateID stateCount = 0;

	if(readValue(stateCount))
	{
		world->states.init(stateCount);
		StateID nextState = 0;

		Boolean finished = False;

		newChunk();

		while(!finished)
		{
			switch(getKeyword())
			{
			case KW_State:
				readState(world->states.get(nextState++));
				break;


			case KW_NotFound:		// Ignore it!
			default:
#ifdef DEBUG
				if(logFile)
					*logFile << "Unknown keyword in States section of file" << buffer << endl;
#endif
				break;

			case KW_Last:			// Finished
			case KW_Error:			// Quit!
			case KW_EndFile:
				finished = True;
				break;
			}
		}
	}
}

void DataFileInStream::readState(State& state)
{
	Boolean finished = False;

	newChunk();

	while(!finished)
	{
		switch(getKeyword())
		{
		case KW_Location:
			readLocation(state.location);
			break;

		case KW_Name:
			{
				char* name = getToken();
				if(name)
#ifdef CAMPEDIT
					if(state.fullName[0])
						strcpy(state.shortName, name);
					else
						strcpy(state.fullName, name);
#else
					if(state.fullName)
						state.shortName = copyString(name);
					else
						state.fullName = copyString(name);
#endif
			}
			break;

		case KW_Side:
			readSide(state.side);
			break;

		case KW_Regiment:
			readValue(state.regimentCount);
			break;

		case KW_NotFound:		// Ignore it!
		default:
#ifdef DEBUG
			if(logFile)
				*logFile << "Unknown keyword in SeaZone section of file" << buffer << endl;
#endif
			break;

		case KW_Last:			// Finished
		case KW_Error:			// Quit!
		case KW_EndFile:
			finished = True;
			break;
		}
	}
}

void DataFileInStream::readFacilities()
{
	FacilityID facilityCount = 0;
	RailSectionID railSectionCount = 0;

	readValue(facilityCount);
	readValue(railSectionCount);

	if(facilityCount)
		world->facilities.init(facilityCount);

#ifndef CAMPEDIT
	if(railSectionCount)
		world->railways.init(railSectionCount);
#endif

	FacilityID nextFacility = 0;

#ifndef CAMPEDIT
	RailSectionID nextRailSection = 0;
#endif

	Boolean finished = False;

	newChunk();

	while(!finished)
	{
		switch(getKeyword())
		{
		case KW_Facility:
			{
				Facility* f = new Facility();
				// world->facilities.set(nextFacility++, f);
				world->facilities[nextFacility++] = f;
#ifndef CAMPEDIT
				readFacility(f, nextRailSection);
#else
				readFacility(f);
#endif
			}
			break;

		case KW_City:
			{
				City* f = new City;
				// world->facilities.set(nextFacility++, f);
				world->facilities[nextFacility++] = f;
				f->facilities |= Facility::F_City;
#ifndef CAMPEDIT
				readCity(f, nextRailSection);
#else
				readCity(f);
#endif
			}
			break;

		case KW_NotFound:		// Ignore it!
		default:
#ifdef DEBUG
			if(logFile)
				*logFile << "Unknown keyword in Facilities section of file" << buffer << endl;
#endif
			break;

		case KW_Last:			// Finished
		case KW_Error:			// Quit!
		case KW_EndFile:
			finished = True;
			break;
		}
	}

#ifdef TESTING
	if(nextFacility != facilityCount)
		throw GeneralError("Wrong number of facilities read");
#ifndef CAMPEDIT
	if(nextRailSection != railSectionCount)
		throw GeneralError("Wrong number of rail sections read");
#endif

#endif
}

#ifdef CAMPEDIT
void DataFileInStream::readFacility(Facility* f)
#else
void DataFileInStream::readFacility(Facility* f, RailSectionID& nextRail)
#endif
{
	newChunk();

	Boolean finished = False;

	while(!finished)
	{
		switch(getKeyword())
		{
		case KW_State:
			readValue(f->state);
			break;

		/*
		 * This will disapear
		 */

		case KW_Flags:
			readValue(f->facilities);
			break;

		case KW_Depot:
			f->facilities |= Facility::F_SupplyDepot;
			break;
		case KW_RailEngineer:
			f->facilities |= Facility::F_RailEngineer;
			break;


		case KW_Side:
			Side side;
			readSide(side);
			f->side = side;
			break;

		case KW_Location:
			readLocation(f->location);
			break;

		case KW_Fortification:
			{
				UBYTE fort;
				if(readValue(fort))
					f->fortification = fort;
			}
			break;

#if 0
		case KW_Wagons:
			{
				UBYTE wagons;
				if(readValue(wagons))
					f->wagons = wagons;
			}
			break;
#endif

		case KW_Supply:
#if 0
			readValue(f->supplies.food);
			readValue(f->supplies.forage);
			readValue(f->supplies.ammo);
#else
			readValue(f->supplies);
#endif
			break;

		case KW_RailCapacity:
			{
				UBYTE cap;
				if(readValue(cap))
				{
					f->railCapacity = cap;

					if(readValue(cap))
						f->freeRailCapacity = cap;
					else
						f->freeRailCapacity = f->railCapacity;
				}
			}
			break;

		case KW_RailLink:
#ifdef CAMPEDIT
			FacilityID link;
			while(readValue(link))
			{
				// FacilityID* fPtr = f->connections.add();
				// *fPtr = link;
				f->connections.add(link);
			}
#else
			f->railSection = nextRail;
			// RailSectionID link;
			FacilityID link;

			while(readValue(link))
			{
				f->railCount++;

				world->railways.set(nextRail++, link);
			}
#endif
			break;

		case KW_WaterCapacity:
			{
				UBYTE cap;
				if(readValue(cap))
				{
					f->waterCapacity = cap;

					if(readValue(cap))
						f->freeWaterCapacity = cap;
					else
						f->freeWaterCapacity = f->waterCapacity;
				}
			}
			break;

		case KW_Blockade:
			{
				UBYTE block;

				if(readValue(block))
					f->blockadeRunners = block;
			}
			break;

		case KW_Victory:
			{
				UBYTE victory;

				if(readValue(victory))
					f->victory = victory;
			}
			break;

		case KW_NotFound:		// Ignore it!
		default:
#ifdef DEBUG
			if(logFile)
				*logFile << "Unknown keyword in Facility section of file" << buffer << endl;
#endif
			break;

		case KW_Name:
			{
				char* name = getToken();
				if(name)
#ifdef CAMPEDIT
					strcpy(f->name, name);
#else
					f->name = copyString(name);
#endif
			}
			break;

		case KW_KeyCity:
			readValue(f->owner);
			break;

		case KW_Last:			// Finished
		case KW_Error:			// Quit!
		case KW_EndFile:
			finished = True;
			break;
		}
	}
}


#ifdef CAMPEDIT
void DataFileInStream::readCity(City* f)
#else
void DataFileInStream::readCity(City* f, RailSectionID& nextRail)
#endif
{
	newChunk();

	Boolean finished = False;

	while(!finished)
	{
		switch(getKeyword())
		{
		case KW_Type:
			{
				CityType t;
				readValue(t);
				f->type = t;
			}
			break;

		case KW_Size:
			{
				CitySize sz;
				readValue(sz);
				f->size = sz;
			}
			break;

		case KW_Resources:
			readResources(f->resource);
			break;

		case KW_Facility:
#ifdef CAMPEDIT
			readFacility(f);
#else
			readFacility(f, nextRail);
#endif
			break;

		case KW_Recruit:
			f->facilities |= Facility::F_RecruitmentCentre;
			break;
		case KW_Training:
			f->facilities |= Facility::F_TrainingCamp;
			break;
		case KW_Hospital:
			f->facilities |= Facility::F_Hospital;
			break;
		case KW_POW:
			f->facilities |= Facility::F_POW;
			break;
		case KW_Capital:
			f->facilities |= Facility::F_KeyCity;
			break;

		case KW_NotFound:		// Ignore it!
		default:
#ifdef DEBUG
			if(logFile)
				*logFile << "Unknown keyword in City section of file" << buffer << endl;
#endif
			break;

		case KW_Last:			// Finished
		case KW_Error:			// Quit!
		case KW_EndFile:
			finished = True;
			break;
		}
	}
}

/*================================================================
 * Water Network reading
 */

void DataFileInStream::readWaterNetwork()
{
	WaterNetwork& waterNet = world->waterNet;

	WaterZoneID zoneCount;
	FacilityListID facilityListCount = 0;
	WaterZoneListID connectListCount = 0;

	readValue(zoneCount);
	readValue(facilityListCount);
	readValue(connectListCount);

	if(zoneCount)
		waterNet.zones.init(zoneCount);

#ifndef CAMPEDIT
	if(facilityListCount)
		waterNet.facilityList.init(facilityListCount);
	if(connectListCount)
		waterNet.connectList.init(connectListCount);
#endif

	/*
	 * Set values for where to place next item
	 */

	facilityListCount = 0;
	connectListCount = 0;
	zoneCount = 0;

 	Boolean finished = False;

 	newChunk();

 	while(!finished)
 	{
 		switch(getKeyword())
 		{
		case KW_WaterZone:
			readWaterZone(waterNet.zones[zoneCount++], facilityListCount, connectListCount);
			break;
 		case KW_NotFound:		// Ignore it!
 		default:
#ifdef DEBUG
 			if(logFile)
 				*logFile << "Unknown keyword in WaterNetwork section of file" << buffer << endl;
#endif
			break;

		case KW_Last:			// Finished
		case KW_Error:			// Quit!
		case KW_EndFile:
			finished = True;
			break;
		}
	}
}


void DataFileInStream::readWaterZone(WaterZone& zone, FacilityListID& facility, WaterZoneListID& nextLink)
{
	Boolean finished = False;

	newChunk();

	while(!finished)
	{
		KeyWords key = getKeyword();

		if( (key >= KW_NavalUnit) && (key <= KW_Monitor) )
		{
	 		NavalType type = NavalType(key - KW_NavalUnit);
			UBYTE howMany;
			if(readValue(howMany))
				zone.boats.count[type] = howMany;
		}
		else
		switch(key)
		{
		case KW_Location:		// river chunk!
			readLocation(zone.location);
#ifdef MOVE_ZONES_UP
			zone.location.y -= MapSectionHeight;
#endif
			break;

		case KW_Coast:
			zone.type = WZ_Coast;
			break;

		case KW_River:
			zone.type = WZ_River;
			break;

		case KW_Sea:
			zone.type = WZ_Sea;
			break;

		case KW_Facilities:
#ifdef CAMPEDIT
			FacilityID fid;
			while(readValue(fid))
			{
				zone.facilityList.add(fid);
			}
#else
			zone.facilityList = facility;	// Entry point in list
			
			FacilityID fid;
			while(readValue(fid))
			{
				zone.facilityCount++;
				world->waterNet.facilityList.set(facility++, fid);
			}
#endif
			break;

		case KW_WaterLink:
#ifdef CAMPEDIT
			WaterZoneID zid;

			while(readValue(zid))
				zone.connections.add(zid);
#else
			zone.connections = nextLink;
			WaterZoneID zid;
			while(readValue(zid))
			{
				zone.connectCount++;
				world->waterNet.connectList[nextLink++] = zid;
			}
#endif
			break;

		case KW_Side:
			readSide(zone.side);
			break;

		case KW_NotFound:		// Ignore it!
		default:
#ifdef DEBUG
			if(logFile)
				*logFile << "Unknown keyword in RiverZone section of file" << buffer << endl;
#endif
			break;

		case KW_Last:			// Finished
		case KW_Error:			// Quit!
		case KW_EndFile:
			finished = True;
			break;
		}
	}
}


void DataFileInStream::readResources(ResourceSet& r)
{
	for(int i = 0; i < ResourceCount; i++)
		readValue(r.values[i]);
}

void DataFileInStream::readLocation(Location& l)
{
	readValue(l.x);
	readValue(l.y);
}


void DataFileInStream::readOB()
{
	newChunk();

	Boolean finished = False;

	while(!finished)
	{
		switch(getKeyword())
		{

		case KW_President:		// One side
			world->ob.addSide(readPresident());
			break;

		case KW_NotFound:		// Ignore it!
		default:
#ifdef DEBUG
			if(logFile)
				*logFile << "Unknown keyword in OB section of file" << buffer << endl;
#endif
			break;

		case KW_Last:			// Finished
		case KW_Error:			// Quit!
		case KW_EndFile:
			finished = True;
			break;
		}
	}

	world->ob.setSides();

}

/*
 * Read general and add him to world general list.
 * Return the general so that caller can allocate him to unit
 */

General* DataFileInStream::readGeneral(Side side)
{
	General* g = new General();

	g->side = side;

	Boolean finished = False;

	newChunk();

	while(!finished)
	{
		switch(getKeyword())
		{
		case KW_Name:
			{
				char* name = getToken();
				if(name)
					g->name = copyString(name);
			}
			break;

		case KW_Rank:
			Rank rank;

			if(readValue(rank))
				g->rank = rank;
			break;

		case KW_Efficiency:
			readValue(g->efficiency);
			break;

		case KW_Ability:
			readValue(g->ability);
			break;

		case KW_Aggression:
			readValue(g->aggression);
			break;

		case KW_Experience:
			readValue(g->experience);
			break;

		case KW_NotFound:		// Ignore it!
		default:
#ifdef DEBUG
			if(logFile)
				*logFile << "Unknown keyword in General section of file" << buffer << endl;
#endif
			break;

		case KW_Last:			// Finished
			finished = True;
			break;

		case KW_Error:			// Quit!
		case KW_EndFile:
			finished = True;
			break;
		}
	}

	// world->ob.addGeneral(g);

	return g;
}

President* DataFileInStream::readPresident()
{
	President* p = new President;

	Boolean finished = False;

	newChunk();

	while(!finished)
	{
		switch(getKeyword())
		{
		case KW_General:			// Free General
			p->addFreeGeneral(readGeneral(p->side));
			break;

		case KW_Regiment:			// Free Regiment
			p->addFreeRegiment(readRegiment(p->side));
			break;

		case KW_Side:
			readSide(p->side);
			break;

		case KW_Army:
			p->addChild(readArmy(p->side));
			break;

		case KW_Unit:
			readUnit(p, p->side);
			break;

		case KW_NotFound:		// Ignore it!
		default:
#ifdef DEBUG
			if(logFile)
				*logFile << "Unknown keyword in President section of file" << buffer << endl;
#endif
			break;

		case KW_Last:			// Finished
			finished = True;
			break;

		case KW_Error:			// Quit!
		case KW_EndFile:
			finished = True;
			break;
		}
	}

	return p;
}

Army* DataFileInStream::readArmy(Side side)
{
	Army* a = new Army;

	Boolean finished = False;

	newChunk();

	while(!finished)
	{
		switch(getKeyword())
		{
		case KW_Corps:
			a->addChild(readCorps(side));
			break;

		case KW_Unit:
			readUnit(a, side);
			break;

		case KW_Name:
			{
				char* name = getToken();
				if(name)
					a->name = copyString(name);
#ifdef TESTING
				strcat(popupBuffer, "\r");
				strcat(popupBuffer, name);
				popup->showText(popupBuffer);
#endif
			}
			break;

		case KW_NotFound:		// Ignore it!
		default:
#ifdef DEBUG
			if(logFile)
				*logFile << "Unknown keyword in Army section of file" << buffer << endl;
#endif
			break;

		case KW_Last:			// Finished
			finished = True;
			break;

		case KW_Error:			// Quit!
		case KW_EndFile:
			finished = True;
			break;
		}
	}

	return a;
}


Corps* DataFileInStream::readCorps(Side side)
{
	Corps* c = new Corps;

	Boolean finished = False;

	newChunk();

	while(!finished)
	{
		switch(getKeyword())
		{
		case KW_Division:
			c->addChild(readDivision(side));
			break;

		case KW_Unit:
			readUnit(c, side);
			break;

		case KW_NotFound:		// Ignore it!
		default:
#ifdef DEBUG
			if(logFile)
				*logFile << "Unknown keyword in Corps section of file" << buffer << endl;
#endif
			break;

		case KW_Last:			// Finished
			finished = True;
			break;

		case KW_Error:			// Quit!
		case KW_EndFile:
			finished = True;
			break;
		}
	}

	return c;
}


Division* DataFileInStream::readDivision(Side side)
{
	Division* d = new Division;

	Boolean finished = False;

	newChunk();

	while(!finished)
	{
		switch(getKeyword())
		{
		case KW_Brigade:
			d->addChild(readBrigade(side));
			break;

		case KW_Unit:
			readUnit(d, side);
			break;

		case KW_NotFound:		// Ignore it!
		default:
#ifdef DEBUG
			if(logFile)
				*logFile << "Unknown keyword in Division section of file" << buffer << endl;
#endif
			break;

		case KW_Last:			// Finished
			finished = True;
			break;

		case KW_Error:			// Quit!
		case KW_EndFile:
			finished = True;
			break;
		}
	}

	return d;
}


Brigade* DataFileInStream::readBrigade(Side side)
{
	Brigade* b = new Brigade;

	Boolean finished = False;

	newChunk();

	while(!finished)
	{
		switch(getKeyword())
		{
		case KW_Regiment:
			b->addChild(readRegiment(side));
			break;

		case KW_Unit:
			readUnit(b, side);
			break;

		case KW_NotFound:		// Ignore it!
		default:
#ifdef DEBUG
			if(logFile)
				*logFile << "Unknown keyword in Brigade section of file" << buffer << endl;
#endif
			break;

		case KW_Last:			// Finished
			finished = True;
			break;

		case KW_Error:			// Quit!
		case KW_EndFile:
			finished = True;
			break;
		}
	}

	return b;
}


Regiment* DataFileInStream::readRegiment(Side side)
{
	Regiment* r = new Regiment;

	Boolean finished = False;

	newChunk();

	while(!finished)
	{
		switch(getKeyword())
		{
		case KW_Name:
			{
				char* name = getToken();
				if(name)
					r->name = copyString(name);
			}
			break;

		case KW_Type:
			{
				BasicRegimentType type;
				RegimentSubType sType;

				if(readValue(type))
				{
					r->type.basicType = type;

					if(readValue(sType))
						r->type.subType = sType;
				}
			}
			break;

		case KW_Strength:
			readValue(r->strength);
			break;

		case KW_Stragglers:
			readValue(r->stragglers);
			break;


		case KW_Casualties:
			readValue(r->casualties);
			break;

		case KW_Fatigue:
			readValue(r->fatigue);
			break;

		case KW_Morale:
			readValue(r->morale);
			break;

#if 0
		case KW_Speed:
			readValue(r->speed);
			break;
#endif

		case KW_Supply:
#if 0
			readValue(r->supply.food);
			readValue(r->supply.forage);
			readValue(r->supply.ammo);
#else
			readValue(r->supply);
			// Bodge
			// r->supply = game->gameRand(150, 255);

#endif
			break;

		case KW_Experience:
			readValue(r->experience);
			// Bodge
			// r->experience = game->gameRand();
			break;

		case KW_Unit:
			readUnit(r, side);
			break;

		case KW_NotFound:		// Ignore it!
		default:
#ifdef DEBUG
			if(logFile)
				*logFile << "Unknown keyword in Regiment section of file" << buffer << endl;
#endif
			break;

		case KW_Last:			// Finished
			finished = True;
			break;

		case KW_Error:			// Quit!
		case KW_EndFile:
			finished = True;
			break;
		}
	}


#ifdef TESTING
	// Bodge

	if( (r->type.basicType == Artillery) && (r->strength > 150))
	{
#ifdef CAMPEDIT
		char buffer[500];
		sprintf(buffer, "Warning\r%s has %ld men\rReducing to 150",
			r->getName(True),
			r->strength);
		dialAlert(0, buffer, "OK");
#endif
		r->strength = 150;
	}


#endif

	return r;
}

void DataFileInStream::readUnit(Unit* unit, Side side)
{
	Boolean finished = False;

	newChunk();

	while(!finished)
	{
		switch(getKeyword())
		{
		case KW_Order:
			unit->makeCampaignIndependant(True);
			unit->makeCampaignReallyIndependant(True);
			readOrders(unit->realOrders);
			unit->givenOrders = unit->realOrders;
			break;

#if 0
		case KW_MoveTime:
			if(readValue(unit->lastMovedTime.value))
				readValue(unit->lastMovedTime.ticks);
			break;
#endif

		case KW_Location:
			readLocation(unit->location);
			unit->destination = unit->location;
			break;

		case KW_General:
			{
				General* g = readGeneral(side);
				unit->general = g;
				g->allocation = unit;
			}
			break;

		case KW_OrderList:
			{
				SentOrder* newOrder = new SentOrder;

				readSentOrder(*newOrder);

				unit->orderList.append(newOrder);
			}
			break;

		case KW_Occupying:
			{
				FacilityID id;
				if(readValue(id))
				{
					unit->occupying = world->facilities[id];
					unit->siegeAction =  SIEGE_Occupying;
				}
			}
			break;

		case KW_Sieging:
			{
				FacilityID id;
				if(readValue(id))
				{
					unit->occupying = world->facilities[id];
					unit->siegeAction =  SIEGE_Besieging;
				}
			}
			break;

		case KW_NotFound:		// Ignore it!
		default:
#ifdef DEBUG
			if(logFile)
				*logFile << "Unknown keyword in Unit section of file" << buffer << endl;
#endif
			break;

		case KW_Last:			// Finished
			finished = True;
			break;

		case KW_Error:			// Quit!
		case KW_EndFile:
			finished = True;
			break;
		}
	}

	// Bodge the orders to hold where they are

	unit->realOrders = Order(ORDER_Stand, ORDER_Land, unit->location);
	unit->givenOrders = unit->realOrders;
}

void DataFileInStream::readSentOrder(SentOrder& order)
{
	Boolean finished = False;

	newChunk();

	while(!finished)
	{
		switch(getKeyword())
		{
		case KW_Time:
#if 0
			GameTimeVal val;
#elif 0
			GameTicks val;
			if(readValue(val))
				order.time = val;
#else
			GameTicks val;
			Day day;

			if(readValue(day))
			{
				if(readValue(val))
					order.time = TimeBase(day, val);
			}
#endif
			break;

		case KW_Order:
			readOrders(order);
			break;


		case KW_NotFound:		// Ignore it!
		default:
#ifdef DEBUG
			if(logFile)
				*logFile << "Unknown keyword in SentOrder section of file" << buffer << endl;
#endif
			break;

		case KW_Last:			// Finished
			finished = True;
			break;

		case KW_Error:			// Quit!
		case KW_EndFile:
			finished = True;
			break;
		}
	}
}

void DataFileInStream::readOrders(Order& order)
{
	Boolean finished = False;


	KeyWords key = readKeyword();
	while(key != KW_Last)
	{
		if( (key >= KW_OrderHow) && (key < (KW_OrderHow + ORDER_MaxHow)))
			order.how = OrderHow(key - KW_OrderHow);
		else if( (key >= KW_OrderMode) && (key < (KW_OrderMode + ORDER_Max)))
			order.mode = OrderMode(key - KW_OrderMode);
#ifdef DEBUG
		else
			if(logFile)
				*logFile << "Unknown keyword in Order section of file" << buffer << endl;
#endif

		key = readKeyword();
	}



	newChunk();

	while(!finished)
	{
		switch(getKeyword())
		{
		case KW_Location:
			readLocation(order.destination);
			break;

		case KW_NotFound:		// Ignore it!
		default:
#ifdef DEBUG
			if(logFile)
				*logFile << "Unknown keyword in Order section of file" << buffer << endl;
#endif
			break;

		case KW_Last:			// Finished
			finished = True;
			break;

		case KW_Error:			// Quit!
		case KW_EndFile:
			finished = True;
			break;
		}
	}
}


void DataFileInStream::readSide(Side& side)
{
	side = 0;

	KeyWords key = readKeyword();
	while(key != KW_Last)
	{
		if(key == KW_USA)
			side |= SIDE_USA;
		if(key == KW_CSA)
			side |= SIDE_CSA;
		key = readKeyword();
	}
}























