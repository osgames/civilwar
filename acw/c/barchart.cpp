/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Bar Chart drawing
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "barchart.h"
#include "generals.h"
#include "colours.h"
#include "text.h"

/*
 * Strength bar chart
 * Displayed as a full length bar using:
 *		green (current men)
 *		yellow (stragglers)
 *		red (casualties)
 */

void showStrengthBar(Region* bm, const UnitInfo* info, Rect r)
{
	Strength totalStrength = info->strength + info->stragglers + info->casualties;

	if(totalStrength)
	{
		// Draw current Strength

		SDimension strengthBarLength = r.getW();

		SDimension x = r.getX();		// Left edge
		SDimension w = SDimension((info->strength * strengthBarLength) / totalStrength);
		if(w)
		{
			bm->box(x, r.getY(), w, r.getH(), Green);
			x += w;
		}
		w = SDimension((info->stragglers * strengthBarLength) / totalStrength);
 		if(w)
 		{
 			bm->box(x, r.getY(), w, r.getH(), Gold5);
			x += w;
		}
		w = SDimension((info->casualties * strengthBarLength) / totalStrength);
		if(w)	// w could be calculated as barlength-x
		{
			bm->box(x, r.getY(), w, r.getH(), Red);
		}
	}
	else
		bm->box(r, DGrey);

	/*
	 * Display strength as a number
	 */

	Region bm1 = *bm;
	TextWindow textWind(&bm1, Font_JAME_5);
	bm1.setClip(bm->getX() + r.getX(), bm->getY() + r.getY(), r.getW(), r.getH());
	textWind.setFormat(TW_CV);		// Centre vertically
	textWind.setPosition(Point(1, 0));	// Leave a pixel to the left
	textWind.setColours(White);
	textWind.wprintf("%ld", (long) totalStrength);

}

void showBar(Region* bm, int val, int maxVal, Rect r, const char* text)
{
	/*
	 * Fill area in grey
	 */

	bm->box(r.getX(), r.getY(), r.getW(), r.getH(), DGrey);

	/*
	 * Add graduations
	 */

	bm->VLine(r.getX() + r.getW() / 4, 			r.getY(), r.getH(), LGrey);
	bm->VLine(r.getX() + r.getW() / 2, 			r.getY(), r.getH(), LGrey);
	bm->VLine(r.getX() + (r.getW() * 3) / 4, 	r.getY(), r.getH(), LGrey);

	/*
	 * Put the filled in area on
	 */

	SDimension w = (val * (r.getW() - 1)) / maxVal;
	if(w)
		bm->box(r.getX(), r.getY() + 1, w, r.getH() - 2, Green);

	/*
	 * Overlay Text
	 */

	if(text)
	{
		Region bm1 = *bm;
		TextWindow textWind(&bm1, Font_JAME_5);
		bm1.setClip(bm->getX() + r.getX(), bm->getY() + r.getY(), r.getW(), r.getH());
		textWind.setFormat(TW_CV);		// Centre vertically
		textWind.setPosition(Point(1, 0));	// Leave a pixel to the left
		textWind.setColours(White);
		textWind.draw(text);
	}
}

#if 0
void showBarTransparent(Region* bm, int val, int maxVal, Rect r, const char* text)
{
	/*
	 * Fill area in grey
	 */

	bm->frame(r.getX(), r.getY(), r.getW(), r.getH(), Black);
	bm->greyOut(r + Rect(1,1,-2,-2), LRed);

	/*
	 * Add graduations
	 */

	bm->VLine(r.getX() + r.getW() / 4, 			r.getY(), r.getH(), LGrey);
	bm->VLine(r.getX() + r.getW() / 2, 			r.getY(), r.getH(), LGrey);
	bm->VLine(r.getX() + (r.getW() * 3) / 4, 	r.getY(), r.getH(), LGrey);

	/*
	 * Put the filled in area on
	 */

	SDimension w = (val * (r.getW() - 1)) / maxVal;
	if(w)
		bm->box(Rect(r.getX(), r.getY() + 1, w, r.getH() - 2), Green);

		// bm->greyOut(Rect(r.getX(), r.getY() + 1, w, r.getH() - 2), Green);

	/*
	 * Overlay Text
	 */

	if(text)
	{
		Region bm1 = *bm;
		TextWindow textWind(&bm1, Font_JAME_5);
		bm1.setClip(bm->getX() + r.getX(), bm->getY() + r.getY(), r.getW(), r.getH());
		textWind.setFormat(TW_CV);		// Centre vertically
		textWind.setPosition(Point(2, 0));	// Leave a couple of pixels to the left
		textWind.setColours(White);
		textWind.draw(text);
	}
}

#endif

