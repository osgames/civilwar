/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	FLI Tester
 *
 * Actually plays FLI files to a SVGA screen
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <fstream.h>
#include <conio.h>
#include <ctype.h>
#include "vesa.h"
#include "palette.h"
#include "flicread.h"
#include "timer.h"

class FlicSystem {
public:
	Vesa* screen;
public:
	FlicSystem();
	~FlicSystem();
};

FlicSystem::FlicSystem()
{
	screen = new Vesa(0x101);
	timer = new Timer;
}

FlicSystem::~FlicSystem()
{
	delete screen;
	delete timer;
	timer = 0;
}


void usage()
{
	cout << "\nUsage: showflic [-1] filename\n";
	cout << "  -1 : Play flic once, otherwise loop play until key press\n\n";
}

void main(int argc, char* argv[])
{
	char *fliName = 0;
	Boolean playOnce = False;

	int i = 0;
	while(++i < argc)
	{
		char* arg = argv[i];

		if((*arg == '-') || (*arg == '/'))
		{
			switch(toupper(arg[1]))
			{
			case '1':
				playOnce = True;
				break;
			default:										  
				usage();
				return;
			}
		}
		else
		{
			if(fliName)
			{
				usage();
				return;
			}
			else
				fliName = arg;
		}
	}
	if(!fliName)
	{
		usage();
		return;
	}


	try
	{

		FlicRead fli(fliName, True);	// This opens it using 64 colour palette

		if(fli.getError())
		{
			cout << "Error in FLIC file " << fliName << endl;
			return;
		}

		const FlicHead* head = fli.getHeader();

		cout << "Header for " << fliName << endl;
		cout << "Size: " << head->size << endl;
		cout << "Type: " << hex << head->type << dec << endl;
		cout << "frames: " << head->frames << endl;
		cout << "width: " << head->width << endl;
		cout << "height: " << head->height << endl;
		cout << "depth: " << head->depth << endl;
		cout << "flags: " << head->flags << endl;
		cout << "speed: " << head->speed << endl;

		unsigned int waitTime = (head->speed * timer->ticksPerSecond) / 1000;

		FlicSystem system;
		Palette64* pal = (Palette64*)&fli.palette;

		while(!kbhit() && (!playOnce || !fli.finished))
		{
			fli.nextFrame();

			timer->waitRel(waitTime);

			if(fli.paletteChanged)
				setHardwarePalette(0, 256, pal->colors);

			/*
			 * Blit it to screen
			 */

			system.screen->blit(&fli.image);
		}
	}
	catch( GeneralError e )
	{
		cout << "Error:";
		if ( e.get() )
		 	cout << e.get();
		else
			cout << " ??";
		cout << endl;
		getch();
	}
	catch( ...)
	{
		cout << "Unknown error!\n";
		getch();
	}
}
