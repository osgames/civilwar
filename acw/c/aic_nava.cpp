/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign AI for Naval Units.
 *
 *
 *-----------------------------------------------------------------
 * Method:
 *
 * For each Water Zone:
 *   Calculate Priority and CV needed
 *	Sort Water Zones by Priority
 * Make a list of available fleets
 * While available fleets left and water zones left
 *   Retrieve water zone with highest priority
 *   Search for fleets already on their way to zone and remove from CV needed
 *   Initialise list of fleets to send
 *   Clear all available fleet processed flags
 *   WHILE CV sent < CV needed and some fleets not processed are left
 *     Scan available fleets for closest of correct type
 *		 IF fleet CV < CV needed
 *       Add to fleets to send list
 *     ELSE
 *       Pull out boats needed and add to send list
 *   IF enough in fleets to send
 *     Send fleets and remove from available list
 *
 * While calculating all this, also fill in seaNavalNeeded and riverNavalNeeded.
 *
 * Although this is quite simplistic, it will probably work due to the
 * restricted water routes, e.g. on the way to a high priority waterzone
 * boats may have to pass through enemy zones and fight their way through.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "ai_camp.h"
#include "campaign.h"
#include "campwld.h"
#include "water.h"
#include "game.h"
#include "combval.h"
#include "camptab.h"

#ifdef DEBUG
#include "tables.h"

LogFile aiWZlog("ai_wz.log");

static char* wzTypeStr[] = {
	"Coastal",
	"Sea",
	"River"
};

#endif

/*
 * Define whether zones are sorted in high or low priority order
 */

#define SORT_HIGH				// High priority events processed first
// #define SORT_LOW			// Low priority events processed first

/*
 * Straight line function
 */

inline int linearEquation(int minY, int maxY, int x, int xRange)
{
	return minY + (x * (maxY - minY)) / xRange;
}

/*
 * Class to store sorted list of waterzone actions
 */

class AIC_WZEvent {
	friend class AIC_WZList;
private:
	AIC_WZEvent* next;			// Singly linked list

public:
	WaterZone* wz;
	CombatValue cvNeeded;
	AIC_Priority priority;

#ifdef DEBUG
	void print();
#endif
};

class AIC_WZList {
	AIC_WZEvent entry;

	AIC_WZEvent* current;		// Last value returned
	AIC_WZEvent* prev;			// For easy removal of current
public:
	AIC_WZList();
	~AIC_WZList() { clear(); }

	void clear();

	AIC_WZEvent* add(AIC_Priority priority);		// Add keeping sorted by priority

	void rewind();								// Next call to next() will start at beginning
	Boolean next(AIC_WZEvent*& value);		// Get next in list
	void remove();								// Remove last node that was returned
#ifdef DEBUG
	void print();
#endif

private:
	inline Boolean AIC_WZList::eventCompare(AIC_WZEvent* event, AIC_Priority priority);
};

/*
 * Functions for WaterZone List
 */

inline Boolean AIC_WZList::eventCompare(AIC_WZEvent* event, AIC_Priority priority)
{
#if defined(SORT_HIGH)
	return (event->priority > priority);
#elif defined(SORT_LOW)	// Lowest first
	return (event->priority < priority);
#else
	#error "Some sort method must be defined!"
#endif
}


/*
 * Constructor
 */

AIC_WZList::AIC_WZList()
{
	entry.next = 0;
	current = &entry;
	prev = 0;
}

/*
 * Destructor
 */

void AIC_WZList::clear()
{
	while(entry.next)
	{
		AIC_WZEvent* event = entry.next;
		entry.next = event->next;
		delete event;
	}

	prev = 0;
	current = &entry;
}

/*
 * Create a new entry sorted by priority
 */

AIC_WZEvent* AIC_WZList::add(AIC_Priority priority)
{
	AIC_WZEvent* newEvent = new AIC_WZEvent;

	ASSERT(newEvent != 0);

	newEvent->priority = priority;
	newEvent->cvNeeded = 0;
	newEvent->wz = 0;

	AIC_WZEvent* event = &entry;

	while(event->next && eventCompare(event->next, priority))
		event = event->next;

	ASSERT(event != 0);

	newEvent->next = event->next;
	event->next = newEvent;

	return newEvent;
}

/*
 * Make it so that next call to next() will return the 1st element
 */

void AIC_WZList::rewind()
{
	current = &entry;
	prev = 0;
}

/*
 * Return the next element in value
 * Return True if value was returned
 */

Boolean AIC_WZList::next(AIC_WZEvent*& value)
{
	ASSERT(current != 0);		// Should not call if returned 0

	if(current->next)
	{
		prev = current;
		current = current->next;

		value = current;
		return True;
	}
	else
		return False;
}

/*
 * Remove the last value from the list
 */

void AIC_WZList::remove()
{
	ASSERT(prev != 0);
	ASSERT(current != 0);

	prev->next = current->next;
	delete current;
	current = prev;
	prev = 0;				// Make it illegal to call remove again until next() is called
}


#ifdef DEBUG

/*
 * Display sorted waterzone list
 */

void AIC_WZEvent::print()
{
	WaterNetwork* wNet = &campaign->world->waterNet;

	aiWZlog.printf("  p=%3d cv=%5ld Zone:%3d %s %s",
		(int) priority,
		cvNeeded,
		wNet->getID(wz),
		sideStr[wz->side],
		wzTypeStr[wz->type]);
}

void AIC_WZList::print()
{
	WaterNetwork* wNet = &campaign->world->waterNet;

	aiWZlog.printf("--------");
	aiWZlog.printf("Sorted WaterZone priority list");
	aiWZlog.printf("");

	AIC_WZEvent* wzEvent = entry.next;
	while(wzEvent)
	{

		aiWZlog.printf("  p=%3d cv=%5ld Zone:%3d %s %s",
			(int) wzEvent->priority,
			wzEvent->cvNeeded,
			wNet->getID(wzEvent->wz),
			sideStr[wzEvent->wz->side],
			wzTypeStr[wzEvent->wz->type]);

		wzEvent = wzEvent->next;
	}
	aiWZlog.printf("--------");
}
#endif


void CampaignAI::make_wzList(AIC_WZList& wzList)
{
	WaterNetwork* wNet = &campaign->world->waterNet;

	Side enemySide = otherSide(aiSide);

	/*
	 * Calculate priorities and CV needed
	 */

	for(int i = 0; i < wNet->zones.entries(); i++)
	{
		WaterZone* zone = &wNet->zones[i];

		ASSERT(zone != 0);

#ifdef DEBUG
		aiWZlog.printf("Processing water zone %d, side = %s", i, sideStr[zone->side]);
#endif

		CombatValue cvNeeded = 0;
		int priority = 0;				// this gets added to

		Boolean enemyZone = False;	// True if either enemy controlled OR contains enemy fleets

		if(zone->side == enemySide)
			enemyZone = True;

		if(zone->side != aiSide)
			priority += 10;			// Small constant for all enemy/neutral zones

		/*
		 * Process Enemy boats
		 */

		/*
		 * Boats in current zone
		 */

		if(zone->side == enemySide)
		{
			if(zone->boats.totalBoats())
			{
				CombatValue cv = zone->boats.totalCombatFactor();

				cvNeeded += cv;
				priority += 20;		// Maybe depend on boats or cv?
#ifdef DEBUG
	  			aiWZlog.printf("  Enemy flotilla in zone ==> p=%d, cv=%ld",
							priority, cvNeeded);
#endif
			}
		}

		/*
		 * fleets in current zone
		 */

		if(zone->fleets.entries())
		{
			FleetIter fi = zone->fleets;

			Fleet* fleet;

			while( (fleet = fi.next()) != 0)
			{
				if(fleet->side == enemySide)
				{
					CombatValue cv = fleet->boats.totalCombatFactor();

					cvNeeded += cv;
					priority += 20;		// Maybe depend on boats or cv?

					enemyZone = True;
#ifdef DEBUG
	  			aiWZlog.printf("  Enemy fleet in zone ==> p=%d, cv=%ld",
							priority, cvNeeded);
#endif
				}
			}
		}

		/*
		 * Boats in neighbouring zones
		 */

		int cNum = zone->connections;
		int cCount = zone->connectCount;

		while(cCount--)
		{
			WaterZone* cZone = &wNet->zones[wNet->getConnect(cNum++)];

			ASSERT(cZone != 0);

			if( (zone->side == aiSide) && (cZone->side == enemySide) )
			{
#ifdef DEBUG
		  		aiWZlog.printf("  Enemy zone next to friendly zone => p=%d, cv=%ld",
							priority, cvNeeded);
#endif
				priority += 15;		// Defend intersections.
			}

			if(cZone->side == enemySide)
			{
				if(cZone->boats.totalBoats())
				{
					CombatValue cv = cZone->boats.totalCombatFactor();

					cvNeeded += cv;
					priority += 20;		// Maybe depend on boats or cv?
#ifdef DEBUG
		  			aiWZlog.printf("  Enemy flotilla in neighbouring zone ==> p=%d, cv=%ld",
								priority, cvNeeded);
#endif
				}
			}

			if(cZone->fleets.entries())
			{
				FleetIter fi = cZone->fleets;

				Fleet* fleet;

				while( (fleet = fi.next()) != 0)
				{
					if(fleet->side == enemySide)
					{
						CombatValue cv = fleet->boats.totalCombatFactor();

						cvNeeded += cv;
						priority += 20;		// Maybe depend on boats or cv?

						enemyZone = True;
#ifdef DEBUG
		  				aiWZlog.printf("  Enemy fleet in neighbouring zone ==> p=%d, cv=%ld",
								priority, cvNeeded);
#endif
					}
				}
			}
		}


		/*
		 * Process Cities in zone
		 */

		if(zone->facilityCount)
		{
			int fCount = zone->facilityCount;
			int fNum = zone->facilityList;
			while(fCount--)
			{
				Facility* f = wNet->getFacility(fNum++);

				ASSERT(f != 0);

				CitySize cSize = 0;

				if(f->facilities & Facility::F_City)
				{
					City* c = (City*)f;
					cSize = c->size;
				}

				if(f->side == aiSide)
				{	// Friendly city
					if(enemyZone)
					{
						// priority += 64 + (cSize * (256-64)) / (MaxCitySize + 1);	// 64..256
						priority += linearEquation(64, 256, cSize, MaxCitySize + 1);

#ifdef DEBUG
						aiWZlog.printf("  Friendly city in enemy zone %s ==> p=%d",
							f->getNameNotNull(), priority);
#endif
					}
				}
				else
				{	// Enemy or neutral City
					if(f->side == enemySide)
					{
						cvNeeded += calc_townNavalCV(f);
					}


					if(enemyZone)
					{
						// priority += 16 + (cSize * (32-16)) / (MaxCitySize + 1);		// 16..32
						priority += linearEquation(5, 20, cSize, MaxCitySize + 1);

#ifdef DEBUG
						aiWZlog.printf("  Enemy city in enemy zone %s ==> p=%d, cv=%ld",
							f->getNameNotNull(), priority, cvNeeded);
#endif
					}
					else
					{
						// priority += 32 + (cSize * (48-32)) / (MaxCitySize + 1);		// 32..48
						priority += linearEquation(32, 48, cSize, MaxCitySize + 1);

#ifdef DEBUG
						aiWZlog.printf("  Enemy city in friendly zone %s ==> p=%d, cv=%ld",
							f->getNameNotNull(), priority, cvNeeded);
#endif
					}


				}
			}
		}

		if(priority > AIC_PriorityMax)
			priority = AIC_PriorityMax;

		if(priority)
		{
			AIC_WZEvent* event = wzList.add(priority);
			event->wz = zone;
			event->cvNeeded = cvNeeded;
		}

#ifdef DEBUG
		zone->ai_cvNeeded = cvNeeded;
		zone->ai_priority = priority;
#endif

		if(zone->type == WZ_Sea)
			seaNavalNeeded += cvNeeded;
		else if(zone->type == WZ_River)
			riverNavalNeeded += cvNeeded;
		else if(zone->type == WZ_Coast)
		{
			riverNavalNeeded += cvNeeded / 2;
			seaNavalNeeded += cvNeeded / 2;
		}

	}	// Next Zone
}


/*=========================================================
 * List of available fleets
 */

class AvailableFleet {
friend class AvailableFleetList;
	AvailableFleet* next;
public:
	WaterZone* wz;
	Boolean picked;		// Set if already chosen
	Boolean considered;
};

class AvailableFleetList {
	AvailableFleet entry;
	AvailableFleet* current;
	AvailableFleet* prev;
public:
	AvailableFleetList();
	~AvailableFleetList() { clear(); }
	void clear();
	void add(WaterZone* wz);
	void rewind();
	Boolean next(AvailableFleet*& value);
	void remove();
#ifdef DEBUG
	void print();
#endif
};

AvailableFleetList::AvailableFleetList()
{
	entry.next = 0;
	current = &entry;
	prev = 0;
}

void AvailableFleetList::clear()
{
	while(entry.next)
	{
		AvailableFleet* af = entry.next;
		entry.next = af->next;
		delete af;
	}
	prev = 0;
	current = &entry;
}

void AvailableFleetList::add(WaterZone* wz)
{
	AvailableFleet* newFleet = new AvailableFleet;
	newFleet->wz = wz;
	newFleet->picked = False;
	newFleet->considered = False;

	newFleet->next = entry.next;
	entry.next = newFleet;
}

void AvailableFleetList::rewind()
{
	current = &entry;
	prev = 0;
}

Boolean AvailableFleetList::next(AvailableFleet*& value)
{
	ASSERT(current != 0);

	if(current->next)
	{
		prev = current;
		current = current->next;

		value = current;
		return True;
	}
	else
		return False;
}

void AvailableFleetList::remove()
{
	ASSERT(prev != 0);
	ASSERT(current != 0);

	prev->next = current->next;
	delete current;
	current = prev;
	prev = 0;				// Make it illegal to call remove again until next() is called
}

#ifdef DEBUG
void AvailableFleetList::print()
{
	aiWZlog.printf("--------");
	aiWZlog.printf("Available Fleets:");

	WaterNetwork* wNet = &campaign->world->waterNet;

	AvailableFleet* af = entry.next;
	while(af)
	{
		aiWZlog.printf("  Zone: %3d %s %s", 
			wNet->getID(af->wz), 
			wzTypeStr[af->wz->type],
			flotillaName(&af->wz->boats));

		af = af->next;
	}

	aiWZlog.printf("--------");
	
}
#endif

void makeAvailableFleets(AvailableFleetList& aFleets, Side aiSide)
{
	WaterNetwork* wNet = &campaign->world->waterNet;
	for(int i = 0; i < wNet->zones.entries(); i++)
	{
		WaterZone* zone = &wNet->zones[i];

		ASSERT(zone != 0);

		if( (zone->side == aiSide) && (zone->boats.totalBoats()) )
		{
			aFleets.add(zone);
		}
	}

#ifdef DEBUG
	aFleets.print();
#endif
}

void countNavalUnits(CombatValue& riverCV, CombatValue& seaCV, Side aiSide)
{
	WaterNetwork* wNet = &campaign->world->waterNet;

	riverCV = 0;
	seaCV = 0;

	for(int i = 0; i < wNet->zones.entries(); i++)
	{
		WaterZone* zone = &wNet->zones[i];

		ASSERT(zone != 0);

		if( (zone->side == aiSide) && (zone->boats.totalBoats()))
		{
			seaCV += navalCombat[Nav_NavalUnit] * zone->boats.count[Nav_NavalUnit];
			seaCV += navalCombat[Nav_NavalIronClad] * zone->boats.count[Nav_NavalIronClad];
			riverCV += navalCombat[Nav_Riverine] * zone->boats.count[Nav_Riverine];
			riverCV += navalCombat[Nav_RiverineIronClad] * zone->boats.count[Nav_RiverineIronClad];
		}


	 	if(zone->fleets.entries())
		{
			FleetIter fi = zone->fleets;
			Fleet* fleet;

			while( (fleet = fi.next()) != 0)
			{
				if(fleet->side == aiSide)
				{
					seaCV   += navalCombat[Nav_NavalUnit]        * fleet->boats.count[Nav_NavalUnit];
					seaCV   += navalCombat[Nav_NavalIronClad]    * fleet->boats.count[Nav_NavalIronClad];
					riverCV += navalCombat[Nav_Riverine]         * fleet->boats.count[Nav_Riverine];
					riverCV += navalCombat[Nav_RiverineIronClad] * fleet->boats.count[Nav_RiverineIronClad];
				}
			}
		}
	}
}


CombatValue findSentFleets(const WaterZone* wz, Side aiSide)
{
	WaterNetwork* wNet = &campaign->world->waterNet;
	WaterZoneID zid = wNet->getID(wz);

	CombatValue cv = 0;

	for(int i = 0; i < wNet->zones.entries(); i++)
	{
		WaterZone* zone = &wNet->zones[i];

		ASSERT(zone != 0);

	 	if(zone->fleets.entries())
		{
			FleetIter fi = zone->fleets;
			Fleet* fleet;

			while( (fleet = fi.next()) != 0)
			{
				if(fleet->side == aiSide)
				{
					if(fleet->destination == zid)
						cv += fleet->boats.totalCombatFactor();
				}
			}
		}
	}

	return cv;
}

/*
 * Class for storing potential fleets to send somewhere
 *
 * They are stored like this so that if there is not enough CV to make
 * the action worthwhile they can be freed up again and sent on an easier
 * mission.  This means for example that CSA boats will not all go off to
 * attack the heavily defended Cairo until the navy has been built up a bit.
 */

struct BoatsToSend {
	BoatsToSend* next;
	AvailableFleet* af;
	Flotilla boats;
};

class BoatsToSendList
{
	BoatsToSend* entry;

public:
	BoatsToSendList() { entry = 0; }
	~BoatsToSendList() { clear(); }

	BoatsToSend* make(AvailableFleet* fleet, Flotilla* flotilla);
	void clear();
	BoatsToSend* getEntry() { return entry; }
};

BoatsToSend* BoatsToSendList::make(AvailableFleet* fleet, Flotilla* flotilla)
{
	ASSERT(fleet != 0);
	ASSERT(flotilla != 0);
	ASSERT(flotilla->totalBoats() > 0);

	BoatsToSend* b = new BoatsToSend;

	ASSERT(b != 0);

	b->next = entry;
	b->af = fleet;
	b->boats = *flotilla;
	entry = b;
	return b;
}

void BoatsToSendList::clear()
{
	while(entry)
	{
		BoatsToSend* b = entry;
		entry = b->next;
		delete b;
	}
}

/*
 * Send some boats on their way to perform tasks
 */

void assignBoats(AIC_WZList& wzList, AvailableFleetList& aFleets, Side aiSide)
{
	WaterNetwork* wNet = &campaign->world->waterNet;
	Boolean finished = False;

	AIC_WZEvent* event;

	wzList.rewind();
	while(!finished && wzList.next(event))
	{
		ASSERT(event != 0);

#ifdef DEBUG
		aiWZlog.printf("Considering Event:");
		event->print();
#endif

		WaterZone* wz = event->wz;
		WaterZoneID zid = wNet->getID(wz);
		
		/*
		 * Remove any fleets already on their way here
		 */

		CombatValue cvNeeded = event->cvNeeded;
		if(cvNeeded == 0)
			cvNeeded = 1;
		CombatValue cvSent = findSentFleets(wz, aiSide);

#ifdef DEBUG
		aiWZlog.printf("  cvNeeded=%ld, cvSent=%ld", cvNeeded, cvSent);
#endif

		if(cvNeeded > cvSent)
		{
			BoatsToSendList sendList;

			cvNeeded -= cvSent;

			Boolean giveUp = False;

			while( (cvNeeded > 0) && !giveUp)
			{
				/*
				 * Find closest suitable available fleet 
				 */

				GameTicks bestTime = 0;
				AvailableFleet* bestFleet = 0;
				AvailableFleet* af;
			
				aFleets.rewind();
				while(aFleets.next(af))
				{
					if(!af->picked && !af->considered)
					{
						WaterZoneID fZid = wNet->getID(af->wz);

						if(fZid == zid)
						{
							bestTime = 0;
							bestFleet = af;
						}
						else
						{
							Flotilla newFlotilla;
							Flotilla& sBoats = af->wz->boats;

							if(wz->type != WZ_Sea)
							{
								newFlotilla.count[Nav_Riverine] 			 = sBoats.count[Nav_Riverine];
								newFlotilla.count[Nav_RiverineIronClad] = sBoats.count[Nav_RiverineIronClad];
							}

							if(wz->type != WZ_River)
							{
								newFlotilla.count[Nav_NavalUnit] 	 = sBoats.count[Nav_NavalUnit];
								newFlotilla.count[Nav_NavalIronClad] = sBoats.count[Nav_NavalIronClad];
							}

							GameTicks t = wNet->findRouteTime(fZid, zid, &newFlotilla);
	
							if(t && (!bestFleet || (t < bestTime)))
							{
								bestTime = t;
								bestFleet = af;
							}
						}
					}
				}

				if(bestFleet)
				{
#ifdef DEBUG
					aiWZlog.printf("  Found flotilla in Zone: %d %s %s",
						wNet->getID(bestFleet->wz), 
						wzTypeStr[bestFleet->wz->type],
						flotillaName(&bestFleet->wz->boats));
#endif

					bestFleet->considered = True;

					WaterZone* sZone = bestFleet->wz;
					WaterZoneID sZid = wNet->getID(sZone);
					Flotilla& sBoats = sZone->boats;

					if(sZid == zid)		// Already there!
					{
						CombatValue newCV = sBoats.totalCombatFactor();

						if(newCV > cvNeeded)
							cvNeeded = 0;
						else
							cvNeeded -= newCV;

						sendList.make(bestFleet, &sBoats);
#ifdef DEBUG
						aiWZlog.printf("  Adding stationary flotilla %s in zone %d",
							flotillaName(&sBoats), (int) zid);
						aiWZlog.printf("  cvNeeded (-%ld) => %ld", newCV, cvNeeded);
#endif
					}
					else
					{
						/*
					 	 * Pull out the neccessary boats and send on its way
					 	 */

						Flotilla newFlotilla;

						/*
					 	 * Only boats of apropriate type can go.
					 	 *
					 	 *                     Sea    Coast   River
					 	 * NavalUnit				Y		  Y		 N
					 	 * NavalIronClad			Y		  Y		 N
					 	 * Riverine					N		  Y		 Y
					 	 * RiverineIronClad		N		  Y		 Y
					 	 */


						if(wz->type != WZ_Sea)
						{
							newFlotilla.count[Nav_Riverine] 			 = sBoats.count[Nav_Riverine];
							newFlotilla.count[Nav_RiverineIronClad] = sBoats.count[Nav_RiverineIronClad];
						}

						if(wz->type != WZ_River)
						{
							newFlotilla.count[Nav_NavalUnit] 	 = sBoats.count[Nav_NavalUnit];
							newFlotilla.count[Nav_NavalIronClad] = sBoats.count[Nav_NavalIronClad];
						}

						if(newFlotilla.totalBoats())
						{
							CombatValue newCV = newFlotilla.totalCombatFactor();

							if(newCV > cvNeeded)
								cvNeeded = 0;
							else
								cvNeeded -= newCV;

							sendList.make(bestFleet, &newFlotilla);


#ifdef DEBUG
							aiWZlog.printf(" Adding %s from %d to %d",
								flotillaName(&newFlotilla),
								(int) sZid,
								(int) zid);
							aiWZlog.printf("  cvNeeded (-%ld) = %ld", newCV, cvNeeded);
#endif
						}
					}
				}
				else
					giveUp = True;		// Give up.
			}

			/*
			 * If there was enough CV then send them...
			 */

			if(cvNeeded == 0)
			{
				BoatsToSend* bs = sendList.getEntry();
				while(bs)
				{
					AvailableFleet* af = bs->af;
					WaterZone* sZone = af->wz;
					WaterZoneID sZid = wNet->getID(sZone);
					Flotilla& sBoats = sZone->boats;

					if(sZid == zid)
					{
#ifdef DEBUG
						aiWZlog.printf(" >Leaving flotilla %s in zone %d",
							flotillaName(&sBoats), (int) zid);
#endif
						af->picked = True;
					}
					else
					{
						Fleet* newFleet = new Fleet(bs->boats, aiSide);

						sBoats -= bs->boats;
						wNet->sendFleet(newFleet, sZid, zid, campaign->gameTime);

						if(sBoats.totalBoats() == 0)
							af->picked = True;
#ifdef DEBUG
						aiWZlog.printf(" >Sending %s from %d to %d",
							flotillaName(&bs->boats),
							(int) sZid,
							(int) zid);
#endif
					}

					bs = bs->next;
				}
			}
#ifdef DEBUG
			else
				aiWZlog.printf(" >CV too low (still need %ld) Not sending...", cvNeeded);
#endif

			sendList.clear();
		}

		/*
		 * Clear all the considered flags
		 * and find out if anybody left...
		 */

		finished = True;
		AvailableFleet* af;
		aFleets.rewind();
		while(aFleets.next(af))
		{
			af->considered = False;
			if(!af->picked)
				finished = False;
		}

		wzList.remove();		// Remove event from list
	}
}

/*=========================================================
 *	Main Naval Function called from ai_camp
 */

void CampaignAI::aic_ProcNaval()
{
#ifdef DEBUG
	aiLog.printf("aic_ProcNaval()");
	aiWZlog.setTime(0);		// Disable Time display
	aiWZlog.printf("");
	aiWZlog.printf("Processing Waterzones for %s", sideStr[aiSide]);
#endif

	riverNavalNeeded = 0;
	seaNavalNeeded = 0;

	countNavalUnits(riverNavalGot, seaNavalGot, aiSide);

	AIC_WZList wzList;			// List of water zones sorted by priority

	make_wzList(wzList);
#ifdef DEBUG
	wzList.print();
#endif

	/*
	 * Make list of available fleets
	 */

	AvailableFleetList aFleets;		// List of available fleets

	makeAvailableFleets(aFleets, aiSide);

#ifdef DEBUG
	aiWZlog.printf("-------- assigning");
#endif

	assignBoats(wzList, aFleets, aiSide);

#ifdef DEBUG
	aiWZlog.printf("--------");
	aiWZlog.printf("Finished");
	aiWZlog.printf("--------");
	aiWZlog.close();
#endif
}


