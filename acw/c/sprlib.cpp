/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Sprite Library Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.7  1994/08/22  16:02:09  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/08/19  17:29:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/07/19  19:53:03  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.3  1994/01/06  22:38:30  Steven_Green
 * File is closed when destructed
 *
 * Revision 1.2  1993/12/21  00:31:04  Steven_Green
 * Fills in SpriteBlock instead of a simple image.
 *
 * Revision 1.1  1993/12/16  22:19:35  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>

#include "sprlib.h"
#include "makename.h"
#include "swgspr.h"
#include "image.h"
#include "ptrlist.h"
#include "lzhuf.h"
#include "memlog.h"
#include "strutil.h"
#include "cd_open.h"

/*==========================================================
 * Sprite List Memory Exhaustion Handlers
 */

class SpriteCacheList;


class SpriteMemHandle {
	PtrDList<SpriteCacheList> libs;			// List of open libraries
	Boolean initialised;
public:
	SpriteMemHandle() { initialised = False; }
	~SpriteMemHandle();

	void add(SpriteCacheList* lib);
	void remove(SpriteCacheList* lib);

	Boolean freeUp(size_t s);
};

SpriteMemHandle memHandle;

/*==========================================================
 * Sprite List class
 *
 * This needs updating to actually check whether things already
 * exist in the cache and reuse them if so.
 */

class SpriteCache {
friend class SpriteCacheList;
	SpriteIndex index;
	SpriteBlock* sprite;
	UBYTE used;
public:
	SpriteCache(SpriteIndex n, SpriteBlock* data)
	{
		index = n;
		sprite = data;
		used = 1;
	}
	~SpriteCache() { delete sprite; }

	void operator ++() { used++; }
	UBYTE operator --() { used--; return used; }
};

class SpriteCacheList : public PtrDList<SpriteCache> {
public:
	SpriteCacheList() { memHandle.add(this); }
	~SpriteCacheList() { memHandle.remove(this); }

	void add(SpriteIndex n, SpriteBlock* data);
	void free(SpriteIndex n);
	void free(SpriteBlock* data);

	SpriteBlock* findAndLock(SpriteIndex n);

	Boolean flush();		// Flush out unused entries

#ifdef DEBUG
	void check();
#endif
};

#ifdef DEBUG

/*
 * Check cache for any unreleased images
 */

void SpriteCacheList::check()
{
	if(memLog && memLog->logFile)	// (memLog->mode != MemoryLog::None))
	{
		*memLog->logFile << "\nChecking Sprites\n";

		PtrDListIter<SpriteCache> iter = *this;

		Boolean found = False;

		while(++iter)
		{
			SpriteCache* cache = iter.current();

			if(cache->used)
			{
				*memLog->logFile << "  Image " << cache->index << " is in used " << (int)cache->used << " times\n";
				found = True;
			}
		}

		if(!found)
			*memLog->logFile << "  No Unfreed images\n";

		*memLog->logFile << endl;
	}
}

#endif

/*
 * FLush out unused sprite entries
 *
 * This could be made more complex by freeing items in a certain order
 * until enough memory has been freed.
 */

Boolean SpriteCacheList::flush()
{
	Boolean freedSomething = False;

	PtrDListIter<SpriteCache> iter = *this;

	while(++iter)
	{
		SpriteCache* cache = iter.current();

		if(!cache->used)
		{
			iter.remove();
			delete cache;
			freedSomething = True;
		}
	}

	return freedSomething;
}

void SpriteCacheList::add(SpriteIndex n, SpriteBlock* data)
{
	append(new SpriteCache(n, data));
}

/*
 * See if sprite is already in cache
 * If so, then increment its used count and return it
 */

SpriteBlock* SpriteCacheList::findAndLock(SpriteIndex n)
{
	if(entries())
	{
		PtrDListIter<SpriteCache> iter = *this;

		while(++iter)
		{
			SpriteCache* cache = iter.current();

			if(cache->index == n)
			{
				cache->used++;

				/*
				 * Should move it to the front of the queue so that
				 * it is not deleted
				 */

				return cache->sprite;
			}
		}
	}

	return 0;
}

void SpriteCacheList::free(SpriteIndex n)
{
	if(entries())
	{
		PtrDListIter<SpriteCache> iter = *this;

		while(++iter)
		{
			SpriteCache* cache = iter.current();

			if(cache->index == n)
			{
				if(!--cache->used)
				{
#if 0	// Leave it in the cache!
					delete cache->sprite;
					iter.remove();
					delete cache;
#endif
				}
				return;
			}
		}
	}
}

void SpriteCacheList::free(SpriteBlock* data)
{
	if(entries())
	{
		PtrDListIter<SpriteCache> iter = *this;

		while(++iter)
		{
			SpriteCache* cache = iter.current();

			if(cache->sprite == data)
			{
				if(!--cache->used)
				{
#if 0
					delete cache->sprite;
					iter.remove();
					delete cache;
#endif
				}
				return;
			}
		}
	}
}

/*
 * Open library
 */

SpriteLibrary::SpriteLibrary(const char* name)
{
	/*
	 * Make the filename
	 */

	char fname[FILENAME_MAX];
	makeFilename(fname, name, ".SPR", False);

	fileName = copyString(fname);

	/*
	 * Open up the file and read table of offsets
	 */

	// fileHandle.open(fileName, ios::in | ios::binary);
	
	fileOpen( fileHandle, fileName, ios::in | ios::binary );
	
	if(fileHandle.fail())
		throw GeneralError("Couldn't open sprite library %s", name);

	/*
	 * Read Header
	 */

	SpriteFileHeaderD header;
	fileHandle.read( (char*) &header, sizeof(header));
	if(fileHandle.fail())
		throw GeneralError("Couldn't read SpriteFileHeader in %s", name);
	
	spriteCount = getWord(&header.spriteCount);

	/*
	 * Read offset table
	 */

	offsets = new ULONG[spriteCount];
	int count = spriteCount;
	ULONG* p = offsets;
	while(count--)
	{
		ILONG l;

		fileHandle.read((char*) &l, sizeof(l));

		*p++ = getLong(&l);
	}

	cache = new SpriteCacheList;
}

/*
 * Close a library
 */

SpriteLibrary::~SpriteLibrary()
{
#ifdef DEBUG
	/*
	 * Check cache for any unfreed images
	 */

	cache->check();

#endif

	cache->clearAndDestroy();
	delete cache;
	delete[] offsets;
	fileHandle.close();

	delete[] fileName;
}

/*
 * Read a sprite from library
 */

SpriteBlock* SpriteLibrary::read(SpriteIndex n)
{
	if( (n >= spriteCount) || (n < 0) )
		throw GeneralError("Illegal Sprite Number %d in %s", int(n), fileName);

	/*
	 * See if it's already in the cache
	 */

	SpriteBlock* image = cache->findAndLock(n);
	if(!image)
	{
		/*
	 	 * Move to offset
	 	 */

		fileHandle.seekg(offsets[n]);

		/*
	 	 * Read Header
	 	 */

		SpriteHeaderD head;

		fileHandle.read((char*) &head, sizeof(head));
		if(fileHandle.fail())
			throw GeneralError("Couldn't read SpriteHeader of %d in %s", int(n), fileName);

		/*
	 	 * Create image and read in data
	 	 */

		UWORD width = getWord(&head.width);
		UWORD height = getWord(&head.height);

		image = new SpriteBlock(width, height, this);

		image->fullWidth 			= getWord(&head.fullWidth);
		image->fullHeight 		= getWord(&head.fullHeight);
		image->xOffset 			= getWord(&head.xOffset);
		image->yOffset 			= getWord(&head.yOffset);
		image->anchorX 			= getWord(&head.anchorX);
		image->anchorY 			= getWord(&head.anchorY);
		image->shadowColor 		= getByte(&head.shadowColor);
		image->setTransparent(getByte(&head.transparentColor));

		UBYTE flags = getByte(&head.flags);
		image->flags.rle 		= (flags >> RLE_B) 		& 1;
		image->flags.lzHuf	= (flags >> LZHUF_B) 	& 1;
		image->flags.nybble	= (flags >> NYBBLE_B) 	& 1;
		image->flags.squeeze	= (flags >> SQUEEZE_B)	& 1;
		image->flags.shadow	= (flags >> SHADOW_B)	& 1;
		image->flags.remap	= (flags >> REMAP_B)		& 1;

		/*
	 	 * read data block
	 	 */

		UWORD dataSize = getWord(&head.dataSize);

		if(image->flags.lzHuf)
		{
			UBYTE* inBuf = new UBYTE[dataSize];

			fileHandle.read(inBuf, dataSize);

			if(!fileHandle.fail())
				decode_lzhuf(inBuf, image->getAddress(), dataSize, width * height);

			delete inBuf;
		}
		else
		{
			/*
		 	 * Raw Read the data... assumes it is not nybble packed...
		 	 */

			fileHandle.read(image->getAddress(), width * height);
		}

		if(fileHandle.fail())
			throw GeneralError("Couldn't read Sprite data for %d in %s", int(n), fileName);

		cache->add(n, image);
	}

	return image;
}

/*
 * Mark a sprite as unused
 *
 * This will be more complex when the cacheing system is in place.
 */

void SpriteLibrary::release(SpriteBlock* sprite)
{
	cache->free(sprite);
}

void SpriteLibrary::release(SpriteIndex n)
{
	cache->free(n);
}


/*
 * Memory Exhaustion Handling functions
 */

/*
 * This is the function that gets called if new fails
 */

Boolean spriteMemHandlerFunction(size_t s)
{
	return memHandle.freeUp(s);
}

Boolean SpriteMemHandle::freeUp(size_t s)
{
	if(initialised)
	{
		if(libs.entries())
		{
			PtrDListIter<SpriteCacheList> iter = libs;
			while(++iter)
			{
				SpriteCacheList* lib = iter.current();

				if(lib->flush())
					return True;
			}
		}
	}
	return False;
}

/*
 * Add a new library
 */

static MemHandler spriteMemHandler = { &spriteMemHandlerFunction, 0 };

SpriteMemHandle::~SpriteMemHandle()
{
	if(initialised)
		removeMemHandler(&spriteMemHandler);
}

void SpriteMemHandle::add(SpriteCacheList* lib)
{
	if(!initialised)
	{
		addMemHandler(&spriteMemHandler);
		initialised = True;
	}

	libs.append(lib);
}

void SpriteMemHandle::remove(SpriteCacheList* lib)
{
	libs.remove(lib);
}


/*
 * Sprite Display functions
 */

void SpriteLibrary::drawSprite(Region* bm, Point where, SpriteIndex i)
{
	SpriteBlock* image = read(i);
	bm->maskBlit(image, where);
	release(image);
}

void SpriteLibrary::drawAnchoredSprite(Region* bm, Point where, SpriteIndex i)
{
	SpriteBlock* image = read(i);
	bm->maskBlit(image, where - Point(image->anchorX, image->anchorY));
	release(image);
}

void SpriteLibrary::drawCentredSprite(Region* bm, SpriteIndex i)
{
	SpriteBlock* image = read(i);
	bm->maskBlit(image, Point(image->xOffset + (bm->getW() - image->fullWidth) / 2, image->yOffset + (bm->getH() - image->fullHeight) / 2));
	release(image);
}

/*
 * Simple way of releasing a Spriteblock without needing to know what library
 * it comes from.
 */

void SpriteBlock::release()
{
	library->release(this);
}
