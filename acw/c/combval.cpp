/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Calculate Combat Value
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "combval.h"
#include "tables.h"
#include "generals.h"
#if !defined(TESTBATTLE)
#include "city.h"
#endif
#include "myassert.h"


#ifdef DEBUG
#include "clog.h"
LogFile cvLog("combval.log");
#endif

/*
 * Calculate Combat Value for a unit.
 * All dependant units below the unit are used.
 *
 * IF real is True then really dependant units are counted
 * IF real is False then ordered dependant units are counted
 *
 * Note that this implementation is recursive
 */

CombatValue calc_UnitCV(Unit* uit, Boolean real)
{
	ASSERT(uit != 0);

	if(uit == 0)
		return 0;


	if(uit->getRank() == Rank_Regiment)
	{
		Regiment* r = (Regiment*) uit;

		/*
		 * Could add something based on morale, efficiency, fatigue, etc...
		 */

		CombatValue value = (r->strength * BrigadeCombatValue[r->type.basicType][r->type.subType]) / 100;
		return value;
	}
	else
	{
		CombatValue total = 0;
	
		uit = uit->child;

		while(uit)
		{
			// if(real ? !uit->isReallyIndependant() : !uit->isIndependant())
			if(real ?
				(uit->getAttachMode() == Attached) :
				!uit->isDetached() )
					total += calc_UnitCV(uit, real);
				
			uit = uit->sister;
		}

		return total;
	}

#if 0

	UnitInfo ui(True, True);

	uit->getInfo(ui);

	for ( int i = 0; i < 3; i++ )		// was < 2
	{
		for ( int j = 0; j < 4; j++ )
		{
			total += (ui.strengths[i][j] * BrigadeCombatValue[i][j]) / 100;

			// count++;
		}
	}

	return total;
#endif
}

#if !defined(TESTBATTLE)
CombatValue calc_townCV(Facility* f, Boolean includeOccupier)
{
	ASSERT(f != 0);
	if(f == 0)
		return 0;

	/*
	 * Add on percentage for fort values
	 * (5 is to emulate unprotected town).
	 */

	static CombatValue fortValues[5] = {
		5 + 0,					// == 0 Militia Infantry
		5 + 20,					// == 2000 Militia Infantry
		5 + 20 * 2 * 2,		// == 8000
		5 + 20 * 3 * 3,		// == 18000
		5 + 20 * 4 * 4,		// == 32000
	};

	CombatValue count = fortValues[f->fortification];
	if(f->facilities & Facility::F_City)
	{
		count += 10;

		if(f->facilities & Facility::F_KeyCity)		// New.. 26th July
			count += 10;

	}


#ifdef DEBUG
	if(f->facilities & Facility::F_City)
		cvLog.printf("is a City");
	if(f->fortification)
		cvLog.printf("Has %d fortifications", (int)f->fortification);
	cvLog.printf("with fort = %ld", count);
#endif

	// New... 26th July 1995

	if(includeOccupier && f->occupier)
		count += calc_UnitCV(f->occupier, True);
		

	if(count)
	{
		int percent = 0;

		if(f->facilities & Facility::F_City)
		{
			// New 27th July... Defenses based on size of city!

			City* city = (City*) f;

			percent += 20 + (city->size * 50) / 64;		// 20..70%
			// percent += 20;
		}

		if(f->fortification)
			percent += f->fortification * 20;
		if(percent)
			count += (count * percent) / 100;
#ifdef DEBUG
		cvLog.printf("+%d%% for city or fort = %ld", (int)percent, count);
#endif
	}


	/*
	 * reduce for sieging?
	 */

	if(f->sieger)
	{
		count = (count * f->supplies) / MaxSupply;

#ifdef DEBUG
		cvLog.printf("Reduced because of sieging to: %ld", count);
#endif
	}

	return count;
}





CombatValue calc_townNavalCV(Facility* f)
{
	return f->fortification * 10 + 1;
}


#endif	// TESTBATTLE
