/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Find out if machine has at least n K free
 * Returns 0 if it has
 * Returns 1 if it has less
 *
 * Amount required is passed on command line
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

// #define DEBUG

#include <stdio.h>
#include <i86.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

struct MemInfo {
	// unsigned long freeDPMI;
	// unsigned long freeDOS;
	unsigned long totalMemory;
};

int main(int argc, char* argv[])
{
	unsigned long address;

	size_t minRequired = 0x380000;		// 3.5 Megs

	int i = 0;

	while(++i < argc)
	{
		if(isdigit(argv[i][0]))
			minRequired = atoi(argv[i]) * 0x400;	// 1K.
	}

	/*
	 * Get DPMI Info
	 */

	struct {
		unsigned long largest;					// Largest free block of DPMI memory
		unsigned long maxPage;
		unsigned long largeBlock;
		unsigned long linSpace;					// Total Linear memory Space
		unsigned long freePages;				// Total Number of free pages
		unsigned long physPages;				// Total Number of free Physical Pages
		unsigned long totalPhysicalPages;	// Total Number of Physical Pages
		unsigned long freeLinSpace;			// Free Linear Space
		unsigned long sizeofPage;				// Size of paging partition
		unsigned long reserved[3];
	} DPMIInfo;

	
	union REGS regs;
	struct SREGS sregs;

	regs.x.eax = 0x500;
	memset(&sregs, 0, sizeof(sregs));
	sregs.es = FP_SEG(&DPMIInfo);
	regs.x.edi = FP_OFF(&DPMIInfo);
	int386x(0x31, &regs, &regs, &sregs);

	/*
	 * DOS Memory
	 */

	memset(&sregs, 0, sizeof(sregs));
	regs.x.eax = 0x0100;
	regs.x.ebx = 0xffff;		// Allocate more than is available

	int386(0x31, &regs, &regs);

	size_t total = DPMIInfo.largest + (regs.w.bx << 4);

	if(total < minRequired)
		return 1;
	else
		return 0;

}

