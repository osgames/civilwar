/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battle Editor Main File
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "bated.h"
#include "system.h"
#include "screen.h"
#include "mouselib.h"
#include "dialogue.h"
#include "batwind.h"
#include "gameicon.h"
#include "filesel.h"
#include "batfile.h"
#include "tables.h"
#include "layout.h"
#include "generals.h"
#include "text.h"
#include "colours.h"
#include "sprlist.h"
#include "files.h"
#include "staticob.h"
#include "batdisp.h"
#include "earth.h"
#include "ob.h"
#include "map3d.h"
#include "cal.h"
#include "unit3d.h"
#include "clock.h"
#include "game.h"
#ifdef DEBUG
#include "memlog.h"
#include "options.h"
#endif
// #include "membodge.h"

/*=====================================================
 * Global Variables
 */

BattleData* battle = 0;
BattleEditControl* control = 0;

#if 0		// Now included in game.obj
GameVariables* game = 0;
#endif

Boolean changed = False;				// Has anything changed?
Boolean obChanged = False;

/*=========================================================
 * Miscellaneous Options
 */

class GouradIcon : public NullIcon {
public:
	GouradIcon(IconSet* parent) : NullIcon(parent, 'G') { }
	void execute(Event* event, MenuData* data)
	{
		if(event->buttons)
		{
			globalOptions.toggle(Options::Gourad);
			control->updateView();
		}
	}
};

class TextureIcon : public NullIcon {
public:
	TextureIcon(IconSet* parent) : NullIcon(parent, 'T') { }
	void execute(Event* event, MenuData* data)
	{
		if(event->buttons)
		{
			globalOptions.toggle(Options::Texture);
			control->updateView();
		}
	}
};

#if 0
/*=========================================================
 * Options Icon
 */

class BattleEditOptionIcon : public OptionsIcon {
public:
	BattleEditOptionIcon(IconSet* set, Point p) : OptionsIcon(set, p) { }

	void doDiskFunctions(MenuData* data);
	void doSystemFunctions(MenuData* data);
};

void BattleEditOptionIcon::doDiskFunctions(MenuData* data)
{
	switch(dialAlert(data, getPosition() + getSize() / 2,
		"Disk Functions",
		"Save Battle|Cancel"))
	{
	case 0:		// Save
		saveData();
		break;
	default:		// Cancel
		break;
	}
}

void BattleEditOptionIcon::doSystemFunctions(MenuData* data)
{
	quitProg(data);
}

#endif

void saveData()
{
	char fileName[FILENAME_MAX];
	strcpy(fileName, "gamedata\\*.btl");
	if(fileSelect(fileName, "Save Battle file", False) == 0)
	{
		writeHistoricalBattle(fileName, battle);

		changed = False;
	}
}

void quitProg(MenuData* data)
{
	Boolean needPrompt = True;
	Boolean wantQuit = False;

	if(changed)
	{
		char buffer[500];

		sprintf(buffer, "Warning\r\rUnsaved Changes have been made\r");

		switch(dialAlert(data,
			buffer,
			"Save|Quit|Cancel"))
		{
		case 0:
			saveData();
			// Drop through
		case 1:
			wantQuit = True;
			break;
		case 2:
			break;
		}
		needPrompt = False;
	}

	if(needPrompt)
	{
		if(dialAlert(data,
			"Do you really want to quit?",
			"Quit to DOS|Cancel") == 0)
		{
			wantQuit = True;
		}
	}

	if(wantQuit)
		data->setFinish(MenuData::MenuQuit);
}


class BattleSaveIcon : public MenuIcon {
public:
	BattleSaveIcon(IconSet* set, Point p, Key k1 = 0, Key k2 = 0) : MenuIcon(set, MM_Save, Rect(p, Point(MAIN_ICON_W,MAIN_ICON_H)), k1, k2) { }
	void execute(Event* event, MenuData* d);
};

void BattleSaveIcon::execute(Event* event, MenuData* d)
{
	if(event->buttons)
		saveData();
}


class ToDosIcon : public MenuIcon {
public:
	ToDosIcon(IconSet* set, Point p, Key k1 = 0, Key k2 = 0) : MenuIcon(set, MM_Dos, Rect(p, Point(MAIN_ICON_W,MAIN_ICON_H)), k1, k2) { }
	void execute(Event* event, MenuData* d);
};

void ToDosIcon::execute(Event* event, MenuData* data)
{
	if(event->buttons)
		quitProg(data);
}


void QuitIcon::execute(Event* event, MenuData* data)
{
	if(event->buttons)
		quitProg(data);
}

/*==============================================================
 * Mode Change Icon
 */

class EditModeIcon : public Icon {
public:
	EditModeIcon(BattleEditControl* parent) :
		Icon(parent, Rect(INFO_X, MENU_ICON_Y8, MENU_ICON_W, MENU_ICON_H))
	{
	}

	void execute(Event* event, MenuData* d);
	void drawIcon();
};

/*====================================================
 * Mode Icon
 */

static char* editStr[] = {
	"Troops",
	"Objects",
	"Heights",
	"Rivers",
	"Roads",
	"Terrain",
	0
};

void EditModeIcon::execute(Event* event, MenuData* d)
{
	if(event->buttons & Mouse::LeftButton)
	{
		MenuChoice choices[EDITMODE_HowMany + 1];
		MenuChoice* m = choices;
		int i = 0;
		while(i < EDITMODE_HowMany)
		{
			m->text = editStr[i];
			m->value = ++i;
			m++;
		}
		m->text = 0;

		int sel = menuSelect(0, choices, getPosition() + getSize() / 2);

		if(sel > 0)
		{
			control->setMode(EditMode(sel -1));
			setRedraw();
		}
	}
	else if(event->buttons & Mouse::RightButton)
		control->doOptions(getPosition() + getSize() / 2);
}

void EditModeIcon::drawIcon()
{
	Region bm(machine.screen->getImage(), Rect(getPosition(), getSize()));
	bm.fill(LBlue);
	TextWindow window(&bm, Font_EMMA14);
	window.setFormat(True, True);
	bm.frame(0, 0, getW(), getH(), Blue, LGrey);
	window.draw(editStr[control->mode]);
	machine.screen->setUpdate(bm);
}

void BattleEditControl::doOptions(const Point& p)
{
	switch(mode)
	{
		case EDIT_Troops:
			break;
		case EDIT_Objects:
			objectOptions();
			break;
		case EDIT_Landscape:
			heightOptions();
			break;
		case EDIT_Rivers:
			riverOptions();
			break;
		case EDIT_Roads:
			roadOptions();
			break;
		case EDIT_Terrain:
			terrainOptions();
			break;
	}
}

/*
 * Change to a new mode
 */

static PointerIndex mapPointers[] = {
	M_WhereTo,		// Troops
	M_WhereTo,		// Objects
	M_WhereTo,		// Landscape
	M_WhereTo,		// Rivers
	M_WhereTo,		// Roads
	M_WhereTo		// Terrains
};

void BattleEditControl::setMode(EditMode m)
{
	if(m != mode)
	{
		loseObject();

		/*
		 * Undo previous mode
		 */

		switch(mode)
		{
			case EDIT_Troops:
				break;
			case EDIT_Objects:
				break;
			case EDIT_Landscape:
				break;
			case EDIT_Terrain:
				break;
		}
		doingDrag = False;
	}

	mode = m;
	trackMode = Tracking;
	hint(0);

	/*
	 * Set up for new mode
	 */


	switch(mode)
	{
		case EDIT_Troops:
			break;
		case EDIT_Objects:
			break;
		case EDIT_Landscape:
			break;
		case EDIT_Terrain:
			break;
	}
}

/*==========================================================
 * Clock Icon
 */

class BattleEditClockIcon : public ClockIcon {
public:
	BattleEditClockIcon(IconSet* set, Point p, TimeInfo* t) : ClockIcon(set, p, t, 0, False) { }
	
	void execute(Event* event, MenuData* d);
};

char p_day[3];
char p_year[5];
char p_hour[3];
char p_minute[3];

char* monthNames[] = {
	"Jan",
	"Feb",
	"Mar",
	"Apr",
	"May",
	"Jun",
	"Jul",
	"Aug",
	"Sep",
	"Oct",
	"Nov",
	"Dec",
	0
};

DialItem timeParameterItems[] = {
	DialItem(Dial_Button,		0,		Rect(0,0,256,16), "Set Date/Time"),
	DialItem(Dial_Button,		0,		Rect(0,16,64,16),	"Date:"),
	DialItem(Dial_NumberInput,	0,		Rect(64,16,48,16),	p_day, sizeof(p_day)),
	DialItem(Dial_Button,	   1,		Rect(112,16,64,16), 0),	// Month goes here
	DialItem(Dial_NumberInput,	0,		Rect(176,16,64,16),	p_year, sizeof(p_year)),

	DialItem(Dial_Button,		0,		Rect(0,32,64,16),	"Time:"),
	DialItem(Dial_NumberInput,	0,		Rect(64,32,48,16),	p_hour, sizeof(p_hour)),
	DialItem(Dial_NumberInput,	0,		Rect(112,32,48,16),	p_minute, sizeof(p_minute)),
	DialItem(Dial_Button, 		-1,  	Rect( 32,48, 64,16), "Cancel"),
	DialItem(Dial_Button, 		-2,  	Rect(160,48, 64,16), "OK"),
	DialItem(Dial_End)
};


class TimeParameterDial : public Dialogue {
public:
	UBYTE month;

	TimeParameterDial();
	void doIcon(DialItem* item, Event* event, MenuData* d);
};

TimeParameterDial::TimeParameterDial()
{
	Dialogue::setup(timeParameterItems);
}

void TimeParameterDial::doIcon(DialItem* item, Event* event, MenuData* d)
{
	if(event->buttons)
	{
		switch(item->id)
		{
		case 1:
			{
				int m = chooseFromList(0, "Month", (char const **) monthNames, "Cancel");
				if(m > 0)
				{
					month = m - 1;
					timeParameterItems[3].text = monthNames[month];
					d->setRedraw();
				}
			}
			break;
		case -1:
		case -2:
			d->setFinish(item->id);
			break;
		}
	}
}

void BattleEditClockIcon::execute(Event* event, MenuData* d)
{
	if(event->buttons)
	{
		TimeInfo* t = &battle->timeInfo;

		sprintf(p_day, "%d", (int) t->day);
		sprintf(p_year, "%d", (int) t->year);
		sprintf(p_hour, "%d", (int) t->hour);
		sprintf(p_minute, "%d", (int) t->minute);

		TimeParameterDial dial;
		dial.month = t->month;
		timeParameterItems[3].text = monthNames[t->month];

		if(dial.process() == -2)
		{
			t->month = Month(dial.month);
			
			int n = atoi(p_day);
			if( (n > 0) && (n <= 31) )
				t->day = n;

			n = atoi(p_year);
			if( (n >= startYear) && (n < (startYear + 256)))
				t->year = n;

			n = atoi(p_hour);
			if( (n >= 0) && (n < 24) )
				t->hour = n;

			n = atoi(p_minute);
			if( (n >= 0) && (n < 60) )
				t->minute = n;

			battle->gameTime.set(t->day, t->month, t->year, t->hour, t->minute);
			battle->timeInfo = battle->gameTime;	// Force proper update

			setRedraw();
		}
	}
}


/*==========================================================
 * Display Battle Game
 */

void BattleEditControl::display()
{
	mapWindow->mapArea->setRedraw();
}

#if 0	// Now include game.obj

GameVariables::GameVariables()
{
	connection = new NullConnection;
	sprites = new SpriteLibrary("art\\game");
}

GameVariables::~GameVariables()
{
	if(sprites)
		delete sprites;
}

#endif

/*==========================================================
 * Controlling Class
 */

BattleEditControl::BattleEditControl() :
	MenuData(0),
	BattleData()
{
	game = new GameVariables;
	// memHeap = new Heap(170100 * 2);	// Size of land route finder
	trackMode = Tracking;
	currentObject = 0;
	currentBattleObject = 0;

	defaultTreeType = Tree1Object;
	defaultTreeFacing = 0;

	hillHeight = 0x80;
	hillRadius = 64;
	hillRough = 16;
	hillFlat = False;

	riverHeight = 0;
	riverWidth = 8;
	riverCurvature = 60;

	terrainType = PastureTerrain;
	terrainSize = 1;
	riverProtected = True;
	roadProtected = True;

	doingDrag = False;

	backupGrid = 0;

	battle = this;
	control = this;
	
	mouseOnMap = False;
	zooming = False;
	alone = False;

	clickUnit = False;
	justDrawn = False;

#ifdef DEBUG
	icons = new IconList[10];
#else
	icons = new IconList[8];
#endif
	Icon** iconPtr = icons;

	// BattleMap* mapWindow;

	*iconPtr++ = mapWindow = new BattleMap(this);
	*iconPtr++ = viewIcons = new BattleViewIcons(this);
#if 0
	*iconPtr++ = new BattleEditOptionIcon (this, Point(MAIN_ICON_X2, MAIN_ICON_Y));
	*iconPtr++ = new BattleEditClockIcon(this, Point(MAIN_ICON_X1,MAIN_ICON_Y), &battle->timeInfo);
#else
	*iconPtr++ = new BattleSaveIcon(this, Point(MAIN_ICON_X3,MAIN_ICON_Y));
	*iconPtr++ = new ToDosIcon(this, Point(MAIN_ICON_X1, MAIN_ICON_Y));
	*iconPtr++ = new BattleEditClockIcon(this, Point(MAIN_ICON_X2,MAIN_ICON_Y), &battle->timeInfo);
#endif



	*iconPtr++ = new QuitIcon(this);
	*iconPtr++ = new EditModeIcon(this);
#ifdef DEBUG
	*iconPtr++ = new GouradIcon(this);
	*iconPtr++ = new TextureIcon(this);
#endif
	*iconPtr++ = 0;

	infoArea = new Region(machine.screen->getImage(), Rect(INFO_X,INFO_Y,INFO_WIDTH,INFO_HEIGHT));	// was 315 high
	textWin = new TextWindow(infoArea, Font_SimpleMessage);

	setMode(EDIT_Troops);
}

BattleEditControl::~BattleEditControl()
{
	delete textWin;
	delete infoArea;
	delete backupGrid;
	delete game;
	game = 0;
	// delete memHeap;
	// memHeap = 0;
}

void BattleEditControl::init(const char* fileName)
{
	char fullFileName[FILENAME_MAX];

	if(fileName)
		strcpy(fullFileName, fileName);
	else
	{
		strcpy(fullFileName, "gamedata\\*.btl");
		
		if(fileSelect(fullFileName, "Load Historical Battle file", True) != 0)
			strcpy(fullFileName, "gamedata\\default.btl");

		readHistoricalBattle(fullFileName, this);
	}

	makeBattleField();
	ob->setupBattle();

	updateView();
}

void BattleEditControl::drawIcon()
{
	showFullScreen("art\\screens\\playscrn.lbm");
	battle->data3D.terrain.setPalette();
	IconSet::drawIcon();

	justDrawn = True;
}

void BattleEditControl::proc()
{
	if(clickUnit && justDrawn)
	{
		clickUnit = False;
		unitClicked();
	}
}

void BattleEditControl::askClickUnit()
{
	justDrawn = False;
	clickUnit = True;
}

/*===============================================================
 * BattleData functions
 */

BattleData::BattleData()
{
	mapWindow = 0;
	viewIcons = 0;
	spriteList = new Sprite3DList;
	spriteLib = new SpriteLibrary(battleSpriteFileName);
	localTrees = new StaticObjectData;
	miscObjects = new StaticObjectData;
	displayedObjects = new BattleDisplayList;
	displayedCommanders = new DispCommandList;
	earthFace = new LineList;
	currentUnit = 0;
	gameTime.setRate(15);
	timeInfo = gameTime;
	ob = new OrderBattle;
	grid = new MapGrid;
	gotMouseLocation = False;
}

BattleData::~BattleData()
{
	delete earthFace;
	delete displayedCommanders;
	delete displayedObjects;
	delete miscObjects;
	delete localTrees;
	delete spriteLib;
	delete spriteList;
	delete ob;
	delete grid;
}

/*
 * Zooming
 */

void BattleData::zoomIn(ZoomLevel level, const BattleLocation& location)
{
	stopZoomMode();
	data3D.view.zoomIn(level, location);
	updateView();
}

void BattleData::zoomIn()
{
	stopZoomMode();
	if(data3D.view.zoomIn())
		updateView();
}

void BattleData::zoomOut()
{
	stopZoomMode();
	if(data3D.view.zoomOut())
		updateView();
}

void BattleData::updateView()
{
	displayedObjects->clear();
	localTrees->clear();
	miscObjects->reset();

	data3D.view.setBoundaries(grid);
	setLight();
	grid->updateViewGrid(data3D);

	makeLocalTrees();

	spriteList->clear();

	if(data3D.view.zoomLevel <= Zoom_8)
	{
		miscObjects->draw(data3D.view);
		localTrees->draw(data3D.view);
	}

	viewIcons->setRedraw();
	mapWindow->setRedraw();
}

void BattleEditControl::startZoomMode(ZoomLevel newLevel)
{
	stopZoomMode();
	if(newLevel > data3D.view.zoomLevel)
	{
		data3D.view.zoomIn(newLevel);
		updateView();
	}
	else
	{
		oldZoomPointer = control->getPointer();
		control->setPointer(M_Zoom);
		zooming = True;
		newZoomLevel = newLevel;
	}
}

void BattleEditControl::startZoomMode()
{
	stopZoomMode();
	ZoomLevel zoom = data3D.view.zoomLevel;
	
	if(zoom > Zoom_Min)
	{
		DECREMENT(zoom);
		startZoomMode(zoom);
	}
}

void BattleEditControl::stopZoomMode()
{
	if(zooming)
	{
		zooming = False;
		control->setPointer(oldZoomPointer);
	}
}

void BattleData::drawObjects(System3D& drawData)
{
	displayedCommanders->clear();

	Unit* u = ob->sides;
	while(u)
	{
		drawUnits(u, drawData);
		u = u->sister;
	}

	if(control->currentBattleObject)
	{
		control->currentBattleObject->draw(drawData.view);
	}
}

void BattleData::drawUnits(Unit* ob, System3D& drawData)
{

	/*
	 * Draw this unit
	 */

	ob->draw3D(drawData);

	/*
	 * Depending on zoom level, go down and draw children
	 */

	static Rank rankView[ZOOM_LEVELS] = {
		Rank_Regiment,		// Half Mile
		Rank_Regiment,		// 1 Mile
		Rank_Regiment,		// 2 Mile
		Rank_Regiment,		// 4 Mile
		Rank_Brigade,		// 8 Mile
		Rank_Division,		// 16 Mile
		Rank_Division,		// 32 Mile
		Rank_Division,		// 64 Mile
	};

	if(ob->getRank() < rankView[drawData.view.zoomLevel])
	{
		/*
		 * Draw its children
		 */

		Unit* c = ob->child;
		while(c)
		{
			drawUnits(c, drawData);
			c = c->sister;
		}
	}
}

void BattleEditControl::overMap(Event* event, const Icon* icon, MenuData* d)
{
	if(clickUnit)
		return;

	mouseOnMap = True;

	Boolean gotDestination = False;

	gotMouseLocation = data3D.screenToBattleLocation(event->where, mouseLocation);

	/*
	 * If in tracking mode, then get closest unit
	 * otherwise in destination mode, work out a position for
	 * the pointer
	 */

	if(zooming)
	{
		if(gotMouseLocation)
			setTempPointer(M_Zoom1);

		if(event->buttons & Mouse::RightButton)
			stopZoomMode();
		else if(gotMouseLocation && (event->buttons & Mouse::LeftButton))
		{
			zoomIn(newZoomLevel, mouseLocation);
			stopZoomMode();
		}
	}
	else
	{
		if(trackMode == Tracking)
			setTempPointer(mapPointers[mode]);
		else
		{
			if(gotMouseLocation)
				setTempPointer(M_WhereTo1);
		}

		switch(mode)
		{
		case EDIT_Troops:
			troopOverMap(event);
			break;

		case EDIT_Objects:
			objectOverMap(event);
			break;

		case EDIT_Landscape:
			heightOverMap(event);
			break;

		case EDIT_Rivers:
			riverOverMap(event);
			break;
		case EDIT_Roads:
			roadOverMap(event);
			break;
		case EDIT_Terrain:
			terrainOverMap(event);
			break;
		}
	}
}

void BattleEditControl::troopOverMap(const Event* event)
{
	if(trackMode == Tracking)
	{
		/*
		 * Update the current Unit to the one closest to the cursor
		 */

		Unit* newUnit = displayedCommanders->getCloseCommander(event->where);
	
		if(newUnit != currentUnit)
		{
			if(newUnit)
			{
				setCurrentObject(newUnit);
				currentUnit = newUnit;
			}
			else
			{
				loseObject();
			}
		}
	}

	if(event->buttons & Mouse::RightButton)
	{	// Go to CAL or close down
		if(trackMode == Tracking)
		{
			int giveOrders;
			Unit* newUnit = doBattleCAL(battle->ob, currentUnit, &giveOrders, this);
			setUpdate();

			/*
			 * Centre on new unit
			 */

			if(newUnit && (newUnit != currentUnit))
			{
				currentUnit = newUnit;
				zoomIn(data3D.view.zoomLevel, currentUnit->battleInfo->where);
				askClickUnit();
			}
		}
		else
			trackMode = Tracking;
	}
	else if(event->buttons & Mouse::LeftButton)
	{
		if(trackMode == Tracking)
		{
			unitClicked();
		}
		else if(trackMode == Moving)
		{
			if(currentUnit && gotMouseLocation)
			{
				Location location;

				location.x = BattleToDist(mouseLocation.x);
				location.y = BattleToDist(mouseLocation.z);

				currentUnit->makeBattleDetached(True);
#if 0
				currentUnit->makeBattleReallyDetached(True);
#else
				currentUnit->setBattleAttachMode(Detached);
#endif
				currentUnit->battleInfo->setPosition(location, alone);
				changed = True;
				obChanged = True;
			}

			trackMode = Tracking;
		}
		else if(trackMode == GetDestination)
		{
			hint(0);
			if(currentUnit && gotMouseLocation)
			{
				UnitBattle* b = currentUnit->battleInfo;

				Location location;

				location.x = BattleToDist(mouseLocation.x);
				location.y = BattleToDist(mouseLocation.z);

				b->setFacing(location);
				b->battleOrder.destination = location;
				b->setBattleOrder(currentUnit->battleInfo->battleOrder);
			}
			trackMode = Tracking;
			hint(0);
		}
	}
	
}

/*
 * Ask User what he wants to do with the unit
 */

// Pull up order box

void BattleEditControl::unitClicked()
{
	if(currentUnit)
	{
		DoWhat what;
		static MenuChoice unitChoices[] = {
			MenuChoice(0, 					0 			),						// User fills in this
			MenuChoice("-", 				0 			),
			MenuChoice("Move", 			DoMove 	),
			MenuChoice("Move Alone",	DoMoveLone),
			MenuChoice("Change Facing",  DoFacing	),
			MenuChoice("Make Detached", DoIndependant ),
			MenuChoice("Give Order", DoOrder),
			MenuChoice("CAL", 			DoCal 	),
			MenuChoice("-", 				0 			),
			MenuChoice("Cancel", 		-1 		),
			MenuChoice(0,					0 			)
		};

		unitChoices[0].text = currentUnit->getName(True);

		if(currentUnit->isBattleDetached())
			unitChoices[5].text = "Rejoin Command";
		else
			unitChoices[5].text = "Make Detached";




		what = DoWhat(menuSelect(0, unitChoices, machine.mouse->getPosition()));

		switch(what)
		{
			case DoMove:
				trackMode = Moving;
				alone = False;
				break;
			case DoMoveLone:
				trackMode = Moving;
				alone = True;
				break;
			case DoCal:
			{
				int giveOrders;
				Unit* newUnit = doBattleCAL(ob, currentUnit, &giveOrders, 0);
				setUpdate();
				if(newUnit)
				{
					if(newUnit != currentUnit)
					{
						setCurrentObject(newUnit);
						zoomIn(data3D.view.zoomLevel, currentUnit->battleInfo->where);
						askClickUnit();
					}

					if(giveOrders)
						what = DoMove;
				}
				break;
			}
			case DoIndependant:
			{
				Boolean iFlag = !currentUnit->isBattleDetached();
				currentUnit->makeBattleDetached(iFlag);
#if 0
				currentUnit->makeBattleReallyDetached(iFlag);
#else
				currentUnit->setBattleAttachMode(iFlag ? Detached : Attached);
#endif
				break;
			}
			case DoOrder:
				{
					static MenuChoice orderChoice[] = {
						MenuChoice("Advance",				1+ORDER_Advance),
						MenuChoice("Cautious Advance",	1+ORDER_CautiousAdvance),
						MenuChoice("Attack",					1+ORDER_Attack),
						MenuChoice("Stand",					1+ORDER_Stand),
						MenuChoice("Hold",					1+ORDER_Hold),
						MenuChoice("Defend",					1+ORDER_Defend),
						MenuChoice("Withdraw",				1+ORDER_Withdraw),
						MenuChoice("Retreat",				1+ORDER_Retreat),
						MenuChoice("Hasty Retreat",		1+ORDER_HastyRetreat),
						MenuChoice("-", 						0),
						MenuChoice("Cancel", 				-1),
						MenuChoice(0,							0)
					};


					int order = menuSelect(0, orderChoice, machine.mouse->getPosition());

					if(order > 0)
					{
						OrderMode orderMode = (OrderMode) (order - 1);

						currentUnit->battleInfo->battleOrder.mode = orderMode;
						currentUnit->battleInfo->setBattleOrder(currentUnit->battleInfo->battleOrder);
						currentUnit->makeBattleDetached(True);
#if 0
						currentUnit->makeBattleReallyDetached(True);
#else
						currentUnit->setBattleAttachMode(Detached);
#endif

						trackMode = GetDestination;
						hint("Click on Destination for %s order", getOrderStr(orderMode));
					}
				}
				break;
			case DoFacing:
			{
				trackMode = GetDestination;
				hint("Click on location to face");
				break;
			}
		}
	}
}	// BattleEditControl::unitClicked();


void BattleEditControl::offMap()
{
	if(trackMode == Tracking)
		loseObject();
}

void BattleEditControl::setCurrentObject(MapObject* ob)
{
	loseObject();
	currentObject = ob;
	currentObject->highlight = 1;

	if( (mode == EDIT_Troops) && (ob->obType == OT_Unit))
		currentUnit = (Unit*)ob;

	/*
	 * Display Item's Info!
	 */

	currentObject->showInfo(infoArea);
}

void BattleEditControl::setCurrentObject(TreeObject* ob)
{
	loseObject();
	currentBattleObject = ob;
	ob->highlight = 1;

	/*
	 * Display Item's Info!
	 */

	currentBattleObject->showInfo(infoArea);
}

void BattleEditControl::loseObject()
{
	if(currentObject)
	{
		currentObject->highlight = 0;
		currentUnit = 0;
		clearInfo();
	}

	if(currentBattleObject)
	{
		currentBattleObject->highlight = 0;
		currentBattleObject->draw(data3D.view);
		currentBattleObject = 0;
		clearInfo();
	}
}

void BattleData::makeBattleField()
{
	data3D.light.set(Degree(30), 0, intensityRange/2, intensityRange/8); 	// North, 30 degree
	data3D.view.set(0, 0, 0x0000, 0x1555, Zoom_8);
}


/*======================================================
 * Draw hint line
 */

void BattleEditControl::hint(const char* fmt, ...)
{
	Region bm(machine.screen->getImage(), Rect(0,0,516,20));
	bm.fill(0x1d);
	bm.frame(0,0,516,20, 0x1f,0x50);

	char fmtBuffer[500];

	if(fmt == 0)
	{
		fmt = fmtBuffer;

		switch(mode)
		{
		case EDIT_Troops:
			strcpy(fmtBuffer, "Select a Unit");
			break;

		case EDIT_Objects:
			sprintf(fmtBuffer, "Placing: %s, Facing: %s",
				getTreeTypeName(defaultTreeType),
				getQuadName(defaultTreeFacing));
			break;

		case EDIT_Landscape:
			sprintf(fmtBuffer, "Editting %s, Height: %d, Radius: %d",
				hillFlat ? "Plateaus" : "Hills",
				(int) hillHeight,
				(int) hillRadius);
			break;

		case EDIT_Rivers:
			sprintf(fmtBuffer, "River height: %d, plain width: %d, curvature %d%%",
				(int) riverHeight,
				(int) riverWidth,
				(int) riverCurvature);
			break;
		case EDIT_Roads:
			sprintf(fmtBuffer, "Editting Roads, curvature: %d%%", (int) riverCurvature);
			break;
		case EDIT_Terrain:
			sprintf(fmtBuffer, "Editting Terrain, type: %s, size: %d, river: %s, road: %s",
				getTerrainStr(terrainType),
				(int) terrainSize,
				riverProtected ? "Protect" : "Overwrite",
				roadProtected ? "Protect" : "Overwrite");
			break;
		default:
			strcpy(fmtBuffer, "Battle Editor");
			break;
		}
	}

	if(fmt)
	{
		char buffer[500];

		va_list vaList;
		va_start(vaList, fmt);
		vsprintf(buffer, fmt, vaList);
		va_end(vaList);

		TextWindow win(&bm, Font_SimpleMessage);
		win.setFormat(True, True);
		win.setColours(Black);
		win.draw(buffer);
	}
	machine.screen->setUpdate(bm);
}

void BattleEditControl::setupInfo()
{
	infoArea->fill(0x1d);
	infoArea->frame(*infoArea, 0x1f, 0x50);

	textWin->setFormat(False, False);
	textWin->setColours(Black);
	textWin->setPosition(Point(2,2));
	textWin->setFont(Font_Title);

	machine.screen->setUpdate(*infoArea);
}

void BattleEditControl::clearInfo()
{
	setupInfo();

	textWin->setFont(Font_Title);
	textWin->setColours(Black);
	textWin->setFormat(True,True);
	textWin->draw("Nothing\rselected");
}

void MapObject::showInfo(Region* r)
{
	TextWindow& textWind = *control->textWin;

	control->setupInfo();

	textWind.setFont(Font_Title);
	textWind.setColours(Black);

	textWind.setFormat(False, True);
	textWind.setPosition(Point(4,4));
	textWind.draw("Unknown Object\r");

	textWind.setFormat(False, False);
	textWind.setLeftMargin(4);

	textWind.wprintf("Object Type %d\r", (int)obType);

	textWind.wprintf("at %ld,%ld\r", location.x, location.y);
}

/*===========================================================
 * Keep Linker happy
 */

MapPriority Army::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Army::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

MapPriority Corps::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Corps::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

MapPriority Division::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Division::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

MapPriority Brigade::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}


void Brigade::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

MapPriority Regiment::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Regiment::mapDraw(MapWindow* map, Region* bm, Point where)
{
}


MapPriority Unit::isVisible(MapWindow* map) const
{
	return Map_NotVisible;
}

void Unit::mapDraw(MapWindow* map, Region* bm, Point where)
{
}

void Unit::beforeBattle()
{
}

void Unit::afterBattle()
{
}

#if 0

void OrderBattle::setupBattle()
{
	for(Unit* p = sides; p; p = p->sister)
	{
		for(Unit* a = p->child; a; a = a->sister)
		{
			a->location = a->battleInfo->where;

			for(Unit* c = a->child; c; c = c->sister)
			{
				c->location = c->battleInfo->where;

				for(Unit* d = c->child; d; d = d->sister)
				{
					d->location = d->battleInfo->where;

					for(Unit* b = d->child; b; b = b->sister)
					{
						b->location = b->battleInfo->where;

						for(Unit* r = b->child; r;  r = r->sister)
						{
							r->location = r->battleInfo->where;

							RegimentBattle* rb = (RegimentBattle*) r->battleInfo;
							rb->setFormationRows(True);
						}
					}	// Brigades
				}	// Divisions
			}	// Corps
		}	// Armies
	}	// Sides
}
#endif

/*===============================================================
 * Main Loop
 */

void editBattle(const char* fileName)
{
	BattleEditControl menuData;

	menuData.init(fileName);

	while(!menuData.isFinished())
	{
		menuData.mouseOnMap = False;
		menuData.process();
		if(!menuData.mouseOnMap)
			menuData.offMap();
		menuData.display();
	}
}

void usage()
{
	throw GeneralError("\nBattle Editor usage:\n"
				"bated [options] [filename]\n"
				"Options are:\n"
#ifdef DEBUG
				"\t-m : Full Memory Log\n"
				"\t-ms : Short Memory Log\n"
				"\t-d : Full Debug Log\n"
				"\t-dn : Debug log of level n (n is a number)\n"
#endif
				"\t-? -h : Display this\n");
}



int main(int argc, char *argv[])
{
	const char* loadName = 0;

	try
	{
		/*
		 * Process command line
		 */

#ifdef DEBUG
		MemoryLog::Mode memMode = MemoryLog::None;
		int debugLevel = 0;
#endif

		int i = 0;
		while(++i < argc)
		{
			const char* arg = argv[i];

			if((arg[0] == '-') || (arg[0] == '/'))
			{
				switch(toupper(arg[1]))
				{
#ifdef DEBUG
				case 'M':										// -M for Memory Logging
					if(toupper(arg[2]) == 'S')				// -MS for memory summary
						memMode = MemoryLog::Summary;
					else
						memMode = MemoryLog::Full;
					break;

				case 'D':										// -D for debug log
					if(isdigit(arg[2]))
						debugLevel = atoi(&arg[2]);
					else
						debugLevel = 1;
					break;
#endif
				default:
					usage();
					break;
				}
			}
			else if(loadName)
				usage();
			else
				loadName = arg;
		}



		machine.init();
#ifdef DEBUG
		memLog->setMode("memory.log", memMode);
		if(debugLevel)
			logFile = new LogStream("debug.log", debugLevel);
#endif

		editBattle(loadName);
	}
 	catch(GeneralError e)
 	{
		cout << "Error: ";
		if(e.get())
			cout << e.get();
		else
			cout << "No description";
		cout << endl;
 	}
 	catch(...)
 	{
 		cout << "Caught some kind of error" << endl;
 	}

	return 0;
}



