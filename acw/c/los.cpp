/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Line of Sight calculations
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

// #define DEBUG_LOS

#include "los.h"
#include "batldata.h"
#include "map3d.h"

#ifdef DEBUG_LOS

#include "clog.h"

LogFile losLog("los.log", False);

#endif

/*
 * See if dest can be seen from viewer
 *
 * Return:
 *		True if viewer can see dest
 * 	False if blocked by something
 *
 * Implementation Notes:
 *   This can be speeded up by keeping a running pointer to the
 *   height and terrain grids.
 *
 * Also sunken roads are not taken into account yet.
 */

Boolean inLOS(Location viewer, Location dest, Distance minDistance)
{
	/*
	 * Bodge... things within 200 yards can always see each other
	 * even in thick forests.
	 */

	// const Distance minDistance = Yard(200);
	// const Distance minDistance = Yard(200);

	Distance d = distanceLocation(viewer, dest);
	if(d < minDistance)
		return True;

	/*
	 * Get the bottom level grid
	 */

	const Grid& g = *battle->grid->getGrid(Zoom_Min);
	const Terrain& terrain = battle->data3D.terrain;

	/*
	 * Convert coordinates into grid coordinates
	 */

	MapGridCord xFrom = battle->grid->cord3DtoGrid(DistToBattle(viewer.x));
	MapGridCord yFrom = battle->grid->cord3DtoGrid(DistToBattle(viewer.y));
	MapGridCord xTo	= battle->grid->cord3DtoGrid(DistToBattle(dest.x));
	MapGridCord yTo	= battle->grid->cord3DtoGrid(DistToBattle(dest.y));

#ifdef DEBUG_LOS
 	losLog.printf("---");
	losLog.printf("LOS (%d,%d) -> (%d,%d)",
		(int) xFrom, (int) yFrom,
		(int) xTo,   (int) yTo);
#endif

	/*
	 * Quick exit if same square
	 */

	if( (xFrom == xTo) && (yFrom == yTo) )
	{
		return !terrain.isWooded(g.getTerrain(xFrom, yFrom));
	}

	UBYTE hFrom = g.getHeight(xFrom, yFrom).height;
	UBYTE hTo	= g.getHeight(xTo, yTo).height;

	/*
	 * Bodge... make the viewer a bit taller
	 * applied twice to make LOS more lax
	 */

	if(hFrom < 0xff)
		hFrom++;
	if(hFrom < 0xff)
		hFrom++;


#ifdef DEBUG_LOS
	losLog.printf("hFrom=%d, hTo=%d", (int) hFrom, (int) hTo);
#endif

	/*
	 * Prepare to do bresenham line through the height/terrain grid
	 */


	WORD dx = xTo - xFrom;
	WORD dy = yTo - yFrom;
	WORD dh = hTo - hFrom;

	UWORD adx = abs(dx);
	UWORD ady = abs(dy);
	UWORD adh = abs(dh);

	WORD sx = sgn(dx);
	WORD sy = sgn(dy);
	WORD sh = sgn(dh);

	/*
	 * Step along x or y axis?
	 */

	if(adx >= ady)
	{	// Step along x axis
		UWORD val = adx/2;		// Y Step value
		WORD hDiv = adh / adx;
		UWORD hRem = adh % adx;
		UWORD hVal = adx/2;
		if(dh < 0)
			hDiv = -hDiv;

		while(xFrom != xTo)
		{
			xFrom += sx;

			/*
			 * Adjust Y coordinate
			 */

			val += ady;
			if(val >= adx)
			{
				yFrom += sy;
				val -= adx;
			}

			/*
			 * Adjust Height
			 */

			hFrom += hDiv;
			hVal += hRem;
			if(hVal >= adx)
			{
				hVal -= adx;
				hFrom += sh;
			}

			UBYTE h = g.getHeight(xFrom, yFrom).height;

			/*
			 * Adjust for terrain
			 */

			h += terrain.getHeightAdjust(g.getTerrain(xFrom, yFrom));


#ifdef DEBUG_LOS
				losLog.printf("height(%d,%d) = %d <-> %d",
					(int) xFrom, (int) yFrom,
					(int) hFrom, (int) h);
#endif

			if(h > hFrom)
			{
#ifdef DEBUG_LOS
				losLog.printf("Blocked by hill");
#endif
				return False;
			}
		}
	}
	else
	{	// Step along Y axis
		UWORD val = ady/2;
		WORD hDiv = adh / ady;
		UWORD hRem = adh % ady;
		UWORD hVal = ady/2;
		if(dh < 0)
			hDiv = -hDiv;

		while(yFrom != yTo)
		{
			yFrom += sy;

			/*
			 * Adjust Y coordinate
			 */

			val += adx;
			if(val >= ady)
			{
				xFrom += sx;
				val -= ady;
			}

			/*
			 * Adjust Height
			 */

			hFrom += hDiv;
			hVal += hRem;
			if(hVal >= ady)
			{
				hVal -= ady;
				hFrom += sh;
			}

			UBYTE h = g.getHeight(xFrom, yFrom).height;

			/*
			 * Adjust for terrain
			 */

			h += terrain.getHeightAdjust(g.getTerrain(xFrom, yFrom));


#ifdef DEBUG_LOS
				losLog.printf("height(%d,%d) = %d <-> %d",
					(int) xFrom, (int) yFrom,
					(int) hFrom, (int) h);
#endif

			if(h > hFrom)
			{
#ifdef DEBUG_LOS
				losLog.printf("Blocked by hill");
#endif
				return False;
			}
		}
	}

#ifdef DEBUG_LOS
 	losLog.printf("LOS clear");
#endif

	return True;
}



const TerrainVal* getTerrainType(const Location& where)
{
	Grid* gr = battle->grid->getGrid( Zoom_1 );

	MapGridCord lgx = battle->grid->cord3DtoGrid(DistToBattle(where.x));
	MapGridCord lgy = battle->grid->cord3DtoGrid(DistToBattle(where.y));

	TerrainType ter = gr->getTerrain(lgx, lgy);

	return battle->data3D.terrain.getValues( ter );
}


Cord3D getTerrainHeight(const Location& where)
{
	Grid* gr = battle->grid->getGrid( Zoom_1 );

	MapGridCord lgx = battle->grid->cord3DtoGrid(DistToBattle(where.x));
	MapGridCord lgy = battle->grid->cord3DtoGrid(DistToBattle(where.y));

	Height8 h = gr->getHeight(lgx, lgy);

	return h.getHeight();
}
