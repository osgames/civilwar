/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Graphical Test of random Number
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <dos.h>
#include <string.h>
#include <conio.h>
#include "vgascrn.h"
#include "random.h"

void main()
{
	struct videoconfig vconfig;
	_getvideoconfig( &vconfig );
	short oldmode = vconfig.mode;
	_setvideomode(_MRES256COLOR);
	unsigned char* screenPtr = (unsigned char* )(0xa000 << 4);

	memset(screenPtr, 0, 320*200);

	RandomNumber r(0x12345678);

	unsigned long count = 1000000;
	while(count-- && !kbhit())
	{
		screenPtr[r.getW(320*200)] = r.getB();
	}
	if(kbhit())
		getch();

	count = 1000000;
	while(count-- && !kbhit())
	{
		screenPtr[r.getW(320*200)]++;
	}
	if(kbhit())
		getch();

	count = 1000;
	while(count--)
	{
		unsigned short count1 = 320*200;
		while(count1-- && !kbhit())
		{
			screenPtr[count1] = r.getB();
		}
	}

	while(!kbhit())
		;
	getch();

	_setvideomode(oldmode);
}
