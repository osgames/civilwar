/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Palette Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/09/02  21:25:33  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/04/05  12:28:09  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/12/04  01:06:10  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <conio.h>
#include <string.h>

#include "palette.h"
#include "error.h"

/*
 * Borrowed from Super VGA Graphics Programming Secrets - Rimmer
 */

void Palette::set() const
{
	int i = 0;
	const TrueColour* c = colors;

	outp(0x3c6,0xff);

	while(i < 256)
	{
		outp(0x3c8, i++);
		outp(0x3c9, c->red >> 2);
		outp(0x3c9, c->green >> 2);
		outp(0x3c9, c->blue >> 2);
		c++;
	}
}

void setHardwarePalette(UBYTE start, UWORD howMany, TrueColour* colours)
{
	outp(0x3c6, 0xff);

	while(howMany--)
	{
		outp(0x3c8, start++);
		outp(0x3c9, colours->red   >> 2);
		outp(0x3c9, colours->green >> 2);
		outp(0x3c9, colours->blue  >> 2);
		colours++;
	}
}

void setHardwarePalette(UBYTE start, UWORD howMany, Colour64* colours)
{
	outp(0x3c6, 0xff);

	while(howMany--)
	{
		outp(0x3c8, start++);
		outp(0x3c9, colours->red);
		outp(0x3c9, colours->green);
		outp(0x3c9, colours->blue);
		colours++;
	}
}


void TrueColour::fromHSV(Hue h, Saturation s, Intensity v)
{
	/*
	 * Normalise intensity
	 */

	UBYTE i = UBYTE((v * 256) / intensityRange);


	if(s == 0)		// Its grey!
	{
		red = i;
		green = i;
		blue = i;
	}
	else
	{
		int band = h / (256/6);		// which primary/secondary colour
		UBYTE f = 6 * UBYTE(h % (256/6));
		UBYTE p = UBYTE((v * (256 - s)) / intensityRange);
		UBYTE q = UBYTE((v * (65536 - (s * f))) / (intensityRange * 256));
		UBYTE t = UBYTE((v * (65536 - (s * (256 - f)))) / (intensityRange * 256));

		if(band >= 6)
			band -= 6;

		switch(band)
		{
		case 0:		// Red..Yellow
			red 	= i;
			green = t;
			blue 	= p;
			break;
		case 1:		// Yellow..green
			red 	= q;
			green = i;
			blue 	= p;
			break;
		case 2:		// green..cyan
			red 	= p;
			green = i;
			blue 	= t;
			break;
		case 3:		// cyan..blue
			red 	= p;
			green = q;
			blue 	= i;
			break;
		case 4:		// blue..purple
			red 	= t;
			green = p;
			blue 	= i;
			break;
		case 5:		// purple..red
			red 	= i;
			green = p;
			blue 	= q;
			break;
		default:
			throw GeneralError("Illegal colour band in TrueColour::fromHSV");
		}
	}
}

static UBYTE HSLValue(UBYTE n1, UBYTE n2, Hue h)
{
	if(h < (256/6))
		return UBYTE(n1 + ((n2 - n1) * h) / (256/6));
	else if(h < 128)
		return n2;
	else if(h < (512 / 3))
		return UBYTE(n1 + ((n2 - n1) * ((512/3) - h)) / (256/6));
	else
		return n1;
}

void TrueColour::fromHSL(Hue h, Saturation s, Intensity v)
{
	UBYTE m1;
	UBYTE m2;

	if(v <= (intensityRange / 2))
		m2 = UBYTE( (v * (256 + s)) / intensityRange);
	else
		m2 = UBYTE( (v * (256 - s)) / intensityRange + s);
	m1 = UBYTE((256 * 2 * v) / intensityRange - m2);

	if(s == 0)
		red = green = blue = UBYTE((v * 256) / intensityRange);
	else
	{
		red = HSLValue(m1, m2, h + 256/3);
		green = HSLValue(m1, m2, h);
		blue = HSLValue(m1, m2, h + 512/3);
	}

}


/*
 * Fade a palette towards a new palette
 * Also updating hardware values
 * Returns True if there was a change
 */

Boolean Palette::fadeTo(const Palette* src, int ratio)
{
	Boolean changed = False;

	outp(0x3c6, 0xff);		// Get the palette register to wait for new colours
	
	TrueColour* c1 = colors;
	const TrueColour* c2 = src->colors;

	for(int i = 0; i < 256; i++)
	{
		Boolean update = False;
	
		if(c1->red != c2->red)
		{
			c1->red += (c2->red - c1->red) / ratio;
			update = True;
		}
		if(c1->green != c2->green)
		{
			c1->green += (c2->green - c1->green) / ratio;
			update = True;
		}
		if(c1->blue != c2->blue)
		{
			c1->blue += (c2->blue - c1->blue) / ratio;
			update = True;
		}


		if(update)
		{
			changed = True;

			outp(0x3c8, i);		// Colour number
			outp(0x3c9, c1->red >> 2);
			outp(0x3c9, c1->green >> 2);
			outp(0x3c9, c1->blue >> 2);

		}

		c1++;
		c2++;
	}

	return changed;
}

void Palette::setBlack()
{
	memset(colors, 0, sizeof(colors));
}

void Palette::setColours(int c, TrueColour* t, int count)
{
	TrueColour* dest = &colors[c];
	while(count--)
		*dest++ = *t++;
}

void Palette64::setColours(int c, TrueColour* t, int count)
{
	Colour64* dest = &colors[c];
	while(count--)
	{
		dest->red = t->red >> 2;
		dest->green = t->green >> 2;
		dest->blue = t->blue >> 2;

		dest++;
		t++;
	}
}
