/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Combat
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "campaign.h"
#include <stdio.h>
#include "campcomb.h"
#include "campwld.h"
#include "camptab.h"
#include "calcbat.h"
#include "game.h"

#ifdef DEBUG
#include "clog.h"
#endif

/*
 * Recursively set occupying variable for an organisation
 */

void Unit::setOccupy(Facility* f, SiegeMode mode, Boolean clearInBat )
{
#ifdef TESTING_NO
	if(getRank() == Rank_Regiment)
		throw GeneralError("Regiment %s occupying %s",
			getName(True),
			f->getNameNotNull());
#endif

	if( (mode != siegeAction) && (f != occupying) )
	{
		Unit* u = this;

		/*
	 	 * New code... make sure to clear facility if vacating
	 	 */

		if(u->occupying && (u->siegeAction == SIEGE_Occupying))
		{
			Facility* f = u->occupying;
			if(f->occupier == u)
				f->occupier = 0;
		}

		u->occupying = f;
		u->siegeAction = mode;

#if 0
		if(!clearInBat)
			u->inBattle = True;				// Prevent from being reprocessed this frame
		else
			u->inBattle = False;
#else
		if(clearInBat)
			u->inBattle = False;
#endif

		if(f)
		{
			if(mode == SIEGE_Occupying)
				u->location = f->location;
			u->realOrders.destination = f->location;
			u->givenOrders.destination = f->location;

			// Removed 30th August as destination doesn't exist.
			// u->destination = f->location;
		}

#if 0	// Only top guy to occupy?
		if(u->childCount)
		{
			Unit* c = child;
			while(c)
			{
				if(!c->isCampaignReallyIndependant())
					c->setOccupy(f, mode);
				c = c->sister;
			}
		}
#endif
	}
}

/*
 * Tell a unit to do something to a facility
 */

void actOnCity(Facility* f, Unit* u, SiegeMode mode)
{
	if(u->getRank() == Rank_Regiment)
#ifdef TESTING
		throw GeneralError("Regiment %s trying to act on %s",
			u->getName(True),
			f->getName());
#else
		return;
#endif

	u = u->getTopCommand();

	/*
	 * Do nothing if no change!
	 */

	if((u->siegeAction != mode) || (u->occupying != f))
	{
#ifdef DEBUG
		static char* siegeStr[] = {
			"None",
			"Occupy",
			"Siege",
			"Stand"
		};

		cLog.printf("actOnCity(%s, %s, %s)",
			f->getNameNotNull(),
			u->getName(True),
			siegeStr[mode]);
#endif

		Boolean clearFlag = False;

#ifdef DEBUG
		game->simpleMessage("%s at %s\rchanging mode from %s to %s",
			u->getName(False),
			f->getNameNotNull(),
			siegeStr[u->siegeAction],
			siegeStr[mode]);
		cLog.printf("%s at %s, changing mode from %s to %s",
			u->getName(True),
			f->getNameNotNull(),
			siegeStr[u->siegeAction],
			siegeStr[mode]);
#endif

		/*
		 * Undo previous mode
		 */

		if(u->siegeAction == SIEGE_Occupying)
		{
			// f->occupier = 0;
			u->occupying->occupier = 0;
		}
		else if(u->siegeAction == SIEGE_Besieging)
		{
			u->occupying->sieger = 0;
			// f->sieger = 0;
		}
#if 0
		else
		{
			u->unTieAll();
		}
#endif

		/*
		 * Set up for new mode
		 */

		if(mode == SIEGE_Occupying)
		{
			/*
			 * Remove any previous enemy tenent!
			 */

			if(f->occupier)
			{
				if(f->occupier->superior)		// Bodge.. bodge...
				{
					if(f->side != u->getSide())
					{
						actOnCity(f, f->occupier, SIEGE_None);
					}
					else
					{
#if 0
#ifdef DEBUG
						cLog.printf("%s and %s amalgamate at %s",
							f->occupier->getName(True),
							u->getName(True),
							f->getNameNotNull());
#endif
						u = joinForces(f->occupier, u);
#endif
					}
				}
#ifdef DEBUG
				else
					cLog.printf("Warning %s has a dead occupier",
						f->getNameNotNull());
#endif
			}

			setCitySide(f, u->getSide());

			if(u->getRank() == Rank_Regiment)
#ifdef TESTING
				throw GeneralError("Regiment %s trying to act on %s",
					u->getName(True),
					f->getName());
#else
				return;
#endif

			f->occupier = u;

			f->side = u->getSide();
		}

		else if(mode == SIEGE_Besieging)
		{
#if 0
			if(f->sieger)
				u = joinForces(f->sieger, u);

			f->sieger = u;
#else
			if(!f->sieger)
				f->sieger = 0;
#endif
		}
		else if(mode == SIEGE_None)
		{
			f = 0;
			clearFlag = True;
		}

		u->setOccupy(f, mode, clearFlag);

		if (!f ) u->inBattle = False;
	}
}

/*
 * Take over, but do not occupy a facility
 */

void takeOverCity(Facility* f, Unit * b)
{
	f->side = b->getSide();
#ifdef DEBUG
	game->simpleMessage("%s has been taken over by %s",
		f->getNameNotNull(),
		b->getName(False));
	cLog.printf("%s has been taken over by %s",
		f->getNameNotNull(),
		b->getName(True));
#endif

	if ( f->side == SIDE_CSA )
	{
		campaign->CSAVictory -= f->getBigVictory();
		campaign->USAVictory += f->getBigVictory();
	}
	else
	{
		campaign->CSAVictory += f->getBigVictory();
		campaign->USAVictory -= f->getBigVictory();
	}

}

/*
 * Attempt to occupy an occupied facility
 */

void attemptOccupyCity(Facility* f, Unit* b)
{
#ifdef DEBUG
	game->simpleMessage("%s attempts to occupy %s",
		b->getName(False),
		f->getNameNotNull());
	cLog.printf("%s attempts to occupy %s",
		b->getName(True),
		f->getNameNotNull());
#endif

  	MarkedBattle* batl = campaign->world->makeBattle(f, b, False);
	Boolean cresult = calcFortBattle(batl, f);
	// campaign->world->IncExperience( batl );
	batl->updateExperience(5);

	if(!cresult)
	{
		if ( f->occupier )
		{
			Unit* u = f->occupier;

			u->setOccupy(0, SIEGE_None, True);

			u->inBattle = False;

			/*
			 * If unit has hold orders, then make them get out of town.
			 */

			if(u->realOrders.basicOrder() == ORDERMODE_Hold)
			{
				withdrawUnit(u, f->location, cityBattleDistance * 2);
			}

#ifdef CHRIS_AI
			campaign->CheckAI(u);
#endif
		}

		actOnCity(f, b, SIEGE_Occupying);
	}
	else
		actOnCity(f, b, SIEGE_Besieging);
}

void fightFortifications(Facility* f, Unit* b)
{
	Unit* u = b->getTopCommand();

#ifdef DEBUG
	game->simpleMessage("%s fights\rfortifications at %s",
		u->getName(False),
		f->getNameNotNull());
	cLog.printf("%s fights fortifications at %s",
		u->getName(True),
		f->getNameNotNull());
#endif

	// int result = rand(100) < 50;
	
	/*
	 * 	Make up a temp MarkedBattle and pass it to calcFortBattle;
	 */

	MarkedBattle* batl = campaign->world->makeBattle(f, b, False);

	int cresult = calcFortBattle( batl, f );

	// campaign->world->IncExperience( batl );
	batl->updateExperience(5);

	if(!cresult)
	{
		if ( f->occupier ) 
		{
			Unit* u1 = f->occupier;
			u1->setOccupy( 0, SIEGE_None, True);
				 
			u1->inBattle = False;

			withdrawUnit(u1, u->location, cityBattleDistance * 2);
		}

		actOnCity(f, u, SIEGE_Occupying);
	}
	else
		actOnCity(f, u, SIEGE_Besieging);
}


void Unit::setInBattle()
{
	Unit* u = this;

#if 0
	do
	{
#endif
		u->inBattle = True;

		if(u->childCount)
		{
			Unit* c = child;
			while(c)
			{
				// if(!c->isCampaignReallyIndependant())
				if(c->getCampaignAttachMode() == Attached)
					c->setInBattle();
				c = c->sister;
			}
		}
#if 0
		u = u->tied;
	} while(u && (u != this));
#endif
}

void CampaignWorld::processCloseCity(Facility* f, Unit* b)
{
	// Distance dLoc = distanceLocation(f->location, b->location);

	b = b->getTopCommand();

	/*
	 * What to do...
	 *
	 * A) Is facility friendly or enemy?
	 */

	Side bSide = b->getSide();
	Distance dOrder = distanceLocation(f->location, b->realOrders.destination);
	Boolean orderedToCity = (dOrder < cityWidth);

	if(onOtherSide(f->side, bSide))
	{
		/*
		 * Enemy controlled city
		 */

		if(f->occupier)
		{
			/*
			 * Occupied enemy city
			 */

#ifdef DEBUG
			game->simpleMessage("%s encounters\renemy occupied city at %s",
				b->getName(False),
				f->getNameNotNull());
			cLog.printf("%s encounters enemy occupied city at %s",
				b->getName(True),
				f->getNameNotNull());
#endif

			/*
			 * Players should be given option to change orders
			 */

			switch(f->occupier->realOrders.basicOrder())
			{
			case ORDERMODE_Advance:
				switch(b->realOrders.basicOrder())
				{
				case ORDERMODE_Advance:
					makeBattle(f, b, True);
					break;

				case ORDERMODE_Hold:
					makeBattle(f, b, True);
					break;

				case ORDERMODE_Withdraw:
					break;
				}
				break;
			case ORDERMODE_Hold:
				switch(b->realOrders.basicOrder())
				{
				case ORDERMODE_Advance:
					if(b->realOrders.mode == ORDER_CautiousAdvance)
					{
						actOnCity(f, b, SIEGE_Besieging);
					}
					else
					{
						// Do a calculated battle
						attemptOccupyCity(f, b);
					}
					break;

				case ORDERMODE_Hold:
					break;
				case ORDERMODE_Withdraw:
					break;
				}
				break;
			case ORDERMODE_Withdraw:
				switch(b->realOrders.basicOrder())
				{
				case ORDERMODE_Advance:
					// f->occupy(b);
					actOnCity(f, b, SIEGE_Occupying);
					break;

				case ORDERMODE_Hold:
					break;

				case ORDERMODE_Withdraw:
					break;
				}
				break;
			}

		}
		else
		{
			/*
			 * Unoccupied enemy city
			 *
			 * IF we have attack/advance orders then fight
			 * against fortifications.
			 * ELSE
			 *   IF order destination is here..
			 *      Stand
			 *   ELSE
			 *      Walk around it on way to destination
			 */

#if 0	// Still fight even if unfortified
			if(f->fortification)
			{
#endif
				if( (b->realOrders.mode == ORDER_Advance) ||
						(b->realOrders.mode == ORDER_Attack) )
				{
					/*
					 * Fight against fortifications
					 *
					 * Result will be a siege or occupy
					 */

					fightFortifications(f, b);

				}
				else if(orderedToCity)
				{
					b->getTopCommand()->setOrder(ORDER_CautiousAdvance, b->location);

#ifdef CHRIS_AI
					campaign->CheckAI( b );
#endif


					// b->getTopCommand()->setOccupy(f, SIEGE_Standing);
					actOnCity(f, b, SIEGE_Standing);
#ifdef DEBUG
					game->simpleMessage("%s is\rstanding near %s",
						b->getName(False),
						f->getNameNotNull());
					cLog.printf("%s is standing near %s",
						b->getName(True),
						f->getNameNotNull());
#endif
				}
				else
				{
					;	// Walk around it?
				}
		}

	}
	else
	{
		/*
		 * Friendly or neutral city
		 */

		if(f->sieger &&
				((b->realOrders.mode == ORDER_Advance) ||
					(b->realOrders.mode == ORDER_Attack)))
		{
			/*
			 * City is under siege...
			 * Player should be given option to
			 * change orders of the unit's currently
			 * in the city to help the new unit in its
			 * battle.
			 */

#ifdef DEBUG
			game->simpleMessage("%s helps\rsieged city %s",
				b->getName(False),
				f->getNameNotNull());
			cLog.printf("%s (%s) helps\rsieged city %s",
				b->getName(True),
				b->getTopCommand()->getName(True),
				f->getNameNotNull());

#endif

		}
		else 
		if(orderedToCity)
		{
			/*
			 * Only replace friendly occupier if rank is greater
			 */

			if( (f->occupier == 0) || (b->getRank() > f->occupier->getRank()))
				actOnCity(f, b, SIEGE_Occupying);
		}

		// Otherwise, just ignore it...

	}
}


/*
 * Check if a brigade should consider battling against a city
 */

void CampaignWorld::checkCityBattles(Brigade* b)
{
	/*
	 * Don't bother if on a boat or train
	 */

	if( (b->moveMode == MoveMode_Marching) || (b->moveMode == MoveMode_Standing) )
	{
		if(facilities.entries())
		{
			FacilityIter list = facilities;

			Facility* closest = 0;
			Distance bestDist;

			while(++list)
			{
				Facility* f = list.current();

				if(!f->isRealTown())
					continue;

				Distance d = distanceLocation(f->location, b->location);

				if(d < CampaignVisibleDistance && (b->getSide() == otherSide(f->side)))
				 	b->getTopCommand()->canBeSeen = True;

				if( (d < cityBattleDistance) && (!closest || (d < bestDist)))
				{
					/*
					 * Find out if moving towards it!
					 */

					Boolean isClose = False;

					if(d < cityWidth)
						isClose = True;
					else if(b->moved)
					{
						/*
					 	 * Get relative angle from city
					 	 */

						Wangle cityDir = directionLocation(b->location, f->location);
						cityDir -= b->facing;
						if(cityDir >= 0x8000)
							cityDir = -cityDir;

						/*
					 	 * Get arc that is considered to be towards city
					 	 */

						Wangle arc = direction(cityWidth, d);

						if(cityDir < arc)
							isClose = True;
					}

					if(isClose)
					{
#ifdef DEBUG1
						game->simpleMessage("%s is\r%d Yards from %s",
							b->getName(False),
							unitToYard(d),
							f->getNameNotNull());
						cLog.printf("%s is %d Yards from %s",
							b->getName(True),
							unitToYard(d),
							f->getNameNotNull());
#endif
						closest = f;
						bestDist = d;

					}
				}
			}

			if(closest)
				processCloseCity(closest, b);
		}
	}
}

/*
 * Check if a brigade should consider fighting against another unit
 */

void CampaignWorld::checkUnitBattles(Brigade* brigade)
{
	/*
	 * Don't bother if on a boat or train
	 */

	BasicOrder basicMode = brigade->realOrders.basicOrder();

	// if( (basicMode == ORDERMODE_Advance) || (basicMode == ORDERMODE_Hold) )

	if( ((brigade->moveMode == MoveMode_Marching) || (brigade->moveMode == MoveMode_Standing)) &&
		 (basicMode == ORDERMODE_Advance))
	{

		President* pres = ob.getPresident(otherSide(brigade->getSide()));
		for(Unit* army = pres->child; army; army = army->sister)
			for(Unit* corps = army->child; corps; corps = corps->sister)
				for(Unit* div = corps->child; div; div = div->sister)
					for(Brigade* b = (Brigade*)div->child; b; b = (Brigade*)b->sister)
					{
						/*
						 * At least one side must be advancing to trigger a battle
						 */

						if( ((basicMode == ORDERMODE_Advance) || (b->realOrders.basicOrder() == ORDERMODE_Advance)) &&
							 ((b->moveMode == MoveMode_Marching) || (b->moveMode == MoveMode_Standing)) )
						{
							Distance d = distanceLocation(brigade->location, b->location);

							if(d < CampaignVisibleDistance )
								b->getTopCommand()->canBeSeen = True;

							if(d < brigadeBattleDistance)
							{
								Boolean triggerBattle = False;

								if(d < brigadeWidth)
										triggerBattle = True;
								else if(b->moved)
								{
									Wangle dir = directionLocation(brigade->location, b->location);
									dir -= brigade->facing;
									if(dir >= 0x8000)
										dir = -dir;
									Wangle arc = direction(brigadeWidth, d);

									if(dir < arc)
										triggerBattle = True;
								}

								if(triggerBattle)
								{
#ifdef DEBUG
									game->simpleMessage("%s is\r%d Yards from %s",
										brigade->getName(False),
										unitToYard(d),
										b->getName(False));
									cLog.printf("%s is %d Yards from %s",
										brigade->getName(True),
										unitToYard(d),
										b->getName(True));
#endif

									if( ( (b->realOrders.basicOrder() == ORDERMODE_Advance) ||
									 	(b->realOrders.basicOrder() == ORDERMODE_Hold) ) &&
								  	( (b->realOrders.mode == ORDER_Advance) ||
									 	(b->realOrders.mode == ORDER_Attack) ||
								    	(brigade->realOrders.mode == ORDER_Advance) ||
									 	(brigade->realOrders.mode == ORDER_Attack) ) )
									{
#ifdef DEBUG
										cLog.printf("Battle between %s and %s",
											brigade->getName(True),
											b->getName(True));
#endif
										makeBattle(b, brigade);
										return;
									}
									else if (b->realOrders.basicOrder() != ORDERMODE_Withdraw )
									{
										/*
									 	 * Alter path to avoid enemy or Stand?
									 	 */

#ifdef DEBUG
										game->simpleMessage("%s stands to avoid %s",
											brigade->getName(False),
											b->getName(False));
										cLog.printf("%s stands to avoid %s",
											brigade->getName(True),
											b->getName(True));
#endif
										Unit* u = (Unit* )brigade;

										// while ( !u->isCampaignReallyIndependant() )
										while (u->getCampaignAttachMode() == Attached)
											u = u->superior;

										if ( u->realOrders.mode != ORDER_Stand ) 
										{
											u->setOrder(ORDER_Stand, u->location);
#ifdef CHRIS_AI
											campaign->CheckAI( u );
#endif
										}

										brigade->setInBattle();
										b->setInBattle();
										return;
									}
								}
							}
						}
					}
	}
}

void withdrawUnit(Unit* unit, const Location& where, Distance howFar)
{
	Location tloc = unit->location;

	Wangle whichWay;
	
	if(where == tloc)
		whichWay = game->gameRand();	// automatic convert to word
	else
		whichWay = directionLocation(tloc, where);

	tloc.x += msin(howFar, whichWay);
	tloc.y += mcos(howFar, whichWay);

#ifdef DEBUG
	cLog.printf("%s's order changed to Withdraw (%ld miles at %d degrees)",
		unit->getName(True),
		unitToMile(howFar),
		(int) (whichWay * 360) / 0x10000);
#endif


	unit->setOrderNow(Order(ORDER_Withdraw, ORDER_Land, tloc));
	unit->clearInBattle();
}

/*
 * Set the side of a facility
 * If it is changing side then:
 *		update victory points
 *		remove half built items
 *		destroy rail head
 *		Reduce resources
 */

void setCitySide(Facility* f, Side side)
{
	/*
	 * If it is a change of side then update victory points
	 * and remove facilities.
	 */

	if(f->side != side)
	{
		if(side == SIDE_CSA)
		{
			campaign->CSAVictory += f->getBigVictory();
			campaign->USAVictory -= f->getBigVictory();
		}
		else
		{
			campaign->CSAVictory -= f->getBigVictory();
			campaign->USAVictory += f->getBigVictory();
		}

#if 0
		if(f->side == SIDE_USA)
		{
		}
		else		// Neutral or CSA
		{
			campaign->CSAVictory -= f->getBigVictory();
			campaign->USAVictory += f->getBigVictory();
		}
#endif

		/*
		 * Remove any half built facilities and destroy rail capacity.
		 * ONLY if it is an enemy city (i.e. in enemy state)
		 */

		// if((f->side != SIDE_None) && (campaign->world->states[f->state].side != side))

		if(f->side != SIDE_None)
		{
			if(f->railCapacity)
			{
				f->railCapacity = 0;
				f->freeRailCapacity = 0;
			}
		
			campaign->world->mobList.removeAll(f);

			/*
			 * Reduce resources
			 */

			if(f->facilities & Facility::F_City)
			{
				City* city = (City*) f;
				city->resource /= 4;
			}
		}

		f->side = side;

	}
}
