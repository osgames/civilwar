/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Height editor for battle map
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdlib.h>
#include <stdio.h>

#include "bated.h"
#include "map3d.h"
#include "dialogue.h"
#include "text.h"
#include "system.h"
#include "mouselib.h"

static char const* terrainNames[] = {

	"Built Up",
	"Dense Woods",
	"Light Woods",
	"Dense Orchard",
	"Light Orchard",
	"Pasture",
	"Ploughed Field",
	"Corn Field",
	"Wheat Field",
	"Meadow",
	"Camp Site",
	"Water",
	"Marsh",
	"Rough",
	"Rocky Desert",
	"Desert",

	"Off Map",

	// These next ones are special case used to create rivers/roads

	"River Block",
	"Surface Road Block",
	"Sunken Road Block",
	"Rail Block",

	0
};

const char* getTerrainStr(UBYTE t)
{
	// if(t > BASE_TERRAIN_COUNT)
	//	t = battle->data3D.terrain.getBaseTerrain(t);

	if(t >= TerrainCount)
		return "Illegal";

	int special = NormalTerrainCount - NormalTerrain;

	if(t >= NormalTerrain)
		t -= NormalTerrain;
	else if(isWater(t))
		t = special;
	else if(isRoad1(t))
		t = special + 1;
	else if(isRoad2(t))
		t = special + 2;
	else if(isRail(t))
		t = special + 3;

	return terrainNames[t];
}

TerrainType makeTerrainType(UBYTE n)
{
	int special = NormalTerrainCount - NormalTerrain;

	if(n == special)
		return RiverTerrain;
	else if(n == (special + 1))
		return Road1Terrain;
	else if(n == (special + 2))
		return Road2Terrain;
	else if(n == (special + 3))
		return RailwayTerrain;
	else
		return n + NormalTerrain;
}


void BattleEditControl::heightOverMap(const Event* event)
{
	MapGridCord gx;
	MapGridCord gy;

	if(gotMouseLocation)
	{
		gx = grid->cord3DtoGrid(mouseLocation.x);
		gy = grid->cord3DtoGrid(mouseLocation.z);
	
		Grid* g = grid->getGrid(Zoom_Min);

		Height8 h = *g->getHSquare(gx, gy);
		setupInfo();
		TextWindow& textWind = *textWin;
		textWind.setFont(Font_JAME08);
		textWind.wprintf("Height:\r%d Units\r%d yards",
			(int) h.height,
			(int) unitToYard(BattleToDist(h.getHeight())));
	}
	else
		clearInfo();

	if(gotMouseLocation && event->buttons)
	{
		storeGrid();

		Grid* g = grid->getGrid(Zoom_Min);

		MapGridCord x = gx;
		MapGridCord y = gy;

		if(hillFlat)
			g->makePlateau(x, y, hillHeight, hillRadius);
		else
			g->makeHill(x, y, hillHeight, hillRadius, event->buttons & Mouse::LeftButton);
		grid->makeZoomedGrids(data3D);
		updateView();
		changed = True;
	}
}

static char p_height[4];	// Also used for river Height
static char p_radius[4];	// Also used for river width
static char p_rough[4];		// Also used for river curvature %
static char p_hYards[] = "123456 Yards";
static char p_rYards[] = "123456 Yards";

char flatStr[] = "Plateau";
char hillStr[] = "Hill / Hole";

DialItem hillParameterItems[] = {
	DialItem(Dial_Button,		0,		Rect(0,0,256,16), "Set Hill Parameters"),
	DialItem(Dial_Button,		0,		Rect(0,16,128,16),	"Height (1..255):"),
	DialItem(Dial_NumberInput,	1,		Rect(128,16,48,16),	p_height, sizeof(p_height)),
	DialItem(Dial_Button,		0,		Rect(176,16,80,16),	p_hYards),
	DialItem(Dial_Button,		0,		Rect(0,32,128,16),	"Radius (1..128)"),
	DialItem(Dial_NumberInput,	2,		Rect(128,32,48,16),	p_radius, sizeof(p_radius)),
	DialItem(Dial_Button,		0,		Rect(176,32,80,16),	p_rYards),
	DialItem(Dial_Button,		0,		Rect(0,48,128,16),	"Roughness (1..255)"),
	DialItem(Dial_NumberInput,	3,		Rect(128,48,48,16),	p_rough, sizeof(p_rough)),
	DialItem(Dial_Button,      4,		Rect(0,64,256,16),	0),
	DialItem(Dial_Button, 		-1,  	Rect( 32,80, 64,16), "Cancel"),
	DialItem(Dial_Button, 		-2,  	Rect(160,80, 64,16), "OK"),
	DialItem(Dial_End)
};

class HillParameterDial : public Dialogue {
public:
	HillParameterDial();
	void doIcon(DialItem* item, Event* event, MenuData* d);

	Boolean flat;
};

HillParameterDial::HillParameterDial()
{
	Dialogue::setup(hillParameterItems);
}

void makeHeightString(char* dest, UBYTE h)
{
	Height8 h8;

	h8.height = h;

	sprintf(dest, "%d Yards", (int) unitToYard(BattleToDist(h8.getHeight())));
}

void makeGridYardString(char* dest, UBYTE size)
{
	Grid* g = battle->grid->getGrid(Zoom_Min);

	sprintf(dest, "%d Yards", (int) unitToYard(BattleToDist(size * g->gridSize)));
}

void HillParameterDial::doIcon(DialItem* item, Event* event, MenuData* d)
{
	switch(item->id)
	{
		case -1:
		case -2:
			if(event->buttons)
				d->setFinish(item->id);
			break;
		case 1:
			if(event->buttons || event->key)
			{
				int n = atoi(p_height);

				if( (n > 0) && (n <= 0xff))
					makeHeightString(p_hYards, n);
				d->setRedraw();
			}
			break;
		case 2:
			if(event->buttons || event->key)
			{
				int n = atoi(p_radius);

				if( (n > 0) && (n <= 128))
					makeGridYardString(p_rYards, n);
				d->setRedraw();
			}
			break;
		case 4:
			if(event->buttons)
			{
				flat = !flat;
				item->text = flat ? flatStr : hillStr;
				d->setRedraw();
			}
			break;
	}
}

void BattleEditControl::setHillParameters()
{
	sprintf(p_height, "%d", (int) hillHeight);
	sprintf(p_radius, "%d", (int) hillRadius);
	sprintf(p_rough,  "%d", (int) hillRough);
	makeHeightString(p_hYards, hillHeight);
	makeGridYardString(p_rYards, hillRadius);

	hillParameterItems[9].text = hillFlat ? flatStr : hillStr;

	HillParameterDial dial;
	dial.flat = hillFlat;

	if(dial.process() == -2)	// OK Pressed
	{
		int n = atoi(p_height);

		if((n > 0) && (n <= 0xff))
			hillHeight = n;

		n = atoi(p_radius);
		if((n > 0) && (n <= 128))
			hillRadius = n;

		n = atoi(p_rough);
		if((n > 0) && (n <= 0xff))
			hillRough = n;

		hillFlat = dial.flat;

		hint(0);
	}
}

void BattleEditControl::heightOptions()
{
	static const char* choices[] = {
		"Flatten All",
		"Smooth",
		"Roughen",
		"Randomise",
		"Set Hill Parameters",
		"Undo Last change",
		0
	};

	Boolean gridChanged = False;
	Grid* g = grid->getGrid(Zoom_Min);

	switch(chooseFromList(0, "Height Adjustment", choices, "Cancel"))
	{
	case 1:	// Flatten
		storeGrid();
		g->flatten(0);
		gridChanged = True;
		break;
	case 2:	// Smooth
		storeGrid();
		g->smooth();
		gridChanged = True;
		break;
	case 3:	// Roughen
		storeGrid();
		g->roughen(hillRough);
		gridChanged = True;
		break;
	case 4:	// Randomise
		storeGrid();
		grid->create(data3D, hillHeight, hillRadius);
		gridChanged = True;
		break;
	case 5:	// Set parameters
		setHillParameters();
		break;
	case 6:
		undoGrid();
		break;
	}

	if(gridChanged)
	{
		grid->makeZoomedGrids(data3D);
		updateView();
		changed = True;
	}
}

/*
 * Adjust the coordinate to be either
 *   the nearest edge
 * or
 *   the nearest location already containing water
 */


	/*
	 * 1st find closest edge
	 */

MapGridCord Grid::findCloseEdge(MapGridCord& x, MapGridCord& y, Boolean endPoint)
{
	MapGridCord x1;
	MapGridCord y1;

	MapGridCord d;

	if(x < (gridCount / 2))
	{
		if(endPoint)
			x1 = -1;
		else
			x1 = 0;
		y1 = y;
		d = x;
	}
	else
	{
		if(endPoint)
			x1 = gridCount;
		else
			x1 = gridCount - 1;
		y1 = y;
		d = x1 - x;
	}

	if(y < (gridCount / 2))
	{
		if(y < d)
		{
			x1 = x;
			if(endPoint)
				y1 = -1;
			else
				y1 = 0;
			d = y;
		}
	}
	else
	{
		MapGridCord d1 = gridCount - y;

		if(d1 < d)
		{
			x1 = x;
			if(endPoint)
				y1 = gridCount;
			else
				y1 = gridCount - 1;
			d = d1;
		}
	}

	x = x1;
	y = y1;

	return d;
}

void Grid::adjustRiverCord(MapGridCord& x, MapGridCord& y, Boolean endPoint, MapGridCord range)
{
	MapGridCord x1 = x;
	MapGridCord y1 = y;

	MapGridCord d = findCloseEdge(x1, y1, endPoint);

	/*
	 * Now search terrain for close water
	 *
	 * Do it brute force!
	 */

	for(int y2 = max(0, y-d); (y2 < (y + d)) && (y2 < gridCount); y2++)
	{
		for(int x2 = max(0, x-d); (x2 < (x + d)) && (x2 < gridCount); x2++)
		{
			if(isWater(*getTSquare(x2, y2)))
			{
				MapGridCord d2 = distance(x2 - x, y2 - y);
				if(d2 < d)
				{
					x1 = x2;
					y1 = y2;
					d = d2;
				}
			}
		}
	}

	if(d < range)
	{
		x = x1;
		y = y1;
	}
}

void Grid::adjustRoadCord(MapGridCord& x, MapGridCord& y, Boolean endPoint, MapGridCord range)
{
	MapGridCord x1 = x;
	MapGridCord y1 = y;

	MapGridCord d = findCloseEdge(x1, y1, endPoint);

	/*
	 * Now search terrain for close water
	 *
	 * Do it brute force!
	 */

	for(int y2 = max(0, y-d); (y2 < (y + d)) && (y2 < gridCount); y2++)
	{
		for(int x2 = max(0, x-d); (x2 < (x + d)) && (x2 < gridCount); x2++)
		{
			if(isRoad(*getTSquare(x2, y2)))
			{
				MapGridCord d2 = distance(x2 - x, y2 - y);
				if(d2 < d)
				{
					x1 = x2;
					y1 = y2;
					d = d2;
				}
			}
		}
	}

	if(d < range)
	{
		x = x1;
		y = y1;
	}
}


void BattleEditControl::riverOverMap(const Event* event)
{
	if(trackMode == Tracking)
	{
		if(event->buttons & Mouse::LeftButton)
		{
			if(gotMouseLocation)
			{
				startLocation = mouseLocation;
				hint("Click on end point of river");
				trackMode = Moving;
			}
		}
	}
	else
	{
		if(event->buttons & Mouse::LeftButton)
		{
			if(gotMouseLocation)
			{
				Grid* g = grid->getGrid(Zoom_Min);

				RiverMakeStruct rm;

				rm.x1 = grid->cord3DtoGrid(startLocation.x);
				rm.y1 = grid->cord3DtoGrid(startLocation.z);
				rm.x2 = grid->cord3DtoGrid(mouseLocation.x);
				rm.y2 = grid->cord3DtoGrid(mouseLocation.z);

				MapGridCord range = grid->getGrid(data3D.view.zoomLevel)->pointCount / 16;

				g->adjustRiverCord(rm.x1, rm.y1, False, range);
				g->adjustRiverCord(rm.x2, rm.y2, True, range);

				rm.height = riverHeight;
				rm.plainWidth = riverWidth;
				rm.curvature = riverCurvature;

				storeGrid();
				g->makeRiver(&rm);

				grid->makeZoomedGrids(data3D);
				updateView();
				changed = True;
			}

			trackMode = Tracking;
			hint(0);
		}
		else if(event->buttons & Mouse::RightButton)
		{
			trackMode = Tracking;
			hint(0);
		}
	}
}


DialItem riverParameterItems[] = {
	DialItem(Dial_Button,		0,		Rect(0,0,256,16), "Set River Parameters"),
	DialItem(Dial_Button,		0,		Rect(0,16,128,16),	"Height (0..255):"),
	DialItem(Dial_NumberInput,	1,		Rect(128,16,48,16),	p_height, sizeof(p_height)),
	DialItem(Dial_Button,		0,		Rect(0,32,128,16),	"Plain Width (1..128)"),
	DialItem(Dial_NumberInput,	2,		Rect(128,32,48,16),	p_radius, sizeof(p_radius)),
	DialItem(Dial_Button,		0,		Rect(0,48,128,16),	"Curvature % (0..100)"),
	DialItem(Dial_NumberInput,	2,		Rect(128,48,48,16),	p_rough, sizeof(p_rough)),
	DialItem(Dial_Button, 		-1,  	Rect( 32,64, 64,16), "Cancel"),
	DialItem(Dial_Button, 		-2,  	Rect(160,64, 64,16), "OK"),
	DialItem(Dial_End)
};

class RiverParameterDial : public Dialogue {
public:
	RiverParameterDial();
	void doIcon(DialItem* item, Event* event, MenuData* d);
};

RiverParameterDial::RiverParameterDial()
{
	Dialogue::setup(riverParameterItems);
}

void RiverParameterDial::doIcon(DialItem* item, Event* event, MenuData* d)
{
	if(event->buttons)
	{
		switch(item->id)
		{
		case -1:
		case -2:
			d->setFinish(item->id);
			break;
		}
	}
}

void BattleEditControl::setRiverParameters()
{
	sprintf(p_height, "%d", (int) riverHeight);
	sprintf(p_radius, "%d", (int) riverWidth);
	sprintf(p_rough,  "%d", (int) riverCurvature);
	RiverParameterDial dial;
	if(dial.process() == -2)	// OK Pressed
	{
		int n = atoi(p_height);

		if((n >= 0) && (n <= 0xff))
			riverHeight = n;

		n = atoi(p_radius);
		if((n > 0) && (n <= 128))
			riverWidth = n;

		n = atoi(p_rough);
		if((n >= 0) && (n <= 100))
			riverCurvature = n;

		hint(0);
	}
}

void BattleEditControl::riverOptions()
{
	static const char* choices[] = {
		"Remove All Rivers",
		"Set Parameters",
		"Undo last change",
		0
	};

	Boolean gridChanged = False;
	Grid* g = grid->getGrid(Zoom_Min);

	switch(chooseFromList(0, "River Commands", choices, "Cancel"))
	{
	case 1:	// Remove all rivers
		storeGrid();
		g->removeRivers();
		gridChanged = True;
		break;
	case 2:	// Set parameters
		setRiverParameters();
		break;
	case 3:	// Undo
		undoGrid();
		break;
	}

	if(gridChanged)
	{
		grid->makeZoomedGrids(data3D);
		updateView();
		changed = True;
	}
}

void BattleEditControl::roadOverMap(const Event* event)
{
	if(trackMode == Tracking)
	{
		if(event->buttons & Mouse::LeftButton)
		{
			if(gotMouseLocation)
			{
				startLocation = mouseLocation;
				hint("Click on end point of road");
				trackMode = Moving;
			}
		}
	}
	else
	{
		if(event->buttons & Mouse::LeftButton)
		{
			if(gotMouseLocation)
			{
				Grid* g = grid->getGrid(Zoom_Min);

				MapGridCord x1 = grid->cord3DtoGrid(startLocation.x);
				MapGridCord y1 = grid->cord3DtoGrid(startLocation.z);
				MapGridCord x2 = grid->cord3DtoGrid(mouseLocation.x);
				MapGridCord y2 = grid->cord3DtoGrid(mouseLocation.z);

				MapGridCord range = grid->getGrid(data3D.view.zoomLevel)->pointCount / 8;

				g->adjustRoadCord(x1, y1, False, range);
				g->adjustRoadCord(x2, y2, True, range);

				storeGrid();
				g->makeRoad(x1, y1, x2, y2, riverCurvature);

				grid->makeZoomedGrids(data3D);
				updateView();
				changed = True;
			}

			trackMode = Tracking;
			hint(0);
		}
		else if(event->buttons & Mouse::RightButton)
		{
			trackMode = Tracking;
			hint(0);
		}
	}
}

DialItem roadParameterItems[] = {
	DialItem(Dial_Button,		0,		Rect(0,0,256,16), "Set Road Parameters"),
	DialItem(Dial_Button,		0,		Rect(0,16,128,16),	"Curvature % (0..100)"),
	DialItem(Dial_NumberInput,	2,		Rect(128,16,48,16),	p_rough, sizeof(p_rough)),
	DialItem(Dial_Button, 		-1,  	Rect( 32,32, 64,16), "Cancel"),
	DialItem(Dial_Button, 		-2,  	Rect(160,32, 64,16), "OK"),
	DialItem(Dial_End)
};

class RoadParameterDial : public Dialogue {
public:
	RoadParameterDial();
	void doIcon(DialItem* item, Event* event, MenuData* d);
};

RoadParameterDial::RoadParameterDial()
{
	Dialogue::setup(roadParameterItems);
}

void RoadParameterDial::doIcon(DialItem* item, Event* event, MenuData* d)
{
	if(event->buttons)
	{
		switch(item->id)
		{
		case -1:
		case -2:
			d->setFinish(item->id);
			break;
		}
	}
}

void BattleEditControl::setRoadParameters()
{
	sprintf(p_rough,  "%d", (int) riverCurvature);
	RoadParameterDial dial;
	if(dial.process() == -2)	// OK Pressed
	{
		int n = atoi(p_rough);
		if((n >= 0) && (n <= 100))
			riverCurvature = n;
		hint(0);
	}
}

void BattleEditControl::roadOptions()
{
	static const char* choices[] = {
		"Remove All Roads",
		"Set Parameters",
		"Undo last change",
		0
	};

	Boolean gridChanged = False;
	Grid* g = grid->getGrid(Zoom_Min);

	switch(chooseFromList(0, "Road Commands", choices, "Cancel"))
	{
	case 1:	// Remove all
		storeGrid();
		g->removeRoads();
		gridChanged = True;
		break;
	case 2:	// Set parameters
		setRoadParameters();
		break;
	case 3:	// Undo
		undoGrid();
		break;
	}

	if(gridChanged)
	{
		grid->makeZoomedGrids(data3D);
		updateView();
		changed = True;
	}
}

/*==============================================================
 * Terrain Editor
 */

void BattleEditControl::terrainOverMap(const Event* event)
{
	MapGridCord gx;
	MapGridCord gy;

	/*
	 * Display current terrain
	 */

	if(gotMouseLocation)
	{
		gx = grid->cord3DtoGrid(mouseLocation.x);
		gy = grid->cord3DtoGrid(mouseLocation.z);
	
		Grid* g = grid->getGrid(Zoom_Min);
		TerrainType t = *g->getTSquare(gx, gy);
		setupInfo();
		TextWindow& textWind = *textWin;
		textWind.setFont(Font_JAME08);
		textWind.wprintf("Over:\r%s\r", getTerrainStr(t));
	}
	else
		clearInfo();


	if(event->buttons & Mouse::RightButton)
	{
		if(gotMouseLocation)
		{
			Grid* g = grid->getGrid(Zoom_Min);
		 	terrainType = *g->getTSquare(gx, gy);
		 	hint(0);
		}
		else
		{
			int n = chooseFromList(0, "Select Terrain Type", terrainNames, "Cancel");
			if(n > 0)
			{
				terrainType = makeTerrainType(n-1);
				hint(0);
			}
		}

	}
	else if(event->buttons & Mouse::LeftButton)
	{
		if(gotMouseLocation)
		{
			startDragX = gx;
			startDragY = gy;
			doingDrag = True;
		}
	}

	if(doingDrag && (!machine.mouse->buttons & Mouse::LeftButton))
	{
		/*
		 * Drag from start to finish
		 */

		Grid* g = grid->getGrid(Zoom_Min);

		MapGridCord x = startDragX;
		MapGridCord y = startDragY;

		int dx = gx - startDragX;
		int dy = gy - startDragY;

		int adx = ::abs(dx);
		int ady = ::abs(dy);

		int sx = (dx < 0) ? -1 : +1;
		int sy = (dy < 0) ? -1 : +1;

		storeGrid();
		g->placeTerrain(terrainType, x, y,
							 	terrainSize, riverProtected, roadProtected);

		if(adx > ady)		// Step horizontally
		{
			int ry = adx / 2;

			while(x != gx)
			{
				x += sx;

				ry += ady;
				if(ry >= adx)
				{
					g->placeTerrain(terrainType, x, y,
							 	terrainSize, riverProtected, roadProtected);
					y += sy;
					ry -= adx;
				}

				g->placeTerrain(terrainType, x, y,
							 	terrainSize, riverProtected, roadProtected);
			}
		}
		else					// Step vertically
		{
			int rx = ady / 2;

			while(y != gy)
			{
				y += sy;

				rx += adx;
				if(rx >= ady)
				{
					g->placeTerrain(terrainType, x, y,
							 	terrainSize, riverProtected, roadProtected);
					x += sx;
					rx -= ady;
				}

				g->placeTerrain(terrainType, x, y,
							 	terrainSize, riverProtected, roadProtected);
			}
		}

		grid->makeZoomedGrids(data3D);
		updateView();
		changed = True;
		doingDrag = False;
	}
}

static char riverProtStr[] = "Protect Rivers";
static char riverUnprotStr[] = "Overwrite Rivers";
static char roadProtStr[] = "Protect roads";
static char roadUnprotStr[] = "Overwrite roads";

DialItem terrainParameterItems[] = {
	DialItem(Dial_Button,		0,		Rect(0,0,256,16), "Set Terrain Parameters"),
	DialItem(Dial_Button,		0,		Rect(0,16,128,16),	"Brush size (1..128)"),
	DialItem(Dial_NumberInput,	3,		Rect(128,16,48,16),	p_radius, sizeof(p_radius)),
	DialItem(Dial_Button,		1,		Rect(0,32,128,16),	0),
	DialItem(Dial_Button,		2,		Rect(128,32,128,16),	0),
	DialItem(Dial_Button, 		-1,  	Rect( 32,64, 64,16), "Cancel"),
	DialItem(Dial_Button, 		-2,  	Rect(160,64, 64,16), "OK"),
	DialItem(Dial_End)
};

class TerrainParameterDial : public Dialogue {
public:
	Boolean riverProt;
	Boolean roadProt;

	TerrainParameterDial();
	void doIcon(DialItem* item, Event* event, MenuData* d);
};

TerrainParameterDial::TerrainParameterDial()
{
	Dialogue::setup(terrainParameterItems);
}

void TerrainParameterDial::doIcon(DialItem* item, Event* event, MenuData* d)
{
	if(event->buttons)
	{
		switch(item->id)
		{
		case 1:
			riverProt = !riverProt;
			terrainParameterItems[3].text = riverProt ? riverProtStr : riverUnprotStr;
			d->setRedraw();
			break;
		case 2:
			roadProt = !roadProt;
			terrainParameterItems[4].text = roadProt ? roadProtStr : roadUnprotStr;
			d->setRedraw();
			break;
		case -1:
		case -2:
			d->setFinish(item->id);
			break;
		}
	}
}

void BattleEditControl::setTerrainParameters()
{
	sprintf(p_radius, "%d", (int) terrainSize);

	terrainParameterItems[3].text = riverProtected ? riverProtStr : riverUnprotStr;
	terrainParameterItems[4].text = roadProtected ? roadProtStr : roadUnprotStr;

	TerrainParameterDial dial;
	dial.riverProt = riverProtected;
	dial.roadProt = roadProtected;


	if(dial.process() == -2)	// OK Pressed
	{
		int n = atoi(p_radius);
		if((n > 0) && (n <= 128))
			terrainSize = n;
		riverProtected = dial.riverProt;
		roadProtected = dial.roadProt;
		hint(0);
	}
}

void BattleEditControl::terrainOptions()
{
	static char const* choices[] = {
		"Clear all",
		"Select Terrain Type",
		"Set Parameters",
		"Undo Last Change",
		0
	};

	Boolean gridChanged = False;
	Grid* g = grid->getGrid(Zoom_Min);

	switch(chooseFromList(0, "Terrain Commands", choices, "Cancel"))
	{
	case 1:		// Clear all to default
		storeGrid();
		g->clearTerrain(terrainType, riverProtected, roadProtected);
		gridChanged = True;
		break;
	case 2:		// Select terrain type
			{
				int n = chooseFromList(0, "Select Terrain Type", terrainNames, "Cancel");
				if(n > 0)
				{
					terrainType = makeTerrainType(n-1);
					hint(0);
				}
			}
			break;
		break;
	case 3:
		setTerrainParameters();
		break;
	case 4:
		undoGrid();
		break;
	}

	if(gridChanged)
	{
		grid->makeZoomedGrids(data3D);
		updateView();
		changed = True;
	}
}

/*===============================================================
 * Undo feature
 */

/*
 * Copy current height & terrain to backup
 */

void BattleEditControl::storeGrid()
{
	Grid* g = grid->getGrid(Zoom_Min);		// this is where we are copying from

	if(!backupGrid)
	{
		backupGrid = new Grid;

		backupGrid->gridCount	= g->gridCount;
		backupGrid->pointCount	= g->pointCount;
		backupGrid->gridSize	= g->gridSize;

		backupGrid->heights = new Height8[g->pointCount * g->pointCount];
		backupGrid->terrains = new TerrainType[g->gridCount * g->gridCount];
	}

	memcpy(backupGrid->heights,  g->heights,  g->pointCount * g->pointCount * sizeof(Height8));
	memcpy(backupGrid->terrains, g->terrains, g->gridCount  * g->gridCount  * sizeof(TerrainType));
}

/*
 * Restore backup grid (actually swaps it)
 */


void memswap(void* s1, void* s2, size_t n)
{
	union Ptr {
		ULONG* l;
		UWORD* w;
		UBYTE* b;
		void* v;
	};

	Ptr p1;
	Ptr p2;
	
	p1.v = s1;
	p2.v = s2;


	while(n >= sizeof(ULONG))
	{
		ULONG l = *p1.l;

		*p1.l++ = *p2.l;
		*p2.l++ = l;

		n -= sizeof(ULONG);
	}

	while(n--)
	{
		UBYTE l = *p1.b;

		*p1.b++ = *p2.b;
		*p2.b++ = l;
	}
}


void BattleEditControl::undoGrid()
{
	if(backupGrid)
	{
		Grid* g = grid->getGrid(Zoom_Min);		// this is where we are copying from

		memswap(g->heights,  backupGrid->heights , g->pointCount * g->pointCount * sizeof(Height8));
		memswap(g->terrains, backupGrid->terrains, g->gridCount  * g->gridCount  * sizeof(TerrainType));

		grid->makeZoomedGrids(data3D);
		updateView();
		changed = True;
	}
}
