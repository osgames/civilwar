/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Text Display
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.10  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.6  1994/04/20  22:21:40  Steven_Green
 * Updated to use TextWindow class
 *
 * Revision 1.2  1994/01/07  23:43:18  Steven_Green
 * centreText added
 *
 * Revision 1.1  1993/11/19  18:59:26  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include <stdarg.h>
#include <stdio.h>
#include <dos.h>
#include <string.h>

#include "text.h"
#include "colours.h"
#include "memptr.h"

#define FORCE_LEFT_ALIGN '\x01'

/*
 * Constructor
 */

TextWindow::TextWindow(Region* bm, FontID n)
{
	bitMap = bm;
	font = fontSet(n);
	foreground = Black;
	background = White;
	transparent = True;
	hCentre = False;
	vCentre = False;
	wrap = False;
	shadow = False;
	bold = False;
	where = Point(0,0);
}

void TextWindow::draw(unsigned char c)
{
	if(c < 0x20)
		return;

	if(shadow)
	{
		Point oldWhere = where;
		Colour oldColour = foreground;
		foreground = background;
		shadow = False;
		where -= Point(1,1);

		draw(c);

		shadow = True;
		foreground = oldColour;
		where = oldWhere;
	}

	if(bold)
	{
		Point oldWhere = where;
		bold = False;

		where -= Point(1,0);
		draw(c);
#if 0
		where = oldWhere;
		where -= Point(0,1);
		draw(c);
		where = oldWhere;
		where -= Point(1,1);
		draw(c);
#endif

		bold = True;
		where = oldWhere;
	}

	int ch;

	if(font->inSet(c))
		ch = c - font->first;
	else
		ch = font->howMany - 1;

	UBYTE w = font->widths[ch];
	FontOffset offset = font->offsets[ch];
	UWORD lineCount = font->height;

	/*
	 * Check for completely off left/right
	 */

	SDimension maxX = bitMap->getW();

	int startX = where.x;
	where.x += w + 1;

	if(where.x <= 0)
		return;
	if(startX >= maxX)
		return;

	/*
	 * Top Clip
	 */

	SDimension y = where.y;
	if(y < 0)
	{
		if(lineCount <= -y)			// Competely off top.. ignore
			return;

		offset -= y * w;
		lineCount += y;

		y = 0;
	}

	/*
	 * Bottom Clip
	 */

	if( (y + lineCount) > bitMap->getH())
	{
		if(y >= bitMap->getH())			// completely off bottom
			return;

		lineCount = SDimension(bitMap->getH() - y);
	}

	UBYTE* bitPtr = font->bitStream + (offset >> 3);
	UBYTE currentByte = *bitPtr++;
	UBYTE bitMask = UBYTE(0x80 >> (offset & 7));

	UBYTE* linePtr = bitMap->getAddress(Point(startX, y));
	while(lineCount--)
	{
		int pixelCount = w;
		UBYTE* pixelPtr = linePtr;
		SDimension x = startX;

		while(pixelCount--)
		{
			if( (x >= 0) && (x < maxX) )
			{
				if(currentByte & bitMask)
					*pixelPtr = foreground;
				else if(!transparent)
					*pixelPtr = background;
			}
			pixelPtr++;
			x++;

			bitMask >>= 1;
			if(bitMask == 0)
			{
				bitMask = 0x80;
				currentByte = *bitPtr++;
			}
		}

		linePtr += bitMap->getLineOffset();
	}
}

void TextWindow::draw(const char* s)
{
	SDimension leftIndent = where.x;
	Boolean tempHCentre = hCentre;

	/*
	 * Count how many lines it is
	 */

	const char* s1 = s;
	int lineCount = 1;

	int x = 0;
	int lastX = 0;
	int firstX = 0;

	while(*s1)
	{
		char c = *s1;

		if(c == '\r')
		{
			lineCount++;
			x = 0;
			lastX = 0;
			firstX = 0;
		}
		else if(wrap)
		{
			if(c == ' ')
			{
				lastX = x;
				x += font->getWidth(c) + 1;
				firstX = x;
			}
			else
			{
				if(c >= 0x20)
				{
					x += font->getWidth(c) + 1;
					if(x > bitMap->getW())
					{
						lineCount++;
						x = x - firstX;
						lastX = 0;
						firstX = 0;
					}
				}
			}
		}
		else
			x += font->getWidth(c) + 1;

		s1++;
	}

	/*
	 * Centre vertically
	 */

	if(vCentre)
	{
		int height = lineCount * (1 + font->getHeight()) - 1;
		if(height > bitMap->getH())
			height = bitMap->getH();
		where.y = SDimension((bitMap->getH() - height) / 2);
	}

	s1 = s;
	const char* lastSpace = s;
	const char* lineStart = s;
	x = 0;
	lastX = 0;
	firstX = 0;
	Boolean lastWasReturn = False;

	while(*s1)
	{
		char c = *s1;

		if(c == '\r')
		{
			lastWasReturn = True;

			/*
			 * Print line
			 */

			if(tempHCentre)
				where.x = (bitMap->getW() - x) / 2;
			else
				where.x = leftIndent;

			while(lineStart != s1)
				draw(*lineStart++);

			x = 0;
			lastX = 0;
			firstX = 0;
			lastSpace = s1 + 1;
			lineStart = lastSpace;
			tempHCentre = hCentre;

			where.y += font->getHeight() + 1;
			if(where.y >= (bitMap->getH() - font->getHeight()))
				break;

		}
		else if(c == FORCE_LEFT_ALIGN)
			tempHCentre = False;
		else if(c >= 0x20)
		{
			lastWasReturn = False;
		
			if(wrap)
			{
				if(c == ' ')
				{
					lastSpace = s1;
					lastX = x;
					x += font->getWidth(c) + 1;
					firstX = x;
				}
				else
				{
					int x1 = x + font->getWidth(c) + 1;
					if(x1 > bitMap->getW())
					{
						if(tempHCentre)
							where.x = (bitMap->getW() - lastX) / 2;
						else
							where.x = leftIndent;

						while(lineStart != lastSpace)
							draw(*lineStart++);

						x = x1 - firstX;
						// x = x - firstX;
						// x1 = x + font->getWidth(c) + 1;
						lastX = 0;
						firstX = 0;

						while(*lastSpace == ' ')
							lastSpace++;
						lineStart = lastSpace;
			
						where.y += font->getHeight() + 1;
						if(where.y >= (bitMap->getH() - font->getHeight()))
							break;

					}
					else
						x = x1;
				}
			}
			else
				x += font->getWidth(c) + 1;
		}

		s1++;
	}

	if(x)
	{
		if(tempHCentre)
			where.x = (bitMap->getW() - x) / 2;
		else
			where.x = leftIndent;

		while(*lineStart)
			draw(*lineStart++);
	}

	if(lastWasReturn)
		where.x = leftIndent;
}


void TextWindow::wprintf(const char* fmt, ...)
{
	MemPtr<char> buffer(2000);

	va_list vaList;
	va_start(vaList, fmt);
	vsprintf(buffer, fmt, vaList);
	va_end(vaList);

	draw(buffer);
}

