/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Game tester
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1994/07/19  19:53:03  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/06/29  21:38:52  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <ctype.h>
#include <time.h>

#include "game.h"
#include "system.h"
#include "screen.h"
#include "connect.h"
#include "colours.h"
#include "text.h"
#include "dialogue.h"
#include "campscn.h"
#include "options.h"
#include "battle.h"

#include "generals.h"
#include "unit3d.h"

#include "campmusi.h"
#include "sound.h"
#include "city.h"

#include "layout.h"
#include "memptr.h"
#include "memalloc.h"

#ifdef DEBUG
#include "clto.h"
#include "memlog.h"
#endif

#define Font_SimpleMessage Font_EMFL10
#define MsgFillColour 0x1d
#define MsgBorderColour1 0x1f
#define MsgBorderColour2 0x50

#ifdef TESTING

static Boolean forceRandom = False;
static ULONG forceRandomSeed = 0x12345678;
#endif

/*
 * Global Variables
 *
 * Merely defining this, automatically sets up the screen mode and mouse
 * pointer
 */

GameVariables* game;

#ifdef DEBUG
Boolean disableAI = False;
#endif

class GameControl {
public:
	GameControl(GameType gt) { game = new GameVariables(gt); }
	~GameControl() { delete game; game = 0; }

	void play(Boolean skipMenu) { game->play(skipMenu); }

	void setplayersSide( Side s ) { game->playersSide = s; }
};


/*
 * Set up global variables
 */


GameVariables::GameVariables(GameType gt)
{
#if 0
	quit = False;

	gameType = gt;
	playersSide = SIDE_USA;
	playerCount = 1;
	connection = 0;
	sprites = new SpriteLibrary("art\\game");
#else
	quit = False;

	gameType = gt;
	playersSide = SIDE_USA;
	controller[0] = Player;
	controller[1] = AI;
	playerCount = 1;
	connection = 0;
	sprites = new SpriteLibrary("art\\game");

	/*
	 * Seed random Number
	 */

	ULONG seed;

#ifdef TESTING
	if(forceRandom)
		seed = forceRandomSeed;
	else
		seed = time(0);
#else
	seed = time(0);
#endif
	srand(seed);
	gameRand.seed(seed);
	miscRand.seed(seed);

	musicInGame			= True;
	musicInCampaign	= memInfo.freePhysical >= 0x180000;
	musicInBattle 		= memInfo.freePhysical >= 0x280000;
	musicInDatabase	= True;
	soundInGame			= True;

	inCampaign = False;
	inBattle = False;

	gameMusic = new CampaignMusic;
	globalProcedures.add(gameMusic);
#endif
}

GameVariables::~GameVariables()
{
	if(sprites)
		delete sprites;
}

/*
 * Quick bodge to display a simple text box
 */

void GameVariables::simpleMessage(const char* fmt, ...)
{
	Region bm(machine.screen->getImage(), Rect(MSGW_X, MSGW_Y, MSGW_W, MSGW_H));
	bm.fill(MsgFillColour);
	bm.frame(0,0,MSGW_W-1, MSGW_H-1, MsgBorderColour1, MsgBorderColour2);
	if(fmt)
	{
		MemPtr<char> buffer(1000);

		va_list vaList;
		va_start(vaList, fmt);
		vsprintf(buffer, fmt, vaList);
		va_end(vaList);

		TextWindow win(&bm, Font_SimpleMessage);
		win.setFormat(True, True);
		win.setColours(Black);
		win.draw(buffer);
	}
	machine.screen->setUpdate(bm);
}


/*
 * Handle global packets
 */

Packet* GameVariables::receiveData()
{
	Packet* pkt = connection->receiveData();

	if(pkt)
	{
		switch(pkt->type)
		{
		case Connect_Disconnect:
			connection->releaseData(pkt);
			delete connection;
			connection = 0;
			playerCount = 1;
			dialAlert(0, "Remote player has quit", "OK");
			return 0;

		default:
			return pkt;
		}
	}
	else
		return 0;
}

void GameVariables::disconnect()
{
	if(connection)
	{
		connection->sendData(Connect_Disconnect, 0, 0);
		if(connection)
			connection->flush();
		if(connection)
			delete connection;
		connection = 0;
		playerCount = 1;
	}
}

/*======================================================================
 * Undefined functions required to link
 */

void Unit::draw3D(System3D& drawData) { }
UnitBattle* Unit::setupBattle() { return 0; }
void Unit::clearupBattle() { }

UnitBattle* President::makeBattleInfo() { return 0; }
UnitBattle* Unit::makeBattleInfo() { return 0; }
UnitBattle* Regiment::makeBattleInfo() { return 0; }
UnitBattle* Brigade::makeBattleInfo() { return 0; }

void playCampaignBattle(OrderBattle* ob, MarkedBattle* batl)
{
	dialAlert(0, "Battles can not be\rplayed in the campaign tester!", "Excuse me\rfor being\rso stupid");
}
void SpriteID::remove() { }

#if 0
void City::writeData(DataFileOutStream& stream) { }
void Facility::writeData(DataFileOutStream& stream) { }
#endif

/*======================================================================
 * Entry point
 */

void GameVariables::play(Boolean skipMenu)
{
	// showFullScreen("art\\screens\\mn_menu.lbm");

	campaignGame();

	if(connection)
		connection->sendData(Connect_Disconnect, 0, 0);
}


/*
 * main function
 */


int main(int argc, char *argv[])
{
	try
	{
		/*
		 * Process command line
		 */

#ifdef DEBUG
		MemoryLog::Mode memMode = MemoryLog::None;
		int debugLevel = 0;
		Side playSide = SIDE_USA;
#endif

		int i = 0;
		while(++i < argc)
		{
			const char* arg = argv[i];

			if((arg[0] == '-') || (arg[0] == '/'))
			{
				switch(toupper(arg[1]))
				{
#ifdef DEBUG
				case 'M':										// -M for Memory Logging
					if(toupper(arg[2]) == 'S')				// -MS for memory summary
						memMode = MemoryLog::Summary;
					else
						memMode = MemoryLog::Full;
					break;

				case 'D':										// -D for debug log
					if(isdigit(arg[2]))
						debugLevel = atoi(&arg[2]);
					else
						debugLevel = 1;
					break;
#endif
#ifdef DEBUG

				case 'P':										// Used to select AI side 
					if ( toupper(arg[2]) == 'C')			// and AI CLTO
						playSide = SIDE_CSA;
					else if ( toupper(arg[2]) == 'U')
						playSide = SIDE_USA;
					else break;

					if ( isdigit( arg[3] ) )
					{
						UBYTE which = arg[3] - '0';

						if(playSide == SIDE_CSA)
						{
							if(which < CSA_CLTO_Count)
							{
								AI_Option = CLTOtype(CSA_Quick + which);
								got_AI_Option = True;
							}
						}
						else
						{
							if(which < USA_CLTO_Count)
							{
								AI_Option = CLTOtype(USA_Quick + which);
								got_AI_Option = True;
							}
						}
					}

					break;
#endif
#ifdef TESTING
				case 'R':		// Force Random Number
					forceRandom = True;
					if(isdigit(arg[2]))
						forceRandomSeed = atoi(&arg[2]);
					break;
				case 'S':		// Put in SeeAll mode
					seeAll = True;
					break;
#endif
				}
			}
		}

		machine.init();
		
#ifdef DEBUG
		memLog->setMode("memory.log", memMode);
		if(debugLevel)
			logFile = new LogStream("debug.log", debugLevel);
#endif

		GameControl game(CampaignGame);
#ifdef DEBUG
		game.setplayersSide(playSide);
#endif

		game.play(False);
	}
 	catch(GeneralError e)
 	{
		cout << "Untrapped Error: ";
		if(e.get())
			cout << e.get();
		else
			cout << "No description";
		cout << endl;
#ifdef DEBUG
		getch();
#endif

 	}
 	catch(...)
 	{
 		cout << "Caught some kind of error" << endl;
#ifdef DEBUG
		getch();
#endif
 	}

	return 0;
}

