/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Code that used to be testicon.h
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "dbicon.h"
#include "db_index.h"
#include "dbmem.h"
#include "system.h"
#include "screen.h"
#include "mouselib.h"

#if defined(COMPUSA_DEMO)
class GotoQuitGame {
};
#endif


/*=========================================================================
 * Constructors for ChrisIcon
 *
 * It just makes sure the initial colours are set to something
 *
 * It also calls the Icon constructor with its screen rectangle.
 */

ChrisIcon::ChrisIcon(IconSet* parent, Point where, char* which, char loop, Index *indptr, short int iflag ) :
		Icon(parent, Rect(where, Point( indptr->getIW( loop ), indptr->getIH( loop ) )) )
{
		
	backColour = 0;
	chicon = 0;					  
#ifdef SWG_REMOVED
	kpic = M_Arrow;
	highlite = M_Arrow;
#endif
	test = which;
	maxIcon = 0;
	ind = loop + 1;
	choice = IconNum++;
	index = indptr;
	iconflag = iflag;
	extendif = 0;
	lastkey = 0;
}


ChrisIcon::ChrisIcon(IconSet* parent, Point where, char loop, char *which, short int iflag, short int eif  ) :
		Icon(parent, Rect( where, Point( 0, 0 )))
{
   backColour = 0;
	chicon = 0;
	if ( loop == 0 ) IconNum = 0;
#ifdef SWG_REMOVED
	kpic = M_Arrow;
	highlite = M_Arrow;
#endif
	test = which;
	maxIcon = 0;
	ind = 0;
	choice = IconNum++;
	iconflag = iflag;
	extendif = eif;
	// lastkey = 0;
}


ChrisIcon::ChrisIcon(IconSet* parent, Point where, char* which, int positions[][5], char loop ) :
		Icon(parent, Rect(where, Point( positions[ loop][2], positions[ loop ][3] )))
{
		
	backColour = 0;
	chicon = 0;
	if ( loop == 0 ) IconNum = 0;
#ifdef SWG_REMOVED
	kpic = M_Arrow;
	highlite = M_Arrow;
#endif
	test = which;
	maxIcon = 0;
	ind = 0;
	choice = IconNum++;
	iconflag = positions[0][4];
	extendif = positions[1][4];
	lastkey = 0;
}

#ifdef SWG_REMOVED
ChrisIcon::ChrisIcon(IconSet* parent, Point where, GameSprites pic, GameSprites bac, char* which, int again, short int iflag, short int eif) :
	Icon(parent, Rect(where, Point(113,105)))
{
	backColour = 0;
	if ( again == 0 ) IconNum = 0; 
	chicon = 0;
	kpic = pic;
	highlite = bac;
	test = which;
	maxIcon = 0;
	choice = IconNum++;
	ind = 0;

	Region bm(machine.screen->getImage(), Rect( getPosition(), getSize()));

 	drawSprite( &bm, Point(0, 0 ), kpic );

	machine.screen->setUpdate(bm);

	iconflag = iflag;
	extendif = eif;
	lastkey = 0;
}
#endif

ChrisIcon::ChrisIcon(IconSet* parent, Point where, char* which, char loop, char numkeys, short int iflag, short int eif) :
	Icon(parent, Rect(where, Point( keyinfo[ loop ].width, keyinfo[ loop].height )))
{
	backColour = 0;
	chicon = 0;
#ifdef SWG_REMOVED
	kpic = M_Arrow;
	highlite = M_Arrow;
#endif
	test = which;
	maxIcon = numkeys;
	choice = IconNum++;
	ind  = 0;
	iconflag = iflag;
	extendif = eif;
	lastkey = 0;
}


/*
 * This gets called to draw the icon
 *
 * getPosition() tells it where it should be on the full screen
 * getSize() returns its size.
 *
 */


void ChrisIcon::drawIcon()
{
	/*
	 * Make up a Region on the backup screen matching the icon's
	 * location and size.
	 */

 //	Region bm(machine.screen->getImage(), Rect( getPosition(), getSize()));

 //	drawSprite( &bm, Point(0, 0 ), MM_Load );

 //	machine.screen->setUpdate(bm);

}


int ChrisIcon::IconNum = 0;
int ChrisIcon::lastkey = 0;


/*
 * Set up the TestIcon screen
 *
 */

TestIconControl::TestIconControl( char HowMany, char numkeys, int positions[][5] )
{
	
	which = 0;
	
	if ( HowMany > 20 ) HowMany = 2;
		
	icons = new IconList[ HowMany + 1 + numkeys ];		// Space for HowMany Icon(s) + NULL

	for ( char a = 0; a < HowMany; a++ )
		{
			if ( (positions[0][4] & ( 1 << a ) ) == 0 )
				{
				 icons[a] = new ChrisIcon( this, Point ( 0, 0 ), a*2, &which, positions[0][4], positions[1][4] );
				}
			else
				{
				 icons[a] = new ChrisIcon(this, Point( positions[a][0], positions[a][1] ),&which, positions, a );
				}
		}
  	if ( numkeys > 0 ) 
		{
			for ( char ab = 0; ab < numkeys; ab++ )
				{
				 	icons[HowMany + ab ] = new ChrisIcon(this, Point( keyinfo[ab].kx, keyinfo[ab].ky-2), &which, ab, numkeys, positions[0][4], positions[1][4] );
				}
		}

	iconNumber = HowMany + numkeys;
	keynum = numkeys;
	icons[ iconNumber ] = 0;

}



/*
 * 	Sets up the icons if the corresponding bit in positions[0][4] is set;
 */

TestIconControl::TestIconControl( char HowMany, char numkeys, int positions[][5], Index *ind )
{
	
	which = 0;
	
	if ( HowMany > 20 ) HowMany = 2;
		
	icons = new IconList[ HowMany + 1 + numkeys ];		// Space for HowMany Icon(s) + NULL

	for ( char a = 0; a < HowMany; a++ )
		{
			if ( (positions[0][4] & ( 1 << a ) ) == 0 )
				{
				 icons[a] = new ChrisIcon( this, Point ( 0, 0 ), a*2, &which, positions[0][4], 0 );
  				}
			else
				{
				 icons[a] = new ChrisIcon(this, Point( positions[a][0], positions[a][1] ),&which, positions, a );
				}
		}
  	if ( numkeys > 0 ) 
		{
			for ( char ab = 0; ab < numkeys; ab++ )
				{
				 	icons[ HowMany + ab ] = new ChrisIcon(this, Point( ind->getIX(ab), ind->getIY(ab)), &which, ab, ind, positions[0][4] );
				}																					
		}

	iconNumber = HowMany + numkeys;
	keynum = numkeys;
	icons[ iconNumber ] = 0;

}


/*
 * This is called is constantly called to mainly to check the mouse.
 * see icon.h for info about the Event structure.
 *
 * What we are doing here is:
 *   IF right button pressed, change the 'Hello' colour
 *   IF left button pressed, quit the icon tester
 *   IF the mouse is over the icon then change the cross colour
 */

void ChrisIcon::execute(Event* event, MenuData* d)
{
  // mouse only changes if it is over a keyword

  Point tempmouse;

  if ( ( lastkey < 6 ) && ( IconNum > 6 ) ) lastkey = 6;

  if ( (event->overIcon) && ( maxIcon > 0 ) )
  	d->setTempPointer(M_WhereTo);
  
  	if ( ( ind > 0 ) && ( index->getRemove( ind ) == 1 ) )
	{
		 index->light( event->where, ind, 1 );
 	}

   if (event->buttons & Mouse::LeftButton) 
	{
		if ( ( ind == 0 ) || ( ( ind != 0 ) && ( index->getHighlight( ind ) != 255 ) ) )
		{																					 
			d->setFinish(MenuData::MenuOK);
			*test = choice;	 
			mousepos = event->where;
		}
	}		 
	else if (event->overIcon)
	{ 
	  if ( ind != 0 )
	  	index->light( event->where, ind );

	}

	if ( event->key )
		{
			// cout << "Yep";
			switch( event->key )
				{
					case KEY_PgUp:
						if ( ( iconflag & 32 ) != 0 ) 
							{
								d->setFinish(MenuData::MenuOK);
								*test = 5;	 
								mousepos = event->where;
							}
						lastkey = event->key;
						break;

					case KEY_PgDown:
						if ( ( iconflag & 16 ) != 0 ) 
							{
								d->setFinish(MenuData::MenuOK);
								*test = 4;	 
								mousepos = event->where;
							}
						lastkey = event->key;
						break;
					case KEY_Escape:
#if defined(COMPUSA_DEMO)
						throw GotoQuitGame();
#else
						if ( ( iconflag & 1 ) != 0 )
							{
								d->setFinish(MenuData::MenuOK);
								*test = 0;	 
								mousepos = event->where;
							}
						lastkey = event->key;
						break;
#endif
					case KEY_Up:
						if ( ( ( iconflag & 32 ) != 0 ) && ( ( extendif & 32 ) == 0 ) )
							{
								d->setFinish(MenuData::MenuOK);
								*test = 5;	 
								mousepos = event->where;
							}
						else if ( (ind) && ( event->overIcon) )
							{
								index->MoveMousePtr( 1, ind, 0, event->where );
							}
						lastkey = event->key;

						break;

					case KEY_Down:
						if ( ( ( iconflag & 16 ) != 0 ) && ( ( extendif & 16 ) == 0 ) )
							{
								d->setFinish(MenuData::MenuOK);
								*test = 4;	 
								mousepos = event->where;
							}
						else if ( (ind) && ( event->overIcon ) )
							{
								index->MoveMousePtr( 2, ind, 0, event->where );
							}

						lastkey = event->key;
						break;

					case KEY_Right:
						if ( ( ( iconflag & 16 ) != 0 ) && ( ( extendif & 16 ) != 0 ) )
							{
								d->setFinish(MenuData::MenuOK);
								*test = 4;	 
								mousepos = event->where;
								lastkey = event->key;
							}
						else if ( ( choice == 7 ) && ( ind ) ) 
							{
								tempmouse = getPosition();

								Point actualM = machine.mouse->getPosition();
								
								machine.mouse->moveMouse( Point( tempmouse.x + 5, actualM.y ) ) ;

								if ( (index->MoveMousePtr( 3, ind, 1, event->where )).x == 0 ) machine.mouse->moveMouse( actualM ) ;
								
								event->where = Point( tempmouse.x + 5, actualM.y ); // , tempmouse.y + 5 );

								lastkey = choice;
							}
						break;

				  	case KEY_Left:
						if ( ( ( iconflag & 32 ) != 0 ) && ( ( extendif & 32 ) != 0 ) )
							{
								d->setFinish(MenuData::MenuOK);
								*test = 5;	 
								mousepos = event->where;
								lastkey = event->key;
							}
						else if ( ( choice == 6 ) && ( ind ) )
							{
								tempmouse = getPosition();

								Point actualM = machine.mouse->getPosition();

								machine.mouse->moveMouse( Point( tempmouse.x + 5, actualM.y ) ) ; // tempmouse.y + 5 ) ); 
								if ( ind ) index->MoveMousePtr( 3, ind, 1, event->where );

								event->where = Point( tempmouse.x + 5, actualM.y ); // , tempmouse.y + 5 );
								
								lastkey = choice;
							}
						break;

					case KEY_Home:
						if ( ( iconflag & 2) != 0 ) 
							{
								d->setFinish(MenuData::MenuOK);
								*test = 1;	 
								mousepos = event->where;
							}
						lastkey = event->key;
						break;

					case KEY_Space:
						if ( ( iconflag & 4 ) != 0 ) 
							{
								d->setFinish(MenuData::MenuOK);
								*test = 2;	 
								mousepos = event->where;
							}
						lastkey = event->key;
						break;

					case KEY_End:
						if ( ( iconflag & 8 ) != 0 ) 
							{
								d->setFinish(MenuData::MenuOK);
								*test = 3;	 
								mousepos = event->where;
							}
						lastkey = event->key;
						break;

					case KEY_F1:
						if ( choice == 6 )
							{
								tempmouse = getPosition();
								machine.mouse->moveMouse( Point( tempmouse.x + 5, tempmouse.y + 5 ) );

								if ( ind ) index->MoveMousePtr( 1, ind, 1, event->where );

								event->where = Point( tempmouse.x + 5, tempmouse.y + 5 );
								lastkey = choice;
							}
						break;
					case KEY_F2:
						if ( choice == 7 )
							{
								tempmouse = getPosition();
								machine.mouse->moveMouse( Point( tempmouse.x + 5, tempmouse.y + 5 ) );
								if ( ind ) index->MoveMousePtr( 1, ind, 1, event->where );

								event->where = Point( tempmouse.x + 5, tempmouse.y + 5 );
								lastkey = choice;
							}
						break;
					case KEY_F3:
						if ( choice == 8 )
							{
								tempmouse = getPosition();
								machine.mouse->moveMouse( Point( tempmouse.x + 5, tempmouse.y + 5 ) );
								lastkey = choice;
							}
						break;
					case KEY_F4:
						if ( choice == 9 )
							{				
								tempmouse = getPosition();
								machine.mouse->moveMouse( Point( tempmouse.x + 5, tempmouse.y + 5 ) );
								lastkey = choice;
							}
						break;
					case KEY_F5:
						if ( choice == 10 )
							{
								tempmouse = getPosition();
								machine.mouse->moveMouse( Point( tempmouse.x + 5, tempmouse.y + 5 ) );
								lastkey = choice;
							}
						break;

					case KEY_F6:
						if ( choice == 11 )
							{
								tempmouse = getPosition();
								machine.mouse->moveMouse( Point( tempmouse.x + 5, tempmouse.y + 5 ) );
								lastkey = choice;
							}
						break;

/*					case KEY_Commer:

						if ( lastkey > 6 )
							{
								if ( choice == lastkey - 1 )
									{
										tempmouse = getPosition();
										machine.mouse->moveMouse( Point( tempmouse.x + 5, tempmouse.y + 5 ) );

										lastkey = choice;
									}
							}

						else
							{
								if ( choice == IconNum - 1 )
									{
										tempmouse = getPosition();
										machine.mouse->moveMouse( Point( tempmouse.x + 5, tempmouse.y + 5 ) );

										lastkey = choice;
									}
							}
						break;

					case KEY_FullStop:

						if ( lastkey < IconNum - 2 )
							{
								if ( choice == lastkey + 1 )
									{
										tempmouse = getPosition();
										machine.mouse->moveMouse( Point( tempmouse.x + 5, tempmouse.y + 5 ) );

										lastkey = choice;
									}
							}
						else
							{
								if ( choice == 6 )
									{
										tempmouse = getPosition();
										machine.mouse->moveMouse( Point( tempmouse.x + 5, tempmouse.y + 5 ) );

										lastkey = 6;
									}
							}
						break;
				*/	

					case KEY_Enter:
						//tempmouse = getPosition();
						/// cout << "tm=" << tempmouse.x << "," << tempmouse.y << "   e->w=" << event->where.x << ", " << event->where.y << " ";
						
						if ( event->overIcon ) 	//  ( !d->isFinished() ) && ( tempmouse.x == 0 ) && ( machine.mouse->getPosition() == event->where ) )
							{
								lastkey = event->key;
								
								//cout << "  choice=" << choice;
								//getch();

								d->setFinish(MenuData::MenuOK);
								
								// if ( choice < IconNum )
								*test = choice;
								//	else cout << "Error!!!";

								mousepos = event->where;

								//cout << "yep";
							}

						break;

					default:
						// cout << " " << (int)event->key;
						lastkey = event->key;
						break;
				}

			
		}
	// else if ( event->key != lastkey ) if ( lastkey != KEY_Enter ) lastkey = 0;
}


