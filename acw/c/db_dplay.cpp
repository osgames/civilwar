/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Database dPlay Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG
#include <conio.h>
#endif
#include "db_dplay.h"
#include "datab.h"
#include "system.h"
#include "screen.h"
#include "dialogue.h"
#include "dbmem.h"
#include "game.h"
#include "campmusi.h"
#include "db_index.h"
#include "flicread.h"
#include "timer.h"
#include "mouselib.h"
#include "text.h"
#include "colours.h"
#include "dbicon.h"
#include "memptr.h"
#include "language.h"

#define FONT_HEADLINE Font_EMMA14

/*
 * List of songs... must match with the text in dbdata\\songs.*
 *
 * Note that the gameMusic automatically adds path and extension
 */

static const char* dbSongNames[] = {
	"bonblufl",
	"tramp",
	"richmond",
	"batlcry",
	"batlmthr",
	"batlhymn",
	"lincoln",
	"johnny",
	"dixie",
	"cleartrk",
	"tenting",
	"maryland",
	"rollalab",
	"cumbrlnd",
	"ylorose",
};

static const int DBSongCount = 15;	// Must be size of above table

#if !defined(COMPUSA_DEMO)
static const char* flicNames[] = {
	"anim\\rifle1.fli",
	"anim\\6lb_gun.fli",
	"anim\\12lb_nap.fli",
	"anim\\Sharps.fli",
	"anim\\canon2.fli",
	"anim\\boat2.fli",
	"anim\\boat4.fli"
};

static const int DBFlicCount = 7;

static const char* mapNames[] = {
	"dbdata\\map_01",
	"dbdata\\map_02",
	"dbdata\\map_03",
	"dbdata\\map_04",
	"dbdata\\map_05",
	"dbdata\\map_00"
};
#endif

static const int DBMapCount = 6;

#define BOOK_X1 44		// Was 68
#define BOOK_W1 246		// Was 237
#define BOOK_X2 324		// Was 337 (or 340)
#define BOOK_W2 261		// Was 237 (or 243)

#define BOOK_Y1 30
#define BOOK_H1 390
#define BOOK_Y2 21
#define BOOK_H2 375

#if defined(COMPUSA_DEMO)
static void demoFeature()
{
	dialAlert(0, "Parts of the encyclopedia\rare not included\rin this demo version", "OK");
}
#endif

/*=======================================================
 * Dplay Implementation
 */

void Dplay::terminate()
{
	delete text_xpos;
	delete text_ypos;
	delete totalWidth;
	delete totalHeight;
#ifdef SWG_REMOVED
	delete di;
#endif
	delete Newspage;
	if ( ( sprlibflags & 1 ) != 0 ) delete biolib;
	delete opiconlib;
	if ( picbit == ON ) delete picture;
	// ~SetUpWins()
}

/*
 *    SetUpWindows defines regions for indices, text and headings, and also
 *   allocates memory for any pictures.
 */

void Dplay::SetUpWindows()  // ( char page, short int totalWidth [], short int totalHeight[], short int text_xpos[],	short int text_ypos [] )
{

	if ( picbit == ON ) { delete picture; picbit = OFF; }

	switch ( page )
		{
		 case Page_Main:										 // Main menu
		 	break;
	 	
		 case Page_Bio1:
		  {
		   totalWidth [0] = 188;			 // index( column ) 1
			totalHeight[0] = 321;
			text_xpos  [0] = 100;
			text_ypos  [0] = 77;

			totalWidth [1] = 245;			 // index( column ) 2
			totalHeight[1] = 320;
			text_xpos  [1] = 340;
			text_ypos  [1] = 77;
		  }
		   break;

		 case Page_Bio2:									 // Biographies
		  {
		   totalWidth [0] = 565;			 // Text area
			totalHeight[0] = 120;
			text_xpos  [0] = 40;
			text_ypos  [0] = 275;

			totalWidth [1] = 210;			 // Header
			totalHeight[1] = 167;
			text_xpos  [1] = 1;
			text_ypos  [1] = 45;
						  
			totalWidth [2] = 565;			 // Job
			totalHeight[2] = 20;
			text_xpos  [2] = 40;
			text_ypos  [2] = 255;

			totalWidth [3] = 158;			 // Picture size
 			totalHeight[3] = 220;
			text_xpos  [3] = 226;
			text_ypos  [3] = 16;

			
			if ( ( sprlibflags & 1 ) == 0 )
				{
					sprlibflags += 1;
					biolib = new SpriteLibrary( "dbdata\\biosprs" );
					// cout << "Setting biosprs!!";
					// getch();
				}
			
			picture = new Region( machine.screen->getImage(), Rect( 226, 16, 158, 220 ) );  // 384, 236 ) );
			picbit = ON;
		  }
		  break;
		 // case 3:
		 	
		 case Page_Battles:									 // Battles
		  {
			totalWidth [0] = 565;			 // Text area
			totalHeight[0] = 150;
			text_xpos  [0] = 40;
			text_ypos  [0] = 220;

			totalWidth [1] = 565;			 // Header
			totalHeight[1] = 120;
			text_xpos  [1] = 40;
			text_ypos  [1] = 45;
						  
			totalWidth [2] = 565;			 // Job
			totalHeight[2] = 20;
			text_xpos  [2] = 40;
			text_ypos  [2] = 200;
		  }	
		  break; 

		 case Page_Chrono:
		  {
		   totalWidth [0] = 565;			 // Text area
	 		totalHeight[0] = 250;
			text_xpos  [0] = 40;
			text_ypos  [0] = 120;

			totalWidth [2] = 565;			 // Chronology title
			totalHeight[2] = 30;
			text_xpos  [2] = 40;
			text_ypos  [2] = 25;
						  
			totalWidth [1] = 565;			 // Year
			totalHeight[1] = 40;
			text_xpos  [1] = 40;
			text_ypos  [1] = 60;
		  }	
		  break; 

		 case Page_Testtabl:
		 case Page_Artillery:
		 case Page_CavalryTactics:
		 case Page_InfantryWeapons:
		 case Page_NavalTactics:
		 case Page_InfantryTactics:
		 case Page_ArtilleryTactics:
		 case Page_CavalryWeapons:
		 case Page_NavalWeapons:
		  {
		   totalWidth [0] = 565;			 // Text area
			totalHeight[0] = 320;
			text_xpos  [0] = 40;
			text_ypos  [0] = 75;
		  }	
		  break;
		
		case Page_Features2:
		case Page_Songs:									 // Songs
		case Page_Battles2:									 // Battle index;
		  {
		   totalWidth [0] = BOOK_W2;			 // index( column ) 1
			totalHeight[0] = BOOK_H2; // 1
			text_xpos  [0] = BOOK_X2; // 68
			text_ypos  [0] = BOOK_Y2;

/*			totalWidth [1] = 237;			 // index( column ) 2
			totalHeight[1] = 375;
			text_xpos  [1] = 100;
			text_ypos  [1] = 22;
*/			
		  }
		  break;


		 case Page_History:
		 case Page_History2:
		  {
		   totalWidth [0] = 188;			 // Page( column ) 1
			totalHeight[0] = 384;
			text_xpos  [0] = 100;  // 68
			text_ypos  [0] = 13;

			totalWidth [1] = 188;			 // Page( column ) 2
			totalHeight[1] = 382;
			text_xpos  [1] = 350;  // 392
			text_ypos  [1] = 14;
		  }
		  break;

		 case Page_Features:									 // Features;
		  {
#if 0
		   totalWidth [0] = 208;			 // column 1
			totalHeight[0] = 240;
			text_xpos  [0] = 2;
			text_ypos  [0] = 161;

			totalWidth [1] = 208;			 // column 2
			totalHeight[1] = 160;
			text_xpos  [1] = 216;
			text_ypos  [1] = 238;

			totalWidth [2] = 208;			 // column 3	
			totalHeight[2] = 240;
			text_xpos  [2] = 429;
			text_ypos  [2] = 161;

			totalWidth [3] = 208;			 // Header
 			totalHeight[3] = 69;
			text_xpos  [3] = 216;
			text_ypos  [3] = 158;

			totalWidth [4] = 637;			 // job ( date )
			totalHeight[4] = 16;
			text_xpos  [4] = 1;
			text_ypos  [4] = 136;
#else
			text_xpos  [0] = 6;
			text_ypos  [0] = 124;
		   totalWidth [0] = 200;			 // column 1
			totalHeight[0] = 276;

			text_xpos  [1] = 220;
			text_ypos  [1] = 200;
			totalWidth [1] = 200;			 // column 2
			totalHeight[1] = 200;

			text_xpos  [2] = 434;
			text_ypos  [2] = 124;
			totalWidth [2] = 200;			 // column 3	
			totalHeight[2] = 276;

			text_xpos  [3] = 220;
			text_ypos  [3] = 124;
			totalWidth [3] = 200;			 // Header
 			totalHeight[3] = 70;

			text_xpos  [4] = 4;
			text_ypos  [4] = 98;
			totalWidth [4] = 632;			 // job ( date )
			totalHeight[4] = 16;
#endif
		  }
		  break;

		 case Page_BriefHistory:
		  {
		   totalWidth [0] = 188;			 // Page( column ) 1
			totalHeight[0] = 384;
			text_xpos  [0] = 100;
			text_ypos  [0] = 13;

			totalWidth [1] = 188;			 // Page( column ) 2
			totalHeight[1] = 382;
			text_xpos  [1] = 350;
			text_ypos  [1] = 14;
		  }
		  break;

		case Page_Pictures:
		  {
		   totalWidth [0] = BOOK_W1;			 // index( column ) 1
			totalHeight[0] = BOOK_H2; // 375
			text_xpos  [0] = BOOK_X1; // 330
			text_ypos  [0] = BOOK_Y2;	// 21

			totalWidth [1] = BOOK_W2;			 // Page( column ) 2
			totalHeight[1] = BOOK_H2;
			text_xpos  [1] = BOOK_X2;
			text_ypos  [1] = BOOK_Y2;
		  }
		  break;

		case Page_HistorySelect:
			break;
		
		case Page_Songs2:									 // Songs
		  {
		   totalWidth [0] = 565;			 // Text area
	 		totalHeight[0] = 260;
			text_xpos  [0] = 40;
			text_ypos  [0] = 120;

			totalWidth [1] = 565;			 // Song title
			totalHeight[1] = 60;
			text_xpos  [1] = 40;
			text_ypos  [1] = 30;
		  }	
		  break; 
			
		 default:
		  {
		   // cout << "Error in SetWindows  ";
			dialAlert(0, "This page is currently unavailable", "Ooops!");
			for ( char a = 0; a < 3; a++ )
				{
					totalWidth[0] = 1;
					totalHeight[0] = 1;
				}
		  }
		  break;
		}

//	machine.mouse->move( Point( text_xpos[0] + 5, text_ypos[0] + 5 ) );

	return;
}



Dplay::Dplay()	: SetUpWins()
{
	const char ms = 5;
	page = Page_Main;
	memory.basepic  = 0;
	memory.baseanim = 0;
	
	picbit = OFF;

	sprlibflags = 0;
	sprIndex = 0;
	
   n = Font_JAME08;

	// BasePtrOffset = 0;
	skipPtr = 1;
	quit = NO;
	ind = 0;

	keytotal = 0;

	Newspage = new long int[ 61 ];

	NewsNum = 0;

	totalWidth = new short int [ ms ];
	totalHeight = new short int [ ms ];
	text_xpos = new short int [ ms ];
	text_ypos = new short int [ ms ];

	SetUpWindows();
	UpWins( n ); // page, totalWidth, totalHeight, text_xpos, text_ypos, n );
	
	if ( getError() != 0 ) 
		{
			wipeOut();
#ifdef DEBUG
			dialAlert ( 0, "Text file currently unavailable", "ok" );
#endif
			wipeIn();
			quit = YES;

			return;
		}

	HowMany = SetUpDi	( getjumpcount() );
			
	changewins(); // page, totalWidth, totalHeight, text_xpos, text_ypos );

	if ( getError() != 0 ) 
		{
			quit = YES;

			return;
		}

	opiconlib = new SpriteLibrary( "dbdata\\icons_1.spr" );

	playState = 0;
}


/*
 * Here is the actual test routine
 */

int testIcon( char keytotal, char HowMany, int positions[][5] )
{
   TestIconControl menuData ( HowMany, keytotal, positions );

	if ( ( ( positions[ICON_Quit][4] & 16 ) == 0 ) && ( HowMany == 6 ) )
	{
		//cout << "Yep16";
		Region tempreg(machine.screen->getImage(), Rect ( positions[ICON_Next][0], positions[ICON_Next][1], positions[ICON_Next][2], positions[ICON_Next][3] )); 
		tempreg.greyOut( Rect( 0, 0, positions[ICON_Next][2], positions[ICON_Next][3] ), 16 );
		machine.screen->setUpdate(tempreg);
		positions[ICON_Memory][4] |= 16;
	}
	else if ( ( ( positions[ICON_Memory][4] & 16 ) != 0 ) && ( HowMany == 6 ) )
	{
		SpriteLibrary newiconlib("dbdata\\icons_1.spr");
		Region tempreg( machine.screen->getImage(), Rect ( positions[ICON_Next][0], positions[ICON_Next][1], positions[ICON_Next][2], positions[ICON_Next][3] ) ); 
		newiconlib.drawCentredSprite( &tempreg, 1 );
		machine.screen->setUpdate( tempreg );
		positions[ICON_Memory][4] &= ~16;
	}

	if ( ( ( positions[ICON_Quit][4] & 32 ) == 0 ) && ( HowMany == 6 ) )
	{
		//cout << "Yep32";
		Region tempreg( machine.screen->getImage(), Rect ( positions[ICON_Prev][0], positions[ICON_Prev][1], positions[ICON_Prev][2], positions[ICON_Prev][3] ) ); 
		tempreg.greyOut( Rect( 0, 0, positions[ICON_Prev][2], positions[ICON_Prev][3] ), 16 );
		machine.screen->setUpdate( tempreg );
		positions[ICON_Memory][4] |= 32;
	}	
	else if ( ( ( positions[ICON_Memory][4] & 32 ) != 0 ) && ( HowMany == 6 ) )
	{
		SpriteLibrary opiconlib("dbdata\\icons_1.spr");
		Region tempreg( machine.screen->getImage(), Rect ( positions[ICON_Prev][0], positions[ICON_Prev][1], positions[ICON_Prev][2], positions[ICON_Prev][3] ) ); 
		opiconlib.drawCentredSprite( &tempreg, 0 );
		positions[ICON_Memory][4] &= ~32;
		machine.screen->setUpdate( tempreg );
	}

	while( !menuData.isFinished() )
	{
		menuData.process();
	}

	return menuData.which;				// Return which icon was pressed

}

int testIconI( char keytotal, char HowMany, int positions[][5], Index *ind )
{

   TestIconControl menuData ( HowMany, keytotal, positions, ind );
		
	while( !menuData.isFinished() )
	{
		menuData.process();
	}

	return menuData.which;				// Return which icon was pressed

}


void Dplay::mainloop()
{
	if ( error != 0 ) quit = YES;

	while ( quit == NO )
		{
			// machine.screen->update();
			upDatewin();
				
#ifdef SWG_REMOVED
			if ( ind == 0 ) tret = testIcon( keytotal, di, HowMany, positions );
				else 
					{
						tret = testIconI( getcols(), di, HowMany, positions, index );
					}
#else
			if ( ind == 0 ) tret = testIcon( keytotal, HowMany, positions );
				else 
					{
						tret = testIconI( getcols(), HowMany, positions, index );
					}
#endif

#if 0
#ifdef DEBUG
 outp(0x3c6, 0xff);

 outp(0x3c8, 7);

 outp(0x3c9, 0xff >> 2);
 outp(0x3c9, 0xff >> 2);
 outp(0x3c9, 0xff >> 2);
#endif
#endif

			switch( tret )
				{
					case ICON_Quit: 
						quit = YES;
						if ( playState ) 
						{
							game->gameMusic->endForce();
							playState = 0;
						}

						break;

					case ICON_Main:
						page = Page_Main;
						SetNonText();
						if ( playState ) 
						{
							game->gameMusic->endForce();
							playState = 0;
						}
						break;

					case ICON_Memory:
						UseMemory();
						if ( playState ) 
						{
							game->gameMusic->endForce();
							playState = 0;
						}
						break;

					case ICON_Index:
						getRetIndex();	 
						break;

					case ICON_Next:
						NextPara();
						break;

					case ICON_Prev:
						PrevPara();
						break;

					default:
						if ( playState ) 
						{
#if 0
							sound.stopMusic( song );
							sound.unloadSong( song );
#else
							game->gameMusic->endForce();
#endif
							playState = 0;
						}
						if ( !error )
						{
							if ( (signed)( tret - HowMany - ind ) >= 0 ) getKeywordDisplay();
							else getIcon();
						}
						break;
				}

			if ( error != 0 ) quit = YES;

		}
	changewins(); // 0, totalWidth, totalHeight, text_xpos, text_ypos );
	if ( ind != 0 ) { index->distroy(); delete index; }
	dispointers();
	terminate();
}

void Dplay::getPic()
{			
	/*
		*  Include error checking here;
		*/

	if ( sprIndex > 0 ) resetarea( picback );

	if ( ( getPicFlag() > 0 ) && ( getFormat() == '2' ) )
		{ 
			
			sprIndex = getPicFlag();
						
			underpic.resize( totalWidth[3], totalHeight[3] );
			machine.unBlit(&underpic, where[ 3 ] );

			if ( ( sprIndex < 1 ) || ( sprIndex > 53 ) ) 
			{ 
#ifdef DEBUG
				cout << "Error in getPic: sprIndex=" << sprIndex << "!! ";
				getch();
#endif
			}
			else biolib->drawCentredSprite( retReg( 5 ), (sprIndex - 1) );

		}
	else 
		{
			if ( getFormat() == 'D' && (getTextFP() < DBSongCount) ) playsong( getTextFP() );
			sprIndex = 0;		 
		}
	NewsNum = 0;
}

#if !defined(COMPUSA_DEMO)

void Dplay::disFlic( int flicNum )
{


	if(flicNum < DBFlicCount)
	{
		const char* filename = flicNames[flicNum];

		FlicRead flicfile( filename );
	
		if(flicfile.getError())
		{

		#ifdef DEBUG
			cout << "Error in FLIC file " << filename << endl;
			getch();
		#endif
			// delete filename;			 
			return;
		}

		const FlicHead* head = flicfile.getHeader();

		unsigned int waitTime = (head->speed * timer->ticksPerSecond) / 1000;

		Point posit( 0, 0 );

		posit.x = ( 640 - head->width  ) / 2;
		posit.y = ( 440 - head->height ) / 2;

		// Region fbm(machine.screen->getImage(), Rect( posit.x, posit.y, head->width, head->height ));

		machine.mouse->clearEvents();
		machine.mouse->getEvent();
	
		machine.mouse->setPointer( M_Arrow );

		while ( !machine.mouse->getEvent() )   	// !Mouse::LeftButton )
			{
				flicfile.nextFrame();

				machine.blit(&flicfile.image, posit );

				// machine.screen->setUpdate( fbm );
				machine.screen->update();

				timer->waitRel(waitTime);
			}

		// delete filename;
	}
}

#endif	// DEMO

void Dplay::wipeOut( Boolean returnFlag )
{
	char loop;
	Region *tempreg;

	for ( loop = 0; loop < 6; loop++ )
		{
			if ( ( ( positions[ICON_Quit][4] & ( 1 << loop ) ) != 0 ) && !(returnFlag && loop == 2 ) )
				{
					
					tempreg = new Region ( machine.screen->getImage(), Rect ( positions[loop][0], positions[loop][1], positions[loop][2], positions[loop][3] ) ); 
					tempreg->greyOut( Rect( 0, 0, positions[loop][2], positions[loop][3] ), 16 );
					machine.screen->setUpdate( *tempreg );
					delete tempreg;	
				}
		}

	machine.screen->update();
}

void Dplay::wipeIn()
{
	char loop;
	Region *tempreg;

	// waitOnMouse();
 
	for ( loop = 0; loop < 6; loop++ )
		{
			if ( ( positions[ICON_Quit][4] & ( 1 << loop ) ) != 0 ) 
				{
					
					tempreg = new Region ( machine.screen->getImage(), Rect ( positions[loop][0], positions[loop][1], positions[loop][2], positions[loop][3] ) ); 
					if ( loop != 0 ) opiconlib->drawCentredSprite( tempreg, 5 - loop );
						else opiconlib->drawCentredSprite( tempreg, 7 );
					machine.screen->setUpdate( *tempreg );
					delete tempreg;	
				}
		}
}

void Dplay::playsong ( int songnumber )
{
	Boolean loaded = False;

	if( game->musicInDatabase && (songnumber < DBSongCount) && (songnumber >= 0) )
	{
		const char* title = dbSongNames[songnumber];

		if(title)
		{
			/*
		 	 * playState must be set, so that  gameMusic->endForce is called
		 	 * later on, regardless of whether or not the campaig song was
		 	 * found.
		 	 */

			playState = 1;
			loaded = game->gameMusic->forceMusic(title);
		}
	}
#ifdef DEBUG
	if(!loaded)
	{
		wipeOut();
		dialAlert(0, "Song not currently available", "o.k.");
		wipeIn();
	}
#endif
}


void Dplay::SetNonText()
{
	
	HowMany = SetUpDi	( getjumpcount() );
			
	changewins(); // page, totalWidth, totalHeight, text_xpos, text_ypos );

	keytotal = 0;

	if ( ind > 0 ) { index->distroy(); delete index; ind = 0; }
	
}


/*
 *    Displays a table of data.
 *		It expects the data to be in the form:-
 *
 *		~x
 *		column 1 title
 *		column 2 title, etc
 *		~
 *		~cn text~  ( where n is the number of the column for the text to be 
 *		displayed ).	If n is less than or equal to the previous column printed
 *		a new line is started.
 *		~s text~ misses a line and then displays a subheading with text.
 *
 *		returns:  The number of characters read from file;
 */

int displaytable( Dplay *scr, char *textptr, Point *where, Region *bm, TextWindow *window, unsigned char *keyt, Font *font, short int totalWidth, short int totalHeight, char check )
{
	int space;
	char columns, dat, loop, storesp = 0;
	int length, lastc= 0;
	char word[125], depth = 0, maxdepth = 0, b;
	unsigned char keytotal = *keyt;
	enum status { YES, NO };
	status leave = NO;
	char *store = textptr;
	Point start;

	int leftshift;

	if ( *textptr == 'x' ) textptr++;

	columns = ( *textptr++) - '0';
	
	if ( *textptr == 13 ) textptr += 2;

	space = bm->getW() / ( columns + 1 ) - 10;
	space += space / ( 2 * columns );

	leftshift = space / 2;

	where->x = 0;
	where->y += 5;

	start.y = where->y;
	start.x = where->x;

	for ( loop = 0; loop < columns; loop++ )
		{
			length = 0;

			while ( ( *textptr != 13 ) && ( *textptr != '~' ) ) word [ length++ ] = *textptr++;

			word [ length ] = '\0';

			if ( ( *textptr == '~' ) && ( loop != ( columns - 1) ) ) 
				{ 
#ifdef DEBUG
					cout << "Error: found '~' unexpectedly : columns=" << columns << " loop=" << (int)loop << " word=" << word;
#endif
					break;
				}
			
			if ( *textptr == 13 ) textptr += 2;

			where->y = start.y;
			
			if ( ( font->getWidth( word ) >= space ) || ( ( where->x + font->getWidth( word ) ) > bm->getW() ) )
 				{
					char a = 0;
					
					storesp = 0;

					while ( a++ != length ) 
							{
								if ( word[a] == ' ' )
									{
										word[a] = '\0';
										if ( font->getWidth( word ) >= ( space - 10 ) )
										 	{
										 	  	word [ a ] = ' ';
										 	   word[ storesp ] = 13;
												dat = word[ storesp + 1];
												word [storesp + 1] = '\0';
												if ( check != 0 )
													{
		  												where->x += ( space - font->getWidth( word ) ) / 2;
														window->setPosition( *where );
														where->x -= ( space - font->getWidth( word ) ) / 2;
	
														keytotal = scr->writeline( window, bm, *where, word, keytotal, font, totalWidth, totalHeight );
													}
												where->y += font->getHeight();
												word [ storesp + 1] = dat;
												for ( b = storesp + 1; b < length; b++ )
													{
													   word [ b - storesp - 1 ] = word [ b ];	
													}
												
												word[b - storesp - 1] = '\0';
												length -= (storesp + 1);
												depth++;
												a = 0;

											}
										else word[a] = ' ';
		  						}	
								storesp = a + 1;
									
							}	 
					storesp = 0;
					 //( depth * font->getHeight() );
					depth = 0;
					
				}
		  	 else if ( space - font->getWidth( word ) > 40 ) leftshift = 0;
			 if ( depth > maxdepth ) maxdepth = depth;
			 depth = 0;
			 word [ length++ ] = 13;
			 word [ length ] = '\0';
			 if ( check != 0 ) 
			 	{
					where->x += ( space - font->getWidth( word ) ) / 2;

					window->setPosition( *where );
	
					where->x -= ( space - font->getWidth( word ) ) / 2;
	
					keytotal = scr->writeline( window, bm, *where, word, keytotal, font, totalWidth, totalHeight );
				}
			 word [ 0 ] = '\0';

			 if ( ( leftshift ) && ( where->x == 0 ) ) where->x += leftshift;

			 where->x += space + 15; // + 20;
		}
	
	while ( *textptr < 32 ) textptr++;

	where->x = 0;
#ifdef DEBUG
	if ( *textptr++ != '~' ) cout << "No '~' after column headings";
#endif		
	where->y = start.y + (maxdepth + 1) * font->getHeight();
 	
	*keyt = keytotal;
	
	b = 0;

	while ( leave == NO )
		{
			while ( ( *textptr != '~' ) && ( *textptr++ != '@' ) );
			if ( *textptr == '~' ) textptr++;
			dat = *textptr++;
			
			length = 0;
			if ( dat == 's' )
				{

					if ( *textptr == 13 ) textptr += 2;
					where->y += 2* font->getHeight();
						
					while ( ( *textptr != 13 ) && ( *textptr != '~' ) ) word[ length++ ] = *textptr++;
					if ( *textptr == 13 ) textptr += 2;
					
					if ( check != 0 )
						{
							where->x = 0;
							window->setColours( c_subTitle );
							window->setPosition( *where );
							word [ length++ ] = 13;
							word[ length ] = '\0';
							keytotal = scr->writeline( window, bm, *where, word, keytotal, font, totalWidth, totalHeight );
							window->setColours( c_Text );
						}
					where->y += font->getHeight() + 4;
					lastc = columns + 1;
				}
			else if ( dat == 'c' ) 
				{
					where->x = ( space + 15 ) * ( *textptr - '1' );
					 
					if ( *textptr <= lastc ) where->y += font->getHeight() + 4;
					
					lastc = *textptr;
					textptr++;
					if ( *textptr == 13 ) { textptr += 2; where->y += font->getHeight(); }
					while ( *textptr == ' ' ) textptr++;
					if ( *textptr == '*' ) where->y += 3;
					while ( ( *textptr != 13 ) && ( *textptr != '~' ) ) word[ length++ ] = *textptr++;
					if ( *textptr == 13 ) textptr += 2;
					word[ length++ ] = 13;
					word[ length ] = '\0';

					if ( where->x > 0 )
						{
							where->x += leftshift;
							where->x += ( space - font->getWidth( word ) ) / 2; 
						}

					if ( check != 0 )
						{
							window->setPosition( *where );							
							keytotal = scr->writeline( window, bm, *where, word, keytotal, font, totalWidth, totalHeight );
						}
				}
			else if ( dat == 'x' ) leave = YES; 
			else if ( dat == '@' ) { leave = YES; textptr -= 2; }
			// else cout << (int)dat << " ";
		}
	where->x = 0;
	where->y += font->getHeight();
	*keyt = keytotal;

 	return (int)( textptr - store );
}




/*
 *   Displays a page or column of text. It checks if a paragraph can all be
 *  displayed and only then displays it. It checks for keywords and stores
 *  them in the keyinfo data structure ( see memory.h );
 *	 Headings ( ~h ) and jobs ( ~j ) are displayed in seperate pre-defined
 *  ( i.e. in totalwidth, totalheight, text_xpos, text_ypos ) areas of the
 *  screen.
 */

int texttest ( Dplay *scr, FontID n, const Point& wh, short int totalWidth[], short int totalHeight[], char direc, int positions[][5], long int Newspage[], int NewsNum )
{ 
	char *textptr, *storetp;
	Region *bitMap;
	TextWindow* window;
	Region *rheading;
	TextWindow *wheading;
	Region *rjob;
	TextWindow *wjob;

	char *original;
	char *filestart;
	Font *font = fontSet(n);

	MemPtr<char> line(201);
	MemPtr<char> word(MaxIndexLength);

	// char line[ 201 ];
	// char word[ 60 ];

	char dat;
	char length = 0;
	signed char paragraphs = 0;
	char nopara = 0;

	int coordStore = 0;

	const char fincre = 2;		 // Sets the gap between two rows of text

	int linelen = 0;
	char keyno = 0;								  
	char test;
	unsigned char keytotal = 0;
	char count = 0;
	enum until { NO, YES };
	until bold = NO, screen = NO, underline = NO;
	long tsize;
	Point temp;
	int head;
	Point wcheck;
	unsigned char tformat = scr->getFormat();
	unsigned short int textfileptr;
	char check = 0;
	int column = 0;
	int which = 0;
	int Numcols;
	int numa;

	Point coord = wh;

	until quit;
	quit = NO;

	Numcols = scr->getcols();

	if ( Numcols == 0 ) Numcols = 1;

	char z = Numcols;

	numa = scr->getnuma();

	if ( Numcols > 2 ) nopara = 1;

	positions[ICON_Main][4] = 0;

	if ( z++ < ( numa + Numcols ) ) 
	{
		wheading = scr->retWin ( z - 1 );
		rheading = scr->retReg ( z - 1 );
	}

	if ( z++ < ( numa + Numcols ) ) 
	{
		wjob = scr->retWin ( z - 1 );
		rjob = scr->retReg ( z - 1 );
	}

	textfileptr = scr->getTextFP();

	filestart = scr->getOriginal();

	tsize = 0 - scr->getFilePos();

	if ( tsize == -2 ) 
	{
		if ( ( positions[ICON_Quit][4] & 16 ) != 0 )
			positions[ICON_Quit][4] -= 16;
		return ( keytotal + 110);		// end of file;
	}
	else if ( (positions[ICON_Quit][4] & 16 ) == 0 )
		positions[ICON_Quit][4] += 16; // Turn on Next	 	

	original = filestart - tsize; //  getTOffset();	// Points to beginning of text record

	storetp = original;

	tsize += scr->getsz();


	if ( ( ( memory.textbegin < original ) && ( direc == 0 ) ) || ( ( *(memory.textbegin - 3 ) == '@' ) && ( *(memory.textbegin - 4 ) == '@' ) && ( direc == 0 )  ) )
 	{ 

		if ( textfileptr > 0 )
		{
			scr->modTextFP( (char)(-1) );
			textfileptr--; 
		}
		else 
		{ 
#if 0
			textfileptr = scr->findlastTFP() + 1;
#else
			textfileptr = scr->findlastTFP();
#endif
			scr->setTextFP(textfileptr);
		}
	
		tsize = 0 - scr->getFilePos();

		scr->getTOffset();

		scr->getPic();

		if ( tsize == -2 )
		{
#ifdef DEBUG
			cout << "Error in tfp=" << scr->getTextFP() << "    ";
#endif
			scr->setTextFP( 0 ); 
			return ( keytotal + 80);
		}

		original = filestart - tsize;

		memory.textbegin = original;
		memory.textend = original;
		if ( Numcols > 2 ) memory.colbegin = original;
		direc = 1;

		tsize += scr->getsz();
	}

	if ( direc == 0 )
	{
		if ( ( Numcols > 2 ) && ( memory.colbegin == original ) )
		{
		  	if ( textfileptr > 0 )
			{
				scr->modTextFP( (char)(-1) );
				textfileptr--; 
			}
			else
			{ 
#if 0
			 	textfileptr = scr->findlastTFP() + 1;
#else
			 	textfileptr = scr->findlastTFP();
#endif
			 	scr->setTextFP( textfileptr );
			}

			tsize = 0 - scr->getFilePos();

			scr->getTOffset();

			scr->getPic();
		
			NewsNum = 0;

			if ( tsize == -2 )
			{
#ifdef DEBUG
				cout << "Error in tfp=" << scr->getTextFP() << "    ";
#endif
				scr->setTextFP( 0 ); 
				return ( keytotal + 80);
			}

			original = filestart - tsize;
		
			tsize += scr->getsz();

			Newspage[ NewsNum ] = 0;
		} 
		if ( Newspage[ 0 ] != 0 ) 
			Newspage[ 0 ] = 0;
		memory.textbegin = original + Newspage[ NewsNum ];
		memory.textend = original + Newspage[ NewsNum ];
		memory.colbegin = original + Newspage[ NewsNum ];
	
		direc = 1;
	}

	/*
	 * Added by SWG to make sure starts at beginning of word.
	 */

	while(memory.textend[-1] > ' ')
		memory.textend--;


	if ( direc == 1 ) 
		memory.textbegin = memory.textend;				 // Forward
	// 	else memory.textend = memory.textbegin;							 // Back
	

#if 0
	if ( ( textfileptr == 0 ) && ( original == memory.textbegin ) && ( Numcols < 3 ) ) 
	{
		// if ( ( positions[ICON_Quit][4] & 32 ) == 32 )
			positions[ICON_Quit][4] &= ~32; 
	} // Turn off prev. icon
	else	// if ( (positions[ICON_Quit][4] & 32 ) == 0 ) 
		positions[ICON_Quit][4] |= 32; 	

	if ( ( textfileptr > 0 ) && ( (positions[ICON_Quit][4] & 32 ) == 0 ) ) 
		positions[ICON_Quit][4] |= 32; // Turn on Next	 	
#else
	if((textfileptr == 0) && (original == memory.textbegin) && (Numcols < 3)) 
		positions[ICON_Quit][4] &= ~32;
	else
		positions[ICON_Quit][4] |= 32; 	

	if(textfileptr > 0)
		positions[ICON_Quit][4] |= 32; // Turn on Next	 	
#endif

#if 0
	if ( coord.x != 0 ) 		//????? why not just set them?
		coord.x = 0;
	if ( coord.y != 0 ) 
		coord.y = 0;
#else
	coord.x = 0;
	coord.y = 0;
#endif

	// window->setPosition ( coord );

	head = 0;
	line [ 0 ] = '\0';

	if ( *(original - 1) > ' ' ) 
		original--;
	if ( *original == ' ' ) 
		original++;

	if ( original > memory.textbegin ) 
	{ 
		memory.textbegin = original;
		memory.textend = original;
	}

	if ( Numcols > 1 ) 
	{
		if ( memory.colbegin > memory.textend )
 		{
 			memory.textbegin = memory.colbegin;
 			memory.textend = memory.colbegin;
 		}
		else 
			memory.colbegin = memory.textbegin;
	}

	int loopc = 0;

	/*
	 * Added by SWG... make sure textbegin set up correctly
	 */

	memory.textbegin = memory.textend;

	while ( column < Numcols )
	{

		window = scr->retWin ( column );
		bitMap = scr->retReg ( column );
		which = column;
	
		coord.x = 0;
		coord.y = 0;
		window->setPosition ( coord );
		window->setColours( c_Text );

		if ( column > 0 )
		{
			if ( nopara == 0 )
				memory.textend = textptr - length;
			else
				memory.textend = textptr;	
		
			memory.textbegin = memory.textend;		 // Forward
	
			if((textfileptr == 0 ) && (original == memory.textbegin)) 
				positions[ICON_Quit][4] &= ~32;  // Turn off prev. icon
			else
				positions[ICON_Quit][4] |= 32;

			if ( ( *( textptr - 1 )  == '~' ) && ( *textptr >= 'A' )  )
				textptr--;
		}

		// while ( *( memory.textend - 1 ) > 33 )
		while(memory.textend[-1] > ' ')
			memory.textend--;

		check = 0;

		screen = NO;
	
		test = 0;

		while ( screen == NO )
  		{
			wcheck.x = coord.x;
			wcheck.y = coord.y;
			window->setPosition( coord );
		
			quit = NO;
		
			if ( check != 1 )
				textptr = memory.textend;
			else 
			{
				// textptr = memory.textend;
				textptr = memory.textbegin;
				if ( column == 0 ) Newspage[ NewsNum ] = ( long int ) ( textptr - original);
			}
		
			if ( *(textptr - 1) == '~' )
				textptr--;
			if ( ( textptr == original ) && ( check == 0 ) )  		 // == 0
			{
				if ( textptr == filestart )
					keytotal += 150; 
				positions[ICON_Main][4] |= 32;
			}

#ifdef DEBUG
			if ( tsize > 200000 )
			{
				cout << "Error tsize=" << tsize << "   "; tsize = 20;
			}
#endif
	   	if ( check == 1 )
			{
				screen = YES;
				if ( nopara == 1 )
					paragraphs++;
			}
		
			if ( *textptr == 10 ) 
				textptr++;
			while ( *textptr == 13 )
				textptr += 2;

			line[0] = '\0';

	   	while ( ( quit == NO ) && ( ( dat = *textptr++ ) != '\0' ) ) // && ( ( (int)(textptr - memory.textbegin) ) < tsize ) )
			{
				if ( dat == '~' )
	  			{
					if ( ( check != 0 ) && ( bold == YES ) && ( *textptr < 'A' ) )
					{
						bold = NO;
						word[ length++ ] = '~';
					} // linelen -= font->getWidth('~'); }
				
					if ( *textptr == '#' )
					{
						word[ length++ ] = 13;
						word[length] = '\0';
						textptr++;
				
						if ( ( linelen + font->getWidth( word ) ) > bitMap->getW() )
						{
							coord.y += font->getHeight() + fincre;
							if ( ( coord.y + font->getHeight() ) > bitMap->getH() )
								quit = YES;
							if ( check != 0 ) 
							{
								keytotal = scr->writeline ( window, bitMap, coord, line, keytotal, font, totalWidth[ which ], totalHeight[ which ] );   //window->draw( line );
								window->setPosition( coord );
							}
							line[0] = '\0';
							linelen = 0;
					  	}

					  	strcat( line, word );
					  	linelen += font->getWidth( word );

					  	length = 0;

					  	coord.y += font->getHeight() + fincre;
					  	if ( ( coord.y + font->getHeight() ) > bitMap->getH() )
							quit = YES;

						if ( check != 0 ) 
						{
							keytotal = scr->writeline ( window, bitMap, coord, line, keytotal, font, totalWidth[ which ], totalHeight[ which ] );   //window->draw( line );
							window->setPosition( coord );
						}
						linelen = 0;
						line[0] = '\0';

					}
					else if ( length > 0 ) 
  					{
						word[length] = '\0';
						if ( ( linelen + font->getWidth( word ) ) > bitMap->getW() )
						{
							coord.y += font->getHeight() + fincre;
							if ( coord.y > bitMap->getH() ) 
								quit = YES;
							if ( check != 0 ) 
							{
								keytotal = scr->writeline ( window, bitMap, coord, line, keytotal, font, totalWidth[ which ], totalHeight[ which ] );   //window->draw( line );
								window->setPosition( coord );
							}
							line[0] = '\0';
							linelen = 0;
						}
								 								
						strcat( line, word );
						linelen += font->getWidth( word );

					   length = 0;
						 						
					}			 
					
					if ( *textptr < 'A' )
					{
						if ( underline == YES ) 
						{ 
							underline = NO; 
							word [ length++ ] = '~'; 
							word [ length++ ] = 13;
							word [ length ] ='\0';
							strcat( line, word );
							if ( check != 0 )
							{
								window->setPosition( coord );
								keytotal = scr->writeline ( window, bitMap, coord, line, keytotal, font, totalWidth[ which ], totalHeight[ which ] );   //window->draw( line );
							}
							coord.y += font->getHeight() + fincre;
							if ( (coord.y + font->getHeight() ) > bitMap->getH() ) 
								quit = YES;
							window->setPosition( coord );
							linelen = 0;
							line[0] = '\0';
							length = 0;
							// linelen -= 3*( font->getWidth('~') );
						}
					}
					else if ( *textptr == 'x' ) 
					{
						if ( ( linelen > 0 ) && ( check != 0 ) )
						{
							word [ 0 ] = 13;
							word [ 1 ] = '\0';
							strcat ( line, word );
							window->setPosition( coord );
							keytotal = scr->writeline ( window, bitMap, coord, line, keytotal, font, totalWidth[ which ], totalHeight[ which ] );   //window->draw( line );
						}
						if ( linelen > 0 ) 
							coord.y += font->getHeight() + fincre;
						length = 0;
						linelen = 0;
						line [ 0] = '\0';
						textptr += displaytable( scr, textptr, &coord, bitMap, window, &keytotal, font, totalWidth[ which ], totalHeight[ which ], check );

						if ( coord.y > bitMap->getH() ) 
						{
							quit = YES;
							word[0] = '\0';
						}

				  	}
					else if ( ( *textptr == 'b' ) && ( check != 0 ) )				 // Bold
					{				   
						bold = YES;
						word[ length++ ] = '~';
						word[ length++ ] = 'b';
						linelen -= 2*font->getHeight();
						textptr++;
						if ( *textptr == ' ' ) 
							textptr++;
					}
					else if ( ( *textptr == 'p' ) && ( check != 0 ) )				 // Bold
					{				   
						bold = YES;
						word[ length++ ] = '~';
						word[ length++ ] = 'b';
						linelen -= 2*font->getHeight();
						textptr++;
						if ( *textptr == ' ' ) textptr++;
					}
					else if ( *textptr == 's' ) 
					{
						underline = YES;
						word[ length++ ] = '~';
						word[ length++ ] = 's';
						textptr++;
					}

					else if ( ( *textptr == 'h' ) && ( numa > 1 ) )		 // Title
					{
				 		linelen = 0;
						temp = coord;
							
						textptr++;
						if ( *textptr == ' ' )
							textptr++;
						 	
						FontID largen = Font_EMFL32;

						if ( Numcols > 2 )
							largen = FONT_HEADLINE;

				  		font = fontSet( largen ); 
 						   
						wheading->setFont ( largen );

						if ( check != 0 )
						{
					 		wheading->setColours( c_Title );
							coord.y = head;
				   		wheading->setPosition( coord );
						}
						else
							coord.y = 0;

						while ( *textptr != '~' ) 	
						{
							while ( ( ( dat = *textptr ) != 13 ) && ( *textptr++ != '~' ) )
							{
								if ( dat != ',' ) word[ length++ ] = dat;

							}
						
							if ( dat == 13 )
								textptr += 2;
							if ( ( *textptr == '~' ) && ( *( textptr + 1 ) == '^' ) )
								textptr += 2;
							if ( *(textptr - 1) == '~' )
								textptr--;
							word [ length ] = '\0';
					
							coord.x = ( rheading->getW() - font->getWidth ( word ) ) / 2;

							count = 0;
						
							while ( ( coord.x < 1 ) && ( count != length ) ) 
							{
			
								while ( ( word [ count++ ] != ' ' )	&& ( count < length ) );
								
								if ( count != length )
								{
									if ( ( word [ count ] == '/' ) && ( word[ count + 1 ] == ' ' ) ) 
										count = count + 2;
									word [ count - 1 ] = '\0';
									coord.x = ( rheading->getW() - font->getWidth ( word ) ) / 2;
									if ( check != 0 ) 
									{
										wheading->setPosition( coord );
										wheading->draw ( word );
									}
			
									coord.y += font->getHeight() + fincre;
								
									for ( char h = 0; h < length - count; h++ )
									{
										word [ h ]	= word[ count + h ];
									}

									word [h] = '\0';
									coord.x = ( rheading->getW() - font->getWidth ( word ) ) / 2;
								}
							}
						
							if (check != 0 )
							{
								if ( coord.x < 0 ) coord.x = 0;
							  	wheading->setPosition( coord );
								wheading->draw ( word );
							}
						
							coord.y += font->getHeight() + fincre;
						
							if ( ( largen != FONT_HEADLINE ) && ( dat == 13 ) )
							{
								largen = FONT_HEADLINE;
								font = fontSet( largen );
				 				wheading->setFont ( largen  );
							}		
							length = 0;
						}
						if ( check == 0 )
						{
							head = ( rheading->getH() - coord.y ) / 2;
						}
					
						coord.x = 0;
						coord.y = 0;
						if ( check != 0 )
						{
							wheading->setPosition( coord );
							wheading->setColours( c_Text );
						}

						font = fontSet(n);
						wheading->setFont ( n );

						if ( *textptr == '~' ) textptr++;
					
						coord = temp;
					} 
					else if ( *textptr == 'H' )		 // Title
					{
						linelen = 0;
					
						// temp = coord;
					
						coord.y = 0;
					
						quit = YES;

						textptr++;
						if ( *textptr == ' ' ) textptr++;
					
						FontID largen = Font_EMFL32;

						// if ( Numcols > 2 ) largen = FONT_HEADLINE;

						font = fontSet( largen ); 
 					
						window->setFont ( largen );

						if ( check != 0 )
						{
							window->setColours( c_PageTitle );
							coord.y = head;
						   window->setPosition( coord );
						}

						while ( *textptr != '~' ) 	
						{
							while ( ( ( dat = *textptr ) != 13 ) && ( *textptr++ != '~' ) )
							{
								if ( dat != ',' ) 
									word[ length++ ] = dat;
							}
							word [ length ] = '\0';
						
							if ( dat == 13 )
								textptr += 2;
							if ( ( *textptr == '~' ) && ( *( textptr + 1 ) == '^' ) ) 
								textptr += 2;
							if ( *(textptr - 1) == '~' )
								textptr--;
					
#if 0
							coord.x = ( bitMap->getW() - font->getWidth ( word ) ) / 2;

							count = 0;
						
							while ( ( coord.x < 1 ) && ( count != length ) ) 
							{
			
								while ( ( word [ count++ ] != ' ' )	&& ( count < length ) )
									;
								
								if ( count != length )
								{
									if ( ( word [ count ] == '/' ) && ( word[ count + 1 ] == ' ' ) ) count = count + 2;
									word [ count - 1 ] = '\0';
									coord.x = ( bitMap->getW() - font->getWidth ( word ) ) / 2;
									if ( check != 0 ) 
									{
										window->setPosition( coord );
										window->draw ( word );
									}
			
									coord.y += font->getHeight() + fincre;
								
									for ( char h = 0; h < length - count; h++ )
										word [ h ]	= word[ count + h ];

									word [h] = '\0';
									coord.x = ( bitMap->getW() - font->getWidth ( word ) ) / 2;
								}
							}
					
							if (check != 0 )
							{
								if ( coord.x < 0 ) coord.x = 0;
							  	window->setPosition( coord );
								window->draw ( word );
							}
					
							coord.y += font->getHeight() + fincre;
					
#else
							/*
							 * Simplified Version using TextWindow to wrap text
							 */

							if(check != 0)
							{
								window->setWrap(True);
								window->setHCentre(True);
								// window->setVCentre(True);
								window->draw(word);
								window->newLine();
							}
#endif

							if ( ( largen != FONT_HEADLINE ) && ( dat == 13 ) )
							{
								largen = FONT_HEADLINE;
								font = fontSet( largen );
				 				window->setFont ( largen );
							}		
							length = 0;
						}

#if 0
						if ( check == 0 )
							head = ( bitMap->getH() - coord.y ) / 2;
#else
						if(check == 0)
							head = 0;
#endif

						if ( check != 0 )
							window->setColours( c_Text );
	
						font = fontSet(n);
						window->setFont ( n );

						if ( *textptr == '~' ) 
							textptr++;
				
						coord.x = 0;

						line[0] = '\0';
						word[0] = '\0';
					}

					else if ( *textptr == 't' )		 // Tab	= 4 spaces
					{
						textptr++;
						word[0] = ' ';
						word[1]	= ' ';
						word[2] = ' ';
						word[3] = ' ';
						word[4] = '\0';
					
						 			
						if ( ( linelen + font->getWidth( word ) ) > bitMap->getW() )
						{
							if ( ( coord.y + font->getHeight() ) > bitMap->getH() ) quit = YES;
							coord.y += font->getHeight() + fincre;
							if (check != 0 )
							{
								keytotal = scr->writeline ( window, bitMap, coord, line, keytotal, font, totalWidth[ which ], totalHeight[ which ] );   //window->draw( line );
							   window->setPosition( coord );
							}
							line[0] = '\0';
							linelen = 0;
						}
								 							
						strcat( line, word );
						linelen += font->getWidth( word );	  
						length = 0;
					
					}
					else if ( *textptr == 'j' )	
					{	
						if ( check == 0 )	 			// Job
						{
							while ( *textptr++ != '~' );	
						}
						else
						{
							temp = coord;
							coord.y = 0;
							// wjob->setPosition( coord );
						
							wjob->setFont( Font_JAME08 );

							linelen = 0;
							textptr++;
							wjob->setColours( c_Title );
							length = 0;
							while ( *textptr != '~' ) 	
							{
								while ( ( ( dat = *textptr ) != 13) && ( *textptr++ != '~' ) )
						 		{
									word[ length++ ] = dat;

								}
								if ( dat == 13 ) textptr += 2;
							
								word [ length ] = '\0';
								if ( *textptr == 'l')
								{
									textptr++;
									coord.x = 0;
									coord.y = ( rjob->getH() - font->getHeight() ) / 2;
								}
								else coord.x = ( rjob->getW() - font->getWidth ( word ) ) / 2;
					
								if ( *(textptr - 1) == '~' ) textptr--; // So to quit first while loop
					
								wjob->setPosition( coord );

								wjob->draw ( word );
							
								if ( coord.x != 0 ) coord.y += font->getHeight() + fincre;
								length = 0;
							}

							wjob->setColours( c_Text );
						
							wjob->setFont( Font_JAME08 );

							textptr++;
							while ( ( *textptr < '!' ) && ( *textptr != '\0' ) ) textptr++;
						
							coord = temp;
						}

					 }
				}			  
				else if ( dat == '@' ) 
	  			{

					if ( *textptr == '@' )
					{
						keyno = 0;
					
						word[ length++ ] = 13;
						word[ length ] = '\0';

						if ( check == 0 ) 
				 		{
				 			if ( nopara == 0 ) 
							paragraphs++;
				 		}
						else 
						{
							paragraphs--;

							if ( ( ( positions[ICON_Main][4] & 16 ) != 16 ) && ( scr->findlastTFP() > 0 ) ) positions[ICON_Main][4] += 16;

							if ( paragraphs == 0 ) quit = YES; 
							strcat( line, word );
							keytotal = scr->writeline ( window, bitMap, coord, line, keytotal, font, totalWidth[ which ], totalHeight[ which ] );   //window->draw( line );
							line[0] = '\0';
							word[0] = '\0';
							// length = 0;
						}

						if ( ( linelen + font->getWidth( word ) ) > bitMap->getW() )
 						{
	 							coord.y += font->getHeight() + fincre;
								if ( ( coord.y + font->getHeight() ) > bitMap->getH() ) 
								{
									quit = YES;

									textptr -= length;
									length = 0;
									word[0] = '\0';
								}

								if ( check != 0 ) 
								{	
									keytotal = scr->writeline ( window, bitMap, coord, line, keytotal, font, totalWidth[ which ], totalHeight[ which ] );   //window->draw( line );
									window->setPosition( coord );
								}

								line[0] = '\0';
								linelen = 0;
						}

			 			strcat( line, word );
						linelen = font->getWidth( word );
					
						if ( ( quit == NO ) && ( check != 0 ) )
							keytotal = scr->writeline ( window, bitMap, coord, line, keytotal, font, totalWidth[ which ], totalHeight[ which ] );   //window->draw( line );
						if ( check != 0 )
						{
							coord.y += 2 * font->getHeight();

							if ( coord.y > bitMap->getH() ) coord.y = bitMap->getH() - 1;

							coord.x = ( bitMap->getW() ) / 3;
					
							bitMap->HLine( coord.x, coord.y, coord.x, Black );
						}
					
						quit = YES;
										
						if ( *(textptr + 1 ) == '@' )
						{ 
							keytotal += 110;
							textptr--;   // -= 2;
							word[0] = '\0';
							if ( ( scr->findlastTFP() == 0 ) && ( ( positions[ICON_Quit][4] & 16 ) == 16 ) ) positions[ICON_Quit][4] -= 16;
							if ( ( Numcols > 2 ) && ( ( positions[ICON_Quit][4] & 16 ) == 16 ) ) positions[ICON_Quit][4] -= 16;

 							// cout << "End of file ";
						}
					
						line[0] = '\0';
								 						
						length = 0;
						coord.x = 0;
					}
					else if ( keyno == 0)
					{
						keyno = 1;
						word[ length++ ] = dat;
						if ( *textptr == ' ' ) 
						{
						#ifdef DEBUG
							cout << "*tptr= 'space' : poss. error";
						#endif
							textptr++;
						}
					}	  
					else 
					{
						if ( ( *textptr != 13 ) && ( *textptr != ' ' ) && ( linelen + font->getWidth( word ) - font->getWidth( '@' ) ) > bitMap->getW() )
						{

							coord.y += font->getHeight() + fincre;
							if ( (coord.y + font->getHeight()) > bitMap->getH() )
							{
								quit = YES;
								textptr -= (length + 1);
								if ( nopara == 1 ) textptr -= 1;
								length = 0;
								// word[0] = '\0';
							}
							if ( check != 0 )
							{

								keytotal = scr->writeline ( window, bitMap, coord, line, keytotal, font, totalWidth[ which ], totalHeight[ which ] );   //window->draw( line );
								window->setPosition( coord );
							}

							line[ 0 ] = '\0';
							word [ length ] = '\0';
							strcpy( line, word );

							linelen = font->getWidth ( word ); // + length;
							length = 0;
						}

						word [length ] = '\0';

						strcat ( line, word );
						strcat ( line, "@" );

						linelen += font->getWidth( word ) - font->getWidth( '@' );

						if ( ( *textptr != ' ' ) && ( *textptr != 13 ) )
						{
							length = 1;
							word[ 0 ] = *textptr++;
							while ( ( *textptr != ' ' ) && ( *textptr != 13 ) && ( *textptr != '@' ) && ( *textptr != '~' ) ) word [ length++ ] = *textptr++;
							word[ length ] = '\0';
							strcat ( line, word );
							linelen += font->getWidth ( word );
						}

						length = 0;
						word[ 0 ] = '\0';
						keyno = 0;
					}
		
				}
	
				else if ( dat == ' ' ) 
				{
				if ( keyno == 0 )
		  			{
				
						if ( ( linelen + font->getWidth( word ) + font->getWidth( dat ) ) > bitMap->getW() )
						{
							word[ length] = '\0';
							strcat( line, word );

							//textptr--;
							coord.y += font->getHeight() + fincre;
							if ( ( coord.y + font->getHeight() ) > bitMap->getH() ) { quit = YES; if ( nopara < 1 ) textptr--; } 

							if ( check != 0 ) 
					 		{
								keytotal = scr->writeline ( window, bitMap, coord, line, keytotal, font, totalWidth[ which ], totalHeight[ which ] );   //window->draw( line );
								window->setPosition( coord );
							}
					
							line[0] = '\0';
							linelen = 0;
					
							length = 0;
						} 
						else
						{
							if ( *textptr != 13 ) 
				 			{
								word [ length++ ] = ' ';
								word [ length ] = '\0';
					
								if ( ( linelen + font->getWidth( word ) ) > bitMap->getW() )
								{
									coord.y += font->getHeight() + fincre;
									if ( (coord.y + font->getHeight() )  > bitMap->getH() ) { quit = YES; textptr -= ( length + 1 ); }
							 									
									if ( check != 0 ) 
							  		{
							 			keytotal = scr->writeline ( window, bitMap, coord, line, keytotal, font, totalWidth[ which ], totalHeight[ which ] );   //window->draw( line );
							 			window->setPosition( coord );
							 		}
										
									line[0] = '\0';
									linelen = 0;
										
								}
								 								
								strcat( line, word );
								linelen += font->getWidth( word ); //  + length;
								length = 0;
							}			 

						}
						// word [ length ] = '\0';	
					}
					else 
					{
						if ( length >= 60 ) length = 0;
						word [ length++ ] = ' ';
						// word [ length ] = '\0';
					}
	  			}
				else if ( dat == 13 )
	  			{

					if ( *textptr ==  10 ) textptr++;
					if ( *textptr == 13 )
					{
						word [ length++ ] = 13;
						word [ length ] ='\0';
  						textptr++;
						if ( *textptr == 10 ) textptr++;   

						if ( ( linelen + font->getWidth( word ) ) > bitMap->getW() )
				 		{
							if ( check != 0 ) keytotal = scr->writeline ( window, bitMap, coord, line, keytotal, font, totalWidth[ which ], totalHeight[ which ] );   //window->draw( line );
							coord.y += font->getHeight() + fincre;
							if ( (coord.y + font->getHeight() )  > bitMap->getH() ) { quit = YES; if ( check == 1 ) paragraphs--; }
							line[0] = '\0';
							window->setPosition( coord );
							linelen = 0;
						}
  				
						if ( quit != YES )
						{
							strcat( line, word );
							if ( check != 0 )
							{
								keytotal = scr->writeline ( window, bitMap, coord, line, keytotal, font, totalWidth[ which ], totalHeight[ which ] );   //window->draw( line );
								paragraphs--;

								if ( paragraphs < 1 ) quit = YES;  
							}
							else  
							{	
								paragraphs++;

								if ( paragraphs > 20 ) 
									{
									#ifdef DEBUG
										cout << "Error in paragraphs " << coord.y << "   ";
									#endif
										quit = YES;
									}
								quit = YES;
							}
						
							coord.y += 2 * ( font->getHeight() + fincre );
							if ( coord.y + font->getHeight() > bitMap->getH() ) quit = YES; 

							line[0] = '\0';
								 								
							if ( *textptr == 13 ) 
							{
								textptr += 2;
								coord.y += font->getHeight() + fincre;
								if ( (coord.y + font->getHeight() ) > bitMap->getH() ) quit= YES;
		
							}
				
							textptr--;
							window->setPosition( coord );

							linelen = 0; 
							length = 0;
				
							if ( ( ( quit == NO ) && ( check == 0 ) ) || ( ( paragraphs == 0 ) && ( quit == YES ) ) )
							{
								memory.textend = textptr;
							}

							// if ( check == 0 ) quit = YES;
							word[ length ] = '\0';
						}
			  		}
					else if ( keyno != 1 )
			  		{  
						word[ length ] = '\0';

						if ( ( linelen + font->getWidth( word ) + font->getWidth( ' ' ) ) < ( bitMap->getW() ) - 10 )
						{
							word [length++] = ' ';
							word [ length ] = '\0';
						}
									
						if ( ( linelen + font->getWidth( word ) ) > bitMap->getW() )
						{
							coord.y += font->getHeight() + fincre;
							if ( ( coord.y + font->getHeight() ) > bitMap->getH() ) 
							{ 
								quit = YES; 
								if ( nopara < 1 ) textptr -= length;
							  	else textptr -= ( length -1 );
							}
										  								
							if ( check != 0 ) 
						  	{
								keytotal = scr->writeline ( window, bitMap, coord, line, keytotal, font, totalWidth[ which ], totalHeight[ which ] );   //window->draw( line );
								window->setPosition( coord );
							}

							line[0] = '\0';
							linelen = 0;
						}

						strcat( line, word );
						linelen += font->getWidth( word ); // + length;
										
						length = 0;
						word [ 0 ] = '\0';
					} 
	 		 		else 
						word[ length++ ] = ' ';
				}
				else
				{
					if ( dat > 31 )
					{
						word[ length++ ] = dat;
						word[ length ] = '\0';
			
						if ( ( linelen + font->getWidth( word ) ) > bitMap->getW() ) 
				  		{
							coord.y += font->getHeight() + fincre;
							linelen = 0;
							if ( ( coord.y + font->getHeight() ) > bitMap->getH() ) 
							{ 
								if ( keytotal > 150 ) keytotal -= 150;
								quit = YES;
								textptr--;
								if ( keyno != 0 ) 
								{
									textptr -= length;
									length = 0;
									word[0] = '\0';
									keyno = 0;
								}
								else if ( nopara == 1 )
								{ 
									textptr -= ( length - 1);
									while ( *(textptr-1) > 33 ) textptr--;
								}
							}

						   if ( check != 0 ) 
							{
							 	keytotal = scr->writeline ( window, bitMap, coord, line, keytotal, font, totalWidth[ which ], totalHeight[ which ] );   //window->draw( line );
							 	window->setPosition( coord );
							}
							line[0] = '\0';
						}  
					}
				} 
			}

			quit = NO;	

			coordStore = coord.y;

			if ( check != 0 )
			{
	
				if ( (coord.y + font->getHeight() ) > bitMap->getH() ) { memory.textend = textptr; screen = YES; }
				wcheck.x = coord.x;
				wcheck.y = coord.y;

				if ( ( ( keytotal >= 110 ) && ( keytotal < 150 ) ) || ( ( *(textptr - 1) == '@' ) && ( *(textptr ) == '@' ) ) )
				{
					memory.textend = textptr;
					paragraphs = 0;
					if ( ( ( positions[ICON_Quit][4] & 16 ) == 0 ) && ( scr->findlastTFP() > 0 ) ) positions[ICON_Quit][4] += 16;			// Turn Icon on
				}
				if ( paragraphs < 1 ) screen = YES;
			}
			else
		  	{
		 		if ( paragraphs == 0 ) check = 1;

		 		if ( ( *textptr == '@' ) && ( *( textptr + 1 ) == '@' ) && ( *( textptr + 2 ) == '@' ) ) check = 1;
		 		if ( ( ( *storetp == '@' ) && ( *(storetp - 1 ) == '@' )	) || ( ( *(textptr - 1) == '@' ) && ( *(textptr ) == '@' ) ) )
	 			{
 					check = 1;
 				}
 				if ( ( keytotal >= 110 ) && ( keytotal < 150 ) )
	 			{
 					keytotal -= 110;
 					check = 1;
 					memory.textend = textptr;
 				}

		 		if ( ( coord.y + font->getHeight() ) > bitMap->getH() ) 
 				{
 					if ( check == 1 ) memory.textend = textptr;
	 				check = 1;
	 				test = 0;
 				}
		 		else 
	 			{
 					if ( ( *(textptr - 1 ) == '@' ) && ( *textptr == '@' ) )
 					{
 						textptr = memory.textend;
 					}
 					else
 					{
 						memory.textend = textptr;
 					}
 				} 
	 		}

			if ( check == 1 )
			{
				coord.x = 0; // wcheck.x;
				coord.y = 0; // wcheck.y;
				window->setPosition( coord );
			}

			length = 0;
			word[ 0 ] = '\0';
			linelen = 0;
			keyno = 0;
			if ( dat == '\0' ) quit = YES;

			loopc++;
			if ( loopc > 40 ) 
			{ 
				quit = YES; 
				screen = YES; 
			}

		}  //  check while

		if ( ( positions[ICON_Main][4] & 16 ) != 0 )
		{
			if ( scr->getFormat() == '9' )
			{
				SpriteLibrary *newiconlib;
				newiconlib = new SpriteLibrary( "dbdata\\icons_1.spr" );

				if ( coord.y == 0 ) coord.y = coordStore + 6;

				while ( column < Numcols )
				{
					coord.x = 6;

					while( (coord.y + 29) < bitMap->getH() )
					{
						newiconlib->drawSprite( bitMap, coord, 8 );

						coord.y += 29;
					}

					machine.screen->setUpdate( *bitMap );

					column++;

					if ( column < Numcols )
					{
						window = scr->retWin ( column );
						bitMap = scr->retReg ( column );
			
						coord.x = 0;
	 					coord.y = 0;
	 					window->setPosition ( coord );
	 					window->setColours( c_Text );
					}
				}

				delete newiconlib;

			}

			column = Numcols;

		}

		if ( ( keytotal >= 110 ) && ( keytotal < 150 ) ) 
		{ 
			if ( scr->getFormat() == '9' )
			{
				SpriteLibrary *newiconlib;
				newiconlib = new SpriteLibrary( "dbdata\\icons_1.spr" );

				if ( coord.y == 0 ) coord.y = coordStore;

				while ( column < Numcols )
				{

					coord.x = 6;

					while( (coord.y + 29) < bitMap->getH() )
					{
						newiconlib->drawSprite( bitMap, coord, 8 );

						coord.y += 29;
					}

					machine.screen->setUpdate( *bitMap );

					column++;

					if ( column < Numcols )
					{
						window = scr->retWin ( column );
						bitMap = scr->retReg ( column );
			
						coord.x = 0;
	 					coord.y = 0;
	 					window->setPosition ( coord );
	 					window->setColours( c_Text );
					}
				}

				delete newiconlib;

			}

			textptr--; 
			column = Numcols;
		}

		column++;

	}  // columns

	if ( ( *(textptr - 1 ) == '@' ) && (*(textptr ) == '@') ) 
		keytotal += 80; // -1
#ifdef DEBUG 
	if ( memory.textbegin < filestart	)
	{
		cout << " Error memory.textbegin < filestart!! ";
	}
#endif
	if ( *(textptr - 1) == '\0' ) 
		keytotal += 80;

	if ( ( textfileptr == 0 ) && ( original == memory.textbegin ) )  
		if ( keytotal < 150 ) 
			keytotal += 150; 

	return keytotal;
}


void Dplay::SetText( char dis, int reset )
{

	HowMany = SetUpDi ( getjumpcount(), reset );

	if(getTextFP() > 0)
		positions[page][4] |= 32;
							 
	SetUpWindows(); // page, totalWidth, totalHeight, text_xpos, text_ypos );

	if ( getError() != 0 ) 
		{
			quit = YES;

			return;
		}

	switch( page )
		{
			case 1:				  // biography index
				changewins( 2, 0, page, totalWidth, totalHeight, text_xpos, text_ypos );
				break;

			case 2:
				changewins( 0, 3, page, totalWidth, totalHeight, text_xpos, text_ypos );
				getPic();
				break;
			case 4:
				changewins( 0, 3, page, totalWidth, totalHeight, text_xpos, text_ypos );
				break;

			case 5:
				changewins( 0, 3, page, totalWidth, totalHeight, text_xpos, text_ypos );
				break;

			case 6:
			case 15:
			case 16:
			case 17:
			case 21:
			case 23:
			case 24:
			case 22:
			case 26:
				changewins( 0, 1, page,	totalWidth, totalHeight, text_xpos, text_ypos );
				break;
			case 7:				  // Battle index;
				changewins( 1, 0, page, totalWidth, totalHeight, text_xpos, text_ypos );
				break;
			case 8:
			case 18:
				changewins( 2, 0, page, totalWidth, totalHeight, text_xpos, text_ypos );
				break;
			case 9:
				changewins( 3, 2, page, totalWidth, totalHeight, text_xpos, text_ypos );
				break;

			case 10:
				changewins( 2, 0, page, totalWidth, totalHeight, text_xpos, text_ypos );
				break;

			case 11:
				changewins( 1, 0, page,	totalWidth, totalHeight, text_xpos, text_ypos );
				break;
			case Page_Songs:
				changewins( 1, 0, page, totalWidth, totalHeight, text_xpos, text_ypos );
				break;
			case 28:
				changewins( 2, 0, page, totalWidth, totalHeight, text_xpos, text_ypos );
				break;

			case Page_Songs2:
				changewins( 0, 2, page,	totalWidth, totalHeight, text_xpos, text_ypos );
				break;

			default :
			#ifdef DEBUG
				cout << "Error in setting text: page=" << page; 
				getch();
			#endif
				changewins( 0, 3, page, totalWidth, totalHeight, text_xpos, text_ypos );
				error	= 1;
				break;
		}

	setwin( totalHeight, totalWidth );

	if ( getError() != 0 ) 
	{
		quit = YES;

		return;
	}


	if ( dis == 0 ) 
		{
			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
			sortKeytotal();
		}

	if ( ind > 0 ) 
		{ 
			index->distroy(); 
			delete index;
			ind = 0; 
		}
}

void Dplay::getRetIndex()
{
	char temp = page;

	switch( temp )
		{
			case Page_Bio2:
				page = Page_Bio1;			 // Biography index
				getIndex();
				break;
			case Page_Battles:					 // Battle index
				page = Page_Battles2;
				getIndex();
				break;
			case Page_BriefHistory:
			case Page_History:
			case Page_Chrono:
			case Page_History2:
				page = Page_HistorySelect;			 // History selection
				SetNonText();
				break;
			case Page_Features:					 // Features
				page = Page_Features2;			 
				getIndex();
				break;
			case Page_Songs2:					 // Music
				page = Page_Songs;
				getIndex();
				break;
			case Page_Artillery:
			// case 16:
			case Page_InfantryWeapons:
			case Page_CavalryWeapons:
			case Page_NavalWeapons:
				page = Page_Weapons;
				SetNonText();
				break;
			case Page_CavalryTactics:
			case Page_NavalTactics:
			case Page_InfantryTactics:
			case Page_ArtilleryTactics:
				page = Page_Tactics;
				SetNonText();
				break;

			default:
#ifdef DEBUG
				cout << "Error in getIndex: page=" << ( int ) page; 
				getch();
#endif
				page = Page_Bio1;
				error = 1;
				break;
		}
}

void Dplay::getIndex()
{
	SetText( 1 );
	
	if ( error ) return;

	index = new Index ( this, n );
	ind = getcols();

	machine.mouse->moveMouse( Point( text_xpos[0] + 8, text_ypos[0] + 8 ) );

 	if ( page == Page_Battles2 || page == Page_Features2 || page == Page_Songs ) 
	{
		// Region headreg(machine.screen->getImage(), Rect( 70, 22, 230, 375));
		Region headreg(machine.screen->getImage(), Rect(BOOK_X1, BOOK_Y1, BOOK_W1, BOOK_H1));
 		TextWindow headWin(&headreg, Font_EMFL32);
		headWin.setColours(c_PageTitle);

		/*
		 * Use textwindow's built in word wrap!
		 */

		/*
		 * Get Title
		 */

		char* title;
		if ( page == Page_Battles2 ) 
			title = language(lge_battle_title);
		else if ( page == Page_Features2 )
			title = language( lge_feature_title);
		else 
			title = language( lge_songs_title);

		headWin.setHCentre(True);
		headWin.setVCentre(True);
		headWin.setWrap(True);
		headWin.draw(title);
	}
}
				
void Dplay::PrevPara()
{

	if ( getcols() > 0 ) 
		{
			if ( skipPtr > 2 ) resetwin();
				else resetwin( 1 );
		}
	else 
		{
			resetarea( text );
		}

	deltextspr( totalWidth[0], totalHeight[ 0 ] );

	// if ( ( getFormat() == '9' ) && ( getTextFP() == 25 ) && ( NewsNum == 0 ) )
	//	skipPtr = 3;
	
	if ( NewsNum > 0 ) NewsNum--;
 
	if ( ( skipPtr == 3 ) || ( skipPtr == 13 ) )
		{
			unsigned short int lasttfp = findlastTFP();

			// if ( ( getFormat() == '9' ) && ( getTextFP() == 0 ) ) lasttfp = 24;

			skipPtr = 1;

			if ( lasttfp != 0 )
 				{
					setTextFP( lasttfp );
					// modTextFP( 1 );
					memory.textbegin = getOriginal() + getFilePos();

					memory.textend = memory.textbegin;
					memory.colbegin = memory.textbegin;
					getPic();
					keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );

				}

		}
	else 
		{
			if ( skipPtr != 1 ) skipPtr = 1;
			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 0, positions, Newspage, NewsNum );
		}

	sortKeytotal();
	
	if ( ( positions[ 0 ][4] & 16) != 16 ) positions[ICON_Quit][4] += 16;

}

void Dplay::NextPara()
{
	// int storePicFlag = getPicFlag();


	if ( getcols() > 0 ) 
		{
			if ( skipPtr == 2 ) resetwin();
				else resetwin( 1 );
		}
	else 
		{
			resetarea( text );
		}
	
	if ( NewsNum < 60 ) NewsNum++;
		else 
			{
			#ifdef DEBUG
				dialAlert( 0, "NewsNum toooo big!!", "Oops!" );
			#endif
			}
	
	deltextspr( totalWidth[0], totalHeight[ 0 ] );
	
	// if ( ( getFormat() == '9' ) && ( getTextFP() == 24 ) && ( skipPtr != 1 ) ) skipPtr = 2;
														 
	if ( ( skipPtr == 0 ) || ( skipPtr == 13 ) )
		{
			modTextFP( 1 );
			skipPtr = 1;
			memory.textbegin = getOriginal() ;
			memory.textend = getOriginal();
			getPic();
		}
	else if ( skipPtr == 2 ) 
		{
			// if ( ( getFormat() == '9' ) && ( getTextFP() == findlastTFP() ) ) setTextFP( 25 );
 			//	else
			setTextFP( 0 );
			skipPtr = 1;
			memory.textbegin = getOriginal();
			memory.textend = getOriginal();
			if ( page == Page_Features )memory.colbegin = getOriginal();
			getPic();
		}
	else skipPtr = 1;

	keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );

	sortKeytotal();

	positions[ICON_Quit][4] |= 32;
}

void Dplay::getKeywordDisplay()
{
	char dat;
	char oldpage = page;
	// int storePicFlag = getPicFlag();
	long temp;
	char key = (char)( tret - HowMany );
	
	int oldNewsNum = NewsNum;
	
	char OldSP = skipPtr;
	int OldSLFs	= sprlibflags;

	skipPtr = 1;
	sprlibflags &= 1;

	if ( getcols() > 0 )
	 	{
	 		if ( memory.textbegin < memory.colbegin ) pushFormat( (int)( memory.textbegin - getOriginal() ), NewsNum, Newspage );
	  			else pushFormat( (int)( memory.colbegin - getOriginal() ), NewsNum, Newspage );
		}
	else pushFormat( (int)( memory.textbegin - getOriginal() ), NewsNum, Newspage );

	if ( strcmp( keyinfo[key].name, language( lge_cavalry ) ) == 0 ) 
		{
			tret = 7;
			page = Page_Tactics;
			getIcon();
			return;
		}
	else if ( strcmp( keyinfo[key].name, language( lge_naval ) ) == 0 )
		{
			tret = 8;
			page = Page_Tactics;
			getIcon();
			return;
		}

	temp = findkeyword( getOrig(), (char)( tret - HowMany ), getPtrsz() );

	if (temp < 0) 
	{
		wipeOut();
#ifdef DEBUG
		dialAlert ( 0, "Sorry, text file is currently unavailable", "OK" );
#endif
		wipeIn();
		skipPtr = OldSP;
		sprlibflags = OldSLFs;
		return;
	}

   dat = getKeyOffset( (char)( tret - HowMany ));	// ,&BasePtrOffset );

	if ( getFormat() == '0' ) 
		{
#if !defined(COMPUSA_DEMO)
			if ( dat < 7 ) 
			{
				// showFullScreen( "dbdata\\flictext");
				
				page = Page_Animations;

				HowMany = SetUpDi	( getjumpcount() );
		
				changewins(); // page, totalWidth, totalHeight, text_xpos, text_ypos );

				wipeOut( True );

				disFlic( dat );

				skipPtr = OldSP;
				sprlibflags = OldSLFs; 
		
				machine.mouse->setPointer( M_Busy );

				wipeIn();

				NewsNum = oldNewsNum;

				UseMemory();		 
				return;
	
			}
			else
			{
				int mapNo = dat - 7;

				if((mapNo >= DBMapCount) || (mapNo < 0))
					mapNo = DBMapCount - 1;

				showFullScreen(mapNames[mapNo]);

				skipPtr = OldSP;
				sprlibflags = OldSLFs; 
		
				// machine.mouse->setPointer( M_Busy );

				page = Page_Main;

				NewsNum = oldNewsNum;

				machine.screen->update();

				machine.mouse->hide();

				while ( !machine.mouse->getEvent() );

				machine.mouse->show();

				UseMemory();		 
		
				return;
			}
#else
			demoFeature();
			return;
#endif		
		}

	page = getFormat() - '0';
#ifdef DEBUG	
	if ( ( (int)( memory.textbegin - getOriginal() ) >= getsz() ) )
		{
			cout << " Bad news with getKeywordDisplay!! ";
			getch();
		}
#endif
	if ( page > 26 ) page = Page_Bio2;
	
	if ( page == oldpage ) 
		{
			//if ( getcols() > 0 ) resetwin();
			//	else resetarea( text );
			
			deltextspr( totalWidth[0], totalHeight[ 0 ] );

			setTextFP( dat );
			
			memory.textbegin = getOriginal() ;
			memory.textend = getOriginal();
			if ( getcols() > 2 ) memory.colbegin = getOriginal();

			if ( ( positions[ICON_Quit][4] & 4 ) == 0 ) positions[ICON_Quit][4] += 4;
			
		}
	else
		{
			HowMany = SetUpDi ( getjumpcount(), 1 );
			
			sprIndex = 0;

			if(getTextFP() > 0)
				positions[page][4] |= 32;
							 
			SetUpWindows(); // page, totalWidth, totalHeight, text_xpos, text_ypos );

			if ( getError() != 0 ) 
			{
				quit = YES;

				return;
			}

			switch( page )
				{
					case 1:				  // biography index
						changewins( 2, 0, page, totalWidth, totalHeight, text_xpos, text_ypos );
						break;
	
					case 2:
						changewins( 0, 3, page, totalWidth, totalHeight, text_xpos, text_ypos );
						// getPic();
						break;
					case 4:
						changewins( 0, 3, page, totalWidth, totalHeight, text_xpos, text_ypos );
						break;
	
					case 5:
						changewins( 0, 3, page, totalWidth, totalHeight, text_xpos, text_ypos );
						break;

					case 6:
					case 15:
					case 16:
					case 17:
					case 21:
					case 23:
					case 24:
					case 22:
					case 26:
						changewins( 0, 1, page,	totalWidth, totalHeight, text_xpos, text_ypos );
						break;
					case 7:				  // Battle index;
						changewins( 1, 0, page, totalWidth, totalHeight, text_xpos, text_ypos );
						break;
					case 9:
						changewins( 3, 2, page, totalWidth, totalHeight, text_xpos, text_ypos );
						break;
					case 8:
					case 18:
						changewins( 2, 0, page, totalWidth, totalHeight, text_xpos, text_ypos );
						break;
					case 10:
						changewins( 2, 0, page, totalWidth, totalHeight, text_xpos, text_ypos );
						break;
					case 11:
						changewins( 1, 0, page, totalWidth, totalHeight, text_xpos, text_ypos );
						break;
					case Page_Songs:
						changewins( 1, 0, page, totalWidth, totalHeight, text_xpos, text_ypos );
						break;
					case 30:
						changewins( 1, 0, page, totalWidth, totalHeight, text_xpos, text_ypos );
						break;


					case Page_Songs2:
						changewins( 0, 2, page, totalWidth, totalHeight, text_xpos, text_ypos );
						break;
					case 28:
						changewins( 2, 0, page, totalWidth, totalHeight, text_xpos, text_ypos );
						break;

					default :
#ifdef DEBUG
						cout << "Error in setting text: page=" << page; 
						getch();
#endif
						changewins( 0, 3, page, totalWidth, totalHeight, text_xpos, text_ypos );
						error	= 1;
						break;
				}

			if ( getError() != 0 ) 
			{
				quit = YES;

				return;
			}

			setwin( totalHeight, totalWidth );

			if ( ind > 0 ) 
				{ 
					index->distroy(); 
					delete index;
					ind = 0; 
				}

			SetText( 1 );						 
			setTextFP( dat );
			// sprIndex = 0;
		}
	
	keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
	
	getPic();
	
	sortKeytotal();

	if ( getjumpcount() < 2 )
	{
		Region *tempreg;
		tempreg = new Region ( machine.screen->getImage(), Rect ( positions[ICON_Memory][0], positions[ICON_Memory][1], positions[ICON_Memory][2], positions[ICON_Memory][3] ) ); 
		opiconlib->drawCentredSprite( tempreg, 3 );
		machine.screen->setUpdate( *tempreg );
		delete tempreg;
		positions[ICON_Memory][4] |= 4;
	}	 
}


void Dplay::sortKeytotal()
{
	Region *tempreg;

	// showString( Point( 0,0 ), "[1][4]=%u", positions[ICON_Main][4] );
#ifdef DEBUG
	// cout << "\b\b\b\b\b\b\b\b\b\b\b\b" << positions[ICON_Main][4] << " " << (int)keytotal << " " << sprlibflags;
#endif

	if ( ( sprlibflags & 254 ) != 0 )
	{
		if ( ( sprlibflags & 32 ) == 32 )  // && ( ( keytotal < 80 ) || ( ( keytotal >= 110 ) && ( keytotal < 190 ) ) ) )
		{
			tempreg = new Region( machine.screen->getImage(), Rect ( positions[ICON_Prev][0], positions[ICON_Prev][1], positions[ICON_Prev][2], positions[ICON_Prev][3] ) ); 
			opiconlib->drawCentredSprite( tempreg, 0 );
			sprlibflags -= 32;
			machine.screen->setUpdate( *tempreg );
			delete tempreg;
		}
		if ( ( ( sprlibflags & 16 ) == 16 ) && ( ( keytotal < 110 ) || ( ( keytotal >= 150 )  && ( keytotal < 190 ) ) ) )
		{
			tempreg = new Region ( machine.screen->getImage(), Rect ( positions[ICON_Next][0], positions[ICON_Next][1], positions[ICON_Next][2], positions[ICON_Next][3] ) ); 
			opiconlib->drawCentredSprite( tempreg, 1 );
			sprlibflags -= 16;
			machine.screen->setUpdate( *tempreg );
			delete tempreg;
		}
	}

	if ( ( ( positions[ICON_Main][4] & 16 ) == 16 ) && ( findlastTFP() > 0 ) )
		{
			// skipPtr = 0;
			// if ( ( keytotal >= 80 ) && ( keytotal < 110 ) ) keytotal = keytotal - 80;
			
			if ( ( sprlibflags & 16 ) != 16 ) 
				{
					tempreg = new Region( machine.screen->getImage(), Rect ( positions[ICON_Next][0], positions[ICON_Next][1], positions[ICON_Next][2], positions[ICON_Next][3] ) ); 
					opiconlib->drawCentredSprite( tempreg, 6 );
					sprlibflags += 16;
					machine.screen->setUpdate( *tempreg );
					delete tempreg;
					if ( ( positions[ICON_Memory][4] & 16 ) != 0 ) positions[ICON_Memory][4] -= 16;
					// skipPtr = 0;
				}
		}

	if ( ( ( positions[ICON_Main][4] & 32 ) == 32 ) && ( findlastTFP() > 0 ) ) 
		{
			if ( ( sprlibflags & 32 ) != 32 ) 
			{
				tempreg = new Region( machine.screen->getImage(), Rect ( positions[ICON_Prev][0], positions[ICON_Prev][1], positions[ICON_Prev][2], positions[ICON_Prev][3] ) ); 
				opiconlib->drawCentredSprite( tempreg, 5 );
				sprlibflags += 32;
				machine.screen->setUpdate( *tempreg );
				delete tempreg;
				positions[ICON_Memory][4] &= ~32;
				if ( getTextFP() == 0 ) skipPtr = 3;
			}
		}
	
	if ( keytotal >= 80 )
	{
		if ( keytotal < 110 )
		{
			skipPtr = 0;
			keytotal = keytotal - 80;
			if ( ( sprlibflags & 16 ) != 16 ) 
			{
				tempreg = new Region( machine.screen->getImage(), Rect ( positions[ICON_Next][0], positions[ICON_Next][1], positions[ICON_Next][2], positions[ICON_Next][3] ) ); 
				opiconlib->drawCentredSprite( tempreg, 6 );
				sprlibflags += 16;
				machine.screen->setUpdate( *tempreg );
				delete tempreg;
				if ( ( positions[ICON_Memory][4] & 16 ) != 0 ) positions[ICON_Memory][4] -= 16;
			}

		}
		else
		{
			if ( keytotal < 150 )
			{
				keytotal -= 110;
				if ( getTextFP() > 0 ) 
				{
					skipPtr = 2;
					positions[ICON_Quit][4] |= 16;
					if ( ( sprlibflags & 16 ) != 16 ) 
 					{
						tempreg = new Region( machine.screen->getImage(), Rect ( positions[ICON_Next][0], positions[ICON_Next][1], positions[ICON_Next][2], positions[ICON_Next][3] ) ); 
						opiconlib->drawCentredSprite( tempreg, 6 );
						sprlibflags += 16;
						machine.screen->setUpdate( *tempreg );
						delete tempreg;
						positions[ICON_Memory][4] &= ~16;
					}
				}
				else 
					positions[ICON_Quit][4] &= ~16;
			}
			else 
			{
				if ( keytotal >= 190 )
				{
					
					keytotal -= 190;
					if ( findlastTFP() == 0 )
 					{
 						positions[ICON_Quit][4] &= ~32;
 						positions[ICON_Quit][4] &= ~16;
 					} 
					else
 					{
 						skipPtr = 13;
 						if ( ( sprlibflags & 16 ) != 16 ) 
 						{
 							tempreg = new Region( machine.screen->getImage(), Rect ( positions[ICON_Next][0], positions[ICON_Next][1], positions[ICON_Next][2], positions[ICON_Next][3] ) ); 
							opiconlib->drawCentredSprite( tempreg, 6 );
							sprlibflags += 16;
							machine.screen->setUpdate( *tempreg );
							delete tempreg;
							positions[ICON_Memory][4] &= ~16;
						}

						if ( ( sprlibflags & 32 ) != 32 ) 
 						{
							tempreg = new Region( machine.screen->getImage(), Rect ( positions[ICON_Prev][0], positions[ICON_Prev][1], positions[ICON_Prev][2], positions[ICON_Prev][3] ) ); 
							opiconlib->drawCentredSprite( tempreg, 5 );
							sprlibflags += 32;
							machine.screen->setUpdate( *tempreg );
							delete tempreg;
							positions[ICON_Memory][4] &= ~32;
						}
					}
				}
				else 
				{
					keytotal -= 150;
					if ( getTextFP() == 0 )
					{
						if ( findlastTFP() > 0 )
						{
							skipPtr = 3;
							positions[ICON_Quit][4] |= 32;
							if ( ( sprlibflags & 32 ) != 32 ) 
 							{
								tempreg = new Region( machine.screen->getImage(), Rect ( positions[ICON_Prev][0], positions[ICON_Prev][1], positions[ICON_Prev][2], positions[ICON_Prev][3] ) ); 
								opiconlib->drawCentredSprite( tempreg, 5 );
								sprlibflags += 32;
								machine.screen->setUpdate( *tempreg );
								delete tempreg;
								positions[ICON_Memory][4] &= ~32;
							}
						}
						else 
						{
							positions[ICON_Quit][4] &= ~32;
							// if ( ( positions[ICON_Quit][4] & 16 ) != 16 ) positions[ICON_Quit][4] += 16;
						}
					}
				}
			}
		}
	}
}	

void Dplay::waitOnMouse()
{
	machine.mouse->clearEvents();
	machine.mouse->getEvent();
	// machine.mouse->waitEvent();
	machine.mouse->setPointer( M_Arrow );

	do
	{
		machine.mouse->waitEvent();

	} while ( !Mouse::LeftButton );
}


/*
 *   Sees which icon has been selected for the screen 'page'.
 */


void Dplay::getIcon()
{
	int dat;

	if ( page == Page_Pictures )
	{
#if !defined(COMPUSA_DEMO)
		Image picback;

		wipeOut( True );

		dat = index->getHighlight( (char)(tret - HowMany + 1) );

		if ( ( tret - HowMany) > 0 ) 
		{
			char temp = index->getnumitems() / 2;
			if ( ( index->getnumitems() % 2 ) != 0 ) temp++;
			
			dat += temp;
		}

		// picback.resize( 640, 480 );

		// machine.unBlit( &picback, Point( 0, 0 ) );

		Region pbm(machine.screen->getImage(), Rect( 0, 0, 639, 479 ));

		SpriteLibrary* piclib = new SpriteLibrary( "anim\\pics" );

		if ( dat < 67 ) piclib->drawCentredSprite( &pbm ,dat );

		machine.screen->setUpdate( pbm );

		machine.screen->update();   // pbm

		// machine.mouse->setPointer( M_Arrow );

		waitOnMouse();

		delete piclib;

		// machine.blit( &picback, Point( 0, 0 ) );

		pushFormat( (int)( memory.textbegin - getOriginal() ), NewsNum, Newspage );

		page = Page_Main;

		UseMemory();		 

		machine.mouse->setPointer( M_Busy );

		wipeIn();
#else
		demoFeature();
#endif
		
		return;
	}

	switch ( page )
		{
			case Page_Main:							  // Main menu
				sprIndex = 0;
				skipPtr = 1;
				switch ( tret )
					{
						case ICON_Biog:				  // Biography Index
							page = Page_Bio1;
							setFormat ( '1' );
							
							if ( error ) return;

							getIndex();
							break;
						case ICON_Weapon:				  // Weapon Selection
							page = Page_Weapons;		  
							SetNonText();
							if ( error ) return;

							break;
						case ICON_Tactics:				  // Tactics Selection
							page = Page_Tactics;		
							setFormat( 'c' );
							SetNonText();
							if ( error ) return;

							break;
						case ICON_History:				  // History selection;
							page = Page_HistorySelect;
							setFormat( '<' );
							SetNonText();
							if ( error ) return;

							break;
						case ICON_Battles:				  // Battle Index
							page = Page_Battles2;
							setFormat( '7' );
							if ( error ) return;
							getIndex();
							break;
						case ICON_Features:				  // Feature Articles
							page = Page_Features2;
							setFormat( ';' );
							// setTextFP( 0 );
							// SetNonText();
							getIndex();

							break;
						
						case ICON_Photos:				  // Photos
#if defined(COMPUSA_DEMO)
							demoFeature();
#else
							page = Page_Pictures;
							setFormat( 'L' );
							// setTextFP( 0 );
							// SetNonText();
							getIndex();
#endif

							break;

						case ICON_Songs:				  // Songs
							page = Page_Songs;
							setFormat( ';' );
							// setTextFP( 0 );
							// SetNonText();
							getIndex();

							break;
						default:
#ifdef DEBUG
							cout << "Error in getIcon2: page=" << (int)page << " tret=" << (int)tret << "  ";
							getch();
#endif
							error = 1;
							break;
					}
				break;
			case Page_Bio1:					  // Biography list
				page = Page_Bio2;
				setFormat( '2' );
				skipPtr = 1;

				dat = index->getHighlight( (char)(tret - HowMany + 1) );
				if ( ( tret - HowMany) > 0 ) 
					{
						char temp = index->getnumitems() / 2;
						if ( ( index->getnumitems() % 2 ) != 0 ) temp++;
						dat += temp;
					}

				sprIndex = 0;

				SetText( 1 );
				setTextFP ( (char)dat );
				
				keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
				
				sortKeytotal();
				break;

			case Page_Weapons:
				sprIndex = 0;
				skipPtr = 1;
				switch( tret )
					{
						case 9:
							page = 15;  						
							setFormat( '?' );
		  					setTextFP( 0 );
							SetText();
							break;

					 	case 7:
							page = 22;
							format = 'F';
		  					setTextFP( 0 );
							SetText();
							break;

					 	case 8:
							page = 26;
							format = 'J';
							setTextFP( 0 );
							SetText();
							break;
					 		
						case 6:
							page = 17;
							setFormat( 'A' );
							setTextFP( 0 );
							SetText();
							break;
							
						default:
							wipeOut();
#ifdef DEBUG
						 	dialAlert ( 0, "Sorry, weapon text file is currently unavailable", "OK" );
#endif
							wipeIn();
							error = 1;
							break;
					 }
				break;
			case 7:

				page = 4;
				setFormat( '4' );
				skipPtr = 1;

				dat = index->getHighlight( (char)(tret - HowMany + 1) );
				if ( (signed)( tret - HowMany) > 0 ) 
					{
						char temp = index->getnumitems() / getcols();
						if ( ( index->getnumitems() % getcols() ) != 0 ) temp++;
						dat += temp;
					}
				SetText( 1 );
				setTextFP ( (char)dat );
				
				keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
				
				sortKeytotal();
				break;
			case 12:											 // History selection;
				sprIndex = 0;
				skipPtr = 1;
				switch( tret )
					{
						case 6:
							page = 5;  						 // Chronology;
							setFormat( '5' );
		  					setTextFP( 0 );
							SetText();
							break;

						case 9:								 // Hist 1861
							page = 18;
 							setFormat( 'B' );
							setTextFP( 0 );
							SetText();
							break;
	
						case 8:								 // Hist 1863
							page = 8;
							setFormat( '8' );
							setTextFP( 0 );
							SetText();
							break;
							
						case 7:								 // Brief
							page = 10;
							setFormat( ':' );
							setTextFP( 0 );
							SetText();
							break;
							
						default:
#ifdef DEBUG
						 	dialAlert ( 0, "Sorry, History text file is currently unavailable", "OK" );
#endif

							// error = 1;
							break;
					 }
				break;
			case 11:								// Features
				page = 9;
				setFormat( '9' );
				skipPtr = 1;
				sprIndex = 0;

				dat = index->getHighlight( (char)(tret - HowMany + 1) );

				SetText( 1 );
				setTextFP ( (char)dat );
				
				keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
				
				sortKeytotal();
				
				break;

 			case Page_Songs:
				page = Page_Songs2;
				setFormat ( 'O' );
				skipPtr = 1;
				sprIndex = 0;

				dat = index->getHighlight( (char)(tret - HowMany + 1) );
				
				SetText( 1 );
				setTextFP ( (char)dat );
				
				keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
				
				sortKeytotal();

				if ( dat < DBSongCount ) playsong( dat );
						  					 
				break;

			case 13:
				sprIndex = 0;
				skipPtr = 1;
				dat = index->getHighlight( (char)(tret - HowMany + 1) );
				if ( (signed)( tret - HowMany) > 0 ) 
					{
						char temp = index->getnumitems() / getcols();
						if ( ( index->getnumitems() % getcols() ) != 0 ) temp++;
						dat += temp;
					}
				playsong( dat );
				break;
			case 19:						// Tactics
				sprIndex = 0;
				skipPtr = 1;
				switch( tret )
					{
						case 7:
							page = 16;
							setFormat ( '@' );
		  					setTextFP( 0 );
							SetText();
							break;

						case 8:
							page = 21;
							setFormat( 'E' );
							setTextFP( 0 );
							SetText();
							break;

						case 6:
							page = 23;
							setFormat( 'G' );
							setTextFP( 0 );
							SetText();
							break;
						case 9:
							page = 24;
							setFormat( 'H' );
							setTextFP( 0 );
							SetText();
							break;

						default:
							wipeOut();
#ifdef DEBUG
						 	dialAlert ( 0, "Sorry, Tactic text file is currently unavailable", "OK" );
#endif
							wipeIn();
							error = 1;
							break;
					 }
				break;

			default:
			 	wipeOut();
#ifdef DEBUG
				dialAlert ( 0, "Sorry, text file is currently unavailable", "OK" );
#endif
				wipeIn();
				// cout << "Error in getIcon1: page=" << (int)page << " tret=" << (int)tret << "  ";
				// error = 1;
				break;
		}
}

void Dplay::UseMemory()
{
	int tempptr = popFormat( &format, &NewsNum, Newspage );
	char tfp = getTextFP();
	char oldpage = page;
	
	/*
	 * This line added by SWG to prevent pictures and things coming out
	 * on returning screen after returning from bio.
	 */

	skipPtr = 1;

	int OldNewsNum = NewsNum;

	switch( getFormat() ) 
		{				
		 case '8':		// Hist 1863
			page = 8;

		setupPage:
			OldNewsNum = NewsNum;
		

			if ( oldpage == page ) { resetwin(); SetText( 1, 0 ); deltextspr( totalWidth[0], totalHeight[ 0 ] ); }
				else { sprIndex = 0; SetText ( 1 ); }
			
	 		setTextFP( 0 );

			memory.colbegin = tempptr + getOriginal();
			memory.textbegin = memory.colbegin;
			memory.textend = memory.colbegin;
			
			if ( OldNewsNum ) NewsNum = OldNewsNum;

			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
			break;

		 case '?':
		
			page = 15;
			goto setupPage;
#if 0
			if ( oldpage == page ) { resetwin(); SetText( 1, 0 ); deltextspr( totalWidth[0], totalHeight[ 0 ] ); }
				else { sprIndex = 0; SetText ( 1 ); }
			
	 		setTextFP( 0 );

			memory.colbegin = tempptr + getOriginal();
			memory.textbegin = memory.colbegin;
			memory.textend = memory.colbegin;

			if ( OldNewsNum ) NewsNum = OldNewsNum;

			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
			break;
#endif

		 case '@':
		 
			page = 16;
			goto setupPage;
#if 0
			OldNewsNum = NewsNum;

			if ( oldpage == page ) { resetwin(); SetText( 1, 0 ); deltextspr( totalWidth[0], totalHeight[ 0 ] ); }
				else { sprIndex = 0; SetText ( 1 ); }
			
	 		setTextFP( 0 );

			memory.colbegin = tempptr + getOriginal();
			memory.textbegin = memory.colbegin;
			memory.textend = memory.colbegin;

			if ( OldNewsNum ) NewsNum = OldNewsNum;

			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
			break;
#endif		 
		case 'A':
		
			page = 17;
			goto setupPage;

#if 0
			OldNewsNum = NewsNum;

			if ( oldpage == page ) { resetwin(); SetText( 1, 0 ); deltextspr( totalWidth[0], totalHeight[ 0 ] ); }
				else { sprIndex = 0; SetText ( 1 ); }
			
	 		setTextFP( 0 );

			memory.colbegin = tempptr + getOriginal();
			memory.textbegin = memory.colbegin;
			memory.textend = memory.colbegin;

			if ( OldNewsNum ) NewsNum = OldNewsNum;

			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
			break;
#endif

		 case ':':
		
			page = 10;
			goto setupPage;
			
#if 0
			OldNewsNum = NewsNum;

			if ( oldpage == page ) { resetwin(); SetText( 1, 0 ); deltextspr( totalWidth[0], totalHeight[ 0 ] );}
				else {  sprIndex = 0; SetText ( 1 ); }
			
	 		setTextFP( 0 );

			memory.colbegin = tempptr + getOriginal();
			memory.textbegin = memory.colbegin;
			memory.textend = memory.colbegin;

			if ( OldNewsNum ) NewsNum = OldNewsNum;

			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
			break;
#endif

		 case '2':		// Bio2
			page = 2;
			
			if ( oldpage == page ) { resetwin(); SetText( 1, 0 ); deltextspr( totalWidth[0], totalHeight[ 0 ] ); }
				else { sprIndex = 0; SetText ( 1 ); }

			setTextFP( tfp );

			getPic(); 				// due to changing textfileptr above;

			memory.colbegin = tempptr + getOriginal();
			memory.textbegin = memory.colbegin;
			memory.textend = memory.colbegin;
			
		  	getTitles();

			if ( OldNewsNum ) NewsNum = OldNewsNum;

			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
			
			break;

		case '4':
		 	page = 4;
		setupPage4:

			if ( oldpage == page ) { resetwin(); SetText( 1, 0 ); deltextspr( totalWidth[0], totalHeight[ 0 ] ); }
				else { sprIndex = 0; SetText ( 1 ); }

			setTextFP( tfp );

			memory.colbegin = tempptr + getOriginal();
			memory.textbegin = memory.colbegin;
			memory.textend = memory.colbegin;

			if ( (int)( memory.textbegin - getOriginal() ) >= getsz() )
				{
#ifdef DEBUG
					cout << " Bad news with usememory!! ";
					getch();
#endif			

					tfp = 2;
				}

		  	getTitles();
			
			if ( OldNewsNum ) NewsNum = OldNewsNum;

			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
			break;

		 case '5':
		 	page = 5;
			goto setupPage4;
			
#if 0
			if ( oldpage == page ) { resetwin(); SetText( 1, 0 ); deltextspr( totalWidth[0], totalHeight[ 0 ] ); }
				else { sprIndex = 0; SetText ( 1 ); }

			setTextFP( tfp );

			memory.colbegin = tempptr + getOriginal();
			memory.textbegin = memory.colbegin;
			memory.textend = memory.colbegin;

			if ( (int)( memory.textbegin - getOriginal() ) >= getsz() )
				{
#ifdef DEBUG
					cout << " Bad news with usememory!! ";
					getch();
#endif
					tfp = 2;
					setTextFP( tfp );
				}

		  	getTitles();
			
			if ( OldNewsNum ) NewsNum = OldNewsNum;

			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
			break;
#endif
		case '9':
		 	// if ( tfp < 25 )
			page = 9;
			
			// else page = 20;
			
			if ( oldpage == page ) 
			{
				resetwin();
				
				//if ( tfp != 24 ) 
				SetText( 1, 0 );
				//else SetText( 1 );

				deltextspr( totalWidth[0], totalHeight[ 0 ] );
			}
			else 
			{ 
				sprIndex = 0; 
				SetText ( 1 );
			}

			setTextFP( tfp );

			memory.colbegin = tempptr + getOriginal();
			memory.textbegin = memory.colbegin;
			memory.textend = memory.colbegin;

			if ( (int)( memory.textbegin - getOriginal() ) >= getsz() )
				{
#ifdef DEBUG
					cout << " Bad news with usememory!! ";
					getch();
#endif
					tfp = 2;
					setTextFP( tfp );
				}

		  	getTitles();
			
			if ( OldNewsNum ) NewsNum = OldNewsNum;

			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
			
			if ( tfp > 24 ) playsong( tfp - 25 );

			break;
		 case 'B':
		 	page = 18;
			goto setupPageE;
#if 0
			OldNewsNum = NewsNum;

			sprIndex = 0; 
			SetText ( 1 );
			
	 		setTextFP( 0 );

			memory.colbegin = tempptr + getOriginal();
			memory.textbegin = memory.colbegin;
			memory.textend = memory.colbegin;

			if ( OldNewsNum ) NewsNum = OldNewsNum;

			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
			break;
#endif
		 case 'E':
		 	page = 21;
		setupPageE:
	
			OldNewsNum = NewsNum;
			
			sprIndex = 0; 
			SetText ( 1 );
			
	 		setTextFP( 0 );

			memory.colbegin = tempptr + getOriginal();
			memory.textbegin = memory.colbegin;
			memory.textend = memory.colbegin;

			if ( OldNewsNum ) NewsNum = OldNewsNum;

			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
			break;

		case 'G':
		 	page = 23;
			goto setupPageE;
#if 0			
			OldNewsNum = NewsNum;
			
			sprIndex = 0; 
			SetText ( 1 );
			
	 		setTextFP( 0 );

			memory.colbegin = tempptr + getOriginal();
			memory.textbegin = memory.colbegin;
			memory.textend = memory.colbegin;

			if ( OldNewsNum ) NewsNum = OldNewsNum;

			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
			break;
#endif
		 case 'H':
		 	page = 24;
			goto setupPageE;
#if 0			
			OldNewsNum = NewsNum;

			sprIndex = 0; 
			SetText ( 1 );
			
	 		setTextFP( 0 );

			memory.colbegin = tempptr + getOriginal();
			memory.textbegin = memory.colbegin;
			memory.textend = memory.colbegin;

			if ( OldNewsNum ) NewsNum = OldNewsNum;

			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
			break;
#endif
		 case 'F':
		 	page = 22;
			goto setupPageE;
#if 0	
			OldNewsNum = NewsNum;

			sprIndex = 0; 
			SetText ( 1 );
			
	 		setTextFP( 0 );

			memory.colbegin = tempptr + getOriginal();
			memory.textbegin = memory.colbegin;
			memory.textend = memory.colbegin;

			if ( OldNewsNum ) NewsNum = OldNewsNum;

			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
			break;
#endif
		 case 'J':
		 	page = 26;
			goto setupPageE;
#if 0	
			OldNewsNum = NewsNum;

			sprIndex = 0; 
			SetText ( 1 );
			
	 		setTextFP( 0 );

			memory.colbegin = tempptr + getOriginal();
			memory.textbegin = memory.colbegin;
			memory.textend = memory.colbegin;

			if ( OldNewsNum ) NewsNum = OldNewsNum;

			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
			break;
#endif
		 case 'D':
		 	page = 20;
			goto setupPage;
#if 0
			OldNewsNum = NewsNum;

			if ( oldpage == page ) { resetwin(); SetText( 1, 0 ); deltextspr( totalWidth[0], totalHeight[ 0 ] ); }
			else { sprIndex = 0; SetText ( 1 ); }

			sprIndex = 0; 
			SetText ( 1 );
			
	 		setTextFP( tfp );

			memory.colbegin = tempptr + getOriginal();
			memory.textbegin = memory.colbegin;
			memory.textend = memory.colbegin;

			if ( OldNewsNum ) NewsNum = OldNewsNum;

			keytotal = texttest( this, n, Point(0,0), text_xpos, text_ypos, 1, positions, Newspage, NewsNum );
			break;
#endif
		 case 'L':
		 	page = Page_Pictures;

			getIndex();

			// sortKeytotal();

			positions[ICON_Quit][4] = 1 + 2;

			return;

			break;

		 default:
#ifdef DEBUG
		   cout << "Error in UseMemory: format =" << getFormat() << " (" << (int)format << ")   ";
			// getch();
#endif
			break;
		}

	sortKeytotal();
	
	if ( getjumpcount() == 0 )
		{
			Region *tempreg;

			tempreg = new Region ( machine.screen->getImage(), Rect ( positions[ICON_Memory][0], positions[ICON_Memory][1], positions[ICON_Memory][2], positions[ICON_Memory][3] ) ); 
			tempreg->greyOut( Rect( 0, 0, positions[ICON_Memory][2], positions[ICON_Memory][3] ), 16 );
			machine.screen->setUpdate( *tempreg );
			delete tempreg;
			if ( ( positions[ICON_Memory][4] & 4 ) != 0 ) positions[ICON_Memory][4] -= 4;
		}
}	


/*
 *   Stores positions of hotspots for the icons, per page.
 */


char Dplay::SetUpDi( int count, int reset )
{

 char HowMany = 0;

 sprlibflags &= 1;

 NewsNum = 0;
 // positions[ICON_Quit][4] = 0;

 positions[ICON_Memory][4] = 0;
 positions[6][0] = 0;
 positions[6][1] = 0;

 if ( n != Font_JAME08 ) n = Font_JAME08;

 switch( page )					 
  {
	default:
		// cout << "Oops! There's an error in SetUpDi in the page number";
#ifdef DEBUG
		dialAlert ( 0, "That page is not currently available", "OK" );
#endif
		page = Page_Main;

   case Page_Main:							  // Main database screen

		#define MAINICON_W 130
		#define MAINICON_H 98
		#define MAINICON_X1 117
		#define MAINICON_X2 374
		#define MAINICON_Y1 11
		#define MAINICON_Y2 118
		#define MAINICON_Y3 225
		#define MAINICON_Y4 332


		positions[ICON_Biog][0] = MAINICON_X1;	  // biogs     // xpos		 top left
		positions[ICON_Biog][1] = MAINICON_Y1;						// ypos
		positions[ICON_Biog][2] = MAINICON_W;					   // width
		positions[ICON_Biog][3] = MAINICON_H;	  					// height
		
		positions[ICON_Weapon][0] = MAINICON_X1;	  	  // weapon Index
		positions[ICON_Weapon][1] = MAINICON_Y2;			
		positions[ICON_Weapon][2] = MAINICON_W;
		positions[ICON_Weapon][3] = MAINICON_H;
		
		positions[ICON_Tactics][0] = MAINICON_X1;	  // Tactics
		positions[ICON_Tactics][1] = MAINICON_Y3;
		positions[ICON_Tactics][2] = MAINICON_W;
		positions[ICON_Tactics][3] = MAINICON_H;
		

		positions[ICON_History][0] = MAINICON_X2;	  // History
		positions[ICON_History][1] = MAINICON_Y2;
		positions[ICON_History][2] = MAINICON_W;
		positions[ICON_History][3] = MAINICON_H;

		positions[ICON_Battles][0] = MAINICON_X2;	  // Battles
		positions[ICON_Battles][1] = MAINICON_Y1;
		positions[ICON_Battles][2] = MAINICON_W;
		positions[ICON_Battles][3] = MAINICON_H;

		positions[ICON_Features][0] = MAINICON_X2;	  // Features
		positions[ICON_Features][1] = MAINICON_Y3;
		positions[ICON_Features][2] = MAINICON_W;
		positions[ICON_Features][3] = MAINICON_H;

		positions[ICON_Photos][0] = MAINICON_X1;	  	  // photos
		positions[ICON_Photos][1] = MAINICON_Y4;
		positions[ICON_Photos][2] = MAINICON_W;
		positions[ICON_Photos][3] = MAINICON_H;

		positions[ICON_Songs][0] = MAINICON_X2;	  	  // Songs
		positions[ICON_Songs][1] = MAINICON_Y4;
		positions[ICON_Songs][2] = MAINICON_W;
		positions[ICON_Songs][3] = MAINICON_H;

		positions[ICON_Quit][4] =
			(1 << ICON_Quit) |
			(1 << ICON_Biog) |
			(1 << ICON_Weapon) |
			(1 << ICON_Tactics) |
			(1 << ICON_History) |
			(1 << ICON_Battles) |
			(1 << ICON_Features) |
			(1 << ICON_Photos) |
			(1 << ICON_Songs);
		
		if ( reset ) showFullScreen( "dbdata\\datamain" );

		HowMany = 14;
		break;

	case Page_Bio1:							  // Biographies index

		positions[ICON_Quit][4] = 1 + 2;

		if ( count > 0 )
			{
				positions[ICON_Quit][4] += 4;
			}
		
		if ( reset ) showFullScreen( "dbdata\\biogslct");

		positions[ICON_Memory][4] = 4;				

		HowMany = 6;
		break;

	case Page_Bio2:							  // Biography text screen

		if ( reset ) showFullScreen( "dbdata\\biogtext");

		positions[ICON_Quit][4] = 1 + 2 + 8 + 16 + 32;

		if ( count > 0 )
			{
				positions[ICON_Quit][4] += 4;
			}
		HowMany = 6;

		break;

	case Page_Weapons:							 // Weapon catagories
	 	//di[0] = MM_NoSound;
		//di[1] = MM_Sound;
		positions[6][0] = 125;	  // Infantry
		positions[6][1] = 13;
		positions[6][2] = 113;
		positions[6][3] = 105;	  
		
		positions[7][0] = 126;	  // Cavary
		positions[7][1] = 262;
		positions[7][2] = 113;
		positions[7][3] = 105;
		
		positions[8][0] = 382;	  // Naval
		positions[8][1] = 260;
		positions[8][2] = 113;
		positions[8][3] = 105;
		

		positions[9][0] = 383;	  // Artillary
		positions[9][1] = 13;
		positions[9][2] = 113;
		positions[9][3] = 105;

	
		positions[ICON_Quit][4] = 1 + 2 + 64 + 128 + 256 + 512;

		if ( count > 0 )
			{
				positions[ICON_Quit][4] += 4;
			}


		if ( reset ) showFullScreen( "dbdata\\weapslct");
						
		HowMany = 10;
		break;

	case Page_Battles:							  //  Battle text screen
		positions[ICON_Quit][4] = 1 + 2 + 8 + 16 + 32; 
		
		if ( count > 0 )
			{
				positions[ICON_Quit][4] += 4;
			}

		if ( reset ) showFullScreen( "dbdata\\tacttext");

		HowMany = 6;
		break;

	case Page_Chrono:							  // Chrono Text screen
	case Page_Songs2:							  // Song text screen;
	case Page_NavalTactics:
	case Page_InfantryTactics:
	case Page_ArtilleryTactics:
	case Page_CavalryWeapons:
	case Page_NavalWeapons:
		positions[ICON_Quit][4] = 1 + 2 + 8 + 16 + 32;
		
		if ( count > 0 )
			{
				positions[ICON_Quit][4] += 4;
			}

		if ( reset ) showFullScreen( "dbdata\\tacttext");

		HowMany = 6;
		break;

	case Page_Testtabl:							  	// Testtable ( Tactics Selection )
		positions[ICON_Quit][4] = 1 + 2;
		
		if ( count > 0 )
			{
				positions[ICON_Quit][4] += 4;
			}

		if ( reset ) showFullScreen( "dbdata\\tacttext" );

		HowMany = 6;
		break;

	case Page_Artillery:							  // Weapon texts
	case Page_CavalryTactics:
	case Page_InfantryWeapons:
		positions[ICON_Quit][4] = 1 + 2 + 8 + 16 + 32;
		
		if ( count > 0 )
			{
				positions[ICON_Quit][4] += 4;
			}

		if ( reset ) showFullScreen( "dbdata\\tacttext" );

		HowMany = 6;
		break;

	case Page_Pictures:
	case Page_Songs:
	case Page_Battles2:								// Battle index
		positions[ICON_Quit][4] = 1 + 2;
		if ( count > 0 )
			{
				positions[ICON_Quit][4] += 4;
			}
		
		if ( reset ) showFullScreen( "dbdata\\battslct");
		positions[ICON_Memory][4] = 4;
				
		HowMany = 6;
		break;

	case Page_History:
		positions[ICON_Quit][4] = 1 + 2 + 8 + 16 + 32;
		
		if ( reset ) showFullScreen( "dbdata\\history");  // battslct

		if ( count > 0 )
			{
				positions[ICON_Quit][4] += 4;
			}
		
		HowMany = 6;
		break;

	case Page_Features:
		positions[ICON_Quit][4] = 1 + 2 + 8 + 16 + 32;
		
		if ( count > 0 )
			{
				positions[ICON_Quit][4] += 4;
			}

		if ( reset ) showFullScreen( "dbdata\\features" );

		// n = Font_JAME_5;
		n = Font_JAME08;

		HowMany = 6;
		break;
	
	case Page_HistorySelect:
		positions[6][0] = 125;	  // Chronology      // xpos		 top left
		positions[6][1] = 13;	  						// ypos
		positions[6][2] = 113;					   	// width
		positions[6][3] = 105;	  						// height

		positions[7][0] = 126;	  // Full History
		positions[7][1] = 262;
		positions[7][2] = 113;
		positions[7][3] = 105;
		
		positions[8][0] = 383;	  // Full history
		positions[8][1] = 260;
		positions[8][2] = 113;
		positions[8][3] = 105;
		

		positions[9][0] = 382;	  // Brief History
		positions[9][1] = 13;
		positions[9][2] = 113;
		positions[9][3] = 105;


		positions[ICON_Quit][4] = 1 + 2 + 64 + 128 + 256 + 512;		// Enable Icons
		
		if ( reset ) showFullScreen( "dbdata\\histslct");

		if ( count > 0 )
			{
				positions[ICON_Quit][4] += 4;
			}
		
		HowMany = 10;
		break;

	case 10:
	case 18:
		positions[ICON_Quit][4] = 1 + 2 + 8 + 16 + 32;
		
		if ( reset ) showFullScreen( "dbdata\\history");

		if ( count > 0 )
			{
 				positions[ICON_Quit][4] += 4;
			}
		
		HowMany = 6;
		break;
	case Page_Features2:
		positions[ICON_Quit][4] = 1 + 2;

		if ( count > 0 )
			{
				positions[ICON_Quit][4] += 4;
			}

		if ( reset ) showFullScreen( "dbdata\\battslct");
		positions[ICON_Memory][4] = 4;

		HowMany = 6;
		break;

	case 30:
		positions[ICON_Quit][4] = 1 + 2 + 8 + 16 + 32;
		
		if ( reset ) showFullScreen( "dbdata\\flictext");

		if ( count > 0 )
			{
 				positions[ICON_Quit][4] += 4;
			}
		
		HowMany = 6;
		break;
		
  	case 19:							 // Tactics catagories
		positions[6][0] = 125;	  // Infantry
		positions[6][1] = 13;
		positions[6][2] = 113;
		positions[6][3] = 105;	  
		
		positions[7][0] = 126;	  // Cavary
		positions[7][1] = 262;
		positions[7][2] = 113;
		positions[7][3] = 105;
		
		positions[8][0] = 382;	  // Naval
		positions[8][1] = 260;
		positions[8][2] = 113;
		positions[8][3] = 105;
		

		positions[9][0] = 383;	  // Artillary
		positions[9][1] = 13;
		positions[9][2] = 113;
		positions[9][3] = 105;

	
		positions[ICON_Quit][4] = 1 + 2 + 64 + 128 + 256 + 512;

		if ( count > 0 )
			{
				positions[ICON_Quit][4] += 4;
			}


		if ( reset ) showFullScreen( "dbdata\\tcatslct");
						
		HowMany = 10;
		break;

  }

 if ( (positions[ICON_Quit][4] & 1 ) == 0) positions[ICON_Quit][4] += 1;

 positions[ICON_Quit][0] = 512;						 // Quit
 positions[ICON_Quit][1] = 407;
 positions[ICON_Quit][2] = 57;
 positions[ICON_Quit][3] = 52;

// if ( ( positions[ICON_Quit][4] & 2 ) == 2 )
//	{
		 positions[ICON_Main][0] = 420;	 			 // Return to main
		 positions[ICON_Main][1] = 407;
		 positions[ICON_Main][2] = 57;
		 positions[ICON_Main][3] = 52;
//	}

// if ( ( positions[ICON_Quit][4] & 4 ) == 4 )
//	{
	 	 positions[ICON_Memory][0] = 341;	  			 // memory;
		 positions[ICON_Memory][1] = 407;	
		 positions[ICON_Memory][2] = 57;	
		 positions[ICON_Memory][3] = 52;

//	}

 if ( ( ( count == 0 ) && ( HowMany == 6 ) ) && ( ( positions[ICON_Memory][4] & 4 ) == 0 ) )
		{
			Region *tempreg;

			tempreg = new Region ( machine.screen->getImage(), Rect ( positions[ICON_Memory][0], positions[ICON_Memory][1], positions[ICON_Memory][2], positions[ICON_Memory][3] ) ); 
			tempreg->greyOut( Rect( 0, 0, positions[ICON_Memory][2], positions[ICON_Memory][3] ), 16 );
			machine.screen->setUpdate( *tempreg );
			delete tempreg;
			if ( ( positions[ICON_Memory][4] & 4 ) != 0 ) positions[ICON_Memory][4] -= 4;
			// positions[ICON_Memory][4] = 4;
		}

 if ( ( positions[ICON_Quit][4] & 8 ) == 8 )
 	{
  		 positions [ICON_Index][0]	= 234;			 // index
		 positions [ICON_Index][1]	= 407;
		 positions [ICON_Index][2]	= 57;
		 positions [ICON_Index][3]	= 52;

	}

 if ( ( positions[ICON_Quit][4] & 16 ) == 16 )
	{
	  	positions[ICON_Prev][0] = 64;	  			 // Prev. paragraph
		positions[ICON_Prev][1] = 407;
		positions[ICON_Prev][2] = 57;
		positions[ICON_Prev][3] = 52;

	}
			
 if ( ( positions[ICON_Quit][4] & 32 ) == 32 )
	{	 
		positions[ICON_Next][0] = 156;			    // Next paragraph
		positions[ICON_Next][1] = 407;
		positions[ICON_Next][2] = 57;
		positions[ICON_Next][3] = 53;
	}

 if ( HowMany > 6 ) machine.mouse->moveMouse( Point( positions[6][0] + 5, positions[6][1] + 5 ) );

 return HowMany;
}


