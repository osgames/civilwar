/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battle Explosion graphics
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "batexpld.h"
#include "batldata.h"
#include "map3d.h"
#include "batlib.h"

/*
 * Temporary sprites (e.g. used by explosions)
 */

TemporaryBattleSpriteList::TemporaryBattleSpriteList()
{
	entry = 0;
}

TemporaryBattleSpriteList::~TemporaryBattleSpriteList()
{
	while(entry)
	{
		TemporaryBattleSprite* spr = entry;
		entry = spr->next;
		delete spr;
	}
}

void TemporaryBattleSpriteList::process()
{
	TemporaryBattleSprite* spr = entry;
	TemporaryBattleSprite* last = 0;
	while(spr)
	{
		TemporaryBattleSprite* next = spr->next;

		if(spr->process(battle->data3D))
		{
			if(last)
				last->next = spr->next;
			else
				entry = spr->next;

			delete spr;
		}
		else
			last = spr;

		spr = next;
	}
}


TemporaryBattleSprite::TemporaryBattleSprite(const Location& l, Cord3D h, Wangle dir, AnimationTable* tab)
{
	where = l;
	height = h;
	direction = dir;
	table = tab;
	frame = 0;
	element = 0;
	// process(battle->drawData);
}

TemporaryBattleSprite::~TemporaryBattleSprite()
{
	// unDisplay();
	clearUp();
}

Boolean TemporaryBattleSprite::process(System3D& drawData)
{
	if(frame > table->frames)	// Kill if on last frame
		return True;

	if(drawData.view.isOnDisplay(where))
	{
		if(sprite.spriteBlock)
		{
			sprite.spriteBlock->release();
			sprite.spriteBlock = 0;
		}

		Point3D p;

		// p = where;
		p.x = DistToBattle(where.x);
		p.z = DistToBattle(where.y);
		p.y = battle->grid->getHeightView(drawData.view, p.x, p.z) + height;

		drawData.view.transform(p);

		UBYTE octant = facingOctant(direction, drawData.view.yRotate);
		UBYTE sprOffset = table->sprites[drawData.view.zoomLevel][octant];
		if(sprOffset != 0xff)
		{
			SpriteIndex sprNum = sprOffset + table->baseSprite + frame;
			sprite.spriteBlock = battle->spriteLib->read(sprNum);

			Point3D p2 = p;
			p2.x -= sprite.spriteBlock->anchorX;
			p2.y -= sprite.spriteBlock->anchorY;

#ifdef USE_SPRITEID
			sprites = new SpriteID[1];
			battle->spriteList->add(&sprite, p2, sprites);
#else
			if(!element)
				element = battle->spriteList->add(&sprite, p2);
#endif

		}
		else
			clearUp();
	}
	else
		clearUp();

	frame++;

	return False;
}

void TemporaryBattleSprite::clearUp()
{
	// BattleDisplayObject::reset();

	if(element)
	{
		spriteList->remove(element);
		element = 0;
	}

	if(sprite.spriteBlock)
	{
		// sprite.unLinkSprite();
		sprite.spriteBlock->release();
		sprite.spriteBlock = 0;
	}
}


void TemporaryBattleSpriteList::add(const Location& l, Cord3D height, Wangle dir, AnimationTable* tab)
{
#if 0
	Point3D p;

	p.x = DistToBattle(l.x);
	p.z = DistToBattle(l.y);
	p.y = battle->grid->getHeightView(battle->data3D.view, p.x, p.z) + height;
#endif
	TemporaryBattleSprite* spr = new TemporaryBattleSprite(l, height, dir, tab);
	spr->next = entry;
	entry = spr;
}

/*
 * Tables...
 */

AnimationTable cannonSmoke = {
	15, SPR_c_fire_1,
	{
		{   75,   90,  105,    0,   15,   30,   45,   60 },
		{  195,  210,  225,  120,  135,  150,  165,  180 },
		{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff },
		{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff },
		{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff }
	}
};

AnimationTable gunSmoke = {
	7, SPR_gunfire1,
	{
		{   30,   36,   42,    0,    6,   12,   18,   24 },
		{   78,   84,   90,   48,   54,   60,   66,   72 },
		{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff },
		{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff },
		{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff }
	}
};

void BattleData::makeCannonExplosion(const Location& l, Cord3D height, Wangle dir)
{
#ifdef USE_SMOKE
	tempSprites->add(l, height, dir, &cannonSmoke);
#endif
}

void BattleData::makeGunExplosion(const Location& l, Cord3D height, Wangle dir)
{
#ifdef USE_SMOKE
	tempSprites->add(l, height, dir, &gunSmoke);
#endif
}
