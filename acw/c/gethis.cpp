/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#include "textload.h"
#include <conio.h>
#include <fstream.h>


void main()
{
 char filename[] = "c:\\database\\text\\history.txt";

// int texttable[ 100 ];
 fstream tout, tin, txt;
 char joe[] = "Joe Johnston";
 char HowMany = 0;
 char eor = '@';
 long tsize, count;
 char *textptr;
 char *original;
 char dat, a, b, men;
 int var;
 char name[20];

 
 TextLoad textl( filename );

 textl.getdata();

 original = textl.gettexdata();

 tsize = textl.getSize();
 
 tout.open ( "his.ptr", ios::out );

 textptr = original + 2;

 cout << (int)tout.good() << "    " << *(textptr - 1) << "  ";

 count = 0;

 if ( *textptr == 0 ) textptr++;
 while ( ( dat = *textptr++ ) != '\n' )
	{
		if ( dat != 13 ) tout.write ( (unsigned char* ) &dat, sizeof ( char ) );
				 else cout << "b" << (int)*(textptr - 2) << " a" << (int)*textptr << " ";
		if ( dat >= 'A' ) cout << dat;
			else cout << (int)dat;
		count++;
		if ( count > 20 ) { cout << "Error in Lincoln loop\n"; break; }
	}
 
 cout << "  count=" << count << " ";

 men = 1;

 dat = '~';
 tout.write ( (unsigned char* ) &dat, sizeof ( char ) );

 dat = '2';
 tout.write ( (unsigned char* ) &dat, sizeof ( char ) );

 var = 0;
 tout.write( (unsigned char* ) &var, sizeof ( int ) );
 var = 0;
 tout.write( (unsigned char* ) &var, sizeof ( int ) );
 var = 0;
 tout.write( (unsigned char* ) &var, sizeof ( int ) );

 tout.write ( (unsigned char* ) &eor, sizeof ( char ) );
 
 count = 0;

 for ( long loop = 10; loop < tsize; loop++ )
 		{
		 dat = *textptr++;

		 if ( ( dat == '@' )	&& ( *textptr == '@' ) )
		 			{
					  if ( *(textptr + 1) == '@' ) break;
					  textptr += 5;
 					  // if  ( *(textptr - 1 ) >= 'A' ) textptr--;	  
					  var = (int) (textptr - original - 2);
					  if ( *textptr == 0 ) textptr++;

					  while ( ( dat = *textptr++ ) != '\n' )
							{
								// tout.write ( (unsigned char* ) &dat, sizeof ( char ) );
								
								if ( dat != 13 ) name[count++] = dat;
								if ( dat >= 'A' ) cout << dat;
								
								if ( count > 20 ) { cout << "Error name to long!"; break; }
							}
					  
					  name[ count ] = '\0';

					  if ( ( strstr( name, "Johnston" ) != NULL ) && ( *textptr == 'J' ) )
								 	{ 
										tout.write ( joe, 12 );
									}
							else {
							 			tout.write ( name, count );
								  }
					  count = 0;
					  dat = '~';
					  tout.write ( (unsigned char* ) &dat, sizeof ( char ) );
					  
					  dat = '2';   // Screen Format;
					  
					  tout.write ( (unsigned char* ) &dat, sizeof ( char ) );
					  					  	
					  // var = (int) ((textptr - original) + 1 );

					  tout.write( (unsigned char* ) &var, sizeof ( int ) );
					  cout << " : " << var << "  ";

					  var = 0;
					  tout.write( (unsigned char* ) &var, sizeof ( int ) );
					  var = 0;
					  tout.write( (unsigned char* ) &var, sizeof ( int ) );

					  tout.write ( (unsigned char* ) &eor, sizeof ( char ) );

					  men++;
					 }
		 if ( dat == EOF ) break;
		}

 dat = '\0';
 tout.write ( (unsigned char* ) &dat, sizeof ( char ) );

 tout.close();


 tin.open ( "his.ptr", ios::in | ios::binary );

  if ( !tin )
 		{
		 cout << "Error: can't open his.ptr";
		 return;
		}

 count = 0;

 cout << "\n" << (int)'\0' << "\n";

 for ( a = 0; a < men - 30; a++ )
	 {
 	  	 tin.read ( ( unsigned char *) &dat, sizeof ( char ) );
		 cout << (int)a << ":" << (int)dat; // << dat;

		 while ( dat != '~' )
  			  {
					count++;
					if ( count > 20 ) { cout << "Error in count"; return; }
  				  	tin.read ( ( unsigned char *) &dat, sizeof ( char ) );
				  	cout << dat;
					//	else cout << (int)dat << " ";
	  		  }
  		 cout << ":   ";
		 count = 0;
  		 
		 tin.read ( ( unsigned char *) &dat, sizeof ( char ) );

		 cout << " Format: " << dat << "  ";

		 tin.read ( ( unsigned char *) &var, sizeof ( int ) );	 // Text ptr
		
		 cout << var << " :";

  		 if ( var > 80000 ) { cout << "\nError"; return; }
  
		 for ( loop = 0; loop < 10; loop++ )
 				{
					 cout << *(original + var + loop);
				}

		 tin.read ( ( unsigned char *) &var, sizeof ( int ) );	 // Pic ptr
		
		 cout << var << ":    ";
		 
		 tin.read ( ( unsigned char *) &var, sizeof ( int ) );	 // Anim ptr
		
		 tin.read ( ( unsigned char *) &dat, sizeof ( char ) );	 // skip End Of Record

		 
		 // cout << var << ": ";


	 	 cout << "\n";
	 }

 tin.close();

}
