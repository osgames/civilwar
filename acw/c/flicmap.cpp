/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Remap FLIC files to a given palette
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <conio.h>
#include "daglib.h"
#include "flicread.h"
#include "flicwrit.h"
#include "makename.h"
#include "ilbm.h"

class RGBColour {
public:
	UBYTE red;
	UBYTE green;
	UBYTE blue;

	RGBColour(UBYTE* ad)
	{
		red = ad[0];
		green = ad[1];
		blue = ad[2];
	}

	UBYTE closestColour(Palette& p);
	int colourDiff(const RGBColour& col);
};

int cdiff(UBYTE v1, UBYTE v2)
{
	if(v1 > v2)
		return v1 - v2;
	else
		return v2 - v1;
}

/*
 * Perceptual colour difference
 */

int RGBColour::colourDiff(const RGBColour& col)
{
	int rDiff = (cdiff(red, col.red) * 19) / 20;
	int bDiff = cdiff(blue, col.blue) / 10;
	int gDiff = cdiff(green, col.green);

	return rDiff * rDiff + bDiff * bDiff + gDiff * gDiff;
}

/*
 * Find closest colour in a palette
 */

UBYTE RGBColour::closestColour(Palette& pal)
{
	UBYTE best = 0;
	int bestDiff = colourDiff(pal.getColour(0));

	for(int i = 1; i < 256; i++)
	{
		RGBColour col = pal.getColour(i);

		int diff = colourDiff(col);

		if(diff < bestDiff)
		{
			bestDiff = diff;
			best = UBYTE(i);
		}
	}

	if(bestDiff > 300)
		cout << "Warning: Colour " << int(red) << "," << int(green) << "," << int(blue) <<
			" matched badly with a difference of "  << bestDiff << endl;

	return best;
}


void remapImage(Image& srcImage, Palette& srcPalette, Image& destImage, Palette& destPalette)
{
	destImage.resize(srcImage.getSize());

	memcpy(destImage.getAddress(), srcImage.getAddress(), srcImage.getWidth() * srcImage.getHeight());

	/*
	 * Histogram
	 */

	int used[256];
	memset(used, 0, sizeof(used));
	
	UBYTE* ad = srcImage.getAddress();
	int pixelCount = srcImage.getWidth() * srcImage.getHeight();
	while(pixelCount--)
		used[*ad++]++;

	/*
	 * Make a remap table
	 */

	UBYTE remap[256];

	for(int i = 0; i < 256; i++)
	{
		if(used[i])
		{
			RGBColour col = srcPalette.getColour(i);
			UBYTE n = col.closestColour(destPalette);

			remap[i] = n;

#ifdef DEBUG1
			if(i != n)
				cout << "Remap " << int(i) << " to " << int(n) << endl;
#endif
		}
	}

	/*
	 * Go through a pixel at a time and remap it
	 */


	ad = srcImage.getAddress();		// Address of 1st pixel
	UBYTE* destAd = destImage.getAddress();

	pixelCount = srcImage.getWidth() * srcImage.getHeight();
	while(pixelCount--)
		*destAd++ = remap[*ad++];
}

void usage()
{
	cout << "Usage\n" <<
			  "\tflicmap palette.lbm flicfile.flc\n" << endl;
}


void main(int argc, char* argv[])
{
	try
	{

		Palette palette;

		char inName[120];
		char outName[120];
		char bakName[120];

		if(argc != 3)
		{
			usage();
			return;
		}

		int error = 0;

		makeFilename(inName, argv[2], ".FLC", False);
		makeFilename(bakName, argv[2], ".BAK", True);
		makeFilename(outName, argv[2], ".TMP", True);

		if(readILBM(argv[1], 0, &palette) != 0)
		{
			cout << "Error reading palette from " << argv[1] << endl;
			return;
		}

		if(!error)
		{
			cout << "Processing " << outName;

			FlicRead inFli(inName);

			if(inFli.getError())
			{
				cout << "Error in FLIC file " << inName << endl;
				error = inFli.getError();
			}
			else
			{
				FlicWrite outFli(outName);
				if(outFli.getError())
					cout << "\nCouldn't create output FLIC file " << outName << endl;
				else
				{
					/*
					 * Set up some things in header
					 */

					outFli.setSpeed(inFli.getHeader()->speed);


					Image workImage;

					while(!inFli.finished)
					{
						cout << ".";

						inFli.nextFrame();


						/*
			 	 	 	 * Now remap it
			 	 	 	 */

						remapImage(inFli.image, inFli.palette, workImage, palette);
		
						outFli.addFrame(workImage, palette);
					}

					/*
					 * Do final frame (goes back to 2nd frame again)
					 */

					cout << ".";
					inFli.nextFrame();
					remapImage(inFli.image, inFli.palette, workImage, palette);
					outFli.addLastFrame(workImage, palette);

					cout << endl;
				}

				error = outFli.getError();
			}


			/*
		 	 * If there was an error restore previous file names
		 	 */

			if(error)
			{
				cout << "There was some kind of error" << endl;
				remove(outName);
			}
			else
			{
				remove(bakName);
				rename(inName, bakName);
				rename(outName, inName);
				cout << "Remapping complete.  " << inName << " has been backed up as " << bakName << endl;
			}
		}
		else
		{
			cout << "Unable to rename " << outName << " to " << inName << endl;
		}

	}
	catch( GeneralError e )
	{
		cout << "Error:";
		if ( e.get() )
		 	cout << e.get();
		else
			cout << " ??";
		cout << endl;
		getch();
	}
	catch( ...)
	{
		cout << "Unknown error!\n";
		getch();
	}
}
