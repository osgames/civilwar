/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Shape List Implementation
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.7  1994/06/02  15:27:46  Steven_Green
 * Graphics and screen management changed (see screen.h and image.h)
 *
 * Revision 1.6  1994/04/05  12:28:09  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1993/12/23  09:26:01  Steven_Green
 * Mods for 3D Sprites
 *
 * Revision 1.4  1993/12/01  15:11:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1993/11/30  02:57:11  Steven_Green
 * Some attempted optimisation
 *
 * Revision 1.2  1993/11/24  09:32:52  Steven_Green
 * Use of Shape base object and virtual render function
 *
 * Revision 1.1  1993/11/19  18:59:26  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


#include "shplist.h"
#include "image.h"

/*
 * Add element to shapelist
 */
 
void ShapeListBase::add(Shape* shape, Cord3D dist)
{
	// ShapeList* s = new ShapeList;
	ShapeList* s = allocShapeList.findNew();

	s->shape = shape;
	s->distance = dist;

	/*
	 * Insert into linked list
	 */

	ShapeList* list = entry;
	ShapeList* prev = 0;

	while(list)
	{
		if(list->distance <= dist)
			break;

		prev = list;
		list = list->next;
	}

	s->next = list;

	if(prev)
		prev->next = s;
	else
		entry = s;
}

ShapeListBase::~ShapeListBase()
{
#if 0		// All shapes are automatically destroyed when shapelistbase is destructed!
	ShapeList* list = entry;

	while(list)
	{
		ShapeList* next = list->next;

		delete list;
		list = next;
	}
#endif
}

/*
 * Draw the shapes
 */

void ShapeListBase::draw(Region* bm, ObjectDrawData* data)
{
	ShapeList* list = entry;

	while(list)
	{
		list->shape->render(bm, data);
		list = list->next;
	}
}



