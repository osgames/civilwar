/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Editor - Facility Editting
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>

#include "camped.h"
#include "campwld.h"
#include "mapwind.h"
#include "system.h"
#include "screen.h"
#include "text.h"
#include "colours.h"
#include "mouselib.h"
#include "camptab.h"
#include "memptr.h"
#include "tables.h"

/*====================================================================
 * facility Mode
 */

static char* cityTypes[] = {
	"Industrial",
	"Agricultural",
	"Mixed",
	"Other"
};

static MenuChoice railChoices[] = {
	MenuChoice(0, 						0 			),						// User fills in this
	MenuChoice("-", 					0 			),
	MenuChoice("Set Links",			DoLinks	),
	MenuChoice("Set Capacity",		DoCapacity),
	MenuChoice("-", 					0 			),
	MenuChoice("New Facility", 	DoNew 	),
	MenuChoice("Delete Facility",	DoDelete ),
	MenuChoice("Move Facility",	DoMove 	),
	MenuChoice("Edit Facility", 	DoEdit 	),
	MenuChoice("-", 					0 			),
	MenuChoice("Cancel", 			-1 		),
	MenuChoice(0,						0 			)
};

static MenuChoice supplyChoices[] = {
	MenuChoice(0, 						0 			),						// User fills in this
	MenuChoice("-", 					0 			),
	MenuChoice("Set Owner",			DoOwner	),
	MenuChoice("Make Key City",	DoMakeKey),
	MenuChoice("-", 					0 			),
	MenuChoice("New Facility", 	DoNew 	),
	MenuChoice("Delete Facility",	DoDelete ),
	MenuChoice("Move Facility",	DoMove 	),
	MenuChoice("Edit Facility", 	DoEdit 	),
	MenuChoice("-", 					0 			),
	MenuChoice("Cancel", 			-1 		),
	MenuChoice(0,						0 			)
};


void Facility::showInfo(Region* r)
{
	control->setupInfo();

	TextWindow& textWind = *control->textWin;

	textWind.setFont(Font_Info);
	textWind.setColours(Black);
	textWind.setHCentre(True);
	textWind.setPosition(Point(4,4));
	
	/*
	 * What is it?
	 * City/Key City/Named Facility/Facility
	 */

	if(facilities & F_KeyCity)
		textWind.draw("Key City");
	else if(facilities & F_City)
		textWind.draw("City");
	else
		textWind.draw("Facility");

	textWind.newLine();
	
	textWind.setFont(Font_Info);

	/*
	 * What is it called?
	 */

	textWind.draw(getName());
	textWind.newLine();

	textWind.moveDown(4);
	textWind.setFont(Font_Heading);

	/*
	 * Display what facilities it has got
	 */

	if(facilities & F_Port)
	{
		textWind.draw("Port");
		textWind.newLine();
	}

	if(facilities & F_LandingStage)
	{
		textWind.draw("Landing Stage");
		textWind.newLine();
	}

	if(facilities & F_SupplyDepot)
	{
		textWind.draw("Supply Depot");
		textWind.newLine();
	}
	
	if(facilities & F_RecruitmentCentre)
	{
		textWind.draw("Recruitment Centre");
		textWind.newLine();
	}

	if(facilities & F_TrainingCamp)
	{
		textWind.draw("Training Camp");
		textWind.newLine();
	}

	if(facilities & F_Hospital)
	{
		textWind.draw("Hospital");
		textWind.newLine();
	}

	if(facilities & F_POW)
	{
		textWind.draw("POW camp");
		textWind.newLine();
	}

	/*
	 * Other info
	 */

	textWind.moveDown(4);

	textWind.setHCentre(False);
	textWind.setLeftMargin(4);

	textWind.setFont(Font_Heading);
	textWind.draw("State:");
	// textWind.setFont(Font_Info);
	textWind.draw(campaign->world->states[state].fullName);
	textWind.newLine();

	textWind.setFont(Font_Heading);
	textWind.draw("Side:");
	// textWind.setFont(Font_Info);
	textWind.draw(sideStr[side]);
	textWind.newLine();

	textWind.setFont(Font_Heading);
	textWind.draw("Fortifications:");
	// textWind.setFont(Font_Info);
	textWind.wprintf("%d", (int)fortification);
	textWind.newLine();


	textWind.setFont(Font_Heading);
	textWind.draw("Victory:");
	// textWind.setFont(Font_Info);
	textWind.wprintf("%d", (int)victory);
	textWind.newLine();

	if(owner != NoFacility)
	{
		Facility* f = campaign->world->facilities[owner];
		textWind.wprintf("\rSupplied from\r%s\r\r", f->getName());
	}

	if(connections.entries())
	{
		textWind.wprintf("Rail Capacity: %d\r", (int)railCapacity);
		textWind.wprintf("Rail connects: %d\r", (int)connections.entries());

		UBYTE jCount = 0;
		while(jCount < connections.entries())
		{
			FacilityID con = connections[jCount++];
			if(con != NoFacility)
			{
				Facility* f = campaign->world->facilities[con];
				textWind.wprintf("-> %s\r", f->getName());
			}
		}
	}

	if(waterZone != NoWaterZone)
	{
		textWind.wprintf("Water Zone: %d\r", (int)waterZone);
		textWind.wprintf("Water Cap: %d\r",(int)waterCapacity);
		textWind.wprintf("Blockades: %d\r", (int)blockadeRunners);
	}
}

void City::showInfo(Region* r)
{
	Facility::showInfo(r);

	TextWindow& textWind = *control->textWin;

	/*
	 * Continue where others left off
	 */

	// Resources
	// Type
	// Size
	// Regiment count

	textWind.wprintf("Type: %s\r", cityTypes[type]);
	textWind.wprintf("Size: %d\r", (int)size);

	// textWind.wprintf("Regiment %d\r", (int)regimentCount);
	textWind.wprintf("Human: %d\r", (int)resource.values[R_Human]);
	textWind.wprintf("Horses: %d\r", (int)resource.values[R_Horses]);
	textWind.wprintf("Food: %d\r", (int)resource.values[R_Food]);
	textWind.wprintf("Materials: %d\r", (int)resource.values[R_Materials]);

}

void getCapacity(Facility* facility)
{
	char numBuf[3];
	char prompt[200];

	sprintf(prompt, "Enter new rail capacity for\r%s\r(0..99)", facility->getName());
	sprintf(numBuf, "%d", facility->railCapacity);

	if(dialInput(0, machine.mouse->getPosition(), prompt, "OK|Cancel", numBuf, sizeof(numBuf)) == 0)
		facility->railCapacity = atoi(numBuf);
}

const char* getStateName(StateID id)
{
	State& state = campaign->world->states[id];
	return state.fullName;
}

/*
 * Remove a particular rail junction
 */

void removeJunction(Facility* f, FacilityID j)
{
	int i = 0;
	while(i < f->connections.entries())
	{
		if(f->connections[i] == j)
		{
			f->connections.removeID(i);
			return;
		}
		i++;
	}
}

void removeRailConnection(FacilityID j1, FacilityID j2)
{
	removeJunction(campaign->world->facilities[j1], j2);
	removeJunction(campaign->world->facilities[j2], j1);
}

/*
 * Delete a facility and patch up data
 */

void CampaignWorld::deleteFacility(Facility* f)
{
	FacilityID id = facilities.getID(f);
	if(id != NoFacility)
	{
		/*
		 * Remove rail connections
		 */

		while(f->connections.entries())
			removeRailConnection(id, f->connections[0]);

		/*
		 * Really delete it
		 */

		campaign->mapArea->onScreenMoveable.clear();
		campaign->mapArea->onScreenStatic.clear();
		facilities.removeID(id);

		/*
		 * Go through and alter:
		 *		key cities
		 *		rail links
		 *		water network
		 */

		for(FacilityID i1 = 0; i1 < facilities.entries(); i1++)
		{
			Facility* f1 = facilities[i1];

			if(f1->owner != NoFacility)
			{
				if(f1->owner == id)
					control->setCloseOwner(f1, True);
				else if(f1->owner > id)
					f1->owner--;
			}

			int jCount = 0;
			while(jCount < f1->connections.entries())
			{
				if(f1->connections[jCount] == id)
				{
					// Shouldn't happen!
					;
				}
				else if(f1->connections[jCount] > id)
				{
					f1->connections[jCount]--;
				}
				jCount++;
			}
		}


		facilityChanged = True;
		changed = True;
	}

	/*
	 * Adjust Water network
	 * RiverZone and CoastZone have facilityIds
	 *
	 * Waterzones will have to be stored differently for editor
	 * to enable items to be added/removed easily.
	 */

	for(int i = 0; i < waterNet.zones.entries(); i++)
	{
		WaterZone& z = waterNet.zones[i];

		for(int j = 0; j < z.facilityList.entries(); j++)
		{
			if(z.facilityList[j] == id)	// Remove it
			{
				z.facilityList.removeID(j);
				j--;
				waterChanged = True;
				changed = True;
			}
			else if(z.facilityList[j] > id)	// Adjust it
			{
				z.facilityList[j]--;
				waterChanged = True;
				changed = True;
			}
		}
	}
}

Facility* CampaignWorld::makeNewFacility()
{
	Facility** fPtr = facilities.add();
	City* f = new City();
	*fPtr = f;

	strcpy(f->name, "New Facility");

	return f;
}

/*
 * City Editting dialogue
 */

enum edFacilityId {
	EF_None,
	EF_Cancel,
	EF_OK,
	EF_Type,
	EF_State,
	EF_Side,
	EF_Victory,
	EF_Recruitment,
	EF_Training,
	EF_Hospital,
	EF_POW,
	EF_SupplyDepot,
	EF_Supply,
	EF_Fort,
	EF_CityType,
	EF_CitySize,
	// EF_RegCount,
	EF_Human,
	EF_Horses,
	EF_Food,
	EF_Materials,
	EF_WaterCap,

	EF_City,
};

static char* yesStr = "Yes";
static char* noStr = "No";


char f_typeStr[40];
char f_sideStr[40];
char f_victoryStr[4];
char f_stateStr[MaxStateName + 10];
char f_supplyStr[4];
char f_fortStr[2];
// char f_regStr[4];
char f_sizeStr[3];
char f_humanStr[6];
char f_horseStr[6];
char f_foodStr[6];
char f_materialStr[6];
char f_waterCapStr[4];

enum edFacEnum {
	EFE_Type,
	EFE_Name,
	EFE_State,
	EFE_Side,
	EFE_RecruitStr,
	EFE_RecruitFlag,
	EFE_TrainingStr,
	EFE_TrainingFlag,
	EFE_HospitalStr,
	EFE_HospitalFlag,
	EFE_POWString,
	EFE_POWFlag,
	EFE_DepotStr,
	EFE_DepotFlag,
	EFE_SupplyStr,
	EFE_SupplyNum,
	EFE_FortStr,
	EFE_FortNum,
	EFE_VictoryStr,
	EFE_VictoryNum,
	EFE_CityTypeStr,
	EFE_CityTypeFlag,
	EFE_CitySizeStr,
	EFE_CitySizeNum,
	// EFE_RegCountStr,
	// EFE_RegCountNum,
	EFE_ResourceTitle,
	EFE_HumanStr,
	EFE_HumanNum,
	EFE_HorsesStr,
	EFE_HorsesNum,
	EFE_FoodStr,
	EFE_FoodNum,
	EFE_MaterialsStr,
	EFE_MaterialsNum,
	EFE_WaterCapStr,
	EFE_WaterCapNum,
	EFE_Cancel,
	EFE_OK
};

DialItem edFacilityItems[] = {
	DialItem(Dial_Button,   	EF_Type, 		Rect(  0,  0,256,16), f_typeStr),
	DialItem(Dial_TextInput,   EF_None,			Rect(  0, 16,256,16), 0, 0),
	DialItem(Dial_Button,   	EF_State, 		Rect(  0, 34,256,28), f_stateStr),
	DialItem(Dial_Button,   	EF_Side, 		Rect(  0, 64,256,14), f_sideStr),

	DialItem(Dial_Button, 		EF_Recruitment,Rect(  0, 80,100,16), "Recruitment"),
	DialItem(Dial_Button, 		EF_Recruitment,Rect(100, 80, 24,16), yesStr),
	DialItem(Dial_Button, 		EF_Training, 	Rect(132, 80,100,16), "Training Centre"),
	DialItem(Dial_Button, 		EF_Training, 	Rect(232, 80, 24,16), yesStr),

	DialItem(Dial_Button, 		EF_Hospital, 	Rect(  0, 96,100,16), "Hospital"),
	DialItem(Dial_Button, 		EF_Hospital, 	Rect(100, 96, 24,16), yesStr),
	DialItem(Dial_Button, 		EF_POW, 			Rect(132, 96,100,16), "POW Camp"),
	DialItem(Dial_Button, 		EF_POW, 			Rect(232, 96, 24,16), yesStr),

	DialItem(Dial_Button, 		EF_SupplyDepot,Rect(  0,112,100,16), "Supply Depot"),
	DialItem(Dial_Button, 		EF_SupplyDepot,Rect(100,112, 24,16), yesStr),
	DialItem(Dial_Button,		EF_None,			Rect(  0,128,100,16), "Supplies"),
	DialItem(Dial_NumberInput,	EF_Supply,		Rect(100,128, 24,16), f_supplyStr, sizeof(f_supplyStr)),

	DialItem(Dial_Button,  		EF_None,			Rect(132,112,100,16), "Fort (0..4)"),
	DialItem(Dial_NumberInput, EF_Fort,			Rect(232,112, 24,16), f_fortStr, sizeof(f_fortStr)),
	DialItem(Dial_Button,  		EF_None,			Rect(132,128,100,16), "Victory"),
	DialItem(Dial_NumberInput, EF_Victory,		Rect(232,128, 24,16), f_victoryStr, sizeof(f_victoryStr)),

	DialItem(Dial_Button, 		EF_CityType,	Rect(  0,146,100,16), "City Type"),
	DialItem(Dial_Button, 		EF_CityType,	Rect(100,146,156,16), 0),
	DialItem(Dial_Button,		EF_None,			Rect(  0,162,100,16), "Size (0..63)"),
	DialItem(Dial_NumberInput,	EF_CitySize,	Rect(100,162, 24,16), f_sizeStr, sizeof(f_sizeStr)),

	// DialItem(Dial_Button,  		EF_None,			Rect(132,162,100,16), "Regiment"),
	// DialItem(Dial_NumberInput, EF_RegCount,	Rect(232,162, 24,16), f_regStr, sizeof(f_regStr)),

	DialItem(Dial_Button,		EF_None,			Rect(  0,180,256,12), "Resources (0..16363)"),
	DialItem(Dial_Button,		EF_None,			Rect(  0,192, 92,16), "Human"),
	DialItem(Dial_NumberInput,	EF_Human,		Rect( 92,192, 32,16), f_humanStr, sizeof(f_humanStr)),
	DialItem(Dial_Button,		EF_None,			Rect(132,192, 92,16), "Horses"),
	DialItem(Dial_NumberInput,	EF_Horses,		Rect(224,192, 32,16), f_horseStr, sizeof(f_horseStr)),
	DialItem(Dial_Button,		EF_None,			Rect(  0,208, 92,16), "Food"),
	DialItem(Dial_NumberInput,	EF_Food,			Rect( 92,208, 32,16), f_foodStr, sizeof(f_foodStr)),
	DialItem(Dial_Button,		EF_None,			Rect(132,208, 92,16), "Materials"),
	DialItem(Dial_NumberInput,	EF_Materials,	Rect(224,208, 32,16), f_materialStr, sizeof(f_materialStr)),

	DialItem(Dial_Button,		EF_None,			Rect(132,162,100,16), "Water Capacity"),
	DialItem(Dial_NumberInput,	EF_WaterCap,	Rect(232,162, 24,16), f_waterCapStr, sizeof(f_waterCapStr)),

	DialItem(Dial_Button, 		EF_Cancel,  	Rect( 32,228, 64,16), "Cancel"),
	DialItem(Dial_Button, 		EF_OK,  			Rect(160,228, 64,16), "OK"),
	DialItem(Dial_End)
};

class FacilityEdit : public Dialogue {
	City* city;
public:
	FacilityEdit(City* c);

	void doIcon(DialItem* item, Event* event, MenuData* d);
	void updateButtons();
	void enableSupply(Boolean which)
	{
		edFacilityItems[EFE_SupplyStr].enabled = which;
		edFacilityItems[EFE_SupplyNum].enabled = which;
	}

	void enableCity(Boolean which)
	{
		for(int i = EFE_RecruitStr; i <= EFE_POWFlag; i++)
			edFacilityItems[i].enabled = which;

		for(i = EFE_CityTypeStr; i <= EFE_MaterialsNum; i++)
			edFacilityItems[i].enabled = which;
	}
};

void FacilityEdit::updateButtons()
{
	if(city->facilities & Facility::F_City)
	{
		strcpy(f_typeStr, "Type: City");
		enableCity(True);
	}
	else
	{
		strcpy(f_typeStr, "Type: Facility");
		enableCity(False);
	}

	edFacilityItems[EFE_Name].text = city->name;
	edFacilityItems[EFE_Name].bufferSize = MaxFacilityName;

	sprintf(f_stateStr, "State:\r%s", 
		(city->state == NoState) ?
		"None" : campaign->world->states[city->state].fullName);

	sprintf(f_sideStr, "Side: %s", sideStr[city->side]);

	sprintf(f_victoryStr, "%d", int(city->getVictory()));
	sprintf(f_supplyStr, "%d", int(city->supplies));
	sprintf(f_fortStr, "%d", int(city->fortification));
	// sprintf(f_regStr, "%d", int(city->regimentCount));
	sprintf(f_sizeStr, "%d", int(city->size));

	sprintf(f_humanStr, "%d", int(city->resource.values[R_Human]));
	sprintf(f_horseStr, "%d", int(city->resource.values[R_Horses]));
	sprintf(f_foodStr, "%d", int(city->resource.values[R_Food]));
	sprintf(f_materialStr, "%d", int(city->resource.values[R_Materials]));

	edFacilityItems[EFE_CityTypeFlag].text = cityTypes[city->type];

	if(city->facilities & Facility::F_RecruitmentCentre)
		edFacilityItems[EFE_RecruitFlag].text = yesStr;
	else
		edFacilityItems[EFE_RecruitFlag].text = noStr;

	if(city->facilities & Facility::F_TrainingCamp)
		edFacilityItems[EFE_TrainingFlag].text = yesStr;
	else
		edFacilityItems[EFE_TrainingFlag].text = noStr;

	if(city->facilities & Facility::F_Hospital)
		edFacilityItems[EFE_HospitalFlag].text = yesStr;
	else
		edFacilityItems[EFE_HospitalFlag].text = noStr;

	if(city->facilities & Facility::F_POW)
		edFacilityItems[EFE_POWFlag].text = yesStr;
	else
		edFacilityItems[EFE_POWFlag].text = noStr;

	if(city->facilities & Facility::F_SupplyDepot)
	{
		enableSupply(True);
		edFacilityItems[EFE_DepotFlag].text = yesStr;
	}
	else
	{
		enableSupply(False);
		edFacilityItems[EFE_DepotFlag].text = noStr;
	}

	if(city->waterZone != NoWaterZone)
	{
		edFacilityItems[EFE_WaterCapNum].enabled = True;
		edFacilityItems[EFE_WaterCapStr].enabled = True;
		sprintf(f_waterCapStr, "%d", city->waterCapacity);
	}
	else
	{
		edFacilityItems[EFE_WaterCapNum].enabled = False;
		edFacilityItems[EFE_WaterCapStr].enabled = False;
	}
}

FacilityEdit::FacilityEdit(City* c)
{
	city = c;

	/*
	 * Set up values in table
	 */

	updateButtons();

	Dialogue::setup(edFacilityItems);
}

void FacilityEdit::doIcon(DialItem* item, Event* event, MenuData* d)
{
	if(event->buttons)
	{
		switch(item->id)
		{
		case EF_Cancel:
			d->setFinish(EF_Cancel);
			break;
		case EF_OK:
			d->setFinish(EF_OK);
			break;
		case EF_Type:
			city->facilities ^= Facility::F_City;
			updateButtons();
			d->setRedraw();
			break;
		case EF_State:
			d->setFinish(EF_State);
			break;
		case EF_Side:
			city->side = inputSide(city->side);
			updateButtons();
			d->setRedraw();
			break;
		case EF_Recruitment:
			city->facilities ^= Facility::F_RecruitmentCentre;
			updateButtons();
			d->setRedraw();
			break;
		case EF_Training:
			city->facilities ^= Facility::F_TrainingCamp;
			updateButtons();
			d->setRedraw();
			break;
		case EF_Hospital:
			city->facilities ^= Facility::F_Hospital;
			updateButtons();
			d->setRedraw();
			break;
		case EF_POW:
			city->facilities ^= Facility::F_POW;
			updateButtons();
			d->setRedraw();
			break;
		case EF_SupplyDepot:
			city->facilities ^= Facility::F_SupplyDepot;
			updateButtons();
			d->setRedraw();
			break;
		case EF_CityType:
			city->type = (city->type + 1) & 3;
			updateButtons();
			d->setRedraw();
			break;
		}
	}

	if(item->id == EF_Victory)
		city->setVictory(atoi(item->text));

	if(item->id == EF_Supply)
		city->supplies = atoi(item->text);

	if(item->id == EF_Fort)
		city->fortification = atoi(item->text);

#if 0
	if(item->id == EF_RegCount)
		city->regimentCount = atoi(item->text);
#endif

	if(item->id == EF_CitySize)
		city->size = atoi(item->text);

	if(item->id == EF_Human)
		city->resource.values[R_Human] = atoi(item->text);
	if(item->id == EF_Horses)
		city->resource.values[R_Horses] = atoi(item->text);
	if(item->id == EF_Food)
		city->resource.values[R_Food] = atoi(item->text);
	if(item->id == EF_Materials)
		city->resource.values[R_Materials] = atoi(item->text);
	if(item->id == EF_WaterCap)
		city->waterCapacity = atoi(item->text);
}

void CampaignEditControl::editFacility(Facility* oldFacility)
{
	if(oldFacility)
	{
		edittingFacility = oldFacility;

		if(edittingFacility->facilities & Facility::F_City)
		{
			City* city = (City*) edittingFacility;
			edittedFacility = *city;
		}
		else
		{
			Facility* f = &edittedFacility;
			*f = *edittingFacility;
		}
	}

	/*
	 * User dialogue to edit facility!
	 */

	int result;

	{
		FacilityEdit edit(&edittedFacility);
		result = edit.process();
		// destructor called here
	}

	if(result == EF_OK)		// User didn't cancel
	{
		Facility** fPtr;

		FacilityID id = world->facilities.getID(edittingFacility);
		fPtr = &world->facilities[id];

		if(edittedFacility.facilities & Facility::F_City)
		{
			City* newCity = new City();
			*newCity = edittedFacility;
			*fPtr = newCity;
		}
		else
		{
			Facility* newCity = new Facility();
			*newCity = edittedFacility;
			*fPtr = newCity;
		}

		delete edittingFacility;
		edittingFacility = 0;

		facilityChanged = True;
		changed = True;
		edittingFacility = 0;
	}
	else if(result == EF_State)
	{
		/*
		 * Set up for state tracking
		 */

		loseObject();
		trackMode = GetState;
		showFacilities = False;
		hint("Select State for %s", edittedFacility.name);
	}
	else
		edittingFacility = 0;
}

void CampaignEditControl::facilityOverMap	(MapWindow* map, Event* event, MenuData* d, const Location* l)
{
	if(trackMode == Tracking)
	{
		trackFor(map, event, OT_Facility);

		if(event->buttons)
		{
			DoWhat what;
			Facility* facility = 0;

			if(currentObject)
			{
				facility = (Facility*) currentObject;

				if(event->buttons & Mouse::LeftButton)
				{
					MenuChoice* choices;

					if(mode == EDIT_Facilities)
						choices = whatChoices;
					else if(mode == EDIT_Railway)
						choices = railChoices;
					else if(mode == EDIT_Supply)
						choices = supplyChoices;
					else	// shouldn't happen!
						choices = whatChoices;


					choices[0].text = facility->getName();

					what = DoWhat(menuSelect(0, choices, machine.mouse->getPosition()));
				}
				else
					what = DoEdit;
			}
			else
				what = DoNew;

			if(what == DoNew)
			{
				trackMode = Moving;
				loseObject();
				objectIsNew = True;
				hint("Click location for new Facility");
			}

			/*
		 	 * edit the data
		 	 */

			if(what == DoEdit)
			{
				loseObject();
				editFacility(facility);
			}
			else if(what == DoMove)
				trackMode = Moving;
			else if(what == DoDelete)
			{
				loseObject();
				world->deleteFacility(facility);
			}
			else if(what == DoCapacity)
			{
				getCapacity(facility);
			}
			else if(what == DoLinks)
			{
				hint("Left click links, Right to end");

				trackMode = GetLink;
				edittingFacility = facility;
				showLink = True;
			}
			else if(what == DoOwner)
			{
				hint("Select owner of %s", facility->getName());

				trackMode = GetOwner;
				edittingFacility = facility;
				showLink = True;
			}
			else if(what == DoMakeKey)
			{
				if(facility->facilities & Facility::F_City)
				{
					facilityChanged = True;
					changed = True;

					facility->facilities ^= Facility::F_KeyCity;

					/*
					 * Automatically connect or break
					 */

					if(facility->facilities & Facility::F_KeyCity)
					{
						facility->owner = NoFacility;

						if(dialAlert(0, "Automatically join close cities?", "Yes|No") == 0)
						{
							for(int i = 0; i < world->facilities.entries(); i++)
							{
								Facility* f = world->facilities[i];

								if( (f->facilities & Facility::F_City) && !(f->facilities & Facility::F_KeyCity) )
								{
									Facility* best = 0;
									Distance close = 0;

									for(int j = 0; j < world->facilities.entries(); j++)
									{
										Facility* f1 = world->facilities[j];

										if( (f1->facilities & Facility::F_KeyCity) &&
											 (f1->side == f->side))
										{
											Distance d = distance(f->location.x - f1->location.x,
																		 f->location.y - f1->location.y);

											if(!best || (d < close))
											{
												best = f1;
												close = d;
											}
										}
									}

									if(best && (best == facility))
										f->owner = world->facilities.getID(facility);
								}
							}
						}
					}
					else
					{
						FacilityID id = world->facilities.getID(facility);

						for(int i = 0; i < world->facilities.entries(); i++)
						{
							Facility* f = world->facilities[i];
							if((f->facilities & Facility::F_City) && (f->owner == id))
								f->owner = NoFacility;
						}
					}
				}

			}
		}
	}
	else if(trackMode == Moving)
	{
		if(event->buttons)
		{
			hint(0);


		 	trackMode = Tracking;

			if(event->buttons & Mouse::LeftButton)
			{
				Facility* facility;

				if(objectIsNew)
					facility = world->makeNewFacility();
				else
		 			facility = (Facility*)currentObject;

				facility->location = *l;
				facilityChanged = True;
				changed = True;

				if(objectIsNew)
				{
					loseObject();
					control->setCloseOwner(facility, True);
					control->setCloseState(facility, False);
					editFacility(facility);
				}
			}

		 	objectIsNew = False;
		}
	}
	else if(trackMode == GetState)
	{
		trackFor(map, event, OT_State);

		if(event->buttons)
		{
			if(currentObject && (currentObject->obType == OT_State))
			{
				State* state = (State*) currentObject;

				edittedFacility.state = world->states.getID(state);
				edittedFacility.side = state->side;
			}

			loseObject();
			hint(0);
			trackMode = Tracking;
			showFacilities = True;
			editFacility(0);
		}
	}
	else if(trackMode == GetLink)
	{
		edittingFacility->highlight = 1;

		trackFor(map, event, OT_Facility);

		if(event->buttons & Mouse::RightButton)
		{
			hint(0);
			trackMode = Tracking;
			edittingFacility->highlight = 0;
			edittingFacility = 0;
			loseObject();
			showLink = False;
		}
		else if(event->buttons & Mouse::LeftButton)
		{
			if(currentObject && (currentObject != edittingFacility))
			{
				/*
				 * find out if a link already exists
				 */

				Facility* f = (Facility*)currentObject;
				FacilityID id = world->facilities.getID(f);
				FacilityID id1 = world->facilities.getID(edittingFacility);

				Boolean gotConnection = False;
				for(int i = 0; !gotConnection && (i < edittingFacility->connections.entries()); i++)
				{
					if(edittingFacility->connections[i] == id)
						gotConnection = True;
				}

				if(gotConnection)
				{
					/*
					 * Break connection
					 */

					removeRailConnection(id, id1);
				}
				else
				{
					/*
					 * Make new connection
					 */

					edittingFacility->connections.add(id);

					f->connections.add(id1);

					/*
					 * Make the new connection current
					 */

					edittingFacility->highlight = 0;
					edittingFacility = f;
				}

				facilityChanged = True;
				changed = True;
			}
		}
	}
	else if(trackMode == GetOwner)
	{
		edittingFacility->highlight = 1;

		trackFor(map, event, OT_Facility);

		if(event->buttons)
		{
			if(event->buttons & Mouse::LeftButton)
			{
				if(currentObject)
				{
					/*
					 * find out if a link already exists
					 */

					Facility* f = (Facility*)currentObject;
					FacilityID id = world->facilities.getID(f);
					FacilityID id1 = world->facilities.getID(edittingFacility);

					/*
					 * Selecting self, removes owner
					 * Selecting existing link breaks it
					 * Selecting new link replaces or sets it
					 */

					if(edittingFacility == currentObject)
						edittingFacility->owner = NoFacility;
					else if(edittingFacility->owner == id)
						edittingFacility->owner = NoFacility;
					else
						edittingFacility->owner = id;
			
					facilityChanged = True;
					changed = True;
				}
			}

			hint(0);
			trackMode = Tracking;
			edittingFacility->highlight = 0;
			edittingFacility = 0;
			loseObject();
			showLink = False;
		}
	}
}

void CampaignEditControl::facilityOffMap	()
{
}

void FacilityList::zeroResources(Boolean all)
{
	for(int i = 0; i < entries(); i++)
	{
		Facility* f = get(i);

		if(f->facilities & Facility::F_City)
		{
			City* city = (City*) f;

			if(all || 
				( (city->resource.values[R_Human] == MaxResource) &&
				  (city->resource.values[R_Horses] == MaxResource) &&
				  (city->resource.values[R_Food] == MaxResource) &&
				  (city->resource.values[R_Materials] == MaxResource)
				)
			  )
			{
				city->resource.values[R_Human] = 0;
				city->resource.values[R_Horses] = 0;
				city->resource.values[R_Food] = 0;
				city->resource.values[R_Materials] = 0;
			}
		}
	}
}

void FacilityList::setResources(int days)
{
	for(int i = 0; i < entries(); i++)
	{
		Facility* f = get(i);

		if(f->facilities & Facility::F_City)
		{
			City* city = (City*) f;

			if(city->size)
			{
				unsigned short divisor = (unsigned short)(64 - city->size) ;		// 1..64

				ResourceSet generated = resourceGeneration[city->type];
				
				city->resource.values[R_Human] 	  += (generated.values[R_Human] 	  * days) / divisor;
				city->resource.values[R_Horses] 	  += (generated.values[R_Horses]	  * days) / divisor;
				city->resource.values[R_Food] 	  += (generated.values[R_Food] 	  * days) / divisor;
				city->resource.values[R_Materials] += (generated.values[R_Materials] * days) / divisor;
			}
		}
	}
}

void CampaignEditControl::facilityOptions	(const Point& p)
{
	static MenuChoice choose[] = {
		MenuChoice("Facility Options",				0),
		MenuChoice("-",								0),
		MenuChoice("Remove All Facilities",		1),
		MenuChoice("Zero All Resources",			2),
		MenuChoice("Zero All Max Resources",	3),
		MenuChoice("Add a days resources",		4),
		MenuChoice("-",								0),
		MenuChoice("Cancel",							-1),
		MenuChoice(0,									0),
	};

	switch(menuSelect(0, choose, machine.mouse->getPosition()))
	{
	case 1:
		{
			while(world->facilities.entries())
				world->deleteFacility(world->facilities[0]);
		}
		break;
	case 2:		// Zero All
		world->facilities.zeroResources(True);
		break;

	case 3:
		world->facilities.zeroResources(False);
		break;

	case 4:
		world->facilities.setResources(1);
		break;
	}
}

void CampaignEditControl::railwayOptions	(const Point& p)
{
	static MenuChoice choose[] = {
		MenuChoice("Railway Options",				0),
		MenuChoice("-",								0),
		MenuChoice("Remove All Connection",		1),
		MenuChoice("Remove unused railheads",	2),
		MenuChoice("Check Zero Railhead",	   3),
		MenuChoice("-",								0),
		MenuChoice("Cancel",							-1),
		MenuChoice(0,									0),
	};

	switch(menuSelect(0, choose, machine.mouse->getPosition()))
	{
	case 1:		// Remove all
		{
			for(int i = 0; i < world->facilities.entries(); i++)
			{
				Facility* f = world->facilities[i];
				f->connections.clear();
			}
		}
		break;
	case 2:		// Remove unused rail heads
		{
			Boolean deleteAll = False;
			Boolean deleteNone = False;

			for(int i = 0; i < world->facilities.entries(); i++)
			{
				Facility* f = world->facilities[i];

				if(!f->connections.entries())
				{
					// if(f->railCapacity)
					// {
						f->railCapacity = 0;

						if(!f->facilities && !f->fortification)
						{
							Boolean result;

							if(!deleteAll && !deleteNone)
							{
								MemPtr<char> buffer(500);

								sprintf(buffer, "Delete empty facility\r%s\r%s",
									f->getName(),
									getStateName(f->state));

								switch(dialAlert(0, buffer, "Leave|Delete|Delete All|Delete None"))
								{
								case 0:
								default:
									result = False;
									break;
								case 1:
									result = True;
									break;
								case 2:
									deleteAll =True;
									result = True;
									break;
								case 3:
									deleteNone = True;
									result = False;
									break;
								}
							}
							else
								result = deleteAll;

							if(result)
							{
								world->deleteFacility(f);
								i--;
							}
						}
					// }
				}
			}
		}
		break;
	case 3:		// Look for places with no rail capacity
		{
			Boolean finished = False;

			for(int i = 0; !finished && (i < world->facilities.entries()); i++)
			{
				Facility* f = world->facilities[i];

				if(f->connections.entries() && !f->railCapacity)
				{
					MemPtr<char> buffer(500);

					sprintf(buffer, "%s, %s has %d junctions",
						f->getName(),
						getStateName(f->state),
						f->connections.entries());

					switch(dialAlert(0, buffer, "Leave|Set|Cancel"))
					{
					case 0:		// leave alone
						break;
					case 1:		// Set
						getCapacity(f);
						break;
					case 2:		// Cancel
						finished = True;
						break;
					}
				}
			}
		}
		break;
	}
}


void CampaignEditControl::supplyOptions 	(const Point& p)
{
	static MenuChoice choose[] = {
		MenuChoice("Supply Route Options",		0),
		MenuChoice("-",								0),
		MenuChoice("Clear All Supply Routes",	1),
		MenuChoice("Set to closest",				2),
		MenuChoice("-",								0),
		MenuChoice("Cancel",							-1),
		MenuChoice(0,									0),
	};

	switch(menuSelect(0, choose, machine.mouse->getPosition()))
	{
	case 1:	// Clear all
		{
			for(int i = 0; i < world->facilities.entries(); i++)
			{
				Facility* f = world->facilities[i];
				f->owner = NoFacility;
			}
			facilityChanged = True;
			changed = True;
			break;
		}
	case 2:	// Set to defaults
		{
			for(int i = 0; i < world->facilities.entries(); i++)
				setCloseOwner(world->facilities[i], True);
			break;
		}
	}
}

