/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Implementation of Unit's orders
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.9  1994/07/19  19:53:03  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/07/13  13:50:17  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/07/04  13:26:52  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/06/24  14:43:30  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/06/21  18:49:27  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/06/06  13:17:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/03/11  23:12:15  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/03/10  14:27:18  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/02/28  23:04:17  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "orders.h"
#include "gamelib.h"

Order::Order()
{
	mode = ORDER_Stand;
	how = ORDER_Land;
	destination = Location(0,0);
	onFacility = NoFacility;
	offFacility = NoFacility;
}

Order::Order(OrderMode o, OrderHow h, const Location& l)
{
	mode = o;
	how = h;
	destination = l;
	onFacility = NoFacility;
	offFacility = NoFacility;
}

SentOrder::SentOrder(OrderMode mode, OrderHow how, const Location& l, const TimeBase& when) :
	Order(mode, how, l)
{
	time = when;
}


static GameSprites orderIcons[] = {
	AI_Advance,
	AI_Cautious,
	AI_Attack,
	AI_Stand,
	AI_Hold,
	AI_Defend,
	AI_Withdraw,
	AI_Retreat,
	AI_HastyRetreat
};

UWORD Order::getOrderIcon() const
{
	return orderIcons[mode];
}


static BasicOrder basics[] = {
	ORDERMODE_Advance,
	ORDERMODE_Advance,
	ORDERMODE_Advance,
	ORDERMODE_Hold,
	ORDERMODE_Hold,
	ORDERMODE_Hold,
	ORDERMODE_Withdraw,
	ORDERMODE_Withdraw,
	ORDERMODE_Withdraw
};

BasicOrder Order::basicOrder() const
{
	return basics[mode];
}

BasicOrder getBasicOrder(OrderMode mode)
{
	return basics[mode];
}

/*====================================================
 * Battle Orders
 */

BattleOrder::BattleOrder()
{
	mode = ORDER_Stand;
	destination = Location(0,0);
}

BattleOrder::BattleOrder(OrderMode o, const Location& l)
{
	mode = o;
	destination = l;
}

SentBattleOrder::SentBattleOrder(OrderMode mode, const Location& l, const TimeBase& when) :
	BattleOrder(mode, l)
{
	time = when;
}


UWORD BattleOrder::getOrderIcon() const
{
	return orderIcons[mode];
}


BasicOrder BattleOrder::basicOrder() const
{
	return basics[mode];
}












































