/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign AI
 *
 * Completely redesigned and programmed by Steven Morle-Green
 * to replace bug-ridden passive AI designed by Adrian.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "ai_camp.h"
#include "campaign.h"
#include "campwld.h"
#include "city.h"
#include "game.h"

#ifdef DEBUG
#include "memptr.h"
#include "dialogue.h"

/*
 * Create a LogFile
 */

LogFile aiLog("ai.log");

#endif		// DEBUG


/*=================================================================
 * Constructor for CampaignAI class
 */

CampaignAI::CampaignAI()
{
	aiSide = SIDE_None;
	seaNavalNeeded = 0;
	riverNavalNeeded = 0;
	seaNavalGot = 0;
	riverNavalGot = 0;
#ifdef DEBUG
	aiLog.setTime(0);		// Disable Time display
#endif
}

/*
 * Destructor for CampaignAI class
 */

CampaignAI::~CampaignAI()
{
}

/*
 * Initialise Campaign AI for given side.
 *
 * reconstruct is set to indicate that the AI is being reconstructed 
 * from a saved game.
 *
 */

void CampaignAI::startCampaign(Side sd, Boolean reconstruct)
{
	aiSide = sd;

	/*
	 * Clear all Unit's UnitAI stuff
	 */

	clearUnits();


#ifdef DEBUG 
	dailyAI();			// Force 1 pass through AI to initialise variables
#endif
}

/*=================================================================
 * Process AI each day.
 *
 * During the processing a list of potential actions is built up
 * sorted by priority.
 *
 * This list is built up by:
 *   Looking at all cities and considering whether to attack or defend them.
 *   Looking at all friendly units and seeing if there are enemy units
 *   nearby and thus need reinforcing.
 *   Looking at all enemy units and consider attacking them.
 *
 *	Orders are then despatched to units and naval units by considering
 * each action in order of descending priority:
 *   Make list of independant units sorted in order of distance from action
 *   Send orders to units until enough forces have been sent.
 *
 * Other things the AI does is:
 *   Order things to be built in cities
 *   Keep the OB balanced and attach newly mobilised regiments.
 *   Order unused naval units to go attacking/defending water zones.
 *
 * During the List building phase variables are set:
 *   Cities:
 *     Fortifications needed
 *     Supplies needed
 *     Naval support needed
 */

void CampaignAI::dailyAI()
{
#ifdef DEBUG
	MemPtr<char> buffer(500);
	sprintf(buffer, "AI : %s", language(aiSide == SIDE_USA ? lge_Union : lge_Confederate));
	PopupText popup(buffer);
#endif

	events.clear();
	seaNavalNeeded = 0;
	riverNavalNeeded = 0;

#ifdef DEBUG
	sprintf(buffer + strlen(buffer), "\rAttaching new regiments");
	popup.showText(buffer);
#endif
	aic_AddNewRegiments();				// Attach any new regiments, balance OB
#ifdef DEBUG
	sprintf(buffer + strlen(buffer), "\rInitialising Units");
	popup.showText(buffer);
#endif
	aic_InitUnits();			// Check for completed/valid orders, clear flags
#ifdef DEBUG
	sprintf(buffer + strlen(buffer), "\rCity Actions");
	popup.showText(buffer);
#endif
	aic_SetCityActions();	// Make priority for attacking defending cities
#ifdef DEBUG
	sprintf(buffer + strlen(buffer), "\rUnit Actions");
	popup.showText(buffer);
#endif
	aic_SetUnitActions();	// Make priority for attacking enemy units

#ifdef DEBUG
	sprintf(buffer + strlen(buffer), "\rLand Units");
	popup.showText(buffer);
#endif
	aic_ProcUnits();			// Order Units to move
#ifdef DEBUG
	sprintf(buffer + strlen(buffer), "\rTransfer Units");
	popup.showText(buffer);
#endif
	aic_TransferUnits();		// Transfer units depending on Fields of Operation
#ifdef DEBUG
	sprintf(buffer + strlen(buffer), "\rNaval Units");
	popup.showText(buffer);
#endif
	aic_ProcNaval();			// Look for Water zones to defend/attck
#ifdef DEBUG
	sprintf(buffer + strlen(buffer), "\rResources");
	popup.showText(buffer);
#endif
	aic_Resources();			// Build facilities, mobilise regiments

	/*
	 * Clear the events list
	 */

	events.clear();

#ifdef DEBUG
	aiLog.printf("--------");
	aiLog.printf("Finished");
	aiLog.printf("--------");
	aiLog.close();
#endif
}


/*-----------------------------------------------------------------
 * Make priority for attacking enemy units
 */

void CampaignAI::aic_SetUnitActions()
{
#ifdef DEBUG
	aiLog.printf("aic_SetUnitActions()");
#endif
}

