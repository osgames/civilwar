/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Sound Library Class
 *
 * A Sound library is a class whereby a program can ask for a sound
 * effect to be played.  The library will take care of loading the
 * sample into memory, playing it and freeing it afterwards.  A cache
 * system will be used so that samples used several times are only
 * loaded once and samples will remain in memory until the memory
 * manager asks for more room.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */


#include <stdio.h>
#include "sndlib.h"
#include "readwav.h"
#include "game.h"
#include "memlog.h"

#ifdef SOUND_DEBUG
LogFile soundLog("sound.log");
#endif

class SoundCacheList;

class SoundMemHandle {
	PtrDList<SoundCacheList> libs;			// List of open libraries
	Boolean initialised;
public:
	SoundMemHandle() { initialised = False; }
	~SoundMemHandle();

	void add(SoundCacheList* lib);
	void remove(SoundCacheList* lib);

	Boolean freeUp(size_t s);
};

SoundMemHandle SndMemHandle;

class SoundCacheItem {
	friend class SoundCacheList;

	// SoundCacheItem* next;
	SoundID id;
	SoundBlock* sound;
	UBYTE used;
public:
	SoundCacheItem(SoundID n, SoundBlock* data)
	{
	 	id = n;
		sound = data;
		used = 1;
	}

	~SoundCacheItem() { delete sound; }

	void operator ++() { used++; }
	UBYTE operator --() { used--; return used; }
};

class SoundCacheList : public PtrDList<SoundCacheItem> {
public:
	SoundCacheList() { SndMemHandle.add(this); }
	~SoundCacheList() { SndMemHandle.remove(this); }

	void add(SoundID n, SoundBlock* data);
	void free(SoundID n);
	void free(SoundBlock* data);

	SoundBlock* findAndLock( SoundID n);
	Boolean flush();

	Boolean onlyUser(SoundBlock* data);

#ifdef DEBUG
	void check();
#endif
};

void SoundCacheList::add(SoundID n, SoundBlock* data)
{
	append(new SoundCacheItem(n, data));
}


#if defined(SOUND_DSMI)
/*============================================================
 * Sample Functions
 */

Sample::Sample()
{
	sample = 0;
	length = 0;
	loopstart = 0;
	loopend = 0;
	mode = 0;
	sampleID = 0;
	sampleRate = 0;
}

Sample::~Sample()
{
#ifdef SOUND_DEBUG
	soundLog.printf("Deleting sample %p %p",
		this, sample);
#endif

	if(sample)
		delete[] sample;
}

#endif   // defined(SOUND_DSMI)

/*============================================================
 * SoundBlock functions
 */

SoundBlock::SoundBlock()
{
	library = 0;
}

SoundBlock::~SoundBlock()
{
}

void SoundBlock::release()
{
	if(library)
		library->release(this);
}

Boolean SoundBlock::onlyUser()
{
	if(library)
		return library->onlyUser(this);
	else
		return False;
}

/*============================================================
 * SoundLibrary functions
 */

SoundLibrary::SoundLibrary(const char* name, const char** fileNames)
{
	cache = new SoundCacheList;
	names = fileNames;
}

SoundLibrary::~SoundLibrary()
{

#ifdef DEBUG
	/*
	 * Check cache for any unfreed images
	 */

	cache->check();

#endif

	cache->clearAndDestroy();
	delete cache;

}

void SoundLibrary::playSound(SoundID id, Volume volume, Panning pan, SoundPriority priority)
{
#if defined(SOUND_DSMI)
	/*
	 * This is for testing!
	 */

	if(game->soundInGame && (sound.scard.ID != NO_SOUNDCARD))
	{
		SoundBlock* sndBlock = cache->findAndLock(id);

		if(!sndBlock)
		{
			//	SoundBlock* 

			sndBlock = new SoundBlock;
			sndBlock->library = this;
#if 0
			readWAV("sounds\\testsnd.wav", sndBlock->sample);
#else
			char fileName[FILENAME_MAX];
			sprintf(fileName, "sounds\\%s.wav", names[id]);
			readWAV(fileName, sndBlock->sample);
#endif
			cache->add( id, sndBlock );
			soundManager.playSound(sndBlock, volume, pan, priority);

#ifdef SOUND_DEBUG
			soundLog.printf("loaded sound %d [%s] at %p, %p, volume %d, panning %d, priority %d",
				(int) id,
				fileName,
				sndBlock,
				sndBlock->sample.sample,
				(int) volume,
				(int) pan,
				(int) priority);
#endif


		}
		else 
		{
			soundManager.playSound(sndBlock, volume, pan, priority);

#ifdef SOUND_DEBUG
			soundLog.printf("cached sound %d [%s] at %p, %p, volume %d, panning %d, priority %d",
				(int) id,
				names[id],
				sndBlock,
				sndBlock->sample.sample,
				(int) volume,
				(int) pan,
				(int) priority);
#endif
		}
	}
#endif   // defined(SOUND_DSMI)
}

void SoundLibrary::release(SoundBlock* sound)
{
#ifdef SOUND_DEBUG
	soundLog.printf("Releasing soundblock %p %p", sound, sound->sample.sample);
#endif
	
	cache->free( sound );
}


Boolean SoundLibrary::onlyUser(SoundBlock* sound)
{
	return cache->onlyUser(sound);
}


void SoundLibrary::release(SoundID n)
{
	cache->free(n);
}

/*================================================================
 * Sound Manager Functions
 */

SoundManager soundManager;

#if defined(SOUND_DSMI)

SoundManager::SoundManager()
{
	for(int i = 0; i < EffectChannelCount; i++)
	{
		channels[i].sample = 0;
		channels[i].priority = 0;
	}

	nextChannel = 0;

}

SoundManager::~SoundManager()
{
}

int SoundManager::playSound(SoundBlock* sound, Volume volume, Panning pan, SoundPriority priority)
{
	/*
	 * Find a spare channel
	 */

	long channel = nextChannel;
	Boolean gotChannel = False;

	do {
		if(!channels[channel].sample ||
			(channels[channel].priority <= priority))
		{
			gotChannel = True;
			break;
		}

		channel++;
		if(channel >= EffectChannelCount)
			channel = 0;
	} while(channel != nextChannel);

	if(!gotChannel)
	{
		sound->release();
		return -1;
	}

	int physChannel = channel + EffectChannelStart;

	/*
	 * Release any currently playing sound on this channel
	 */

	SoundChannel* chan = &channels[channel];
	if(chan->sample)
		stopSound(channel);

	/*
	 * Adjust nextChannel
	 */

	nextChannel = channel + 1;
	if(nextChannel >= EffectChannelCount)
		nextChannel = 0;

	/*
	 * Set up channel
	 */

	chan->sample = sound;
	chan->priority = priority;

	/*
	 * Send it to DSMI
	 */

	SAMPLEINFO* sample = &sound->sample;

	
	if(!sound->onlyUser() || (cdiDownloadSample(physChannel, sample->sample, sample->sample, sample->length) == 0))
	// if(!sound->onlyUser() || (cdiDownloadSample(physChannel, sample->sample, (void*)physChannel, sample->length) == 0))
	{
		cdiSetInstrument(physChannel, sample);
		cdiPlayNote(physChannel, sound->sample.sampleRate, volume);
		cdiSetPanning(physChannel, pan);
	}
	else
	{
		/*
		 * Error...
		 */

		chan->sample->release();
		chan->sample = 0;
		return -1;
	}

	return channel;
}

void SoundManager::stopSound(int channel)
{
	if(channels[channel].sample)
	{
		int physChannel = channel + EffectChannelStart;

		SAMPLEINFO* sample = &channels[channel].sample->sample;


		cdiStopNote(physChannel);
		if(channels[channel].sample->onlyUser())
			cdiUnloadSample(physChannel, sample->sample);
		channels[channel].sample->release();
		channels[channel].sample = 0;
	}
}

void SoundManager::stopAll()
{
	for(int i = 0; i < EffectChannelCount; i++)
		stopSound(i);
}

/*
 * Check channels for any sound effects that have finished
 */

void SoundManager::process()
{
	for(int i = 0; i < EffectChannelCount; i++)
	{
		if(channels[i].sample)
		{
			if(!(cdiGetChannelStatus(i + EffectChannelStart) & CH_PLAYING))
				stopSound(i);
		}
	}
}

#endif   // defined(SOUND_DSMI)



#ifdef DEBUG

/*
 * Check cache for any unreleased sounds
 */

void SoundCacheList::check()
{
	if(memLog && memLog->logFile)
	{
		*memLog->logFile << "\nChecking Sounds\n";

		PtrDListIter<SoundCacheItem> iter = *this;

		Boolean found = False;

		while(++iter)
		{
			SoundCacheItem* cache = iter.current();

			if(cache->used)
			{
				*memLog->logFile << "  Sound " << cache->id << " is in use " << (int)cache->used << " times\n";
				found = True;
			}
		}

		if(!found)
			*memLog->logFile << "  No Unfreed sounds\n";

		*memLog->logFile << endl;
	}
}

#endif

/*
 * Memory Exhaustion Handling functions
 */

/*
 * This is the function that gets called if new fails
 */

Boolean soundMemHandlerFunction(size_t s)
{
	return SndMemHandle.freeUp(s);
}

Boolean SoundMemHandle::freeUp(size_t s)
{
	if(initialised)
	{
		if(libs.entries())
		{
			PtrDListIter<SoundCacheList> iter = libs;
			while(++iter)
			{
				SoundCacheList* lib = iter.current();

				if(lib->flush())
					return True;
			}
		}
	}
	return False;
}

static MemHandler soundMemHandler = { &soundMemHandlerFunction, 0 };

SoundMemHandle::~SoundMemHandle()
{
	if(initialised)
		removeMemHandler(&soundMemHandler);
}

/*
 * FLush out unused Sound entries
 *
 * This could be made more complex by freeing items in a certain order
 * until enough memory has been freed.
 */

Boolean SoundCacheList::flush()
{
	Boolean freedSomething = False;

	PtrDListIter<SoundCacheItem> iter = *this;

	while(++iter)
	{
		SoundCacheItem* cache = iter.current();

		if(!cache->used)
		{
			iter.remove();
			delete cache;
			freedSomething = True;
		}
	}

	return freedSomething;
}

/*
 * See if Sound is already in cache
 * If so, then increment its used count and return it
 */

SoundBlock* SoundCacheList::findAndLock(SoundID n)
{
	if(entries())
	{
		PtrDListIter<SoundCacheItem> iter = *this;

		while(++iter)
		{
			SoundCacheItem* cache = iter.current();

			if(cache->id == n)
			{
				cache->used++;

#ifdef SOUND_DEBUG
				soundLog.printf("cache %d (%p), used=%d",
					(int) n, cache->sound, cache->used);
#endif

				/*
				 * Should move it to the front of the queue so that
				 * it is not deleted
				 */

				return cache->sound;
			}
		}
	}

	return 0;
}

void SoundCacheList::free(SoundID n)
{
#ifdef SOUND_DEBUG
	soundLog.printf("SoundCache::free(%d)", (int) n);
#endif
	if(entries())
	{
		PtrDListIter<SoundCacheItem> iter = *this;

		while(++iter)
		{
			SoundCacheItem* cache = iter.current();

			if(cache->id == n)
			{
				if(!--cache->used)
				{
#if 0	// Leave it in the cache!
					delete cache->Sound;
					iter.remove();
					delete cache;
#endif
				}
#ifdef SOUND_DEBUG
				soundLog.printf("used = %d", (int) cache->used);
#endif
				return;
			}
		}
	}
#ifdef SOUND_DEBUG
	soundLog.printf("Not found in cache");
#endif
}

void SoundCacheList::free(SoundBlock* data)
{
#ifdef SOUND_DEBUG
	soundLog.printf("SoundCache::free(%p)", data);
#endif
	if(entries())
	{
		PtrDListIter<SoundCacheItem> iter = *this;

		while(++iter)
		{
			SoundCacheItem* cache = iter.current();

			if(cache->sound == data)
			{
				if(!--cache->used)
				{
#if 0
					delete cache->sound;
					iter.remove();
					delete cache;
#endif
				}
#ifdef SOUND_DEBUG
				soundLog.printf("used = %d", (int) cache->used);
#endif
				return;
			}
		}
	}
#ifdef SOUND_DEBUG
	soundLog.printf("Not found in cache");
#endif
}


Boolean SoundCacheList::onlyUser(SoundBlock* data)
{
	if(entries())
	{
		PtrDListIter<SoundCacheItem> iter = *this;

		while(++iter)
		{
			SoundCacheItem* cache = iter.current();

			if(cache->sound == data)
			{
				return (cache->used == 1);
			}
		}
	}
	return False;
}

/*
 * Add a new library
 */

void SoundMemHandle::add(SoundCacheList* lib)
{
	if(!initialised)
	{
		addMemHandler(&soundMemHandler);
		initialised = True;
	}

	libs.append(lib);
}

void SoundMemHandle::remove(SoundCacheList* lib)
{
	libs.remove(lib);
}

