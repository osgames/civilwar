/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Option settings
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/06/21  18:49:27  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/06/09  23:32:59  Steven_Green
 * Option to create world or read it from file
 *
 * Revision 1.1  1993/12/14  23:29:01  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "options.h"

Options globalOptions;

#ifdef DEBUG
UBYTE AI_ForceOption = 0;
Boolean showAI = False;
Boolean dontMove = False;
Boolean instantBuild = False;
Boolean fastResources = False;
Boolean quickMove = False;
Boolean seeAll = True;
#else
Boolean seeAll = False;
#endif

Boolean smoothClock = True;
Boolean noBuildMessages = False;
Boolean realTimeCampaign = False;
RouteMode routeMode = RM_Simple;
int aiPasses = 10;							// Maximum number of AI Passes to do
