/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Battle Statistics
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "batldata.h"
#include "unititer.h"
#include "game.h"
#include "sprlib.h"
#include "tables.h"
#include "colours.h"
#include "language.h"
#include "text.h"
#include "layout.h"
#include "system.h"
#include "screen.h"

#define Font_Header   Font_EMMA14
#define Font_Heading	 Font_JAME_5
#define Font_Value	 Font_JAME08

void BattleData::setupStats()
{
	/*
	 * Clear values
	 */

	for(SideIndex i = SideIndex(0); i < SideCount; INCREMENT(i))
	{
		for(BasicRegimentType t = 0; t < 3; t++)
		{
			losses[i][t] = 0;
			currentStrength[i][t] = 0;
		}
	}

	/*
	 * Go through Battle OB and initialise currentStrengths
	 */

	for(Unit* p = ob->getSides(); p; p = p->sister)
	{
		UnitIter iter(p, False, False, Rank_Regiment, Rank_Regiment);

		SideIndex si = sideIndex(p->getSide());
		ASSERT(si >= 0);
		ASSERT(si < 2);

		while(iter.ok())
		{
			Unit* unit = iter.get();
			ASSERT(unit != 0);
			ASSERT(unit->getRank() == Rank_Regiment);
			Regiment* r = (Regiment*) unit;
			if(r->battleInfo)
			{
				BasicRegimentType type = r->type.basicType;
				ASSERT(type >= 0);
				ASSERT(type < 3);
				currentStrength[si][type] += r->strength;
			}
		}
	}
}

void BattleData::statsUpdate(const Regiment* r, Strength loss) 
{
	ASSERT(r != 0);

	SideIndex side = sideIndex(r->getSide());
	BasicRegimentType type = r->type.basicType;

	ASSERT(side >= 0);
	ASSERT(side < 2);
	ASSERT(type >= 0);
	ASSERT(type < 3);

	ASSERT(currentStrength[side][type] >= loss);

	losses[side][type] += loss;
	currentStrength[side][type] -= loss;

	statsChanged = True; 
}

void BattleData::showStats(Boolean force)
{
	if(force || statsChanged)
	{
		statsChanged = False;

		putStatWindow(SIDE_USA);
		putStatWindow(SIDE_CSA);

		machine.screen->setUpdate(*infoArea);
	}
#ifdef DEBUG
	else
	{
		static Boolean flag = False;
		infoArea->box(10, 10, 10, 10, flag ? Red : Blue);
		flag = !flag;
		machine.screen->setUpdate(*infoArea);
	}
#endif
}

/*
 * Display one line of values
 */

static void putValues(Region& r, TextWindow& textWind, Strength str, Strength loss, int y)
{
	const Colours LossColour = Red2;
	const Colours StrengthColour = Black;

	textWind.setColours(StrengthColour);
	textWind.setPosition(Point(30, y));
	textWind.wprintf("%lu", (ULONG) str);

	if((str + loss) != 0)
	{
		int percent = (str * 100) / (str + loss);
		textWind.setPosition(Point(30, y + 9));
		textWind.wprintf("%d%%", percent);

		textWind.setPosition(Point(80, y));
		textWind.setColours(LossColour);
		textWind.wprintf("%lu", (ULONG) loss);
		textWind.setPosition(Point(80, y + 9));
		textWind.wprintf("%d%%", 100 - percent);

		int width = r.getW() - 30 - 4;
		r.frame(30, y + 18, width, 6, Black);
		width -= 2;
		SDimension w = (str * width) / (str + loss);
		if(w)
			r.box(31, y + 19, w, 4, Green);
		int w1 = width - w;
		if(w1)
			r.box(31 + w, y + 19, w1, 4, Red2);
	}
}

void BattleData::putStatWindow(Side side)
{
	const int StatHeight = INFO_HEIGHT / 2;		// = 156
	const Colours DivideColour = Grey6;
	const Colours LossColour = Red2;

	int y;

	if(game->getLocalSide() == side)
		y = 0;
	else
		y = StatHeight;

	Region r = *infoArea;
	r.setClip(infoArea->getX(), infoArea->getY() + y, infoArea->getW(), StatHeight);

	SpriteIndex sNum;
	SpriteIndex flagSpr;

	if(side == SIDE_USA)
	{
		sNum = FillUSA_Grey;
		flagSpr = TinyFlagUSA;
	}
	else
	{
		sNum = FillCSA_Grey;
		flagSpr = TinyFlagCSA;
	}

	SpriteBlock* fillPattern = game->sprites->read(sNum);
	r.fill(fillPattern);
	fillPattern->release();

	r.frame(0, 0, r.getW(), r.getH(), Black);
	r.HLine(0, 20, r.getW(), Black);

	r.frame(1, 1, r.getW()-2, 19, Greye, Grey4);
	r.frame(1, 21, r.getW()-2, r.getH()-22, Greye, Grey4);

	// r.HLine(0, 20, r.getW(), Black);

	r.HLine(2, 29, r.getW()-4, DivideColour);

	r.VLine(28, 21, r.getH()-23, DivideColour);
	r.VLine(76, 21, r.getH()-23, DivideColour);

	game->sprites->drawSprite(&r, Point(3, 5), flagSpr);

	TextWindow textWind(&r, Font_Header);
	textWind.setFormat(False, False);

	textWind.setPosition(Point(24, 4));
	textWind.setColours(Black);
	textWind.draw(getSideStr(side));

	textWind.setFont(Font_Heading);
	textWind.setColours(Black);

	textWind.setPosition(Point(30, 23));
	textWind.draw(language(lge_statStrength));

	textWind.setColours(LossColour);
	textWind.setPosition(Point(78, 23));
	textWind.draw(language(lge_statCasualties));

	textWind.setFont(Font_Value);

	/*
	 * Display Strengths
	 */

	SideIndex si = sideIndex(side);
	Strength totalStrength = 0;
	Strength totalLoss = 0;

	for(int t = 0; t < 3; t++)
	{
		int y = 31 + t * 28;

		static SpriteIndex sprites[3] = {
			I_CAL_Infantry,
			I_CAL_Cavalry,
			I_CAL_Artillery
		};

		putValues(r, textWind, currentStrength[si][t], losses[si][t], y);
		r.HLine(2, y + 26, r.getW()-4, DivideColour);
		game->sprites->drawSprite(&r, Point(3, y+1), sprites[t]);

#if 0
		textWind.setPosition(Point(34, y));
		textWind.setColours(StrengthColour);
		textWind.wprintf("%lu", (ULONG) currentStrength[si][t]);

		textWind.setPosition(Point(80, y));
		textWind.setColours(LossColour);
		textWind.wprintf("%lu", (ULONG) losses[si][t]);
#endif

		totalStrength += currentStrength[si][t];
		totalLoss += losses[si][t];
	}

	r.HLine(2, 114, r.getW()-4, DivideColour);
	putValues(r, textWind, totalStrength, totalLoss, 118);
	r.HLine(2, 144, r.getW()-4, DivideColour);
	r.HLine(2, 145, r.getW()-4, DivideColour);

#if 0
	textWind.setPosition(Point(34, 108));
	textWind.setColours(StrengthColour);
	textWind.wprintf("%lu", (ULONG) totalStrength);

	textWind.setPosition(Point(80, 108));
	textWind.setColours(LossColour);
	textWind.wprintf("%lu", (ULONG) totalLoss);
#endif

}


