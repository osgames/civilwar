/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battled Editor : Static Object Editting
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "bated.h"
#include "staticob.h"
#include "map3d.h"
#include "dialogue.h"
#include "text.h"
#include "colours.h"
#include "system.h"
#include "screen.h"
#include "mouselib.h"
#include "batltab.h"

static const char* staticObjectNames[] = {
	"Church",
	"Church 45",
	"House",
	"House 45",
	"Tavern",
	"Tavern 45",
	"Church1",
	"Church1 45",
	"House1",
	"House1 45",
	"Tavern1",
	"Tavern1 45",
	"Tree1",
	"Tree2",
	"Tree3",
	"Tree4",
	"Tree5",
	"Tree6",

	"Dead Horse",
	"Dead Artillery",
	"Dead Union",
	"Dead Confederate",

	"tent",

	0
};

static const char* quadNames[] = {
	"North",
	"East",
	"South",
	"West",

	0
};

const char* getTreeTypeName(StaticSpriteType t)
{
	return staticObjectNames[t];
}

const char* getQuadName(Qangle t)
{
	return quadNames[t];
}

StaticSpriteType getTreeType(StaticSpriteType initValue)
{
	StaticSpriteType retVal = initValue;

	MenuChoice* choices = new MenuChoice[StaticSpriteTypeCount + 5];
	MenuChoice* ch = choices;

	*ch++ = MenuChoice("Select Object Type", 0);
	*ch++ = MenuChoice("-", 0);
	
	for(int i = 0; i < StaticSpriteTypeCount; i++)
	{
		*ch++ = MenuChoice(staticObjectNames[i], i + 1);
	}

	*ch++ = MenuChoice("-", 0);
	*ch++ = MenuChoice("Cancel", -1);
	*ch++ = MenuChoice(0, 0);

	int choice = menuSelect(0, choices, machine.mouse->getPosition());
	if(choice == -1)
		retVal = initValue;
	else
		retVal = StaticSpriteType(choice - 1);

	delete[] choices;

	return retVal;
}

Qangle getTreeFacing(Qangle initValue)
{
	Qangle retVal;

	MenuChoice* choices = new MenuChoice[4 + 5];
	MenuChoice* ch = choices;

	*ch++ = MenuChoice("Select Facing", 0);
	*ch++ = MenuChoice("-", 0);
	
	for(int i = 0; i < 4; i++)
	{
		*ch++ = MenuChoice(quadNames[i], i + 1);
	}

	*ch++ = MenuChoice("-", 0);
	*ch++ = MenuChoice("Cancel", -1);
	*ch++ = MenuChoice(0, 0);

	int choice = menuSelect(0, choices, machine.mouse->getPosition());
	if(choice == -1)
		retVal = initValue;
	else
		retVal = choice - 1;

	delete[] choices;

	return retVal;
}

void editStaticObject(TreeObject* ob)
{
	Boolean finished = False;
	StaticSpriteType what = ob->what;
	Qangle facing = ob->facing;

	while(!finished)
	{
		enum {
			GetTreeType = 1,
			GetTreeFacing,
			GetTreeOk,
			GetTreeCancel
		};
		
		static MenuChoice choices[] = {
			MenuChoice("Edit Object", 0),
			MenuChoice("-", 0),
			MenuChoice(0, GetTreeType),
			MenuChoice(0, GetTreeFacing),
			MenuChoice("-", 0),
			MenuChoice("OK", GetTreeOk),
			MenuChoice("Cancel", GetTreeCancel),
			MenuChoice(0,0)
		};

		choices[2].text = staticObjectNames[what];
		choices[3].text = quadNames[facing];

		switch(menuSelect(0, choices, machine.mouse->getPosition()))
		{
			case GetTreeType:
				what = getTreeType(what);
				break;
			case GetTreeFacing:
				facing = getTreeFacing(facing);
				break;
			case GetTreeOk:
				ob->what = what;
				ob->facing = facing;
				control->adjustObjectPosition(ob);
				changed = True;
			case GetTreeCancel:
				finished = True;
				break;
		}
	}
}

void BattleEditControl::objectOverMap(const Event* event)
{
	if(trackMode == Tracking)
	{
		if(gotMouseLocation)
		{
			/*
			 * Find closest Object to mouse
			 */

			StaticObjectIter iter(miscObjects);
			TreeObject* best = 0;
			Cord3D close = 0;

			BattleObject* ob;
			while( (ob = iter.next()) != 0)
			{
				Cord3D d = distance(ob->where.x - mouseLocation.x,
										  ob->where.z - mouseLocation.z);
				if(!best || (d < close))
				{
					close = d;
					best = (TreeObject*) ob;
				}
			}

			if(best)
				setCurrentObject(best);
			else
				loseObject();
		}
		else
			loseObject();

		if(event->buttons & Mouse::LeftButton)
		{
			if(gotMouseLocation)
			{
				DoWhat what = DoNothing;

				if(currentBattleObject)
				{
					static MenuChoice choices[] = {
						MenuChoice(0, 0),
						MenuChoice("-", 0),
						MenuChoice("Move", DoMove),
						MenuChoice("Delete", DoDelete),
						MenuChoice("Edit", DoEdit),
						MenuChoice("New", DoNew),
						MenuChoice("-", 0),
						MenuChoice("Cancel", -1),
						MenuChoice(0,0)
					};

					choices[0].text = staticObjectNames[currentBattleObject->what];

					what = DoWhat(menuSelect(0, choices, machine.mouse->getPosition()));
				}
				else		// Create a new one
				{
					static MenuChoice choices[] = {
						MenuChoice("New Object", DoNew),
						MenuChoice("-", 0),
						MenuChoice("Cancel", -1),
						MenuChoice(0,0)
					};

					what = DoWhat(menuSelect(0, choices, machine.mouse->getPosition()));
				}

				switch(what)
				{
				case DoNew:
					{
						TreeObject* newOb = new TreeObject(defaultTreeType, mouseLocation, defaultTreeFacing);
						miscObjects->add(newOb);
						setCurrentObject(newOb);
						adjustObjectPosition(newOb);
						changed = True;
					}
					break;
				case DoMove:
					defaultTreeType = currentBattleObject->what;
					defaultTreeFacing = currentBattleObject->facing;
					trackMode = Moving;
					hint("Click on place to move object");
					break;
				case DoDelete:
					{
						TreeObject* ob = currentBattleObject;
						loseObject();
						miscObjects->remove(ob);
						delete ob;
						changed = True;
					}
					break;
				case DoEdit:
					editStaticObject(currentBattleObject);
					defaultTreeType = currentBattleObject->what;
					defaultTreeFacing = currentBattleObject->facing;
					hint(0);
					break;
				}
			}

		}
		else if(event->buttons & Mouse::RightButton)
		{
			if(currentBattleObject)
			{
				editStaticObject(currentBattleObject);
				defaultTreeType = currentBattleObject->what;
				defaultTreeFacing = currentBattleObject->facing;
				hint(0);
			}
		}
	}
	else if(trackMode == Moving)
	{
		if(event->buttons & Mouse::LeftButton)
		{
			if(gotMouseLocation && currentBattleObject)
			{
				miscObjects->remove(currentBattleObject);

				currentBattleObject->where = mouseLocation;
				adjustObjectPosition(currentBattleObject);
				miscObjects->add(currentBattleObject);
				currentBattleObject->draw(data3D.view);
				changed = True;
			}
			trackMode = Tracking;
			hint(0);
		}
		else if(event->buttons & Mouse::RightButton)
		{
			trackMode = Tracking;
			hint(0);
		}
	}
}

void BattleEditControl::objectOptions()
{
	static MenuChoice choices[] = {
		MenuChoice("Delete All Objects", 1),
		MenuChoice("Update Terrain", 2),
		MenuChoice("Undo Terrain", 3),
		MenuChoice("-", 0),
		MenuChoice("Cancel", -1),
		MenuChoice(0,0)
	};

	switch(menuSelect(0, choices, machine.mouse->getPosition()))
	{
	case 1:
		miscObjects->clear();
		trackMode = Tracking;
		hint(0);
		changed = True;
		break;
	case 2:
		storeGrid();
		setupObjectTerrain();
		grid->makeZoomedGrids(data3D);
		updateView();
		changed = True;
		break;
	case 3:
		undoGrid();
		break;
	}
}


void TreeObject::showInfo(Region* r)
{
	control->setupInfo();
	TextWindow& textWind = *control->textWin;
	textWind.setFont(Font_Heading);
	textWind.setColours(Black);
	textWind.setHCentre(True);
	textWind.setPosition(Point(4,4));
	textWind.draw(staticObjectNames[what]);
	textWind.newLine();
	textWind.moveDown(4);
	textWind.setHCentre(False);
	textWind.wprintf("Facing: %s\r", quadNames[facing]);
}



/*
 * Adjust object positions so aligned with terrain grid
 * Set terrain underneath to built up.
 * Remove builtup terrains without buildings
 */

void BattleEditControl::setupObjectTerrain()
{
	/*
	 * Go through and remove all builtup area terrain
	 */

	Grid* g = grid->getGrid(Zoom_Min);
	g->removeTerrain(BuiltupTerrain);

	/*
	 * Step through object list
	 */

	StaticObjectIter iter(miscObjects);
	BattleObject* ob;
	while( (ob = iter.next()) != 0)
	{
		TreeObject* tob = (TreeObject*) ob;
		UBYTE obSize = staticObjectSizes[tob->what];

		if(obSize)
		{
			MapGridCord gx = grid->cord3DtoGrid(tob->where.x);
			MapGridCord gy = grid->cord3DtoGrid(tob->where.z);

			tob->where.x = grid->gridToCord3D(gx);
			tob->where.z = grid->gridToCord3D(gy);

			if(obSize & 1)
			{
				tob->where.x += g->gridSize / 2;
				tob->where.z += g->gridSize / 2;
			}

			g->placeTerrain(BuiltupTerrain, gx, gy, obSize, False, False);
		}
	}

	g->tidy();
}

void BattleEditControl::adjustObjectPosition(TreeObject* tob)
{
	UBYTE obSize = staticObjectSizes[tob->what];

	if(obSize)
	{
		Grid* g = battle->grid->getGrid(Zoom_Min);

		MapGridCord gx = grid->cord3DtoGrid(tob->where.x);
		MapGridCord gy = grid->cord3DtoGrid(tob->where.z);

		tob->where.x = grid->gridToCord3D(gx);
		tob->where.z = grid->gridToCord3D(gy);

		if(obSize & 1)
		{
			tob->where.x += g->gridSize / 2;
			tob->where.z += g->gridSize / 2;
		}

		g->placeTerrain(BuiltupTerrain, gx, gy, obSize, False, False);
	}
}
