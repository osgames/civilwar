/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Files used in ACW placed here to make it easier to change them
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:17  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "files.h"

const char playScreenFileName[]		= "art\\screens\\playscrn.lbm";
const char battleSpriteFileName[]	= "art\\battle";
const char battleSoundFileName[]		= "sounds\\battle";

const char workDir[] = "work";
const char musicDir[] = "dbdata";

const char startWorldName[] = "gamedata\\start.cam";

const char zoomMapName[] = "art\\maps\\mapzoom.bm";
// const char dynamicWorldName[] = "gamedata\\world_d.rrf";
// const char staticTroopName[] = "gamedata\\troops.rrf";

