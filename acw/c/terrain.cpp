/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Terrain Information
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.8  1994/08/31  15:23:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/08/24  15:05:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/08/19  17:29:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/04/05  12:28:09  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/03/18  15:07:16  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1993/12/23  09:26:01  Steven_Green
 * 4 Terrain tiles are loaded.
 * Colours are weighted towards greens.
 *
 * Revision 1.2  1993/12/21  00:31:04  Steven_Green
 * Function to get height at any arbitary location.
 *
 * Revision 1.1  1993/12/15  20:56:36  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

// #include <stdlib.h>	// for rand()

#include "terrain.h"
#include "system.h"
#include "screen.h"
#include "sprlib.h"
#include "image.h"
#include "ilbm.h"
#include "options.h"
#include "error.h"
#ifdef DEBUG
#include "log.h"
#endif

/*
 * Uncommenting the following line
 * forces terrain colours to use the maximum colour range
 */

#define MAX_COLOUR_RANGE

/*
 * Global Terrain Information
 *
 * It will need initialising at some point.
 */

// Terrain terrain;


/*
 * Initialisation
 */

Terrain::Terrain() :
	colourUsage(0x70, 0xef)
{
	{
		SpriteLibrary textureLib("art\\texture.spr");

		for(int i = 0; i < TEXTURE_COUNT; i++)
		{
			sampleTexture[i] = *textureLib.read(i);
			textureLib.release(i);
		}
	}

	/*
	 * Copy colour table
	 */

	for(int i = 0; i < MAX_TERRAIN_COL; i++)
	{
		colourInfo[i].hue = colours[i].hue;
		colourInfo[i].saturation = colours[i].saturation;
		colourInfo[i].used = False;
	}
}

Terrain::~Terrain()
{
}

void Terrain::makeColours()
{
	colourUsage.reset();

	for(int i = 0; i < MAX_TERRAIN_COL; i++)
		colourUsage.makeColours(&colourInfo[i]);
}


void Terrain::resetColours()
{
#ifdef DEBUG
	if(logFile && logFile->isShown(100))
		*logFile << "Terrain::resetColours()" << endl;
#endif

	for(int i = 0; i < MAX_TERRAIN_COL; i++)
	{
		colourInfo[i].used = False;
		colourInfo[i].colourRange = 0;
	}
	colourUsage.reset();
}

const Image* Terrain::getTexture(TerrainType t) const
{
	if(optTexture())
		return &sampleTexture[terrains[t].texture];
	else
		return 0;
}

TerrainCol* Terrain::getColour(TerrainType t)
{
	return &colourInfo[terrains[t].colour];
}



/*
 * Colours
 */

TerrainCol::TerrainCol()
{
	hue = 0;					// UBYTE(rand() & 0xff);
	saturation = 0xff;	// UBYTE(rand() & 0xff);

	minVol = 0;
	maxVol = 0;
	baseColour = 0;
	colourRange = 0;

	used = False;
}

void TerrainCol::addIntensity(Intensity i)
{
#ifdef DEBUG
	if(logFile && logFile->isShown(100))
		*logFile << "TerrainCol<" << (int)hue << "," << (int)saturation << ">::addIntensity(" << i << ")" << endl;
#endif

	if(used)
	{
		if(i < minVol)
			minVol = i;
		if(i > maxVol)
			maxVol = i;
	}
	else
	{
		minVol = i;
		maxVol = i;
		used = True;
	}
}


/*
 * Palette Manager
 */


PaletteManager::PaletteManager(Colour min, Colour max)
{
	minCol = min;
	maxCol = max;
	nextColour = min;

	colours = new TrueColour[max - min + 1];
}

PaletteManager::~PaletteManager()
{
	delete colours;
}

void PaletteManager::reset()
{
#ifdef DEBUG
	if(logFile && logFile->isShown(100))
		*logFile << "PaletteManager::reset()" << endl;
#endif
	nextColour = minCol;
}

void PaletteManager::makeColours(TerrainCol* v)
{
	v->baseColour = nextColour;
	v->colourRange = 16;
	nextColour += 16;

// #ifdef DEBUG
#if defined(MAX_COLOUR_RANGE)
	if(optGourad())
	{
		// Experiment with preset colours
		// Temporarily do this by fixing minVol and maxVol

		v->minVol = 0;
		v->maxVol = intensityRange;
	}
#endif

	/*
	 * Add colours to the palette!
	 */

	UBYTE req = 16;

	Intensity deltaI = Intensity((v->maxVol - v->minVol) / req);
	Intensity i = v->minVol;
	TrueColour* col = &colours[v->baseColour - minCol];

	while(req--)
	{
		col->fromHSV(v->hue, v->saturation, i);
	
		i += deltaI;
		col++;
	}
}

void PaletteManager::setPalette()
{
#ifdef DEBUG
	if(logFile && logFile->isShown(100))
		*logFile << "PaletteManager::setHardwarePalette()" << endl;
#endif

#if 0
	setHardwarePalette(minCol, maxCol - minCol + 1, colours);
#else
	machine.screen->updateColours(minCol, maxCol - minCol + 1, colours);
#endif
}

