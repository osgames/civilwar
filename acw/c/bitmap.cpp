/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Uncompressed bitmap support
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/06/21  18:45:12  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

// #define CPP_IO		// C++ Streams
// #define C_IO		// Buffer C I/O
// #define SYS_IO			// Unbuffered
#define DOS_IO

#include <string.h>
#include "cd_open.h"

#if defined(CPP_IO)
 #include <fstream.h>
#elif defined(C_IO)
 #include <stdio.h>
#elif defined(SYS_IO)
 #include <fcntl.h>
 #include <io.h>
#elif defined(DOS_IO)
 #include <dos.h>
 #include <fcntl.h>
 #include <dosfunc.h>
 #include <io.h>
#endif

#include "bitmap.h"
#include "image.h"

#if defined(DOS_IO)
inline void _dos_lseek(int fp, long offset, int how)
{
	union REGS in_regs, out_regs;

	in_regs.h.ah = DOS_LSEEK;
	in_regs.h.al = (unsigned char) how;
	in_regs.w.bx = (unsigned short) fp;
	in_regs.w.cx = (unsigned short)(offset >> 16);
	in_regs.w.dx = (unsigned short)offset;
	intdos(&in_regs, &out_regs);
}
#endif



/*
 * Read section of bitmap from disk
 */

void readBMsection(Image* img, const char* name, const Point& pt)
{
#if defined(CPP_IO)
	ifstream stream;
	
	int result = fileOpen( stream, name, ios::in | ios::binary | ios::nocreate);

	if(stream)
	{
		BM_Header header;
		stream.read( (char*)&header, sizeof(header));

		UWORD width = header.getWidth();
		UWORD height = header.getHeight();

		/*
		 * Set up rectangle to read (clipping to image and file)
		 */

		SDimension w = img->getWidth();
		SDimension h = img->getHeight();

		Point p = pt;

		if(p.x < 0)
		{
			w += p.x;
			p.x = 0;
		}

		if(p.y < 0)
		{
			h += p.y;
			p.y = 0;
		}

		if( (p.x + w) > width)
			w = SDimension(width - p.x);

		if( (p.y + h) > height)
			h = SDimension(height - p.y);

		if( (w > 0) && (h > 0) )
		{
			/*
			 * Seek to position
			 */

			stream.seekg(p.x + p.y * width, ios::cur);

			/*
			 * Read line by line
			 */

			UBYTE* line = img->getAddress();

			while(h--)
			{
				stream.read(line, w);

				if(h)
				{
					line += img->getWidth();
					stream.seekg(width - w, ios::cur);
				}
			}
		}

		stream.close();
	}
	else
		throw GeneralError("Couldn't read bitmap");
#elif defined(C_IO)
	FILE* fp = FileOpen(name, "rb");
			//	fopen(name, "rb");

	if(fp)
	{
		BM_Header header;

		fread(&header, sizeof(header), 1, fp);

		UWORD width = header.getWidth();
		UWORD height = header.getHeight();

		/*
		 * Set up rectangle to read (clipping to image and file)
		 */

		SDimension w = img->getWidth();
		SDimension h = img->getHeight();

		Point p = pt;

		if(p.x < 0)
		{
			w += p.x;
			p.x = 0;
		}

		if(p.y < 0)
		{
			h += p.y;
			p.y = 0;
		}

		if( (p.x + w) > width)
			w = SDimension(width - p.x);

		if( (p.y + h) > height)
			h = SDimension(height - p.y);

		if( (w > 0) && (h > 0) )
		{
			/*
			 * Seek to position
			 */

			fseek(fp, p.x + p.y * width, SEEK_CUR);

			/*
			 * Read line by line
			 */

			UBYTE* line = img->getAddress();

			while(h--)
			{
				fread(line, w, 1, fp);

				if(h)
				{
					line += img->getWidth();
					fseek(fp, width - w, SEEK_CUR);
				}
			}
		}

		fclose(fp);
	}
	else
		throw GeneralError("Couldn't read bitmap");
#elif defined(SYS_IO)
	int fp = fileOpen(name, O_RDONLY | O_BINARY);

	if(fp)
	{
		BM_Header header;

		read(fp, &header, sizeof(header));

		UWORD width = header.getWidth();
		UWORD height = header.getHeight();

		/*
		 * Set up rectangle to read (clipping to image and file)
		 */

		SDimension w = img->getWidth();
		SDimension h = img->getHeight();

		Point p = pt;

		if(p.x < 0)
		{
			w += p.x;
			p.x = 0;
		}

		if(p.y < 0)
		{
			h += p.y;
			p.y = 0;
		}

		if( (p.x + w) > width)
			w = SDimension(width - p.x);

		if( (p.y + h) > height)
			h = SDimension(height - p.y);

		if( (w > 0) && (h > 0) )
		{
			/*
			 * Seek to position
			 */

			lseek(fp, p.x + p.y * width, SEEK_CUR);

			/*
			 * Read line by line
			 */

			UBYTE* line = img->getAddress();

			while(h--)
			{
				read(fp, line, w);

				if(h)
				{
					line += img->getWidth();
					lseek(fp, width - w, SEEK_CUR);
				}
			}
		}

		close(fp);
	}
	else
		throw GeneralError("Couldn't read bitmap");
#elif defined(DOS_IO)

	int fp;

	if(_dos_open( getPathAndCheck( name ), O_RDONLY, &fp) == 0)
	{
		BM_Header header;

		unsigned int bytes;

		_dos_read(fp, &header, sizeof(header), &bytes);

		UWORD width = header.getWidth();
		UWORD height = header.getHeight();

		/*
		 * Set up rectangle to read (clipping to image and file)
		 */

		SDimension w = img->getWidth();
		SDimension h = img->getHeight();

		Point p = pt;

		if(p.x < 0)
		{
			w += p.x;
			p.x = 0;
		}

		if(p.y < 0)
		{
			h += p.y;
			p.y = 0;
		}

		if( (p.x + w) > width)
			w = SDimension(width - p.x);

		if( (p.y + h) > height)
			h = SDimension(height - p.y);

		if( (w > 0) && (h > 0) )
		{
			/*
			 * Seek to position
			 */

			_dos_lseek(fp, p.x + p.y * width, SEEK_CUR);

			/*
			 * Read line by line
			 */

			UBYTE* line = img->getAddress();

			while(h--)
			{
				_dos_read(fp, line, w, &bytes);

				if(h)
				{
					line += img->getWidth();
					_dos_lseek(fp, width - w, SEEK_CUR);
				}
			}
		}

		_dos_close(fp);
	}
	else
		throw GeneralError("Couldn't read bitmap");
#endif
}

