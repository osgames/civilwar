/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign AI: City attack Value calculations
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1995/09/26 15:05:42  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "ai_camp.h"
#include "aic_misc.h"
#include "campaign.h"
#include "campwld.h"
#include "city.h"
#include "unititer.h"
#include "combval.h"
#include "myassert.h"
#include "camptab.h"
#ifdef DEBUG
#include "memptr.h"
#include "dialogue.h"
#endif

/*
 * Get the side of a town's state
 */

Side getStateSide(const Facility* f)
{
	if(f->state != NoState)
	{
		const State& state = campaign->world->states[f->state];
		return state.side;
	}
	else
		return SIDE_None;
}

/*------------------------------------------------------
 * Calculate influence of a side's towns from a town
 * Not including itself!
 *
 * side: Side of facilities to influence
 * aiSide: Side considering attacking/defending
 */

long calcNearCityValue(const Facility* where, Side side, Side aiSide)
{
	long cityValue = 0;

	Location loc = where->location;

	/*
	 * Constants used to calculate distance values
	 */

	const int K1 = 5;									// Ignore values < 5%
	const int Vmax = 20 + K1;						// 20% at 0 miles
	const int D1 = 25;								// 5% at 25 miles
	const int V1 = 5 + K1;
	const int K = (D1 * V1) / (Vmax - V1);		// K = 40 Miles

	// Maximum miles to make any difference.
	// The 4 is a bodge factor... assumes that adjustments
	// will not reduce it to less than 1/4.

	const int maxM = 4 * ((Vmax * K) / (1 + K1) - K);
	const Distance maxD = Mile(maxM);


	FacilityList& fList = campaign->world->facilities;

	for(int i = 0; i < fList.entries(); i++)
	{
		Facility* f = fList[i];

		if((f->side == side) && (f != where) && f->isRealTown())
		{
			Distance d = distanceLocation(loc, f->location);

			/*
			 * Quick cut off for towns far away
			 */

			if(d > maxD)
				continue;

			int miles = unitToMile(d);

			/*
			 * Let different types of home cities have different weightings
			 *		Capital Cities to have larger values
			 *		Cities to have larger values
			 *		Larger cities to have more value
			 *		Occupied Cities to be more
			 *		Ports to be more
			 *		Fortifications and other facility types
			 * These done by changing the effective distance by a percentage
			 */

			if(f->facilities & Facility::F_City)
			{
				subPercent(miles, 5);		// 5% for being a city

				if(f->facilities & Facility::F_KeyCity)
					subPercent(miles, 10);		// Another 10% for a key city

				City* city = (City*) f;

				subPercent(miles, city->size / 3);		// Up to 20% for large city
			}

			if(f->facilities & Facility::F_SupplyDepot)
				subPercent(miles, 3);
			if(f->facilities & Facility::F_RecruitmentCentre)
				subPercent(miles, 2);
			if(f->facilities & Facility::F_TrainingCamp)
				subPercent(miles, 1);
			if(f->facilities & Facility::F_Hospital)
				subPercent(miles, 1);
			if(f->facilities & Facility::F_POW)
				subPercent(miles, 1);
			if(f->facilities & Facility::F_Port)
				subPercent(miles, 5);
			if(f->facilities & Facility::F_LandingStage)
				subPercent(miles, 3);

			if(f->railCapacity)
				subPercent(miles, (f->railCapacity / 10) + 1);

			if(f->fortification)
				subPercent(miles, f->fortification * 1);

			subPercent(miles, f->supplies / 30);

			/*
			 * Adjust for home/neutral state if it is an enemy city
			 * in a home state!
			 */

			if(side != aiSide)		// Its an enemy or neutral town
			{
				Side stateSide = getStateSide(f);

				if(stateSide == aiSide)
					subPercent(miles, 40);
				else if(stateSide == SIDE_None)
					subPercent(miles, 20);
			}

			long newVal = (Vmax * K) / (miles + K) - K1;

			if(newVal > 0)
	  			cityValue += newVal;
		}
	}

	return cityValue;
}

/*
 * Calculate influence of a side's troops from a location
 *
 * side: Side of troops to influence location
 *
 * Surely should be based on a number of troops
 */

long calcNearUnitValue(const Location& where, Side side)
{
	/*
	 * For each Unit
	 * Increase priority based on distance from city
	 */

	long unitValue = 0;

	Unit* topUnit = campaign->world->ob.getPresident(side);
	UnitIter iter(topUnit, False, True, Rank_Army, Rank_Brigade);

	while(iter.ok())
	{
		Unit* u = iter.get();

		/*
		 * Constants used to calculate distance values
		 */

		const int K1 = 10;  								// Ignore values < 10%
		const int Vmax = 60 + K1;						// 60% at 0 miles
		const int D1 = 50;								// 10% at 50 miles
		const int V1 = 8 + K1;
		const int K = (D1 * V1) / (Vmax - V1);		// K = 15 Miles

		Unit* unit = iter.get();

		Distance d = distanceLocation(where, unit->location);
		int miles = unitToMile(d);

		long newVal = (Vmax * K) / (miles + K) - K1;

		if(newVal >= 0)
			unitValue += newVal * calc_UnitCV(u, True);
	}

	/*
	 * Reduce value to manageable range
	 */

	unitValue = (unitValue + 999) / 1000;

	return unitValue;
}

/*
 * Calculate how isolated a unit is
 *
 * Return   0 if completely isolated
 *        255 if with lots of friendly troops
 */

AIC_Priority calcNearPriority(const Location& where, Side side)
{
	/*
	 * For each Unit
	 * Increase priority based on distance from city
	 */

	Unit* topUnit = campaign->world->ob.getPresident(side);
	UnitIter iter(topUnit, False, True, Rank_Army, Rank_Brigade);

	CombatValue totalCV = 0;
	CombatValue localValue = 0;

	while(iter.ok())
	{
		Unit* unit = iter.get();

		CombatValue cv = calc_UnitCV(unit, True);
		totalCV += cv;

		int d = unitToMile(distanceLocation(where, unit->location));

		// Need to be careful not to overflow!

		const int MaxD = 150;				// 150 Miles away
		const int MaxD2 = MaxD * MaxD;

		if(d < MaxD)
		{
			// long value = cv - (cv * d * d) / MaxD2;

			long value = cv - (((cv * d) / MaxD) * d) / MaxD;

			if(value > 0)
				localValue += value;
		}
	}

	int ratio = (localValue * 256) / totalCV;

	if(ratio > 255)
		ratio = 255;

	return ratio;

}

/*
 * Adjust priority value depending on what type of town it is
 *
 * Also need to take into account:
 *		Size of city
 *		Whether it is a port
 *		Capital City
 *		Railway links
 *
 * Optional extras to consider:
 *		Fortifications
 *		Is it a City
 *		Is it a Capital City
 *		Is it defended by enemy troops?
 *		Size/Type of City
 *		Is it on the sea?
 *
 * side: Which side is considering attacking/defending (i.e. aiSide)
 */

enum AttackDefend {
	Attack,
	Defend
};

void adjustCityValue(long& value, const Facility* f, Side side, AttackDefend how)
{
	/*
	 * If a port or landing stage, then automatically add constant
	 * to value, depends if in enemy water zone or not.
	 *
	 * If waterzone occupied, then add even more!
	 *
	 * If defending and occupied by enemy then increase priority
	 * If attacking and occupied by us then increase priority
	 */

	if(f->waterZone != NoWaterZone)
	{
		WaterZone* z = &campaign->world->waterNet.zones[f->waterZone];

		long waterValue = 5;						// Start with 5 if waterzone

		// If its a sea zone, then more important

		if(z->type == WZ_Coast)
			waterValue += 10;

		Side waterSide;

		if(how == Attack)
			waterSide = side;
		else
			waterSide = otherSide(side);

		if(z->side == waterSide)
		{
			waterValue = waterValue * 3;				// Triple for enemy controlled

			if(z->boats.totalBoats())
			{
				waterValue += z->boats.totalBombardmentFactor() * 5;	// Probably should be multiplied by some constant
			}
		}

		if(f->facilities & Facility::F_City)
		{
			City* city = (City*)f;

			waterValue += 10;

			// waterValue += (waterValue * city->size) / 128;		// Upto 150%

			waterValue = waterValue / 2 + (waterValue * city->size) / 128;	// 50..100% of old value
		}

		/*
		 * Examples:
		 *		Coastal City size 63 being attacked by enemy strength 20
		 *			((5 + 10) * 3 + 100 + 10) * 3/2 = 232
		 *		Inland town in friendly zone:
		 *			5
		 *		Inland town in enemy zone:
		 *			5 * 3 = 15
		 *		Inland town in enemy zone of strength 20
		 *			5 * 3 + 100 = 115
		 */


		value += waterValue;
	}

	/*
	 * If on a railway line, then also add a fixed amount based on
	 * railhead capacity.
	 *
	 * Only do this if neighbours on enemy station.
	 */

	if(f->railCount)
	{
		RailSectionID rCount = f->railSection;
		int jCount = f->railCount;

		ASSERT(rCount != NoRailSection);

		while(jCount--)
		{
			FacilityID con = campaign->world->railways[rCount];
			ASSERT(con != NoFacility);
			Facility* dest = campaign->world->facilities[con];
			ASSERT(dest != 0);

			Boolean add;

			if(how == Attack)
				add = (dest->side != f->side);
			else	// assume defend
				add = (dest->side == otherSide(f->side));

			if(add)
				value += f->railCapacity / 5 + 10;

			rCount++;
		}
	}

#if 0
	/*
	 * Change this to add up a percentage and add on at end?
	 */

	if(f->facilities & Facility::F_City)
	{
		addPercent(value, 5);

		if(f->facilities & Facility::F_KeyCity)
			addPercent(value, 10);		// Another 10% for a key city

		City* city = (City*) f;

		addPercent(value, city->size / 3);		// Up to 20% for large city
	}

	if(f->facilities & Facility::F_SupplyDepot)
		addPercent(value, 3);
	if(f->facilities & Facility::F_RecruitmentCentre)
		addPercent(value, 2);
	if(f->facilities & Facility::F_TrainingCamp)
		addPercent(value, 1);
	if(f->facilities & Facility::F_Hospital)
		addPercent(value, 1);
	if(f->facilities & Facility::F_POW)
		addPercent(value, 1);

	if(f->facilities & Facility::F_Port)
		addPercent(value, 5);
	if(f->facilities & Facility::F_LandingStage)
		addPercent(value, 3);

	if(f->railCapacity)
		addPercent(value, (f->railCapacity / 10) + 1);

	if(f->fortification)
		addPercent(value, f->fortification * 1);
#else
	/*
	 * Change this to add up a percentage and add on at end?
	 */

	int percent = 0;

	if(f->facilities & Facility::F_City)
	{
		percent += 5;

		if(f->facilities & Facility::F_KeyCity)
			percent += 10;		// Another 10% for a key city

		City* city = (City*) f;

		percent += city->size / 3;		// Up to 20% for large city
	}

	if(f->facilities & Facility::F_SupplyDepot)
		percent += 3;
	if(f->facilities & Facility::F_RecruitmentCentre)
		percent += 2;
	if(f->facilities & Facility::F_TrainingCamp)
		percent +=  1;
	if(f->facilities & Facility::F_Hospital)
		percent +=  1;
	if(f->facilities & Facility::F_POW)
		percent +=  1;

	if(f->facilities & Facility::F_Port)
		percent +=  5;
	if(f->facilities & Facility::F_LandingStage)
		percent +=  3;

	if(f->railCapacity)
		percent +=  (f->railCapacity / 10) + 1;

	if(f->fortification)
		percent +=  f->fortification * 1;

	if(percent)
		addPercent(value, percent);

#endif

	//... supply level shouldn't be a consideration
	// addPercent(value, f->supplies / 30);

	//... Maybe consider its resource level?
	//... Victory points?

	/*
	 * If in home or neutral state, increase
	 */

	Side stateSide = getStateSide(f);

	if(how == Attack)
	{
		if(stateSide == side)
			addPercent(value, 40);
		else if(stateSide == SIDE_None)
			addPercent(value, 20);
	}
	else	// Defend
	{
		if(stateSide == side)
			addPercent(value, 25);
		else if(stateSide == SIDE_None)
			addPercent(value, 10);
	}
}

/*
 * Calculate the priority for attacking a city
 */

UBYTE CampaignAI::calcCityAttackValue(Facility* enemyCity)
{
	Side enemySide = otherSide(aiSide);

#if 0
	/*
	 * Sum up the distance from friendly cities
	 */

	long friendlyValue = calcNearCityValue(enemyCity, aiSide, aiSide);
	long enemyCityValue = calcNearCityValue(enemyCity, enemySide, aiSide);

	// Best places to attack will be where cityValue > enemyCityValue

	// long value = 128 + (cityValue - enemyCityValue) / 2;
	// long cityValue = 64 + friendlyValue / 3 - enemyCityValue / 2;
	long cityValue = friendlyValue - (enemyCityValue * friendlyValue) / 256;
#elif 0
	long cityValue = calcNearCityValue(enemyCity, aiSide, aiSide);
#else
	long friendlyValue = calcNearCityValue(enemyCity, aiSide, aiSide);
	long enemyValue = calcNearCityValue(enemyCity, enemySide, aiSide);

	long cityValue;

	if(friendlyValue)			// f+e can not be 0.
		cityValue = (friendlyValue * friendlyValue * 3) / (friendlyValue + enemyValue);
	else
		cityValue = 0;
#endif

	/*
	 * Closeness to enemy troops if in friendly state
	 */

	long value;

	long unitValue = calcNearUnitValue(enemyCity->location, enemySide);
	Side stateSide = getStateSide(enemyCity);
	if(stateSide == aiSide)
	{
		/*
		 * Combine values
		 *
		 * In friendly state, enemy troops increase likelihood of attacking
		 */

		value = (unitValue * cityValue) / 256 + 
							(unitValue * 128) / 256 +
							(cityValue * 64) / 256;
	}
	else
	{
		/*
		 * Enemy state... more likely to attack if NO troops (i.e. opportunity)
		 */

		// const int R = 2;					// Let zero unitValue add + 2*cityValue
		// const int K = (10 / (8-1));	// Reduce to 1/8 when unitValue=10
		const int R = 4;					// Let zero unitValue add + 4*cityValue
		const int K = (20 / (8-1));	// Reduce to 1/8 when unitValue=20
		 
		value = (cityValue + (R * K * cityValue) / (unitValue + K)) / 4;
	}


#ifdef DEBUG
	long value1 = value;
#endif

	adjustCityValue(value, enemyCity, aiSide, Attack);


#ifdef DEBUG
#if 1
	aiLog.printf("Attack %-28s: %4ld %4ld %4ld %4ld ==> %4ld",
		enemyCity->getNameNotNull(), friendlyValue, enemyValue, unitValue, value - value1, value);
#elif 0
	aiLog.printf("Attack %-28s: %4ld %4ld %4ld ==> %4ld",
		enemyCity->getNameNotNull(), cityValue, unitValue, value - value1, value);
#else
	aiLog.printf("Attack %-28s: %4ld %4ld %4ld %4ld ==> %4ld",
		enemyCity->getNameNotNull(), friendlyValue, enemyCityValue, unitValue, value - value1, value);
#endif
#endif
		
	if(value < 0)
		value = 0;
	else if(value > 255)
		value = 255;
		
	return value;


}

/*
 * Calculate priority to defend a town
 * Called for towns on our side
 */

UBYTE CampaignAI::calcCityDefendValue(Facility* homeCity)
{
	/*
	 * Precalculate a few useful values
	 */

	Side enemySide = otherSide(aiSide);

	/*
	 * Closeness to enemy facilities
	 * Note that this is opposite to attack calculation
	 * so that friendly cities near strong enemy cities are likely to
	 * be defended.
	 */

#if 1
	long enemyValue = calcNearCityValue(homeCity, enemySide, aiSide);
	long friendlyValue = calcNearCityValue(homeCity, aiSide, aiSide);

	long cityValue;
	if(enemyValue)
		cityValue = (enemyValue * enemyValue * 3) / (friendlyValue + enemyValue);
	else
		cityValue = 0;

#elif 0
	long cityValue = calcNearCityValue(homeCity, enemySide, aiSide);
#else
	long enemyValue = calcNearCityValue(homeCity, enemySide, aiSide);
	long friendlyValue = calcNearCityValue(homeCity, aiSide, aiSide);

	// long cityValue = 128 + (enemyValue - friendlyValue) / 2;

	long cityValue = enemyValue - (enemyValue * friendlyValue) / 256;

	if(cityValue < 0)
		cityValue = 0;
#endif

	/*
	 * Closeness to enemy troops
	 */

	long unitValue = calcNearUnitValue(homeCity->location, enemySide);

	/*
	 * Combine values
	 */

	long value = (unitValue * cityValue) / 256 + 
						(unitValue * 128) / 256 +
						(cityValue * 64) / 256;
#ifdef DEBUG
	long value1 = value;
#endif

	adjustCityValue(value, homeCity, aiSide, Defend);


#ifdef DEBUG
#if 1
	aiLog.printf("Defend %-28s: %4ld %4ld %4ld %4ld ==> %4ld",
		homeCity->getNameNotNull(), friendlyValue, enemyValue, unitValue, value - value1, value);
#elif 0
	aiLog.printf("Defend %-28s: %4ld %4ld %4ld ==> %4ld",
		homeCity->getNameNotNull(), cityValue, unitValue, value - value1, value);
#else
	aiLog.printf("Defend %-28s: %4ld %4ld %4ld %4ld ==> %4ld",
		homeCity->getNameNotNull(), enemyValue, friendlyValue, unitValue, value - value1, value);
#endif
#endif

	if(value > 255)
		value = 255;

	return value;

}


/*
 * Make priority for attacking defending cities
 *
 * For every town
 *   If it is friendly
 *     Calculate threat value and store in aiValue
 *   Else if it is enemy or neutral
 *     Calculate attack potential and store in aiValue
 *
 * These values are used later in aiProcUnits to decide what to do.
 */



void CampaignAI::aic_SetCityActions()
{
#ifdef DEBUG
	aiLog.printf("aic_SetCityActions()");

	MemPtr<char> popupBuffer(200);
	PopupText popup(0);
	int popCount = 0;
#endif

	Side enemySide = otherSide(aiSide);

	FacilityList& fList = campaign->world->facilities;

	for(int i = 0; i < fList.entries(); i++)
	{
#ifdef DEBUG
		if(popCount-- == 0)
		{
			sprintf(popupBuffer, "City: %d", i);
			popup.showText(popupBuffer);
			popCount = 9;
		}
#endif

		Facility* f = fList[i];

		if(!f->isRealTown())
			continue;

		/*
		 * If it's friendly...
		 */

		if(f->side == aiSide)
		{
			/*
			 * How much does it need defending?
			 */

			// How close to enemy Facility
			// How close to enemy Unit
			// Is it in enemy water zone (occupied?)
			// Modify with victory points

			/*
			 * Calculate aiCV as:
			 *		CV of any nearby enemy units
			 */

			AIC_Priority p = calcCityDefendValue(f);

			if(p > 0)
			{

				AIC_Event* event = events.add(p);

				event->action = AIA_DefendTown;
				event->where = f;
				event->enemyCV = calc_nearCV(f->location, Mile(50), enemySide);

				// ... enemyCV needs a better calculation based on:
				//			- distance of enemy units from town (for several days march)
				//			- Whether enemy units are marching towards town.
				//

				// Reduce enemy threat, because defenders are better than attackers

				percent(event->enemyCV, 70);

				// Enforce a minimum value

				if(event->enemyCV < 50)
					event->enemyCV = 50;

				event->friendlyCV = calc_townCV(f, False);
				// event->nearCV = calc_nearCV(f->location, Mile(250), aiSide);
				// event->nearPriority = calcNearUnitValue(f->location, aiSide);
				event->nearPriority = calcNearPriority(f->location, aiSide);

				f->aiDefend = p;
			}
			else
			{
				f->aiDefend = 0;
			}
		}
		else
		{
			AIC_Priority p = calcCityAttackValue(f);

			if(p > 0)
			{
				AIC_Event* event = events.add(p);

				event->action = AIA_AttackTown;
				event->where = f;
				event->enemyCV = calc_townCV(f, True);
				event->enemyCV += calc_nearCV(f->location, cityBattleDistance, enemySide);
				addPercent(event->enemyCV, 30);	// Over estimate the enemy
				event->friendlyCV = 0;		// This is recalculated during dishing out of orders
				// event->nearPriority = calcNearUnitValue(f->location, aiSide);
				event->nearPriority = calcNearPriority(f->location, aiSide);

				/*
				 * Are there enough nearby forces to go ahead with the attack?
				 */
				 
				event->canAttack = True;

				Facility* closeFacility = findCloseFacility(f, aiSide);
				if(closeFacility)
				{
					Distance d = distanceLocation(f->location, closeFacility->location) + cityWidth;
					if(d < involvedBattleDistance)
						d = involvedBattleDistance;

					CombatValue localCV = calc_nearCV(f->location, d, aiSide);
					CombatValue neededCV = (event->enemyCV * 3) / 4;

					if(localCV < neededCV)
					{
						event->closeFriend = closeFacility;
						event->canAttack = False;
					}
				}

				//... This needs updating to include:
				//			- Units in City
				//			- Units within a days march (with bias if marching)

#ifdef DEBUG
				f->aiAttack = p;
#endif
			}
#ifdef DEBUG
			else
			{
				f->aiAttack = 0;
			}
#endif
		}
	}
}


