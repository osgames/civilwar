/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Chunk File reading
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:24  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:18  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <sys/stat.h>
#include "wldfile.h"
#include "dfile.h"
#include "filesup.h"
#include "campaign.h"
#include "ai_camp.h"
#include "campwld.h"
#include "obfile.h"
#include "memptr.h"
#ifdef DEBUG
#include "memlog.h"
#endif


#ifdef DEBUG
#include "patchlog.h"
#endif

/*=============================================================
 * Some constants
 */

static const UWORD worldVersion = 0x0002;

static const UWORD CityVersion 		= 0x0000;	// Version 0.0
static const UWORD RailsVersion 		= 0x0000;	// Version 0.0
static const UWORD StatesVersion 	= 0x0000;	// Version 0.0
static const UWORD WaterVersion 		= 0x0002;	// Version 0.2
static const UWORD RailNetVersion	= 0x0001;		// Version 0.1
static const UWORD MobListVersion	= 0x0000;
static const UWORD WorldHeadVersion	= 0x0002;
static const UWORD CampaignBattleVersion = 0x0002;		// 0.2

static const ChunkID StaticFacilityID 	= 'SFAC';
static const ChunkID DynamicFacilityID = 'DFAC';
static const ChunkID RailwaySectionID 	= 'RAIL';
static const ChunkID StaticStateID 		= 'SSTA';
static const ChunkID DynamicStateID 	= 'DSTA';
static const ChunkID StaticWaterID 		= 'SWAT';
static const ChunkID DynamicWaterID 	= 'DWAT';
static const ChunkID DynamicRailID 		= 'DRAL';
static const ChunkID MobiliseListID 	= 'MOBL';
static const ChunkID WorldHeaderID		= 'DWLD';
static const ChunkID CampaignBattleID	= 'CBAT';
static const ChunkID CampaignTempID		= 'CTMP';

/*=============================================================
 * Cities and Facilities
 */

/*=============================================================
 * Define Data structures
 */

/*
 * Header chunk for cities
 */

struct D_City_H {
	IWORD count;
	IWORD version;
};

struct D_Rail_H {
	IWORD count;
	IWORD version;
};

struct D_State_H {
	IWORD count;
	IWORD version;
};


/*
 * An individual facility
 * Note that this contains enough info for both cities and facilities
 * Commented out items do not need to be loaded
 */

struct D_StaticFacility {
	D_Location location;
	IWORD owner;
	// IBYTE name[MaxFacilityName];
	IWORD facilities;
	IBYTE state;
	IBYTE side;
	IBYTE fortification;
	IBYTE supply;
	// IBYTE attacking;	... only in dynamic
	// IBYTE reinforcing; ... only in dynamic
	// IBYTE AISkipFlag; ... only in dynamic
	// UnitID Occupier... filled in later after OB is loaded
	// UnitID Sieger... filled in after OB is loaded
	IBYTE railCapacity;
	IBYTE freeRailCapacity;
	IBYTE railCount;
	IWORD railSection;
	IBYTE waterCapacity;
	IBYTE freeWaterCapacity;
	IBYTE blockadeRunners;
	IWORD waterZone;
	IBYTE victory;
	IWORD resources[ResourceCount];
	IBYTE cityType;
	IBYTE citySize;
};

struct D_DynamicFacility {
	// IWORD owner;
	// IBYTE name[MaxFacilityName];
	IWORD facilites;
	// IBYTE state;
	IBYTE side;
	IBYTE fortification;
	IBYTE supply;
#if 1		// To be moved into an old version structure
// #ifdef CHRIS_AI
	IBYTE attacking;
	IBYTE reinforcing;
	IBYTE AISkipFlag;
#endif
	// UnitID Occupier... filled in later after OB is loaded
	// UnitID Sieger... filled in after OB is loaded
	IBYTE railCapacity;
	IBYTE freeRailCapacity;
	// IBYTE railCount;
	// IWORD railSection;
	IBYTE waterCapacity;
	IBYTE freeWaterCapacity;
	IBYTE blockadeRunners;
	// IWORD waterZone;
	// IBYTE victory;
	IWORD resources[ResourceCount];
	// IBYTE cityType;
	// IBYTE citySize;
};

void writeStaticFacilities(ChunkFileWrite& file, FacilityList& facilities)
{
	file.startWriteChunk(StaticFacilityID);

	/*
	 * Sort out the header
	 */

	D_City_H header;

	putWord(&header.count, facilities.entries());
	putWord(&header.version, CityVersion);
	file.write(&header, sizeof(header));

#ifdef CAMPEDIT
	UWORD railSection = 0;
#endif

	/*
	 * Write all the facilities
	 */

	for(int i = 0; i < facilities.entries(); i++)
	{
		Facility* f = facilities[i];
		City* c = 0;

		if(f->facilities & Facility::F_City)
			c = (City*) f;

		/*
		 * fill in the data
		 */

		D_StaticFacility data;
		memset(&data, 0, sizeof(data));

		putLocation(&data.location, f->location);
		putWord(&data.owner, f->owner);
		// putName(data.name, f->name, MaxFacilityName);
		putWord(&data.facilities, f->facilities);
		putByte(&data.state, f->state);
		putByte(&data.side, f->side);
		putByte(&data.fortification, f->fortification);
		putByte(&data.supply, f->supplies);
		// putByte attacking;	... only in dynamic
		// putByte reinforcing; ... only in dynamic
		// putByte AISkipFlag; ... only in dynamic
		// UnitID Occupier... filled in later after OB is loaded
		// UnitID Sieger... filled in after OB is loaded
#ifdef CAMPEDIT
		f->freeRailCapacity = f->railCapacity;
		f->freeWaterCapacity = f->waterCapacity;
#endif

		putByte(&data.railCapacity, f->railCapacity);
		putByte(&data.freeRailCapacity, f->freeRailCapacity);
#ifdef CAMPEDIT
		putByte(&data.railCount, f->connections.entries());
		if(f->connections.entries())
			putWord(&data.railSection, railSection);
		else
			putWord(&data.railSection, NoRailSection);
		railSection += f->connections.entries();
#else
		putByte(&data.railCount, f->railCount);
		putWord(&data.railSection, f->railSection);
#endif
		putByte(&data.waterCapacity, f->waterCapacity);
		putByte(&data.freeWaterCapacity, f->freeWaterCapacity);
		putByte(&data.blockadeRunners, f->blockadeRunners);
		putWord(&data.waterZone, f->waterZone);
		putByte(&data.victory, f->getVictory());
		if(c)
		{
			for(int r = 0; r < ResourceCount; r++)
				putWord(&data.resources[r], c->resource.values[r]);
			putByte(&data.cityType, c->type);
			putByte(&data.citySize, c->size);
		}

		/*
		 * Write it
		 */

		file.write(&data, sizeof(data));
		file.writeString(f->name);
	}



	file.endWriteChunk();
}

void writeDynamicFacilities(ChunkFileWrite& file, FacilityList& facilities)
{
	file.startWriteChunk(DynamicFacilityID);

	/*
	 * Sort out the header
	 */

	D_City_H header;

	putWord(&header.count, facilities.entries());
	putWord(&header.version, CityVersion);
	file.write(&header, sizeof(header));

	/*
	 * Write all the facilities
	 */

	for(int i = 0; i < facilities.entries(); i++)
	{
		Facility* f = facilities[i];
		City* c = 0;

		if(f->facilities & Facility::F_City)
			c = (City*) f;

		/*
		 * fill in the data
		 */

		D_DynamicFacility data;
		memset(&data, 0, sizeof(data));

		putWord(&data.facilites, f->facilities);
		putByte(&data.side, f->side);
		putByte(&data.fortification, f->fortification);
		putByte(&data.supply, f->supplies);
#ifdef CHRIS_AI
		putByte(&data.attacking, f->attacking);
		putByte(&data.reinforcing, f->reinforcing);
		putByte(&data.AISkipFlag, f->AISkipFlag);
#endif
		// UnitID Occupier... filled in later after OB is loaded
		// UnitID Sieger... filled in after OB is loaded
		putByte(&data.railCapacity, f->railCapacity);
		putByte(&data.freeRailCapacity, f->freeRailCapacity);
		putByte(&data.waterCapacity, f->waterCapacity);
		putByte(&data.freeWaterCapacity, f->freeWaterCapacity);
		putByte(&data.blockadeRunners, f->blockadeRunners);
		if(c)
		{
			for(int r = 0; r < ResourceCount; r++)
				putWord(&data.resources[r], c->resource.values[r]);
		}

		/*
		 * Write it
		 */

		file.write(&data, sizeof(data));
	}



	file.endWriteChunk();
}


/*
 * Write the Railway section list
 */

#ifdef CAMPEDIT

void writeRailSections(ChunkFileWrite& file, CampaignWorld* world)
{
	file.startWriteChunk(RailwaySectionID);

	/*
	 * Count the entries
	 */

	UWORD railCount = 0;
	for(int i = 0; i < world->facilities.entries(); i++)
	{				  	
		Facility* f = world->facilities[i];
		railCount += f->connections.entries();
	}


	/*
	 * Sort out the header
	 */

	D_Rail_H header;

	putWord(&header.count, railCount);
	putWord(&header.version, RailsVersion);
	file.write(&header, sizeof(header));

	/*
	 * Write the data
	 */

	for(i = 0; i < world->facilities.entries(); i++)
	{				  	
		Facility* f = world->facilities[i];

		for(int j = 0; j < f->connections.entries(); j++)
		{
			IWORD data;

			putWord(&data, f->connections[j]);
			file.write(&data, sizeof(data));
		}
	}

	file.endWriteChunk();
}

#else

void writeRailSections(ChunkFileWrite& file, RailSectionList& rails)
{
	file.startWriteChunk(RailwaySectionID);

	/*
	 * Sort out the header
	 */

	D_Rail_H header;

	putWord(&header.count, rails.entries());
	putWord(&header.version, RailsVersion);
	file.write(&header, sizeof(header));

	/*
	 * Write the data
	 */

	for(int i = 0; i < rails.entries(); i++)
	{
		IWORD data;

		putWord(&data, rails[i]);
		file.write(&data, sizeof(data));
	}

	file.endWriteChunk();
}
#endif

void readFacilities(ChunkFileRead& file, FacilityList& facilities)
{
	if(file.startReadChunk(StaticFacilityID) == CE_OK)
	{
		D_City_H header;
		file.read(&header, sizeof(header));
		UWORD count = getWord(&header.count);

		facilities.init(count);

		for(int i = 0; i < count; i++)
		{
			D_StaticFacility data;

			file.read(&data, sizeof(data));

			Facility* f = 0;
			City* c = 0;

			UWORD flags = getWord(&data.facilities);

			if(flags & Facility::F_City)
			{
				c = new City;
				f = c;
			}
			else
				f = new Facility;

			facilities[i] = f;

			getLocation(&data.location, f->location);
			f->owner = getWord(&data.owner);
			f->facilities = flags;
			f->state = getByte(&data.state);
			f->side = Side(getByte(&data.side));
			f->fortification = getByte(&data.fortification);
			ASSERT(f->fortification <= MaxFortification);
			f->supplies = getByte(&data.supply);
			f->railCapacity = getByte(&data.railCapacity);
			f->freeRailCapacity = getByte(&data.freeRailCapacity);
			ASSERT(f->railCapacity <= MaxRailHead);
			ASSERT(f->freeRailCapacity <= MaxRailHead);
#ifdef CAMPEDIT
			f->connections.init(getByte(&data.railCount));
#else
			f->railCount = getByte(&data.railCount);
			f->railSection = getWord(&data.railSection);
#endif
			f->waterCapacity = getByte(&data.waterCapacity);
			f->freeWaterCapacity = getByte(&data.freeWaterCapacity);

			f->blockadeRunners = getByte(&data.blockadeRunners);
			ASSERT(f->blockadeRunners <= MaxBlockade);
			f->waterZone = getWord(&data.waterZone);
			f->setVictory(getByte(&data.victory));
			if(c)
			{
				for(int r = 0; r < ResourceCount; r++)
					c->resource.values[r] = getWord(&data.resources[r]);

#if !defined(CAMPEDIT)
				c->resource.adjust();
#endif

				c->type = getByte(&data.cityType);
				c->size = getByte(&data.citySize);
			}
#ifdef CAMPEDIT
			file.readString(f->name);
			if(!f->name[0])
			{
				f->side = SIDE_None;
				f->setVictory(0);
			}
#else
			f->name = file.readString();

			/*
			 * Force Railhalts to be neutral.
			 */

			if(!f->name)
			{
				f->side = SIDE_None;
				f->setVictory(0);
			}
#endif
#if 0	// defined(PATCHLOG)
			patchLog.printf("Fortification at %s = %d",
					(const char*) f->name,
					(int) newFort);
#endif
		}
	}
}

void updateFacilities(ChunkFileRead& file, FacilityList& facilities)
{
	if(file.startReadChunk(DynamicFacilityID) == CE_OK)
	{
		D_City_H header;
		file.read(&header, sizeof(header));
		UWORD count = getWord(&header.count);

#ifdef DEBUG
		if(count != facilities.entries())
			throw GeneralError("There are %d facilities instead of %d",
				(int)count, (int)facilities.entries());
#endif

		for(int i = 0; i < count; i++)
		{
			D_DynamicFacility data;

			file.read(&data, sizeof(data));

			Facility* f = facilities[i];
			City* c = 0;

			if(f->facilities & Facility::F_City)
				c = (City*) f;

			f->facilities = getWord(&data.facilites);
			f->side = Side(getByte(&data.side));

			UBYTE newFort = getByte(&data.fortification);
			if(newFort > MaxFortification)
			{
				newFort = MaxFortification;
#ifdef PATCHLOG
				patchLog.printf("Illegal fortification %d patched to %d",
					(int) getByte(&data.fortification),
					(int) newFort);
#endif
			}
			f->fortification = newFort;
			ASSERT(f->fortification <= MaxFortification);

			f->supplies = getByte(&data.supply);
#ifdef CHRIS_AI
			f->attacking = getByte(&data.attacking);
			f->reinforcing = getByte(&data.reinforcing);
			f->AISkipFlag = getByte(&data.AISkipFlag);
#endif
			UBYTE newRail = getByte(&data.railCapacity);
			if(newRail > MaxRailHead)
			{
				newRail = MaxRailHead;
#ifdef PATCHLOG
				patchLog.printf("Railhead patched from %d to %d",
					(int) getByte(&data.railCapacity),
					(int) newRail);
#endif
			}
			f->railCapacity = newRail;
			f->freeRailCapacity = getByte(&data.freeRailCapacity);

			ASSERT(f->railCapacity <= MaxRailHead);
			ASSERT(f->freeRailCapacity <= MaxRailHead);

			f->waterCapacity = getByte(&data.waterCapacity);
			f->freeWaterCapacity = getByte(&data.freeWaterCapacity);
			UBYTE newBlockade = getByte(&data.blockadeRunners);
			if(newBlockade > MaxBlockade)
			{
				newBlockade = MaxBlockade;
#ifdef PATCHLOG
				patchLog.printf("Blockades patched from %d to %d",
					(int) getByte(&data.blockadeRunners),
					(int) newBlockade);
#endif
			}
			f->blockadeRunners = newBlockade;

			ASSERT(f->blockadeRunners <= MaxBlockade);

			if(c)
			{
				for(int r = 0; r < ResourceCount; r++)
					c->resource.values[r] = getWord(&data.resources[r]);

#if !defined(CAMPEDIT)
				c->resource.adjust();
#endif
			}
		}
	}
}

#ifdef CAMPEDIT

void readRailSections(ChunkFileRead& file, CampaignWorld* world)
{
	if(file.startReadChunk(RailwaySectionID) == CE_OK)
	{
		D_Rail_H header;
		file.read(&header, sizeof(header));
		UWORD count = getWord(&header.count);

		RailSectionList rails;
		rails.init(count);

		for(int i = 0; i < count; i++)
		{
			IWORD data;

			file.read(&data, sizeof(data));
			rails[i] = getWord(&data);
		}

		/*
		 * Go through facility list and copy into them
		 * The connections array will already have been initialised
		 */

		int nextRail = 0;
		for(i = 0; i < world->facilities.entries(); i++)
		{
			Facility* f = world->facilities[i];

			for(int j = 0; j < f->connections.entries(); j++)
				f->connections[j] = rails[nextRail++];
		}
	}
}

#else

void readRailSections(ChunkFileRead& file, RailSectionList& rails)
{
	if(file.startReadChunk(RailwaySectionID) == CE_OK)
	{
		D_Rail_H header;
		file.read(&header, sizeof(header));
		UWORD count = getWord(&header.count);

		rails.init(count);

		for(int i = 0; i < count; i++)
		{
			IWORD data;

			file.read(&data, sizeof(data));
			rails[i] = getWord(&data);
		}
	}
}
#endif

/*=======================================
 * States
 */

struct D_StaticState {
	D_Location location;
	IBYTE side;
	IBYTE regimentCount;
};


struct D_DynamicState {
	IBYTE regimentCount;
};

void writeStaticStates(ChunkFileWrite& file, StateList& states)
{
	file.startWriteChunk(StaticStateID);

	/*
	 * Sort out the header
	 */

	D_State_H header;

	putWord(&header.count, states.entries());
	putWord(&header.version, StatesVersion);
	file.write(&header, sizeof(header));

	for(int i = 0; i < states.entries(); i++)
	{
		D_StaticState data;
		memset(&data, 0, sizeof(data));

		State* state = &states[i];

		putLocation(&data.location, state->location);
		putByte(&data.side, state->side);
		putByte(&data.regimentCount, state->regimentCount);

		file.write(&data, sizeof(data));
	  
		file.writeString(state->fullName);
		file.writeString(state->shortName);
	}


	file.endWriteChunk();
}

void writeDynamicStates(ChunkFileWrite& file, StateList& states)
{
	file.startWriteChunk(DynamicStateID);
	/*
	 * Sort out the header
	 */

	D_State_H header;

	putWord(&header.count, states.entries());
	putWord(&header.version, StatesVersion);
	file.write(&header, sizeof(header));

	for(int i = 0; i < states.entries(); i++)
	{
		D_DynamicState data;
		memset(&data, 0, sizeof(data));

		State* state = &states[i];

		putByte(&data.regimentCount, state->regimentCount);

		file.write(&data, sizeof(data));
	}

	file.endWriteChunk();
}

/*
 * Read static states
 */

void readStates(ChunkFileRead& file, StateList& states)
{
	if(file.startReadChunk(StaticStateID) == CE_OK)
	{
		/*
		 * Read the header
		 */

		D_State_H header;
		file.read(&header, sizeof(header));

		UWORD nStates = getWord(&header.count);
		states.init(nStates);

		for(int i = 0; i < nStates; i++)
		{
			D_StaticState data;
			State& state = states[i];

			file.read(&data, sizeof(data));

			getLocation(&data.location, state.location);
			state.side = Side(getByte(&data.side));
			state.regimentCount = getByte(&data.regimentCount);

#ifdef CAMPEDIT
			file.readString(state.fullName);
			file.readString(state.shortName);
#else
			state.fullName = file.readString();
			state.shortName = file.readString();
#endif
		}
	}
}

/*
 * Update states from dynamic state chunk
 */

void updateStates(ChunkFileRead& file, StateList& states)
{
	if(file.startReadChunk(DynamicStateID) == CE_OK)
	{
		/*
		 * Read the header
		 */

		D_State_H header;
		file.read(&header, sizeof(header));

		UWORD nStates = getWord(&header.count);
		
#ifdef DEBUG
		if(nStates != states.entries())
			throw GeneralError("file has %d states instead of %d",
				(int)nStates, (int)states.entries());
#endif

		for(int i = 0; i < nStates; i++)
		{
			State& state = states[i];
			D_DynamicState data;

			file.read(&data, sizeof(data));

			state.regimentCount = getByte(&data.regimentCount);
		}
	}
}

/*===============================================
 * Water Network
 */

struct D_Water_H {
	IWORD version;
	IWORD nFacilities;
	IWORD nConnects;
	IWORD nZones;
};

struct D_DynamicWater_H_00 {
	// IWORD version;
	IWORD nZones;
	IWORD nFleets;
};

struct D_DynamicWater_H {
	// IWORD version;
	IWORD nZones;
	IWORD nFleets;
	IWORD nFerries;
};


struct D_StaticWater {
	D_Location location;
	IBYTE boats[Nav_TypeCount];
	// fleets
	IBYTE side;
	IBYTE type;
	IBYTE connectCount;
	IWORD connections;
	IBYTE facilityCount;
	IWORD facilityList;
};

struct D_DynamicWater {
	IBYTE boats[Nav_TypeCount];
	IBYTE fleetCount;		// Number of fleets
	IWORD fleetList;		// Entry to fleet list
	IBYTE side;
};

struct D_Fleet {
	IBYTE boats[Nav_TypeCount];
	IWORD where;
	IWORD destination;
	IWORD via;
	ILONG timeDay;
	ILONG timeTicks;
	IBYTE side;
};

struct D_Ferry {
	IWORD lastZone;
	IWORD nextZone;
	IWORD destination;
	D_GameTime arrival;
	IBYTE side;
	ILONG passenger;
	IBYTE mode;
};

struct D_Ferry_01 {
	IWORD lastZone;
	IWORD nextZone;
	IWORD destination;
	D_GameTime arrival;
	IBYTE side;
	IWORD passenger;
	IBYTE mode;
};


void writeStaticWater(ChunkFileWrite& file, WaterNetwork& water)
{
	file.startWriteChunk(StaticWaterID);

#ifdef CAMPEDIT
	UWORD facilityCount = 0;
	UWORD connectCount = 0;

	for(int i = 0; i < water.zones.entries(); i++)
	{
		WaterZone* zone = &water.zones[i];

		facilityCount += zone->facilityList.entries();
		connectCount += zone->connections.entries();
	}
#endif

	/*
	 * Sort out the header
	 */

	D_Water_H header;

	putWord(&header.version, WaterVersion);
#ifdef CAMPEDIT
	putWord(&header.nFacilities, facilityCount);
	putWord(&header.nConnects, connectCount);
#else
	putWord(&header.nFacilities, water.facilityList.entries());
	putWord(&header.nConnects, water.connectList.entries());
#endif
	putWord(&header.nZones, water.zones.entries());

	file.write(&header, sizeof(header));

#ifdef CAMPEDIT

	/*
	 * Put the facilities
	 */

	for(i = 0; i < water.zones.entries(); i++)
	{
		WaterZone* zone = &water.zones[i];

		for(int j = 0; j < zone->facilityList.entries(); j++)
		{
			IWORD data;
			putWord(&data, zone->facilityList[j]);
			file.write(&data, sizeof(data));
		}
	}

	/*
	 * Connections
	 */

	for(i = 0; i < water.zones.entries(); i++)
	{
		WaterZone* zone = &water.zones[i];

		for(int j = 0; j < zone->connections.entries(); j++)
		{
			IWORD data;
			putWord(&data, zone->connections[j]);
			file.write(&data, sizeof(data));
		}
	}

#else

	/*
	 * Put the facilities
	 */

	for(int i = 0; i < water.facilityList.entries(); i++)
	{
		IWORD data;
		putWord(&data, water.facilityList[i]);
		file.write(&data, sizeof(data));
	}

	/*
	 * Put the connections
	 */

	for(i = 0; i < water.connectList.entries(); i++)
	{
		IWORD data;
		putWord(&data, water.connectList[i]);
		file.write(&data, sizeof(data));
	}
#endif

	/*
	 * Put the zones
	 */

#ifdef CAMPEDIT
	UWORD nextConnect = 0;
	UWORD nextFacility = 0;
#endif

	for(i = 0; i < water.zones.entries(); i++)
	{
		D_StaticWater data;
		memset(&data, 0, sizeof(data));

		WaterZone* zone = &water.zones[i];

		putLocation(&data.location, zone->location);
		for(int b = 0; b < Nav_TypeCount; b++)
			putByte(&data.boats[b], zone->boats.count[b]);
		putByte(&data.side, zone->side);
		putByte(&data.type, zone->type);
#ifdef CAMPEDIT
		putByte(&data.connectCount, zone->connections.entries());
		putWord(&data.connections, nextConnect);
		nextConnect += zone->connections.entries();
		putByte(&data.facilityCount, zone->facilityList.entries());
		putWord(&data.facilityList, nextFacility);
		nextFacility += zone->facilityList.entries();
#else
		putByte(&data.connectCount, zone->connectCount);
		putWord(&data.connections, zone->connections);
		putByte(&data.facilityCount, zone->facilityCount);
		putWord(&data.facilityList, zone->facilityList);
#endif

		file.write(&data, sizeof(data));
	}



	file.endWriteChunk();
}

void writeDynamicWater(ChunkFileWrite& file, WaterNetwork& water, const OrderBattle& ob)
{
	file.startWriteChunk(DynamicWaterID);

	/*
	 * Count the fleets
	 */

	UWORD nFleets = 0;

	for(int i = 0; i < water.zones.entries(); i++)
	{
		WaterZone* zone = &water.zones[i];

		nFleets += zone->fleets.entries();
	}

	UWORD nFerries = 0;
	Ferry* ferry = water.ferries;
	while(ferry)
	{
		nFerries++;
		ferry = ferry->next;
	}


	/*
	 * Sort out the header
	 */

	IWORD iVersion;
	putWord(&iVersion, WaterVersion);
	file.write(&iVersion, sizeof(iVersion));

	D_DynamicWater_H header;
	// putWord(&header.version, WaterVersion);
	putWord(&header.nZones, water.zones.entries());
	putWord(&header.nFleets, nFleets);
	putWord(&header.nFerries, nFerries);
	file.write(&header, sizeof(header));

	/*
	 * Put the zones
	 */

	UWORD nextFleet = 0;

	for(i = 0; i < water.zones.entries(); i++)
	{
		D_DynamicWater data;
		memset(&data, 0, sizeof(data));

		WaterZone* zone = &water.zones[i];


		for(int b = 0; b < Nav_TypeCount; b++)
			putByte(&data.boats[b], zone->boats.count[b]);

		UBYTE nFleets = zone->fleets.entries();
		putByte(&data.fleetCount, nFleets);
		putWord(&data.fleetList, nextFleet);
		nextFleet += nFleets;

		putByte(&data.side, zone->side);

		file.write(&data, sizeof(data));
	}

	/*
	 * Put the fleets
	 */

	for(i = 0; i < water.zones.entries(); i++)
	{
		WaterZone* zone = &water.zones[i];

		FleetIter flist = zone->fleets;
		Fleet* fleet;
		while( (fleet = flist.next()) != 0)
		{
			D_Fleet data;
			memset(&data, 0, sizeof(data));

			for(int b = 0; b < Nav_TypeCount; b++)
				putByte(&data.boats[b], fleet->boats.count[b]);
			putWord(&data.where, fleet->where);
			putWord(&data.destination, fleet->destination);
			putWord(&data.via, fleet->via);
			putLong(&data.timeDay, fleet->arrival.days);
			putLong(&data.timeTicks, fleet->arrival.ticks);
			putByte(&data.side, fleet->side);

			file.write(&data, sizeof(data));
		}
	}

	/*
	 * Ferries
	 */

	ferry = water.ferries;
	for(i = 0; i < nFerries; i++)
	{
		D_Ferry data;

		putWord(&data.lastZone, ferry->lastZone);
		putWord(&data.nextZone, ferry->nextZone);
		putWord(&data.destination, ferry->destination);
		putGameTime(&data.arrival, ferry->arrival);
		putByte(&data.side, ferry->side);
		putLong(&data.passenger, ob.getID(ferry->passenger));
		putByte(&data.mode, ferry->mode);

		file.write(&data, sizeof(data));

		ferry = ferry->next;
	}

	file.endWriteChunk();
}

void readWater(ChunkFileRead& file, WaterNetwork& water)
{
	if(file.startReadChunk(StaticWaterID) == CE_OK)
	{
		D_Water_H header;

		file.read(&header, sizeof(header));

		UWORD nFacilities = getWord(&header.nFacilities);
		UWORD nConnects = getWord(&header.nConnects);
		UWORD nZones = getWord(&header.nZones);

#if !defined(CAMPEDIT)
		water.facilityList.init(nFacilities);
		water.connectList.init(nConnects);
#endif
		water.zones.init(nZones);

#ifdef CAMPEDIT
		Array<FacilityID> facilityList;	// List of facility IDs used by river/coast
		Array<WaterZoneID> connectList;	// List of connections

		facilityList.init(nFacilities);
		connectList.init(nConnects);

		for(int i = 0; i < nFacilities; i++)
		{
			IWORD data;
			file.read(&data, sizeof(data));
			facilityList[i] = getWord(&data);
		}

		for(i = 0; i < nConnects; i++)
		{
			IWORD data;
			file.read(&data, sizeof(data));
			connectList[i] = getWord(&data);
		}

#else

		for(int i = 0; i < nFacilities; i++)
		{
			IWORD data;
			file.read(&data, sizeof(data));
			water.facilityList[i] = getWord(&data);
		}

		for(i = 0; i < nConnects; i++)
		{
			IWORD data;
			file.read(&data, sizeof(data));
			water.connectList[i] = getWord(&data);
		}
#endif

		for(i = 0; i < nZones; i++)
		{
			D_StaticWater data;

			file.read(&data, sizeof(data));

			WaterZone* zone = &water.zones[i];

			getLocation(&data.location, zone->location);
			for(int b = 0; b < Nav_TypeCount; b++)
				zone->boats.count[b] = getByte(&data.boats[b]);
			zone->side = Side(getByte(&data.side));
			zone->type = WaterZoneType(getByte(&data.type));
#ifdef CAMPEDIT
			UBYTE count = getByte(&data.connectCount);
			UWORD k = getWord(&data.connections); 
			zone->connections.init(count);
			for(int j = 0; j < count; j++)
				zone->connections[j] = connectList[k++];
			count = getByte(&data.facilityCount);
			zone->facilityList.init(count);
			k = getWord(&data.facilityList);
			for(j = 0; j < count; j++)
				zone->facilityList[j] = facilityList[k++];

#else
			zone->connectCount = getByte(&data.connectCount);
			zone->connections = getWord(&data.connections);
			zone->facilityCount = getByte(&data.facilityCount);
			zone->facilityList = getWord(&data.facilityList);
#endif
		}

	}
}

void updateWater(ChunkFileRead& file, WaterNetwork& water, const OrderBattle& ob)
{
	if(file.startReadChunk(DynamicWaterID) == CE_OK)
	{
		IWORD iVersion;

		file.read(&iVersion, sizeof(iVersion));

		UWORD version = getWord(&iVersion);

		UWORD nZones;
		UWORD nFleets;
		UWORD nFerries;

		if(version >= 0x01)
		{
			D_DynamicWater_H header;

			file.read(&header, sizeof(header));

			nZones = getWord(&header.nZones);
			nFleets = getWord(&header.nFleets);
			nFerries = getWord(&header.nFerries);
		}
		else
		{
			D_DynamicWater_H_00 header;

			file.read(&header, sizeof(header));

			nZones = getWord(&header.nZones);
			nFleets = getWord(&header.nFleets);
			nFerries = 0;
		}


#ifdef DEBUG
		if(nZones != water.zones.entries())
			throw GeneralError("%d zones in file instead of %d",
				(int) nZones, (int) water.zones.entries());
#endif

		struct FleetStore {
			UBYTE fleetCount;
			UWORD fleetList;
		};

		FleetStore* fleetInfo = new FleetStore[nZones];

		for(int i = 0; i < nZones; i++)
		{
			D_DynamicWater data;

			file.read(&data, sizeof(data));

			WaterZone* zone = &water.zones[i];

			for(int b = 0; b < Nav_TypeCount; b++)
				zone->boats.count[b] = getByte(&data.boats[b]);

			zone->side = Side(getByte(&data.side));

			fleetInfo[i].fleetCount = getByte(&data.fleetCount);
			fleetInfo[i].fleetList = getWord(&data.fleetList);
		}

		/*
		 * Read the fleets
		 */

		int z = 0;		// Start with 1st water zone

		for(i = 0; i < nFleets; i++)
		{
			D_Fleet data;

			file.read(&data, sizeof(data));

			Fleet* fleet = new Fleet;

			for(int b = 0; b < Nav_TypeCount; b++)
				fleet->boats.count[b] = getByte(&data.boats[b]);
			fleet->where = getWord(&data.where);
			fleet->destination = getWord(&data.destination);
			fleet->via = getWord(&data.via);
			fleet->arrival.days = getLong(&data.timeDay);
			fleet->arrival.ticks = getLong(&data.timeTicks);
			fleet->side = Side(getByte(&data.side));

			/*
			 * Attach it to a waterzone
			 */

			while(i >= (fleetInfo[z].fleetList + fleetInfo[z].fleetCount))
			{
				z++;
#ifdef DEBUG
				if(z >= nZones)
					throw GeneralError("Too many fleets in file");
#endif
			}

			water.zones[z].fleets.add(fleet);
		}

		delete[] fleetInfo;

		/*
		 * Read ferries
		 */

		for(i = 0; i < nFerries; i++)
		{
			if(version >= 0x02)
			{
				D_Ferry data;
				file.read(&data, sizeof(data));

#if !defined(CAMPEDIT)
				Ferry* ferry = new Ferry;

				ferry->lastZone = getWord(&data.lastZone);
				ferry->nextZone = getWord(&data.nextZone);
				ferry->destination = getWord(&data.destination);
				getGameTime(&data.arrival, ferry->arrival);
				ferry->side = Side(getByte(&data.side));
				ferry->passenger = ob.getUnitPtr(getLong(&data.passenger));
				ferry->mode = Ferry::FerryMode(getByte(&data.mode));

				// Patch... 7th February 1996
				// ASSERT(ferry->passenger != 0);

				if(ferry->passenger != 0)
					water.addFerry(ferry);
				else
				{
#ifdef PATCHLOG
					patchLog.printf("Patch 1.1.6, Ferry with no passengers");
#endif
					delete ferry;
				}
				//.... end of patch
#endif	// CAMPEDIT
			}
			else
			{
				D_Ferry_01 data;
				file.read(&data, sizeof(data));

#if !defined(CAMPEDIT)
				Ferry* ferry = new Ferry;

				ferry->lastZone = getWord(&data.lastZone);
				ferry->nextZone = getWord(&data.nextZone);
				ferry->destination = getWord(&data.destination);
				getGameTime(&data.arrival, ferry->arrival);
				ferry->side = Side(getByte(&data.side));
				ferry->passenger = ob.getUnitPtr(getWord(&data.passenger));
				ferry->mode = Ferry::FerryMode(getByte(&data.mode));

				// Patch... 7th February 1996
				// ASSERT(ferry->passenger != 0);

				if(ferry->passenger != 0)
					water.addFerry(ferry);
				else
				{
#ifdef PATCHLOG
					patchLog.printf("Patch 1.1.6, Ferry with no passengers");
#endif
					delete ferry;
				}
				//.... end of patch
#endif		// CAMPEDIT
			}
		}
	}
}


/*=============================================================
 * Railway Network
 */

struct D_RailHead {
	IWORD version;
	IWORD nTrains;
	IWORD nPlatforms;
	D_GameTime lastTime;
};

struct D_Train {
	IWORD owner;
	IWORD lastStation;
	IWORD nextStation;
	ILONG spaces;
	IBYTE side;
	IBYTE mode;
	D_GameTime arrival;
	IWORD nPassengers;
	// Followed by list of UnitID for passengers
};

struct D_Platform {
	IWORD station;
	IWORD destination;
	IBYTE side;
	IWORD nWaiting;
	// Followed by UnitID[] for waiting units
};

void writeDynamicRail(ChunkFileWrite& file, RailNetwork& rails, const OrderBattle& ob)
{
	file.startWriteChunk(DynamicRailID);

	UWORD nTrains = 0;
	Train* train = rails.trains;
	while(train)
	{
		nTrains++;
		train = train->next;
	}

	UWORD nPlatforms = 0;
	Platform* platform = rails.platforms;
	while(platform)
	{
		nPlatforms++;
		platform = platform->next;
	}

	D_RailHead header;

	putWord(&header.version, RailNetVersion);
	putWord(&header.nTrains, nTrains);
	putWord(&header.nPlatforms, nPlatforms);
	putGameTime(&header.lastTime, rails.lastTime);

	file.write(&header, sizeof(header));

	/*
	 * Write Trains
	 */

	train = rails.trains;
	while(train)
	{
		D_Train data;

		putWord(&data.owner, train->owner);
		putWord(&data.lastStation, train->lastStation);
		putWord(&data.nextStation, train->nextStation);
		putLong(&data.spaces, train->spaces);
		putByte(&data.side, train->side);
		putByte(&data.mode, train->mode);
		putGameTime(&data.arrival, train->arrival);
		putWord(&data.nPassengers, train->passengers.entries());

		file.write(&data, sizeof(data));

		PtrDListIter<Unit> pIter(train->passengers);
		while(++pIter)
		{
			Unit* unit = pIter.current();

			ILONG unitID;

			/*
			 * This is a bodge!
			 */

			if(unit)
				putLong(&unitID, ob.getID(unit));
			else
				putLong(&unitID, -1);
			file.write(&unitID, sizeof(unitID));
		}

		train = train->next;
	}

	/*
	 * Platforms
	 */

	platform = rails.platforms;
	while(platform)
	{
		D_Platform data;

		putWord(&data.station, platform->station);
		putWord(&data.destination, platform->destination);
		putByte(&data.side, platform->side);
		putWord(&data.nWaiting, platform->waiting.entries());

		file.write(&data, sizeof(data));

		PtrDListIter<Unit> pIter(platform->waiting);
		while(++pIter)
		{
			Unit* unit = pIter.current();

			ILONG unitID;

			/*
			 * This is a bodge!
			 */

			if(unit)
				putLong(&unitID, ob.getID(unit));
			else
				putLong(&unitID, -1);
	  
			file.write(&unitID, sizeof(unitID));
		}

		platform = platform->next;
	}

	file.endWriteChunk();
}

void readDynamicRail(ChunkFileRead& file, RailNetwork& rails, const OrderBattle& ob)
{
	if(file.startReadChunk(DynamicRailID) == CE_OK)
	{
		D_RailHead header;

		file.read(&header, sizeof(header));

		UWORD version = getWord(&header.version);

		UWORD nTrains = getWord(&header.nTrains);
		UWORD nPlatforms = getWord(&header.nPlatforms);
		getGameTime(&header.lastTime, rails.lastTime);

		/*
		 * Read the trains
		 */

		Train* lastTrain = 0;	// New trains are appended on to the end of the list

		while(nTrains--)
		{
			Train* train = new Train;

			D_Train data;
			file.read(&data, sizeof(data));
			train->owner = getWord(&data.owner);
			train->lastStation = getWord(&data.lastStation);
			train->nextStation = getWord(&data.nextStation);
			train->spaces = getLong(&data.spaces);
			train->side = Side(getByte(&data.side));
			train->mode = Train::TrainMode(getByte(&data.mode));
			getGameTime(&data.arrival, train->arrival);

			UWORD nPassengers = getWord(&data.nPassengers);

			while(nPassengers--)
			{
				if(version >= 0x01)
				{
					ILONG unitID;

					file.read(&unitID, sizeof(unitID));
					Unit* u = ob.getUnitPtr(getLong(&unitID));

					// Patch 23Jan96
					if(u != 0)
					{
						train->passengers.append(u);
					}
#ifdef PATCHLOG
					else
					{
						patchLog.printf("Patch 1.1.5, Train with NULL unit");
					}
#endif
				}
				else
				{
					IWORD unitID;

					file.read(&unitID, sizeof(unitID));
					Unit* u = ob.getUnitPtr(getWord(&unitID));

					// Patch 23Jan96
					if(u != 0)
					{
						ASSERT(u != 0);

						train->passengers.append(u);
					}
#ifdef PATCHLOG
					else
					{
						patchLog.printf("Patch 1.1.5, Train with NULL unit");
					}
#endif
				}
			}

			// Patch 23Jan96
			if(train->passengers.entries() == 0)
			{
#ifdef PATCHLOG
				patchLog.printf("Patch 1.1.5, Train with no passengers");
#endif
				delete train;
			}
			else
			{
				if(lastTrain)
					lastTrain->next = train;
				else
					rails.trains = train;
				lastTrain = train;
			}
		}

		/*
		 * Get the platforms
		 */

		Platform* lastPlatform = 0;

		while(nPlatforms--)
		{
			Platform* platform = new Platform;

			D_Platform data;

			file.read(&data, sizeof(data));

			platform->station = getWord(&data.station);
			platform->destination = getWord(&data.destination);
			platform->side = Side(getByte(&data.side));
			UWORD nWaiting = getWord(&data.nWaiting);

			while(nWaiting--)
			{
				if(version >= 0x01)
				{
					ILONG unitID;

					file.read(&unitID, sizeof(unitID));
					// ASSERT(getLong(&unitID) != -1);
					Unit* u = ob.getUnitPtr(getLong(&unitID));
					// ASSERT(u != 0);
					if(u != 0)
						platform->waiting.append(u);
#ifdef PATCHLOG
					else
						patchLog.printf("Patch 2.0.3, Illegal Unit on platform");
#endif
				}
				else
				{
					IWORD unitID;

					file.read(&unitID, sizeof(unitID));
					// ASSERT(getWord(&unitID) != -1);
					Unit* u = ob.getUnitPtr(getWord(&unitID));
					// ASSERT(u != 0);
					if(u != 0)
						platform->waiting.append(u);
#ifdef PATCHLOG
					else
						patchLog.printf("Patch 2.0.3, Illegal Unit on platform");
#endif
				}
			}

			// Patch 1st February 1997

			if(platform->waiting.entries() == 0)
			{
#ifdef PATCHLOG
				patchLog.printf("Patch 2.0.3, Platform with no units");
#endif
				delete platform;
			}
			else
			{
				if(lastPlatform)
					lastPlatform->next = platform;
				else
					rails.platforms = platform;
				lastPlatform = platform;
			}
		}


	}
}

/*=============================================================
 * Mobilise List
 */

struct D_MobListHead {
	IWORD version;
	IWORD howMany;
};

struct D_Mobilising {
	IBYTE what;
	IBYTE when;
	IBYTE howMany;
	IWORD where;
};


void writeMobList(ChunkFileWrite& file, CampaignWorld* world)
{
	file.startWriteChunk(MobiliseListID);

	UWORD howMany = 0;
	Mobilising* mob = world->mobList.entry;
	while(mob)
	{
		howMany++;
		mob = mob->next;
	}

	D_MobListHead header;

	putWord(&header.version, MobListVersion);
	putWord(&header.howMany, howMany);

	file.write(&header, sizeof(header));

	mob = world->mobList.entry;
	while(mob)
	{
		D_Mobilising data;

		putByte(&data.what, mob->what);
		putByte(&data.when, mob->when);
		putByte(&data.howMany, mob->howMany);
		putWord(&data.where, world->facilities.getID(mob->where));

		file.write(&data, sizeof(data));

		mob = mob->next;
	}

	file.endWriteChunk();
}

void readMobList(ChunkFileRead& file, CampaignWorld* world)
{
	if(file.startReadChunk(MobiliseListID) == CE_OK)
	{
		D_MobListHead header;

		file.read(&header, sizeof(header));

		UWORD howMany = getWord(&header.howMany);

		Mobilising* last = 0;
		while(howMany--)
		{
			Mobilising* mob = new Mobilising;

			D_Mobilising data;

			file.read(&data, sizeof(data));

			mob->what = MobiliseType(getByte(&data.what));
			mob->when = getByte(&data.when);
			mob->howMany = getByte(&data.howMany);
			mob->where = world->facilities[getWord(&data.where)];

			if(last)
				last->next = mob;
			else
				world->mobList.entry = mob;
			last = mob;
		}
	}
}

/*=============================================================
 * World Header Chunks
 */

struct D_WorldHeader_00 {
	IWORD version;
	D_GameTime gameTime;
	ILONG CSAVictory;
	ILONG USAVictory;
	IBYTE cltoType;
	IBYTE aiArmies;
};

struct D_WorldHeader_01 {
	IWORD version;
	D_GameTime gameTime;
	ILONG CSAVictory;
	ILONG USAVictory;
	IBYTE cltoType;
	IBYTE aiArmies;
	IBYTE CSAWinVictory;
	IBYTE USAWinVictory;
};

struct D_WorldHeader {
	IWORD version;
	D_GameTime gameTime;
	ILONG CSAVictory;
	ILONG USAVictory;
	IBYTE cltoType;
	IBYTE aiArmies;
	IBYTE CSAWinVictory;
	IBYTE USAWinVictory;
	IBYTE CSAStartVictory;
};

void writeDynamicWorldHeader(ChunkFileWrite& file, Campaign* campaign)
{
	file.startWriteChunk(WorldHeaderID);
	
	D_WorldHeader header;
	memset(&header, 0, sizeof(header));

	putWord(&header.version, WorldHeadVersion);
	putGameTime(&header.gameTime, campaign->gameTime);
#ifdef CAMPEDIT
	putLong(&header.CSAVictory, 0);
	putLong(&header.USAVictory, 0);
	putByte(&header.USAWinVictory, 0);
	putByte(&header.USAWinVictory, 0);
	putByte(&header.CSAStartVictory, 0);
	putByte(&header.cltoType, 0);
#else
	putLong(&header.CSAVictory, campaign->CSAVictory);
	putLong(&header.USAVictory, campaign->USAVictory);
	putByte(&header.USAWinVictory, campaign->USAWinVictory);
	putByte(&header.CSAWinVictory, campaign->CSAWinVictory);
	putByte(&header.CSAStartVictory, campaign->CSAStartVictory);
#ifdef CHRIS_AI
	if(campaign->ai)
	{
		putByte(&header.cltoType, campaign->ai->CLTObjective);
		putByte(&header.aiArmies, campaign->ai->getArmyByte());
	}
#endif
#endif


	file.write(&header, sizeof(header));

	file.endWriteChunk();
}

void readDynamicWorldHeader(ChunkFileRead& file, Campaign* campaign)
{
	if(file.startReadChunk(WorldHeaderID) == CE_OK)
	{
		ChunkPos pos = file.getPos();

		IWORD version;

		file.read(&version, sizeof(version));
		file.setPos(pos);

		if(getWord(&version) < 1)
		{
			D_WorldHeader_00 header;

			file.read(&header, sizeof(header));

			getGameTime(&header.gameTime, campaign->gameTime);
			campaign->timeInfo = campaign->gameTime;

#if !defined(CAMPEDIT)
			// Set victory to 0 to force a recalculation
			campaign->CSAVictory = 0;	// getLong(&header.CSAVictory);
			campaign->USAVictory = 0;	// getLong(&header.USAVictory);
			campaign->USAWinVictory = 0;
			campaign->CSAWinVictory = 0;
			campaign->CSAStartVictory = 0;
#ifdef CHRIS_AI
			if(campaign->ai)
			{
				campaign->ai->CLTObjective = CLTOtype(getByte(&header.cltoType));
				campaign->ai->setArmyByte(getByte(&header.aiArmies));
			}
#endif
#endif
		}
		if(getWord(&version) < 2)
		{
			D_WorldHeader_01 header;

			file.read(&header, sizeof(header));

			getGameTime(&header.gameTime, campaign->gameTime);
#if !defined(CAMPEDIT)
			campaign->CSAVictory = getLong(&header.CSAVictory);
			campaign->USAVictory = getLong(&header.USAVictory);
			campaign->CSAWinVictory = getByte(&header.CSAWinVictory);
			campaign->USAWinVictory = getByte(&header.USAWinVictory);
			campaign->CSAStartVictory = 0;
#ifdef CHRIS_AI
			if(campaign->ai)
			{
				campaign->ai->CLTObjective = CLTOtype(getByte(&header.cltoType));
				campaign->ai->setArmyByte(getByte(&header.aiArmies));
			}
#endif
#endif
		}
		else
		{
			D_WorldHeader header;

			file.read(&header, sizeof(header));

			getGameTime(&header.gameTime, campaign->gameTime);
#if !defined(CAMPEDIT)
			campaign->CSAVictory = getLong(&header.CSAVictory);
			campaign->USAVictory = getLong(&header.USAVictory);
			campaign->CSAWinVictory = getByte(&header.CSAWinVictory);
			campaign->USAWinVictory = getByte(&header.USAWinVictory);
			campaign->CSAStartVictory = getByte(&header.CSAStartVictory);
#ifdef CHRIS_AI
			if(campaign->ai)
			{
				campaign->ai->CLTObjective = CLTOtype(getByte(&header.cltoType));
				campaign->ai->setArmyByte(getByte(&header.aiArmies));
			}
#endif
#endif
		}
	}
}


/*
 * Read the world
 */

void readStaticWorld(ReadWithPopup& file, CampaignWorld* world)
{
#ifdef DEBUG
	memLog->track("Before readStates");
#endif

#ifdef DEBUG
	file.showInfo("\rStates");
#endif
	readStates(file, world->states);

#ifdef DEBUG
	memLog->track("Before readFacilities");
#endif

#ifdef DEBUG
	file.showInfo("\rFacilities");
#endif
	readFacilities(file, world->facilities);

#ifdef DEBUG
	memLog->track("Before readRailways");
#endif
#ifdef DEBUG
	file.showInfo("\rRailway Sections");
#endif
#ifdef CAMPEDIT
	readRailSections(file, world);
#else
	readRailSections(file, world->railways);
#endif

#ifdef DEBUG
	memLog->track("Before readWaterways");
#endif
#ifdef DEBUG
	file.showInfo("\rWaterways");
#endif
	readWater(file, world->waterNet);

#ifdef DEBUG
	memLog->track("End of readStaticWorld");
#endif
}

void readDynamicWorld(ReadWithPopup& file, Campaign* campaign)
{
	if(file.fileVersion >= 0x0001)
		readDynamicWorldHeader(file, campaign);

#ifdef DEBUG
	memLog->track("After readOB");
#endif


#ifdef DEBUG
	file.showInfo("\rFacilities");
#endif
	updateFacilities(file, campaign->world->facilities);

#ifdef DEBUG
	file.showInfo("\rStates");
#endif
	updateStates(file, campaign->world->states);

#ifdef DEBUG
	file.showInfo("\rWater network");
#endif
	updateWater(file, campaign->world->waterNet, campaign->world->ob);

	if(file.fileVersion >= 0x0001)
	{
#ifdef DEBUG
		file.showInfo("\rRail Network");
#endif
		readDynamicRail(file, campaign->world->railNet, campaign->world->ob);


#ifdef DEBUG
		file.showInfo("\rCity Mobilise List");
#endif
		readMobList(file, campaign->world);
	}

	if(!file.isGood())
		throw GeneralError("Error in output file");
}

void writeStaticWorld(WriteWithPopup& file, CampaignWorld* world)
{
#ifdef DEBUG
	file.showInfo("\rStates");
#endif
	writeStaticStates(file, world->states);

#ifdef DEBUG
	file.showInfo("\rFacilities");
#endif
	writeStaticFacilities(file, world->facilities);

#ifdef DEBUG
	file.showInfo("\rRailway Sections");
#endif
#ifdef CAMPEDIT
	writeRailSections(file, world);
#else
	writeRailSections(file, world->railways);
#endif

#ifdef DEBUG
	file.showInfo("\rWaterways");
#endif
	writeStaticWater(file, world->waterNet);
}

void writeDynamicWorld(WriteWithPopup& file, Campaign* campaign)
{
	writeDynamicWorldHeader(file, campaign);

#ifdef DEBUG
	file.showInfo("\rFacilities");
#endif
	writeDynamicFacilities(file, campaign->world->facilities);

#ifdef DEBUG
	file.showInfo("\rStates");
#endif
	writeDynamicStates(file, campaign->world->states);

#ifdef DEBUG
	file.showInfo("\rWater network");
#endif
	writeDynamicWater(file, campaign->world->waterNet, campaign->world->ob);

#ifdef DEBUG
	file.showInfo("\rRail network");
#endif
	writeDynamicRail(file, campaign->world->railNet, campaign->world->ob);

#ifdef DEBUG
	file.showInfo("\rMobilise List");
#endif
	writeMobList(file, campaign->world);

#if 0
#ifdef DEBUG
	file.showInfo("\rOrder of Battle");
#endif
	writeOB(file, &campaign->world->ob, campaign->world);
#endif

	if(!file.isGood())
		throw GeneralError("Error in output file");
}

void writeSavedCampaign(WriteWithPopup& file, Campaign* campaign)
{
	writeDynamicWorld(file, campaign);
	writeOB(file, &campaign->world->ob, campaign->world);
}

void readSavedCampaign(ReadWithPopup& file, Campaign* campaign)
{
	readOB(file, &campaign->world->ob, campaign->world);
	readDynamicWorld(file, campaign);
}

void readStaticWorld(const char* fileName, CampaignWorld* world)
{
	ReadWithPopup file(fileName, FT_StartCampaign);

	readStaticWorld(file, world);
}


void readDynamicWorld(const char* fileName, Campaign* campaign)
{

	ReadWithPopup file(fileName, FT_DynamicCampaign);

	readDynamicWorld(file, campaign);
}

void writeDynamicWorld(const char* fileName, Campaign* campaign)
{

	WriteWithPopup file(fileName, FT_DynamicCampaign, "Temporary Campaign", worldVersion, 7);

	writeDynamicWorld(file, campaign);
}

void readStartCampaign(const char* fileName, Campaign* campaign)
{
	ReadWithPopup file(fileName, FT_StartCampaign);

	readStaticWorld(file, campaign->world);

#ifdef DEBUG
	file.showInfo("\rOrder of Battle");
	memLog->track("Before readOB");
#endif
	readOB(file, &campaign->world->ob, campaign->world);

	readDynamicWorld(file, campaign);

	campaign->world->setUp();
}

void writeStartCampaign(const char* fileName, Campaign* campaign)
{
	WriteWithPopup file(fileName, FT_StartCampaign, "Initial World", worldVersion, 11);

	writeStaticWorld(file, campaign->world);
	writeDynamicWorld(file, campaign);
#ifdef DEBUG
	file.showInfo("\rOrder of Battle");
#endif
	writeOB(file, &campaign->world->ob, campaign->world);
}


/*
 * Write the marked battle list
 */

struct D_BattleListHeader {
	IBYTE version;
	IBYTE nBattles;		// Number of battles in list
};

struct D_MarkedBattle {
	D_Location where;
	D_GameTime when;
	IBYTE nUnits;
	IBYTE playable;
};

struct D_BattleUnit {
	ILONG unit;
	D_GameTime when;
};

struct D_BattleUnit_01 {
	IWORD unit;
	D_GameTime when;
};


void writeCampaignBattleInfo(WriteWithPopup& file, Campaign* campaign)
{
	file.startWriteChunk(CampaignBattleID);

#if defined(CAMPEDIT)
	D_BattleListHeader head;
	putByte(&head.version, CampaignBattleVersion);
	putByte(&head.nBattles, 0);
	file.write(&head, sizeof(head));
#else

	BattleList* bList = &campaign->world->battles;

	D_BattleListHeader head;
	putByte(&head.version, CampaignBattleVersion);
	putByte(&head.nBattles, bList->entries());
	file.write(&head, sizeof(head));

	MarkedBattle* mb = 0;

	while( (mb = bList->next(mb)) != 0)
	{
		D_MarkedBattle mbHead;

		putLocation(&mbHead.where, mb->where);
		putGameTime(&mbHead.when, mb->when);
		putByte(&mbHead.nUnits, mb->nUnits());
		putByte(&mbHead.playable, mb->playable);
		file.write(&mbHead, sizeof(mbHead));

		BattleUnit* bu = mb->units;
		while(bu)
		{
			D_BattleUnit uHead;

			putLong(&uHead.unit, campaign->world->ob.getID(bu->unit));
			putGameTime(&uHead.when, bu->when);
			file.write(&uHead, sizeof(uHead));

			bu = bu->next;
		}
	}
#endif

	file.endWriteChunk();
}

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
void writeCampaignTemp(WriteWithPopup& file, Campaign* campaign)
{
	char* workFileName = campaign->getWorkFileName();
	if(workFileName)
	{
		struct stat sBuf;
		if(stat(workFileName, &sBuf) == 0)
		{
			FILE* inFile = fopen(workFileName, "rb");
			if(inFile)
			{
				ULONG fileSize = sBuf.st_size;

				file.startWriteChunk(CampaignTempID);

				ILONG length;
				putLong(&length, fileSize);
				file.write(&length, sizeof(length));

				/*
				 * Copy from inFile to file....
				 *
				 * Use a 16K Buffer
				 */

				const size_t BufLen = 0x4000;
				MemPtr<UBYTE> buffer(BufLen);

				while(fileSize)
				{
					size_t amount;

					if(fileSize > BufLen)
						amount = BufLen;
					else
						amount = fileSize;

					fileSize -= amount;

					fread((UBYTE*)buffer, amount, 1, inFile);
					file.write((UBYTE*)buffer, amount);
				}

				fclose(inFile);

				file.endWriteChunk();
			}
		}
	}
}
#endif

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
void readCampaignBattleInfo(ReadWithPopup& file, Campaign* campaign, OrderBattle* ob)
{
	if(file.startReadChunk(CampaignBattleID) == CE_OK)
	{
		D_BattleListHeader header;
		file.read(&header, sizeof(header));
		int nBattles = getByte(&header.nBattles);
		UBYTE version = getByte(&header.version);

		while(nBattles--)
		{
			D_MarkedBattle mbHead;
			file.read(&mbHead, sizeof(mbHead));

			MarkedBattle* mb = new MarkedBattle;

			getLocation(&mbHead.where, mb->where);
			getGameTime(&mbHead.when, mb->when);
			int nUnits = getByte(&mbHead.nUnits);
			mb->playable = getByte(&mbHead.playable);

			while(nUnits--)
			{
				BattleUnit* bu = new BattleUnit;

				if(version >= 0x02)
				{
					D_BattleUnit uHead;
					file.read(&uHead, sizeof(uHead));
					bu->unit = ob->getUnitPtr(getLong(&uHead.unit));
					getGameTime(&uHead.when, bu->when);
				}
				else
				{
					D_BattleUnit_01 uHead;
					file.read(&uHead, sizeof(uHead));
					bu->unit = ob->getUnitPtr(getWord(&uHead.unit));
					getGameTime(&uHead.when, bu->when);
				}

				mb->append(bu);
			}

			campaign->world->battles.append(mb);
		}
	}
}
#endif

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
void readCampaignTemp(ReadWithPopup& file, Campaign* campaign)
{
	if(file.startReadChunk(CampaignTempID) == CE_OK)
	{
		ILONG d_length;
		file.read(&d_length, sizeof(d_length));
		ULONG fileSize = getLong(&d_length);

		/*
		 * Make a temporary filename and open it
		 */

		char* fname = campaign->makeWorkFileName();
		FILE* fp = fopen(fname, "wb");
		if(fp)
		{
			const size_t bufSize = 0x4000;		// Use 16K buffer
			MemPtr<UBYTE> buffer(bufSize);

			while(fileSize)
			{
				size_t amount;

				if(fileSize < bufSize)
					amount = fileSize;
				else
					amount = bufSize;
				fileSize -= amount;

				file.read((UBYTE*)buffer, amount);
				fwrite((UBYTE*)buffer, amount, 1, fp);
			}

			fclose(fp);
		}
	}
}
#endif
