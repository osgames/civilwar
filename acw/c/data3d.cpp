/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	3D System Control
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:13:23  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/15 14:26:16  greenius
 * Converted filenames to lower case
 *
 * Revision 1.1  2001/03/11 00:58:48  greenius
 * Added to sourceforge
 *
 * Revision 1.5  1994/09/23  13:28:28  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/08/24  15:05:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/08/19  17:29:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/08/09  21:29:58  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/08/09  15:42:15  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "data3d.h"
#ifdef DEBUG
#include "colours.h"
#endif

#ifdef USE_SCANBUF

void System3D::render(Region* bm)
{
	scanBuffer.draw(*this);
#ifdef DEBUG1
	Point3D p1(view.minX, view.centre.y, view.minZ);
	Point3D p2(view.maxX, view.centre.y, view.minZ);
	Point3D p3(view.minX, view.centre.y, view.maxZ);
	Point3D p4(view.maxX, view.centre.y, view.maxZ);

	Point3D p5 = view.centre;

	view.transform(p1);
	view.transform(p2);
	view.transform(p3);
	view.transform(p4);

	view.transform(p5);

	bitmap->line(p1, p2, Black);
	bitmap->line(p2, p4, Black);
	bitmap->line(p4, p3, Black);
	bitmap->line(p3, p1, Black);

	bitmap->line(p1, p5, LRed);
	bitmap->line(p2, p5, LRed);
	bitmap->line(p4, p5, LRed);
	bitmap->line(p3, p5, LRed);


	bitmap->VLine(p1.x, 0, bitmap->getH(), Black);
	bitmap->VLine(p2.x, 0, bitmap->getH(), Black);
	bitmap->VLine(p3.x, 0, bitmap->getH(), Black);
	bitmap->VLine(p4.x, 0, bitmap->getH(), Black);
#endif
}

#endif

/*
 * Miscellaneous functions to do with 3D
 */

#if 0
Point3D& Point3D::operator *= (const Wangle3D& r)
{
	rotate(x, z, -r.yRotate);
	rotate(z, y, r.xRotate);
	// rotate(x, y, r.zRotate);

	return *this;
};
#endif

/*
 * Cross Product
 */

void cross(Point3D& result, const Point3D& p1, const Point3D& p2)
{
	result.x = p1.y * p2.z - p1.z * p2.y;
	result.y = p1.z * p2.x - p1.x * p2.z;
	result.z = p1.x * p2.y - p1.y * p2.x;
}

Cord3D dot(const Point3D& p1, const Point3D& p2)
{
	return p1.x * p2.x + p1.y * p2.y + p1.z * p2.z;
}

/*
 * Absolute value of a point
 * supposed to be:
 *
 *	    +---------------
 *	   / 2	 2	    2
 *  \/ x  + y	 + z
 *
 * Aproximate it with:
 *
 */

Cord3D Point3D::distance()
{
	Cord3D ax = ::abs(x);
	Cord3D ay = ::abs(y);
	Cord3D az = ::abs(z);

	if(ax > ay)
		ax += ay / 2;
	else
		ax = ay + ax / 2;

	if(ax > az)
		return ax + az / 2;
	else
		return az + ax / 2;
}

/*
 * The point is set up such that it normalised to be a vector of size 256
 */

void Point3D::normalise()
{
	// Reduce in size a bit to avoid overflows!

	while( 
		 (x >=  0x800000) ||		// 2^31 >> 8
	    (y >=  0x800000) ||
		 (z >=  0x800000) ||
		 (x <= -0x800000) ||
		 (y <= -0x800000) ||
		 (z <= -0x800000))
	{
		x >>= 1;
		y >>= 1;
		z >>= 1;
	}


	Cord3D size = distance();

	if(size == 0)
		throw Zero();

	if(x)
		x = (x * 255) / size;
	if(y)
		y = (y * 255) / size;
	if(z)
		z = (z * 255) / size;
};


Boolean System3D::screenToBattleLocation(const Point& p, BattleLocation& location)
{
	/*
	 * Search scan buffer for x,y,z
	 */

	Point3D p3d;
#ifdef USE_SCANBUF
	if(scanBuffer.findPoint(p, p3d))
#else
	if(zBuffer.findPoint(p, p3d))
#endif
	{
		view.unTransform(p3d);
		location.x = p3d.x;
		location.z = p3d.z;
		return True;
	}
	else
		return False;
}

