#####################################################################
#
# Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
# This Software is subject to the GNU General Public License.  
# For License information see the file COPYING in the root directory of the project.
# For more information see the file README.
#
#####################################################################
# $Id$
####################################################
#
# Makefile for Dagger Library
#   Setup so that even in debugging mode they are optimised
#   This means a lot of the debugging information is not there
#   but it gives a better indication of the final speed whilst
#   still keeping debugging options such as log files
#
####################################################
#
# $Log$
# Revision 1.1  2001/03/15 14:08:48  greenius
# Converted filenames to lower case
#
# Revision 1.1  2001/03/11 00:58:48  greenius
# Added to sourceforge
#
# Revision 1.3  1994/09/02  21:32:51  Steven_Green
# *** empty log message ***
#
# Revision 1.2  1994/08/22  16:09:53  Steven_Green
# *** empty log message ***
#
# Revision 1.1  1994/08/19  17:30:59  Steven_Green
# Initial revision
#
#
####################################################

!include include.mak

# This overides those in include.mak

# CFLAGDEBUG = $(CFLAG1) -d1 -DDEBUG -oneatx -s -DSCREENDUMP

# !ifdef NODEBUG
# CFLAGS = $(CFLAGOPT)
# !else
# !ifdef NOLOG
# CFLAGS = $(CFLAGNL)
# !else
# CFLAGS = $(CFLAGDEBUG)
# !endif
# !endif

# CPPFLAGS = $(CFLAGS) -xst

#######################
# Program specifics

default: $(DAGLIB) $(LZHUF_LIB) .SYMBOLIC

all: $(DAGLIB) $(DAGLIBDEMO) $(LZHUF_LIB) .SYMBOLIC

##############
# Compression Library

LZHUF_OBS = lzencode.obj lzdecode.obj lzmisc.obj

$(LZHUF_LIB) : $(LZHUF_OBS)
	@%create lzhlib.lbc
	@for %i in ($?) do @%append lzhlib.lbc -+ %i
	@type lzhlib.lbc
	$(LIB) $(LIBFLAGS) -p=32 $(LIBDIR)\$^. @lzhlib.lbc
	@del lzhlib.lbc

##########################
# Dagger Machine Library

DAGLIBOBS = system.obj screen.obj  &
	trig.obj strutil.obj random.obj &
	font.obj text.obj vesa.obj vesa_asm.obj &
	image.obj hline.obj makename.obj filedata.obj &
	menudata.obj globproc.obj icon.obj texticon.obj dialogue.obj &
	mouse.obj mouse_s.obj sprlib.obj mouselib.obj sprite.obj &
	ptrlist.obj timer.obj ilbm.obj ilbm_w.obj &
	palette.obj pal_s.obj &
	realmode.obj real_s.obj trap.obj &
	miscfunc.obj &
	sound.obj sndlib.obj &
	filesel.obj scroll.obj scrndump.obj sysicon.obj &
	log.obj memlog.obj memalloc.obj error.obj myassert.obj &
	$(LZHUF_OBS)

!ifdef %DSMI_H
DAGLIBOBS +=	nosound.obj sndconfi.obj readwav.obj &
!else
DAGLIBOBS += timer_s.obj
!endif

# timer_s.obj prtscn_s.obj mouse_s.obj 

$(DAGLIB) : $(DAGLIBOBS)
	@%create daglib.lbc
	@for %i in ($?) do @%append daglib.lbc -+ %i
	@type daglib.lbc
	$(LIB) $(LIBFLAGS) -p=32 $(LIBDIR)\$^. @daglib.lbc
	@del daglib.lbc

###################
# Demo version of dagger library

DAGLIBDEMOOBS = system.obj screen.obj  &
	trig.obj strutil.obj random.obj &
	font.obj text.obj vesa.obj vesa_asm.obj &
	image.obj hline.obj makename.obj filedata.obj &
	menudata.dem globproc.obj icon.obj texticon.dem dialogue.dem &
	mouse.obj mouse_s.obj sprlib.obj mouselib.obj sprite.obj &
	ptrlist.obj timer.obj ilbm.obj ilbm_w.obj &
	palette.obj pal_s.obj &
	realmode.obj real_s.obj trap.obj &
	miscfunc.obj &
	sound.obj sndlib.obj &
	filesel.dem scroll.obj scrndump.obj sysicon.obj &
	log.obj memlog.obj memalloc.obj error.obj myassert.obj &
	$(LZHUF_OBS)

!ifdef %DSMI_H
DAGLIBOBS +=	nosound.obj sndconfi.obj readwav.obj &
!else
DAGLIBOBS += timer_s.obj
!endif

# timer_s.obj prtscn_s.obj mouse_s.obj 

$(DAGLIBDEMO) : $(DAGLIBDEMOOBS)
	@%create daglib.lbc
	@for %i in ($?) do @%append daglib.lbc -+ %i
	@type daglib.lbc
	$(LIB) $(LIBFLAGS) -p=32 $(LIBDIR)\$^. @daglib.lbc
	@del daglib.lbc

##############
# asm files

vesa_asm.obj: vesa_asm.asm types.i vesa_asm.i real_s.i

mouse_s.obj: mouse_s.asm types.i mouse_s.i

prtscn_s.obj: prtscn_s.asm types.i prtscn_s.i

timer_s.obj: timer_s.asm types.i timer_s.i

text_s.obj: text_s.asm types.i

real_s.obj: real_s.asm types.i real_s.i

ipx_s.obj: ipx_s.asm types.i ipx_s.i real_s.i

trap.obj: trap.asm types.i

#######################
# Miscelleneous

#######################
# Miscelleneous

nosound.obj: $(S)\nosound.asm
	$(AS) /m5 /mx /zn /d__C32__ /o /I$(DSMIINC) $(S)\nosound.asm $(O)\nosound.obj

