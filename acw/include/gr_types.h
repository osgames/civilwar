/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef GR_TYPES_H
#define GR_TYPES_H

#ifndef __cplusplus
#error gr_types.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Types used by graphics
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.22  1994/08/31  15:27:28  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.21  1994/08/09  15:48:13  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.20  1994/07/11  14:32:07  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.19  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.18  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.17  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.16  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.14  1994/02/03  15:05:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/01/07  23:45:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1993/12/21  00:34:28  Steven_Green
 * Automatic conversion from Point3D to Point.
 *
 * Revision 1.11  1993/12/16  22:20:15  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1993/12/13  22:02:51  Steven_Green
 * intensityMask defined
 *
 * Revision 1.9  1993/12/13  17:13:13  Steven_Green
 * IntensityRatio added
 *
 * Revision 1.8  1993/12/10  16:08:02  Steven_Green
 * Intensity added
 *
 * Revision 1.7  1993/11/26  22:32:24  Steven_Green
 * virtual destructor added to cliprect
 *
 * Revision 1.6  1993/11/19  19:01:25  Steven_Green
 * Point operators use & wherever possible.
 * const keyword used.
 *
 * Revision 1.5  1993/11/16  22:43:10  Steven_Green
 * Added Point::+= and -=
 *
 * Revision 1.4  1993/11/15  17:20:33  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1993/11/05  16:52:08  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/10/27  21:09:15  Steven_Green
 * Clipping Rectangle defined more fully
 * Point has +, -, >>, <<, * and / operators
 *
 * Revision 1.1  1993/10/26  21:33:33  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <limits.h>
#include "types.h"
#include "miscfunc.h"

typedef WORD SDimension;			// Short Dimension
typedef UBYTE Colour;

const short maxSDimension = SHRT_MAX;

class Point3D;

class Point {
public:
	SDimension x;
	SDimension y;



					Point(SDimension x1, SDimension y1) { x = x1; y = y1; }
					Point() { }
					Point(Point3D& p);
	
	SDimension	getX() const { return x; }
	SDimension	getY() const { return y; }

	// Point& operator = (const Point& p) { *this = p; return *this; }

	Point& operator += (const Point& p)	{ x += p.x; y += p.y; return *this; }
	Point	operator +  (const Point& p) const  { return Point(SDimension(x + p.x), SDimension(y + p.y)); }
	Point operator +  (int val) const  { return Point(SDimension(x + val), SDimension(y + val)); }
	Point& operator -= (const Point& p)	{ x -= p.x; y -= p.y; return *this; }
	Point	operator -  (const Point& p) const	{ return Point(SDimension(x - p.x), SDimension(y - p.y)); }
	Point operator >> (int shift) const { return Point(SDimension(x >> shift), SDimension(y >> shift)); }
	Point operator << (int shift) const { return Point(SDimension(x << shift), SDimension(y << shift)); }
	Point operator /  (int val) const  { return Point(SDimension(x /  val), SDimension(y /  val));   }
	Point operator *  (int val) const  { return Point(SDimension(x *  val), SDimension(y *  val));   }

	Boolean operator !=(Point& p) const { return (x != p.x) || (y != p.y); }
	Boolean operator ==(Point& p) const { return (x == p.x) && (y == p.y); }

	Point			abs();
	Point			sign();
};

inline Point Point::abs()
{
	Point newPoint;

	newPoint.x = (x < 0) ? SDimension(-x) : SDimension(+x);
	newPoint.y = (y < 0) ? SDimension(-y) : SDimension(+y);

	return newPoint;
}


inline Point Point::sign()
{
	Point newPoint;

	newPoint.x = sgn(x);
	newPoint.y = sgn(y);

	return newPoint;
}


class Rect : public Point {
protected:
	Point size;
public:
					Rect(SDimension x1, SDimension y1, SDimension w1, SDimension h1) :
						Point(x1, y1), size(w1, h1) { }
					Rect(Point p, Point s) : Point(p), size(s) { }
					Rect() { }

	SDimension	getW() const { return size.x; }
	SDimension	getH() const { return size.y; }
	void			setW(SDimension w) { size.x = w; }
	void			setH(SDimension h) { size.y = h; }
	void			setSize(Point size) { Rect::size = size; }
	const Point& getSize() const { return size; }

	Rect operator &(const Rect r1) const;		// Check overlap

	Boolean operator !=(const Rect& r) const
	{
		return (x != r.x) || (y != r.y) || (size.x != r.size.x) || (size.y != r.size.y);
	}

	Rect operator + (const Point& p)
	{
		Rect r;

		r.x = x + p.x;
		r.y = y + p.y;
		r.size = size;

		return r;
	}

	Rect operator + (const Rect& r)
	{
		Rect result;

		result.x = x + r.x;
		result.y = y + r.y;
		result.size.x = size.x + r.size.x;
		result.size.y = size.y + r.size.y;

		return result;
	}

};

class ClipRect : public Rect {
public:
	Point max;		// The point to the botttom right outside the rectangle


						ClipRect(SDimension x1, SDimension y1, SDimension w1, SDimension h1) : Rect(x1,y1,w1,h1)
						{
							max.x = (SDimension) (x1 + w1);
							max.y = (SDimension) (y1 + h1);
						}
						
						ClipRect() {}

	virtual			~ClipRect() {}

	virtual void 	setClip(SDimension x1 = 0, SDimension y1 = 0, SDimension w1 = 0, SDimension h1 = 0)
						{
							x=x1; y=y1; setW(w1); setH(h1);
							max.x = (SDimension)(x1 + w1);
							max.y = (SDimension)(y1 + h1);
						}

				int	clip(Point& dest, Rect& src) const;
				int 	clipSrc(Point& dest, Rect& src) const;

			ClipRect	getClip() const { return *this; }
			Boolean inside(const Point& p);		// Returns true if inside rect
};



#endif /* GR_TYPES_H */
