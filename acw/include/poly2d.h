/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef POLY2D_H
#define POLY2D_H

#ifndef __cplusplus
#error poly2d.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	2D Polygon Renderer
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "gr_types.h"

class Point;
class Region;

typedef int PolyPointIndex;

class Polygon2D {
	PolyPointIndex 	nPoints;			// Number of points used
	Boolean 				pointsAlloced;	// Set if we created the points array
	PolyPointIndex 	maxPoints;		// Points allocated
	Point* 				points;			// Array of points
	Colour				colour;			// Colour to use for straight colours

public:
				Polygon2D()
				{
					nPoints = 0;
					pointsAlloced = False;
					maxPoints = 0;
					colour = 0;
				}

				Polygon2D(PolyPointIndex nPoints, Point* points = 0);
			  ~Polygon2D();

	void		add(const Point& p) { points[nPoints++] = p; };
	PolyPointIndex countPoints() const { return nPoints; }

	void		setColour(Colour c) 		{ colour = c; }

	void clipPoly(Polygon2D* destPoly, const Region* bitmap) const;
	void render(Region* bm) const;
};

#endif /* POLY2D_H */

