/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef TEXTICON_H
#define TEXTICON_H

#ifndef __cplusplus
#error texticon.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Text Input Icon
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.8  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.4  1994/05/25  23:33:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/05/04  22:12:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/04/26  13:40:53  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "icon.h"
#include "font.h"

class Font;

class InputIcon : public Icon {
	char* buffer;			// Where the characters are going
	size_t length;			// Length of buffer (excluding null)
	FontID font;		 	// Font to use for displaying string

	size_t cursorPos;		// Location of cursor in buffer

	// Boolean numeric;

protected:
	Boolean active;

public:
	// InputIcon(IconSet* set, Rect r, char* buf, size_t size, FontID f, Boolean number = False);
	InputIcon(IconSet* set, const Rect& r, char* buf, size_t size, FontID f);
	void execute(Event* event, MenuData* d);
	void drawIcon();

	/*
	 * Virtual function to remap keys
	 * Used for example by file selector to make sure that
	 * only valid filename characters are enabled.
	 */

	virtual Key processKey(Key k) { return k; }
};

class NumberInputIcon : public InputIcon {
public:
	NumberInputIcon(IconSet* set, const Rect& r, char* buf, size_t size, FontID f) :
		InputIcon(set, r, buf, size, f)
	{
	}

	Key processKey(Key k);
};

#endif /* TEXTICON_H */

