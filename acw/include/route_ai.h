/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef ROUTE_AI_H
#define ROUTE_AI_H

#ifndef __cplusplus
#error route_ai.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id ROUTE_AI 1.0 Chris Wall $
 *----------------------------------------------------------------------
 *
 *	Routines to control the route of the AI's armies on a Battlefield
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "measure.h"

class Route_AI {

	Location* clickpoints;
	char local;
	char MaxL;

public:
	Route_AI( const int number );
	~Route_AI();

	void addLocation( const Location& dest );
	Location* getLocation();

};


#endif /* ROUTE_AI_H */

