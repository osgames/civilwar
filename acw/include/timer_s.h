/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef TIMER_S_H
#define TIMER_S_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	BIOS Timer Interface
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1993/12/01  15:12:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/09  14:02:26  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif

//-- swg:10mar01: replaced with definition in timer.h
typedef void __saveregs __interrupt TimerFunction();
// typedef void far (TimerFunction)();


void installIRQ(TimerFunction* timerHandler);
void removeIRQ();

extern unsigned long timerCount;

#ifdef __cplusplus
};
#endif

#endif /* TIMER_S_H */

