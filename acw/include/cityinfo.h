/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CITYINFO_H
#define CITYINFO_H

#ifndef __cplusplus
#error cityinfo.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	City Information Display and Icons
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"
#include "gr_types.h"
#include "gamelib.h"
#include "mobilise.h"
#include "city.h"

class Facility;
class ResourceSet;
class Region;

class CityInfoIcon;
class TrackCity;

/*
 * Information that can be asked for about facility
 */

enum BuildIconType {
	BuildFort,
	BuildRail,
	BuildRecruit,
	BuildTraining,
	BuildHospital,
	BuildPOW,
	BuildDepot,
	BuildWagon,
	BuildInfantry,
	BuildCavalry,
	BuildArtillery,
	BuildNaval,

	BuildCount
};


struct BuildSetup {
	Rect r;
	MobiliseType what;
	Boolean onlyInCity;
	GameSprites graphic;			// Default Graphic
	Facility::FacilityTypes type;			// What it is (or None)

	BuildSetup(const Rect& r1, MobiliseType what1, Boolean inCity, GameSprites gr, Facility::FacilityTypes tp)
	{
		r = r1;
		what = what1;
		onlyInCity = inCity;
		graphic = gr;
		type = tp;
	}
};

extern BuildSetup buildSetup[BuildCount];

struct BuildInfo {
#if defined(NOBITFIELD)
	Boolean enabled:1;		// True if should be displayed
	GameSprites graphic;		// What graphic to display

	Boolean isBuilt:1;		// Set if thing exists
	Boolean upgradeable:1;	// True if can be constructed or upgraded
#else
	Boolean enabled;		// True if should be displayed
	GameSprites graphic;		// What graphic to display

	Boolean isBuilt;		// Set if thing exists
	Boolean upgradeable;	// True if can be constructed or upgraded
#endif
	UBYTE buildDays;			// Days till built (0 if not being built)
	UBYTE totalDays;			// Total number of days till built

	BuildInfo() { upgradeable = False; enabled = False; buildDays = 0; isBuilt = False; }

	void setupGroup(MobiliseType* table, Facility* where);
};


struct FacilityInfo {
	Facility* facility;
	BuildInfo info[BuildCount];
	const ResourceSet* localResources;
	const ResourceSet* importedResources;

	/*
	 * Functions
	 */

	void makeInfo(Facility* where);
	void drawInfo(Region* region, Boolean iconize);

	void makeIcons(CityInfoIcon* control, TrackCity* c);		// This is implemented in campcity.cpp
};



#endif /* CITYINFO_H */

