/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MEMBODGE_H
#define MEMBODGE_H

#ifndef __cplusplus
#error membodge.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Memory Bodge
 *
 * Allocate some big memory chunks at start of game to avoid problem
 * with memory fragmentation on 4 Meg Machine.
 *
 * Implement as a heap.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

class Heap {
	UBYTE* buffer;
	size_t length;

	UBYTE* nextFree;
	size_t amountLeft;

public:
	Heap(size_t howMuch);
	~Heap();

	void* allocate(size_t n);
	void free(void* ad);
	void reset();
};

extern Heap* memHeap;

#endif /* MEMBODGE_H */

