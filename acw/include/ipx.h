/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef IPX_H
#define IPX_H

#ifndef __cplusplus
#error ipx.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	IPX Classes and Data structures
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.2  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/05/04  22:12:01  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "connect.h"
#include "realmode.h"

#define	NODE_ADDRESS_SIZE	6	// Number of bytes in a node address, either local or global
#define	MAX_PACKET_SIZE		576	// Maximum size of a packet including IPX/SPX header
#define	IPX_TYPE			4	// Packet type number for IPX packets
#define	SPX_TYPE			5	// Packet type number for SPX packets

typedef UWORD Socket;

struct RealPtr {
	UWORD offset;
	UWORD segment;

	RealPtr() { }

	RealPtr(void* ad)
	{
		ULONG adL = ULONG(ad);
		segment = UWORD(adL >> 4);
		offset = UWORD(adL & 15);
	}

	RealPtr& operator =(void* ad)
	{
		ULONG adL = ULONG(ad);
		segment = UWORD(adL >> 4);
		offset = UWORD(adL & 15);
		return *this;
	}

	operator void*()
	{
		return (void*)(offset + (segment << 4));
	}
};

struct Fragment
{
	RealPtr ptr;			// -> buffer that data is sent from or received into
	UWORD size;				// Size of that buffer
};

struct ECB_Header
{
	RealPtr next;						// Used by IPX/SPX when the ECB is active, can be used when ECB is inactive
	void (*esr)(void);				// Called when packet sent/recd, called Event Service Routine					* s,r
	UBYTE	in_use;						// Flag used by IPX/SPX while it processes the ECB, !0 while in use
	UBYTE	completion_code;			// Completion code that is valid only when in_use is set to 0
	Socket socket;						// Transmission/reception socket to use for this ECB							* s,r
	UBYTE	IPX_work[4];				// Workspace used internally by IPX
	UBYTE	driver_work[12];			// Workspace used internally by the IPX driver
	UBYTE	dest_address[NODE_ADDRESS_SIZE];	// Address of node to which packet is sent/received			* s,r
	UWORD	fragment_count;						// Number of address/size IPX/SPX fragments that follow		* s,r

	Fragment hdr;						// Pointer to IPX_Header
	Fragment buffer;
};



struct IPX_NetAddress
{
	ULONG	network;							// Network number
	UBYTE	node[NODE_ADDRESS_SIZE];	// Physical node address
	Socket socket;							// Socket number
};


struct IPX_Header
{
	UWORD	checksum;		// Dummy checksum of the 30-byte packet header, filled by IPX
	UWORD	length;			// Length of the complete IPX packet, filled by IPX
	UBYTE	control;		// Transport control byte for internetwork bridges, filled by IPX
	UBYTE	type;			// Packet type: IPX=4, SPX=5, defined by Xerox, set by application	*
	IPX_NetAddress dest;			// Destination node address, set by application						*
	IPX_NetAddress src;			// Source node address, set by IPX
};

/*
 * Connection Level Classes
 */

const int IPXrxBufferCount = 4;				// Number of receive buffers to set up


struct IPX_Message {
	UBYTE id[6];			// "DWG_00"
	ULONG serial;			// Serial Number of this packet
	UBYTE needReceipt;	// Non zero if acknowledgement required
	Packet packet;			// This is as passed to the send functions
};

struct IPX_Buffer {
	ECB_Header* header;
	IPX_Header* ipxHeader;
	IPX_Message* message;
};

class IPXConnection : public Connection {
	RealMemPtr realMemory;			// Chunk of memory allocated for buffers, etc

	// ULONG serial;
	// ULONG rxSerial;
	// ReceiptList receiptList;
	// PtrDList<Packet> packetList;	// packets waiting for application

#ifdef DEBUG
public:
#endif
	IPX_Buffer txBuffer;
	IPX_Buffer rxBuffer[IPXrxBufferCount];
	IPX_NetAddress* localAddress;
	IPX_NetAddress* remoteAddress;

public:
	IPXConnection();
	~IPXConnection();
	int connect();
	void disconnect();
	int sendRawData(PacketType type, UBYTE* data, size_t length, Boolean receipt, ULONG serial);
	// Packet* receiveData();
	void processIncomming();
	Boolean isLocal() const { return False; }
private:
	void releaseData(Packet* data);
	void* initIPXbuffer(IPX_Buffer* buffer, void* ad);
	Boolean canSend();
};

#endif /* IPX_H */

