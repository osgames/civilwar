/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CAMPWLD_H
#define CAMPWLD_H

#ifndef __cplusplus
#error campwld.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign World environment
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.6  1994/06/07  18:33:23  Steven_Green
 * River/Sea added to world data
 *
 * Revision 1.4  1994/06/02  15:31:12  Steven_Green
 * UnitList removed... units are manipulated from OB tree instead.
 *
 * Revision 1.1  1994/05/19  17:47:49  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "ob.h"
#include "city.h"

#ifndef CAMPEDIT
#include "campbatl.h"
#endif

#include "campter.h"
#include "campmove.h"
#include "mobilise.h"
#include "water.h"
#include "railway.h"

#ifdef DEBUG
#include "clog.h"
extern LogFile cLog;
#endif

#ifdef TESTING_POPUP
class PopupText;
#endif

class RailRouteInfo;
class CampaignWorld {
	TimeBase lastBattleCheckTime;
public:
	OrderBattle ob;						// Troops
	FacilityList facilities;			// Facilities and cities
#ifndef CAMPEDIT
	RailSectionList railways;			// Rail sections
#endif
	StateList states;						// states
	CampaignTerrain terrain;			// Terrain bitmap
	MobiliseList mobList;				// Things being mobilised
	RailNetwork railNet;					// Platforms and trains
	WaterNetwork waterNet;				// Sea, Coast and Rivers
#ifndef CAMPEDIT
	BattleList battles;					// Battles to be played today
#endif

public:
	CampaignWorld();
	~CampaignWorld();
	void init();
	void setUp();						// Setup things after load
	void smallSetup();

#ifdef TESTING_POPUP
	void processDay(PopupText& popup, char* buffer);
#else
	void processDay();
#endif
	void processHour(const TimeBase& gameTime, GameTicks ticks);

	FacilityID findRailRoute(FacilityID from, FacilityID to, Side side, RailRouteInfo* routes = 0);

	// void IncExperience( MarkedBattle* batl );

#ifndef CAMPEDIT
	void getBestRoute(const Location& from, const Location& to, Order& order, Side side);
	Speed getOrderEffect( Speed s, OrderMode how );

	Speed getTerrainEffect( Speed s, const Location& l );

	void UpdateStats( void );

	
	void EODMoraleEffect(Regiment* regiment);
	void EODFatigueEffect(Regiment* regiment);
	void EODSupplyEffect(Brigade* regiment);
	void EODStragglersEffect(Regiment* regiment);

	// void EODDisplayEffect( Regiment* regiment );

	Boolean invisibilityON();

	void CanBeSeen( Unit* u );

	Boolean CheckLocalDest( const Location& l );

	Boolean waterRouteOK( WaterZoneID on, WaterZoneID off );

#ifdef CREATE_WORLD
	void makeCities();
	void makeRailways(PopupText& popup);
	void setCityStates();

	void checkRails();
#endif
#endif

#ifdef CAMPEDIT
	State* makeNewState();
	void deleteState(State* state);
	Facility* makeNewFacility();
	void deleteFacility(Facility* f);

	WaterZone* makeNewWaterZone();
	void deleteWaterZone(WaterZone* zone);
#endif

	MarkedBattle* makeBattle(Facility* f, Unit* u, Boolean play);
	MarkedBattle* makeBattle(Unit* u, Unit* u1);
private:
	void checkCityBattles(Brigade* b);
	void checkUnitBattles(Brigade* b);

	void processCloseCity(Facility* f, Unit* b);
};


/*
 * These are in campunit.cpp
 */

WaterZone* findCloseWaterZone(MapWindow* map, const Point& where, Flotilla& fleet);
FacilityID findCloseRailhead(const Location& l, Side side);
FacilityID findCloseRailJunction(const Location& l);
FacilityID findCloseLandingStage(const Location& l, Side side);
FacilityID findCloseWaterFacility(const Location& l);

#endif /* CAMPWLD_H */

