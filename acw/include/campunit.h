/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CAMPUNIT_H
#define CAMPUNIT_H

#ifndef __cplusplus
#error campunit.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Troop Movement Interface
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.12  1994/08/09  15:46:26  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/07/28  19:01:36  Steven_Green
 * Naval Movement Implemented
 *
 * Revision 1.10  1994/07/25  20:34:58  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/07/13  13:52:35  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/07/11  14:31:08  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/07/04  13:31:45  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/06/22  20:08:24  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.1  1994/05/21  13:18:12  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "campmode.h"
#include "orders.h"
#include "city.h"
#include "railway.h"
#include "water.h"

/*
 * Forward class references
 */

class Unit;
// class MapWindow;
// class Event;
// class Location;
// class CampaignOrderIcon;
// class CampaignMoveIcon;
class MobIL;
// class Facility;

FacilityID findCloseRailhead(const Location& l, Side side);
FacilityID findCloseLandingStage(const Location& l, Side side);
FacilityID findCloseWaterFacility(const Location& l);
FacilityID FindBestTrainRoute( FacilityID on, Location dest, int loop = 0, FacilityID comeFrom = NoFacility );
  


/*
 * Track Unit class
 */

class TrackUnit : public CampaignFunction {
friend class CampaignOrderIcon;
friend class CampaignMoveIcon;
friend class NavalMoveIcon;

public:
	MapObject* currentObject;			// Currently selected Object
	PtrDList<MapObject> objectList;	// List of all objects close to cursor

	FacilityID startLocation;	// get on point for rail/water movement
	FacilityID endLocation;		// get off point for rail/water movement
	enum { RL_None, RL_Friend, RL_Enemy } routeHow;	// Whether there is a valid route
	RailRouteInfo railInfo;		// Railway route information
	WaterRoute waterRoute;		// Water Route Information
	WaterZone* destWaterZone;
	WaterZoneID fromWater;
	WaterZoneID toWater;
	WaterZoneID viaWater;		// Next waterzone on route
	OrderHow currentHow;

	Boolean onMap;

private:
	MobIL* startObject;			// Display Object for rail/water movement
	MobIL* endObject;				// Display Object for rail/water movement


	enum { WaitUnit, WaitOrder } waitMode;
	OrderMode currentMode;
	PointerIndex oldPointer;
	Boolean forceUpdate;
	unsigned int highlightTimer;

	Flotilla fleet;				// Fleet that is being built

	Point previousMouse;
	Boolean mouseMoved;

public:
	TrackUnit(CampaignMode* p);
	~TrackUnit();

	void overMap(MapWindow* map, Event* event, MenuData* d, const Location* l);
	void offMap();

	void clickOK();

	void setCurrentUnit(Unit* unit, Boolean giveOrders);
private:
	void closeDown();
	void setupOrders();
	void setMoveMode();
	void startRailMode();
	void startWaterMode();
	void startLandMode();
	void sendOrder(const Location& l);
	void chooseStackedObject();
};

#endif /* CAMPUNIT_H */

