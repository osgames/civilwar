/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef BATSND_H
#define BATSND_H

#ifndef __cplusplus
#error batsnd.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battle Sound Effects
 *
 * These are sound effects called with a location, which is converted
 * to a volume and panning information before calling the raw sound
 * effect class.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "sndlib.h"
#include "measure.h"

/*
 * Enumeration of sound effects in battle.snd
 */

enum BattleSoundID {
	SND_CannonFire,
	SND_GunFire,
	SND_Melee,
	SND_Test
};

extern const char* battleSoundEffectNames[];

class BattleSound : public SoundLibrary {
public:
	BattleSound(const char* name) : SoundLibrary(name, battleSoundEffectNames) { }
	void playSound(BattleSoundID id, const Location& l, SoundPriority priority = MinSoundPriority);
};

#endif /* BATSND_H */

