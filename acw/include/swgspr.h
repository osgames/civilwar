/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SWGSPR_H
#define SWGSPR_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Sprite Header
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/07/19  19:56:23  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/12/21  00:40:06  Steven_Green
 * Boolean defined
 *
 * Revision 1.1  1993/12/16  22:20:15  Steven_Green
 * Initial revision
 *
 * Revision 1.1  1993/12/16  16:42:54  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifndef FILEDATA_H
#include "filedata.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define SPRITE_ID "SPRT";


typedef struct {
	Boolean lzHuf:1;			// Set if LZhuf compression used on data
	Boolean rle:1;
	Boolean nybble:1;
	Boolean squeeze:1;
	Boolean shadow:1;
	Boolean remap:1;
} SpriteFlags;

typedef struct {
	UWORD width;
	UWORD height;
	UWORD fullWidth;
	UWORD fullHeight;
	UWORD xOffset;
	UWORD yOffset;
	UWORD anchorX;
	UWORD anchorY;
	UBYTE shadowColor;
	UBYTE transparentColor;
	SpriteFlags flags;
} SpriteHeader;


#define RemapColorCount 16

typedef UBYTE REMAP_TABLE[RemapColorCount];

/*
 * Disk File structure
 */

typedef struct {
	char id[4];
	IWORD headerSize;
	IBYTE versionOrder;
	IWORD spriteCount;
} SpriteFileHeaderD;

/* Version order usage */

#define IsIntel 0x00
#define IsMotorola 0x80

#define SPRITE_VERSION 0x30		/* Version 3.0 */

typedef struct {
	IBYTE flags;
	IBYTE headerSize;				// Size of this header
	IWORD dataSize;				// Size of data on disk
	IWORD width;					// Width as stored on disk
	IWORD height;
	IWORD fullWidth;				// Width before squeezing
	IWORD fullHeight;
	IWORD xOffset;					// Amount squeezed from left
	IWORD yOffset;					// Amount squeezed from right
	IWORD anchorX;					// HotSpot
	IWORD anchorY;
	IBYTE shadowColor;
	IBYTE transparentColor;
} SpriteHeaderD;


/*
 * Flags Usage
 */

#define RLE_B 		0
#define SQUEEZE_B 1
#define SHADOW_B	2
#define NYBBLE_B	3
#define REMAP_B	4
#define LZHUF_B	5

#ifdef __cplusplus
};
#endif

#endif /* SWGSPR_H */

