/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef NAVAL_H
#define NAVAL_H

#ifndef __cplusplus
#error naval.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Naval Units
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/07/28  19:01:36  Steven_Green
 * Naval Movement Implemented
 *
 * Revision 1.2  1994/07/25  20:34:58  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/06/07  18:33:23  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"
#include "gamedef.h"
#include "gametime.h"
#include "side.h"

enum NavalType {
	Nav_NavalUnit,
	Nav_NavalIronClad,
	Nav_Riverine,
	Nav_RiverineIronClad,
	// Nav_Blockade,
	Nav_TypeCount,

	Nav_Monitor = Nav_RiverineIronClad,
	Nav_IronClad = Nav_NavalIronClad
};

/*
 * Naval Units are stored in flotilla's, which are a group of
 * boats that happen to be in the same location.
 *
 * Individual boats don't really exist.
 */

class Flotilla {
public:
	UBYTE count[Nav_TypeCount];

	Flotilla();
	~Flotilla();

	int totalBoats() const;
	Flotilla& operator+=(const Flotilla& f);
	Flotilla& operator-=(const Flotilla& f);
	Flotilla& operator = (const Flotilla& f);
	void clear();

	CombatValue totalCombatFactor() const;
	CombatValue totalBombardmentFactor() const;
	UBYTE getSpeed() const;
	void sinkBoats(UBYTE percent, Boolean force, Flotilla& lostBoats);
};


/*
 * A fleet is a flotilla that is on its way somewhere
 */

class ChunkFileWrite;
class ChunkFileRead;

class WaterNetwork;

class Fleet {
	friend class FleetList;
	friend class FleetIter;
	friend void writeDynamicWater(ChunkFileWrite& file, WaterNetwork& water);

	Fleet* next;		// So that it can be in a linked list

public:
#ifdef DEBUG
	int id;
#endif

	Flotilla boats;
	WaterZoneID where;			// Where it is now
	WaterZoneID destination;	// Where it wants to get to
	WaterZoneID via;				// Next zone on its route
	TimeBase arrival;			// Arival time at next zone
	Side side;						// What side is this fleet on?

	Fleet();
	Fleet(const Flotilla& b, Side s);
};

class FleetList {
	friend class FleetIter;
	friend void writeDynamicWater(ChunkFileWrite& file, WaterNetwork& water);

	Fleet* entry;
public:
	FleetList() { entry = 0; }
	~FleetList();

	void add(Fleet* fleet);
	void add(Fleet* fleet, WaterZoneID from, WaterZoneID to, GameTicks gameTime);
	void remove(Fleet* fleet);

	Boolean isEmpty() const { return entry==0; }

	void total(Flotilla& f) const;

	int entries();
};

class FleetIter {
	Fleet* fleet;
public:
	FleetIter(FleetList& list)
	{
		fleet = list.entry;
	}

	Fleet* next()
	{
		Fleet* rval = fleet;
		fleet = fleet->next;
		return rval;
	}
};

UBYTE getNavalCombat(NavalType type);
UBYTE getNavalBombardment(NavalType type);


#endif /* NAVAL_H */

