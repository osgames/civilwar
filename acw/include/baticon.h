/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef BATICON_H
#define BATICON_H

#ifndef __cplusplus
#error baticon.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Icons from the battlefield Sprites
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:12  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "batlib.h"
#include "icon.h"

class BattleIcon : public Icon {
	BattleSprite iconNumber;

public:
	BattleIcon(IconSet* parent, BattleSprite n, Rect r, Key k1 = 0, Key k2 = 0);
	void setIcon(BattleSprite n) { iconNumber = n; }
	void drawIcon();
};


#endif /* BATICON_H */

