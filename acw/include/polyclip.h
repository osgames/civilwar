/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef POLYCLIP_H
#define POLYCLIP_H

#ifndef __cplusplus
#error polyclip.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * 3D Polygon Clipping
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.19  1994/08/09  21:31:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.18  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.17  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.16  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/06/07  18:33:23  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.14  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.12  1994/05/21  13:18:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/04/20  22:24:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/04/11  21:31:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/04/11  13:38:20  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/03/18  15:08:40  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/02/28  23:05:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/02/17  20:00:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/12/10  16:08:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/24  09:34:12  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


#ifndef SHAPE3D_H
#include "shape3d.h"
#endif

#ifndef POLYGON_H
#include "polygon.h"
#endif

/*
 * 2D Clipping
 */

class PolyClip;
class PolyClipEdge {
	Point first;
	Point last;
	Boolean needFirst;
public:
				PolyClipEdge() : first(), last() { needFirst = True; };

	friend class PolyClip;
};

class PolyClip { 
	Polygon<Point>* destPoly;
	const Region* bitmap;
	enum { None, Left, Right, Top, Bottom } closing;
	PolyClipEdge top;
	PolyClipEdge bottom;
	PolyClipEdge left;
	PolyClipEdge right;

	void clipLeft(Point p);
	void clipRight(Point p);
	void clipTop(Point p);
	void clipBottom(Point p);
	
public:
				PolyClip(Polygon<Point>* destPoly, const Region* bitmap)
				{
					closing = None;
					PolyClip::destPoly = destPoly;
					PolyClip::bitmap = bitmap;
				};

				~PolyClip();

		void	clipPoint(Point p);



};

/*
 * 3D Clipping
 */

class PolyClipEdge3D {
	Point3D first;
	Point3D last;
	Boolean needFirst;
public:
	PolyClipEdge3D() : first(), last() { needFirst = True; };

	friend class PolyClip3D;
};

class PolyClip3D {
	Polygon<Point3D>* destPoly;			// Where the clipped polygon ends up!
	const ViewPoint* view;			// Viewpoint to clip it to

	enum { None, Front, Back, Left, Right, Top, Bottom } closing;

	PolyClipEdge3D front;
	PolyClipEdge3D back;
	PolyClipEdge3D left;
	PolyClipEdge3D right;
	PolyClipEdge3D top;
	PolyClipEdge3D bottom;

	void clipFront(Point3D p);
	void clipBack(Point3D p);
	void clipLeft(Point3D p);
	void clipRight(Point3D p);
	void clipTop(Point3D p);
	void clipBottom(Point3D p);

public:

	PolyClip3D(Polygon<Point3D>* destPoly, const ViewPoint* view)
	{
		closing = None;
		PolyClip3D::destPoly = destPoly;
		PolyClip3D::view = view;
	};

	~PolyClip3D();

	void clipPoint(Point3D p);
};

/*
 * 3DI Clipping
 */

class PolyClipEdge3DI {
	Point3DI first;
	Point3DI last;
	Boolean needFirst;
public:
	PolyClipEdge3DI() : first(), last() { needFirst = True; };

	friend class PolyClip3DI;
};

class PolyClip3DI {
	Polygon<Point3DI>* destPoly;			// Where the clipped polygon ends up!
	const ViewPoint* view;			// Viewpoint to clip it to

	enum { None, Front, Back, Left, Right, Top, Bottom } closing;

	PolyClipEdge3DI front;
	PolyClipEdge3DI back;
	PolyClipEdge3DI left;
	PolyClipEdge3DI right;
	PolyClipEdge3DI top;
	PolyClipEdge3DI bottom;

	void clipFront(Point3DI p);
	void clipBack(Point3DI p);
	void clipLeft(Point3DI p);
	void clipRight(Point3DI p);
	void clipTop(Point3DI p);
	void clipBottom(Point3DI p);

public:

	PolyClip3DI(Polygon<Point3DI>* destPoly, const ViewPoint* view)
	{
		closing = None;
		PolyClip3DI::destPoly = destPoly;
		PolyClip3DI::view = view;
	};

	~PolyClip3DI();

	void clipPoint(Point3DI p);
};


#endif /* POLYCLIP_H */

