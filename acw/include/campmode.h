/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CAMPMODE_H
#define CAMPMODE_H

#ifndef __cplusplus
#error campmode.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Modes
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/07/04  13:31:45  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifndef CAMPEDIT

#include "types.h"
#include "micelib.h"
#include "gamelib.h"


class MapWindow;
class Event;
class MenuData;
class Location;
class Unit;

class CampaignMode;

class CampaignFunction {
	CampaignMode* parent;
	PointerIndex oldPointer;
public:
	CampaignFunction(CampaignMode* p);
	virtual ~CampaignFunction();
	virtual void overMap(MapWindow* map, Event* event, MenuData* d, const Location* l) = 0;
	virtual void offMap() = 0;

	void setPointer(PointerIndex p);
	PointerIndex getPointer() const;

	MenuData* getMenuData();
};

class CampaignMode {
  	CampaignFunction* function;
	MenuData* parent;
	PointerIndex oldPointer;
	PointerIndex zoomPointer;
#if defined(NOBITFIELD)
	Boolean zooming;				// Set if in zooming mode
#else
	Boolean zooming:1;				// Set if in zooming mode
#endif
public:
	enum CM_Mode { CM_City, CM_Troop };
#if defined(NOBITFIELD)
	CM_Mode mode;

	Boolean overFlag;
#else
	CM_Mode mode:1;

	Boolean overFlag:1;
#endif
public:
	CampaignMode(MenuData* d);
	~CampaignMode();

	void overMap(MapWindow* map, Event* event, MenuData* d, const Location* l);
	void offMap() { if(function) function->offMap(); }

	void setMode(CM_Mode newMode);
	void toggleMode();
	void forceTroopMode(Unit* unit, Boolean giveOrders);
	GameSprites getModeIcon();

	void startZoom();
	void setPointer(PointerIndex p);
	PointerIndex getPointer() const;
	MenuData* getMenuData() { return parent; }
};

#else		// CAMPEDIT

class MapWindow;
class Event;
class MenuData;
class Location;

class CampaignMode {
public:
	virtual void overMap(MapWindow* map, Event* event, MenuData* d, const Location* l) = 0;
	virtual void startZoom() = 0;
};

#endif	// CAMPEDIT

#endif /* CAMPMODE_H */

