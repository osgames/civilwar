/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef HINTSTAT_H
#define HINTSTAT_H

#ifndef __cplusplus
#error hintstat.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Class for MenuData that shares hint/status line
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

class HintStatus {
	Boolean gotStatMessage;
	char* hintText;
public:
	HintStatus();
	virtual ~HintStatus();

	void showHint(const char* s);
	void showStatus(const char* s);

	virtual void showHintStatus(const char* s) = 0;
};

#endif /* HINTSTAT_H */

