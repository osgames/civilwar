/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SPRITE_H
#define SPRITE_H

#ifndef __cplusplus
#error sprite.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Sprite Handling
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.18  1994/09/02  21:28:59  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.17  1994/07/11  14:32:07  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.16  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.14  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.12  1994/04/20  22:24:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/03/11  23:14:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/02/28  23:05:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/02/17  20:00:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/01/07  23:45:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/01/06  22:40:53  Steven_Green
 * Based on Image instead of BitMap
 *
 * Revision 1.4  1993/12/21  00:34:28  Steven_Green
 * Changes for 3D Sprites
 *
 * Revision 1.3  1993/12/04  01:07:29  Steven_Green
 * Destructor for Sprite added.
 *
 * Revision 1.2  1993/11/15  17:20:33  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/05  16:52:08  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifndef IMAGE_H
#include "image.h"
#endif

class Mouse;

/*
 * A sprite is an image with a transparent colour
 */
 
class Sprite : public Image {
	Colour transparent;			// Transparent Colour
public:
				Sprite(Image& bm);
				Sprite(SDimension w, SDimension h): Image(w, h) { transparent = 0; }
			  ~Sprite() { };

	// Sprite operator = (BitMap bm);

	void		setTransparent(Colour col) { transparent = col; };

	friend class Vesa;
	friend class Image;
	friend class Region;
	// friend class BitMap;
};


#endif /* SPRITE_H */
