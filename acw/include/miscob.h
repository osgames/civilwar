/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MISCOB_H
#define MISCOB_H

#ifndef __cplusplus
#error miscob.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Miscellaneous Objects such as trees and house
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.9  1994/07/13  13:52:35  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.3  1994/05/21  13:18:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/04/20  22:24:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/04/11  13:38:20  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


#include "mapob.h"
#include "ptrlist.h"
#include "batlib.h"

class MiscObject : public MapObject {
	BattleSprite sprite;
public:
	MiscObject(const Location& l, BattleSprite spriteIndex);
	void draw3D(ObjectDrawData* drawData);
};

class MiscObjectList : public PtrSList<MiscObject> { };
typedef PtrSListIter<MiscObject> MiscObjectIter;

#endif /* MISCOB_H */

