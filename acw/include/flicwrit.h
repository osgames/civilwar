/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef FLICWRIT_H
#define FLICWRIT_H

#ifndef __cplusplus
#error flicwrit.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Flic Writing class
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/09/23  13:29:31  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>
#include "image.h"
#include "palette.h"
#include "flic.h"

// Define some file handling values so that we can
// change the file reading method easily to increas speed

// This is for Buffered C I/O

class FileHandleW {
	FILE* handle;

public:

	int fileOpen(const char* name)
	{
		handle = fopen(name, "wb");
		if(handle)
			return 0;
		else
			return -1;
	}

	void fileClose()
	{
		fclose(handle);
	}

	int fileWrite(void* data, long size)
	{
	  if(fwrite(data, size, 1, handle) != 1)
			return -1;
		else
			return 0;
	}

	int writeWord(UWORD w)
	{
		fputc(w & 0xff, handle);
		fputc(w >> 8, handle);
		return 0;
	}

	int writeByte(UBYTE b)
	{
		fputc(b, handle);
		return 0;
	}


	long fileSeek(long where)
	{
		return fseek(handle, where, SEEK_SET);
	}

	long getPos()
	{
		return ftell(handle);
	}
};

class FlicWrite {
	FlicHead header;
	Image image;		// Last image written
	Palette palette;	// Last palette written
	FileHandleW file;
	int error;
public:
	FlicWrite(const char* fileName);
	~FlicWrite();

	void addFrame(Image& img, Palette& p);
	void addLastFrame(Image& img, Palette& p);
	int getError() const { return error; }

	void setSpeed(long speed)
	{
		header.speed = speed;
	}

private:
	void writeDeltaFLC(Image& img);
	void writeColor256(Palette& p);
	void writeByteRun(Image& img);

};

#endif /* FLICWRIT_H */

