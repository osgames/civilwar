/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MAKENAME_H
#define MAKENAME_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Filename handling function header prototype
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1993/12/16  22:20:15  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/12/03  17:26:12  Steven_Green
 * *** empty log message ***
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif


#ifndef TYPES_H
#include "types.h"
#endif

void makeFilename(char *dest, const char *src, const char *ext, Boolean force);


#ifdef __cplusplus
};
#endif

#endif /* MAKENAME_H */
