/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef POOL_H
#define POOL_H

#ifndef __cplusplus
#error pool.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Simple memory pool for constant sized objects
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/09/02  21:27:49  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

struct Link {
	Link* next;
};

struct PoolBlock {
	PoolBlock* next;
	void* data;
};

class Pool {
	size_t elSize;		// Size of one element
	int blockSize;		// Number to allocate in each chunk

	Link* freeBlocks;	// Linked list of free blocks
	PoolBlock* blocks;

public:
	Pool(size_t sz, int n);
	~Pool();

	void* allocate();
	void release(void* mem);
};

#endif /* POOL_H */

