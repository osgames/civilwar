/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef OB_H
#define OB_H

#ifndef __cplusplus
#error ob.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Data for an Order of Battle
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.7  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.1  1994/04/06  12:42:17  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "gamedef.h"
#include "measure.h"
#include "generals.h"

/*
 * Undefined classes
 */

#ifdef DEBUG
class President;
class GeneralGenerator;
class RegimentGenerator;
class CampaignWorld;
#endif
class MarkedBattle;


typedef ULONG UnitID;


/*
 * Order of Battle Class
 */

class OrderBattle {


public:
	// GeneralList* generals;				// Complete list of generals
	President* sides;

public:
	OrderBattle();
	~OrderBattle();

	UnitID getID(Unit* u) const;
	Unit* getUnitPtr(UnitID id) const;

	UnitID getID(General* g) const;
	General* getGeneralPtr(UnitID id) const;

	void deleteUnit(Unit* u);

	// void assignUnit(Unit* unit, Unit* to, char *name = 0 );
	// Unit* makeNewUnder(Unit* unit, const Location& l, General* g, const char* name);

	void unassignUnit(Unit* u);
	void placeUnit(Unit* unit, Unit* to);
	void assignUnit(Unit* unit, Unit* to);
	Unit* makeNewUnder(Unit* unit, const Location& l);
	Unit* makeNew(Unit* unit, General* g);

	void assignGeneral(General* g, Unit* to);
	void unassignGeneral(General* g);
	void loseGeneral(General* g);
#if !defined(TESTBATTLE) && !defined(BATEDIT)
	void makeGeneral(Unit* to);
#endif

	void addFreeGeneral(General* g);
	void removeFreeGeneral(General* g);

	void addFreeRegiment(Regiment* r);

	void addSide(President* p);
	President* getSides() const { return sides; }

	void setSides();
	President* getPresident(Side side) const;

	/*
	 * Reassignments, etc
	 */

	void mergeRegiment(Regiment* src, Regiment* dest);

#if !defined(TESTBATTLE) && !defined(BATEDIT)
	void tidy();		// Remove Childless commanders, etc
#endif

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
	Unit* getNextBattleUnit(Unit* last, Rank maxRank, Rank minRank);
#endif

#if defined(DEBUG) && defined(CREATE_WORLD)
	void makeTestArmies(CampaignWorld* world);
	void makeNewSide(CampaignWorld* world, President* president, const Location& l, GeneralGenerator& gen, RegimentGenerator& reg);
#endif

#if !defined(CAMPEDIT) && !defined(TESTCAMP)		// && !defined(BATEDIT) 
	void setupBattle();
	void setupBattle(MarkedBattle* batl);
	void clearupBattle();
	void beforeBattle();
	void afterBattle();
#endif

#if !defined(CAMPEDIT) && !defined(BATEDIT)
	void clearInBattle();
	void daily();
	void moveCampaignUnits(const TimeBase& gameTime, GameTicks ticks);
	void setVisibility();
#endif

#if !defined(CAMPEDIT) && !defined(TESTBATTLE) && !defined(BATEDIT)
	void clearupDead();		// Clear out any almost dead regiments
#endif

#ifdef DEBUG
	void check(const char* file, int line);
#endif

private:
	// void kill(Unit* unit);
	Boolean tidyUnit(Unit* u);
	Boolean clearupDead(Unit* u);
	void updateDailyOrders();

	void checkUnit(Unit* u, const char* file, int line);
};

/*
 * These functions can be called without needing OB
 */




#endif /* OB_H */

