/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CALICON_H
#define CALICON_H

#ifndef __cplusplus
#error calicon.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	CAL Main Area Icons
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.24  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.1  1994/02/10  22:58:07  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include "calmain.h"
#include "generals.h"
/*
 * Forward reference
 */

class CAL;

/*
 * Icons within main window (Intrusive Linked List)
 */

class CAL_CommandIcon : public CALIcon {
protected:
	Unit* element;
	// GameSprites iconNumber;
	PointerIndex iconNumber;
public:
	CAL_CommandIcon(Unit* what, const Rect& r, PointerIndex n /* , PointerIndex m */) : CALIcon(r)
	{
		element = what;
		iconNumber = n;
		// mouseSprite = m;
	}

	virtual ~CAL_CommandIcon() { }

	void execute(Event* event, CAL* cal);
	void showInfo(CAL* cal);

#if !defined(TESTBATTLE)
	// Boolean pickup(CAL* cal);	// Pickup an item (return True if succesful)
	Boolean drop(CAL* cal, const Point& p);		// Drop an item
#endif

private:
#if !defined(TESTBATTLE)
	Boolean canDrop(CAL* cal);
#endif
};

#if !defined(TESTBATTLE)

class CAL_NewIcon : public CALIcon {
	Unit* superior;

	Boolean dropNew(CAL* cal);
	void makeGeneral(CAL* cal, Unit* p);
#if defined(CAMPEDIT) || defined(BATEDIT)
	void createNewUnit(CAL* cal);
#endif

public:
	CAL_NewIcon(Unit* s, Point p) : CALIcon(Rect(p, Point(17,17))) { superior = s; }

	void execute(Event* event, CAL* cal);
	void showInfo(CAL* cal);

private:
#if !defined(TESTBATTLE)
	Boolean canDrop(CAL* cal);
#endif
};

#endif

/*
 * Scrolling Icons
 */

class CAL_ScrollIcon : public CALIcon {
	Rank rank;
public:
	enum Direction { Left, Right };
protected:
	CAL_MainDisplay* main;
	Direction dir;
public:
	CAL_ScrollIcon(Rank r, Direction d, CAL_MainDisplay* i, Point p) : CALIcon(Rect(p, Point(24,16))) { rank = r; dir = d; main = i; }
	void execute(Event* event, CAL* cal);
	void showInfo(CAL* cal);
};



#endif /* CALICON_H */
