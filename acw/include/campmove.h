/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CAMPMOVE_H
#define CAMPMOVE_H

#ifndef __cplusplus
#error campmove.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Movement
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/06/24  14:45:10  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

#if 0	// Continuous time version

class Unit;

/*
 * Structure saved in stack
 */

enum LeftRight {
	Left,
	Right
};

enum CampaignFormation {
	Parallel,
	Line
};

struct CampaignMoveInfo {
	Unit* currentUnit;
	Unit* leftUnit;
	Unit* rightUnit;

#if defined(NOBITFIELD)
	LeftRight edge;
	CampaignFormation formation;
#else
	LeftRight edge:1;
	CampaignFormation formation:1;
#endif
};


#define MaxCampaignQueueDepth 5

class CampaignMoveQueue {
	CampaignMoveInfo info[MaxCampaignQueueDepth];	// Guaranteed never to exceed this depth
	UBYTE depth;

public:
	CampaignMoveInfo* current;

	CampaignMoveQueue();

	void pop();
	void push(Unit* unit, CampaignFormation form);
	void next();

	Boolean isEmpty() { return !depth; }
};
#endif

#endif /* CAMPMOVE_H */

