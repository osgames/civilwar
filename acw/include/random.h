/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef RANDOM_H
#define RANDOM_H

#ifndef __cplusplus
#error random.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Random Number Generating Class
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

class RandomNumber {
	union {
		UBYTE bytes[6];
		UWORD words[3];
		struct {
			UWORD wHi;
			ULONG lLo;
		} wl;
		struct {
			ULONG lHi;
			UWORD wLo;
		} lw;
	} value;

public:
	RandomNumber(ULONG start);		// Initialise with a seed
	RandomNumber();					// Initialise without a seed

	void seed(ULONG start);			// Set a new seed

	ULONG getL();						// Get a random number

	UWORD getW();

	UBYTE getB();

	/*
	 * 	Added if/then/else to the return s below
	 * 	due to crash calculating battlefield heights. CJW
	 */

	int get(int range)
	{
		return range ? getL() % range : 0;
	}

	ULONG getL(ULONG range)
	{
		return range ? getL() % range : 0;
	}

	UWORD getW(UWORD range)
	{
		return range ? getL() % range : 0;
	}

	UBYTE getB(UBYTE range)
	{
		return range ? getL() % range : 0;
	}

	ULONG operator () () { return getL(); }
	ULONG operator () (int n) { return get(n); }
	ULONG operator () (int from, int to) { return get(to - from) + from; }

#ifdef DEBUG
	ULONG getSeed() const			// Get value without changing it
	{
		return value.lw.lHi;
	}
#endif

private:
	void shuffle();
};


#endif /* RANDOM_H */

