/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef COLOURS_H
#define COLOURS_H

#ifndef __cplusplus
#error colours.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Indeces of commonly used colours
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.8  1994/09/23  13:30:51  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/07/13  13:54:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.5  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/04/20  22:24:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/04/11  13:38:20  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/03/01  22:30:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/02/28  23:05:43  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

enum Colours {
	Black 	= 0xf0,
	Blue 		= 0x38,
	Green 	= 0x64,
	// Cyan	 	= 3,
	Red 		= 0x14,
	// Purple	= 5,
	Yellow	= 0x5f,
	DGrey 	= 0xf2,
	LGrey 	= 0xfa,
	LBlue 	= 0x3f,
	LGreen 	= 0x6c,
	// LCyan	 	= 11,
	LRed 		= 0x17,
	// LPurple	= 13,
	LYellow	= 0x5f,
	White 	= 0xff,

	/*
	 * Colours Used for CAL boxes
	 */

	CAL_FillColour 	= 0x34,	// Was 0xb6
	CAL_IconColour		= 0x33,	// Was 0xb8
	CAL_IconSColour	= 0x37,	// Was 0xb1
	CAL_Frame1Colour	= 0x30,	// Was 0xbf
	CAL_Frame2Colour	= 0x36,	// Was 0xb2

	USAColour			= 0x3a,		// Normal colour
	CSAColour			= 0x16,
	USA_HColour			= 0x3f,		// Highlighted colour
	CSA_HColour			= 0x17,
	Neutral_Colour		= 0x26,		// Neutral side's colour
	Neutral_HColour	= 0x2f,

	CityColour			= 0x01,

	RailHeadColour		= 0x17,
	LandingColour		= 0x4e,
	Fort1Colour			= 0x15,
	Fort2Colour			= 0x14,

	MsgFillColour 		= 0x1d,		// Message Window Colours
	MsgBorderColour1 	= 0x1f,
	MsgBorderColour2 	= 0x50,

	/*
	 * Colours
	 */

	Orange1 = 1,
	Orange2,
	Orange3,
	Orange4,
	Orange5,
	Orange6,
	Orange7,

	yellowGrey0,
	yellowGrey1,
	yellowGrey2,
	yellowGrey3,
	yellowGrey4,
	yellowGrey5,
	yellowGrey6,
	yellowGrey7,

	Red0,
	Red1,
	Red2,
	Red3,
	Red4,
	Red5,
	Red6,
	Red7,

	Gold0,
	Gold1,
	Gold2,
	Gold3,
	Gold4,
	Gold5,
	Gold6,
	Gold7,

	RedBrown0,
	RedBrown1,
	RedBrown2,
	RedBrown3,
	RedBrown4,
	RedBrown5,
	RedBrown6,
	RedBrown7,
	RedBrown8,
	RedBrown9,
	RedBrowna,
	RedBrownb,
	RedBrownc,
	RedBrownd,
	RedBrowne,
	RedBrownf,

	BlueGrey0,
	BlueGrey1,
	BlueGrey2,
	BlueGrey3,
	BlueGrey4,
	BlueGrey5,
	BlueGrey6,
	BlueGrey7,

	Blue0,
	Blue1,
	Blue2,
	Blue3,
	Blue4,
	Blue5,
	Blue6,
	Blue7,

	Brown0,
	Brown1,
	Brown2,
	Brown3,
	Brown4,
	Brown5,
	Brown6,
	Brown7,
	Brown8,
	Brown9,
	Browna,
	Brownb,

	Blue8,
	Blue9,
	Bluea,
	Blueb,

	GreenYellow0,
	GreenYellow1,
	GreenYellow2,
	GreenYellow3,
	GreenYellow4,
	GreenYellow5,
	GreenYellow6,
	GreenYellow7,
	GreenYellow8,
	GreenYellow9,
	GreenYellowa,
	GreenYellowb,

	Yellow0,
	Yellow1,
	Yellow2,
	Yellow3,

	Green0,
	Green1,
	Green2,
	Green3,
	Green4,
	Green5,
	Green6,
	Green7,
	Green8,
	Green9,
	Greena,
	Greenb,
	Greenc,

	Flesh0,
	Flesh1,
	Flesh2,

	Grey0 = 0xf0,
	Grey1,
	Grey2,
	Grey3,
	Grey4,
	Grey5,
	Grey6,
	Grey7,
	Grey8,
	Grey9,
	Greya,
	Greyb,
	Greyc,
	Greyd,
	Greye,
	Greyf
};


#endif /* COLOURS_H */

