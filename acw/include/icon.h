/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef ICON_H
#define ICON_H

#ifndef __cplusplus
#error icon.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Icon & Hotspot Interface Definition
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.25  1994/09/23  13:30:51  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.22  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.17  1994/04/22  22:16:24  Steven_Green
 * Icon Handling Tidied Up
 *
 * Revision 1.9  1994/02/10  22:58:07  Steven_Green
 * Added virtual destructor
 *
 * Revision 1.4  1994/01/11  22:30:12  Steven_Green
 * menudata moved into its own file
 *
 * Revision 1.1  1994/01/06  22:40:53  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */


#include "gr_types.h"
#include "mouse.h"
#include "ptrlist.h"

class MenuData;
class IconSet;

/*
 * Disable parameter not used warning message
 */

// #pragma warning 14 9

/*
 * Key press
 */

typedef UWORD Key;
#define SpecialKey 0x8000

enum Keys {
	KEY_Enter	= 0x0d,
	KEY_Escape	= 0x1b,
	KEY_Space	= 0x20,

	KEY_Home			= 0x47 + SpecialKey,
	KEY_CtrlHome	= 0x77 + SpecialKey,
	KEY_AltHome		= 0x97 + SpecialKey,

	KEY_End			= 0x4f + SpecialKey,
	KEY_CtrlEnd		= 0x75 + SpecialKey,
	KEY_AltEnd		= 0x9f + SpecialKey,
	
	KEY_Up			= 0x48 + SpecialKey,
	KEY_PgUp			= 0x49 + SpecialKey,

	KEY_Left			= 0x4b + SpecialKey,
	KEY_CtrlLeft 	= 0x73 + SpecialKey,
	KEY_AltLeft		= 0x9b + SpecialKey,

	KEY_Right 		= 0x4d + SpecialKey,
	KEY_CtrlRight 	= 0x74 + SpecialKey,
	KEY_AltRight 	= 0x9d + SpecialKey,
	
	KEY_Down			= 0x50 + SpecialKey,
	KEY_PgDown		= 0x51 + SpecialKey,

	KEY_BackSpace		= 8,
	KEY_CtrlBackSpace	= 0x7f,
	KEY_AltBackSpace	= 0x0e + SpecialKey,

	KEY_Insert			= 0x52 + SpecialKey,
	KEY_Delete			= 0x53 + SpecialKey,
	KEY_CtrlDelete	 	= 0x93 + SpecialKey,
	KEY_AltDelete	 	= 0xa3 + SpecialKey,

	KEY_F1				= 0x3b + SpecialKey,
	KEY_F2				= 0x3c + SpecialKey,
	KEY_F3				= 0x3d + SpecialKey,
	KEY_F4				= 0x3e + SpecialKey,
	KEY_F5				= 0x3f + SpecialKey,
	KEY_F6				= 0x40 + SpecialKey,
	KEY_F7				= 0x41 + SpecialKey,
	KEY_F8				= 0x42 + SpecialKey,
	KEY_F9				= 0x43 + SpecialKey,
	KEY_F10				= 0x44 + SpecialKey,

	KEY_AltA				= 0x1e + SpecialKey,
	KEY_AltB				= 0x30 + SpecialKey,
	KEY_AltC				= 0x2e + SpecialKey,
	KEY_AltD				= 0x20 + SpecialKey,
	KEY_AltE				= 0x12 + SpecialKey,
	KEY_AltF				= 0x21 + SpecialKey,
	KEY_AltG				= 0x22 + SpecialKey,
	KEY_AltH				= 0x23 + SpecialKey,
	KEY_AltI				= 0x17 + SpecialKey,
	KEY_AltJ				= 0x24 + SpecialKey,
	KEY_AltK				= 0x25 + SpecialKey,
	KEY_AltL				= 0x26 + SpecialKey,
	KEY_AltM				= 0x32 + SpecialKey,
	KEY_AltN				= 0x31 + SpecialKey,
	KEY_AltO				= 0x18 + SpecialKey,
	KEY_AltP				= 0x19 + SpecialKey,
	KEY_AltQ				= 0x10 + SpecialKey,
	KEY_AltR				= 0x13 + SpecialKey,
	KEY_AltS				= 0x1F + SpecialKey,
	KEY_AltT				= 0x14 + SpecialKey,
	KEY_AltU				= 0x16 + SpecialKey,
	KEY_AltV				= 0x2F + SpecialKey,
	KEY_AltW				= 0x11 + SpecialKey,
	KEY_AltX				= 0x2d + SpecialKey,
	KEY_AltY				= 0x15 + SpecialKey,
	KEY_AltZ				= 0x2c + SpecialKey,
};

/*
 * Event structure passed to execute
 */

class Event {
public:
	Boolean overIcon;
	ButtonStatus buttons;
	Point where;
	Key key;

	// Event()
};

/*
 * Icon Class
 */

class Icon : public Rect {
	Key leftKey;
	Key rightKey;

	Boolean reDraw:1;			// set to force redraw next frame

protected:
	IconSet* parent;

	virtual void drawUpdate();

public:
	Icon(IconSet* set, Rect r, Key lkey = 0, Key rkey = 0) : Rect(r), leftKey(lkey), rightKey(rkey) { parent = set; reDraw = True; }
	virtual ~Icon() {	}

	ButtonStatus matchKey(Key key);

	Point getPosition() const;

	void setRedraw() { reDraw = True; }

	void redrawIcon() { reDraw = False; drawIcon(); }

#ifdef DEBUG
	virtual void execute(Event* event, MenuData* data);
	virtual void drawIcon();
#else
	virtual void execute(Event* event, MenuData* data) { event = event; data = data; }
	virtual void drawIcon() { }
#endif
	friend class IconSet;
};


/*
 * A set of icons that are nested together
 */

typedef Icon* IconList;

class IconSet : public Icon {
protected:
	IconList* icons;			// An array of pointers to icons

	void drawUpdate();
public:
	// IconSet(IconSet* set, IconList* i, Rect r) : Icon(set, r) { icons = i; }
	IconSet(IconSet* parent, Rect r);
	virtual ~IconSet() { killIcons(); }

	void killIcons();

	void execute(Event* event, MenuData* data);
	void drawIcon();

	void setIcons(IconList* ic) { icons = ic; }

};


/*
 * A sub menu is a group of icons that can be replaced by another
 */

class SubMenu : public IconSet {
	PtrSList<IconList> last;		// Linked list of old menus
public:
	SubMenu(IconSet* set, Rect r) : IconSet(set, r) { }
	~SubMenu();

	void add(IconList* i);
	void remove();
	void kill();
	void destroy();
};

#endif /* ICON_H */

