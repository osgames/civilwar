/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef TABLES_H
#define TABLES_H

#ifndef __cplusplus
#error tables.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Tables used by all parts of game
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "measure.h"
#include "gamedef.h"
#include "side.h"
#include "orders.h"
#include "language.h"

extern Speed regimentMarchSpeeds[3][4];
extern Speed commanderMarchSpeed;

extern LGE infantryTypeNames[];
extern LGE cavalryTypeNames[];
extern LGE artilleryTypeNames[];

struct ValueEnum {
	LGE name;
	AttributeLevel range;
};

const char* getAttributeName(ValueEnum* table, AttributeLevel level);
UBYTE getAttributeEnum(ValueEnum* table, AttributeLevel level);

extern ValueEnum fatigueLevels[];
extern ValueEnum experienceLevels[];
extern ValueEnum moraleLevels[];
extern ValueEnum efficiencyLevels[];
extern ValueEnum abilityLevels[];
extern ValueEnum aggressionLevels[];
extern ValueEnum supplyLevels[];

inline const char* fatigueStr(Fatigue f)
{
	return getAttributeName(fatigueLevels, f);
}

inline UBYTE getFatigueEnum(Fatigue f)
{
	return getAttributeEnum(fatigueLevels, f);
}

inline const char* experienceStr(Experience e)
{
	return  getAttributeName(experienceLevels, e);
}

inline UBYTE getExperienceEnum(Experience e)
{
	return getAttributeEnum(experienceLevels, e);
}

inline const char* moraleStr(Morale m)
{
	return getAttributeName(moraleLevels, m);
}

inline UBYTE getMoraleEnum(Morale m)
{
	return getAttributeEnum(moraleLevels, m);
}

inline const char* efficiencyStr(AttributeLevel e)
{
	return getAttributeName(efficiencyLevels, e);
}

inline UBYTE getEfficiencyEnum(AttributeLevel e)
{
	return getAttributeEnum(efficiencyLevels, e);
}

inline const char* abilityStr(AttributeLevel e)
{
	return getAttributeName(abilityLevels, e);
}

inline UBYTE getAbilityEnum(AttributeLevel e)
{
	return getAttributeEnum(abilityLevels, e);
}

inline const char* aggressionStr(AttributeLevel e)
{
	return getAttributeName(aggressionLevels, e);
}

inline UBYTE getAggressionEnum(AttributeLevel e)
{
	return getAttributeEnum(aggressionLevels, e);
}

inline const char* supplyStr(AttributeLevel f)
{
	return getAttributeName(supplyLevels, f);
}

inline UBYTE getSupplyEnum(AttributeLevel f)
{
	return getAttributeEnum(supplyLevels, f);
}


extern const LGE rankNames[];
// extern const char* rankNames[];

inline const char* rankStr(Rank rank)
{
	return language(rankNames[rank]);
}

const char* getOrderStr(OrderMode mode);
const char* getOrderStr(const Order& order);
const char* getOrderStr(const BattleOrder& order);

extern int BrigadeCombatValue[3][4];


#ifdef DEBUG

extern const char* sideStr[];

const char* getBoolStr(Boolean flag);

#endif

const char* getSideStr(Side side);

#endif /* TABLES_H */

