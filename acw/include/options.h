/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef OPTIONS_H
#define OPTIONS_H

#ifndef __cplusplus
#error options.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Option settings
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.6  1994/09/23  13:29:31  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/05/04  22:12:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/12/14  23:30:30  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifndef TYPES_H
#include "types.h"
#endif


class Options {
public:
	enum OptionBit {
		Gourad  = 0x0001,
		Texture = 0x0002,
		Show	  = 0x0004,			// Display polygons while drawing
		HSL	  = 0x0008,			// Use HSL model instead of HSV

		Grid	  = 0x0010,			// Show grid on zoomed campaign map
		Terrain = 0x0020,			// Show terrain on zoomed campaign map

		RailShow = 0x0040,		// Display rail network
		WaterShow = 0x0080,		// Display water network

		QuickMove =0x0100,		// Force Instant Movement
	};

	typedef unsigned short OptionState;

private:
	OptionState settings;

public:
	Options(): settings(Gourad | Texture) { }

	void set(OptionBit what) { settings |= what; }
	void clear(OptionBit what) { settings &= OptionState(~what); }
	Boolean toggle(OptionBit what) { settings ^= what; return settings & what; }
	Boolean get(OptionBit what) { return settings & what; }
};

extern Options globalOptions;

inline Boolean optGourad() { return globalOptions.get(Options::Gourad); }
inline Boolean optTexture() { return globalOptions.get(Options::Texture); }
inline Boolean optShow() { return globalOptions.get(Options::Show);	}
inline Boolean optHSL() { return globalOptions.get(Options::HSL);	}

inline Boolean optGrid() { return globalOptions.get(Options::Grid);	}
inline Boolean optTerrain() { return globalOptions.get(Options::Terrain);	}

inline Boolean optRailShow() { return globalOptions.get(Options::RailShow); }
inline Boolean optWaterShow() { return globalOptions.get(Options::WaterShow); }

inline Boolean optQuickMove() { return globalOptions.get(Options::QuickMove); }

#ifdef DEBUG
extern UBYTE AI_ForceOption;
extern Boolean showAI;				// Display AI values on map
extern Boolean dontMove;			// Skip movement (only do AI)
extern Boolean instantBuild;		// Mobilising objects happens on same day.
extern Boolean fastResources;		// Build resources at 4 times normal rate
extern Boolean quickMove;			// Move in whole terrain squares at once
#endif
extern Boolean seeAll;				// Show and allow control of all units on all sides
extern Boolean smoothClock;		// Set to enable clock going round in campaign movement
extern Boolean noBuildMessages;	// Don't display build messages
extern Boolean realTimeCampaign;	// Campaign is real time

enum RouteMode {
	RM_Full,
	RM_Simple,
	RM_Line
};
extern RouteMode routeMode;
extern int aiPasses;

#endif /* OPTIONS_H */

