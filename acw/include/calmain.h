/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CALMAIN_H
#define CALMAIN_H

#ifndef __cplusplus
#error calmain.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Internal CAL classes
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.24  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.18  1994/04/22  22:16:24  Steven_Green
 * Icon Handling Tidied Up
 *
 * Revision 1.1  1994/02/10  22:58:07  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <strstrea.h>

#include "menudata.h"
#include "gameicon.h"
#include "colours.h"

class Unit;
class General;
class President;
class Region;
class OrderBattle;
class Regiment;
class SpriteBlock;

/*
 * Where the things are on screen
 */

#define CAL_X 			5
#define CAL_Y 			47
#define CAL_WIDTH 	630
#define CAL_HEIGHT 	321

#define CALREG_X			8
#define CALREG_Y			377
#define CALREG_WIDTH		288
#define CALREG_HEIGHT	97

#define CALGEN_X			300
#define CALGEN_Y			377
#define CALGEN_WIDTH		215
#define CALGEN_HEIGHT	97

#define CAL_RegimentTop 	179
#define CAL_RegimentHeight 23
#define CAL_RegimentWidth 	34

#define CAL_NewWidth 		20

#define CAL_OverLine 		8

#define CAL_INFO_X			5
#define CAL_INFO_Y			5
#define CAL_INFO_W			630
#define CAL_INFO_H			38

#define CAL_OK_X				519
#define CAL_OK_Y				373
#define CAL_OK_W				113
#define CAL_OK_H				105

/*
 * Forward References
 */

class CAL_InfoWindow;
class CAL;

class CAL_InfoIcon {
public:
	virtual ~CAL_InfoIcon() {}
	virtual void showInfo(CAL* cal) = 0;
};

/*
 * Return values for mode
 */

enum CAL_ReturnMode {
	CAL_Find,
	CAL_GiveOrder
};

/*
 * The Main CAL Class
 */

class CAL : public MenuData {	// , public GeneralMode {
friend class CAL_MainDisplay;

public:
	CAL_InfoWindow* infoWindow;			// ostrstream for text

	enum PickMode {
		Picking,
		Dropping
	};

	SpriteBlock* fillPattern;		// Pattern used to fill in display

private:
#if defined(NOBITFIELD)
	PickMode pickMode;				// Picking or Dropping mode?
#else
	PickMode pickMode:2;				// Picking or Dropping mode?
#endif

public:
	CAL_InfoIcon* currentIcon;
#if defined(NOBITFIELD)
	Boolean dragRecursive;		// Set if dragging all units (as opposed to General)
#else
	Boolean dragRecursive:1;		// Set if dragging all units (as opposed to General)
#endif

	PointerIndex dragPointer;

public:
	Boolean campaignMode;
	OrderBattle* ob;
	President* top;			// Unit at the top of the tree

	Unit*		pickedUnit;		// Unit currently being dragged
	General* pickedGeneral;	// General currently being dragged

	Unit* currentUnit;		// Unit that the CAL was entered on and to be returned

	int tops[5];

	void setCurrentUnit(Unit* newUnit);

	void updateTops(Unit* topUnit);

	void setPickMode();
	void setDropMode(PointerIndex icon);

	PickMode getPickMode() const { return pickMode; }

	void showDropPointer();

public:
	CAL(OrderBattle* b, Unit* unit, MenuData* proc, Boolean inCampaign);
	~CAL();

	void drawMain(const Rect& r);		// Draw the main area within rectangle

	void doPickup(Unit* unit, General* general, PointerIndex icon, const Point& p);

#if defined(CAMPEDIT) || defined(BATEDIT)
	void options();
#endif

	Boolean unitIsValid(Unit* u);
};


/*
 * Useful baseclass for parts of the CAL screen to share info
 */

class CALBase {
public:
	CAL* cal;

	CALBase(CAL* c) { cal = c; }
};

/*
 * Unallocated Regiment Window
 */

class CAL_Regiments : public IconSet, public CALBase {
public:
	int top;
	int nLines;
public:
	CAL_Regiments(CAL* c);
	void drawIcon();
};

/*
 * Unallocated Generals Window
 */

class CAL_Generals : public IconSet, public CALBase  {
public:
	int top;
	int nLines;
public:
	CAL_Generals(CAL* c);
	void drawIcon();
};

/*
 * A hotspot within an area
 */

class CALIcon : public Rect, public CAL_InfoIcon {
public:
	CALIcon(const Rect& r) : Rect(r) { }
	virtual ~CALIcon() {	}

	virtual void execute(Event* event, CAL* cal) = 0;
};


/*
 * Info Area (as an Icon for easy display)
 */

class CAL_InfoWindow : public Icon, public CALBase {
public:
public:
	CAL_InfoWindow(CAL* cal);

	void drawIcon();

	void clear();
	void showInfo(Unit* p);
	void showInfo(General* g);
	void showText(const char* s);
private:
	void showGeneralInfo(General* g);
};		 	

/*
 * Main Display Area
 */

class CAL_MainDisplay : public Icon, public CALBase {
private:

	/*
	 * Highlight method
	 */

	enum CAL_Highlight {
		HL_None,					// No highlighting
		HL_Picked,				// Processing picked unit
		HL_PickedChild,		// Processing picked unit's children
		HL_Current,				// Processing current Unit
		HL_CurrentChild		// Processing current Unit's children
	};

	int currentX;
	Boolean noSpace;			// Set when there is no more room to display anything
	Boolean doingFirst;

	PtrSList<CALIcon> icons;
	void removeIcons();
	void addIcon(CALIcon* i);
	void drawCALCommander(Region* bm, int midX, Unit* unit, CAL_Highlight hl);
	
	void showUnit		(Region* bm, Unit* unit, CAL_Highlight hl);
	int showCommand	(Region* bm, Unit* unit, CAL_Highlight hl);
	int showBrigade	(Region* bm, Unit* brig, CAL_Highlight hl);
	void showRegiment	(Region* bm, Regiment* reg, int y, CAL_Highlight hl);

	void updateX(int add);
	int showNewIcon(Region* bm, int x, int y, Unit* superior, Colour col);
	SDimension CAL_MainDisplay::getCALwidth(Unit* unit, SDimension x);

public:
	CAL_MainDisplay(CAL* c);
	~CAL_MainDisplay();

	void drawIcon();
	void execute(Event* event, MenuData* data);

private:
	void showUnitStatus(Region* bm, const Rect& r, Unit* u, CAL_Highlight hl);
};

class CAL_OkIcon : public MenuIcon, public CALBase {
public:
	CAL_OkIcon(CAL* c);

	void execute(Event* event, MenuData* data);
};


extern GameSprites calRegimentIcon[];
extern PointerIndex calRegimentMice[];

extern CAL* cal;		// Global Pointer to currently displayed CAL.

#endif /* CALMAIN_H */

