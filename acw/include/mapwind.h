/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MAPWIND_H
#define MAPWIND_H

#ifndef __cplusplus
#error mapwind.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Map Window Class
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.22  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.1  1994/01/20  20:04:54  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include "measure.h"
#include "icon.h"
#include "mapob.h"
#include "image.h"
#include "zoom_map.h"

/*
 * Undefined Classes
 */

class CampaignMode;

/*
 * Screen coordinates within overall map window
 */

/*
 * Some constants
 */


#define MapSectionPwidth	480				// Pixel Size of drawn map section
#define MapSectionPheight	340

#define MapSectionWidth		(MapSectionPwidth * 65536)		// Physical size of drawn map section
#define MapSectionHeight	(MapSectionPheight * 65536)
#define MapMaxX				(5 * MapSectionWidth)			// Physical size of overall map
#define MapMaxY				(5 * MapSectionHeight)

#define MapMaxPX				(5 * MapSectionPwidth)			// Overall Pixel size
#define MapMaxPY				(5 * MapSectionPheight)

/*
 * A List of objects on the screen is built up
 * It contains a list of priorities, each with a list of objects.
 *
 *  p0 -> p1 -> p2 -> ...
 *   |	 |		 |
 *   v	 v		 v
 *  ob1  ob3   ob5
 *   |	 |		 |
 *   v	 v		 v
 *  ob2  ob4   ob6
 *   |	 |		 |
 *   v	 v		 v
 *  ...  ...   ...
 */


struct MapDisplayObject {
	MapDisplayObject* next;
	MapObject* ob;
	Point screenPos;		// Where it is on screen
};

struct MapPriorityObject {
	MapPriorityObject* next;
	MapPriorityObject* prev;

	MapPriority priority;
	MapDisplayObject* objects;

	/*
	 * Functions
	 */

	~MapPriorityObject();
};

class MapDisplayList {
friend class MapDisplayIter;

	MapPriorityObject* entry;		// Double list so can go backwards
	MapPriorityObject* last;
public:
	MapDisplayList();
	~MapDisplayList();

	void add(MapObject* ob, const Point& where, MapPriority priority);
	void clear();
	void display(MapWindow* map) const;

	int entries() const { return entry != 0; }
};

class MapDisplayIter {
	MapPriorityObject* plist;
	MapDisplayObject* dob;
public:
	MapDisplayIter(MapDisplayList& list);

	MapDisplayObject* current() const { return dob; }
	MapPriorityObject* currentPlist() const { return plist; }

	int operator ++();
};


/*
 * Map Window Class
 *
 * An IconSet that includes the entire map and associated scroll bars, etc
 */

class MapWindow : public IconSet {

	/*
	 * Types
	 */

public:
	enum MapStyle { Full, Zoomed };

	enum DisplayType {
		Disp_Moveable,		// Items to be redisplayed each frame
		Disp_Static			// Items that can be placed on static map
	};

	/*
	 * Data
	 */

private:
	Bitmap staticMap;			// Where unmoveable parts of the map go
	Region region;				// Where the main map is on screen

	DiskBM zoomMap;			// Structure for accessing bit Zoomed bitmap

	Location mapTop;				// Location at top of map
	Location mapCentre;			// Location map is centred on

	Location displaySize;		// Physical size being displayed


#ifdef DEBUG
	Location mouseLocation;		// Where the mouse is
#endif

public:
	CampaignMode* mapMode;		// Class to handle clicking on map area
	MapStyle mapStyle;			// Zoom level
	MapDisplayList onScreenStatic;
	MapDisplayList onScreenMoveable;
	
	/*
	 * Functions
	 */

public:
 	MapWindow(IconSet* parent, CampaignMode* mode);
	MapWindow(IconSet* parent, CampaignMode* mode, MapStyle style, const Location& l);
	~MapWindow();

	void setStyle(MapStyle s, const Location& l);		// Set up zoom and location
	void toggleStyle(MenuData* d);
	void toggleZoom(const Location& l);

	void mapRedraw();						// Ask for map to be redrawn

	void makeStaticMap();				// Setup static map
	void overMap(Event* event, MenuData* d);
	void scroll(Distance dx, Distance dy);
	const Location& getMapTop() const { return mapTop; }
	const Location& getMapCentre() const { return mapCentre; }

	SDimension getH() { return region.getH(); }
	SDimension getW() { return region.getW(); }

	void drawObject(MapObject* ob);

	Point locationToPixel(const Location& l);		// Convert location to screen pixels

#ifdef DEBUG
	void showTerrain(Region* bm);
	void putTestPixel(Point p);
#endif
#ifdef CAMPEDIT
	void show1Terrain(const Location& l, UBYTE t);
#endif

	void draw();							// Display the map!

private:
	void drawObjects(DisplayType type);
	void makeZoomedMap();



	/*
	 * These might not be used?
	 */

	void setLocation(const Location& l);
	Location pixelToLocation(const Point& p);

	void setFull();
	void setZoom();

	/*
	 * Stuff used by campaign draw functions
	 */

public:
	Region* drawBM;					// Bitmap being drawn to.
	DisplayType displayType;		// static or moveable?
};

#endif /* MAPWIND_H */

