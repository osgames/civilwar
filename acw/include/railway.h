/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef RAILWAY_H
#define RAILWAY_H

#ifndef __cplusplus
#error railway.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Railway handling
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.1  1994/05/19  17:47:49  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include "city.h"
#include "generals.h"

/*
 * Structure used to record each cities status
 */

struct RailRoute {
	FacilityID from;		// Where did we come from to get here
	GameTicks cost;		// How long does it take to get here?
};

struct RailRouteInfo {
	RailRoute* route;		// Array of route info

	RailRouteInfo() { route = 0; }
	~RailRouteInfo() { if(route) delete[] route; }
	void drawRoute(MapWindow* map, Region* bm, FacilityList& facilities, FacilityID from, FacilityID to) const;
};

#ifdef DEBUG
class MapWindow;
class FacilityList;

#ifdef CAMPEDIT
void showRailways(MapWindow* map, FacilityList& facilities);
#else
class RailSectionList;

void showRailways(MapWindow* map, FacilityList* facilities, RailSectionList* rails);
#endif	// CAMPEDIT
#endif	// DEBUG

/*
 * Platform Structures are used to store unit's waiting for a train
 */

class Train;
class ChunkFileRead;
class ChunkFileWrite;

class Platform {
	friend class RailNetwork;
	friend void writeDynamicRail(ChunkFileWrite& file, RailNetwork& rails, const OrderBattle& ob);
	friend void readDynamicRail(ChunkFileRead& file, RailNetwork& rails, const OrderBattle& ob);

	Platform* next;

public:
	FacilityID station;
	FacilityID destination;		// 1st station that it is heading for
	Side side;						// What side is the platform on?
	PtrDList<Unit> waiting;		// List of Units waiting to get on

#if !defined(CAMPEDIT)
	Platform(FacilityID start, FacilityID end, Side owner);
#endif

	Platform();
	~Platform();
};

/*
 * Trains are collections of Units in-transit between stations
 *
 * Trains continue moving through stations until either someone
 * wants to get off, the next station is enemy controlled, or
 * it is a junction, in which case everyone is offloaded to a
 * platform and the train returns to its home.
 */

class Train {
	friend class RailNetwork;
	friend void writeDynamicRail(ChunkFileWrite& file, RailNetwork& rails, const OrderBattle& ob);
	friend void readDynamicRail(ChunkFileRead& file, RailNetwork& rails, const OrderBattle& ob);

	Train* next;
	FacilityID owner;			// Where it lives
	FacilityID lastStation;	// The last visited station
	FacilityID nextStation;	// Next station on-line
	ULONG spaces;				// Number of empty seats
	PtrDList<Unit> passengers;		// List of Units on the train!
	Side side;

	enum TrainMode { TR_Loading, TR_Moving } mode;
	TimeBase arrival;		// Time to depart/arrive

public:
	Train(FacilityID home, FacilityID destination, Side owner, const TimeBase& now);
	Train();
	~Train();

#if !defined(CAMPEDIT)
	Boolean moveTrain(RailNetwork* net, const TimeBase& now);
	void returnHome();
private:
	void moveToNextStation();
#endif
};


class RailNetwork {
	friend void writeDynamicRail(ChunkFileWrite& file, RailNetwork& rails, const OrderBattle& ob);
	friend void readDynamicRail(ChunkFileRead& file, RailNetwork& rails, const OrderBattle& ob);

	Train* trains;
	Platform* platforms;

	TimeBase lastTime;			// Last time railways were processed

	/*
	 * These 2 used to avoid recalculating rail route
	 * when many units transferred at once
	 */

	FacilityID lastStart;			// Previous starting point
	FacilityID lastDestination;	// Previous destination
	FacilityID lastStation;			// Result of previous route search

public:
	RailNetwork();
	~RailNetwork();

	void clear();

#if !defined(CAMPEDIT)
	void add(Unit* unit, FacilityID from, FacilityID to);
	// void remove(Unit* unit);

	void process(const TimeBase& t);
	void unloadPassengers(Boolean all, Boolean foot, Train* train, const Location& l);

	void removeUnit(Unit* u);
private:
	Train* makeTrain(Platform* where, const TimeBase& t);
#endif	// CAMPEDIT
};

#endif /* RAILWAY_H */

