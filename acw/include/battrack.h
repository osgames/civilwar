/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef BATTRACK_H
#define BATTRACK_H

#ifndef __cplusplus
#error battrack.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Battle Tracking
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

class LinkListBase {
	LinkListBase* next;
	LinkListBase* prev;

public:
	LinkListBase() { next = this; prev = this; }
	LinkListBase* getNext() const { return next; }
	LinkListBase* getPrev() const { return prev; }

	void addAfter(LinkListBase* newNode);
	void addBefore(LinkListBase* newNode);

	void remove();
};

class TrackObject : public LinkListBase {
public:
	Unit* unit;					// Unit
	SDimension d;				// Distance from Mouse
	Boolean inUse;

	TrackObject* getNext() const { return (TrackObject*) LinkListBase::getNext(); }
	TrackObject* getPrev() const { return (TrackObject*) LinkListBase::getPrev(); }
};

class TrackedUnitList {
	TrackObject entry;			// Dummy first entry
	int count;						// How many entries?
	Point lastPosition; 			// Last mouse coordinate

public:
	TrackedUnitList();
	~TrackedUnitList();

	void clearInUse();
	TrackObject* findUnit(Unit* u) const;
	void append(TrackObject* ob);
	int removeUnused();
	void sort();
	int entries() const { return count; }
	Unit* getTop() const;	// Return top object

	void next();				// Cycle through stacked objects
	void prev();

	int countValid() const;
	TrackObject* getNextValid(TrackObject* ob) const;

	Boolean updatePosition(const Point& p);

private:
	void remove(TrackObject* ob);
	Boolean isValid(Unit* u) const;

};

#endif /* BATTRACK_H */

