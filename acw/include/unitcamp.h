/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef UNITCAMP_H
#define UNITCAMP_H

#ifndef __cplusplus
#error unitcamp.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Units/Generals things that only concern the campaign game
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:38  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/08/31  15:26:29  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "generals.h"

class City;

void makeNewRegiment(City* where, BasicRegimentType type, RegimentSubType subType);


#endif /* UNITCAMP_H */

