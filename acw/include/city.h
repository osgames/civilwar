/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CITY_H
#define CITY_H

#ifndef __cplusplus
#error city.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Cities and Facilities
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.28  1994/07/28  19:01:36  Steven_Green
 * regimentCount added to City data
 *
 * Revision 1.21  1994/06/09  23:36:46  Steven_Green
 * Added States
 *
 * Revision 1.18  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.1  1994/01/20  20:04:54  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#include "gamedef.h"
#include "supply.h"
#include "resource.h"
#include "mapob.h"
#include "array.h"
#include "mobilise.h"

#if !defined(CAMPEDIT)
#include "aic_even.h"
#endif

class Region;
class TextWindow;
class Unit;
// class DataFileOutStream;
/*
 * Somewhere that a facility can be.
 *
 * There are several types of these, which are built using this as
 * a base class:
 *   City
 *   RailHead
 *   other
 */

class City;


#define MaxFortification	4		// 4 Levels of fortification
#define MaxRailHead			99		// 99 Railhead capacity allowed
#define MaxBlockade			20		// 20 Blockade runners per city
#define MaxVictory			0xff

#define MaxStateName			64
#define MaxFacilityName		64

/*
 * State (every city belongs to a state).
 *
 * Its location is where the state name will be
 *
 * The owner always remains constant, even though the cities within
 * a state can be taken over by different sides.
 */

typedef UBYTE StateID;		// Less than 256 states.
#define NoState 0xff

class State : public MapObject {
public:
#ifdef CAMPEDIT
	char fullName[MaxStateName];
	char shortName[MaxStateName];
#else
	char* fullName;
	char* shortName;
#endif
	Side side;			// USA, CSA or Neutral.
	UBYTE regimentCount;			// Number of Regiments built here!

public:
	State();
	State(const Location& l): MapObject(l, OT_Other) { }
	~State();

	void mapDraw(MapWindow* map, Region* bm, Point where);
	MapPriority isVisible(MapWindow* map) const;
#ifdef CAMPEDIT
	void showInfo(Region* window);
#endif
};

#ifdef CAMPEDIT

class StateList : public DynamicArray<State> {
public:
};

typedef DynamicArrayIter<State> StateListIter;

#else

class StateList : public Array<State> {
public:
};
typedef ArrayIter<State> StateListIter;

#endif	// CAMPEDIT


/*
 * A Base facility
 */

class Facility : public MapObject {
public:
	enum FacilityTypes {
		F_None					= 0,

		F_SupplyDepot 			= 0x0001,		// Facility contains a supply depot
		F_RecruitmentCentre	= 0x0002,
		F_TrainingCamp			= 0x0004,
		F_Hospital				= 0x0008,
		F_POW						= 0x0010,
		F_Port					= 0x0020,		// Port and landing stage are
		F_LandingStage			= 0x0040,		// calculated based on various things
		F_City					= 0x0080,
		F_KeyCity				= 0x0200,		// City is a key city
		F_RailEngineer			= 0x0400,		// Facility has rail engineers
	};

	typedef UWORD FacilityFlag;

public:
	FacilityID owner;				// City that is its key city

#ifdef CAMPEDIT
	char name[MaxFacilityName];
#else
	char* name;
#endif

	FacilityFlag facilities;
	StateID state;							// What state is it in?
#ifndef CAMPEDIT
#if defined(NOBITFIELD)
	Side side;							// Which side controls it?
	UBYTE fortification;				// 0 means no fortification
#else
	Side side:2;							// Which side controls it?
	UBYTE fortification:3;				// 0 means no fortification
#endif
#else
	Side side;							// Which side controls it?
	UBYTE fortification;				// 0 means no fortification
#endif
	Supply supplies;						// How well supplied is it?

#ifdef CHRIS_AI
	/*
	 * Used by AI
	 */

#if defined(NOBITFIELD)
	Boolean attacking;
	Boolean reinforcing;
	Boolean AISkipFlag;
#else
	Boolean attacking:1;
	Boolean reinforcing:1;
	Boolean AISkipFlag:1;
#endif

#endif	// CHRIS_AI

	/*
	 * Used by Campaign AI
	 */

#if !defined(CAMPEDIT)
	AIC_Priority aiDefend;				// How much does it need defending?
#ifdef DEBUG
	AIC_Priority aiAttack;				// How much does it need attacking
#endif
#endif

	Unit* occupier;						// Unit that is occupying facility (0 if none)
	Unit* sieger;							// Unit that is sieging it

	UBYTE railCapacity;					// How many trains belong at this place?
	UBYTE freeRailCapacity;				// Trains currently available
#ifdef CAMPEDIT
	DynamicArray<FacilityID> connections;
#else
	UBYTE railCount;						// Number of railway connections
	RailSectionID railSection;			// Entry into RailSection table
#endif

	UBYTE waterCapacity;
	UBYTE freeWaterCapacity;
	UBYTE blockadeRunners;				// Number of blockade runners

	WaterZoneID waterZone;				// What water zone it is in

private:
	UBYTE victory;							// Value for victory conditions

public:
	Facility();
#ifdef CREATE_CITIES
	Facility(const char* s, const Location& l, Side sd, FacilityFlag f = F_None);
#endif
	virtual ~Facility();

	virtual MapPriority isVisible(MapWindow* map) const;
	virtual void mapDraw(MapWindow* map, Region* bm, Point where);

#ifdef CAMPEDIT
	void showInfo(Region* window);
#else
	virtual void dailyProcess();

	void showCampaignInfo(Region* window);
	// void showResource(Region* window, ResourceType type, const ResourceSet* local, const ResourceSet* imported) const;

	// void placeInfo(Region* window, FacilityInfo& info, Boolean iconize) const;
	// void getBuildInfo(FacilityInfo& info);
#endif	// CAMPEDIT

#ifdef CAMPEDIT
	const char* getName() const
	{
		if(name[0])
			return name;
		else
			return "Unnamed";
	}
#else
	const char* getName() const { return name; }
#endif
	const char* getNameNotNull() const;

	virtual ResourceSet* getLocalResources() { return 0; }
	ResourceSet* getGlobalResources();

	void setOwner(FacilityID c) { owner = c; }

	Supply getSupplyLevel() const;		// Return supply level taking into account difficulty levels

	UWORD getBigVictory() const { return victory * 16; }
	UBYTE getVictory() const { return victory; }
	void setVictory(UBYTE value) { victory = value; }

	Boolean isRealTown() const { return (name != 0); }		// False if only an un-named railhead!

	virtual City* getKeyCity() const;
	virtual operator City* () { return 0; }
};

/*
 * A City is a NamedFacility that has Resource Points
 * with which to mobilise new units
 */

typedef UBYTE CitySize;
typedef UBYTE CityType;	// Bodge to avoid :2 maing enum -ve!
#define MaxCitySize 63


enum _CityType {
	Industrial,
	Agricultural,
	Mixed,
	Other
};

class City : public Facility {
public:
	ResourceSet resource;		// What it contains
#ifdef CAMPEDIT
	CityType type;
	CitySize size;
#else
#if defined(NOBITFIELD)
	CityType type;
	CitySize size;
#else
	CityType type:2;
	CitySize size:6;
#endif
#endif

public:
	City();
#ifdef CREATE_CITIES
	City(const char* s, const Location& l, Side owner, FacilityFlag f);
#endif

#ifdef CAMPEDIT
	void showInfo(Region* window);
#else
	// void placeInfo(Region* window, FacilityInfo& info, ShowInfoMode mode) const;
	// void getBuildInfo(FacilityInfo& info);
#endif	// CAMPEDIT

	MapPriority isVisible(MapWindow* map) const;
	void mapDraw(MapWindow* map, Region* bm, Point where);

#ifndef CAMPEDIT
	void dailyProcess();
#endif	// CAMPEDIT

	ResourceSet* getLocalResources() { return &resource; }
	operator City* () { return this; }
	
	// void writeData(DataFileOutStream& stream);

	UBYTE allocateRegiment();
};

/*
 * Global List of Cities
 */

class FacilityPtr {
	Facility* fPtr;
public:
	FacilityPtr() { fPtr = 0; }
	~FacilityPtr()	{ delete fPtr;	}

	operator Facility* () { return fPtr; }
	FacilityPtr& operator = (Facility* f) { fPtr = f; return *this; }
};


#ifdef CAMPEDIT
class FacilityList : public DynamicArray<Facility*> {
public:
	void clearAndDestroy();

	FacilityID getID(const Facility* f) const;
	void zeroResources(Boolean all);
	void setResources(int days);
};

typedef DynamicArrayIter<Facility*> FacilityIter;

#else

class FacilityList : public Array<FacilityPtr> {
public:
	void clearAndDestroy();

	FacilityID getID(const Facility* f) const;

	void dailyProcess();				// Daily process

	Facility* findClose(const Location& l, Distance maxDist);
};

typedef ArrayIter<FacilityPtr> FacilityIter;

#endif
/*
 * Railway Section List
 */

#ifdef CAMPEDIT

class RailSectionList : public DynamicArray<FacilityID> {
};

#else

class RailSectionList : public Array<FacilityID> {
};

#endif


#endif /* CITY_H */
