/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MOBILISE_H
#define MOBILISE_H

#ifndef __cplusplus
#error mobilise.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Mobilisation and facility building
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.1  1994/05/25  23:33:41  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"
#include "gamedef.h"
#include "side.h"
#include "resource.h"
#include "language.h"

class Facility;
class MenuData;
class ChunkFileWrite;
class ChunkFileRead;
class CampaignWorld;

enum MobiliseType {
	/*
	 * Infantry
	 */

	Mob_InfantryRegular,
	Mob_InfantryMilitia,
	Mob_InfantrySharpShooter,
	Mob_Engineer,

	/*
	 * Cavalry
	 */

	Mob_cavalryRegular,
	Mob_cavalryMilitia,
	Mob_cavalryRegularMounted,
	Mob_cavalryMilitiaMounted,

	/*
	 * Artillery
	 */

	Mob_ArtillerySmoothbore,
	Mob_ArtilleryLight,
	Mob_ArtilleryRifled,
	Mob_ArtillerySiege,

	/*
	 * Naval
	 */

	Mob_Naval,
	Mob_NavalIronClad,
	Mob_Riverine,
	Mob_RiverineIronClad,

	/*
	 * Supplies
	 */

	Mob_SupplyWagon,

	/*
	 * Facilities
	 */

	Mob_Fortification,
	Mob_SupplyDepot,
	Mob_Recruitment,
	Mob_Training,
	Mob_RailHead,
	Mob_RailEngineer,
	Mob_Hospital,
	Mob_POW,
	Mob_Blockade,

	Mob_HowMany,

	Mob_Nothing = -1,
};

/*
 * Cost structure
 */

struct MobiliseCost {
	ResourceSet cost;
	UBYTE days;
#if defined(NOBITFIELD)
	Boolean canBuildMulti;
	Boolean reduceIfTraining;
	Boolean inCity;
	Boolean onlyOne;
#else
	Boolean canBuildMulti:1;
	Boolean reduceIfTraining:1;
	Boolean inCity:1;
	Boolean onlyOne:1;
#endif
	// const char* description;
	LGE description;
	Realism regRealism;				// Minimum Regiment realism level for item to be enabled
	Realism facRealism;				// Minimum Facility realism level for item to be enabled
	Realism supRealism;				// Minimum Supply realism level

	MobiliseCost(ResourceSet c, UBYTE t, Boolean multi, Boolean reduce, Boolean city, Boolean only, Realism rReal, Realism fReal, Realism sReal, LGE s) : cost(c)
	{
		days = t;
		canBuildMulti = multi;
		description = s;
		regRealism = rReal;
		facRealism = fReal;
		supRealism = sReal;
		reduceIfTraining = reduce;
		onlyOne = only;
		inCity = city;
	}

	UBYTE getBuildTime(const Facility* f) const;
};

/*
 * Mobilising thing
 */

// class MobiliseIter;
class MobiliseList;

class Mobilising {
// friend class MobiliseIter;
friend class MobiliseList;
	friend void writeMobList(ChunkFileWrite& file, CampaignWorld* world);
	friend void readMobList(ChunkFileRead& file, CampaignWorld* world);

	Mobilising* next;			// Next object in list

	MobiliseType what;		// What is being mobilised
	UBYTE when;					// How many days till it is ready
	UBYTE howMany;				// How many are being mobilised?
	Facility* where;			// Where is it being mobilised?

public:
#if !defined(CAMPEDIT)
	Mobilising(MobiliseType type, UBYTE days, UBYTE count, Facility* city);
#endif

	Mobilising();
};

class MobiliseList {
// friend class MobiliseIter;
	friend void writeMobList(ChunkFileWrite& file, CampaignWorld* world);
	friend void readMobList(ChunkFileRead& file, CampaignWorld* world);

	Mobilising* entry;

public:
	MobiliseList() { entry = 0; }
	~MobiliseList();

#ifndef CAMPEDIT
	// int add(MobiliseType type, Facility* city, MenuData* control);
	int askHowMany(MobiliseType type, Facility* city, MenuData* control);
	int startBuild(MobiliseType type, Facility* city, int howMany);

	void process();
	UBYTE whenMobilising(const Facility* where, MobiliseType what) const;
	UBYTE totalMobilising(const Facility* where, MobiliseType what) const;
	int howManyMobilising(Side side, MobiliseType what) const;
	int MobilisingPerFacility( const Facility *f, MobiliseType what) const;

	Boolean canEverBuild(const Facility* where, MobiliseType what) const;
	Boolean canIbuild(Facility* where, MobiliseType what) const;
	Boolean canItemExist(MobiliseType what) const;

	void removeAll(Facility* where);
	void clear();
#endif	// CAMPEDIT
};


#ifndef CAMPEDIT

#if 0
class MobiliseIter {
	Mobilising* nextItem;
	MobiliseList* container;
public:
	MobiliseIter(MobiliseList* list) { container = list; nextItem = list->entry; }
	void reset() { nextItem = container->entry; }

	UBYTE whenMobilising(const Facility* where, MobiliseType what);
};

#endif

int canAffordToMobilise(MobiliseType type, Facility* city);

#endif	// CAMPEDIT

#endif /* MOBILISE_H */

