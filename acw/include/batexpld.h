/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef BATEXPLD_H
#define BATEXPLD_H

#ifndef __cplusplus
#error batexpld.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battle Explosion (Temporary graphics)
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:12  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */


// #include "batdisp.h"
#include "sprlist.h"
#include "sprlib.h"

/*
 * temporary Sprites (e.g. for explosions)
 */

struct AnimationTable {
	UBYTE frames;
	SpriteIndex baseSprite;
	UBYTE sprites[5][8];
	// UBYTE sprites[zoom][view][frame]
};

class TemporaryBattleSprite {
	friend class TemporaryBattleSpriteList;

	TemporaryBattleSprite* next;	// Singly Linked list

	Sprite3D sprite;			// Current graphic
	Location where;			// Where is it?
	Cord3D height;
	Wangle direction;			// Which direction is it facing?
	AnimationTable* table;
	UBYTE frame;
	SpriteElement* element;

public:
	TemporaryBattleSprite(const Location& l, Cord3D height, Wangle dir, AnimationTable* tab);
	~TemporaryBattleSprite();

	Boolean process(System3D& drawData);

	void clearUp();
	// void unLink();
};

class TemporaryBattleSpriteList {
	TemporaryBattleSprite* entry;
public:
	TemporaryBattleSpriteList();
	~TemporaryBattleSpriteList();

	void process();
	void add(const Location& l, Cord3D height, Wangle dir, AnimationTable* tab);
};

#endif /* BATEXPLD_H */

