/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef DB_INDEX_H
#define DB_INDEX_H

#ifndef __cplusplus
#error db_index.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Database Index definition
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "font.h"
#include "image.h"

class Dplay;

/*
 *   Handles the display and highlighting of a index
 */


class Index {

	protected:
		int numitems;
		int* highlight;
		int* remove;
		short int *iwidth;
		short int *iheight;
		short int *ixpos;
		short int *iypos;
		Dplay *scr;
		Font *font;
		FontID n;
		char twinline;
		int keylit;

	public:
		Index( Dplay *display, FontID n );
		void distroy();
		void setupindex( Dplay *display );
		void light( Point mousepos, char temp, char off = 0 );
		int getHighlight( char temp) { return highlight[temp]; }
		int getRemove( char temp ) { return remove[ temp]; }
		int getnumitems() { return numitems; }

		Point MoveMousePtr( int direction, char ind, int clear, Point temp );

		short int getIX( char loop ) { return ixpos[ loop ]; }
		short int getIW( char loop ) { return iwidth[ loop ]; }
		short int getIH( char loop ) { return iheight[ loop ]; }
		short int getIY( char loop ) { return iypos[ loop ]; }

};

#endif /* DB_INDEX_H */

