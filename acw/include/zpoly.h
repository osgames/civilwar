/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef POLYGON_H
#define POLYGON_H

#ifndef __cplusplus
#error polygon.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Polygon Class definition for use with ZBuffer
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:38  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "types3d.h"
#include "terrain.h"

struct TriangleElement {
	TriangleElement* next;
	Point3DI points[3];
	TerrainType terrain;
};

class TriangleList {
	TriangleElement* entry;
public:
	TriangleList() { entry = 0; }
	TriangleElement* get()
	{
		TriangleElement* el = entry;
		if(el)
			entry = el->next;
		return el;
	}
	void add(const Point3DI& p1, const Point3DI& p2, const Point3DI& p3, TerrainType ter);
};

#endif /* POLYGON_H */
