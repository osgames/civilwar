/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef OBFILE_H
#define OBFILE_H

#ifndef __cplusplus
#error obfile.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Read and Write Order of Battle to Binary Chunk Files
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

class OrderBattle;
class CampaignWorld;

class ChunkFileWrite;
class ChunkFileRead;

void writeOB(ChunkFileWrite& file, OrderBattle* ob, CampaignWorld* world = 0);
void readOB(ChunkFileRead& file, OrderBattle* ob, CampaignWorld* world = 0);

#endif /* OBFILE_H */

