/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MOUSE_S_H
#define MOUSE_S_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Assembler Mouse Interface
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/07/11  14:32:07  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/11/09  14:02:26  Steven_Green
 * MouseHandler typedef modified
 *
 * Revision 1.1  1993/11/05  16:52:08  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef MOUSE_CALLBACK
typedef void far __loadds (MouseHandler)(int event, int buttons, int xMove, int yMove);
#pragma aux MouseHandler parm [EAX] [EBX] [ECX] [EDX]

void setMouseHandler();

#endif

#if 0

extern WORD initMouse();
extern void setMouseHandler(UWORD event, MouseHandler* handler);
extern void setMouseRange(short x, short y, short w, short h);

extern void setMousePosition(short x, short y);
#pragma aux setMousePosition parm [ECX] [EDX]

extern void setMouseRatio(short x, short y);
#pragma aux setMouseRatio parm [ECX] [EDX]
#else

extern void mouseInterrupt();

#endif

#ifdef __cplusplus
};
#endif

#endif /* MOUSE_S_H */

