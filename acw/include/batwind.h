/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef BATWIND_H
#define BATWIND_H

#ifndef __cplusplus
#error batwind.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battle Map Window and Icons
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.16  1994/09/23  13:29:31  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/08/19  17:30:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.14  1994/07/04  13:31:45  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.10  1994/05/25  23:33:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/05/04  22:12:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/04/26  13:40:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/04/22  22:16:24  Steven_Green
 * Icon Handling Tidied Up
 *
 * Revision 1.5  1994/04/13  19:48:44  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/04/11  21:31:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/03/17  14:30:48  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "icon.h"

class Region;
class Bitmap;
class BattleData;


/*
 * Map View Clump of icons
 */

class BattleViewIcons : public IconSet {
public:
	BattleViewIcons(IconSet* parent);
	void drawIcon();
};

/*
 * Map Window including scroll bars, etc
 */

class BattleMapArea;

class BattleMap : public IconSet {
public:
	Icon* UDBar;
	Icon* LRBar;
	Icon* rotBar;
	BattleMapArea* mapArea;
public:
	BattleMap(IconSet* parent);
};



class BattleMapArea : public Icon {
	Region* bm;
	BattleMap* map;
	Bitmap* buffer;
public:
	BattleMapArea(BattleMap* parent, const Rect& r);
	~BattleMapArea();
	void drawIcon();
	void execute(Event* event, MenuData* d);
};

void setBattleLight();

#endif /* BATWIND_H */
