/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef AI_CAMP_H
#define AI_CAMP_H

#ifndef __cplusplus
#error ai_camp.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign AI Classes and Definitions  
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:12  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "aic_even.h"
#include "side.h"

class Facility;
class AIC_WZList;


class CampaignAI {
	Side aiSide;
	AIC_EventList events;
	CombatValue seaNavalNeeded;	// Naval CV needed at sea
	CombatValue riverNavalNeeded;	// Naval CV needed on rivers
	CombatValue seaNavalGot;		// Naval CV needed at sea
	CombatValue riverNavalGot;		// Naval CV needed on rivers
public:
	CampaignAI();					// Constructor
	~CampaignAI();					// Destructor

	void startCampaign(Side sd, Boolean reconstruct);	// Initial call at start of campaign
	void dailyAI();

	Side getSide() const { return aiSide; }

private:
	void aic_AddNewRegiments();	// Attach any new regiments, balance OB
	void aic_TransferUnits();		// Update OB based on orders, Field of Operation
	void aic_InitUnits();			// Check for completed orders, clear flags
	void aic_SetCityActions();		// Make priority for attacking defending cities
	void aic_SetUnitActions();		// Make priority for attacking enemy units
	void aic_ProcUnits();			// Order Units to move
	void aic_ProcNaval();			// Look for zones to defend/attck
	void aic_Resources();			// Build facilities, mobilise regiments

	UBYTE calcCityAttackValue(Facility* enemyCity);
	UBYTE calcCityDefendValue(Facility* homeCity);
	void clearUnits();

	void make_wzList(AIC_WZList& wzList);
};


#ifdef DEBUG
#include "clog.h"
extern LogFile aiLog;
#endif


#endif /* AI_CAMP_H */

