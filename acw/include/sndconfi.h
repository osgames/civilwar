/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SNDCONFI_H
#define SNDCONFI_H

#ifndef __cplusplus
#error sndconfi.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Sound Configuration File Strings
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */


extern char sc_fileName[];

extern char sc_title[];

extern char sc_ID[];
extern char sc_version[];
extern char sc_name[];
extern char sc_ioPort[];
extern char sc_dma[];
extern char sc_irq[];
extern char sc_minRate[];
extern char sc_maxRate[];
extern char sc_stereo[];
extern char sc_mixer[];
extern char sc_sampSize[];
extern char sc_extra[];

extern char sc_rate[];
extern char sc_quality[];


#endif /* SNDCONFI_H */

