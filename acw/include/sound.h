/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SOUND_H
#define SOUND_H

#ifndef __cplusplus
#error sound.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Interface to Sound System
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1994/09/02  21:28:59  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/08/22  16:09:04  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

#if defined(SOUND_DSMI)

#include "dsmi.h"
#include "dsmi32.h"

#define SoundChannelCount 16	// 8 For Music, 8 For Sound
#define MusicChannelCount 8
#define EffectChannelStart 8
#define EffectChannelCount 8

/*
 * This is just a marker to get sound to be deleted properly when
 * program is quit
 */

class Sound {
	Boolean initialised;
	int gusInterruptTag;
	int ampInterruptTag;

	ushort volTable[32];

public:
	SOUNDCARD scard;	  				// Overall values describing sound card
	MCPSTRUCT mcp;	  					// Multi-Channel Player Info
	ushort rate;						// Playback rate (e.g. 21000)
public:
	Sound() { initialised = False; scard.ID = NO_SOUNDCARD; }	// Just create, but not initialised
	~Sound() { closeDown(); }					// Close it all down

	void init();									// Set it up
	void closeDown();								// Shut it down

	// void getCard();								// User interface to select card

	MODULE* loadMusic(const char* name);
	void playMusic(MODULE* song, Boolean loop = False);
	void stopMusic(MODULE* song);
	void unloadSong(MODULE* song);

	SOUNDCARD* getSoundInfo() { return &scard; }

	Boolean getConfiguration();

	unsigned char* getstr( int tsize, char* sndData, char search[] );

};

#else
// Dummy Sound Class

struct MODULE;

class Sound {
public:
//	SOUNDCARD scard;	  				// Overall values describing sound card
//	MCPSTRUCT mcp;	  					// Multi-Channel Player Info
//	ushort rate;						// Playback rate (e.g. 21000)

public:
	Sound() { }
	~Sound() { }

	void init() { }									// Set it up
	void closeDown() { }								// Shut it down

	// void getCard();								// User interface to select card

	MODULE* loadMusic(const char* name) { return 0; }
	void playMusic(MODULE* song, Boolean loop = False) {}
	void stopMusic(MODULE* song) {}
	void unloadSong(MODULE* song) {}

//	SOUNDCARD* getSoundInfo() { return &scard; }

//	Boolean getConfiguration();

//	unsigned char* getstr( int tsize, char* sndData, char search[] );

};

#endif	// DSMI Sound


extern Sound sound;								// Global interface to all Sound


#endif /* SOUND_H */

