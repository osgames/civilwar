/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef GENERALS_H
#define GENERALS_H

#ifndef __cplusplus
#error generals.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Generals and Command Positions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.33  1994/07/28  19:01:36  Steven_Green
 * create new regiment function added
 *
 * Revision 1.21  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.1  1994/02/03  23:33:39  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#undef LIMIT_ARMY_MOVEMENT		// Define this to limit AI armies to particular states


#include "gamedef.h"
#include "side.h"
#include "supply.h"
#include "ptrlist.h"
#include "mapob.h"
#include "trig.h"
#include "orders.h"
#include "logarmy.h"

/*===================================================================
 * Undefined classes
 */

class UnitAI;

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
// class RegimentBattle;
class UnitBattle;
#endif

class Region;
class System3D;

class DataFileOutStream;
class Facility;
class MoveStackItem;

/*===================================================================
 * Forward references
 */

class Unit;
class Brigade;
class Division;
class Corps;
class Army;
class President;
class Regiment;
class OrderBattle;

/*===================================================================
 * Some typedefs for lists and interators
 */

class RegimentList : public PtrDList<Regiment> { };
class PresidentList : public PtrDList<President> { };

typedef PtrDListIter<Regiment> RegimentIter;
typedef PtrDListIter<President> PresidentIter;


/*===================================================================
 * Constants and enumerations
 */

// #define MaxArmyName 64
// #define MaxRegimentName 64

#define MaxMenPerRegiment 1000
#define MaxRegimentsPerBrigade 6

/*
 * Flag used by unit moving on campaign
 */

enum MoveMode {
	MoveMode_Marching, 	// They are marching to a location
	MoveMode_Standing,	// They are at their location
	MoveMode_Waiting,		// Waiting for a train or boat
	MoveMode_Transport	// On a train or boat
};

enum SiegeMode {
	SIEGE_None,				// Unit has nothing to do with any facility
	SIEGE_Occupying,		// Unit is occuping a facility
	SIEGE_Besieging,		// Unit is beiseging a facility
	SIEGE_Standing,		// Unit is standing near a city
};

enum AttachMode {
	Attached,
	Detached,
	Joining
};

/*===================================================================

 * Virtual class for named Units
 */

class NamedUnit {
// protected:
	char*		name;
public:
	NamedUnit() { name = 0; }
	virtual ~NamedUnit() { if(name) { delete[] name; name = 0; }	}

	void setName(char* s);
	const char* getRealName() const { return name; }
};

/*=================================================================
 * Generals
 */

/*
 * A General is an individual
 * He might be attached to a command position, or be unallocated
 */

class General : public NamedUnit {
	friend class OrderBattle;
	friend class DataFileOutStream;
	friend class DataFileInStream;

public:
	Unit* allocation;		// Where he is allocated
#if defined(NOBITFIELD)
	Rank rank;
	Side side;
	Boolean inactive;	// Set if in unallocated General list on 1st day.
#else
	Rank rank:3;
	Side side:2;
	Boolean inactive:1;	// Set if in unallocated General list on 1st day.
#endif

public:
	AttributeLevel efficiency;
	AttributeLevel ability;
	AttributeLevel aggression;
	Experience		experience;

public:
	General(char* n, Side s);
	General();
	~General();

	Rank getRank() const { return rank; }
	Side getSide() const { return side; }
	Unit* getAllocation() const { return allocation; }

	const char* getName() const { return getRealName(); }

	friend Boolean generalBetter(const General* g1, const General* g2);
};


class GeneralList : public PtrDList<General>
{
};

typedef PtrDListIter<General> GeneralIter;


/*===============================================================
 * UnitInfo class
 *
 * Useful structure for passing info about units
 */

class UnitInfo {
public:
	/*
	 * These values filled in by caller
	 */

	Boolean onlyCampaignDependant;
	Boolean onlyBattleDependant;

	/*
	 * Returned Values
	 */

	Strength 		strength;
	Strength		 	stragglers;
	Strength			casualties;
	Fatigue 			fatigue;
	Morale 			morale;
	Speed				speed;
	Supply			supply;
	Experience		experience;

	Strength			infantryStrength;
	Strength			cavalryStrength;
	Strength			artilleryStrength;

	Strength			strengths[3][4];	// [basicType][subType]

	int				regimentCounts[3][4];

	int regimentCount;
public:
	UnitInfo(Boolean campDep, Boolean batDep);

	UnitInfo& operator += (UnitInfo& i);

	void clear();
};


/*===================================================================
 * Unit class
 *
 * Class to include Presidents/Armies/Corps/Divisions/Brigades and Regiments
 *
 * This used to be split into several different parts...
 *   but this started to be too cumbersum!
 */

class Unit : public MapObject {
	friend class OrderBattle;
	friend class DataFileOutStream;
	friend class DataFileInStream;

public:

	Rank rank;

	General* general;

	UBYTE childCount;		// How many children does it have?
	Unit* child;
	Unit* sister;
	Unit* superior;

	/*
	 * Campaign Information
	 */

	Order realOrders;			// Current received orders that are being acted on
	Order givenOrders;		// Orders that the player last gave them...
	PtrSList<SentOrder> orderList;		// Orders in transit
	Location nextDestination;	// Next point on route to realOrders.destination
	Location localDestination;	// Next local point on route
	Boolean needLocalDestination;	// Flag set if localDestination invalid

	Wangle facing;				// Which direction it is facing
	MoveMode moveMode;		// What mode it is in regarding transportation
	OrderHow moveMethod;		// Next method of moving


#if defined(NOBITFIELD)
	Boolean convertedForBattle;	// pointers to facilities have been converted to IDs
	Boolean seperated;				// Bit set if unit is ordered to be Detached
	Boolean hasSeperated;			// Bit set if unit is Detached
	Boolean reallySeperated;		// Set if physically seperated
	Boolean hasReallySeperated;	// Has Detached subordinates
	Boolean joining;					// On its way to rejoining command... or inactive if in unallocated regiment list
	
	Boolean givenNewOrder;			// Set if givenOrders has been changed
	Boolean newOrder;					// force re-evaluation of position
	Boolean inBattle;					// Set if already involved in a battle today
	Boolean canBeSeen;    			// Set to show unit
	SiegeMode siegeAction;
#else
	Boolean convertedForBattle:1;	// pointers to facilities have been converted to IDs
	Boolean seperated:1;				// Bit set if unit is ordered to be Detached
	Boolean hasSeperated:1;			// Bit set if unit is Detached
	Boolean reallySeperated:1;		// Set if physically seperated
	Boolean hasReallySeperated:1;	// Has Detached subordinates
	Boolean joining:1;				// On its way to rejoining command... or inactive if in unallocated regiment list
	
	Boolean givenNewOrder:1;	// Set if givenOrders has been changed
	Boolean newOrder:1;		// force re-evaluation of position
	Boolean inBattle:1;		// Set if already involved in a battle today
	Boolean canBeSeen:1;    // Set to show unit
	SiegeMode siegeAction;
#endif

#if !defined(CAMPEDIT) && !defined(BATEDIT)
	UnitAI* aiInfo;		// Info required by AI
#endif

	union {
		Facility* occupying;	// Facility that unit is occupying or besieging (0 if none)
		FacilityID occupyingID;
	};
	
#if !defined(CAMPEDIT) && !defined(TESTCAMP)
	/*
	 * Battle Information
	 */

	UnitBattle* battleInfo;		// Information used in battle
#endif	// CAMPEDIT

private:

public:
	Unit(Rank r);
	Unit(Rank r, const Location& l);
	virtual ~Unit();

#if !defined(TESTBATTLE)
	virtual void mapDraw(MapWindow* map, Region* bm, Point where);				// Class dependant draw function
	virtual MapPriority isVisible(MapWindow* map) const;
#endif

	void initValues();

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
	void draw3D(System3D& drawData);
#endif

#ifndef BATEDIT
	void drawCampaignUnit(MapWindow* map, Region* bm, Point& where, UWORD icon);
#endif

	Rank getRank() const { return rank; }

	virtual Side getSide() const
	{
		if(general)
			return general->getSide();
		else if(superior)
			return superior->getSide();
		else
			return SIDE_None;
	}

	// virtual Unit* makeNewUnder(const Location& l, const char* name);
	virtual Unit* makeNewUnder(const Location& l);
	void removeChild(Unit* u);
	void addChild(Unit* u);
	void updateHasSeperated();

	Boolean hasUnattached(Boolean recurse) const;
#if !defined(BATEDIT) && !defined(TESTBATTLE)
	void reattachCampaign(Boolean recurse);
	void rejoinCampaign();
#endif
#if !defined(CAMPEDIT) && !defined(TESTCAMP)
	void reattachBattle(Boolean recurse);
	void rejoinBattle();
#endif

#if defined(CAMPEDIT) || defined(TESTCAMP)
	void rejoin() { rejoinCampaign(); }
#elif defined(BATEDIT) || defined(TESTBATTLE)
	void rejoin() { rejoinBattle(); }
#else
	void rejoin() { if(battleInfo) rejoinBattle(); else rejoinCampaign(); }
#endif

	int getOrgPosition() const;

	virtual void getInfo(UnitInfo& i) const;
	virtual Strength getStrength() const;
	virtual Strength getDependantStrength() const;

	virtual const char* getName(Boolean full) const = 0;
	const char* getGeneralName() const;

#ifdef DEBUG
	const char* getFullName() const;
#endif

#if !defined(CAMPEDIT) && !defined(BATEDIT)
	void sendOrder(const Order& order);
#endif

	void setOrder(OrderMode mode, const Location& destination);
	void setOrderNow(const Order& order);
private:
	void giveOrders(const Order& order);
public:

#if defined(CAMPEDIT) || defined(BATEDIT)
	virtual void showInfo(Region* window);
#else
	void processOrderQueue(const TimeBase& timeNow);
#if !defined(TESTBATTLE)
	void moveCampaignBeforeChildren(GameTicks ticks, MoveStackItem& info);
	void moveCampaignAfterChildren(GameTicks ticks, MoveStackItem& info);
	void followLeader(MoveStackItem& info);
	void reallyMove(const Location& newDestination, Speed s, GameTicks ticks);

	void showCampaignInfo(Region* window);
#endif
#if !defined(TESTCAMP)
	void showBattleInfo(Region* window);
#endif
#endif

	void setLocation(const Location& l);
	void setMoveMode(MoveMode newMode);

	Unit* getTopCommand();
	President* getPresident();
	void setOccupy(Facility* f, SiegeMode mode, Boolean clearInBat = False );

	void setInBattle();

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
	UnitBattle* setupBattle();
	virtual UnitBattle* makeBattleInfo();
	void clearupBattle();
	// void startAI();					// Create UnitAI structure

#if !defined(TESTBATTLE)
	void beforeBattle();
	void afterBattle();
#endif
#endif
	void clearInBattle();

#if defined(LOG_ARMIES)
	virtual void logInfo(Boolean isLast);
#endif

	Boolean isCampaignDetached() { return seperated; }
	Boolean hasCampaignDetached() { return hasSeperated; }
	Boolean isCampaignReallyDetached() { return reallySeperated; }
	Boolean hasCampaignReallyDetached() { return hasReallySeperated; }
#if defined(CAMPEDIT) || defined(TESTCAMP)
	Boolean isDetached() { return isCampaignDetached(); }
	Boolean isReallyDetached() { return isCampaignReallyDetached(); }
	Boolean hasDetached() { return hasCampaignDetached(); }
	Boolean hasReallyDetached() { return hasCampaignReallyDetached(); }
#else
	Boolean isDetached() { return battleInfo ? isBattleDetached() : isCampaignDetached(); }
	Boolean isReallyDetached() { return battleInfo ? isBattleReallyDetached() : isCampaignReallyDetached(); }
	Boolean isBattleDetached();
	Boolean hasBattleDetached();
	Boolean isBattleReallyDetached();
	Boolean hasBattleReallyDetached();
	void makeBattleDetached(Boolean flag);	// Mark unit as Detached/joined
	void makeBattleReallyDetached(Boolean flag);	// Mark unit as Detached/joined

	Boolean hasDetached() { return battleInfo ? hasBattleDetached() : hasCampaignDetached(); }
	Boolean hasReallyDetached() { return battleInfo ? hasBattleReallyDetached() : hasCampaignReallyDetached(); }
#endif
	Boolean isJoining();

	void makeCampaignDetached(Boolean flag);	// Mark unit as Detached/joined
	void makeCampaignReallyDetached(Boolean flag);	// Mark unit as Detached/joined

	/*
	 * New simpler method of handing Detached flags...
	 */

	AttachMode getCampaignAttachMode() const;
	void setCampaignAttachMode(AttachMode newMode);

#if !defined(TESTCAMP) && !defined(CAMPEDIT)
	AttachMode getBattleAttachMode() const;
	void setBattleAttachMode(AttachMode newMode);
#endif

#if defined(CAMPEDIT) || defined(TESTCAMP)
	AttachMode getAttachMode() const { return getCampaignAttachMode(); }
	void setAttachMode(AttachMode newMode) { setCampaignAttachMode(newMode); }
#else
	AttachMode getAttachMode() const { return battleInfo ? getBattleAttachMode() : getCampaignAttachMode(); }
	void setAttachMode(AttachMode newMode) { if(battleInfo) setBattleAttachMode(newMode); else setCampaignAttachMode(newMode); }
#endif

	// friend Unit* joinForces(Unit* u1, Unit* u2);

	void setCanBeSeenFlag();

	virtual void incExperience(int n);
};

/*===================================================================
 * Regiment class
 *
 * A regiment is the bottom level of the hierarchy and has its values stored
 */

/*
 * Basic Regiment Types
 */

enum _BasicRegimentType {
	Infantry,
	Cavalry,
	Artillery
};

enum _RegimentSubType {
	Inf_Regular = 0,
	Inf_Militia,
	Inf_Sharpshooter,
	Inf_Engineer,
	// Inf_RailEngineer,
	Inf_TypeCount,

	Cav_Regular = 0,
	Cav_Militia,
	Cav_RegularMounted,
	Cav_MilitiaMounted,
	// Cav_Raiders,
	Cav_TypeCount,

	Art_Smoothbore = 0,		// 12lb
	Art_Light,					// 6lb
	Art_Rifled,
	Art_Siege,
	Art_TypeCount
};

typedef UBYTE BasicRegimentType;		// Bodge to force unsigned values
typedef UBYTE RegimentSubType;


struct RegimentType {
#if defined(NOBITFIELD)
	BasicRegimentType basicType;		// Infantry/cavalry/artillery/naval
	RegimentSubType subType;
#else
	BasicRegimentType basicType:2;		// Infantry/cavalry/artillery/naval
	RegimentSubType subType:3;
#endif

	/*
	 * Constructors
	 */

	RegimentType() {}

	RegimentType(BasicRegimentType bt, RegimentSubType st)
	{
		basicType = bt;
		subType = st;
	}
};

class Regiment : public Unit, public NamedUnit {

	friend class DataFileOutStream;
	friend class DataFileInStream;

public:
	RegimentType	type;

	RegimentStrength strength;				// Total strength calculated as total
	RegimentStrength stragglers;
	RegimentStrength casualties;

	Fatigue 			fatigue;
	Morale 			morale;
	Supply			supply;
	Experience		experience;

	// char*		name;

public:

public:
	Regiment();
	Regiment(const Location& l, const char* n, RegimentType t);
	~Regiment();

	void initValues();

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
	UnitBattle*  makeBattleInfo();
#endif

#if !defined(TESTBATTLE)
	void mapDraw(MapWindow* map, Region* bm, Point where);
	MapPriority isVisible(MapWindow* map) const;
#endif

	void getInfo(UnitInfo& i) const;
	void setInfo(UnitInfo& i);
	Strength getStrength() const { return strength; }
	Strength getDependantStrength() const { return strength; }
	
	const char*	getName(Boolean full) const;
	const char* getTypeStr() const;
	// void setName(const char* s);

	Regiment* findBestMergeable(Unit* topMan, Regiment* us);
	void incExperience(int n);

#if defined(LOG_ARMIES)
	void logInfo(Boolean isLast);
#endif
};

/*===================================================================
 * Brigades
 */

class Brigade : public Unit {
public:
#if defined(NOBITFIELD)
	Boolean moved;			// Set if moved last frame
	Boolean resting;		// Resting because too many stragglers
	Boolean holdBack;		// Too far from formation

	BasicRegimentType AIbasicType;		// Infantry/cavalry/artillery/naval
	RegimentSubType AIsubType;
#else
	Boolean moved:1;			// Set if moved last frame
	Boolean resting:1;		// Resting because too many stragglers
	Boolean holdBack:1;		// Too far from formation

	BasicRegimentType AIbasicType:2;		// Infantry/cavalry/artillery/naval
	RegimentSubType AIsubType:3;
#endif

	GameTicks movedTime;		// Time they moved during the previous day

public:
	Brigade() : Unit(Rank_Brigade) { moved = False; }
	Brigade(const Location& l);
	~Brigade();

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
	UnitBattle*  makeBattleInfo();
#endif

#if !defined(TESTBATTLE)
	void mapDraw(MapWindow* map, Region* bm, Point where);
	MapPriority isVisible(MapWindow* map) const;
#endif

	const char*		getName(Boolean full) const;
	// Unit* makeNewUnder(const Location& l, const char* name);
	Unit* makeNewUnder(const Location& l);

#if !defined(BATEDIT) && !defined(TESTBATTLE)
	void reallyMoveCampaign(const Location& newDestination, GameTicks ticks);
#endif

#if defined(LOG_ARMIES)
	void logInfo(Boolean isLast);
#endif
};

/*===================================================================
 * Divisions
 */

class Division : public Unit {
public:
	Division() : Unit(Rank_Division) {  }
	Division(const Location& l);
	~Division();

#if !defined(TESTBATTLE)
	void mapDraw(MapWindow* map, Region* bm, Point where);
	MapPriority isVisible(MapWindow* map) const;
#endif

	const char*		getName(Boolean full) const;
	// Unit* makeNewUnder(const Location& l, const char* name);
	Unit* makeNewUnder(const Location& l);

#if defined(LOG_ARMIES)
	void logInfo(Boolean isLast);
#endif
};

/*===================================================================
 * Corps
 */

class Corps : public Unit {
public:
	Corps() : Unit(Rank_Corps) {  }
	Corps(const Location& l);
	~Corps();

#if !defined(TESTBATTLE)
	void mapDraw(MapWindow* map, Region* bm, Point where);
	MapPriority isVisible(MapWindow* map) const;
#endif

	const char*		getName(Boolean full) const;
	// Unit* makeNewUnder(const Location& l, const char* name);
	Unit* makeNewUnder(const Location& l);

#if defined(LOG_ARMIES)
	void logInfo(Boolean isLast);
#endif
};

/*===================================================================
 * Armies
 */

class Army : public Unit, public NamedUnit {
	friend class DataFileOutStream;
	friend class DataFileInStream;
	friend class ObReadData;
	friend class ObWriteData;

#ifdef CAMPEDIT
	friend class CampaignEditControl;
#endif
#ifdef BATEDIT
	friend class BattleEditControl;
#endif

#if defined(CAMPEDIT) || defined(BATEDIT)
	friend void editUnit(Unit* oldUnit);
#endif

	// char* name;
public:

#ifdef LIMIT_ARMY_MOVEMENT
	char* NDefensiveFO[5];
	char* NoffensiveFO[5];
#endif

public:
	Army();
	Army(const Location& l, const char* name);
	~Army();

#if !defined(TESTBATTLE)
	void mapDraw(MapWindow* map, Region* bm, Point where);
	MapPriority isVisible(MapWindow* map) const;
#endif

	const char* getName(Boolean full) const;
	// Unit* makeNewUnder(const Location& l, const char* name);
	Unit* makeNewUnder(const Location& l);
	// void setName(const char* s);

#if !defined(TESTBATTLE) && !defined(BATEDIT)
	void makeArmyName();
#endif

#if defined(LOG_ARMIES)
	void logInfo(Boolean isLast);
#endif
};


/*===================================================================
 * President is a special piece used simply as a way of getting into
 * the top of the data for each side.  He doesn't really exist in the
 * world.
 */

 
class President : public Unit {
	friend class DataFileOutStream;
	friend class DataFileInStream;
	friend class OrderBattle;
	friend class ObWriteData;
	friend class ObReadData;

	GeneralList* freeGenerals;
	Regiment* freeRegiments;			// Use the sister field's to link them
	Side side;

public:
	President();
	President(Side side);
	~President();

#if !defined(CAMPEDIT) && !defined(TESTCAMP)
	UnitBattle*  makeBattleInfo();
#endif

	// Unit* makeNewUnder(const Location& l, const char* name);
	Unit* makeNewUnder(const Location& l);
	const char* getName(Boolean full) const
	{
#ifdef DEBUG
		return (side == SIDE_USA) ? "USA President" : "CSA President";
#else
		return 0;
#endif
	}
	
	Side getSide() const { return side; }

	void addFreeGeneral(General* g);
	void removeFreeGeneral(General* g);
	GeneralList* getFreeGenerals() { return freeGenerals; }

	void addFreeRegiment(Regiment* r);
	Boolean removeFreeRegiment(Regiment* r);
	Regiment* getFreeRegiments() const { return freeRegiments; }
	int freeRegimentCount() const;

	void dailyUpdate();		// Clear inactive free general/regiments

#if defined(LOG_ARMIES)
	void logInfo(Boolean isLast);
#endif
};


#if defined(DEBUG) && !defined(TESTCAMP)

/*===================================================================
 * Generators
 */

class GeneralGenerator {
	int howMany;
public:
	GeneralGenerator() { howMany = 0; }

	General* make(Side side, OrderBattle* ob);

};

class RegimentGenerator {
	int howMany;
	Boolean battleField;
public:
	RegimentGenerator(Boolean b = False) { howMany = 0; battleField = b; }

	Regiment* make(const Location& l = Location(0,0), char* s = 0);
};

#endif	// DEBUG

const char* getUnitTypeName(UnitInfo& info);

#endif /* GENERALS_H */
