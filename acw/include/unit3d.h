/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef UNIT3D_H
#define UNIT3D_H

#ifndef __cplusplus
#error unit3d.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * DIsplay units on 3D battlefield
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:38  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/09/23  13:29:31  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "batdisp.h"
#include "orders.h"
#include "ptrlist.h"
#include "side.h"

class Unit;
class Regiment;

enum Formation {
	Form_Column,
	Form_Line
};

/*
 * Class used to draw objects as pixels rather than sprites
 */

class DrawnRegiment : public Shape3D {
public:
	UBYTE height;
	Side side;
	UBYTE type;

	DrawnRegiment(Side s, UBYTE t);

	void draw(const System3D& drawData, const Point3D& where);
};

/*
 * Class for units other than regiment
 */

enum FollowHow { Follow_None, Follow_Left, Follow_Right, Follow_Behind };


class UnitBattle : public BattleDisplayObject {
protected:
	Unit* unit;

	TimeBase lastBattleTime;

	PtrSList<SentBattleOrder> battleOrderList;

	UBYTE animation;
	Distance animationDistance;
public:
	Location where;				// Current Location in battlefield coordinates
	BattleOrder battleOrder;	// What it is currently doing
	Wangle formationFacing;		// Which way the formation is facing
	Wangle menFacing;				// Which way the sprites are facing
#if defined(NOBITFIELD)
	Boolean bSeperated;
	Boolean bHasSeperated;
	Boolean bReallySeperated;
	Boolean bHasReallySeperated;
	Boolean bJoining;
	Boolean isDead;				// Set if all units have been killed
	Boolean routed;				// Set if regiment has routed
	Boolean AIFlag;
	Boolean longRoute;
	Boolean AI_action;	      // 0 = nothing, 1 = stand, 2 = attack, 3 = withdraw;
	Boolean followingOrder;	// Set if not arrived at click point yet
#else
	Boolean bSeperated:1;
	Boolean bHasSeperated:1;
	Boolean bReallySeperated:1;
	Boolean bHasReallySeperated:1;
	Boolean bJoining:1;
	Boolean isDead:1;				// Set if all units have been killed
	Boolean routed:1;				// Set if regiment has routed
	Boolean AIFlag:1;
	Boolean longRoute:1;
	Boolean AI_action:2;	      // 0 = nothing, 1 = stand, 2 = attack, 3 = withdraw;
	Boolean followingOrder:1;	// Set if not arrived at click point yet
#endif
	Formation formation;

	UWORD inCombat;	// Number of regiments in this organisation involved in combat

	/*
	 * How they are keeping in formation
	 */

	Unit* following;
	FollowHow followHow;

	/*
	 *  For AI
	 */

	Unit* reassign;
	Unit* enemy;
	// BattleLocation *AIdestination;

public:
	UnitBattle(Unit* owner);
	~UnitBattle();

#ifndef BATEDIT
	virtual Boolean moveBattle(const TimeBase& timeNow);
	void processOrderQueue(const TimeBase& timeNow);
	void sendBattleOrder(SentBattleOrder* order);
	virtual void deployUnit(const Location& loc);
	void getBattleTerrainEffect(Speed &moveSpeed);
	void getBattleOrderEffect(Speed &moveSpeed );
#endif
	virtual void setBattleOrder(const BattleOrder& newOrder);

#ifdef BATEDIT
	virtual void setPosition(const Location& l, Boolean alone);
	virtual void setFacing(const Location& l);
#endif

	virtual void getArea(Cord3D& width, Cord3D& height);	// Get area relative to formationFacing

	/*
	 * These are inherited from BattleDisplayObject anyway...
	 */

	virtual void draw3D(System3D& drawData) = 0;

	// void adjustInCombat(int change);
	void setInCombat();
	void clearInCombat();

	void moveCloser(Distance d, Wangle w);

	void checkIndependant();		// Make sure commander has children

	/*
	 *  AI patch function
	 */

	Boolean OrderPresent();

protected:
#if !defined(BATEDIT)
	void checkFinishedOrder();
#endif

	void updateAnimation(Distance movedDistance);
};

class CommanderBattle : public UnitBattle {

	/*
	 * Draw Variables
	 */

	Sprite3D sprite[2];				// Sprite used to draw each man and flag

public:
	CommanderBattle(Unit* owner);
	~CommanderBattle();

	void clearUp();
	// void unLink();

	void draw3D(System3D& drawData);
};

/*
 * Class for Brigades
 */

class BrigadeBattle : public CommanderBattle {
	Boolean flipped;
public:
	BrigadeBattle(Unit* owner);

#ifndef BATEDIT
	Boolean moveBattle(const TimeBase& timeNow);
	void deployUnit(const Location& loc);
#endif
	void setupRegiments(Boolean place = False);
	void placeRegiments();
#ifdef BATEDIT
	void setPosition(const Location& l, Boolean alone);
	void setFacing(const Location& l);
#endif
};

#define MenPerGun 25
#define MenPerInfantrySprite 50						// 50 infantry men per sprite
#define MenPerCavalrySprite  28						// 14 Horses per sprite (7 yard + 7 yard)
#define MenPerArtillerySprite (2 * MenPerGun)	// 2 Guns per sprite	= 50 men

/*
 * Class for Regiments
 */

struct CombatData;

enum CombatMode {
	CM_None,				// Either marching or standing
	CM_Firing,			// Set if firing
	CM_Charging,		// Set if charging
	CM_Melee,
};

#define LoadingResolution (USHRT_MAX+1)		// = 65536
#define LoadShowAnimation 0xe000u
#define LoadShowArtAnimation 0xc000u

class RegimentBattle : public UnitBattle {
protected:
	Sprite3D expSprite;		// Explosion Sprite
	UBYTE expFrame;			// Where is it in the explosion sequence
	unsigned int soundCount;	// Countdown from last explosion Sound
	UBYTE corpseCount;			// Counter to indicate a corpse is required

#if defined(NOBITFIELD)
	Boolean wantLimbMounted;		// Set if we want to be limbered
	Boolean wantUnLimbMounted;	// Set if want to be unlimbered
	Boolean limbMounted;			// True if mounted, False=unmounted
#else
	Boolean wantLimbMounted:1;		// Set if we want to be limbered
	Boolean wantUnLimbMounted:1;	// Set if want to be unlimbered
	Boolean limbMounted:1;			// True if mounted, False=unmounted
#endif
	UWORD limberCount;
public:

	/*
	 * Display Info
	 */

	UBYTE rows;
	UBYTE columns;
	Wangle previousFacing;		// For withdraw order to restore facing
	Formation wantFormation;

	/*
	 * AI Info
	 */

	/*
	 * Combat Info
	 */

	Regiment* target;				// Who they are currently firing, charging, etc
	Distance targetDistance;	// How far away our current target is.

	CombatMode combatMode;		// Doing as a bit field doesn't work because stupid compiler sign extends!

#if defined(NOBITFIELD)
	Boolean doneDecideCheck1;	// Set if done a decisive check on current target
	Boolean doneDecideCheck2;	// Set if done a decisive check on current target
	Boolean moving;				// Set if moved last frame
	Boolean disordered;		// Set if formation disordered
	Boolean canTurn;			// Set if should turn whild in withdraw mode
	Boolean fired;				// Used so display can add an explosion
#else
	Boolean doneDecideCheck1:1;	// Set if done a decisive check on current target
	Boolean doneDecideCheck2:1;	// Set if done a decisive check on current target
	Boolean moving:1;				// Set if moved last frame
	Boolean disordered:1;		// Set if formation disordered
	Boolean canTurn:1;			// Set if should turn whild in withdraw mode
	Boolean fired:1;				// Used so display can add an explosion
#endif

	Distance movedDistance;	// Distance that it has moved since this was set!

	// UWORD loadStatus;			// hi byte is number of rounds to fire this frame
	UWORD loadStatus;			// How far into loading... 65535 = ready to fire
	UBYTE fireCount;			// Number of times to fire this frame

	UWORD meleeTime;			// Minutes in day when melee results calculated

public:
	RegimentBattle(Regiment* r);
	~RegimentBattle();

	// virtual void reset();
	virtual void draw3D(System3D& drawData) = 0;

#ifndef BATEDIT
	virtual Boolean moveBattle(const TimeBase& timeNow);
#endif
	void setBattleOrder(const BattleOrder& newOrder);
	void getArea(Cord3D& width, Cord3D& height);

	Boolean getFollowLocation(Location& destination);
	void setFormationRows(Boolean force);

#ifndef BATEDIT
	Distance getEffectiveRange();
	Distance getDecisiveRange();
#endif

	Strength menPerSprite() const;
	void kill(Strength losses);

protected:	
	void mount() { wantLimbMounted = True; }
	void unMount() { wantUnLimbMounted = True; }
	Boolean isMounted() const { return limbMounted; }

	void limber() { wantLimbMounted = True; }
	void unLimber() { wantUnLimbMounted = True; }
	Boolean isLimbered() const { return limbMounted; }
	
#ifndef BATEDIT
	virtual Boolean canUnlimber() = 0;
	Boolean canUnmount() { return canUnlimber(); }
#endif

	void doOngoingMelee();

	Regiment* getRegiment() const { return (Regiment*) unit; }

private:
#ifndef BATEDIT
	void combatCheck(Location& oldLocation, Wangle dir);
	void combatCheck();
	void combatCheckStationary();

	void fillCombatData(CombatData& data);
	void setTarget(Regiment* r, Distance d);
	void loseTarget();
	void setCombatMode(CombatMode newMode);

	void changeOrder(OrderMode newOrder);
	void changeFormation(Formation newFormation);
	Boolean doHoldResponseCheck();
	UBYTE doResponseCheck();

	void doWithdrawCheck();
	void doRetreatCheck();
	void doHastyRetreatCheck();

	void startRouting();
	void stopRouting();
	void fireGuns();

	void reduceMorale(int diff);
	void increaseMorale(int diff);

	virtual Boolean canLoadFire() = 0;
	virtual void adjustLimberSpeed(Speed& speed) = 0;

	friend Distance getRegimentDistance(RegimentBattle* r1, RegimentBattle* r2);
	friend void enemyInRange(CombatData& viewer, CombatData& target, Distance d);
	friend Boolean sightEnemyAdvance(CombatData& viewer, CombatData& target, Distance d);
	friend Boolean sightEnemyCautious(CombatData& viewer, CombatData& target, Distance d);
	friend Boolean sightEnemyAttack(CombatData& viewer, CombatData& target, Distance d);
	friend Boolean sightEnemyStand(CombatData& viewer, CombatData& target, Distance d);
	friend Boolean sightEnemyDefend(CombatData& viewer, CombatData& target, Distance d);
	friend Boolean sightEnemyWithdraw(CombatData& viewer, CombatData& target, Distance d);
	friend void doMelee(CombatData& d1, CombatData& d2);
	friend Strength calculateMeleeDamage(CombatData& attacker, CombatData& defender);
#endif
};

class CavInfantryBattle : public RegimentBattle {
	Sprite3D sprite;				// Sprite used to draw each man
	DrawnRegiment drawInfo;		// Line drawn information for men
public:
	CavInfantryBattle(Regiment* r);
	~CavInfantryBattle();

	void draw3D(System3D& drawData);
	void clearUp();
	// void unLink();
#ifndef BATEDIT
	Boolean moveBattle(const TimeBase& timeNow);

	Boolean isMountedInfantry() const;
	Boolean canUnlimber() { return isMountedInfantry(); }

private:
	Boolean canLoadFire();
	void adjustLimberSpeed(Speed& speed);
#endif
};

#define ArtillerySpriteCount 3

class ArtilleryBattle : public RegimentBattle {
	DrawnRegiment drawInfo;		// Line drawn information for men
	Sprite3D images[ArtillerySpriteCount]; 			// Canon/Limber|Men/Horse

public:
	ArtilleryBattle(Regiment* r);
	~ArtilleryBattle();

	void draw3D(System3D& drawData);
	void clearUp();
#ifndef BATEDIT
	Boolean moveBattle(const TimeBase& timeNow);

	Boolean canUnlimber() { return True; }

private:
	Boolean canLoadFire();
	void adjustLimberSpeed(Speed& speed);
#endif
};



#endif /* UNIT3D_H */

