/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SCREEN_H
#define SCREEN_H

#ifndef __cplusplus
#error screen.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Video Interface combining the physical screen with a buffer
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.5  1994/09/02  21:28:59  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/08/09  15:48:13  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/07/13  13:54:00  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/07/11  14:32:07  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/06/02  15:31:12  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "gr_types.h"
#include "image.h"

class Vesa;
// class Image;
// class Region;
// class Bitmap;
class Mouse;
// class Palette;
// class Sprite;

class LinkedRectList;

class LinkedRect {
friend class LinkedRectList;
friend class Screen;
	LinkedRect* next;
	LinkedRect* prev;
public:
	Rect rect;
};

class LinkedRectList {
friend class Screen;
	LinkedRect* entry;
public:
	LinkedRectList() { entry = 0; }

	void add(LinkedRect* r);
	void remove(LinkedRect* r);
	LinkedRect* get();
};

#define MaxUpdateRects 32

class Screen {
	Point size;
	Vesa* physical;
	// Image* image;
	// Region* buffer;
	Bitmap* buffer;

	Mouse* mouse;

	// The Physical Palette state

	friend void updatePalette();
	long palTimerTag;

	Palette* deferredPalette;

	Boolean paletteChanged;
	int fadeRate;						// Desired fade rate

	// Deferred Palette State

	Palette* palette;								// Destination palette
	Palette* realPalette;						// Current physical palette
	volatile int realFadeRate;					// Current fade rate
	volatile Boolean paletteWrong;			// Set if fading in progress

	LinkedRect rectangles[MaxUpdateRects];	// buffer for getting new rectangles

	LinkedRectList usedRects;
	LinkedRectList freeRects;

public:
	Screen();
	~Screen();

	void setMouse(Mouse* m) { mouse = m; }

	void setUpdate(const Rect& r);		// Mark section of screen as updated
	void setUpdate();							// Mark All of screen as updated
	void update();								// Refresh physical screen

	SDimension getW() const { return size.x; }
	SDimension getH() const { return size.y; }
	Region* getBitmap() { return buffer; }
	Image* getImage() { return buffer->getImage(); }
	// Image* getImage() { return image; }

	void physBlit(Image* image, const Point& dest, const Rect& src);
	void physMaskBlit(Sprite* sprite, const Point& dest);
	void blit(Image* image, const Point& dest, const Rect& src);
	void physUnBlit(Image* image, const Point& where);
	void unBlit(Image* image, const Point& where);
	void maskBlit(Sprite* sprite, const Point& dest);

	/*
	 * Palette Functions
	 */

	Palette* getPhysicalPalette() { return realPalette; }
	Palette* getDeferredPalette() { return deferredPalette; }		// Return pointer to palette
	void updatePalette(int rate = 0);		// Tell manager that palette has changed
	void setPaletteNow(Palette* p, int rate=0);		// Set new palette with required fade rate
	void setPalette(Palette* p, int rate=0);		// Set new palette with required fade rate
	void fadePalette(int rate=0);						// Fade the palette (instantly)
	void updateColours(UBYTE start, UBYTE count, TrueColour* colours);

private:
#ifdef DEBUG
	void checkRectangles();
#endif
};

#endif /* SCREEN_H */

