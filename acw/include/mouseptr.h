/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MOUSEPTR_H
#define MOUSEPTR_H

#ifndef __cplusplus
#error mouseptr.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Mouse Pointer Enumeration
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.16  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.14  1994/05/21  13:18:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1994/04/11  21:31:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/03/17  14:30:48  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/03/15  15:17:16  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/03/11  23:14:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/03/01  22:30:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/02/28  23:05:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/02/17  20:00:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/02/17  15:01:13  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/02/03  15:05:37  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/01/26  19:52:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/01/24  21:21:20  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "iconlib.h"

/*
 * Typedefs
 */

typedef IconNumbers PointerIndex;

#if 0
enum PointerIndex {


	/*
	 * Miscellaneous
	 */

	M_Arrow,
	M_Question,
	M_Zoom,
	M_Busy,

	/*
	 * Infantry
	 */

	M_Infantry,			// Regular
	M_InfMilitia,
	M_InfSharpShooter,
	M_Engineer,
	M_RailEngineer,

	/*
	 * Cavalry
	 */

	M_Cavalry,			// Regular
	M_CavMilitia,
	M_CavRegularMounted,
	M_CavMilitiaMounted,
	M_CavRaiders,

	/*
	 * Artillery
	 */

	M_ArtSmoothbore,
	M_ArtLight,
	M_ArtRifled,
	M_ArtSiege,

	/*
	 * Supplies
	 */

	M_Food,
	M_Forage,
	M_SmallAmmo,
	M_LightAmmo,
	M_FieldSAmmo,
	M_FieldRAmmo,

	/*
	 * Naval Units
	 */

	M_NavalUnit,
	M_IronClad,
	M_Riverine,
	M_Monitor,
	M_Blockade,

	/*
	 * Facilities
	 */

	M_Fortification,
	M_SupplyDepot,
	M_RecruitmentCentre,
	M_TrainingCamp,
	M_Railhead,
	M_Hospital,
	M_POW,

	M_General,

	M_IssueOrders,
	M_WhereTo,
	M_AlterView,

	M_Target,
	M_Deploy,
	M_ChangeFacing,

	M_None = -1
};
#endif

#endif /* MOUSEPTR_H */

/* Depends on h\iconlib.h , touched on 03-01-94 at 11:12:43 */
/* Depends on h\iconlib.h , touched on 03-10-94 at 16:53:48 */
/* Depends on h\iconlib.h , touched on 03-11-94 at 15:36:08 */
/* Depends on h\iconlib.h , touched on 03-11-94 at 16:08:24 */
/* Depends on h\iconlib.h , touched on 03-14-94 at 14:39:20 */
/* Depends on h\iconlib.h , touched on 03-16-94 at 17:27:45 */
/* Depends on h\iconlib.h , touched on 03-18-94 at 12:30:15 */
/* Depends on h\iconlib.h , touched on 04-11-94 at 11:00:42 */
/* Depends on h\iconlib.h , touched on 05-10-94 at 16:18:48 */
/* Touched on 05-19-94 at 18:13:30 */
/* Touched on 05-26-94 at 18:19:11 */
/* Touched on 06-02-94 at 12:40:48 */
