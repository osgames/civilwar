/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef ZOOM_MAP_H
#define ZOOM_MAP_H

#ifndef __cplusplus
#error zoom_map.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Zoomed Bitmap: Access to section of a bitmap file
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:38  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "gr_types.h"
#include "cd_open.h"

// #define C_IO		// Buffer C I/O
#define SYS_IO			// Unbuffered


#if defined(C_IO)
	#include <stdio.h>

	struct FilePointer {
		FILE* fp;

		FilePointer() { fp = 0; }
		~FilePointer() { if(fp) fclose(fp); }

		Boolean isValid() { return fp != 0;	}
		void open(const char* name) { if(fp) fclose(fp); fp = FileOpen(name, "rb"); }
		void read(void* data, size_t length) { fread(data, length, 1, fp); }
		void seekSet(long where) { fseek(fp, where, SEEK_SET); }
		void seekRel(long amount) { fseek(fp, amount, SEEK_CUR); }
	};

#elif defined(SYS_IO)

	#include <fcntl.h>
	#include <io.h>

	struct FilePointer {
		int fp;

		FilePointer() { fp = -1; }
		~FilePointer() { if(fp >= 0) close(fp); }

		Boolean isValid() { return fp >= 0;	}
		void open(const char* name) { if(fp >= 0) close(fp); fp = fileOpen(name, O_RDONLY | O_BINARY); }
		void read(void* data, size_t length) { ::read(fp, data, length); }
		void seekSet(long where) { lseek(fp, where, SEEK_SET); }
		void seekRel(long amount) { lseek(fp, amount, SEEK_CUR); }
	};

#else
	#error("Must define file access method in zoom_map.h")
#endif



class Image;

class DiskBM {
	Point oldPosition;
	Boolean valid;					// oldPosition is valid

	FilePointer fp;
	UWORD width;
	UWORD height;
public:
	DiskBM();
	~DiskBM();

	void init(const char* fileName);
	void updateBM(Image* img, const Point& newP);
	void reset();
private:
	void readArea(Image* img, Point to, Point from, Point sz);

};

#endif /* ZOOM_MAP_H */

