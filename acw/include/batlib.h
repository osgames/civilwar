/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef BATLIB_H
#define BATLIB_H

#ifndef __cplusplus
#error batlib.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battle.SPR enumeration
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:12  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.4  1994/09/23  13:29:31  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/09/02  21:27:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/08/31  15:26:29  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/04/11  13:38:20  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "batspr.h"

enum BattleSprite {
	EarthTexture = SPR_earth,
	SkyTexture = SPR_sky,

	/*
	 * From batzoom
	 */


	BS_RotLeft,
	BS_RotRight,
	BS_ZoomIn,
	BS_ZoomOut,
	BS_ZoomCentre,
	BS_Zoom1,
	BS_Zoom1a,
	BS_Zoom2,
	BS_Zoom2a,
	BS_Zoom3,
	BS_Zoom3a,
	BS_Zoom4,
	BS_Zoom4a,
	BS_Zoom5,
	BS_Zoom5a,

	/*
	 * Static Objects
	 */

	Tree1 = SPR_trees,
	Tree2 = Tree1 + 5,
	Tree3 = Tree2 + 5,
	TreeDead1 = Tree3 + 5,		// 3 Dead Trees at 5 sizes
	TreeDead2 = TreeDead1 + 5,	// 3 Dead Trees
	TreeDead3 = TreeDead2 + 5,	// 3 Dead Trees

	ChurchSprite = SPR_church1,
	HouseSprite = SPR_house01,
	TavernSprite = SPR_tavern1,
	ChurchSprite1 = SPR_chapel_1,
	HouseSprite1 = SPR_house11,
	TavernSprite1 = SPR_tavern11,
	TentSprite = SPR_tent,

	INF_USA_Loading = SPR_i_reload,
	INF_CSA_Loading = SPR_i_reload + 3*8*2,
	INF_USA_firing = SPR_i_shoot,
	INF_CSA_firing = SPR_i_shoot + 3*8,
	INF_USA_March = SPR_i_walk,
	INF_CSA_March = SPR_i_walk + 3*8*4,
	INF_USA_Charging = SPR_i_charge,
	INF_CSA_Charging = SPR_i_charge + 3*8*4,
	INF_USA_Stand = SPR_i_stand,
	INF_CSA_Stand = SPR_i_stand + 3*8,

	Horse_Walk  = SPR_horses,
	Horse_Stand = SPR_stnd_hrs,

	Cav_USA_Walk = SPR_mt_infnt,
	Cav_CSA_Walk = SPR_mt_infnt + 3*8*6,
	Cav_USA_Stand = SPR_stnd_hrs + 3*8,
	Cav_CSA_Stand = SPR_stnd_hrs + 3*8 + 3*8,
	Cav_USA_Charge = SPR_cavalry,
	Cav_CSA_Charge = SPR_cavalry + 3*8*6,

#ifdef EMMA_GRAPHICS		// Emma's graphics
	LimberSprites = SPR_limbers + 32,
	SPR_RifledLimber = SPR_limbers,
	SPR_LightLimber = SPR_limbers + 16,
	SPR_SmoothboreLimber = SPR_limbers + 32,

	CanonSprites = SPR_canons,
	SPR_RifledCanon = SPR_canons,
	SPR_LightCanon = SPR_canons + 24,
	SPR_SmoothboreCanon = SPR_canons + 32,
#else
	SPR_RifledLimber = SPR_limbers,
	SPR_LightLimber = SPR_limbers + 8*3,
	SPR_SmoothboreLimber = SPR_limbers + 8*3*2,

	SPR_RifledCanon = SPR_canons,
	SPR_LightCanon = SPR_canons + 8*3,
	SPR_SmoothboreCanon = SPR_canons + 8*3*2,
#endif

	deadHorse = SPR_dedhorse,
	deadGun = SPR_deadgun,
	deadUnion = SPR_deadmanu,
	deadConfed = SPR_deadmanc,


	SillySprite = SPR_silly,

	BattleSpriteCount = SillySprite+3,
};

#endif /* BATLIB_H */

