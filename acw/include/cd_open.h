/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CD_OPEN_H
#define CD_OPEN_H

#ifndef __cplusplus
#error cd_open.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Open files from Hard disk or CD Rom (see language.cpp for implementation)
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"
#include <stdio.h>
class ifstream;

const char* getFileName(char* name);
Boolean fileOpen( ifstream& txt, const char *name, const int mode );
FILE* FileOpen( const char *name, const char* mode );
int fileOpen(const char* name, int mode);
const char* getPathAndCheck(const char* name);
const char* addPath( const char* path );
Boolean DoesFileExist( const char* fileName );

#endif /* CD_OPEN_H */

