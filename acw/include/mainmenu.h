/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MAINMENU_H
#define MAINMENU_H

#ifndef __cplusplus
#error mainmenu.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Main Menu Definition
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.14  1994/04/22  22:16:24  Steven_Green
 * Icon Handling Tidied Up
 *
 * Revision 1.1  1994/01/11  22:30:12  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

/*
 * Return Values from mainMenu (in addition to those defined in MenuData::MenuMode
 */

enum {
	MMR_LoadGame = 1,
};

int mainMenu();
 
#endif /* MAINMENU_H */

