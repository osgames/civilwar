/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef EARTH_H
#define EARTH_H

#ifndef __cplusplus
#error earth.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Display of the front face making the landscape look like a
 * chunk of earth.
 *
 * This works by storing a list of points at the front edge making
 * up a continuous line.
 *
 * Vertical lines are drawn from the lines connecting these points
 * down to the bottom of the screen using a texture.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/09/02  21:27:49  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types3d.h"

class Region;
class Image;


#define MaxEarthPoints 18		// This should be the maximum number of grids + 2

class LineList {
	Point points[MaxEarthPoints];
	int nextP;
	Cord3D bottom;
	Image* texture;
	Image* skyTexture;

public:
	LineList();
	~LineList();

	void clear();
	void add(const Point3D& p);
	void draw(Region* bitmap);
	void setBottom(Cord3D y) { bottom = y; }
#if defined(DEBUG) && !defined(BATEDIT)
	Cord3D getBottom() { return bottom; }
#endif

	void fillSky(Region* bm);
};


#endif /* EARTH_H */

