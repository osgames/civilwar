/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SCROLL_H
#define SCROLL_H

#ifndef __cplusplus
#error scroll.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Scroll Bar Icons
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.12  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.6  1994/04/22  22:16:24  Steven_Green
 * Icon Handling Tidied Up
 *
 * Revision 1.1  1994/03/17  14:30:48  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "sysicon.h"

class Region;

/*
 * This will be replaced with generic scroll bars soon!!!
 */

class ScrollIcon : public SystemIcon {
	SystemSprite normal;
	SystemSprite pressed;
public:
	ScrollIcon(IconSet* set, Rect r, SystemSprite i1, SystemSprite i2) :
		SystemIcon(set, i1, r)
	{
		normal = i1;
		pressed = i2;
	}

	void setPressed() { setIcon(pressed); drawIcon(); }
	void setNormal() { setIcon(normal); drawIcon(); }

	void execute(Event* event, MenuData* d) { event=event; d = d; }
};

/*
 * Blank Corner
 */

class BlankIcon : public SystemIcon {
public:
	BlankIcon(IconSet* set, Point p) :
		SystemIcon(set, SCR_BlankScroll, Rect(p, Point(16,19)))
	{
	}
};

class ScrollBar : public IconSet {
public:
	ScrollBar(IconSet* parent, Rect r) : IconSet(parent, r) { }
	~ScrollBar() = 0;

	virtual void clickAdjust(int v) = 0; // { v=v; }
	virtual void getSliderSize(SDimension& from, SDimension& to, SDimension size) = 0; 	// { from=0; to=0; size=0; }
	virtual void drawSlider(Region& bm, const Rect& r);
};

class VerticalScrollBar : public ScrollBar {
public:
	VerticalScrollBar(IconSet* parent, Rect r, SystemSprite topIcon = SCR_UScroll, SystemSprite bottomIcon = SCR_DScroll);
};


class HorizontalScrollBar : public ScrollBar {
public:
	HorizontalScrollBar(IconSet* parent, Rect r, SystemSprite leftIcon = SCR_LScroll, SystemSprite rightIcon = SCR_RScroll);
};


class MapBorder : public Icon {
public:
	MapBorder(IconSet* set, const Rect& r) : Icon(set, r) { }

	void drawIcon();
};

#endif /* SCROLL_H */

