/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SHPLIST_H
#define SHPLIST_H

#ifndef __cplusplus
#error shplist.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Polygon Shape List definition
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.24  1994/08/09  15:46:26  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.23  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.22  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.21  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.20  1994/06/07  18:33:23  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.19  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.18  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.17  1994/05/21  13:18:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.16  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/04/20  22:24:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.14  1994/04/11  21:31:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/04/11  13:38:20  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/03/18  15:08:40  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/02/28  23:05:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/02/17  20:00:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1993/12/23  09:29:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1993/12/10  16:08:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1993/12/01  15:12:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1993/11/30  02:57:48  Steven_Green
 * Use of Linked Array instead of new/delete
 *
 * Revision 1.3  1993/11/25  04:40:34  Steven_Green
 * Seperated up to allow different types of 3D object
 *
 * Revision 1.2  1993/11/24  09:34:12  Steven_Green
 * Shape base object used instead of Polygons
 *
 * Revision 1.1  1993/11/19  19:01:25  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifndef SHAPE_H
#include "shape.h"
#endif

#ifndef SHAPE3D_H
#include "shape3d.h"
#endif

#ifndef LNKARRAY_H
#include "lnkarray.h"
#endif


class Terrain;

/*
 * Shape pool
 */

class ShapeList {
	ShapeList* next;
	Shape* shape;
	Cord3D distance;

public:
			ShapeList() { next = 0; shape = 0; }
		  ~ShapeList() { delete shape; }
	// void operator delete(void* ad) { }		// Do nothing

	friend class ShapeListBase;
};

class ShapeListBase {
	ShapeList* entry;

	LinkedArray<ShapeList,64> allocShapeList;	// Allocation method

public:
	ShapeListBase() { entry = 0; };
	~ShapeListBase();

	void	add(Shape* shape, Cord3D dist);
	void	draw(Region* bm, ObjectDrawData* data);
};

/*
 * Data passed to object call back function
 */



class ObjectDrawData : public ShapeListBase {
public:
	ViewPoint* view;
	LightSource* light;
	Terrain* terrain;
	
	ObjectDrawData(ViewPoint* v, LightSource* l, Terrain* t) : ShapeListBase() { view = v; light = l; terrain = t; };

	friend void drawObject3D(WorldObject* ob, void* data);
};

#endif /* SHPLIST_H */

