/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef TYPES_H
#define TYPES_H
#ifdef __cplusplus
extern "C" {
#endif

/*
 *-------------------------------------------------------------
 * $Id$
 *-------------------------------------------------------------
 *
 * User defined Data types
 *
 *-------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.4  1993/12/03  16:52:10  Steven_Green
 * size_t only defined if not already defined
 *
 * Revision 1.3  1993/11/19  19:01:25  Steven_Green
 * Point operators use Point& wherever possible.
 * Used the const keyword wherever possible.
 *
 * Revision 1.2  1993/10/26  21:33:33  Steven_Green
 * graphics types moved to gr_types
 *
 * Revision 1.1  1993/10/26  14:54:37  Steven_Green
 * Initial revision
 *
 * Revision 1.2  1993/10/21  20:52:00  Steven_Green
 * Changed BOOL to be UWORD instead of enum
 *
 * Revision 1.1  1993/10/20  23:07:58  Steven_Green
 * Initial revision
 *
 *
 *-------------------------------------------------------------
 */

/*
 * Replacements for built in types
 */

typedef short 				WORD;
typedef unsigned short 	UWORD;
typedef long 				LONG;
typedef unsigned long 	ULONG;
typedef signed char 		BYTE;
typedef unsigned char 	UBYTE;

#ifndef _SIZE_T_DEFINED_
#define _SIZE_T_DEFINED_
 typedef unsigned int	size_t;
#endif

enum {
	False = 0,
	True = 1
};
typedef unsigned char Boolean;



#ifdef __cplusplus
}
#endif
#endif	/* TYPES_H */
