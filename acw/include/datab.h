/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef DATAB_H
#define DATAB_H

#ifndef __cplusplus
#error datab.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Database support:
 *
 * I removed the code from here and put it in datab.cpp
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */


class Index;

enum COLS {
#if 0
	c_Title = 65,					// 0x41 (Dark Brown)
	c_subTitle = 65,				// 0x41
	c_Bold  = 65,					// 0x41
	c_Text  = 57,					// 0x39 (Dark Blue)
	c_Keyword = 22,				// 0x16 (Red)
	c_PageTitle = 57,				// 0x39 (Dark Blue)
	c_IndexHighlight = 246,		// 0xf6 (Medium Grey)
	c_IndexUnion = 60				// 0x3c (Light Brown)
#else
	c_Title 		= 0xf0,				// Black!
	c_subTitle 	= 0xf0,				// Black!
	c_Bold  		= 0xf0,				// Black!
	c_Text  		= 0xf0,				// Black!
	c_Keyword 	= 0x17,				// Bright Red
	c_PageTitle = 0xf0,				// Black!
	c_IndexHighlight = 0xf8,		// Medium Grey
	c_IndexUnion = 0x3c,				// Light Brown
#endif
};


/*
 * Enumerations for Page Numbers
 * (to make those switch statements easier to read)
 *
 * Note, I havn't made use of all these yet... as it is a tedious
 * process.
 *
 * When this is complete,
 * then the "SetUpWins::page" can be replaced with "DBPageNumber page"
 * thus automatically forcing type checking.
 * 
 */

enum DBPageNumber {
	Page_Main = 0,
	Page_Bio1 = 1,
	Page_Bio2 = 2,
	Page_Weapons = 3,
	Page_Battles = 4,
	Page_Chrono = 5,
	Page_Testtabl = 6,
	Page_Battles2 = 7,
	Page_History = 8,
	Page_Features = 9,
	Page_BriefHistory = 10,
	Page_Features2 = 11,
	Page_HistorySelect = 12,
	// No 13, 14 (except there is a case 13, which refers to music (31?)
	Page_Artillery = 15,
	Page_CavalryTactics = 16,
	Page_InfantryWeapons = 17,
	Page_History2 = 18,
	Page_Tactics = 19,
	Page_Songs = 20,
	Page_NavalTactics = 21,
	Page_CavalryWeapons = 22,
	Page_InfantryTactics = 23,
	Page_ArtilleryTactics = 24,
	Page_SpecialUse = 25,				// Internal Use
	Page_NavalWeapons = 26,
	// No 27
	Page_Pictures = 28,
	// No 29
	Page_Animations = 30,
	Page_Songs2 = 31,
};

#define MaxPageNumber 32

/*
 * Enumeration of common Icons
 *
 * This is not complete yet, as different pages use icons >= 6 for
 * their own purposes.
 *
 * This is often used in positions[][5], which ought to be changed
 * to using a structure something like:
 *   struct { x,y,w,h, flag }
 * The flag in the above is bitmapped, and should also have enumerations
 * so that it is easier to know what each bit represents.
 *
 * This is also often the value in tret (which can be defined as being
 * of type DBIcon when all the numbers have beed replaced with enumerations.
 */

enum DBIcon {
	ICON_Quit = 0,
	ICON_Main = 1,
	ICON_Memory = 2,
	ICON_Index = 3,
	ICON_Next = 4,
	ICON_Prev = 5,

	// Main Page

	ICON_Biog = 6,
	ICON_Weapon = 7,
	ICON_Tactics = 8,
	ICON_History = 9,
	ICON_Battles = 10,
	ICON_Features = 11,
	ICON_Photos = 12,
	ICON_Songs = 13,
};

#define MaxIndexLength 60

#endif /* DATAB_H */

