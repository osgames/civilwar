/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef BATED_H
#define BATED_H

#ifndef __cplusplus
#error bated.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Battle Editor Global Include File
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:12  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "menudata.h"
#include "batldata.h"
#include "staticob.h"

/*
 * Undefined types
 */

class TextWindow;
class General;
class MapObject;
class Grid;

/*
 * Some definitions
 */

#define Font_SimpleMessage	Font_EMFL10
#define Font_Title			Font_EMFL15
#define Font_Info				Font_EMMA14
#define Font_Heading			Font_EMFL_8

/*===============================================================
 * Enumerations
 */

enum EditMode {
	EDIT_Troops,
	EDIT_Objects,
	EDIT_Landscape,
	EDIT_Rivers,
	EDIT_Roads,
	EDIT_Terrain,

	EDITMODE_HowMany
};

enum DoWhat {
	DoNothing,
	DoNew,
	DoMove,
	DoMoveLone,
	DoDelete,
	DoEdit,
	DoCal,
	DoOrder,
	DoFacing,
	DoIndependant,
};

/*===============================================================
 * Control Class
 */

class BattleEditControl : public MenuData, public BattleData {
	/*
	 * Private Data
	 */

	enum {
		Tracking,
		Moving,
		GetDestination
	} trackMode;

	PointerIndex oldZoomPointer;
	ZoomLevel newZoomLevel;		// zoom level to be set when zooming
	Boolean zooming;				// Set if in Magnify mode

	Boolean alone;

	Boolean clickUnit;
	Boolean justDrawn;

	StaticSpriteType defaultTreeType;
	Qangle defaultTreeFacing;

	UBYTE hillHeight;
	UBYTE hillRough;
	int hillRadius;
	Boolean hillFlat;

	UBYTE riverHeight;
	UBYTE riverWidth;
	UBYTE riverCurvature;

	BattleLocation startLocation;		// Used to remember start point for river/road

	UBYTE terrainType;				// Current Base type of terrain being placed
	UBYTE terrainSize;			  	// Size of terrain chunk to place
	Boolean riverProtected;
	Boolean roadProtected;

	UBYTE startDragX;
	UBYTE startDragY;
	Boolean doingDrag;

	Grid* backupGrid;				// Place to store backup grid

	/*
	 * Private Functions
	 */

	void loseObject();
	void setCurrentObject(MapObject* ob);
	void setCurrentObject(TreeObject* ob);

	void troopOverMap(const Event* event);
	void unitClicked();
	void askClickUnit();
	void objectOverMap(const Event* event);
	void objectOptions();
	void heightOverMap(const Event* event);
	void heightOptions();
	void riverOverMap(const Event* event);
	void riverOptions();
	void roadOverMap(const Event* event);
	void roadOptions();
	void terrainOverMap(const Event* event);
	void terrainOptions();

	void setHillParameters();
	void setRiverParameters();
	void setRoadParameters();
	void setTerrainParameters();

	void setupObjectTerrain();

	void storeGrid();
	void undoGrid();

	/*
	 * Public Data
	 */

public:
	Boolean mouseOnMap;
	EditMode mode;						// What edit mode are we in?

	Region* infoArea;				// Where to display info about cities
	TextWindow* textWin;

	MapObject* currentObject;
	TreeObject* currentBattleObject;

	/*
	 * Public Functions
	 */

public:
	BattleEditControl();
	~BattleEditControl();

	void init(const char* s);
	void overMap(Event* e, const Icon* icon, MenuData* d);
	void offMap();
	void doOptions(const Point& p);

	void startZoomMode(ZoomLevel newLevel);
	void startZoomMode();
	void stopZoomMode();

	void drawIcon();
	void display();
	void proc();

	void setMode(EditMode m);

	void hint(const char* s, ...);
	void clearInfo();
	void setupInfo();

  	// void editUnit(Unit* oldUnit);
	// void editGeneral(General* oldGeneral);
	void adjustObjectPosition(TreeObject* tob);
};

const char* getTreeTypeName(StaticSpriteType t);
const char* getQuadName(Qangle t);

const char* getTerrainStr(UBYTE t);

extern BattleEditControl* control;

extern Boolean changed;
extern Boolean obChanged;

#endif /* BATED_H */

