/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef PTRLIST_H
#define PTRLIST_H

#ifndef __cplusplus
#error ptrlist.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Simplified Pointer Linked List
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/03/10  14:29:08  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/02/17  15:01:13  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/02/15  23:24:13  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

/*
 * Forward references
 */

class DList;
class DListIter;

typedef void(*ForAllFunction)(void*, void*);

/*
 * Doubly Linked Node
 */

class DPtrLink {
friend class DList;
friend class DListIter;
	DPtrLink* next;
	DPtrLink* prev;
	void* data;
public:
};

class DList {
friend class DListIter;
	DPtrLink* first;
	DPtrLink* last;
	int count;
public:
	DList();
	~DList() { clear(); }
	void clear();


	int entries() const { return count; }
	int isEmpty() const { return (count == 0); }

protected:
	void append(void* data);
	void insert(void* data);
	void remove(void* data);

	void* find(int n = 0) const;
	void* findLast() const;
	int getID(void* el) const;

	void* get();	// Should get abitrary node with (int=0)
	void forAll(ForAllFunction function, void* data);
protected:
	void removeNode(DPtrLink* node);
};

template<class Type>
class PtrDList : public DList {
public:
	Type* find(int n = 0) 	const { return (Type*)DList::find(n); }
	Type* findLast() 			const { return (Type*)DList::findLast(); }
	Type* get() { return (Type*)DList::get(); }	// Should get abitrary node with (int=0)
	void forAll(void(*function)(Type* node, void* data), void* data) {
		DList::forAll((ForAllFunction)function, data);
	}
	int getID(Type* el) const { return DList::getID(el); }

	void append(Type* data) { DList::append(data); }
	void insert(Type* data) { DList::insert(data); }
	void remove(Type* data) { DList::remove(data); }

	void clearAndDestroy();
};

template<class Type>
void PtrDList<Type>::clearAndDestroy()
{
	while(entries())
		delete get();
}


class DListIter {
	DList* container;
	DPtrLink* link;
public:
	DListIter(DList& list);

	int operator ++();
	int operator --();

	void remove();					// Delete current entry
protected:
	void insert(void* data);	// Insert before current entry
	void append(void* data);	// Append after current entry

	void* current() const;

	int find(void* data);	// Set current to data's value, return nonzero if found
};

template<class Type>
class PtrDListIter : public DListIter {
public:
	PtrDListIter(PtrDList<Type>& list) : DListIter(list) { }

	Type* current() const { return (Type*)(DListIter::current()); }
	
	void insert(Type* data) { DListIter::insert(data); }	// Insert before current entry
	void append(Type* data) { DListIter::append(data); }	// Append after current entry
	int find(Type* data) { return DListIter::find(data); }
};


/*
 * The same again for single linked lists
 */

/*
 * Forward references
 */

class SList;
class SListIter;

/*
 * Singly Linked Node
 */

class SPtrLink {
friend class SList;
friend class SListIter;
	SPtrLink* next;
	void* data;
public:
};


class SList {
friend class SListIter;
	SPtrLink* first;
	SPtrLink* last;
	int count;
public:
	SList();
	~SList() { clear(); }

	void clear();

	int entries() const { return count; }
	int isEmpty() const { return (count == 0); }

protected:
	void append(void* data);
	void insert(void* data);

	void* find(int n = 0) const;
	void* findLast() const;

	void* get();	// Should get abitrary node with (int=0)
	Boolean get(void*& value);

	void forAll(ForAllFunction function, void* data);
};

template<class Type>
class PtrSList : public SList {
public:
	Type* find(int n = 0) 	const { return (Type*)SList::find(n); }
	Type* findLast() 			const { return (Type*)SList::findLast(); }
	Type* get() { return (Type*)SList::get(); }	// Should get abitrary node with (int=0)

	Boolean get(Type*& value) { return SList::get((void*&)value); }

	void append(Type* data) { SList::append(data); }
	void insert(Type* data) { SList::insert(data); }


	void forAll(void(*function)(Type* node, void* data), void* data) {
		SList::forAll((ForAllFunction)function, data);
	}
	void clearAndDestroy();
};


class SListIter {
	SList* container;
	SPtrLink* link;
public:
	SListIter(SList& list);

	// void insert(void* data);	// Insert before current entry
	// void remove();					// Delete current entry

	int operator ++();
	// int operator --();
protected:
	void append(void* data);	// Append after current entry
	void* current() const;
};

template<class Type>
class PtrSListIter : public SListIter {
public:
	PtrSListIter(PtrSList<Type>& list) : SListIter(list) { }

	Type* current() const { return (Type*)(SListIter::current()); }
	void append(Type* data) { SListIter::append(data); }
};

template<class Type>
void PtrSList<Type>::clearAndDestroy()
{
	while(entries())
		delete get();
}

#endif /* PTRLIST_H */

