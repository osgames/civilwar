/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CONNECT_H
#define CONNECT_H

#ifndef __cplusplus
#error connect.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Interface to MultiPlayer Routines
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1994/06/29  21:40:42  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/05/19  17:47:49  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"
#include "ptrlist.h"
#include "con_msg.h"

#ifdef DEBUG
#include "clog.h"
extern LogFile netLog;
#endif

/*
 * Constants
 */

#define TimeOutSecs 20						// 20 Second Time out
#define MaxRetries (TimeOutSecs * 2)	// Maximum number of times to resend packet


typedef UBYTE REMOTEID;

enum {
	RID_Local,
	RID_Remote,
};

struct Packet {
	PacketType type;				// What is in this packet?
	UWORD length;
	REMOTEID sender;				// Who sent it?
	UBYTE data[64];				// Specific Info about packet
};

struct PacketHead {
	PacketType type;				// What is in this packet?
	UWORD length;
};


/*
 * Packet used to indicate get out of MenuData
 */

struct FinishPKT {
	int how;
};


/*
 * Receipt list used to acknowledge packets
 */

struct Receipt {
	Receipt* next;
	ULONG serial;
	PacketType type;
	UWORD length;
	unsigned int time;
	UBYTE* data;
	UBYTE retries;
};

class ReceiptList {
public:
	Receipt* entry;
	Receipt* last;

	ReceiptList();
	~ReceiptList();

	void add(ULONG serial, PacketType type, UWORD length, UBYTE* data);
	void remove(ULONG serial);
	void clear();
};

/*
 * Virtual class for any multiplayer link
 */

class Connection {
	friend class MenuConnection;

public:
	Boolean inControl;					// Set if the controlling connection
	Boolean connected;
protected:
	ULONG serial;							// Next packet number
	ULONG rxSerial;						// Last receieved packet's serial number
	ReceiptList receiptList;
	PtrDList<Packet> packetList;		// packets waiting for application
	PtrDList<Packet> localPackets;	// Packets sent by us!

public:
	Connection();
	virtual ~Connection();

	int sendMessage(PacketType type, UBYTE* data, size_t length);
	int flush();

protected:
	int sendData(PacketType type, UBYTE* data, size_t length);
private:		// Only allow MenuConnection to use these
	int checkReceipts();

public:
	virtual int connect() { return -1; }
	virtual Boolean isLocal() const { return True; }
private:
	virtual void disconnect() { }
	virtual int sendRawData(PacketType type, UBYTE* data, size_t length, Boolean receipt, ULONG serial) { return -1; }
	virtual void processIncomming() { }

protected:
	Packet* receiveData()
	{
		processIncomming();
		checkReceipts();
		return packetList.get();
	}

private:
	Packet* getLocalPacket()
	{
		return localPackets.get();
	}

public:
	int processMessages(MenuConnection* menu, Boolean needSync);
private:
	int procRemoteMessages(MenuConnection* menu, Boolean needSync);

};

class MenuConnection {
	friend class Connection;

	int gotSync;		// Count of sync signals received
	char* menuID;	// Method of knowing if correct MenuConnection!

	Boolean waitingForReply;
	Boolean remoteAnswer;
	Boolean disconnected;
	Boolean waitSave;


public:
	MenuConnection(char* id);
	virtual ~MenuConnection();

	void processMessages(Boolean needSync);
	void handlePacket(Packet* packet);
	virtual void saveGame(const char* name) { }

	virtual void processPacket(Packet* pkt);		// Call back function from processMessages

	Boolean askRemoteReply(const char* text, const char* buttons);
	Boolean waitRemoteReply();
};

/*
 * Connection Class for single Player Mode
 * Actually this could be the default functions for Connection!
 */

class NullConnection : public Connection {
public:
	NullConnection();
	~NullConnection();

	/*
	 * Virtual Function overloads
	 */

	int connect();
	void disconnect();
	int sendRawData(PacketType type, UBYTE* data, size_t length, Boolean receipt, ULONG serial);
	Packet* receiveData();
	void processIncomming();
	Boolean isLocal() const { return True; }
};


Boolean askRemoteNotRespond();

Connection* connectIPX();
Connection* connectSerial();
Connection* connectModem();

#ifdef DEBUG
const char* packetStr(PacketType type);
#endif

#endif /* CONNECT_H */

