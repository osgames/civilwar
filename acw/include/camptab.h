/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CAMPTAB_H
#define CAMPTAB_H

#ifndef __cplusplus
#error camptab.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Tables and editable values used by campaign game
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "measure.h"
#include "mobilise.h"

extern Distance campaignCommandRange[];
extern UBYTE orderSpeedEffect[];
extern UBYTE terrainSpeedEffect[];
extern UBYTE terrainRouteCost[];
extern Distance cityBattleDistance;
extern Distance cityWidth;
extern Distance brigadeSideSpacing;
extern Distance brigadeFrontSpacing;
extern Distance citySupplyRange;
extern Distance brigadeBattleDistance;
extern Distance brigadeWidth;
extern Distance sameBattleDistance;
extern Distance involvedBattleDistance;
extern Distance CampaignVisibleDistance;
extern UBYTE ferrySpeed;
extern UBYTE seaSpeed;
extern UBYTE riverSpeed;
extern UBYTE navalCombat[];
extern UBYTE navalBombardment[];
extern UBYTE navalSpeeds[];
extern MobiliseCost buildCosts[];
extern ResourceSet resourceGeneration[];

#endif /* CAMPTAB_H */

