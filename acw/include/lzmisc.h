/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef LZMISC_H
#define LZMISC_H

#ifndef __cplusplus
#error lzmisc.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Miscelleneous Functions for LZHuf
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/07/19  19:55:29  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

/*
 * Constants
 */

/*
 * LZSS Encoding
 */

#define N               4096    /* buffer size (must be power of 2) */
#define F               60      /* pre-sence buffer size */
#define THRESHOLD       2

#define NIL             N       /* term of tree */

/*
 * Huffman
 */

#define N_CHAR          (256 - THRESHOLD + F) /* {code : 0 .. N_CHAR-1} */
#define T               (N_CHAR * 2 - 1)        /* size of table */
#define R               (T - 1)                 /* root position */
#define MAX_FREQ        0x8000  /* tree update timing from frequency */


/*
 * Class Containing data for compression and decompression
 *
 * Functions here are only those that both compression and decompression need
 *
 */

class LzHuf {
protected:
	UBYTE  text_buf[N + F - 1];

	UWORD freq[T + 1];  			/* frequency table */
	WORD prnt[T + N_CHAR];    	/* points to parent node */
	WORD son[T];            	/* points to son node (son[i],son[i+]) */

public:

protected:
	void StartHuff ();
	void update (UWORD c);
private:
	void reconst ();
};


#endif /* LZMISC_H */

