/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef ILBMDATA_H
#define ILBMDATA_H

#ifndef __cplusplus
#error ilbmdata.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Constants and structures used by ILBM files
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/04/05  12:29:47  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


enum MaskType {
	mskNone = 0,
	mskHasMask = 1,
	mskHasTransparentColor = 2,
	mskLasso = 3
};

enum CompressType {
	cmpNone = 0,
	cmpByteRun = 1
};

struct BMHD {
	UWORD	w, h;
	UWORD	x, y;
	UBYTE	nPlanes;
	UBYTE	masking;
	UBYTE	compression;
	UBYTE	pad1;
	UWORD	transparentColor;
	UBYTE	xAspect, yAspect;
	UWORD	pageWidth, pageHeight;
};


#endif /* ILBMDATA_H */

