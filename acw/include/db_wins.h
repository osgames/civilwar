/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef DB_WINS_H
#define DB_WINS_H

#ifndef __cplusplus
#error db_wins.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Database SetupWins definition
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "image.h"
#include "font.h"

class TextWindow;
class retrace;
class TextLoad;

/*
 * Enumerations
 */

enum wareas { text = 0, heading, job, picback };

/*
 *   This class controls the regions on screen and the text routines
 */

class SetUpWins {

protected:

	Point *where;

	Region *picture;
	
	Region *region00;
	Region *region01;
	Region *region02;
	Region *region03;
	Region *region04;

	TextWindow *win00;
	TextWindow *win01;
	TextWindow *win02;
	TextWindow *win03;
	TextWindow *win04;
	
	Sprite *textspr;

	retrace* jumps;
	Image under;
	Image untitle;
	Image unjob;
	Image underpic;
	Image uncol2;
	Image uncol3;

	char error;
	char page;
	long size;
	long tsize;

	unsigned short textfileptr;
	char *filename;
	int columns;
	int prevcols;
	int numa;

	TextLoad *textl;
	char *original;

	TextLoad *ptrs;
	char *orig;
	enum until { NO, YES };
	until bold;
	until underline;

	unsigned int filePos;

	FontID n;

	char format;

	int *PointerOffset;
	int picflag;
	
public:
	SetUpWins();
	~SetUpWins();

	void UpWins( FontID nn ); // char pag, short int totalWidth [], short int totalHeight[], short int text_xpos[],	short int text_ypos [], FontID nn );
	void changewins(); //  char pag, short int totalWidth [], short int totalHeight[], short int text_xpos[],	short int text_ypos [] );
	void changewins( char cols, char numa, char pag, short int totalWidth [], short int totalHeight[], short int text_xpos[],	short int text_ypos [] );
	char writeline( TextWindow *win, Region *bm, const Point& wh, char line[], char keytotal, Font *font, short int totalWidth, short int totalHeight  );

	Sprite* getTextSpr() { return textspr; }
	void deltextspr( short int totalWidth, short int totalHeight );

	Region* retReg( char win );
	Region& retAReg( char win );
	TextWindow* retWin( char win );
	
	// void setWhere( short int text_xpos[], short int text_ypos[] );
	Point getwhere( int choice );		  
	void resetwin( int tiltles = 0 );
	void upDatewin();
	void resetarea( wareas area );
	void setwin( short int totalHeight[], short int totalWidth[]);
	unsigned int getTOffset();
	unsigned short int getKeyOffset( char key);	// , int *BasePtrOffset );

	void setTFP2( unsigned short tfp );
	void setTextFP( unsigned short int tfp );

	unsigned short int getTextFP() { return textfileptr; }
	unsigned short int findlastTFP();

	void modTextFP( signed char dat );

	char getError() { return error; }
	char *getOriginal() { return original;	}
	char *getOrig() { return orig; }
	long getsz() { return size; }
	long getPtrsz() { return tsize; }
	void dispointers();

	int getcols() { return columns; }
	int getnuma() { return numa; }
	void pushFormat( int offset, unsigned char NewNum, long int Npaper[] );
	int popFormat( char *form, unsigned char* NewNum, long int Npaper[] );
	int getjumpcount();
	void getTitles();
	
	char getFormat() { return format; }
	void setFormat( char form ) { format = form; }

	int getPointerOffset ( char dat ) { return PointerOffset[ dat ]; }
	int getPicFlag() { return picflag; }
	unsigned int getFilePos() { return filePos; }
};

#endif /* DB_WINS_H */

