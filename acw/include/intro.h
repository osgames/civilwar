/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef INTRO_H
#define INTRO_H

#ifndef __cplusplus
#error battle.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *      Civil War Introduction
 *
 *----------------------------------------------------------------------
 *
 *----------------------------------------------------------------------
 */
 
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_mixer.h"

class Introduction {
private:
  SDL_Surface *title;
  SDL_Surface *image;
  SDL_Surface *screen;
  SDL_Rect titleDest;

  Mix_Chunk *wave;

public:

  Introduction();
  ~Introduction();
  void run();

private:

  void playAudio();
  void fade(SDL_Surface *image,SDL_Surface *screen);
}; 
 
#endif /* INTRO_H */
