/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SIDE_H
#define SIDE_H

#ifndef __cplusplus
#error side.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Things to do with what side something is on
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"
#include "myassert.h"

#if 0		// pre version 2.01


typedef UBYTE Side;			// Bitwise Side enumeration
typedef UBYTE SideIndex;	// Linear Side enumeration

extern Side indexToSideTable[SideCount];
extern SideIndex sideToIndexTable[1 << SideCount];


inline Side indexToSide(int i)
{
	ASSERT( (i >= 0) && (i < SideCount));
 	// return 1 << i;
	return indexToSideTable[i];
}

#define SIDE_USA indexToSide(SideI_USA)		// = 1
#define SIDE_CSA indexToSide(SideI_CSA)		// = 2
#define SIDE_None 0
#define SIDE_Both (SIDE_USA | SIDE_CSA)

inline SideIndex sideIndex(Side side)
{
	ASSERT((side == SIDE_USA) || (side == SIDE_CSA));

	return sideToIndexTable[side];
}


inline Side otherSide(Side s)
{
	return s ^ (SIDE_CSA | SIDE_USA);
}

inline Boolean onOtherSide(Side s1, Side s2)
{
	return (s1 ^ s2) == SIDE_Both;
}

#elif 0		// Experimental Version 2.01


enum ISideEnum {
	SideI_USA,
	SideI_CSA,

	SideCount		// There are 2 sides
};



enum SideEnum {
		SIDE_USA = 1 << SideI_USA,
		SIDE_CSA = 1 << SideI_CSA,
		SIDE_None = 0,
		SIDE_Both = SIDE_USA | SIDE_CSA
};

/*
 * Side and SideIndex to be classes so that compiler will
 * give errors if misusing them!
 */

class Side {
	SideEnum value;
public:
	Side();
	Side(const Side& s) { value = s.value; }
	Side(SideEnum s) { value = s; }
	operator SideEnum() const { return value; }

	Side otherSide() const
	{
		return Side(SideEnum(value ^ SIDE_Both));
	}
};

class SideIndex {
	ISideEnum value;
public:
	SideIndex();
	SideIndex(const SideIndex& s) { value = s.value; }
	SideIndex(ISideEnum s) { value = s; }
	operator ISideEnum() const { return value; }
};

extern SideEnum indexToSideTable[SideCount];
extern ISideEnum sideToIndexTable[1 << SideCount];

inline Side indexToSide(int i)
{
	ASSERT( (i >= 0) && (i < SideCount));
	return indexToSideTable[i];
}


inline SideIndex sideIndex(Side side)
{
	ASSERT((side == SIDE_USA) || (side == SIDE_CSA));

	return sideToIndexTable[side];
}


inline Side otherSide(Side s)
{
	return s.otherSide();
	// return s ^ (SIDE_CSA | SIDE_USA);
}

inline Boolean onOtherSide(Side s1, Side s2)
{
	return (s1 ^ s2) == SIDE_Both;
}

#else			// Actual 2.01 using enums

enum SideIndex {
	SideI_USA,
	SideI_CSA,

	SideCount		// There are 2 sides
};

enum Side {
	SIDE_USA = 1 << SideI_USA,		// = 1
	SIDE_CSA = 1 << SideI_CSA,		// = 2
	SIDE_None = 0,
	SIDE_Both = (SIDE_USA | SIDE_CSA)
};

// typedef UBYTE Side;			// Bitwise Side enumeration
// typedef UBYTE SideIndex;	// Linear Side enumeration

extern Side indexToSideTable[SideCount];
extern SideIndex sideToIndexTable[1 << SideCount];



inline Side indexToSide(SideIndex i)
{
	ASSERT( (i >= 0) && (i < SideCount));
 	// return 1 << i;
	return indexToSideTable[i];
}


inline SideIndex sideIndex(Side side)
{
	ASSERT((side == SIDE_USA) || (side == SIDE_CSA));

	return sideToIndexTable[side];
}


inline Side otherSide(Side s)
{
	return Side(s ^ (SIDE_CSA | SIDE_USA));
}

inline Boolean onOtherSide(Side s1, Side s2)
{
	return (s1 ^ s2) == SIDE_Both;
}


#endif		// v 2.01

#endif /* SIDE_H */

