/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef AITESTS_H
#define AITESTS_H

#ifndef __cplusplus
#error aitests.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Some functions used by AI
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:12  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "gamedef.h"

enum retValue { FAIL = 0, EQUAL, PASS, FAIL_BADLY };

class General;
class Unit;

class aitests {

	public:
		aitests(); 
		~aitests();

		retValue Efficiency_Test( General *general, int loyal, int enemy, AttributeLevel randomNum, int additional = 0 );
		retValue Ability_Test( General *general ) const;
		retValue Aggression_Test( General *general ) const;
		int Aggressvalue( Unit *loyal );
		General* findArmyGeneral( Unit *un );
};

#ifdef CHRIS_AI

enum AIPhase {
	AIP_Init,
	AIP_EnemyNearFacility,
	AIP_NearEnemyFacility,
	AIP_ArmyNearMyArmy,
	AIP_OccupyingEnemy,
	AIP_SelectTarget
};

/*
 * Place to store Unit's AI Data
 */

class UnitAI {

public:
#if defined(NOBITFIELD)
	Boolean RespondingToEnemy;
	Boolean ToBeOrdered;
	Boolean ToBeAttacked;
	Boolean AtFacility;
	Boolean Garrison;
#else
	Boolean RespondingToEnemy:1;
	Boolean ToBeOrdered:1;
	Boolean ToBeAttacked:1;
	Boolean AtFacility:1;
	Boolean Garrison:1;
#endif

	AIPhase CheckFlag;

	union {
		Facility* fac;
		FacilityID facID;
	};

	// OrderMode action;					  // action to be taken on arrival

	UnitAI();
	~UnitAI();

private:
	/*
	 * Any Internal functions used by the AI...
	 */
};


#endif
#endif /* AITESTS_H */

