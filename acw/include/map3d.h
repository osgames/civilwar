/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MAP3D_H
#define MAP3D_H

#ifndef __cplusplus
#error map3d.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	3D Map Definitions
 *
 * Started yet again!
 * This time with seperate maps for different zoom levels
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.28  1994/09/23  13:29:31  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.27  1994/09/02  21:27:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.26  1994/08/31  15:26:29  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.25  1994/08/24  15:07:25  Steven_Green
 * *** empty log message ***
 *
 *
 *----------------------------------------------------------------------
 */


#include "types3d.h"
#include "terrain.h"
#include "view3d.h"

class System3D;
class Grid;

typedef WORD MapGridCord;		// Map grid reference

struct Height8 {
	UBYTE height;

	Height8() { }
	Height8(UBYTE h) { height = h; }

	void setHeight(Cord3D h) { height = h >> 3; }
	Cord3D getHeight() const { return height << 3; }
};

#define MaxGridHeight (0xff << 3)


/*
 * An individual grid
 */

struct SmoothMatrix {
	UBYTE v[3][3];
};

struct RiverMakeStruct {
	MapGridCord x1;
	MapGridCord y1;
	MapGridCord x2;
	MapGridCord y2;
	UBYTE height;
	UBYTE plainWidth;
	UBYTE curvature;		// 0=straight there, 100=really bendy, 33=random
};

struct Grid {
	UWORD gridCount;			// Number of squares
	UWORD pointCount;			// Number of points (always gridCount+1)
	Cord3D gridSize;			// Physical size of a grid square

	Height8* heights;			// Packed height [gridCount+1][gridCount+1]
	TerrainType* terrains;	// Terrain in each square

	Grid()
	{
		gridCount = 0;
		gridSize = 0;
		heights = 0;
		terrains = 0;
	}


	~Grid()
	{
		delete heights;
		delete terrains;
	}

	void flatten(int x, int z, UBYTE h, int radius);
	void makeRoad(Cord3D startX, Cord3D startZ, Cord3D endX, Cord3D endZ, UBYTE curvature);
	void averageSquare(int x, int z);
#ifdef BATEDIT
	void makePlateau(MapGridCord x, MapGridCord z, UBYTE height, int radius);
	void makeHill(int x, int z, UBYTE height, int radius, Boolean raise);
	void makeHillLine(int x, int z, UBYTE height, int radius, Boolean raise);
#else
	void makeHill(int x, int z, UBYTE height, int radius);
	void makeHillLine(int x, int z, UBYTE height, int radius);
#endif
	void adjustHeight(int x, int z, UBYTE newHeight, Boolean raise);
	void flattenRivers();
	void setMaxHeight(int x, int z, UBYTE newHeight);
	
	Height8* getHSquare(int x, int y) const
	{
		return &heights[x + y * (gridCount + 1)];
	}

	TerrainType* getTSquare(int x, int y) const
	{
		return &terrains[x + y * gridCount];
	}

	Boolean isValidHCord(MapGridCord x, MapGridCord y) const
	{
		return( (x >= 0) && (x < pointCount) &&
			 	  (y >= 0) && (y < pointCount) );
	}

	Boolean isValidTCord(MapGridCord x, MapGridCord y) const
	{
		return( (x >= 0) && (x < gridCount) &&
			 	  (y >= 0) && (y < gridCount) );
	}

	Height8 getHeight(MapGridCord x, MapGridCord y) const
	{
		if(isValidHCord(x, y))
			return *getHSquare(x, y);
		else
			return Height8(0);
	}

	TerrainType getTerrain(MapGridCord x, MapGridCord y) const
	{
		if(isValidTCord(x, y))
			return *getTSquare(x, y);
		else
		{
#ifdef BATEDIT
			return IllegalTerrain;
#else
			return PastureTerrain;
#endif
		}
	}

	void smooth();
	void smooth(const SmoothMatrix& sf);
	void clearTerrain(TerrainType t, Boolean riverProt, Boolean roadProt);
	void flatten(Cord3D height = 0);
	void roughen(UBYTE amount);
#ifdef BATEDIT
	void removeRivers();
	void removeRoads();
	void adjustRiverCord(MapGridCord& x, MapGridCord& y, Boolean endPoint, MapGridCord range);
	void adjustRoadCord(MapGridCord& x, MapGridCord& y, Boolean endPoint, MapGridCord range);
	MapGridCord findCloseEdge(MapGridCord& x, MapGridCord& y, Boolean endPoint);
#endif
	void tidyRivers();
	void tidyRoads();
	void tidy();
	void placeTerrain(TerrainType t, MapGridCord x, MapGridCord y, UBYTE size, Boolean riverProt, Boolean roadProt);
	void removeTerrain(TerrainType t);
	void makeRiver(RiverMakeStruct* rm);
	void makeRiver();
};

/*
 * Define the grids
 */

enum GridLevel {
	Grid_Min,

	Grid_128 = Grid_Min,		// Used by 1/2 and 1 mile (8 and 16 across)
	Grid_64,			// 2 Mile
	Grid_32,			// 4 Mile
	Grid_16,			// 8 Mile
#ifdef BATEDIT
	Grid_8,
	Grid_4,
	Grid_2,
	Grid_Max = Grid_2,
#else
	Grid_Max = Grid_16,
#endif

	GridCount		// How many grids there are
};

/*==============================================
 * The Overall Terrain Grid Class
 */

class MapGrid {
	Cord3D size;				// Total size of grid

	/*
	 * Terrain structure
	 */

	Grid grids[GridCount];
	static GridLevel gridNum[ZOOM_LEVELS];

public:
	MapGrid();
	~MapGrid();

#ifdef BATEDIT
	void create(System3D& drawData, UBYTE hillHeight, int hillRadius);
#else
#ifdef OLD_TESTING
	void create(System3D& drawData);				// Make a random grid
#endif
#endif

	Cord3D getHeight(Cord3D x, Cord3D z) const;
	Cord3D getHeightView(const ViewPoint& view, Cord3D x, Cord3D z) const;
	void updateViewGrid(System3D& drawData);
	Cord3D getTotalWidth() const { return size; }
	Cord3D getTotalHeight() const { return size; }
	Cord3D getScrollDistance(ZoomLevel zoom) const
	{
		return getGrid(zoom)->gridSize;
	}

	const Grid* getGrid(ZoomLevel zoom) const
	{
		return &grids[gridNum[zoom]];
	}

	Grid* getGrid(ZoomLevel zoom)
	{
		return &grids[gridNum[zoom]];
	}

	void makeZoomedGrids(System3D& drawData);

	MapGridCord cord3DtoGrid(Cord3D val);
	Cord3D gridToCord3D(MapGridCord val);

private:

	void makeHalfGrid(System3D& drawData, Grid* dest, Grid* src);
#ifdef DEBUG
	void logGrid();
#endif
};

#endif /* MAP3D_H */

