/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef BATDISP_H
#define BATDISP_H

#ifndef __cplusplus
#error batdisp.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Moveable Battle Display Objects
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:12  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/09/23  13:29:31  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types3d.h"
#include "sprlist.h"
#include "sprlib.h"

class ViewPoint;
class Unit;

/*
 * The BattleDisplayObject class is usually used as a base class
 * for other types of objects.
 */

class BattleDisplayObject {
	friend class BattleDisplayList;

	BattleDisplayObject* next;			// In Battle Display list when on screen
protected:
#if defined(NOBITFIELD)
	Boolean inDisplayList;
#else
	Boolean inDisplayList:1;
#endif
	UBYTE spriteCount;
	SpriteID* sprElements;

public:
	BattleDisplayObject();
	virtual ~BattleDisplayObject();

	// virtual void unLink() = 0;			// Remove from sprite list
	void unDisplay();						// Completely reset
	void unLink();
	virtual void clearUp() = 0;		// virtual function to reset
	virtual void draw(const ViewPoint& view) { }
};

/*
 * List of objects on display
 * This is used only to clear up when the viewpoint is changed
 * by calling each object's reset function
 */

class BattleDisplayList {
	BattleDisplayObject* entry;
public:
	BattleDisplayList() { entry = 0; }
	~BattleDisplayList() { clear(); }

	void clear();					// Clear up if viewpoint moved
	void add(BattleDisplayObject* ob);
	void remove(BattleDisplayObject* ob);
};


/*
 * Display Commander List
 */

struct DispCommander {
	DispCommander* next;
	Unit* u;
	Point p;
};

class DispCommandList {
	DispCommander* entry;
public:
	DispCommandList();
	~DispCommandList();

	void clear();

	void add(Unit* u, const Point& p);
	Unit* getCloseCommander(const Point& p);

	DispCommander* getNext(DispCommander* last = 0)
	{
		if(last)
			return last->next;
		else
			return entry;
	}
};

struct AnimationTable {
	UBYTE frames;
	SpriteIndex baseSprite;
	UBYTE sprites[5][8];
};

#endif /* BATDISP_H */

