/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MEMPTR_H
#define MEMPTR_H

#ifndef __cplusplus
#error memptr.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Memory Pointer Template
 * as defined in Stroustrup's "The C++ Programming Language"
 * in the chapter about exceptions.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1993/12/10  16:08:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/12/04  16:26:00  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG
#include "error.h"
#endif
#include "myassert.h"

template<class T>
class MemPtr {
	T* ptr;
public:
	MemPtr(size_t n)
	{
		ptr = new T[n];
	};

#ifdef DEBUG
	MemPtr(MemPtr<T>& m)
	{
		throw GeneralError("MemPtr<T> is being copy constructed in " __FILE__ ", Line %d\nTry casting it to (T*)", __LINE__);
	}

	MemPtr<T>& operator = (MemPtr<T>& m)
	{
		throw GeneralError("MemPtr<T> is being copied with =() in " __FILE__ ", Line %d\nTry casting it to (T*)", __LINE__);
	}
#endif

	~MemPtr()
	{
		ASSERT(ptr != 0);

		delete[] ptr;
		ptr = 0;
	};

	operator T* () const
	{
		ASSERT(ptr != 0);
		return ptr;
	};

};

template<class T>
class TPtr {
	T* ptr;
public:
	TPtr() { ptr = 0; }
	~TPtr()
	{
		ASSERT(ptr != 0);

		delete ptr;
		ptr = 0;
	}

#ifdef DEBUG
	TPtr(TPtr<T>& t)
	{
		throw GeneralError("TPtr<T> copy constructor unwritten in " __FILE__ ", Line %d", __LINE__);
	}

	TPtr<T>& operator = (TPtr<T>& t)
	{
		throw GeneralError("TPtr<T> = function unwritten in " __FILE__ ", Line %d", __LINE__);
	}
#endif

	operator T* () const
	{
		ASSERT(ptr != 0);
		return ptr;
	};

	T* operator =(T* t)
	{
		ASSERT(ptr != 0);

		ptr = t;
		return t;
	}
};

#endif /* MEMPTR_H */

