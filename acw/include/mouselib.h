/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MOUSELIB_H
#define MOUSELIB_H

#ifndef __cplusplus
#error mouselib.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Mouse Pointer
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.22  1994/07/04  13:33:12  Steven_Green
 *
 * Revision 1.18  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.14  1994/04/22  22:16:24  Steven_Green
 * Icon Handling Tidied Up
 *
 * Revision 1.1  1994/01/24  21:21:20  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "mouse.h"
#include "sprlib.h"
#include "micelib.h"
// #include "mouseptr.h"

/*
 * Undefined classes
 */

class Screen;

/*
 * Mouse Library Class Definition
 */

class MouseLibrary : public Mouse {	// , public SpriteLibrary {
	SpriteLibrary* sprLib;
	PointerIndex index;			// Current pointer number
public:
	MouseLibrary(Screen* screen, SpriteLibrary* lib);
	~MouseLibrary();

	PointerIndex setPointer(PointerIndex i);
	PointerIndex getPointer() const { return index; }
};

#endif /* MOUSELIB_H */

