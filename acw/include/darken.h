/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef DARKEN_H
#define DARKEN_H

#ifndef __cplusplus
#error darken.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Colour Palette remap table creations
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "gr_types.h"
// class Palette;
class Region;
class Rect;

// void makeGreyTable(Colour* dest, const Palette* src);
// Colour* makeGreyTable(const Palette* src);

void greyRegion(Region* region, const Rect& r);
void brightenRegion(Region* region, const Rect& r);
void colourizeRegion(Region* region, const Rect& r, Colour c);

#endif /* DARKEN_H */

