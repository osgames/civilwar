/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CAMPCITY_H
#define CAMPCITY_H

#ifndef __cplusplus
#error campcity.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Campaign Resource User Interface
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.7  1994/07/04  13:31:45  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/06/21  18:45:38  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.2  1994/05/25  23:33:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/05/21  13:18:12  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


#include "campmode.h"

/*
 * Forward class references
 */

class Facility;
class MapWindow;
class Event;
class Location;

/*
 * Facility Tracking execute mode
 */

class TrackCity : public CampaignFunction {

	enum {
		Tracking,		// Waiting for user to select city
		Ordering			// Icons are up... user must decide what to do
	} mode;



public:
	Boolean forceUpdate;		// Set to force city Info to be redisplayed
	Facility* currentFacility;		// Facility currently highlighted

public:
	TrackCity(CampaignMode* p);
	~TrackCity();

	void overMap(MapWindow* map, Event* event, MenuData* d, const Location* l);
	void offMap();

	void closeDown();

private:
};

#endif /* CAMPCITY_H */

