/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef DBMEM_H
#define DBMEM_H

#ifndef __cplusplus
#error dbmem.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * 	Class to handle database memory and keyword store;
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */


// Ten memories, each of which can contain a maximum of 61 Text page offsets

typedef long int Textpages[10][61];

const int maxTextpages = 61;

struct Keyinfo {				// Pass this to texttest eventually;
 			short int kx;
			short int ky;
			char name[43];
			short int width;    		// Use getWidth ( keyword );
			short int height;   		//  "	 getHeight(    "    );

			char *basetextptr;
			char *textpointer;
};

extern Keyinfo keyinfo[28];			// Up to 28 keywords on screen at once

struct Memory {
			char* colbegin;
			char* textbegin;
			char* textend;
			int basepic;
			int baseanim;
};

extern Memory memory;          		

class retrace {		// class to handle memory to go back to previous screens

		struct store
		{
				char format;
				unsigned char NewsNum;
				unsigned short int chapter;
				int offset;
		};

		store *undo;

		Textpages Newspaper;

		int count;
		
		int maxcount;

	public:
		retrace();
		~retrace();
		int pop( char *form, unsigned char *NewsNum, unsigned short int *chap, long int pages[] );
		void push( char form, unsigned char NewsNum, int textoffset, unsigned short int chap, long int pages[] );

		int getcount() { return count; }
};


long findkeyword(char *original, char key, long tsize);

#endif /* DBMEM_H */

