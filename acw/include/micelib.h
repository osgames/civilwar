/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MICELIB_H
#define MICELIB_H

#ifndef __cplusplus
#error micelib.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Mouse Pointer enumerations
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "micespr.h"

enum PointerIndex {
	M_None = -1,

	/*
	 * Miscellaneous
	 */

	M_Arrow = SPR_mice,
	M_Question,
	M_Zoom,			// Magnify Glass
	M_Zoom1,			// Cross Hairs
	M_Busy,

	/*
	 * Campaign Mode
	 */

	M_TrackCity,		// Tracking Cities
	M_TrackUnit,		// Tracking Units

	M_WhereTo,			// Pointing Up
	M_WhereTo1,			// Pointing Down

	M_MoveRail,			// Get rail destination
	M_MoveWater,		// Get water destination

	/*
	 * From CALicon
	 */

	M_CAL_Infantry,
	M_CAL_InfantryDrop,
	M_CAL_Cavalry,
	M_CAL_CavalryDrop,
	M_CAL_Artillery,
	M_CAL_ArtilleryDrop,

	M_CAL_Army1,
	M_CAL_Army1Drop,
	M_CAL_Army2,
	M_CAL_Army2Drop,
	M_CAL_Corps1,
	M_CAL_Corps1Drop,
	M_CAL_Corps2,
	M_CAL_Corps2Drop,
	M_CAL_Division1,
	M_CAL_Division1Drop,
	M_CAL_Division2,
	M_CAL_Division2Drop,
	M_CAL_Brigade1,
	M_CAL_Brigade1Drop,
	M_CAL_Brigade2,
	M_CAL_Brigade2Drop,

	M_CAL_Army1U,
	M_CAL_Army1UDrop,
	M_CAL_Army2U,
	M_CAL_Army2UDrop,
	M_CAL_Corps1U,
	M_CAL_Corps1UDrop,
	M_CAL_Corps2U,
	M_CAL_Corps2UDrop,
	M_CAL_Division1U,
	M_CAL_Division1UDrop,
	M_CAL_Division2U,
	M_CAL_Division2UDrop,
	M_CAL_Brigade1U,
	M_CAL_Brigade1UDrop,
	M_CAL_Brigade2U,
	M_CAL_Brigade2UDrop,

};

#endif /* MICELIB_H */

