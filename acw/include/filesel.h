/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef FILESEL_H
#define FILESEL_H

#ifndef __cplusplus
#error filesel.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	File Selector
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

/*
 * File Selector entry point
 *
 * srcPath should be the full path name including wildcards of suitable
 * files to select
 *
 * If existing is True, then the user may only select an existing file
 * otherwise they may type in a name.
 *
 * Prompt is a string displayed to the player
 *
 * Returns 0 if user clicked OK, or -1 if cancelled.
 * The srcPath will contain a complete filename if return is 0.  If the
 * user cancelled then the contents of srcPath will be unaltered.
 *
 * e.g.
 *
 * char pathName[FILENAME_MAX];
 * strcpy(pathName, "c:\\data\\*.dat");
 * if(fileSelect(pathName, "Load a data file", True) == 0)
 *		loadFile(pathName);
 *
 */

int fileSelect(char* srcPath, const char* prompt, Boolean existing);

#endif /* FILESEL_H */

