/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */

#ifndef AI_BAT_H
#define AI_BAT_H

#ifndef __cplusplus
#error ai_bat.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	The AI battle routines
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:12  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include <iostream.h>

#include "gamedef.h"
#include "aitests.h"
#include "timer.h"
#include "unit3d.h"
#include "fstream.h"
#include "array.h"

class OrderBattle;
class President;

enum AI_Tactic {
	Frontal_Assault = 0,
	Left_Flanking,
	Right_Flanking,
	Pincer,
	Feint_Left_Right,	 // = 4,
	Feint_Right_Left,
	Line_Defence,
	Defence_In_Depth,
	Feint_Defence,		 // = 8,
	Holding_Action,
	Covering_Action,
	Withdraw,
	Retreat,				 // = 12,

	Max_Tactic

};

struct deployment { 

	int RegType[3];

};

enum AI_Instruction{

	waitForRest = 0,
	followRoute

};

static char deployTable[13][ 6 ] = {

	// 4 = centre; 1 = rightFlank; 2 = LeftFlank; 3 = Reserve;
	// 5 = rear right flank; 6 = rear left flank;

	{ 4, 3, 2, 1, 5, 6 },
	{ 2, 4, 3, 1, 5, 6 },
	{ 1, 4, 3, 2, 6, 5 },
	{ 2, 1, 4, 3, 6, 5 },
	{ 2, 3, 4, 1, 5, 6 },
	{ 1, 3, 4, 2, 5, 6 },
	
	{ 4, 3, 1, 2, 6, 5 },
	{ 4, 3, 1, 2, 6, 5 },
	{ 4, 3, 1, 5, 2, 6 },
	{ 4, 3, 1, 2, 6, 5 },

	{ 3, 4, 2, 1, 5, 6 },
	{ 3, 4, 2, 1, 5, 6 },
	{ 3, 4, 2, 1, 5, 6 },

};

class UnitPtr {
	Unit* unit;
public:
	UnitPtr() { unit = 0; }
	UnitPtr(Unit* u) { unit = u; }
	operator Unit*() { return unit; }
};

class AI_Battle : public aitests {
#if defined(NOBITFIELD)
	Boolean OnBattlefield;
	Boolean TactAdvantage;
	Boolean AttackFlag;
	Boolean TimeArray;
#else
	Boolean OnBattlefield:1;
	Boolean TactAdvantage:1;
	Boolean AttackFlag:1;
	Boolean TimeArray:1;
#endif

	OrderBattle *ob;

	Side side;

	Array<UnitPtr> Armylist;

	Boolean Lflag: 1;
	Boolean FitFlag: 1;

	int MAXCorpsAL;

	President *pres;

	Unit* senior;

	AI_Tactic tact;

	int corpsTotal;

	Location deployCentre;

	Wangle ang;							// The angle between the two armies

	// Strength strs[2][3];

	TimeBase* time;

public:
	AI_Battle();
	~AI_Battle();

	Boolean AI_Bat_Initiate( Side s, Boolean Historical = True );

	void UnitWithdrawing( Unit* reg );
	void AIReachedClickPoint( Unit* lead, int tempT = 0, Boolean SkipFlag = 0 );
	void NewPlayerClickPoint( Unit* enemy, Unit* loyal );
	int CheckEnemyOrders( Unit* loyal, Unit* enemy );

	CombatValue CalcStrength( Side s );

	// int calc_BrigadeCV( Unit* uit );
	void sendInitialOrders( Boolean initial = 0 );

private:
	void FindDeployCentre( Side s, Unit* loyal = 0 );
	
	void DeployTroops(Side deploySide, Boolean Historical);
	// void DeployPlayerTroops();

	void faceArmies( Side s );
	Boolean IsEnemyInWay( Unit* loyal, Wangle direc );

	void DeployToBL( Location& dest, Unit* loyal );
	void OrderToBL ( Location* dest, Unit* loyal, Boolean Flag = 0 );

	void ReinforcementsRequested( Unit* loyal, Unit* enemy, signed int size = 0 );
	Unit* moveSimilarUnit( Unit* loyal, Unit* enemy, OrderMode action );
	void FillInGap ( Unit* loyal );

	Boolean findSeniorCommander();

	Boolean NotToClose( int xp, int yp );


#ifdef DEBUG
	Unit* findEnemySenior();
#endif

	void IsCPOnBattlefield();
	// void setArmyList();

	void SelectTactic();
	retValue CalcTactAdvantage();

	void placeCorps( Unit* loyalCorps, int which, int nocorps = 255 );
	void placeDivision( Unit* loyalDivision, Location* bl, int which = 0 );
	void placeBrigade( Unit* loyalBrigade, Location* dest, int which = 0 );
	void placeSuperior( Unit* super, Boolean deploy = 0, Location* tempbl = 0, Location* deployO = 0 );

	Location averageEnemyLoc( Side s, Unit* loyal = 0, Boolean allFlag = 0 );
	Location closestEnemyLoc( Side s, Unit* loyal, Location el );

	Boolean IsArmyOnMainBF( Unit* enemyArmy );
	Boolean IsArmyOffMainBF( Unit* enemyArmy );

	Distance getfireRange( Unit* enemy ) { return 1000; }

	int findNumCorpsPerArmy( Unit* loyalArmy );
	int findNumCorps();
	int findNumDivisions();
	int findNumBrigades();

	int findNumChildren( Unit* father, Boolean Flag = 0 );

	Location findBestDefPos( Unit* loyal, Unit* enemy );

	Location KeepFormation( int which, Location enemy, Boolean Before = 0 );

	Boolean CheckCommander( Unit* loyal, Unit* enemy, OrderMode action );
	void getReinforcements( Unit* enemy, OrderMode action, signed int which );

	Unit* findClosestReserve( Unit* enemy, Rank of );
	Location findClosestLow( Unit* loyal, Unit* enemy, Boolean highest = 0 );

	Location findClosestLow( Unit* loyal, Location enemyLoc, int flag = 0 );

	Location findClosestHigh( Location loyal, Location enemyLoc, Boolean direc4 = True );

	Unit* FindClosestEnemy( Unit* loyal, int flank = 0, Unit* loyal2 = 0  );

	void clearEnemyPointer( Unit* enemy );

	Unit* getSideUnit( Boolean Left = 0 );

	// void BuildRoute( Unit* loyal, Unit* enemy );

	void tempAssignUnits();
	void doTempAssign(Unit* u);

	void ReassignUnits();
	void AI_Battle::doReassign(Unit* u);

	void getKeyTerrain( Unit* unit, int deep = 3, Location* sofar = 0 );
	Boolean LOSandHER( Unit* loyal = 0, int CheckEnemy = 0 );
	
	Location getDestOffset( Unit* loyal, Location& dest );

	void sendAIOrder( OrderMode action, const Location& dest, Unit* loyal, Unit* enemy = 0 );
	void withDraw( Unit* loyal, int enemyStr );

	Wangle withdrawAngle( Unit* loyal );

	// Patch Function below !!!!

	void clearSeperated( Unit* loyal, Boolean forget = 0 );
	void DoesCorpFit( Unit* loyalCorps );
	void orderChildren( Unit* loyal, OrderMode action, const Location& dest );


#ifdef DEBUG_CHRIS
	void PutInLog( int num );
	void PutInLog( char* name );
	void PutInLog( const char* name );
	
	void PutOrderMode( OrderMode action );
	void PutTactic( AI_Tactic tactic );

	void LogInfo();
#endif
	
};

#ifdef DEBUG
class LogFile;
extern LogFile aiBLog;
#endif


#endif /* AI_BAT_H */

