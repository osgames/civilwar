/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SNDLIB_H
#define SNDLIB_H

#ifndef __cplusplus
#error sndlib.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Sound Library Class
 *
 * A Sound library is a class whereby a program can ask for a sound
 * effect to be played.  The library will take care of loading the
 * sample into memory, playing it and freeing it afterwards.  A cache
 * system will be used so that samples used several times are only
 * loaded once and samples will remain in memory until the memory
 * manager asks for more room.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG
// #define SOUND_DEBUG		// Enable Logging of sound information
#endif


#include "sound.h"
#include "ptrlist.h"
// #include "memlog.h"

#ifdef SOUND_DEBUG
#include "clog.h"
extern LogFile soundLog;
#endif




typedef UWORD SoundID;
typedef UBYTE Volume;
typedef BYTE Panning;
typedef UWORD Frequency;
typedef UBYTE SoundPriority;

#define MaxVolume 0x40
#define MaxPan		0x3f		// 0 = Centre, -63 = Left, +63 = Right

#define MinSoundPriority 0
#define MaxSoundPriority 0xff


class SoundLibrary;

/*===============================================================
 * Sample
 *
 * This class set up so that I can force SAMPLEINFO to be initialised
 * SAMPLEINFO is defined in DSMI
 */

#if defined(SOUND_DSMI)

class Sample : public SAMPLEINFO {
public:
	/*
	 * Data
	 */

	Frequency sampleRate;		// Hz to play back at

	/*
	 * Functions
	 */

	Sample();
	~Sample();
};

#else
struct Sample { };
#endif


/*===========================================================
 * Sound Block
 */

class SoundBlock {
public:

	/*
	 * Date
	 */

	SoundLibrary* library;		// Which library owns this sound?
	Sample sample;

	/*
	 * Functions
	 */

	SoundBlock();
	~SoundBlock();

	void release();
	Boolean onlyUser();
};

/*========================================================
 * Sound Cache List
 */


/*==============================================================
 * Sound Library
 */

class SoundCacheList;
class SoundMemHandle;
class SoundCacheItem;

class SoundLibrary {

	/*
	 * Private Data
	 */

	SoundCacheList* cache;
	const char** names;

	/*
	 * Public Functions
	 */

public:

	SoundLibrary(const char* name, const char** fileNames);		// Name of sound file
	~SoundLibrary();

	void playSound(SoundID id, Volume volume, Panning pan, SoundPriority priority = MinSoundPriority);
	void release(SoundBlock* sound);
	void release(SoundID n);
	Boolean onlyUser(SoundBlock* sound);

	/*
	 * Private Functions
	 */

private:
};

#if defined(SOUND_DSMI)
/*============================================================
 * Raw Sound Manager
 */

struct SoundChannel {
	SoundBlock* sample;		// What sample is being played?
	SoundPriority priority;
};

class SoundManager {
	SoundChannel channels[EffectChannelCount];

	UBYTE nextChannel;				// Next Channel to use

public:
	SoundManager();
	~SoundManager();

	int playSound(SoundBlock* sound, Volume volume, Panning pan, SoundPriority priority);
	void stopSound(int channel);
	void stopAll();

	void process();				// To be called every game loop
};


#else	/// Dummy Sound Manager

class SoundManager {
public:
	SoundManager() { }
	~SoundManager() { }

	int playSound(SoundBlock* sound, Volume volume, Panning pan, SoundPriority priority) { return 0; }
	void stopSound(int channel) { }
	void stopAll() { }

	void process() { }
};

#endif

extern SoundManager soundManager;

#endif /* SNDLIB_H */

