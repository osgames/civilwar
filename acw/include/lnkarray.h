/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef LNKARRAY_H
#define LNKARRAY_H

#ifndef __cplusplus
#error lnkarray.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Linked Array class
 *
 * Used for buffers of fixed size objects to avoid lots of
 * calls to ::new and ::delete
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1993/12/01  15:12:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/30  02:57:48  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


#ifdef DEBUG
#include "error.h"
#endif

typedef unsigned short SlotIndex;

template<class T, SlotIndex howMany>
class LinkedArraySlot
{
	friend class LinkedArray<T, howMany>;

	T slots[howMany];
	SlotIndex remaining;
	LinkedArraySlot<T,howMany>* next;
public:
	LinkedArraySlot() { remaining = howMany; next = 0; };
	~LinkedArraySlot() { delete next; };
};

template<class T,SlotIndex howMany>
class LinkedArray {
	LinkedArraySlot<T,howMany>* entry;
public:
	LinkedArray() { entry = 0; };
	~LinkedArray() { delete entry; };

#ifdef DEBUG
	LinkedArray(LinkedArray<T>& t)
	{
		throw GeneralError("LinkedArray<T> copy constructor unwritten");
	}

	LinkedArray<T>& operator = (LinkedArray<T>& t)
	{
		throw GeneralError("LinkedArray<T> = function unwritten");
	}
#endif

	T* findNew();
	void release(T* t);
};


template<class T,SlotIndex howMany>
T* LinkedArray<T,howMany>::findNew()
{
	LinkedArraySlot<T,howMany>* slot = entry;

	if(!entry || !slot->remaining)
	{
		slot = new LinkedArraySlot<T,howMany>;

		slot->next = entry;
		entry = slot;
	}

	return &entry->slots[--entry->remaining];

}

template<class T,SlotIndex howMany>
void LinkedArray<T,howMany>::release(T* t)
{
	SlotIndex n = SlotIndex(entry->remaining + 1);
	
	/*
	 * Remove last allocated member if this was the last allocated
	 * slot
	 */

	if( (n != howMany) && (t == &entry->slots[n]) )
		entry->remaining = n;
}



#endif /* LNKARRAY_H */

