/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef CON_MSG_H
#define CON_MSG_H

#ifndef __cplusplus
#error con_msg.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Message ID's for multiplayer game
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

/*
 * A packet as passed to connection sender
 *
 * NB: Rememember to update packetNames[] in connect.cpp if new values
 * are added here!!!!!!!
 */

enum PacketType {
	Connect_HELLO,
	Connect_HelloAck,
	Connect_Acknowledge,			// Serial encoded in data

	Connect_Sync,					// Sync
	Connect_Ping,					// Message to be ignored, sent to see if Remote is awake

	Connect_Go_MainMenu,
	Connect_Disconnect,

	/*
	 * Miscellaneous
	 */

	Connect_Yes,
	Connect_No,
	Connect_Finish,

	/*
	 * Main Menu processing
	 */

	Connect_MM_Setup,				// Initial game setup after connection
	Connect_MM_GameType,			// Select campaign/battle
	Connect_MM_Side,				// CSA/USA
	// Connect_MM_Finish,			// Ask to Leave screen
	// Connect_MM_StartGame,		// Confirm that we want to start the game
	// Connect_MM_NoStartGame,		// Cancel game starting

	/*
	 * Realism screen
	 */

	Connect_RL_Change,			// Change Realism setting
	// Connect_RL_Finish,			// Ask to Leave screen
	// Connect_RL_StartGame,		// Confirm that we want to start the game
	// Connect_RL_NoStartGame,		// Cancel game starting

	/*
	 * Campaign Screen
	 */

	// Connect_CS_Finish,			// Ask to Leave screen
	// Connect_CS_ExitScreen,		// Confirm that we want to exit screen
	// Connect_CS_NoExitScreen,		// Cancel game starting
	Connect_CS_EndDay,

	Connect_CS_Order,				// Send an order to unit
	Connect_CS_SendFleet,		// Send a fleet
	Connect_CS_Mobilise,			// Mobilise something

	Connect_CS_AlertResult,		// Result of alert box

	Connect_CS_TimeUpdate,		// Update the Game time

	/*
	 * Battle Screen
	 */

	// Connect_BS_Finish,			// Ask to Leave screen
	// Connect_BS_ExitScreen,		// Confirm that we want to exit screen
	// Connect_BS_NoExitScreen,	// Cancel game starting

	Connect_BS_Order,				// Give order to unit
	Connect_BS_Tick,				// Number of ticks since last frame

	/*
	 * CAL Screen
	 */

	Connect_CAL_TransferUnit,
	Connect_CAL_TransferGeneral,
	Connect_CAL_MakeNew,
	Connect_CAL_Rejoin,
	Connect_CAL_Reattach,
	Connect_CAL_ReattachAll,

	/*
	 * Miscellaneous
	 */

	Connect_WaitSave,
	Connect_SaveGame,
	Connect_LoadGame,
	Connect_FileExists,
	Connect_FileNotExist,

	/*
	 * Testing and Miscellaneous
	 */

	Connect_TEST,

	Connect_HowMany,
};

#endif /* CON_MSG_H */

