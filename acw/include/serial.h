/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SERIAL_H
#define SERIAL_H

#ifndef __cplusplus
#error serial.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Serial Communications Class
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "connect.h"

class PopupText;

/*
 * Baud Rate Enumeration
 */

enum BaudRate {
	BAUD_1200,
	BAUD_2400,
	BAUD_4800,
	BAUD_9600,
	BAUD_19200,
	BAUD_38400,
	BAUD_57600,
	BAUD_115200,

	BAUD_HowMany
};

enum PortType {
	UART_None,
	UART_8250,
	UART_16450,
	UART_16550,
};


/*
 * Packet header
 */

struct SerialPacket {
	UBYTE id[6];
	ULONG serial;
	UBYTE needReceipt;
	UWORD crc;
#ifdef DEBUG
	ULONG random;				// Random Number to see when out of sync.
#endif

	PacketHead pktHead;
};


struct CommPort {
	UWORD port;
	UBYTE irq;
	PortType available;
	char* name;
};

extern CommPort ports[];

/*
 * The Serial Connection Class
 */

class SerialConnection : public Connection {
private:
	SerialPacket incommingHeader;
	UBYTE incommingData[64];

	enum {
		WaitHeader,
		ReadingHeader,
		ReadingData
	} mode;
	ULONG counter;

	Boolean connected;

protected:
	UBYTE portCount;

	// PtrDList<Packet> packetList;

public:
	SerialConnection();
	~SerialConnection();

	int connect();
	void disconnect();
	int sendRawData(PacketType type, UBYTE* data, size_t length, Boolean receipt, ULONG serial);
	// Packet* receiveData();
	void processIncomming();
	Boolean isLocal() const { return False; }
private:
protected:
	int checkPorts();
	int sendHello();
};

/*
 * Modem Connection class is built on top of the serial class
 * because apart from the connection, all operations are
 * the same as serial.
 *
 * There may be other changes such as having to detect loss of
 * carrier.
 */

class ModemConnection : public SerialConnection {
	PopupText* popup;
	char* popupBuffer;
	char* popupPtr;

	char* initString;
	char* dialString;
	char* answerString;
	char* phoneString;

public:
	ModemConnection();
	~ModemConnection();


	int connect();
	void disconnect();
private:
	int dialModem(const char* initString, const char* dialString, const char* phoneString);
	int answerModem(const char* initString, const char* answerString);
	int initModem(const char* initString);
	void hangup();
	int sendModem(const char* s);
	int waitModem(char* buffer, size_t maxLength);
	int waitConnect();

	void showInfo(Boolean permanent, const char* fmt, ...);
	void killPopup();
};

#endif /* SERIAL_H */

