/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef GLOBPROC_H
#define GLOBPROC_H

#ifndef __cplusplus
#error globproc.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Global Procedures (called from menudata)
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

/*
 * List of any functions to call during process loop
 */

class GlobalProc {
	friend class GlobalProcList;

	GlobalProc* next;
public:
	GlobalProc() { next = 0; }
	virtual ~GlobalProc() { };
	virtual Boolean proc() = 0;		// Return True if to be deleted
};

class GlobalProcList {
	GlobalProc* entry;
	Boolean interlock;				// Flag to prevent re-entrancy
public:
	GlobalProcList();
	~GlobalProcList();

	void proc();
	void add(GlobalProc* p);
	void remove(GlobalProc* p);
};

extern GlobalProcList globalProcedures;


#endif /* GLOBPROC_H */

