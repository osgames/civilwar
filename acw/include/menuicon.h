/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MENUICON_H
#define MENUICON_H

#ifndef __cplusplus
#error menuicon.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Game Menu Definition
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:37  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.27  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.21  1994/04/22  22:16:24  Steven_Green
 * Icon Handling Tidied Up
 *
 * Revision 1.1  1994/01/11  22:30:12  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */

#ifndef ICON_H
#include "icon.h"
#endif
#ifndef ICONLIB_H
#include "gamelib.h"
#endif


class MenuIcon : public Icon {
	GameSprites iconNumber;

public:
	MenuIcon(IconSet* parent, GameSprites n, Rect r, Key k1 = 0, Key k2 = 0);
	void setIcon(GameSprites n) { iconNumber = n; }
	void drawIcon();
};

class NullIcon : public Icon {
public:
	NullIcon(IconSet* parent, Key k1=0, Key k2 = 0) : Icon(parent, Rect(0,0,0,0), k1, k2) { }

	void drawIcon() { }	// Do nothing
};

class ToggleMenuIcon : public MenuIcon {
	GameSprites icon1;
	GameSprites icon2;

	Boolean state;

public:
	ToggleMenuIcon(IconSet* parent, GameSprites i1, GameSprites i2, Rect r, Boolean on = False, Key k1 = 0, Key k2 = 0);

	void execute(Event* event, MenuData* d);
	void drawIcon();

	Boolean getState() const { return state; }
	void setState(Boolean st) { state = st; }
};

class DummyIcon : public Icon {
	const char* text;
public:
	DummyIcon(IconSet* parent, const Rect& r, const char* s, Key k1 = 0, Key k2 = 0) :
		Icon(parent, r, k1, k2),
		text(s)
		{ }

	void drawIcon();
};


#endif /* MENUICON_H */

