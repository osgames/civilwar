/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef VESA_ASM_H
#define VESA_ASM_H
/*
 **********************************************************************
 * $Id$
 **********************************************************************
 *
 *	Interface to Vesa Assembly level routines
 *
 **********************************************************************
 *
 * $Log$
 * Revision 1.1  2001/03/19 06:19:38  dor10122
 * Restructuring source tree to GNU standards.
 *
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.5  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/05/04  22:12:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/04/11  21:31:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/11/05  16:52:08  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/10/26  14:54:37  Steven_Green
 * Initial revision
 *
 *
 **********************************************************************
 */

#include "realmode.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Function calls
 */

VesaInfo* getVesaInfo(Selector& selector);
VesaModeInfo* getVesaModeInfo(VesaMode mode, Selector& selector);
VesaState setVesaBank(VesaBank bank, int window);
VesaState setVesaMode(VesaMode mode);
VesaMode getVesaMode();
int isVesa();

#ifdef __cplusplus
};
#endif

#endif /* VESA_ASM_H */

/* Depends on h\real_s.h , touched on 05-04-94 at 12:21:12 */
/* Depends on h\realmode.h , touched on 05-04-94 at 12:29:30 */
/* Depends on h\realmode.h , touched on 05-04-94 at 15:05:21 */
/* Depends on h\realmode.h , touched on 05-09-94 at 12:00:35 */
