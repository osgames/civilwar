#####################################################################
#
# Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
# This Software is subject to the GNU General Public License.  
# For License information see the file COPYING in the root directory of the project.
# For more information see the file README.
#
#####################################################################
# $Id$
####################################################
#
# ACW Makefile include file for Watcom make
#
####################################################
#
# $Log$
# Revision 1.1  2001/03/15 14:08:48  greenius
# Converted filenames to lower case
#
# Revision 1.1  2001/03/11 00:58:48  greenius
# Added to sourceforge
#
# Revision 1.1  1994/08/22  16:09:53  Steven_Green
# Initial revision
#
####################################################


Assembler = TASM

#######################
# Where things are kept

.extensions:
.extensions: .exe .ldb .lnl .lnk .lib .obj .obb .obc .o4m .dem .cpp .c .h .asm .i

.DEFAULT:
	@%null

.ERASE


C = c
S = s
H = h
LIBH = libh
LIBDIR = lib
!ifdef NODEBUG
O = odb
!else
!ifdef NOLOG
O = onl
!else
O = o
!endif
!endif
I = s
!ifdef %DSMI_H
DSMIINC = $(%DSMI_H)
DSMILIB = $(%DSMI_LIB)\dsmi32.lib
!endif
LNK = lnk

EXEPATH = .

.c   : $(C)
.cpp : $(C)
.h   : $(H)
.obj : $(O)
.obb : $(O)
.obc : $(O)
.o4m : $(O)
.dem : $(O)
.asm : $(S)
.i   : $(I)
.exe : $(EXEPATH)
.lib : $(LIBDIR)
.ldb : $(LNK)
.lnl : $(LNK)
.lnk : $(LNK)

#######################
# Define Compiler tools

CC = wcc386
CPP = wpp386
CL = wcl386
LN = *wlink
LNFLAGS = 
LIB = wlib
LIBFLAGS = -b -c

!ifeq Assembler TASM
AS = tasm
ASFLAGS = /i$(I) /ml /p /w+ /zi /m /jIDEAL /d__C32__
!else
AS = wasm
ASFLAGS = -3pr -d1 -i=$(I) -ms
!endif
# TOUCH = touch
TOUCH = @echo /* Touched on %_DATE at %_TIME */ >> $@

CP = !fcopy


CFLAG1 = -4r -i=$(H) -fr=.\ -i=$(LIBH) -D__C32__ -bt=DOS
# -zm

!ifdef %DSMI_H
CFLAG1 +=  -i=$(DSMIINC) -dSOUND_DSMI
!endif

# -DTESTING

CFLAGOPT = $(CFLAG1) -s -UDEBUG -DSCREENDUMP
CFLAGNL = $(CFLAG1) -s -d2 -UDEBUG -DSCREENDUMP
CFLAGDEBUG = $(CFLAG1) -d2 -DDEBUG -s -DTESTING -DSCREENDUMP

!ifdef NODEBUG
CFLAGS  = $(CFLAGOPT) -oneatx
# CFLAGDB = $(CFLAGOPT) -oneatx
!else
!ifdef NOLOG
CFLAGS  = $(CFLAGNL) -oneatx
# CFLAGDB = $(CFLAGNL) -oneatx
!else
CFLAGS  = $(CFLAGDEBUG)
# CFLAGDB = $(CFLAGDEBUG)
!endif
!endif

###########################
# Do not allow use of bitfields...

CFLAGS += -DNOBITFIELD

###########################
# Set up C++ flags

CPPFLAGS = $(CFLAGS) -xst

###########################
# Set up Linker Flags

LNFLAGS += SYSTEM dos4g
LNFLAGS += DEBUG ALL
LNFLAGS += OPTION CASEEXACT
LNFLAGS += OPTION VERBOSE
# LNFLAGS += OPTION MAP
LNFLAGS += OPTION ARTIFICIAL
LNFLAGS += OPTION ELIMINATE
# LNFLAGS += OPTION MANGLEDNAMES
LNFLAGS += OPTION STATIC
LNFLAGS += OPTION STACK=8192


#######################
# General rules

.c.obj: .AUTODEPEND
	$(CC) $[* -fo$(O)\$^.

.cpp.obj: .AUTODEPEND
	$(CPP) $[* -fo$(O)\$^.

.c.o4m: .AUTODEPEND
	$(CC) -DFOURMEG $[* -fo$(O)\$^.

.cpp.o4m: .AUTODEPEND
	$(CPP) -DFOURMEG $[* -fo$(O)\$^.

.c.obb: .AUTODEPEND
	$(CC) -DTESTBATTLE $[* -fo$(O)\$^.

.cpp.obb: .AUTODEPEND
	$(CPP) -DTESTBATTLE $[* -fo$(O)\$^.

.c.obc: .AUTODEPEND
	$(CC) -DTESTCAMP $[* -fo$(O)\$^.

.cpp.obc: .AUTODEPEND
	$(CPP) -DTESTCAMP $[* -fo$(O)\$^.

.cpp.dem: .AUTODEPEND
	$(CPP) -DCOMPUSA_DEMO $[* -fo$(O)\$^.

!ifeq Assembler TASM
.asm.obj:
	$(AS) $(ASFLAGS) $[*, $(O)\$^.
!else
.asm.obj:
	$(AS) $(ASFLAGS) $[* -fo$(O)\$^.
!endif

.cpp.exe:
	$(CL) $(CPPFLAGS) $[*

.c.exe:
	$(CL) $(CFLAGS) $[*



!ifdef NODEBUG
DAGLIBDEMO = dl_dem.lib
DAGLIB = daglib.lib
LZHUF_LIB = lzhlib.lib
LNKEXT = lnk
!else
!ifdef NOLOG
DAGLIBDEMO = dldem_nl.lib
DAGLIB = daglibnl.lib
LZHUF_LIB = lzhlibnl.lib
LNKEXT = lnl
!else
DAGLIBDEMO = dldem_db.lib
DAGLIB = daglibdb.lib
LZHUF_LIB = lzhlibdb.lib
LNKEXT = ldb
!endif
!endif

.BEFORE:
	# @del *.err *.tmp c\*.err s\*.err >& nul:
	set WCC386=$(CFLAGS)
	set WPP386=$(CPPFLAGS)
