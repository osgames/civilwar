/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MENUDATA_H
#define MENUDATA_H

#ifndef __cplusplus
#error menudata.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Data used inside a menu screen
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.26  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.20  1994/04/22  22:16:24  Steven_Green
 * Icon Handling Tidied Up
 *
 * Revision 1.1  1994/01/11  22:30:12  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifndef TYPES_H
#include "types.h"
#endif
#ifndef ICON_H
#include "icon.h"
#endif
#include "micelib.h"

class InputIcon;

/*
 * Variables used whilst in a screen
 */

class MenuData : public IconSet {		// public FunctionMode, 
public:
	enum MenuMode {
		MenuMainMenu = -2,	// Go to main menu
		MenuQuit = -1,			// Quit/Abort
		MenuOK = 0				// Go ahead and continue with program
	};

	InputIcon* currentInput;
private:

	MenuData* parent;

	UBYTE flags;

	enum FlagBits {
		UpdateFlag = 0x01,
		FinishedFlag = 0x02,
		// RemoteAnswered = 0x04,
		// RemotesReply = 0x08,
#ifdef COMMS_OLD
		RemoteWantFinish = 0x10,
#endif
	};

	Boolean paused;

	int mode;

	PointerIndex tempPointer;
	PointerIndex pointer;

protected:
	Boolean shownHint;

public:
	MenuData(MenuData* parent = 0);

	void setUpdate() { flags |= UpdateFlag; }
	void clearUpdate() { flags &= ~UpdateFlag; }
	Boolean needUpdate() { return flags & UpdateFlag; }

	void clearFinish() { flags &= ~FinishedFlag; }
	void setFinish() { flags |= FinishedFlag; }
	void setFinish(int m) { mode = m; flags |= FinishedFlag; }
	Boolean isFinished() { return flags & FinishedFlag; }

#ifdef COMMS_OLD
	void setRemoteWantFinish(int how) { mode = how; flags |= RemoteWantFinish; }
	void clearRemoteWantFinish() { flags &= ~RemoteWantFinish; }
	Boolean getRemoteWantFinish() { return flags & RemoteWantFinish; }
#endif

	int getMode() const { return mode; }

	void redrawIcon() { IconSet::redrawIcon(); }
	void process();
	void runProc();

	void setTempPointer(PointerIndex i) { tempPointer = i; }
	void setPointer(PointerIndex i) { pointer = i; tempPointer = i; }
	PointerIndex getPointer() const { return pointer; }

	virtual void* getData() { return 0; }

	virtual void proc();

	virtual void showHint(const char* s);
	virtual void showStatus(const char* s);

	void pause() { paused = True; }
	void unPause() { paused = False; }
	Boolean isPaused() const { return paused; }
};

#endif /* MENUDATA_H */
