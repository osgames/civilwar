/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef TEXT_H
#define TEXT_H

#ifndef __cplusplus
#error text.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Text Display
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 *
 * Revision 1.13  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.8  1994/04/20  22:24:21  Steven_Green
 * Complete rewrite using TextWindow class
 *
 *----------------------------------------------------------------------
 */

#include "image.h"
#include "font.h"

/*
 * Flags used for justification
 */

#define TW_CH 1			// Horizontal centering
#define TW_CV 2			// Vertical centering
#define TW_C (TW_CH | TW_CV)

#define TW_CentreH 1			// Horizontal centering
#define TW_CentreV 2			// Vertical centering
#define TW_Centre (TW_CH | TW_CV)

#define TW_Left	0		// Left Alignment
#define TW_Top		0		// Top Alignment


/*
 * Text Window class
 */

class TextWindow {
	Region* bitMap;
	const Font* font;

	Colour foreground;
	Colour background;

	Boolean transparent:1;
	Boolean hCentre:1;
	Boolean vCentre:1;
	Boolean wrap:1;
	Boolean shadow:1;
	Boolean bold:1;

	Point where;

public:
	TextWindow(Region* bm, FontID n);

	void draw(const char* s);
	void draw(unsigned char c);
	void wprintf(const char* fmt, ...);

	void setPosition(Point p) { where = p; }
	const Point& getPosition() const { return where; }

	void setClip(const Rect& r)
	{
		bitMap->setClip(r.getX(), r.getY(), r.getW(), r.getH());
		setPosition(Point(0,0));
	}

	void setColours(Colour f, Colour b)
	{
		foreground = f;
		background = b;
		transparent = False;
	}

	void setColours(Colour f)
	{
		foreground = f;
		transparent = True;
	}

	void setFormat(Boolean verticalCentre, Boolean horizontalCentre)
	{
		vCentre = verticalCentre;
		hCentre = horizontalCentre;
	}

	void setFormat(UBYTE flag)
	{
		vCentre = ((flag & TW_CV) != 0);
		hCentre = ((flag & TW_CH) != 0);
	}

	void setHCentre(Boolean c) { hCentre = c; }
	void setVCentre(Boolean c) { vCentre = c; }
	void setWrap(Boolean c) { wrap = c; }
	void setShadow(Colour c) { shadow = True; background = c; }
	void clearShadow() { shadow = False; }

	void setBold(Boolean c) { bold = c; }

	const Font* getFont() const { return font; }
	void setFont(FontID n) { font = fontSet(n); }

	void setLeftMargin(SDimension x) { where.x = x; }
	void moveDown(SDimension y) { where.y += y; }
	void newLine()  { where.y += font->getHeight(); where.x = 0; }
};



#endif /* TEXT_H */
