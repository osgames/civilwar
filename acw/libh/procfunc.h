/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef PROCFUNC_H
#define PROCFUNC_H

#ifndef __cplusplus
#error procfunc.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Typedef of simple function
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/05/19  17:47:49  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

typedef void (*ProcFunc)();


#endif /* PROCFUNC_H */

