/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef ILBM_H
#define ILBM_H

#ifndef __cplusplus
#error ilbm.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Header for ILBM... functions for reading LBM files
 *
 * Written by Steven Green based on Mark McCubbin's stuff from TILE
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.8  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.6  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/01/06  22:40:53  Steven_Green
 * Filename is const
 *
 * Revision 1.3  1993/12/21  00:34:28  Steven_Green
 * Exception class defined
 *
 * Revision 1.2  1993/12/15  20:58:04  Steven_Green
 * palette defaults to 0.
 *
 * Revision 1.1  1993/12/03  17:02:33  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifndef TYPES_H
#include "types.h"
#endif

#ifndef ERROR_H
#include "error.h"
#endif

class Image;
class Palette;
struct BMHD;
// class Vesa;

/*
 * Functions
 */

int readILBM(const char* name, Image* image, Palette* palette = 0, BMHD* header = 0);
void writeILBM(const char* fname, Image& image, Palette& palette, UBYTE transparent = 0);

#endif /* ILBM_H */

/* Touched on 06-09-94 at 18:29:44 */
