/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef TIMER_H
#define TIMER_H

#ifndef __cplusplus
#error timer.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * BIOS Timer Interface
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.9  1994/09/02  21:28:59  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/08/22  16:09:04  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/08/09  15:48:13  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/03/11  23:14:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/03/10  14:29:08  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/02/28  23:05:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1993/12/01  15:12:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/11/30  02:57:48  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/09  14:02:26  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

#if defined(SOUND_DSMI)
#define DSMI_TIMER
#endif

#ifndef DSMI_TIMER
#include "timer_s.h"
#endif

//-- SWG:10mar01: Moved into timer_s.h
// typedef void __interrupt TimerFunction();

/*
 * Hardware clock resolution
 */

class Timer {
	volatile unsigned int	count;
	unsigned int lastCount;

#ifdef DSMI_TIMER
	volatile unsigned long timerCount;
#endif
	Boolean paused;

public:
			Timer();
		  ~Timer();
	long addRoutine(TimerFunction* function, long hz);
	void removeRoutine(long tag);

	unsigned int	getCount() const { return count; };
	void	wait(unsigned int ticks = 1);
	void	waitRel(unsigned int ticks = 1);

	unsigned long getFastCount() const { return timerCount; };

	void pause() { paused = True; }
	void unPause() { paused = False; }

	// int ticksPerSecond() const { return 55; }		// 18ms 
	static const int ticksPerSecond;
	static const int fastTPS;

#ifdef DSMI_TIMER
	friend void __interrupt timerHandler();
#else
	friend void far timerHandler();
#endif
};

extern Timer* timer;


#endif /* TIMER_H */

