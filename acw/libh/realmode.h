/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef REALMODE_H
#define REALMODE_H

#ifndef __cplusplus
#error realmode.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	RealMode C++ Interface
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.2  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/05/04  22:12:01  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "real_s.h"

/*
 * Access DOS memory
 */

template<class Type>
class RealMemory {
	Selector selector;
	Type* address;
public:
		RealMemory() { address = (Type*)allocDosMemory(sizeof(Type), selector); };
	  ~RealMemory() { freeDosMemory(selector); };

	operator Type*() { return address; }
	operator Type&() { return *address; }
};

class RealMemPtr {
	Selector selector;
	void* address;
public:
	RealMemPtr() { selector = 0; address = 0; }
	~RealMemPtr() { if(selector || address) freeDosMemory(selector); }

	void* alloc(size_t howMuch)
	{
		address = allocDosMemory(howMuch, selector);
		return address;
	}

	operator void*() { return address; }
};



#endif /* REALMODE_H */
