/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef FONT_H
#define FONT_H

#ifndef __cplusplus
#error font.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Font Management
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.6  1994/08/31  15:27:28  Steven_Green
 * Added New Font (EM_FL10)
 * /
 *
 * Revision 1.5  1994/06/09  23:36:46  Steven_Green
 * Added new fonts
 *
 * Revision 1.4  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/05/25  23:33:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/05/04  22:12:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/04/20  22:24:21  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

enum FontID {
	Font_JAME_5, 	// James' tiny 5 high font
	Font_JAME08, 	// James' tiny 8 high Times Roman
	Font_JAME15, 	// James' tiny 16 high Times Roman
	Font_JAME32, 	// James' tiny 32 high Times Roman

	Font_EMMA14,			// Emma's font
	Font_EMFL_8,	// Emma's flowery font
	Font_EMFL10,	// Emma's flowery font
	Font_EMFL15,	// Emma's flowery font
	Font_EMFL32		// Emma's flowery font
};

typedef ULONG FontOffset;

/*
 * Class for accessing Fonts
 */

class Font {
	friend class TextWindow;

	UWORD height;		// In Pixels
	UWORD first;		// ASCII value of 1st character in font
	UWORD howMany;		// Number of characters in font

	UBYTE* widths;		// Table of character widths
	FontOffset* offsets;	// Table of positions within bitstream
	UBYTE* bitStream;	// The bit patterns

	Boolean inSet(UWORD c) const { return (c >= first) && (c < (first + howMany)); }

public:
	Font(const char* fileName);
	~Font();

	UBYTE getWidth(UWORD c) const;
	UWORD getLineWidth(const char* s) const;
	UWORD getWidth(const char* s) const;

	UWORD getHeight() const { return height; }

	void getSize(const char* s, UWORD& width, UWORD& height);
};

/*
 * The bitstream is a continuous stream of bits.  Stored character by
 * character from top left of character to bottom right, starting with b7
 * of a byte.  There is no padding.  The value in the offset table is the
 * actual bit number, so an offset of 33 means start at the 2nd bit (b6)
 * of the 4th byte.
 */

/*
 * Font Set class
 */

class FontSet {
	Font** fonts;
	UBYTE howMany;		// Number of fonts in set
public:
	FontSet();
	~FontSet();

	Font* getFont(FontID n)
	{
		if(n < howMany)
			return fonts[n];
		else
			return fonts[0];
	}
};

extern FontSet* fonts;

inline Font* fontSet(FontID n) { return fonts->getFont(n); }

#endif /* FONT_H */

