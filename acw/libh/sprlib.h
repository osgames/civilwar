/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SPRLIB_H
#define SPRLIB_H

#ifndef __cplusplus
#error sprlib.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Sprite Library Interface
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.17  1994/08/22  16:09:04  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.16  1994/08/19  17:30:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.15  1994/07/19  19:56:23  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.14  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.10  1994/04/20  22:24:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/03/11  23:14:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/02/28  23:05:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/02/17  20:00:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/01/24  21:21:20  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/01/06  22:40:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/12/21  00:34:28  Steven_Green
 * Reads SpriteBlocks instead of Images.
 *
 * Revision 1.1  1993/12/16  22:20:15  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

// #include <string.hpp>
#include <fstream.h>

#ifndef SPRITE_H
#include "sprite.h"
#endif

#ifndef SWGSPR_H
#include "swgspr.h"
#endif

#ifndef ERROR_H
#include "error.h"
#endif

class SpriteLibrary;
class SpriteCacheList;

class SpriteBlock : public Sprite {
	// friend class SpriteLibrary;
public:
	SpriteLibrary* library;

	UWORD fullWidth;
	UWORD fullHeight;
	UWORD xOffset;
	UWORD yOffset;
	 WORD anchorX;
	 WORD anchorY;
	UBYTE shadowColor;
	SpriteFlags flags;
public:
	SpriteBlock(SDimension w, SDimension h, SpriteLibrary* l): Sprite(w, h) { library=l; }

	SDimension adjustX(SDimension x) const { return SDimension(x - anchorX); }
	SDimension adjustY(SDimension y) const { return SDimension(y - anchorY); }

	void release();
};


typedef int SpriteIndex;

class SpriteLibrary {
	char* fileName;

	ifstream fileHandle;

	UWORD spriteCount;			// Number of sprites in file
	ULONG *offsets;				// File offsets

	SpriteCacheList* cache;

public:
	SpriteLibrary(const char* name);
	~SpriteLibrary();

	SpriteBlock* read(SpriteIndex n);
	void release(SpriteBlock* sprite);
	void release(SpriteIndex n);

	void drawSprite(Region* bm, Point where, SpriteIndex i);
	void drawAnchoredSprite(Region* bm, Point where, SpriteIndex i);
	void drawCentredSprite(Region* bm, SpriteIndex i);
};


#endif /* SPRLIB_H */

