/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MYASSERT_H
#define MYASSERT_H

#ifndef __cplusplus
#error myassert.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	My Assert Macro
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG

extern void myAssert(int value, char* expr, char* file, int line);

#define ASSERT(expr) ( (expr) ? (void) 0 : myAssert(expr, #expr, __FILE__,__LINE__))

#else

#define ASSERT(expr) ((void)0)

#endif


#endif /* MYASSERT_H */

