/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef DIALOGUE_H
#define DIALOGUE_H

#ifndef __cplusplus
#error dialogue.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	General Purpose Dialogue boxes
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.10  1994/08/22  16:09:04  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/07/11  14:32:07  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/06/29  21:44:44  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.3  1994/05/25  23:33:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/05/04  22:12:01  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include <stddef.h>		// somewhere to get size_t decleration
#include "image.h"
#include "icon.h"
#include "font.h"
// #include "procfunc.h"
#include "sys_lib.h"

class SpriteBlock;

/*
 * Do a dialogue box
 * buttons is a string containing text to put in buttons
 * in a format such as:
 *   "OK|CANCEL|UNDO"
 *
 * Returns:
 *   Number of icon clicked: 0=1st, 1=2nd, etc
 */

struct MenuChoice {
	const char* text;		// Item to display
	int value;				// Value to return if pressed (0=disabled)

	MenuChoice() { }
	MenuChoice(const char* s, int v) { text = s; value = v; }
};

int dialAlert (MenuData* control, const Point& p, const char* text, const char* buttons);
int dialAlert (MenuData* control, const char* text, const char* buttons );
int dialInput (MenuData* control, const Point& p, const char* prompt, const char* buttons, char* buffer, size_t buflen, Boolean number = False);
int dialInput (MenuData* control, const char* prompt, const char* buttons, char* buffer, size_t buflen, Boolean number = False);
int menuSelect(MenuData* control, MenuChoice* choices);
int menuSelect(MenuData* control, MenuChoice* choices, const Point& p);
int chooseFromList(MenuData* func, const char* title, const char** choices, const char* cancelStr, const Point& where);
int chooseFromList(MenuData* func, const char* title, const char** choices, const char* cancelStr);

class PopupText {
	Boolean textUp;
	Image under;
	Point where;
	SpriteBlock* fillPattern;
public:
	PopupText(const char* s);
	~PopupText();

	void showText(const char* s);
	void clear();
};

class TextBox : public Icon {
	const char* text;
	FontID font;

	Colour interior;
	Colour border1;
	Colour border2;
	Colour textCol;
	SpriteBlock* fillPattern;

public:
	TextBox(IconSet* parent, const Rect& r, const char* text, FontID f,
		Colour iCol, Colour b1Col, Colour b2Col, Colour tCol);

	TextBox(IconSet* parent, const Rect& r, const char* text, FontID f,
		SystemSprite fill, Colour b1Col, Colour b2Col, Colour tCol);

	~TextBox();

	void drawIcon();
};

class ButtonIcon : public Icon {
	char* text;
	int value;
	FontID font;
	Boolean freeIt;
	SpriteBlock* fillPattern;
public:
	ButtonIcon(IconSet* set, const Rect& r, FontID f, int v, char* s, Key key, Boolean freeText);
	~ButtonIcon();

	void drawIcon();
	void execute(Event* event, MenuData* data);
};

class OuterBox : public IconSet {
	// Colour fillCol;
	SpriteBlock* fillPattern;
	Colour edge1Col;
	Colour edge2Col;
public:
	// OuterBox(IconSet* parent, const Rect& r, Colour fc, Colour e1, Colour e2) : IconSet(parent, r) { fillCol = fc; edge1Col = e1; edge2Col = e2; }
	OuterBox(IconSet* parent, const Rect& r, SystemSprite fc, Colour e1, Colour e2);
	~OuterBox();
	void drawIcon();
};

/*
 * Enhanced Popup Style dialogues
 * These are simpler to set up because an array is given containing
 * basic information about dialogues.
 * This is MUCH easier than setting up individual Icon classes for everything.
 */

enum DialType {
	Dial_End,			// End of dialogue item list
	Dial_FreeString,	// Some text... nothing happens if clicked
	Dial_Button,		// A button with text
	Dial_TextInput,	// Place for text to be typed into
	Dial_NumberInput,	// Place for Number to be typed into
	Dial_Box				// A rectangle used for visual grouping of icons
};

struct DialItem {
	DialType type;
	int id;
	Rect r;
	char* text;
	int bufferSize;		// Size of input buffer if type is Input
	Boolean enabled;
	Boolean highlighted;

	DialItem(DialType t, int i, const Rect& rect, char* s = 0, int len = 0)
	{
		type = t;
		id = i;
		r = rect;
		text = s;
		bufferSize = len;
		enabled = True;
		highlighted = False;
	}

	DialItem(DialType t)
	{
		type = t;
		id = 0;
		enabled = False;
		highlighted = False;
	}
};

class DialWork;

class Dialogue {
	DialWork* work;
public:
	Dialogue(DialItem* items) { setup(items); }
	Dialogue() { work = 0; }
	~Dialogue();

	void setup(DialItem* items);
	int process();

	virtual void doIcon(DialItem* item, Event* event, MenuData* d) = 0;
};


extern const char ButtonSeperator;

#endif /* DIALOGUE_H */

