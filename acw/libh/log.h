/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef LOG_H
#define LOG_H

#ifndef __cplusplus
#error log.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Debug Log
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.4  1994/07/19  19:56:23  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/03/10  14:29:08  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/02/09  15:01:35  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/12/23  09:29:10  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG

#include <fstream.h>
#include <iomanip.h>

class LogStream : public ofstream {
	int level;		// What level to show (0=none, 1=all, 10=messages of priority 10 or less)
public:
	LogStream(const char* s, int level = 0);
	~LogStream();

	int isShown(int l) { return l <= level; }
};

extern ostream& startLogLine(ostream&);
extern LogStream* logFile;

#define log(l, s) if(logFile && logFile->isShown(l)) { *logFile << startLogLine << s << endl; }

#else

#define log(l, s) ;		/* Do nothing... semicolon intentional */

#endif

#endif /* LOG_H */

