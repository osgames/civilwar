/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef PAL_S_H
#define PAL_S_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Functions in palette.asm
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/09/02  21:28:59  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif

void __interrupt paletteInterrupt();	// This is in palette.asm


#ifdef __cplusplus
};
#endif

#endif /* PAL_S_H */

