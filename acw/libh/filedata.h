/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef FILEDATA_H
#define FILEDATA_H

#ifndef __cplusplus
#error filedata.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Filedata
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1993/12/16  22:20:15  Steven_Green
 * Initial revision
 *
 * Revision 1.1  1993/12/16  16:42:54  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifndef TYPES_H
#include "types.h"
#endif

/*
 * Disk Format
 */

typedef union {
	UBYTE b[4];
} ILONG;

typedef union {
	UBYTE b[2];
} IWORD;

typedef struct {
	UBYTE b[1];
} IBYTE;

void putWord(IWORD* dest, UWORD src);
void putLong(ILONG* dest, ULONG src);

inline void putByte(IBYTE* dest, UBYTE src)
{
	dest->b[0] = src;
}

UWORD getWord(IWORD* src);
ULONG getLong(ILONG* src);

inline UBYTE getByte(IBYTE* src)
{
 	return src->b[0];
}


#endif /* FILEDATA_H */
