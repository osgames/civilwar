/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef PALETTE_H
#define PALETTE_H

#ifndef __cplusplus
#error palette.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Palette definition
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.5  1994/09/23  13:30:51  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/09/02  21:28:59  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.2  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/12/04  01:07:29  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifndef TYPES_H
#include "types.h"
#endif

typedef UBYTE Hue;				// angle 0=red, 85=green, 170=blue, 255=grey
typedef UBYTE Saturation;		// 0=grey, 255=saturated
typedef WORD Intensity;
const Intensity intensityRange = 16384;						// Intensity resolution

class TrueColour {
public:
	UBYTE red;
	UBYTE green;
	UBYTE blue;

	TrueColour() {	}
	TrueColour(UBYTE r, UBYTE g, UBYTE b)
	{
		red = r;
		green = g;
		blue = b;
	}

	void fromHSV(Hue h, Saturation s, Intensity v);
	void fromHSL(Hue h, Saturation s, Intensity v);
};


struct Colour64 {
	UBYTE red;
	UBYTE green;
	UBYTE blue;
};

struct Palette64 {
	Colour64 colors[256];
	void setColours(int c, TrueColour* t, int count);
};

class Palette {
public:
	// UBYTE colors[256][3];
	TrueColour colors[256];

public:
	// UBYTE* address() { return &colors[0].red; };
	// size_t size() const { return sizeof(colors); };

	void set() const;				// Set the palette

	void setColours(int c, TrueColour* t, int count);

	void setColour(int c, UBYTE r, UBYTE g, UBYTE b)
	{
		colors[c].red = r;
		colors[c].green = g;
		colors[c].blue = b;
	}

	void setColour(int c, UBYTE* b)
	{
		colors[c].red = b[0];
		colors[c].green = b[1];
		colors[c].blue = b[2];
	}

	void getColour(int c, UBYTE& r, UBYTE& g, UBYTE& b)
	{
		r = colors[c].red;
		g = colors[c].green;
		b = colors[c].blue;
	}

	UBYTE* getColour(int c)
	{
		return &colors[c].red;
	}

	Boolean fadeTo(const Palette* src, int ratio);

	void setBlack();
};

void setHardwarePalette(UBYTE start, UWORD howMany, TrueColour* colours);
void setHardwarePalette(UBYTE start, UWORD howMany, Colour64* colours);

#endif /* PALETTE_H */

