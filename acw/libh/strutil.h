/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef STRUTIL_H
#define STRUTIL_H

#ifndef __cplusplus
#error strutil.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	String Utilities
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/03/10  14:29:08  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

char* copyString(const char* s);

#endif /* STRUTIL_H */

