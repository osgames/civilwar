/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef LZHUF_H
#define LZHUF_H

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	LZHuf compression
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/07/19  19:56:23  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include "types.h"


long encode_lzhuf_ff(FILE* infp, FILE* outfp, long size);
long encode_lzhuf_mm(UBYTE* src, UBYTE* dest, long size);
void decode_lzhuf(UBYTE* in, UBYTE* out, long inSize, long outSize);

#ifdef __cplusplus
};
#endif

#endif /* LZHUF_H */

