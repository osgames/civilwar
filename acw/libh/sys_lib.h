/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SYS_LIB_H
#define SYS_LIB_H

#ifndef __cplusplus
#error sys_lib.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Enumeration of sprites in system.spr
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */

#include "sys_spr.h"

enum SystemSprite {

	/*
	 * From scroller
	 */

	SCR_VScroll = SPR_scroller,		// Vertical scroll bar
	SCR_HScroll,		// Horizontal scroll bar
	SCR_UScroll,		// Up Arrow
	SCR_DScroll,		// Down Arrow
	SCR_LScroll,		// Left Arrow
	SCR_RScroll,		// Right Arrow
	SCR_UScrollP,	// Pressed Up Arrow
	SCR_DScrollP,	// Pressed Down Arrow
	SCR_LScrollP,	// Pressed Left Arrow
	SCR_RScrollP,	// Pressed Right Arrow
	SCR_BlankScroll,	// Blank Icon

	SCR_RotLeft,
	SCR_RotRight,

	Fill_Paper = SPR_sysfill,
	Fill_PaperWhite,
	Fill_Alert,
	Fill_FileSel,
	Fill_Button,
};

#endif /* SYS_LIB_H */

