/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef TRIG_H
#define TRIG_H

#ifndef __cplusplus
#error trig.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 * Trigonometry Definition
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.14  1994/09/02  21:28:59  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/08/09  15:48:13  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1994/07/04  13:33:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/06/29  21:44:44  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1994/06/09  23:36:46  Steven_Green
 * Removed commented out section.
 *
 * Revision 1.8  1994/06/07  18:33:23  Steven_Green
 * Wangle class simplified
 *
 * Revision 1.7  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/02/28  23:05:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/01/17  20:15:30  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/11/19  19:01:25  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1993/11/15  17:20:33  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#ifndef TYPES_H
#include "types.h"
#endif


typedef UBYTE Bangle;
typedef UWORD Wangle;
typedef UBYTE Qangle;	// Quadrant (0,1,2,3)

Wangle direction(int x, int y);		// arctan function

inline Wangle Degree(int d)
{
	return UWORD(( (d << 16) + 180) / 360);
}

inline Wangle bangleToWangle(Bangle b)
{
 	return b << 8;
}

inline Bangle wangleToBangle(Wangle w)
{
 	return w >> 8;
}

inline Wangle qangleToWangle(Qangle q)
{
	return q << 14;
}

inline Qangle wangleToQangle(Wangle w)
{
 	return w >> 14;
}


long msin(long value, Wangle angle);
long mcos(long value, Wangle angle);

/*
 * Quick and dirty distance routine
 */

template<class T>
inline T distance(T x, T y)
{
	if(x < 0) x = -x;
	if(y < 0) y = -y;

	if(x > y)
			return x + y / 2;
	else
			return y + x / 2;
}

#if 0
/*
 * Some random number routines
 */

unsigned int rand(unsigned int m);
unsigned int rand(unsigned int from, unsigned int to);
long lrand();
unsigned long lrand(unsigned long range);
#endif

#endif /* TRIG_H */

