/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef SYSICON_H
#define SYSICON_H

#ifndef __cplusplus
#error sysicon.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Icon Used by system
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 *
 *----------------------------------------------------------------------
 */



#include "icon.h"
#include "sys_lib.h"

class SystemIcon : public Icon {
	SystemSprite iconNumber;

public:
	SystemIcon(IconSet* parent, SystemSprite n, Rect r, Key k1 = 0, Key k2 = 0);
	void setIcon(SystemSprite n) { iconNumber = n; }
	void drawIcon();
};

#endif /* SYSICON_H */

