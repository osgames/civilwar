/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef DAGLIB_H
#define DAGLIB_H

#ifndef __cplusplus
#error daglib.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Include File to use the Dagger PC Library
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.3  1994/07/19  19:56:23  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/07/04  13:33:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/06/29  21:44:44  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "system.h"
#include "screen.h"
#include "mouselib.h"
#include "menudata.h"
#include "dialogue.h"
#include "text.h"
#include "colours.h"
#include "lzhuf.h"

#endif /* DAGLIB_H */

