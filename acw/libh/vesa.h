/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef VESA_H
#define VESA_H

#ifndef __cplusplus
#error vesa.h is for use with C++
#endif

/*
 **********************************************************************
 * $Id$
 **********************************************************************
 *
 * Interface to Vesa Driver
 *
 **********************************************************************
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:50  greenius
 * Added to sourceforge
 *
 * Revision 1.24  1994/07/11  14:32:07  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.23  1994/06/24  14:45:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.22  1994/06/09  23:36:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.21  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.20  1994/06/02  15:31:12  Steven_Green
 * Graphics system updated
 *
 * Revision 1.19  1994/05/19  17:47:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.18  1994/04/20  22:24:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.17  1994/04/13  19:48:44  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.16  1994/04/11  21:31:01  Steven_Green
 * Seperated setBank into functions to setABank and setBBank, and use
 * the apropriate bank for reading/writing depending on the values of
 * the attributes
 *
 * Revision 1.15  1994/04/05  12:29:47  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.14  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.13  1994/03/11  23:14:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.12  1994/02/28  23:05:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.11  1994/02/17  20:00:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.10  1993/12/10  16:08:02  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1993/12/04  16:26:00  Steven_Green
 * VesaError exception handler classes added.
 *
 * Revision 1.8  1993/12/04  01:07:29  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1993/11/19  19:01:25  Steven_Green
 * Altermative graphic mode for initialisation
 *
 * Revision 1.6  1993/11/16  22:43:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1993/11/15  17:20:33  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1993/11/09  14:02:26  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1993/11/05  16:52:08  Steven_Green
 * Support for mice added
 *
 * Revision 1.2  1993/10/26  21:33:33  Steven_Green
 * blit from portion of image to screen added
 * Clipping in blit routines
 *
 * Revision 1.1  1993/10/26  14:54:37  Steven_Green
 * Initial revision
 *
 *
 **********************************************************************
 */

#include "gr_types.h"
#include "error.h"
#include "image.h"

class Image;					// Forward reference
class Sprite;

/*
 * Define some data types and structures
 */

typedef UWORD VesaMode;			// Graphics Mode
const UWORD VESANoMode = 0xffff;

typedef UWORD VesaBank;			// Vesa "Window"
#define NoVesaBank VesaBank(-1)	// Illegal value

enum VesaState {
	VESA_OK = 0,
	VESA_ERROR
};

/*
 * Structure filled by Vesa Bios "Return Super VGA Information"
 */

struct VesaInfo {
	UBYTE			vesaSignature[4];		// 'VESA'
	UWORD 		vesaVersion;			// Version Number
	char*			oemStringPtr;			// Manufacturer's string
	UBYTE 		capabilities[4];		// What the card can do
	VesaMode*	videoModePtr;			// Array of supported modes
	UWORD 		memory;					// Kbytes of memory on the card
	UBYTE 		reserved[236];			// Pad out to 256 bytes
};

/*
 * Structure filled by Vesa Bios "Return Super VGA Mode Information"
 */

struct VesaModeInfo {
	UWORD 	modeAttributes;
	UBYTE 	winAAttributes;
	UBYTE 	winBAttributes;
	UWORD 	winGranularity;
	UWORD 	winSize;
	UWORD 	winASegment;
	UWORD 	winBSegment;
	WORD 		(*winFuncPtr)(void);
	UWORD 	bytesPerScanLine;
	UWORD 	xRes;
	UWORD 	yRes;
	UBYTE 	xXharSize;
	UBYTE 	yCharSize;
	UBYTE 	numberOfPlanes;
	UBYTE 	bitsPerPixel;
	UBYTE 	banks;
	UBYTE 	memoryModel;
	UBYTE 	bankSize;
	UBYTE 	imagePages;
	UBYTE 	reserved1;
	UBYTE 	redMask;
	UBYTE 	redField;
	UBYTE 	greenMask;
	UBYTE 	greenField;
	UBYTE 	blueMask;
	UBYTE 	blueField;
	UBYTE 	resMask;
	UBYTE 	resField;
	UBYTE 	directColor;
	UBYTE 	reserved2[216];
};

/*
 * modeAttributes usage
 */

enum {
	VESA_ValidMode   = 0x01,		// 1=valid mode
	VESA_Extended    = 0x02,		// 1=extended header
	VESA_BIOSOutput  = 0x04,		// 1= BIOS output functions available
	VESA_MonoColour  = 0x08,		// 0=mono, 1=colour
	VESA_TextGraphic = 0x10,		// 0=text, 1=graphics mode
};


/*
 * Class to process Vesa
 */


class Vesa : public ClipRect {

	// Private Data

	VesaMode		oldMode;						// Original graphics Mode
	VesaMode		currentMode;				// Current graphics Mode
	volatile VesaBank 	currentABank;
	volatile VesaBank 	currentBBank;
	UBYTE			readWin; 					// Window A or B (0 or 1)
	UBYTE			writeWin;


	UWORD			bankStep;					// Banks in a window
	size_t		winSize;						// Size of a window in bytes
	size_t 		bankSize;					// Size of a bank in bytes (granularity)
	SDimension	xRes;							// Size of screen in pixels
	SDimension	yRes;
	size_t		bytesPerScanLine;			// Offset between lines
	UBYTE*		windowPtrR;					// Where read bank is!
	UBYTE*		windowPtrW;					// Where write bank is!

	// Private Functions

	VesaState	setABank(VesaBank bank);	// Window A
	VesaState	setBBank(VesaBank bank);	// Window B

	VesaState	setRBank(VesaBank bank)	// Readable bank
	{
		if(readWin == 0)
			return setABank(bank);
		else
			return setBBank(bank);
	}

	VesaState	setWBank(VesaBank bank)	// writeable bank
	{
		if(writeWin == 0)
			return setABank(bank);
		else
			return setBBank(bank);
	}

	VesaBank		getRBank() const
	{
		if(readWin == 0)
			return currentABank;
		else
			return currentBBank;
	}

	VesaBank		getWBank() const
	{
		if(writeWin == 0)
			return currentABank;
		else
			return currentBBank;
	}

public:

	// Public Functions

				Vesa(VesaMode mode = VESANoMode, VesaMode mode1 = VESANoMode);
			  ~Vesa();

	void 		blit(Image* image, Point dest = Point(0,0), Rect src = Rect(0,0,0,0));
	void		blit(Image* image, SDimension x, SDimension y) { blit(image, Point(x, y)); }

	void		maskBlit(Sprite* sprite, Point dest);
	void		unBlit(Image* image, Point src);

	void		setClip(SDimension x = 0, SDimension y = 0, SDimension w = 0, SDimension h = 0);

#ifdef DEBUG
	friend void showVesaBank(Vesa& screen);
#endif
	// friend class BitMap;

	
	class VesaError : public GeneralError {
	public:
		VesaError(char* s): GeneralError(s) { }
	};

	class NoVesaBios : public VesaError {
	public:
		NoVesaBios(): VesaError("No Vesa Bios installed!") { }
	};

	class BadVesaMode : public VesaError {
	public:
		BadVesaMode(): VesaError("Vesa Card does not support graphics mode") { }
	};

	class NoBlock 		: public VesaError {
	public:
		NoBlock(): VesaError("Could not allocate real mode memory for Vesa Block") { }
	};
	
	class NoInfoBlock : public VesaError {
	public:
		NoInfoBlock(): VesaError("Could not allocate real mode memory for Vesa Mode Info block") { }
	};

};



#endif /* VESA_H */
