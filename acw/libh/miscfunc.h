/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef MISCFUNC_H
#define MISCFUNC_H

#ifndef __cplusplus
#error miscfunc.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Miscellaneous little template functions
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.9  1994/08/24  15:08:16  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1994/08/19  17:30:43  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1994/07/25  20:35:50  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1994/07/11  14:32:07  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1994/07/04  13:33:12  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1994/06/06  13:20:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/03/21  21:04:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1993/12/21  00:34:28  Steven_Green
 * abs forces Type conversion to avoid warnings about loss of precision..
 *
 * Revision 1.1  1993/12/10  16:08:02  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#if 0	// Use stdlib.h for this, because its intrinsic abs(int) works well
/*
 * Absolute value of a simple type
 */

template<class T>
inline T abs(T t)
{
	return T((t < 0) ? -t : +t);
}
#endif

/*
 * Return -1, 0 or +1
 */

template<class T>
inline T sgn(T d)
{
	if(d < 0)
		return -1;
	else if(d > 0)
		return +1;
	else
		return 0;
}

/*
 * Swap 2 variables
 */

template<class T>
inline void swap(T& t1, T& t2)
{
	T temp = t1;
	t1 = t2;
	t2 = temp;
}

/*
 * Based on graphics GEM algorithm using XOR
 */

template<class T>
inline void qswap(T& t1, T& t2)
{
	t1 ^= t2;
	t2 ^= t1;
	t1 ^= t2;
}


template<class T>
inline T min(T t1, T t2)
{
		return (t1 < t2) ? t1 : t2;
}

template<class T>
inline T max(T t1, T t2)
{
		return (t1 > t2) ? t1 : t2;
}


/*
 * Handy way of incrementing enumerations, which the compiler
 * otherwise complains about.
 *
 * e.g. enum { Red, Green } colour;
 *
 *  INCREMENT(colour);
 */

template<class T>
inline T INCREMENT(T& n)
{
	return n = T(n + 1);
}

template<class T>
inline T DECREMENT(T& n)
{
	return n = T(n - 1);
}



const char* makeNths(int number);
const char* romanNumber(int n);

#endif /* MISCFUNC_H */

