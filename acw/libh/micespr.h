/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
/*
 *====================================================
 *
 * Sprite Definitions for mice.lst
 *
 *====================================================
 */

#define SPR_mice 0
/* Total number of sprites = 49 */
