/*
 * Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
 * This Software is subject to the GNU General Public License.  
 * For License information see the file COPYING in the root directory of the project.
 * For more information see the file README.
 */
#ifndef REAL_S_H
#define REAL_S_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Real Mode support
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/03/15 15:18:13  greenius
 * Renamed files to lower case
 *
 * Revision 1.1  2001/03/11 00:58:49  greenius
 * Added to sourceforge
 *
 * Revision 1.1  1994/05/04  22:12:01  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef UWORD Selector;

/*
 * These are assembler functions
 */

void* allocDosMemory(size_t amount, Selector& selector);
void freeDosMemory(Selector selector);


#ifdef __cplusplus
};
#endif

#endif /* REAL_S_H */

