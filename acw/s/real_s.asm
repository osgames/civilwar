;---------------------------------------------------------------------
; Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
; This Software is subject to the GNU General Public License.  
; For License information see the file COPYING in the root directory of the project.
; For more information see the file README.
;
;---------------------------------------------------------------------
; $Id$
;----------------------------------------------------------------------
;
; Support for interfacing with real mode interrupts
;
;----------------------------------------------------------------------
;
; $Log$
; Revision 1.1  2001/03/15 15:18:13  greenius
; Renamed files to lower case
;
; Revision 1.1  2001/03/11 00:58:50  greenius
; Added to sourceforge
;
; Revision 1.1  1994/05/04  22:13:50  Steven_Green
; Initial revision
;
;
;----------------------------------------------------------------------

	include "types.i"
	include "real_s.i"

;----------------------------------------------
; void *allocDosMemory(size_t howMuch, UWORD& selector)
;
; Allocates memory from DOS for access by real mode
; returns NULL if there was an error
;
; Calls DPMI function 0x100
;
; Entered with:
;	howMuch   : eax
;  selector& : edx

	proc allocDosMemory_
	LOCAL m_selector:DWORD, m_segment:DWORD

		enter 0,0
		push ebx	edx esi

		mov esi,edx

		; Calculate paragraphs required

		add eax,0fh
		shr eax,4

		; Set up registers for interrupt

		mov bx,ax
		mov ax,0100h
		int 031h
		jc @@error

		; Calculate DOS/4GW linear address

		and eax,0000ffffh		; Clear high word
		shl eax,4

		mov [esi],dx
		jmp @@exit

@@error:
		xor eax,eax				; return NULL
		mov [esi],ax

@@exit:
		pop edx ebx

	  	leave
		ret
	endp allocDosMemory_

;------------------------------------------
; Free Dos Memory
;
; void freeDosMemory(Selector selector)

	proc freeDosMemory_
		enter 0,0
		push edx

		mov dx,ax
		mov ax,0101h				; Free DOS Memory
		int 031h

		pop edx
		leave
		ret
	endp freeDosMemory_


	
	end
