;---------------------------------------------------------------------
; Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
; This Software is subject to the GNU General Public License.  
; For License information see the file COPYING in the root directory of the project.
; For more information see the file README.
;
;---------------------------------------------------------------------
; $Id$
;----------------------------------------------------------------------
;
; General definitions and options that ALL assembly modules will need
;
;----------------------------------------------------------------------
;
; $Log$
; Revision 1.1  2001/03/15 15:18:13  greenius
; Renamed files to lower case
;
; Revision 1.1  2001/03/11 00:58:50  greenius
; Added to sourceforge
;
; Revision 1.3  1994/06/24  14:47:09  Steven_Green
; *** empty log message ***
;
; Revision 1.2  1993/11/05  16:53:45  Steven_Green
; *** empty log message ***
;
; Revision 1.1  1993/10/26  14:56:19  Steven_Green
; Initial revision
;
;
;----------------------------------------------------------------------


;----------------------------------------------------------
; Setup assembler options

	ideal
	P386N
	jumps
	smart

;----------------------------------------------------------
; Set up segments and whathaveyou

	MODEL USE32 SMALL

;----------------------------------------------------------
; Start off in code segment

	CODESEG

;----------------------------------------------------------
; Useful Macros

	union farPtr
		pointer dp ?

		struc
			off dd ?
			seg dw ?
		ends
	ends farPtr

