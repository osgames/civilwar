;---------------------------------------------------------------------
; Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
; This Software is subject to the GNU General Public License.  
; For License information see the file COPYING in the root directory of the project.
; For more information see the file README.
;
;---------------------------------------------------------------------
; $Id$
;----------------------------------------------------------------------
;
; Critical Error and Control-C Handlers
;
;----------------------------------------------------------------------
;
; $Log$
; Revision 1.1  2001/03/15 15:18:13  greenius
; Renamed files to lower case
;
; Revision 1.1  2001/03/11 00:58:50  greenius
; Added to sourceforge
;
;
;----------------------------------------------------------------------

	include "types.i"

	public installErrorHandlers_


	proc controlCHandler
		iretd
	endp controlCHandler

	proc criticalErrorHandler
		mov eax,3		; Fail
		iretd
	endp criticalErrorHandler


	proc installErrorHandlers_
	 	push ds eax edx

		; Disable Control-C Checking

		sub dl,dl
		mov ax,03301h
		int 21h

		; Set up Control-C Handler

		mov ax,seg controlCHandler
		mov edx,offset controlCHandler
		mov ds,ax
		mov ax,02523h		; Control-C Interrupt / Set Vector
		int 21h

		; Set up Control-Break Handler
		
		mov ax,seg controlCHandler
		mov edx,offset controlCHandler
		mov ds,ax
		mov ax,0251bh		; Control-Break Interrupt / Set Vector
		int 21h

		; Set up Critical Error Handler

		mov ax,seg criticalErrorHandler
		mov edx,offset criticalErrorHandler
		mov ds,ax
		mov ax,02524h
		int 21h

		pop edx eax ds
		ret
	endp installErrorHandlers_


	end
