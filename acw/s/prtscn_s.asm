;---------------------------------------------------------------------
; Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
; This Software is subject to the GNU General Public License.  
; For License information see the file COPYING in the root directory of the project.
; For more information see the file README.
;
;---------------------------------------------------------------------
; $Id$
;----------------------------------------------------------------------
;
; Print Screen Interrupt replacement
;
; Simply sets a variable that the game's main loop can test
;
;----------------------------------------------------------------------
;
; $Log$
; Revision 1.1  2001/03/15 15:18:13  greenius
; Renamed files to lower case
;
; Revision 1.1  2001/03/11 00:58:50  greenius
; Added to sourceforge
;
; Revision 1.1  1994/05/19  17:50:07  Steven_Green
; Initial revision
;
;
;----------------------------------------------------------------------

	include "types.i"
	include "prtscn_s.i"

PrtScn_INT = 5

	DATASEG
_wantScreenPrint db 0						; flag set when print screen pressed
OldPrtScn_Handler farPtr ?			; Original handler

	CODESEG

	proc installPrtScn_
	enter 0,0
		push ebx ecx edx ds es

		; Get the old interrupt address

		mov eax,03500h+PrtScn_INT
		int 021h
		mov [OldPrtScn_Handler.off],ebx
		mov [OldPrtScn_Handler.seg],es

		; Install new routine

		push cs
		pop ds
		mov edx,offset PrtScn_Handler
		mov eax,02500h+PrtScn_INT
		int 021h

		pop es ds edx ecx ebx
	leave
	ret
	endp installPrtScn_

	proc removePrtScn_
	enter 0,0
		push ebx ecx edx
		push ds
		lds edx,[OldPrtScn_Handler.pointer]
		mov eax,02500h+PrtScn_INT
		int 021h
		pop ds
		pop edx ecx ebx
	leave
	ret
	endp removePrtScn_

	proc PrtScn_Handler
		push ds
		push eax

		mov ax,DGROUP
		mov ds,ax

		mov al,[0417h]				; Keyboard status byte
		test al,8					; Alt key pressed
		jz @@callOld

		mov [_wantScreenPrint],1
		jmp @@finish

@@callOld:
		pushfd
		call [OldPrtScn_Handler.pointer]

@@finish:
		pop eax
		pop ds
		iretd

	endp PrtScn_Handler

	end
