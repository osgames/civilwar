;---------------------------------------------------------------------
; Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
; This Software is subject to the GNU General Public License.  
; For License information see the file COPYING in the root directory of the project.
; For more information see the file README.
;
;----------------------------------------------------------------------
;
; BIOS Timer Interface
;
;----------------------------------------------------------------------
;
; $Log$
; Revision 1.1  2001/03/15 15:18:13  greenius
; Renamed files to lower case
;
; Revision 1.1  2001/03/11 00:58:50  greenius
; Added to sourceforge
;
; Revision 1.3  1994/05/04  22:13:50  Steven_Green
; *** empty log message ***
;
; Revision 1.2  1993/12/01  15:12:53  Steven_Green
; *** empty log message ***
;
; Revision 1.1  1993/11/09  14:03:37  Steven_Green
; Initial revision
;
;
;----------------------------------------------------------------------

	include "types.i"
	include "timer_s.i"


TIMER_INT 	equ     8
timer			equ	040h		;port addr of 8253 interrupt timer
inta01		equ	021h		;see tech ref manual 5-53, Test 2.
Clockrate 	equ	3728d		;1193180/320 gives 320Hz: DOS's rate is 0ffff (18.2 HZ)

	macro TIMERCHIPDELAY
		LOCAL	L1,L2,L3,L4
		jmp	L1
L1:		jmp	L2
L2:		jmp	L3
L3:		jmp	L4
L4:
	ENDM TIMERCHIPDELAY

	DATASEG

_timerCount		 dd		?

DOSTimerRate	 dw		?		;DOS's timer rate
DOSTimerHandler farPtr	?		;DOS's timer int handler

timerHandler	farPtr	<0>		; Routine to call every 20ms

	CODESEG

;----------------------------------------------------------------------------
;	installIRQ	Install DOS timer interrupt routine
;
;	C definition	
;	void installIRQ(far (function*)(void):edx,eax);
;
;	Parameters
;			none
;	Return value	
;			void
;	Notes:
;----------------------------------------------------------------------------

		proc installIRQ_
		enter 0,0
		push ebx ecx edx ds es

		mov	[_timerCount],0	;Initialise counter
		mov	[timerHandler.off],eax
		mov	[timerHandler.seg],dx


		xor	bx,bx		;get old timer rate
		mov	ecx,010000h
@@ReadLoop:
		mov	al,0
		cli
		out	timer+3,al
		TIMERCHIPDELAY
		in	al,timer
		TIMERCHIPDELAY
		mov	ah,al
		in	al,timer
		sti
		xchg	al,ah
		cmp	bx,ax
		jnc	@@NextTime
		mov	bx,ax
@@NextTime:
		loop	@@ReadLoop
		mov	[DOSTimerRate],bx

		mov     eax,03500h+TIMER_INT		;Get pointer to previous handler
		int     021h                      	;..returns es:ebx
		mov	[DOSTimerHandler.off],ebx
		mov	[DOSTimerHandler.seg],es

		mov	dx,Clockrate		;gets dx to indicate clock speed to set
		cli							;disable interrupts
		mov	al,0ffh				;turn timed interrupts off
		out	inta01,al
		mov	al,036h
		out	timer+3,al
		TIMERCHIPDELAY
		mov	al,dl				;first value (lsb)
		out	timer,al
		TIMERCHIPDELAY
		mov	al,dh				;second value (msb)
		out	timer,al
		xor	al,al
		out	inta01,al			;turn timed interrupts back on
		sti					;enable interrupts

		push	cs				;get address of our handler in ds:edx
		pop	ds
		mov	edx,OFFSET timer_handler
		mov     eax,02500h+TIMER_INT 		;Install our new handler
		int     021h

		pop es ds edx ecx ebx
		leave
		ret
		endp installIRQ_


;----------------------------------------------------------------------------
;	removeIRQ	Remove DOS timer interrupt routine
;
;	C definition	
;	void removeIRQ(void);
;
;	Parameters
;			none
;	Return value	
;			void
;	Notes:
;----------------------------------------------------------------------------

		proc removeIRQ_
		enter 0,0
		push ebx ecx edx


			mov	dx,[DOSTimerRate]	;dos rate for timer
			cli
			mov	al,0ffh			;turn timed interrupts off
			out	inta01,al
			mov	al,036h
			out	timer+3,al
			TIMERCHIPDELAY
			mov	al,dl			;first value (lsb)
			out	timer,al
			TIMERCHIPDELAY
			mov	al,dh			;second value (msb)
			out	timer,al
			xor	al,al
			out	inta01,al		;turn timed interrupts back on
			sti

			push ds
			lds     edx,[DOSTimerHandler.pointer]
			mov     eax,02500h+TIMER_INT 	;Remove our handler from chain
			int     021h
			pop ds

      	mov ah,2              	; Read Real Time Clock
      	int 1ah               	; Time functions
      	mov bl,dh

@@swait:					; Wait for it to go exactly over a second
      	mov ah,2
      	int 1ah
      	cmp bl,dh
      	jz @@swait

      	; ch = BCD hours
      	; cl = BCD minutes
      	; dh = BCD seconds

      	mov al,ch
      	call tobcd
      	mov ch,al

      	mov al,cl
      	call tobcd
      	mov cl,al

      	mov al,dh
      	call tobcd
      	mov dh,al

      	xor dl,dl             ; Clear hundredths
      	mov ah,2dh            ; DOS set time
      	int 21h

		pop edx ecx ebx
		leave
		ret

		endp removeIRQ_

;--------------------------------------
; A local function to convert al to BCD

	PROC tobcd	NEAR
      		mov 	dl,al
      		and 	al,0f0h		; 10s (*16)
      		shr 	al,1   		; /2 (*8)
      		mov 	ah,al
      		shr 	al,1   		; /4 (*4)
      		shr 	al,1   		; /8 (*2)
      		add 	al,ah  		; 8+2=10
      		and 	dl,15
      		add 	al,dl
      		ret
 	ENDP tobcd


;
;----------------------------------------------------------------------------
;	timer_handler	DOS timer interrupt handler routine
;
;	C definition	
;			None
;	Parameters
;			none
;	Return value	
;			void
;	Notes:
;		This routine and any it calls will use the dos extenders
;		internal stack.
;----------------------------------------------------------------------------

	proc timer_handler
		push	ds es fs gs
		push	eax ebx ecx edx esi edi
		cld

		mov	ax,DGROUP		;setup our segments
		mov	ds,ax
		mov	es,ax

		inc	[ds:_timerCount]

		;------------------------
		; Call Music routine here
		;------------------------

		test	[_timerCount],011b	;call game every 4th int (80Hz)
		jnz	@@CallDOS

		;------------------------
		; Call C Routine here
		;------------------------

		movzx eax,[timerHandler.seg]
		or eax,[timerHandler.off]
		jz @@noCall
		call [timerHandler.pointer]
@@noCall:

@@CallDOS:
		test	[_timerCount],01111b	;call dos every 16th int (20Hz)
		jnz	@@Exit
		pushfd				
		call     [DOSTimerHandler.pointer]	;call DOS's Timer handler (Via DOS extender)
@@Exit:
		mov	al,020h
		out	020h,al			;send EOI

		pop 	edi esi edx ecx ebx eax
		pop	gs fs es ds
		iretd
	endp timer_handler

	end
