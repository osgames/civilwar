;---------------------------------------------------------------------
; Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
; This Software is subject to the GNU General Public License.  
; For License information see the file COPYING in the root directory of the project.
; For more information see the file README.
;
;---------------------------------------------------------------------
; $Id$
;----------------------------------------------------------------------
;
; Print screen handler header
;
;----------------------------------------------------------------------
;
; $Log$
; Revision 1.1  2001/03/15 15:18:13  greenius
; Renamed files to lower case
;
; Revision 1.1  2001/03/11 00:58:50  greenius
; Added to sourceforge
;
; Revision 1.1  1994/05/19  17:50:07  Steven_Green
; Initial revision
;
;
;----------------------------------------------------------------------

	global installPrtScn_:proc
	global removePrtScn_:proc

	DATASEG
	global _wantScreenPrint:byte
