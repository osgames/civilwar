;---------------------------------------------------------------------
; Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
; This Software is subject to the GNU General Public License.  
; For License information see the file COPYING in the root directory of the project.
; For more information see the file README.
;
;---------------------------------------------------------------------
; $Id$
;----------------------------------------------------------------------
;
; Text Assembly routines
;
;----------------------------------------------------------------------
;
; $Log$
; Revision 1.1  2001/03/15 15:18:13  greenius
; Renamed files to lower case
;
; Revision 1.1  2001/03/11 00:58:50  greenius
; Added to sourceforge
;
; Revision 1.3  1994/05/04  22:13:50  Steven_Green
; *** empty log message ***
;
; Revision 1.2  1993/11/30  02:58:23  Steven_Green
; Saved edx!
;
;  Revision 1.1  1993/11/19  19:03:26  Steven_Green
;  Initial revision
;
;
;----------------------------------------------------------------------

	include "types.i"

	public getFontPtr_

;----------------------------
; DPMI structure to call real mode interrupt

	STRUC RMI
		edi		dd ?
		esi		dd ?
		ebp		dd ?
		reserved	dd ?
		ebx		dd ?
		edx		dd ?
		ecx		dd ?
		eax		dd ?
		flags		dw ?
		es			dw ?
		ds			dw ?
		fs			dw ?
		gs			dw ?
		ip			dw ?
		cs			dw ?
		sp			dw ?
		ss			dw ?
	ENDS	RMI

	MACRO clrMem ad,length
	  push edi ecx ax
	    lea edi,ad
		 mov ecx,length
		 xor al,al
		 rep stosb
	  pop ax ecx edi
	ENDM  clrMem

;-----------------------------------------------
; UBYTE* getFontPtr()
;
; Returns address in eax


	proc getFontPtr_
	LOCAL rmi:RMI
		enter SIZE RMI,0

		push bx cx edx es

			clrMem [rmi],<SIZE RMI>		; Clear RMI structure

			mov [rmi.eax],01130h
			mov [rmi.ebx],0600h

			mov ax,ss
			mov es,ax
			mov ax,0300h
			mov bx,0010h
			mov cx,0
			lea edi,[rmi]
			int 031h
			jc @@error

			movzx eax,[rmi.es]
			shl eax,4
			movzx edx,[WORD PTR rmi.ebp]
			add eax,edx

			jmp @@ret
@@error:
			xor eax,eax
@@ret:
		pop es edx cx bx
		leave
		ret

	endp getFontPtr_

	end
