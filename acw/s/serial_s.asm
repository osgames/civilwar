;---------------------------------------------------------------------
; Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
; This Software is subject to the GNU General Public License.  
; For License information see the file COPYING in the root directory of the project.
; For more information see the file README.
;
;---------------------------------------------------------------------
; $Id$
;----------------------------------------------------------------------
;
; Serial Interrrupt
; Based on example in Watcom Q&A Booklet
;
;----------------------------------------------------------------------
;
; $Log$
; Revision 1.1  2001/03/15 15:18:13  greenius
; Renamed files to lower case
;
; Revision 1.1  2001/03/11 00:58:50  greenius
; Added to sourceforge
;
;
;----------------------------------------------------------------------

	include "types.i"

	UDATASEG

serialStack	db 1024 dup(?)		; A stack used by the mouse handler
_serialStack:

oldSS  farPtr ?		; Value to store ss:esp


	DATASEG

topSerialStack dp _serialStack
serialLock db 0				; Interlock flag


	CODESEG


	global serialIRQ_:proc		; External C Function

	public serialInterrupt_
	proc serialInterrupt_ far
	  pushad
	  push ds es fs gs
			mov ax,DGROUP
			mov ds,ax
			mov es,ax

			inc [serialLock]
			cmp [serialLock],1
			jne @@skip

			mov [oldSS.off],esp
			mov [oldSS.seg],ss

			lss esp,[topSerialStack]

			call serialIRQ_

		   lss esp,[oldSS.pointer]

@@skip:
		dec [serialLock]

		; Tell Interrupt Controller to finish Interrupt

		mov al,20h
		mov dx,20h
		out dx,al

		pop gs fs es ds
		popad
		iretd

	endp serialInterrupt_

end
