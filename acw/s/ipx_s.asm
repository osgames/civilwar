;---------------------------------------------------------------------
; Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
; This Software is subject to the GNU General Public License.  
; For License information see the file COPYING in the root directory of the project.
; For more information see the file README.
;
;---------------------------------------------------------------------
; $Id$
;----------------------------------------------------------------------
;
; IPX interface
;
;----------------------------------------------------------------------
;
; $Log$
; Revision 1.1  2001/03/15 15:18:13  greenius
; Renamed files to lower case
;
; Revision 1.1  2001/03/11 00:58:50  greenius
; Added to sourceforge
;
; Revision 1.1  1994/05/04  22:13:50  Steven_Green
; Initial revision
;
;
;----------------------------------------------------------------------

	include "types.i"
	include "ipx_s.i"
	include "real_s.i"

	DATASEG

ipx_epa	farPtr <0>				; Address of IPX function

	CODESEG

;----------------------------------------------
; Boolean IPX_install()
;
; returns TRUE (1) if IPX is installed else returns False (0)
; 
; Side effects:
;	sets up IPX function call address

	proc IPX_install_
 if 0
	enter 0,0
	push ebx ecx edx esi edi es

	mov ax,7a00h
	int 2fh
	cmp al,0ffh
	je @@present
	mov eax,0
	jmp @@return

@@present:
	mov [ipx_epa.seg],es
	mov [ipx_epa.off],edi

	mov eax,1
@@return:

	pop es edi esi edx ebx
 else
 	LOCAL rmi:RMI
	enter SIZE RMI,0
	push ebx ecx edx esi edi es
		clrMem [rmi],<SIZE RMI>

		mov [rmi.ax],7a00h
		realInt 2fh
		cmp [rmi.al],0ffh
		je @@present
		mov eax,0
		jmp @@return

@@present:
		; mov [ipx_epa.seg],[rmi.es]
		; mov [ipx_epa.off],[rmi.edi]

	mov eax,1
@@return:

	pop es edi esi edx ebx

 endif

	leave
	ret

	endp

;----------------------------------------------------
; Socket IPX_openSocket(Socket socket)
;
; Open an IPX socket

	proc IPX_openSocket_
 if 0
	enter 0,0
		push ebx ecx edx esi edi es

		mov dx,ax		; Socket number
		mov bx,0			; Open Socket Function Number
		mov al,0			; Short lived
		
		; call [ipx_epa.pointer]
		int 7ah

		cmp al,0
		jz @@gotSocket
		mov eax,0
		jmp @@finish
@@gotSocket:
		movzx eax,dx

@@finish:
		pop es edi esi edx ecx ebx
 else
 	LOCAL rmi:RMI
	enter SIZE RMI,0
		push ebx ecx edx esi edi es
		clrMem [rmi],<SIZE RMI>

		mov [rmi.dx],ax		; Socket number
		mov [rmi.bx],0			; Open Socket Function Number
		mov [rmi.al],0			; Short lived
		
		realInt 7ah

		cmp [rmi.al],0
		jz @@gotSocket
		mov eax,0
		jmp @@finish
@@gotSocket:
		movzx eax,[rmi.dx]

@@finish:
		pop es edi esi edx ecx ebx
 endif


	leave
	ret
	endp

;----------------------------------------------------
; void IPX_closeSocket(Socket socket)
;
; Close an IPX socket

	proc IPX_closeSocket_
 if 0
	enter 0,0
		push ebx ecx edx esi edi es
		mov dx,ax		; Socket number
		mov bx,1			; Close Socket Function Number
		int 7ah
		movzx eax,dx
		pop es edi esi edx ecx ebx
 else
 	LOCAL rmi:RMI
	enter SIZE RMI,0
		push ebx ecx edx esi edi es
		clrMem [rmi],<SIZE RMI>
		mov [rmi.dx],ax		; Socket number
		mov [rmi.bx],1			; Close Socket Function Number
		realInt 7ah
		pop es edi esi edx ecx ebx
 endif

	leave
	ret
	endp

;----------------------------------------------------
; int IPX_Call(UWORD function, void* buffer)
;
; Calls a specified IPX function

	proc IPX_call_
	LOCAL rmi:RMI
	enter SIZE RMI,0
		push ebx ecx edx esi edi es
			clrMem [rmi],<SIZE RMI>

			mov [rmi.bx],ax  						; Function Number in bx
			mov ax,dx								; Buffer in es:si
			and ax,000fh
			mov [rmi.si],ax
			shr edx,4
			mov [rmi.es],dx

			realInt 7ah								; Invoke IPX

			movzx eax,[rmi.al]					; Get return value

		pop es edi esi edx ecx ebx
	leave
	ret
	endp

	end
