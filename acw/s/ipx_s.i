;---------------------------------------------------------------------
; Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
; This Software is subject to the GNU General Public License.  
; For License information see the file COPYING in the root directory of the project.
; For more information see the file README.
;
;---------------------------------------------------------------------
; $Id$
;----------------------------------------------------------------------
;
; Header file for IPX functions
;
;----------------------------------------------------------------------
;
; $Log$
; Revision 1.1  2001/03/15 15:18:13  greenius
; Renamed files to lower case
;
; Revision 1.1  2001/03/11 00:58:50  greenius
; Added to sourceforge
;
; Revision 1.1  1994/05/04  22:13:50  Steven_Green
; Initial revision
;
;
;----------------------------------------------------------------------

	global IPX_install_:proc
	global IPX_openSocket_:proc
	global IPX_call_:proc
	global IPX_closeSocket_:proc

