;---------------------------------------------------------------------
; Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
; This Software is subject to the GNU General Public License.  
; For License information see the file COPYING in the root directory of the project.
; For more information see the file README.
;
;---------------------------------------------------------------------
; $Id$
;----------------------------------------------------------------------
;
; Wait for vertical Refreash
;
;----------------------------------------------------------------------
;
; $Log$
; Revision 1.1  2001/03/15 15:18:13  greenius
; Renamed files to lower case
;
; Revision 1.1  2001/03/11 00:58:50  greenius
; Added to sourceforge
;
;
;----------------------------------------------------------------------

include "types.i"
include "vertblnk.i"
include "real_s.i"

;----------------------------------------------------------------------
;-   void awaitVerticalBlank()
 


proc awaitVerticalBlank_

			 push dx
	 		 mov dx,3dah
			 in al,dx
			 cmp al, 255
			 je @@ret
@@already: in al,dx       ; already refreashing
			 and al,8
			 jz @@already
@@ploop:	 in  al,dx
			 and al,8
			 jz  @@ploop
@@ret:	 pop dx
			 ret

endp awaitVerticalBlank_


;----------------------------------------------------------------------
;   void setAllPalette( unsigned char* cols, UWORD start, UWORD last )

;proc setAllPalette_
;
;	LOCAL cols: , start:UWORD, last:UWORD
;			 push es
;			 push ds
;			 push dx
;			 mov ah,10h
;			 mov al,12h
;			 les dx,cols
;			 mov bx,[start]
;			 mov cx,[last]
;			 sub cx,bx
 ;			 int 10h
;			 pop dx
;			 pop ds
;			 pop es
;
end
