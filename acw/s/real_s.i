;---------------------------------------------------------------------
; Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
; This Software is subject to the GNU General Public License.  
; For License information see the file COPYING in the root directory of the project.
; For more information see the file README.
;
;---------------------------------------------------------------------
; $Id$
;----------------------------------------------------------------------
;
; Real Mode Interface Header
;
;----------------------------------------------------------------------
;
; $Log$
; Revision 1.1  2001/03/15 15:18:13  greenius
; Renamed files to lower case
;
; Revision 1.1  2001/03/11 00:58:50  greenius
; Added to sourceforge
;
; Revision 1.1  1994/05/04  22:13:50  Steven_Green
; Initial revision
;
;
;----------------------------------------------------------------------

;----------------------------------
; Structure for accessing Real Mode Interrupts

	STRUC RMI
		UNION
			edi		dd ?
			di			dw ?
		ENDS
		UNION
			esi		dd ?
			si			dw ?
		ENDS
		UNION
			ebp		dd ?
			bp			dw ?
		ENDS
		reserved	dd ?
		UNION
			ebx	dd ?
			bx		dw ?
			STRUC
				bl db ?
				bh db ?
			ENDS
		ENDS
		UNION
			edx	dd ?
			dx		dw ?
			STRUC
				dl db ?
				dh db ?
			ENDS
		ENDS
		UNION
			ecx	dd ?
			cx		dw ?
			STRUC
				cl	db ?
				ch db ?
			ENDS
		ENDS
		UNION
			eax	dd ?
			ax		dw ?
			STRUC
				al	db ?
				ah db ?
			ENDS
		ENDS
		flags		dw ?
		es			dw ?
		ds			dw ?
		fs			dw ?
		gs			dw ?
		ip			dw ?
		cs			dw ?
		sp			dw ?
		ss			dw ?
	ENDS	RMI

	UNION REALPTR
		ptr dd ?

		STRUC
			off dw ?
			seg dw ?
		ENDS

	ENDS  REALPTR

;----------------------------------------------
; Useful macros

	; Macro to convert real mode pointer to protected mode

	MACRO realToProt address
		movzx eax,[address.seg]
		shl eax,4
		movzx ebx,[address.off]
		add eax,ebx
		mov [address.ptr],eax
	ENDM  realToProt

	; Clear a block of memory

	MACRO clrMem ad,length
	  push edi ecx ax
	    lea edi,ad
		 mov ecx,length
		 xor al,al
		 rep stosb

	  pop ax ecx edi

	ENDM  clrMem

	MACRO realInt intNo
		mov ax,0300h				; DPMI Function
		mov bx,intNo				; Interrupt number
		mov cx,0						; Bytes to copy from stack
		lea edi,[rmi]				; Address of RMI structure
		int 031h
	ENDM  realInt

;----------------------------------------------
; Functions included in this module:

	global allocDosMemory_:proc
	global freeDosMemory_:proc

