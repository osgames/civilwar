;---------------------------------------------------------------------
; Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
; This Software is subject to the GNU General Public License.  
; For License information see the file COPYING in the root directory of the project.
; For more information see the file README.
;
;---------------------------------------------------------------------
; $Id$
;----------------------------------------------------------------------
;
; Palette Interrupts
;
;----------------------------------------------------------------------
;
; $Log$
; Revision 1.1  2001/03/15 15:18:13  greenius
; Renamed files to lower case
;
; Revision 1.1  2001/03/11 00:58:50  greenius
; Added to sourceforge
;
;; Revision 1.2  1994/09/23  13:31:35  Steven_Green
;; *** empty log message ***
;;
;; Revision 1.1  1994/09/02  21:32:43  Steven_Green
;; Initial revision
;;
;
;----------------------------------------------------------------------

	include "types.i"

	UDATASEG

paletteStack	db 1024 dup(?)		; A stack used by the mouse handler
_paletteStack:

oldSS  farPtr ?		; Value to store ss:esp


	DATASEG

topPaletteStack dp _paletteStack
lockFlag db 0				; Interlock flag


	CODESEG


	global paletteIRQ_:proc		; External C Function

	public paletteInterrupt_
	proc paletteInterrupt_ far
	  pushad
	  push ds es fs gs
			mov ax,DGROUP
			mov ds,ax
			mov es,ax

			inc [lockFlag]
			cmp [lockFlag],1
			jne @@skip

			mov [oldSS.off],esp
			mov [oldSS.seg],ss

			lss esp,[topPaletteStack]

			call paletteIRQ_

		   lss esp,[oldSS.pointer]

@@skip:
		dec [lockFlag]

		pop gs fs es ds
		popad
		iretd

	endp paletteInterrupt_

end
