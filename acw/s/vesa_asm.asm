;---------------------------------------------------------------------
; Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
; This Software is subject to the GNU General Public License.  
; For License information see the file COPYING in the root directory of the project.
; For more information see the file README.
;
;---------------------------------------------------------------------
; $Id$
;----------------------------------------------------------------------
;
; Interface to Vesa BIOS
;
;----------------------------------------------------------------------
;
; $Log$
; Revision 1.1  2001/03/15 15:18:13  greenius
; Renamed files to lower case
;
; Revision 1.1  2001/03/11 00:58:50  greenius
; Added to sourceforge
;
; Revision 1.7  1994/05/19  17:50:07  Steven_Green
; *** empty log message ***
;
; Revision 1.6  1994/05/04  22:13:50  Steven_Green
; *** empty log message ***
;
; Revision 1.5  1994/04/11  21:33:27  Steven_Green
; *** empty log message ***
;
; Revision 1.4  1993/12/21  00:37:07  Steven_Green
; Bug fix when freeing memory after an error.
;
; Revision 1.3  1993/11/05  16:53:45  Steven_Green
; *** empty log message ***
;
; Revision 1.2  1993/10/27  21:09:57  Steven_Green
; Option to disable setting of graphic mode for easier debugging
; with debuggers that don't like Vesa graphics modes!
;
; Revision 1.1  1993/10/26  14:56:19  Steven_Green
; Initial revision
;
;
;----------------------------------------------------------------------

	include "types.i"
	include "vesa_asm.i"
	include "real_s.i"

; NOGRAPHICS = 1			; Set this to stop the graphics mode changing


;----------------------------------------------------------
; Vesa Structures

	
	STRUC VesaInfo
		vesaSignature	db 4 dup (?)
		vesaVersion	 	dw ?
		oemStringPtr	REALPTR ?
		capabilities	db 4 dup (?)
		videoModePtr	REALPTR ?
		memory			dw ?
		reserved			db 236 dup (?)
	ENDS  VesaInfo

	STRUC VesaModeInfo
		modeAttributes		dw ?
		winAAttributes		db ?
		winBAttributes		db ?
		winGranularity		dw ?
		winSize				dw ?
		winASegment			dw ?
		winBSegment			dw ?
		winFuncPtr			REALPTR ?
		bytesPerScanLine 	dw ?
		xRes					dw ?
		yTes					dw ?
		xCharSize			db	?
		yCharSize			db	?
		numberOfPlanes		db	?
		bitsPerPixel		db ?
		banks					db ?
		memoryModel			db ?
		bankSize				db ?
		imagePages			db ?
		reserved1			db ?
		redMask				db ?
		redField				db ?
		greenMask			db ?
		greenField		  	db ?
		blueMask		  		db ?
		blueField			db ?
		resMask				db ?
		resField				db ?
		directColor			db ?
		reserved2			db 216 dup (?)
	ENDS  VesaModeInfo


	enum VesaError	{
		VESA_OK = 0,
		VESA_ERROR = -1
	}

	MACRO callVesa function
	LOCAL @error,@ret

		mov [rmi.eax], 04f00h + function
		; Call DPMI simulate real mode interrupt

		mov ax,0300h				; Simulate real mode interrupt
		mov bx,0010h 				; Video Interrupt
		mov cx,0						; Words to copy from stack
		lea edi,[rmi]				; Address of RMI structure
		int 031h
		jc @error

		; Check return value

		cmp [rmi.eax],04fh
		jnz @error

		mov eax,VESA_OK
		jmp @ret

@error:
		mov eax,VESA_ERROR
@ret:
	ENDM callVesa


;---------------------------------------
; Image Class structure

	STRUC Image
		width		dw ?
		height	dw ?
		data		dd ?
	ENDS Image

;---------------------------------------
; Vesa Class Structure

	STRUC Vesa
		oldMode				dw ?
		currentMode			dw ?
		currentBank			dw ?
		bankStep				dw ?
		winSize				dd ?
		bankSize				dd ?
		xRes					dw	?
		yRes					dw ?
		bytesPerScanLine	dd ?
		windowPtr			dd ?
	ENDS Vesa

;------------------------------------------
; Read VesaBlock data
;
; VesaInfo* getVesaBlock(Selector& selector)
;
; return:
;	NULL : Error
;  info : OK


	proc getVesaInfo_
	LOCAL rmi:RMI, selector:DWORD
		enter SIZE RMI + 4,0
		push ebx ecx edx esi edi

		mov [selector],eax

		clrMem [rmi],<SIZE RMI>		; Clear RMI structure

		; Ask DOS for some real memory

		mov ebx,SIZE VesaInfo		; How much memory do we need?
		shr ebx,4						; Convert to paragraphs
		mov ax,0100h					; Get DOS memory
		int 031h							; DPMI interrupt
		jc @@error

		mov esi,[selector]
		mov [esi],dx

		movzx esi,ax					; Make DOS/4GW address
		shl esi,4

		; Setup rmi data

		mov [rmi.es],ax				; Address to load
		callVesa 0
		jnz @@error1

		; Convert pointers to DOS/4GW pointers

 		realToProt <esi + VesaInfo.oemStringPtr>
		realToProt <esi + VesaInfo.videoModePtr>
		
		; Setup return value

		mov eax,esi

		jmp @@ret

	
		;---------------------------------
		; Error after DOS memory allocated

@@error1:
		mov eax,[selector]
		mov dx,[eax]
		mov ax,0101h				; Free DOS Memory
		int 031h
@@error:
		xor eax,eax
@@ret:
		pop edi esi edx ecx ebx
		leave
		ret
	endp getVesaInfo_

;------------------------------------------
; Free VesaBlock data
;
; void freeVesaBlock(Selector selector)

; freeVesaInfo_ = freeDosMemory_

;------------------------------------------
; VesaModeInfo* getVesaModeInfo(VesaMode mode, Selector& selector)
;
; return:
;	NULL : Error
;  info : OK


	proc getVesaModeInfo_
	LOCAL rmi:RMI,mode:WORD,selector:DWORD
		enter SIZE RMI + 2 + 4,0
		push ebx ecx edx esi edi

		mov [mode],ax	 				; Save the mode somewhere safe
		mov [selector],edx

		clrMem [rmi],<SIZE RMI>		; Clear RMI structure

		; Ask DOS for some real memory

		mov ebx,SIZE VesaModeInfo	; How much memory do we need?
		shr ebx,4						; Convert to paragraphs
		mov ax,0100h					; Get DOS memory
		int 031h							; DPMI interrupt
		jc @@error

		mov esi,[selector]
		mov [esi],dx

		movzx esi,ax					; Make DOS/4GW address
		shl esi,4

		; Setup rmi data

		mov [rmi.es],ax				; Address to load
		movzx eax,[mode]
		mov [rmi.ecx],eax

		callVesa 1
		jnz @@error1

		; Convert pointers to DOS/4GW pointers

 		realToProt <esi + VesaModeInfo.winFuncPtr>
		
		; Setup return value

		mov eax,esi

		jmp @@ret

	
		;---------------------------------
		; Error after DOS memory allocated

@@error1:
		mov eax,[selector]
		mov dx,[eax]
		mov ax,0101h				; Free DOS Memory
		int 031h
@@error:
		xor eax,eax
@@ret:
		pop edi esi edx ecx ebx
		leave
		ret
	endp getVesaModeInfo_

;------------------------------------------
; void freeVesaBlock(Selector selector)

; freeVesaModeInfo_ = freeDosMemory_

;-----------------------------------------------
; VesaError setVesaBank(VesaBank bank, int windowAB)
;
; ax = bank
; dx = use window A or B
;

	proc setVesaBank_
 ifndef NOGRAPHICS
	LOCAL rmi:RMI
		enter SIZE RMI,0
		push bx cx edi

		clrMem [rmi],<SIZE RMI>
	
		mov [rmi.ebx],edx				; Select window A or B
		mov [rmi.edx],eax				; Which page
		callVesa 5

		pop edi cx bx
		leave
 endif
		ret
	endp setVesaBank_

;-----------------------------------------------------
; VesaError setVesaMode(VesaMode mode)
;
; ax = mode

	proc setVesaMode_
 ifndef NOGRAPHICS
	LOCAL rmi:RMI
		enter SIZE RMI,0
		push bx cx edi

		clrMem [rmi],<SIZE RMI>
		
		mov [rmi.ebx],eax				; Which mode

		callVesa 2

		pop edi cx bx
		leave
 endif
		ret
	endp setVesaMode_

;-----------------------------------------------------
; VesaMode getVesaMode()

	proc getVesaMode_
	LOCAL rmi:RMI
		enter SIZE RMI,0
		push bx cx edi

		clrMem [rmi],<SIZE RMI>
		
		callVesa 3
		jnz @@ret

		mov eax,[rmi.ebx]
@@ret:

		pop edi cx bx
		leave
		ret
	endp getVesaMode_

;--------------------------------
; Boolean isVesa()
;
; Does VESA Bios exist?
; Return 0: OK
;       -1: Error

	proc isVesa_
	LOCAL rmi:RMI
		enter SIZE RMI,0
		push bx cx edi

		clrMem [rmi],<SIZE RMI>
		
		callVesa 3

		pop edi cx bx
		leave
		ret
	endp isVesa_




	end
