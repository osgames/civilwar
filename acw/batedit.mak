#####################################################################
#
# Civil War: Copyright (c) 1994-2001, Steven Green (acw@greenius.co.uk)
# This Software is subject to the GNU General Public License.  
# For License information see the file COPYING in the root directory of the project.
# For more information see the file README.
#
#####################################################
# $Id$
####################################################
#
# ACW Battle Editor makefile
#
# Use with Watcom wmake
#
####################################################
#
# $Log$
# Revision 1.1  2001/03/15 14:08:48  greenius
# Converted filenames to lower case
#
# Revision 1.1  2001/03/11 00:58:48  greenius
# Added to sourceforge
#
#
####################################################

!include include.mak

##################
# Set up special compilation stuff

.extensions:
.extensions: .exe .ldb .lnk .lib .obj .cpp .c .h .asm .i

!ifdef NODEBUG
O = o_bated
!else
O = o_batedb
!endif
.c   : $(C)
.cpp : $(C)
.h   : $(H)
.obj : $(O)
.asm : $(S)
.i   : $(I)
.exe : $(EXEPATH)
.lib : $(LIBDIR)
.ldb : $(LNK)
.lnk : $(LNK)

.cpp.obj: .AUTODEPEND
	set WPP386=$(CPPFLAGS) -DBATEDIT
	$(CPP) $[* -fo$(O)\$^.


#######################
# Program specifics

.BEFORE:
	@del *.err *.tmp c\*.err s\*.err >& nul:

all: batedit.exe getbmap.exe .SYMBOLIC

#######################
# Battle EDITOR

BATEDOBS = &
	bated.obj ed_troop.obj bated_ob.obj ed_land.obj &
	game.obj side.obj &
	batwind.obj scroll.obj terrain.obj &
	view3d.obj data3d.obj map3d.obj &
	unit3d.obj batmove.obj batdisp.obj staticob.obj loctrees.obj earth.obj &
	zpoly.obj zbuffer.obj sprlist.obj pool.obj &
	generals.obj mapob.obj &
	orders.obj logarmy.obj ob.obj &
	cal.obj calmain.obj calicon.obj calshow.obj &
	calgen.obj calreg.obj calinfo.obj &
	batltab.obj tables.obj &
	gametime.obj clock.obj barchart.obj darken.obj &
	gameicon.obj menuicon.obj filesel.obj baticon.obj &
	dfile.obj filesup.obj obfile.obj batfile.obj &
	options.obj files.obj language.obj
# connect.obj calmulti.obj &


batedit.exe: $(BATEDOBS) $(DAGLIB)
	@%make bated.$(LNKEXT)
	$(LN) $(LNFLAGS) NAME $@ @$(LNK)\bated.$(LNKEXT)
!ifdef %DBPATH
        $(CP) $@
!endif

bated.$(LNKEXT): batedit.mak
	@echo making $(LNK)\$^.
	@%create $(LNK)\$^.
	@%append $(LNK)\$^. PATH $(O)
	@%append $(LNK)\$^. LIB $(LIBDIR)\$(DAGLIB)
	@%append $(LNK)\$^. LIB $(DSMILIB)
	@for %i in ($(BATEDOBS)) do @%append $(LNK)\$^. FILE %i

##########################################################
# Utility to create LBM pictures of the height and terrain
# from a battle saved game

GETBMAPOBS = getbmap.obj dfile.obj language.obj

getbmap.exe : $(GETBMAPOBS) $(DAGLIB)
	@%make getbmap.$(LNKEXT)
	$(LN) $(LNFLAGS) NAME $@ @$(LNK)\getbmap.$(LNKEXT)
!ifdef %DBPATH
        $(CP) $@
!endif

getbmap.$(LNKEXT): batedit.mak
	@echo making $(LNK)\$^.
	@%create $(LNK)\$^.
	@%append $(LNK)\$^. PATH $(O)
	@%append $(LNK)\$^. LIB $(LIBDIR)\$(DAGLIB)
	@%append $(LNK)\$^. LIB $(DSMILIB)
	@for %i in ($(GETBMAPOBS)) do @%append $(LNK)\$^. FILE %i

