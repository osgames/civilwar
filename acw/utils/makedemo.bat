rem Make Demo Disks

goto lhaVersion

rem --------------------------------

:CMPversion

set LSTNAME=c:\wargame\cd_list\demodisk.lst
set FNAME=civil_e1

wstrip exe\acwdemo.exe
concat %LSTNAME %FNAME.BIN >! temp.bat
pkzip %FNAME %FNAME.BIN
call temp.bat
del %FNAME.BIN
del %FNAME.ZIP
ppsplit %FNAME.CMP 1457664 1000000
goto finish

rem --------------------------------

:lhaVersion

if exist civil.001 goto makefloppy

wstrip exe\acwdemo.exe
lha a /pmx civil.001 acwdemo.bat exe\testmus.exe exe\acwdemo.exe dos4gw.exe
lha a /pmx civil.001 art\BATTLE.SPR
lha a /pmx civil.001 art\F_EMFL10.FNT
lha a /pmx civil.001 art\F_EMFL15.FNT
lha a /pmx civil.001 art\F_EMFL32.FNT
lha a /pmx civil.001 art\F_EMFL_8.FNT
lha a /pmx civil.001 art\F_EMMA14.FNT
lha a /pmx civil.001 art\F_JAME08.FNT
lha a /pmx civil.001 art\F_JAME15.FNT
lha a /pmx civil.001 art\F_JAME32.FNT
lha a /pmx civil.001 art\F_JAME_5.FNT
lha a /pmx civil.001 art\GAME.SPR
lha a /pmx civil.001 art\MICE.SPR
lha a /pmx civil.001 art\PALETTE.LBM
lha a /pmx civil.001 art\SYSTEM.SPR
lha a /pmx civil.001 art\TERRMAP.TER
lha a /pmx civil.001 art\TEXTURE.SPR
lha a /pmx civil.002 art\MAPS\BIGMAP.LBM
lha a /pmx civil.002 art\MAPS\MAPZOOM.BM
lha a /pmx civil.002 art\SCREENS\CAL_SCRN.LBM
lha a /pmx civil.002 art\SCREENS\ENDGAME.LBM
lha a /pmx civil.002 art\SCREENS\MN_EUROP.LBM
lha a /pmx civil.002 art\SCREENS\MN_GER.LBM
lha a /pmx civil.002 art\SCREENS\MN_MENU.LBM
lha a /pmx civil.002 art\SCREENS\PLAYSCRN.LBM
lha a /pmx civil.002 art\SCREENS\REALISM.LBM
lha a /pmx civil.002 art\SCREENS\VICTORYC.LBM
lha a /pmx civil.002 art\SCREENS\VICTORYN.LBM
lha a /pmx civil.002 art\SCREENS\Victoryu.lbm

lha a /pmx civil.003 dbdata\BATLCRY.AMF
lha a /pmx civil.003 dbdata\BATLHYMN.AMF
lha a /pmx civil.003 dbdata\BATLMTHR.AMF
lha a /pmx civil.003 dbdata\BONBLUE.AMF
lha a /pmx civil.003 dbdata\BONBLUFL.AMF
lha a /pmx civil.003 dbdata\CLEARTRK.AMF
lha a /pmx civil.003 dbdata\CRYFREE.AMF
lha a /pmx civil.003 dbdata\CUMBRLND.AMF
lha a /pmx civil.003 dbdata\DIXIE.AMF
lha a /pmx civil.003 dbdata\JOHNNY.AMF
lha a /pmx civil.003 dbdata\LINCOLN.AMF
lha a /pmx civil.003 dbdata\MARYLAND.AMF
lha a /pmx civil.003 dbdata\RICHMOND.AMF
lha a /pmx civil.003 dbdata\ROLLALAB.AMF
lha a /pmx civil.003 dbdata\TENTING.AMF
lha a /pmx civil.003 dbdata\TRAMP.AMF
lha a /pmx civil.003 dbdata\YLOROSE.AMF

lha a /pmx civil.004 dbdata\BATTSLCT.LBM
lha a /pmx civil.004 dbdata\BIOGSLCT.LBM
lha a /pmx civil.004 dbdata\BIOGTEXT.LBM
lha a /pmx civil.004 dbdata\DATAMAIN.LBM
lha a /pmx civil.004 dbdata\FEATURE2.LBM
lha a /pmx civil.004 dbdata\FEATURES.LBM
lha a /pmx civil.004 dbdata\FLICTEXT.LBM
lha a /pmx civil.004 dbdata\HISTORY.LBM
lha a /pmx civil.004 dbdata\HISTSLCT.LBM
lha a /pmx civil.004 dbdata\PICFRAME.LBM
lha a /pmx civil.004 dbdata\TACTTEXT.LBM
lha a /pmx civil.004 dbdata\TCATSLCT.LBM
lha a /pmx civil.004 dbdata\WEAPSLCT.LBM
lha a /pmx civil.004 dbdata\WEAPTEXT.LBM
lha a /pmx civil.004 dbdata\WEAPTXT2.LBM

lha a /pmx civil.005 dbdata\ARTILL.ENG
lha a /pmx civil.005 dbdata\ARTTACT.ENG
lha a /pmx civil.005 dbdata\BATTLES.ENG
lha a /pmx civil.005 dbdata\BIOSPRS.SPR
lha a /pmx civil.005 dbdata\BIOTXT.ENG
lha a /pmx civil.005 dbdata\BRIEFHIS.ENG
lha a /pmx civil.005 dbdata\CAVTACT.ENG
lha a /pmx civil.005 dbdata\CAVWEAP.ENG
lha a /pmx civil.005 dbdata\CHRONO.ENG
lha a /pmx civil.005 dbdata\FEATURES.ENG
lha a /pmx civil.005 dbdata\HIST1861.ENG
lha a /pmx civil.005 dbdata\HIST1863.ENG
lha a /pmx civil.005 dbdata\ICONS_1.SPR
lha a /pmx civil.005 dbdata\INFTACT.ENG
lha a /pmx civil.005 dbdata\INFWEAP.ENG
lha a /pmx civil.005 dbdata\LANGUAGE.ENG
lha a /pmx civil.005 dbdata\NAVTACT.ENG
lha a /pmx civil.005 dbdata\NAVWEAP.ENG
lha a /pmx civil.005 dbdata\PICHEAD.ENG
lha a /pmx civil.005 dbdata\PICTURES.ENG
lha a /pmx civil.005 dbdata\PTR.ENG
lha a /pmx civil.005 dbdata\SONGS.ENG
lha a /pmx civil.005 gamedata\ANTIETAM.BTL
lha a /pmx civil.005 gamedata\BULLRUN.BTL
lha a /pmx civil.005 gamedata\CHICKAMU.BTL
lha a /pmx civil.005 gamedata\FREDRICK.BTL
lha a /pmx civil.005 gamedata\GETTYSBG.BTL
lha a /pmx civil.005 gamedata\MILLSPR.BTL
lha a /pmx civil.005 gamedata\SHILOH.BTL
lha a /pmx civil.005 gamedata\START.CAM
lha a /pmx civil.005 gamedata\WILSONS.BTL
lha a /pmx civil.005 sounds\CANNON.WAV
lha a /pmx civil.005 sounds\MELEE.WAV
lha a /pmx civil.005 sounds\SHOTGUN.WAV

:makefloppy

pause Insert Disk1 into drive A:
del a:\*.* /q /y /xsz
copy demodisk\install.bat demodisk\_install.bat c:\bin\lha.exe civil.001 a:\
pause Insert Disk2 into drive A:
del a:\*.* /q /y /xsz
copy civil.002 a:\
pause Insert Disk3 into drive A:
del a:\*.* /q /y /xsz
copy civil.003 a:\
pause Insert Disk4 into drive A:
del a:\*.* /q /y /xsz
copy civil.004 a:\
pause Insert Disk5 into drive A:
del a:\*.* /q /y /xsz
copy civil.005 demodisk\cleanup.bat a:\


rem --------------------------------

:finish
