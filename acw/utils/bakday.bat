@rem Backup source files after given date
verify on
pkzip -aP -t%1 a:s_%1 c\*.* h\*.* s\*.* makefile *.mak libh\*.* *.txt -x*.err
pkzip -aPr -t%1 a:s_%1 art\*.lbm artsrc\*.*
verify off

