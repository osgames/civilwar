rem --------------------------------------
rem Make a particular CMP File
rem
rem Usagf:
rem   make1cmp e1 listdir cd_dir
rem Pass it a type in the form e1
rem where 1 is the size and e is the language (english)
rem --------------------------------------

setlocal
set FNAME=civil_%1
set LSTNAME=%2\inst_%1.lst

pushd %3
concat %LSTNAME %FNAME.bin >! temp.bat
pkzip %FNAME %FNAME.bin
call temp.bat
del temp.bat
del %FNAME.bin
del %FNAME.zip
popd
