@echo off
: Create ptr.* for encyclopedia
:
: Pass it a language such as ENG or GER
:   allptr ENG

if "%1"=="" goto usage

set LANG=%1
set DOS4G=quiet



*del bio.ptr
echo Creating ptr.%LANG
getbio.exe dbdata\biotxt.%LANG 2 1
getbio.exe dbdata\battles.%LANG 4 0
getbio.exe dbdata\chrono.%LANG 5 0
getbio.exe dbdata\features.%LANG 9 0
getbio.exe dbdata\songs.%LANG D 0
getbio.exe dbdata\pictures.%LANG L 0
getbio.exe dbdata\pichead.%LANG 0 1
*move bio.ptr dbdata\ptr.%LANG
*copy /u dbdata\*.%LANG l:\dbdata
goto finish

:usage
echo.
echo Usagf:
echo allptr LNG
echo.
echo where LNG is the language extension
echo.
echo e.g.
echo allptr ENG
echo allptr GER
echo.
echo.

:finish
