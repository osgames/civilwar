@rem Backup source files to Backup Drive
: net map q: pentium
net map q: ACW_BACKUP
set MASTERBAK=q:\acw_src.zip
set BAKFILE=q:\acw_%@SUBSTR[%_DATE,0,2]%@SUBSTR[%_DATE,3,2].zip
pkzip -aP %MASTERBAK c\*.* h\*.* s\*.* makefile *.mak libh\*.* *.txt -x*.err
copy %MASTERBAK %BAKFILE

